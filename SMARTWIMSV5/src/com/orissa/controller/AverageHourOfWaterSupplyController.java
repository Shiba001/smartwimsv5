/**
 * @formatter:off
 *
 */
package com.orissa.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.orissa.common.DateCalculation;
import com.orissa.common.MessageUtil;
import com.orissa.common.SortingMap;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.AjaxAverageHourOfWaterSupplyModel;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.ULBModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DashboardService;
import com.orissa.service.DivisionService;
import com.orissa.validation.DashboardValidation;

@Controller
public class AverageHourOfWaterSupplyController {

	private static Logger logger = Logger.getLogger(AverageHourOfWaterSupplyController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private SortingMap sortingMap;
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private DateCalculation dateCalculation;
	
	@Autowired
	private DashboardValidation dashboardValidation;
	
	@RequestMapping(value = "/avg-hr-supply", method = RequestMethod.GET)
	public String AverageHourOfWaterSupplyPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("AverageHourOfWaterSupplyPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		model.addAttribute("loginUsersModel2", loginUsersModel);
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		LinkedHashMap<String, Double> lpcdTotal = null;
		LinkedHashMap<String, DailyWaterSupplyModel> lpcdMap = null;
		List<String> datesBetween = new LinkedList<String>();
		List<Double> currentLPCD = new LinkedList<Double>();
		List<Double> previousLPCD = new LinkedList<Double>();
		double lpcdBoxValue = 0.0D;
		String monthMap = null;
		String yearMap = null;
		String startDateMap = null;
		String endDateMap = null;
		String currentMonth = "";
		String previousMonth = "";
		
		model.addAttribute("startDate", startDate);
		
		if(startDate != null) {
			
			monthMap = startDate.substring(5, 7);
			yearMap = startDate.substring(0, 4);
			
			if (monthMap != null) {
				
				List<String> dates = null;
				try {
					dates = dateCalculation.findDate(Integer.valueOf(yearMap), Integer.valueOf(monthMap));
					startDateMap = dates.get(0);
					endDateMap = dates.get(1);
					currentMonth = dates.get(3);
					previousMonth = dates.get(4);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
		try {
			
			if(flag == 1) {
				
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
					
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
						
					lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
					lpcdBoxValue = (int)(Math.floor(lpcdBoxValue + 0.5));
					lpcdTotal = new LinkedHashMap<String, Double>();

					if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
						
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
								
								DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
								DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
								
								if(dailyWaterSupplyModel1.getType().equals("current")) {
								
									// For All LPCD
									lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
									
								} else {
									
									// For All LPCD
									lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getFreq_supply());
								}
						}
					
					} else {
					
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
						}
					}
				}
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getFreq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getPm_Freq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getPm_Freq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getFreq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_Freq_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				model.addAttribute("lpcdBoxValue", (int)lpcdBoxValue);
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
								
				/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
				
				int n = 3;
				if(amrutTypeId == null ) {
					 
					 n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
				 }
				
				model.addAttribute("topValue", n);
				
				HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
				
				//System.out.println("top10LpcdUlbs: "+ top10LpcdUlbs);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				 
				Set mapSet = (Set) top10LpcdUlbs.entrySet();
		        Iterator mapIterator = mapSet.iterator();
		        while (mapIterator.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
			        String keyValue = (String) mapEntry.getKey();
			        top10ulb.add(keyValue);
			        Double value = (Double) mapEntry.getValue();
			        top10lpcd.add(value);
		        }
		        
		        //System.out.println("top10ulb: "+ top10ulb);
				//System.out.println("top10lpcd: "+ top10lpcd);
				
				 model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
			     model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
			     
			     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
				
			     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
			     
			     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
			    
			    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
			     
			    //System.out.println("bottom10LpcdUlbs: "+ bottom10LpcdUlbs);
			     
		        LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
		        
		        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
		        Iterator mapIterator2 = mapSet2.iterator();
		        while (mapIterator2.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
			        String keyValue = (String) mapEntry.getKey();
			        bottom10ulb.add(keyValue);
			        Double value = (Double) mapEntry.getValue();
			        bottom10lpcd.add(value);
		        }
		        
		        //System.out.println("bottom10ulb: "+ bottom10ulb);
				//System.out.println("bottom10lpcd: "+ bottom10lpcd);
				 
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
				
				/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= lpcdBoxValue) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				model.addAttribute("flag", false);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_Freq_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_Freq_supply());
									
					}
				}
					
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
			} else if(flag == 2) {
				
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				lpcdBoxValue = (int)(Math.floor(lpcdBoxValue + 0.5));
				lpcdTotal = new LinkedHashMap<String, Double>();

				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getFreq_supply());
							}
					}
				
				} else {
				
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
					}
				}
			}
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getFreq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getPm_Freq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getPm_Freq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getFreq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_Freq_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				model.addAttribute("lpcdBoxValue", (int)lpcdBoxValue);
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
			    LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
					
			if(lpcdTotal.size() >= 6 ) {
					
				/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
				
				int n = 3;
				if(amrutTypeId == null ) {
					 
					 n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
				 }
				
				model.addAttribute("topValue", n);
				
				HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
				
				//System.out.println("top10LpcdUlbs: "+ top10LpcdUlbs);
				
				Set mapSet = (Set) top10LpcdUlbs.entrySet();
		        Iterator mapIterator = mapSet.iterator();
		        while (mapIterator.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
			        String keyValue = (String) mapEntry.getKey();
			        top10ulb.add(keyValue);
			        Double value = (Double) mapEntry.getValue();
			        top10lpcd.add(value);
		        }
		        
		        //System.out.println("top10ulb: "+ top10ulb);
				//System.out.println("top10lpcd: "+ top10lpcd);
				
				 model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
			     model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
			     
			     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
				
			     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
			     
			     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
			    
			    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
			     
			    //System.out.println("bottom10LpcdUlbs: "+ bottom10LpcdUlbs);
			     
		        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
		        Iterator mapIterator2 = mapSet2.iterator();
		        while (mapIterator2.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
			        String keyValue = (String) mapEntry.getKey();
			        bottom10ulb.add(keyValue);
			        Double value = (Double) mapEntry.getValue();
			        bottom10lpcd.add(value);
		        }
		        
		        //System.out.println("bottom10ulb: "+ bottom10ulb);
				//System.out.println("bottom10lpcd: "+ bottom10lpcd);
				 
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
				model.addAttribute("flag", false);
				
				/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
				
			} else {
				
				model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
			    model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
			     
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
				model.addAttribute("flag", true);
			}
			
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= lpcdBoxValue) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_Freq_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_Freq_supply());
									
					}
				}
				
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
			} else if(flag == 3) {
				
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				lpcdBoxValue = (int)(Math.floor(lpcdBoxValue + 0.5));
				lpcdTotal = new LinkedHashMap<String, Double>();

				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getFreq_supply());
							}
					}
				
				} else {
				
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
					}
				}
			}
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getFreq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getPm_Freq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getPm_Freq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getFreq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_Freq_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				model.addAttribute("lpcdBoxValue", (int)lpcdBoxValue);
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= lpcdBoxValue) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				model.addAttribute("flag", false);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
			     
			     String month = null;
					String year = null;
					String startDate1 = null;
					String endDate1 = null;
					
					if(startDate != null) {
						
						month = startDate.substring(5, 7);
						year = startDate.substring(0, 4);
						
						if (month != null) {
							
							List<String> dates = null;
							try {
								dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
								startDate1 = dates.get(0);
								endDate1 = startDate;
							} catch (NumberFormatException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							} 
							
						}
					}
					
					datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
					
					for(String date:datesBetween) {
						
						LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
						
						if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
							
							// For Progress Report Current Value
							currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_Freq_supply());
							
							// For Progress Report Previous Value
							previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_Freq_supply());
										
						}
					}
				
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
				
			} else if(flag == 4) {
				
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
				
				listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				lpcdBoxValue = (int)(Math.floor(lpcdBoxValue + 0.5));
				lpcdTotal = new LinkedHashMap<String, Double>();

				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getFreq_supply());
							}
					}
				
				} else {
				
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
					}
				}
			}
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getFreq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getPm_Freq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getPm_Freq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getFreq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_Freq_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				model.addAttribute("lpcdBoxValue", (int)lpcdBoxValue);
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
				model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));

				LinkedList<String> bottom10ulb = new LinkedList<String>();
				LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
				
				LinkedList<String> topulb = new LinkedList<String>();
				LinkedList<Double> toplpcd = new LinkedList<Double>();
				model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
					
				LinkedList<String> bottomulb = new LinkedList<String>();
				LinkedList<Double> bottomlpcd = new LinkedList<Double>();
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
					
				model.addAttribute("flag", false);

				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_Freq_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_Freq_supply());
									
					}
				}
				
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
			}
			
			model.addAttribute("datesBetween", new ObjectMapper().writeValueAsString(datesBetween));
			model.addAttribute("currentLPCD", new ObjectMapper().writeValueAsString(currentLPCD));
			model.addAttribute("previousLPCD", new ObjectMapper().writeValueAsString(previousLPCD));
			
			/*for (Map.Entry<String, DailyWaterSupplyModel> entry : lpcdMap.entrySet()) {
				
			    System.out.println("ULB Name -- >> "+entry.getKey() + "Current LPCD --- >> " + entry.getValue().getRate_of_supply() + "Previous LPCD --- >> " + entry.getValue().getPm_rate_of_supply());
			}*/
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		if (logger.isInfoEnabled()) {
			logger.info("AverageHourOfWaterSupplyPage -- END");
		}

		return "avg-hr-supply";

	}
	
	
	@RequestMapping(value = "/ajax-avg-hr-supply", method = RequestMethod.GET)
	public @ResponseBody AjaxAverageHourOfWaterSupplyModel ajaxAverageHourOfWaterSupply(
			@ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxAverageHourOfWaterSupply -- START");
		}
		
		
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		AjaxAverageHourOfWaterSupplyModel ajaxAverageHourOfWaterSupply = new AjaxAverageHourOfWaterSupplyModel();
		
		try {
			
			
			dashboardValidation.dashboardValidation(listingModel, amrutTypeId, selectedYear, month);
			
		String startDate = null;
		String endDate = null;
		String days = null;
		String year = null;
		String startDate1 = null;
		String endDate1 = null;
		String startDateMap = null;
		String endDateMap = null;
		String currentMonth = "";
		String previousMonth = "";
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxAverageHourOfWaterSupply.setPhDivisionModels(phDivisionModels);
					
					listingModel.setDistrictId(null);
					listingModel.setDivisionId(null);
					listingModel.setUlbId(null);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if (month != null) {
			
			List<String> dates = null;
			try {
				dates = dateCalculation.findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
				startDateMap = dates.get(0);
				endDateMap = dates.get(1);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(0);
			endDate = dates.get(1);
			days = dates.get(2);
			currentMonth = dates.get(3);
			previousMonth = dates.get(4);
			startDate1 = startDate;
			endDate1 = endDate;
			
		} else {
			
			startDate = listingModel.getDate();
			
			if(startDate != null) {
				
				month = startDate.substring(5, 7);
				year = startDate.substring(0, 4);
				
				if (month != null) {
					
					List<String> dates = null;
					try {
						dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
						startDate1 = dates.get(0);
						endDate1 = startDate;
						startDateMap = dates.get(0);
						endDateMap = dates.get(1);
						currentMonth = dates.get(3);
						previousMonth = dates.get(4);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					} 
					
				}
				
			}
		
		}
		
		LinkedHashMap<String, Double> lpcdTotal = null;
		LinkedHashMap<String, DailyWaterSupplyModel> lpcdMap = null;
		List<String> datesBetween = new LinkedList<String>();
		List<Double> currentLPCD = new LinkedList<Double>();
		List<Double> previousLPCD = new LinkedList<Double>();
		double lpcdBoxValue = 0.0D;
		
		try {
			
			if(flag == 1) {
				
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
				
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
					
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				lpcdBoxValue = (int)(Math.floor(lpcdBoxValue + 0.5));
				lpcdTotal = new LinkedHashMap<String, Double>();

				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getFreq_supply());
							}
					}
				
				} else {
				
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
					}
				}
			}
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getFreq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getPm_Freq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getPm_Freq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getFreq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_Freq_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}
				
				ajaxAverageHourOfWaterSupply.setLpcdBoxValue(lpcdBoxValue);
				ajaxAverageHourOfWaterSupply.setLpcdMap(lpcdMap);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
								
				if((amrutTypeId == null) && (listingModel.getDivisionId() == null) && (listingModel.getDistrictId() == null) && (listingModel.getUlbId() == null)) {
					
					
					/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					int n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
					
					ajaxAverageHourOfWaterSupply.setTopValue(n);
					
					HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
					
					Set mapSet = (Set) top10LpcdUlbs.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd.add(value);
			        }
			        
			        ajaxAverageHourOfWaterSupply.setTop10ulb(top10ulb);
			        ajaxAverageHourOfWaterSupply.setTop10lpcd(top10lpcd);
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
				    
				    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd.add(value);
			        }
			        
			        ajaxAverageHourOfWaterSupply.setBottom10ulb(bottom10ulb);
			        ajaxAverageHourOfWaterSupply.setBottom10lpcd(bottom10lpcd);
			        ajaxAverageHourOfWaterSupply.setFlag(false);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
					
				} else {
					
					ajaxAverageHourOfWaterSupply.setFlag(true);
				}
					
					/* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
					
					LinkedList<String> topulb = new LinkedList<String>();
			        LinkedList<Double> toplpcd = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= lpcdBoxValue) {
			        		
			        		topulb.add(entry1.getKey());
			        		toplpcd.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb.add(entry1.getKey());
			        		bottomlpcd.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxAverageHourOfWaterSupply.setTopulb(topulb);
			        ajaxAverageHourOfWaterSupply.setToplpcd(toplpcd);
			        ajaxAverageHourOfWaterSupply.setBottomulb(bottomulb);
			        ajaxAverageHourOfWaterSupply.setBottomlpcd(bottomlpcd);
			        
					
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_Freq_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_Freq_supply());
									
					}
				}
					
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
			} else if(flag == 2) {
				
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				lpcdBoxValue = (int)(Math.floor(lpcdBoxValue + 0.5));
				lpcdTotal = new LinkedHashMap<String, Double>();

				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getFreq_supply());
							}
					}
				
				} else {
				
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
					}
				}
			}
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getFreq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getPm_Freq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getPm_Freq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getFreq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_Freq_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				ajaxAverageHourOfWaterSupply.setLpcdBoxValue(lpcdBoxValue);
				ajaxAverageHourOfWaterSupply.setLpcdMap(lpcdMap);
								
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= lpcdBoxValue) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        ajaxAverageHourOfWaterSupply.setTopulb(topulb);
		        ajaxAverageHourOfWaterSupply.setToplpcd(toplpcd);
		        ajaxAverageHourOfWaterSupply.setBottomulb(bottomulb);
		        ajaxAverageHourOfWaterSupply.setBottomlpcd(bottomlpcd);
		        ajaxAverageHourOfWaterSupply.setFlag(true);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_Freq_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_Freq_supply());
									
					}
				}
				
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
			} else if(flag == 3) {
				
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				lpcdBoxValue = (int)(Math.floor(lpcdBoxValue + 0.5));
				lpcdTotal = new LinkedHashMap<String, Double>();

				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getFreq_supply());
							}
					}
				
				} else {
				
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
					}
				}
			}
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getFreq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getPm_Freq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getPm_Freq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getFreq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_Freq_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				ajaxAverageHourOfWaterSupply.setLpcdBoxValue(lpcdBoxValue);
				ajaxAverageHourOfWaterSupply.setLpcdMap(lpcdMap);
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= lpcdBoxValue) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        ajaxAverageHourOfWaterSupply.setTopulb(topulb);
		        ajaxAverageHourOfWaterSupply.setToplpcd(toplpcd);
		        ajaxAverageHourOfWaterSupply.setBottomulb(bottomulb);
		        ajaxAverageHourOfWaterSupply.setBottomlpcd(bottomlpcd);
		        ajaxAverageHourOfWaterSupply.setFlag(true);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
			     
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_Freq_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_Freq_supply());
									
					}
				}
				
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
				
			} else if(flag == 4) {
				
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
				
				listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				lpcdBoxValue = (int)(Math.floor(lpcdBoxValue + 0.5));
				lpcdTotal = new LinkedHashMap<String, Double>();

				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getFreq_supply());
							}
					}
				
				} else {
				
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
					}
				}
			}
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getFreq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getPm_Freq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setFreq_supply(dailyWaterSupplyModel1.getPm_Freq_supply());
								dailyWaterSupplyModel3.setPm_Freq_supply(dailyWaterSupplyModel2.getFreq_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_Freq_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				ajaxAverageHourOfWaterSupply.setLpcdBoxValue(lpcdBoxValue);
				ajaxAverageHourOfWaterSupply.setLpcdMap(lpcdMap);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();

				LinkedList<String> bottom10ulb = new LinkedList<String>();
				LinkedList<Double> bottom10lpcd = new LinkedList<Double>();

				LinkedList<String> middleulb1 = new LinkedList<String>();
				LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
				LinkedList<String> middleulb2 = new LinkedList<String>();
				LinkedList<Double> middlelpcd2 = new LinkedList<Double>();

				LinkedList<String> topulb = new LinkedList<String>();
				LinkedList<Double> toplpcd = new LinkedList<Double>();
				
				LinkedList<String> bottomulb = new LinkedList<String>();
				LinkedList<Double> bottomlpcd = new LinkedList<Double>();
				
				ajaxAverageHourOfWaterSupply.setTopulb(topulb);
		        ajaxAverageHourOfWaterSupply.setToplpcd(toplpcd);
		        
		        ajaxAverageHourOfWaterSupply.setMiddleulb1(middleulb1);
		        ajaxAverageHourOfWaterSupply.setMiddlelpcd1(middlelpcd1);
				
		        ajaxAverageHourOfWaterSupply.setMiddleulb2(middleulb2);
		        ajaxAverageHourOfWaterSupply.setMiddlelpcd2(middlelpcd2);
		        
		        ajaxAverageHourOfWaterSupply.setBottomulb(bottomulb);
		        ajaxAverageHourOfWaterSupply.setBottomlpcd(bottomlpcd);
		        
		        ajaxAverageHourOfWaterSupply.setTop10ulb(top10ulb);
		        ajaxAverageHourOfWaterSupply.setTop10lpcd(top10lpcd);
		        
		        ajaxAverageHourOfWaterSupply.setBottom10ulb(bottom10ulb);
		        ajaxAverageHourOfWaterSupply.setBottom10lpcd(bottom10lpcd);
		        
		        ajaxAverageHourOfWaterSupply.setFlag(false);
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_Freq_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_Freq_supply());
									
					}
				}
				
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
			}
			
			ajaxAverageHourOfWaterSupply.setDatesBetween(datesBetween);
			ajaxAverageHourOfWaterSupply.setCurrentLPCD(currentLPCD);
			ajaxAverageHourOfWaterSupply.setPreviousLPCD(previousLPCD);
			
			/*for (Map.Entry<String, DailyWaterSupplyModel> entry : lpcdMap.entrySet()) {
				
			    System.out.println("ULB Name -- >> "+entry.getKey() + "Current LPCD --- >> " + entry.getValue().getRate_of_supply() + "Previous LPCD --- >> " + entry.getValue().getPm_rate_of_supply());
			}*/
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		if (logger.isInfoEnabled()) {
			logger.info("ajaxAverageHourOfWaterSupply -- END");
		}

		return ajaxAverageHourOfWaterSupply;

	}
	
}
