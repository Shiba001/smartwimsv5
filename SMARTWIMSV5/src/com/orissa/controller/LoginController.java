package com.orissa.controller;

import java.sql.SQLException;
import java.util.Objects;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.MessageUtil;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.LoginUsersModel;
import com.orissa.service.HomeService;
import com.orissa.service.LoginService;

@Controller
public class LoginController {

	private static Logger logger = Logger.getLogger(LoginController.class);

	@Autowired
	private LoginService loginService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private HomeService homeService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginCheck(Model model, LoginUsersModel loginUsersModel,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request) {

		if (logger.isInfoEnabled()) {
			logger.info("loginCheck -- START");
		}
		
		HttpSession httpSession = request.getSession(false);

		try {
			if (null == httpSession.getAttribute("loginUsersModel2")) {
				
				// / If not logged in
				LoginUsersModel loginUsersModel2 = loginService
						.loginCheck(loginUsersModel, request);
				
				httpSession.invalidate();

				if (loginUsersModel2.getUser_id() != 0) {

					System.out.println("Login Successful");
					
					model.addAttribute("loginUsersModel2", loginUsersModel2);

					if(!Objects.isNull(loginUsersModel2)){
						final HttpSession httpSession1 = request.getSession(true);
						httpSession1.setAttribute("loginUsersModel2", loginUsersModel2);
					}
					

				} else {
					System.out.println("Login Failed");
					
					redirectAttributes.addFlashAttribute("Message",
							messageUtil.getBundle("login.failed"));
					return "redirect:/swims-login";
				}
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
				break;
			}

			logger.debug(e1.getMessage(), e1);

			redirectAttributes.addFlashAttribute("loginUsersModel",loginUsersModel);
			return "redirect:/swims-login";

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "page-500";

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "page-500";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("loginCheck -- END");
		}

		return "redirect:/dashboard";

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {

		if (logger.isDebugEnabled()) {
			logger.debug("logout - Start");
		}

		session.invalidate();

		if (logger.isDebugEnabled()) {
			logger.debug("logout - End");
		}

		return "redirect:/index";
	}
}
