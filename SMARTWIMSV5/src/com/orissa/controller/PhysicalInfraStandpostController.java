package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraStandpostsModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraStandpostService;

@Controller
public class PhysicalInfraStandpostController implements SaltTracker {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraStandpostController.class);

	@Autowired
	private PhysicalInfraStandpostService physicalInfraStandpostService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private DistrictService districtService;

	@Autowired
	private Encryption encryption;

	@RequestMapping(value = "/add-physical-infra-standpost", method = RequestMethod.GET)
	public String addPhysicalInfraStandPostPage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraStandPostPage -- START");
		}
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraStandPostPage -- END");
		}

		return "add-physical-infra-standpost";

	}

	@RequestMapping(value = "/add-physical-infra-standpost", method = RequestMethod.POST)
	public String addPhysicalInfraStandPost(Model model,
			@ModelAttribute PhysicalInfraStandpostsModel physicalInfraStandpostsModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraStandposts -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraStandpostsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			int result = physicalInfraStandpostService
					.addPhysicalInfraStandpost(physicalInfraStandpostsModel,
							httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

			redirectAttributes.addFlashAttribute(
					"physicalInfraStandpostsModel",
					physicalInfraStandpostsModel);
			return ("redirect:/add-physical-infra-standpost");

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraStandpost -- END");
		}

		return "redirect:/list-physical-infra-standpost";

	}

	@RequestMapping(value = "/list-physical-infra-standpost", method = RequestMethod.GET)
	public String fetchPhysicalInfraStandposts(HttpServletRequest httpServletRequest, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupply -- START");
		}

		try {
			LinkedList<PhysicalInfraStandpostsModel> physicalInfraStandpostsModels = physicalInfraStandpostService
					.fetchPhysicalInfraStandposts(httpServletRequest);

			if (physicalInfraStandpostsModels.size() > 0) {

				model.addAttribute("physicalInfraStandpostsModels",
						physicalInfraStandpostsModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStandposts -- END");
		}

		return "list-physical-infra-standpost";

	}

	@RequestMapping(value = "/edit-physical-infra-standpost", method = RequestMethod.GET)
	public String editfetchPhysicalInfraStandpostsPage(
			@RequestParam String phyInfra_std_post_id, Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraStandposts -- START");
		}

		if (StringUtils.isBlank(phyInfra_std_post_id)) {
			return "redirect:/error404";
		}


		PhysicalInfraStandpostsModel physicalInfraStandpostsModel = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		String phyInfra_std_post_id1 = null;
		
		try{
			String key = messageUtil.getBundle("secret.key"); 
			
			phyInfra_std_post_id1 = encryption.decode(key, id);
			if(phyInfra_std_post_id.length() > id.length())
				throw new Exception();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		

		try {
			physicalInfraStandpostsModel = physicalInfraStandpostService
					.fetchPhysicalInfraStandpostById(Integer
							.parseInt(phyInfra_std_post_id1));

			if (physicalInfraStandpostsModel.getPhyInfra_std_post_id() != 0) {
				model.addAttribute("physicalInfraStandpostsModel",
						physicalInfraStandpostsModel);
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return "redirect:/list-physical-infra-standpost";
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(physicalInfraStandpostsModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraStandposts -- END");
		}

		return "edit-physical-infra-standpost";

	}

	@RequestMapping(value = "/delete-physical-infra-standpost", method = RequestMethod.POST)
	public String deletePhysicalInfraStandposts(
			final RedirectAttributes redirectAttributes, Model model, HttpServletRequest httpServletRequest, PhysicalInfraStandpostsModel physicalInfraStandpostsModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraStandposts -- START");
		}

		String phyInfra_std_post_id = physicalInfraStandpostsModel.getPhyInfraStdPostId();
		
		if (StringUtils.isBlank(phyInfra_std_post_id)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraStandpostsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		String phyInfra_std_post_id1 = null;
		
		try{
			String key = messageUtil.getBundle("secret.key"); 
			
			phyInfra_std_post_id1 = encryption.decode(key, phyInfra_std_post_id);
			/*if(phyInfra_std_post_id.length() > id.length())
				throw new Exception();*/
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		
		try {
			int result = physicalInfraStandpostService
					.deletePhysicalInfraStandpostById(Integer
							.parseInt(phyInfra_std_post_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else {
			
			throw new ServletException(messageUtil.getBundle("CSRF"));

		}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraStandposts -- END");
		}

		return "redirect:/list-physical-infra-standpost";

	}

	@RequestMapping(value = "/update-physical-infra-standpost", method = RequestMethod.POST)
	public String updatePhysicalInfraStandposts(Model model,
			@ModelAttribute PhysicalInfraStandpostsModel physicalInfraStandpostsModel,BindingResult bindingResult,
			HttpServletRequest httpServletRequest,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraStandposts -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraStandpostsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			String phyInfraStdPostId = encryption.decode(key, physicalInfraStandpostsModel.getPhyInfraStdPostId());
			physicalInfraStandpostsModel.setPhyInfra_std_post_id(Integer.parseInt(phyInfraStdPostId));
			int result = physicalInfraStandpostService
					.updatePhysicalInfraStandpost(physicalInfraStandpostsModel,
							httpServletRequest);

			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute(
					"physicalInfraStandpostsModel",
					physicalInfraStandpostsModel);
			return ("redirect:/edit-physical-infra-standpost?phyInfra_std_post_id=" + physicalInfraStandpostsModel.getPhyInfraStdPostId());

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraStandposts -- END");
		}

		return "redirect:/list-physical-infra-standpost";

	}
	
	@RequestMapping(value = "/fetch-ajax-physical-infra-standpost", method = RequestMethod.GET)
	public String fetchAJAXPhysicalInfraStandpost(HttpServletRequest httpServletRequest, @ModelAttribute ListingModel listingModel, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStandpost -- START");
		}
		LinkedList<PhysicalInfraStandpostsModel> physicalInfraStandpostsModels = null;

		try {
			physicalInfraStandpostsModels = physicalInfraStandpostService.fetchAJAXPhysicalInfraStandpost(listingModel);
			model.addAttribute("physicalInfraStandpostsModels", physicalInfraStandpostsModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStandpost -- END");
		}

		return "ajax-physical-infra-standpost";

	}
}
