package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraWaterTreatmentPlantModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraTreatmentPlantService;

@Controller
public class PhysicalInfraTreatmentPlantController implements SaltTracker {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraTreatmentPlantController.class);

	@Autowired
	private PhysicalInfraTreatmentPlantService physicalInfraTreatmentPlantService;
	
	@Autowired
	private MessageUtil messageUtil;
	@Autowired
	private DivisionService divisionService;

	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private Encryption encryption;

	@RequestMapping(value = "/add-physical-infra-treatment-plant", method = RequestMethod.GET)
	public String addPhysicalInfraTreatmentPlantPage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTreatmentPlantPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTreatmentPlantPage -- END");
		}

		return "add-physical-infra-treatment-plant";

	}

	@RequestMapping(value = "/add-physical-infra-treatment-plant", method = RequestMethod.POST)
	public String addPhysicalInfraTreatmentPlant(
			Model model,
			@ModelAttribute PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel,BindingResult bindingResult, final RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTreatmentPlant -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraWaterTreatmentPlantModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			int result = physicalInfraTreatmentPlantService
					.addPhysicalInfraTreatmentPlant(physicalInfraWaterTreatmentPlantModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		}  catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute("physicalInfraWaterTreatmentPlantModel", physicalInfraWaterTreatmentPlantModel);
			return ("redirect:/add-physical-infra-treatment-plant");
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTreatmentPlant -- END");
		}

		return ("redirect:/list-physical-infra-treatment-plant");
	}

	@RequestMapping(value = "/list-physical-infra-treatment-plant", method = RequestMethod.GET)
	public String fetchPhysicalInfraTreatmentPlant(HttpServletRequest httpServletRequest, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTreatmentPlant -- START");
		}

		try {
			LinkedList<PhysicalInfraWaterTreatmentPlantModel> physicalInfraWaterTreatmentPlantModels = physicalInfraTreatmentPlantService
					.fetchPhysicalInfraWaterTreatmentPlant(httpServletRequest);

			if (physicalInfraWaterTreatmentPlantModels.size() > 0) {

				model.addAttribute("physicalInfraWaterTreatmentPlantModels",
						physicalInfraWaterTreatmentPlantModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTreatmentPlant -- END");
		}

		return "list-physical-infra-treatment-plant";

	}

	@RequestMapping(value = "/edit-physical-infra-treatment-plant", method = RequestMethod.GET)
	public String editPhysicalInfraTreatmentPlantPage(
			@RequestParam String wtpid, Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraTreatmentPlantPage -- START");
		}

		if (StringUtils.isBlank(wtpid)) {
			return "redirect:/error404";
		}

		PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		String wtpid1 = null;
		try{
			String key = messageUtil.getBundle("secret.key"); 
			
			wtpid1 = encryption.decode(key, id);
			if(wtpid.length() > id.length())
				throw new Exception();
		} catch (Exception e) {			
			e.printStackTrace();
			return "redirect:/error404";
		}


		try {
			physicalInfraWaterTreatmentPlantModel = physicalInfraTreatmentPlantService
					.fetchPhysicalInfraWaterTreatmentPlantById(Integer
							.parseInt(wtpid1));

			if (physicalInfraWaterTreatmentPlantModel.getWtpid() != 0) {
				model.addAttribute("physicalInfraWaterTreatmentPlantModel",
						physicalInfraWaterTreatmentPlantModel);
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return "redirect:/list-physical-infra-treatment-plant";
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(physicalInfraWaterTreatmentPlantModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraTreatmentPlantPage -- END");
		}

		return "edit-physical-infra-treatment-plant";

	}
	
	
	@RequestMapping(value = "/delete-physical-infra-treatment-plant", method = RequestMethod.POST)
	public String deletePhysicalInfraTreatmentPlantPage(
			Model model, final RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest, PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraTreatmentPlantPage -- START");
		}

		String wtpid = physicalInfraWaterTreatmentPlantModel.getWtpipeId();
				
		if (StringUtils.isBlank(wtpid)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraWaterTreatmentPlantModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		String wtpid1 = null;
		try{
			String key = messageUtil.getBundle("secret.key"); 
			
			wtpid1 = encryption.decode(key, wtpid);
		} catch (Exception e) {			
			e.printStackTrace();
			return "redirect:/error404";
		}
		
		try {
			int result = physicalInfraTreatmentPlantService
					.deletePhysicalInfraWaterTreatmentPlantById(Integer
							.parseInt(wtpid1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else {
			
			throw new ServletException(messageUtil.getBundle("CSRF"));

		}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraTreatmentPlantPage -- END");
		}

		return "redirect:/list-physical-infra-treatment-plant";

	}

	@RequestMapping(value = "/update-physical-infra-treatment-plant", method = RequestMethod.POST)
	public String updatePhysicalInfraTreatmentPlant(
			Model model,
			@ModelAttribute PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel,BindingResult bindingResult, final RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraTreatmentPlant -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraWaterTreatmentPlantModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			String pipeId = encryption.decode(key, physicalInfraWaterTreatmentPlantModel.getWtpipeId());
			physicalInfraWaterTreatmentPlantModel.setWtpid(Integer.parseInt(pipeId));
			int result = physicalInfraTreatmentPlantService
					.updatePhysicalInfraWaterTreatmentPlant(physicalInfraWaterTreatmentPlantModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		}  catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute("physicalInfraWaterTreatmentPlantModel", physicalInfraWaterTreatmentPlantModel);
			return ("redirect:/edit-physical-infra-treatment-plant?wtpid="+physicalInfraWaterTreatmentPlantModel.getWtpipeId());

		}catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraTreatmentPlant -- END");
		}

		return "redirect:/list-physical-infra-treatment-plant";

	}
	
	
	@RequestMapping(value = "/fetch-ajax-physical-infra-treatment-plant", method = RequestMethod.GET)
	public String fetchAJAXPhysicalInfraTreatmentPlant(HttpServletRequest httpServletRequest, @ModelAttribute ListingModel listingModel, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraTreatmentPlant -- START");
		}
		LinkedList<PhysicalInfraWaterTreatmentPlantModel> physicalInfraWaterTreatmentPlantModel = null;

		try {
			physicalInfraWaterTreatmentPlantModel = physicalInfraTreatmentPlantService.fetchAJAXPhysicalInfraTreat(listingModel);
			
			model.addAttribute("physicalInfraWaterTreatmentPlantModels", physicalInfraWaterTreatmentPlantModel);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraTreatmentPlant -- END");
		}

		return "ajax-physical-infra-treatment-plant";

	}
}
