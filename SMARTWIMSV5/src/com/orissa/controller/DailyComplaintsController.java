package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.IpModel;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.service.DailyComplaintsService;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;

@Controller
public class DailyComplaintsController implements SaltTracker {

	private static Logger logger = Logger
			.getLogger(DailyComplaintsController.class);

	@Autowired
	private DailyComplaintsService dailyComplaintsService;

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private DistrictService districtService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private Encryption encryption;

	@RequestMapping(value = "/add-daily-complaint", method = RequestMethod.GET)
	public String addDailyComplaintsPage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyComplaintsPage -- START");
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyComplaintsPage -- END");
		}

		return "add-daily-complaint";

	}

	@RequestMapping(value = "/add-daily-complaint", method = RequestMethod.POST)
	public String addDailyComplaints(Model model,
			@ModelAttribute DailyComplaintsModel dailyComplaintsModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterSupply -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }

		String countryJson = "";
		try {
			RestTemplate restTemplate = new RestTemplate();
			countryJson = restTemplate.getForObject("http://gimmeproxy.com/api/getProxy", String.class);
			System.out.println("countryJson = > "+countryJson);
		  } catch (Exception e) {
			  e.printStackTrace();
			  
		  }
		
		Gson gson = new Gson();
		IpModel ipModel = null;
		try {
				ipModel = gson.fromJson(countryJson, IpModel.class);
				
			} catch (Exception e) {
			  e.printStackTrace();
			  return "redirect:/error";
			}
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyComplaintsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
				

				try {
					int result = dailyComplaintsService.addDailyComplaints(
							dailyComplaintsModel, httpServletRequest);

					if (result == 1) {

						System.out.println("Inserted Successfully");
						redirectAttributes.addFlashAttribute("Success",
								messageUtil.getBundle("insert.done"));
					} else {
						System.out.println("Insertion Failed");
						redirectAttributes.addFlashAttribute("Failure",
								messageUtil.getBundle("insert.failed"));
					}

				} catch (FormExceptions e1) {

					for (Entry<String, Exception> entry : ((FormExceptions) e1)
							.getExceptions().entrySet()) {
						System.out.println("key :: " + entry.getKey()
								+ " value :: " + entry.getValue().getMessage());
						redirectAttributes.addFlashAttribute(entry.getKey(),
								entry.getValue().getMessage());
					}

					logger.debug(e1.getMessage(), e1);

					redirectAttributes.addFlashAttribute(
							"dailyComplaintsModel", dailyComplaintsModel);
					return ("redirect:/add-daily-complaint");

				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyComplaints -- END");
		}

		return ("redirect:/list-daily-complaint");

	}

	@RequestMapping(value = "/list-daily-complaint", method = RequestMethod.GET)
	public String fetchDailyWaterSupply(HttpServletRequest httpServletRequest,
			Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupply -- START");
		}

		try {
			LinkedList<DailyComplaintsModel> dailyComplaintsModels = dailyComplaintsService
					.fetchDailyComplaints(httpServletRequest);

			if (dailyComplaintsModels.size() > 0) {

				model.addAttribute("dailyComplaintsModels",
						dailyComplaintsModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaints -- END");
		}

		return "list-daily-complaint";

	}

	@RequestMapping(value = "/edit-daily-complaint", method = RequestMethod.GET)
	public String editDailyComplaintsPage(@RequestParam String tw_id,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editDailyComplaintsPage -- START");
		}

		if (StringUtils.isBlank(tw_id)) {
			return "redirect:/error404";
		}

		DailyComplaintsModel dailyComplaintsModel = null;

		String tw_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1] + "==";

		try {

			String key = messageUtil.getBundle("secret.key");

			tw_id1 = encryption.decode(key, id);
			if (tw_id.length() > id.length())
				throw new Exception();

		} catch (Exception e) {

			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			dailyComplaintsModel = dailyComplaintsService
					.fetchDailyComplaintsById(Integer.parseInt(tw_id1));

			if (dailyComplaintsModel.getTw_id() != 0) {
				model.addAttribute("dailyComplaintsModel", dailyComplaintsModel);
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return ("redirect:/list-daily-complaint");
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(dailyComplaintsModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editDailyComplaintsPage -- END");
		}

		return "edit-daily-complaint";

	}

	@RequestMapping(value = "/delete-daily-complaint", method = RequestMethod.POST)
	public String deleteDailyComplaintsPage(final RedirectAttributes redirectAttributes, Model model,
			HttpServletRequest httpServletRequest,DailyComplaintsModel dailyComplaintsModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyComplaintsPage -- START");
		}
		String tw_id = dailyComplaintsModel.getTwId();
		 
		if (StringUtils.isBlank(tw_id)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyComplaintsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		String tw_id1 = null;
		try {

			String key = messageUtil.getBundle("secret.key");

			tw_id1 = encryption.decode(key, tw_id);

		} catch (Exception e) {

			e.printStackTrace();
			return "redirect:/error404";
		}
		try {
			int result = dailyComplaintsService
					.deleteDailyComplaintsById(Integer.parseInt(tw_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyComplaintsPage -- END");
		}

		return "redirect:/list-daily-complaint";

	}

	@RequestMapping(value = "/isApproved-daily-complaint", method = RequestMethod.GET)
	public @ResponseBody String isApprovedUpdate(@RequestParam String tw_id,
			final RedirectAttributes redirectAttributes, Model model,
			HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}
		System.out.println(tw_id);
		if (StringUtils.isBlank(tw_id)) {
			return "redirect:/error404";
		}
		String tw_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1] + "==";

		try {

			String key = messageUtil.getBundle("secret.key");

			tw_id1 = encryption.decode(key, id);

		} catch (Exception e) {

			e.printStackTrace();
			return "redirect:/error404";
		}
		try {
			int result = dailyComplaintsService.isApprovedUpdate(Integer
					.parseInt(tw_id1));

			if (result == 1) {

				System.out.println("Approved");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("Approved.done"));
			} else {
				System.out.println("Approved Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("Approved.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return "success";

	}

	@RequestMapping(value = "/isNotApproved-daily-complaint", method = RequestMethod.GET)
	public @ResponseBody String isNotApprovedUpdate(@RequestParam String tw_id,
			final RedirectAttributes redirectAttributes, Model model,
			HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}
		System.out.println(tw_id);
		if (StringUtils.isBlank(tw_id)) {
			return "redirect:/error404";
		}
		String tw_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1] + "==";

		try {

			String key = messageUtil.getBundle("secret.key");

			tw_id1 = encryption.decode(key, id);

		} catch (Exception e) {

			e.printStackTrace();
			return "redirect:/error404";
		}
		try {
			int result = dailyComplaintsService.isNotApprovedUpdate(Integer
					.parseInt(tw_id1));

			if (result == 1) {

				System.out.println("Pending");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("Pending.done"));
			} else {
				System.out.println("Pending Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("Pending.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return "success";

	}

	@RequestMapping(value = "/update-daily-complaint", method = RequestMethod.POST)
	public String updateDailyComplaints(Model model,
			@ModelAttribute DailyComplaintsModel dailyComplaintsModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyComplaints -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyComplaintsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {

		String key = messageUtil.getBundle("secret.key");
		try {
			String twId = encryption
					.decode(key, dailyComplaintsModel.getTwId());
			dailyComplaintsModel.setTw_id(Integer.parseInt(twId));
			int result = dailyComplaintsService.updateDailyComplaints(
					dailyComplaintsModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

			redirectAttributes.addFlashAttribute("dailyComplaintsModel",
					dailyComplaintsModel);

			return ("redirect:/edit-daily-complaint?tw_id=" + dailyComplaintsModel
					.getTwId());

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
			} catch (Exception e) {
				redirectAttributes.addFlashAttribute("error", e.getMessage());
			} finally {
				activeSalts.remove(salt);
			}
		
		if (logger.isInfoEnabled()) {
			logger.info("updateDailyComplaints -- END");
		}

		return "redirect:/list-daily-complaint";

	}

	@RequestMapping(value = "/fetch-ajax-daily-complaint", method = RequestMethod.GET)
	public String fetchAJAXDailyWaterSupply(
			HttpServletRequest httpServletRequest, Model model,
			@ModelAttribute ListingModel listingModel) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- START");
		}

		try {
			LinkedList<DailyComplaintsModel> dailyComplaintsModels = dailyComplaintsService
					.fetchAJAXDailyComplaints(listingModel);

			if (dailyComplaintsModels.size() > 0) {

				model.addAttribute("dailyWaterSupplyModels",
						dailyComplaintsModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- END");
		}

		return "ajax-daily-complaint";

	}
}
