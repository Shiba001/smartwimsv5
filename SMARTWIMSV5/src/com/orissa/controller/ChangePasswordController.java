package com.orissa.controller;

import java.sql.SQLException;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.MessageUtil;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.LoginUsersModel;
import com.orissa.service.ChangePasswordService;

@Controller
public class ChangePasswordController {

	private static Logger logger = Logger
			.getLogger(ChangePasswordController.class);

	@Autowired
	private ChangePasswordService changePasswordService;

	@Autowired
	private MessageUtil messageUtil;

	@RequestMapping(value = "/change-password", method = RequestMethod.GET)
	public String changePasswordPage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("changePasswordPage -- START");
		}

		if (logger.isInfoEnabled()) {
			logger.info("changePasswordPage -- END");
		}

		return "change-password";
	}

	@RequestMapping(value = "/change-password", method = RequestMethod.POST)
	public String changePassword(final RedirectAttributes redirectAttributes,
			HttpServletRequest httpServletRequest,
			@RequestParam String oldPassword, @RequestParam String newPassword, @RequestParam String confirmNewPassword) {

		if (logger.isInfoEnabled()) {
			logger.info("changePassword -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		try {
			int result = changePasswordService.checkOldPassword(oldPassword,
					newPassword, loginUsersModel.getUser_id(),confirmNewPassword);

			if (result == 1) {

				redirectAttributes.addFlashAttribute("password.changed",
						messageUtil.getBundle("password.changed"));
			} else {

				redirectAttributes.addFlashAttribute("password.not.changed",
						messageUtil.getBundle("password.not.changed"));

				return ("redirect:/change-password");
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

			redirectAttributes.addFlashAttribute("oldPassword", oldPassword);

			redirectAttributes.addFlashAttribute("newPassword", newPassword);

			return ("redirect:/change-password");

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("changePassword -- END");
		}

		httpSession.invalidate();

		return ("redirect:/index");
	}
}
