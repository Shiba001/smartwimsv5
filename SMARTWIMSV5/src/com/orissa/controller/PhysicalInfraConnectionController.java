package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraConnectionModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraConnectionService;

@Controller
public class PhysicalInfraConnectionController implements SaltTracker {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraConnectionController.class);

	@Autowired
	private PhysicalInfraConnectionService physicalInfraConnectionService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private DistrictService districtService;

	@Autowired
	private DivisionService divisionService;
	
	@Autowired
	private Encryption encryption;

	@RequestMapping(value = "/add-physical-infra-connection", method = RequestMethod.GET)
	public String addPhysicalInfraConnectionPage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraConnection -- START");
		}
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraConnection -- END");
		}

		return "add-physical-infra-connection";

	}

	@RequestMapping(value = "/add-physical-infra-connection", method = RequestMethod.POST)
	public String addPhysicalInfraConnection(Model model,
			HttpServletRequest httpServletRequest,
			@ModelAttribute PhysicalInfraConnectionModel physicalInfraConnectionModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraConnection -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraConnectionModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
				
			
		try {
			int result = physicalInfraConnectionService
					.addPhysicalInfraConnection(physicalInfraConnectionModel,
							httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute(
					"physicalInfraConnectionModel",
					physicalInfraConnectionModel);
			return ("redirect:/add-physical-infra-connection");

			// return ("redirect:/add-daily-complaints");
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraConnection -- END");
		}

		return "redirect:/list-physical-infra-connection";

	}

	@RequestMapping(value = "/list-physical-infra-connection", method = RequestMethod.GET)
	public String fetchPhysicalInfraConnection(HttpServletRequest httpServletRequest,Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraConnection -- START");
		}

		try {
			LinkedList<PhysicalInfraConnectionModel> physicalInfraConnectionModels = physicalInfraConnectionService
					.fetchPhysicalInfraConnection(httpServletRequest);

			if (physicalInfraConnectionModels.size() > 0) {

				model.addAttribute("physicalInfraConnectionModels",
						physicalInfraConnectionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraConnection -- END");
		}

		return "list-physical-infra-connection";

	}

	@RequestMapping(value = "/edit-physical-infra-connection", method = RequestMethod.GET)
	public String editPhysicalInfraConnectionPage(
			@RequestParam String phyInfra_con_id, Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraConnection -- START");
		}

		if (StringUtils.isBlank(phyInfra_con_id)) {
			return "redirect:/error404";
		}

		PhysicalInfraConnectionModel physicalInfraConnectionModel = null;
		String phyInfra_con_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		
	try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			phyInfra_con_id1 = encryption.decode(key, id);
			if(phyInfra_con_id.length() > id.length())
				throw new Exception();
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			physicalInfraConnectionModel = physicalInfraConnectionService
					.fetchPhysicalInfraConnectionById(Integer
							.parseInt(phyInfra_con_id1));

			if (physicalInfraConnectionModel.getPhyInfra_con_id() != 0) {
				model.addAttribute("physicalInfraConnectionModel",
						physicalInfraConnectionModel);
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return "redirect:/list-physical-infra-connection";
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(physicalInfraConnectionModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraConnectionPage -- END");
		}

		return "edit-physical-infra-connection";

	}

	@RequestMapping(value = "/delete-physical-infra-connection", method = RequestMethod.POST)
	public String deletePhysicalInfraConnectionPage(final RedirectAttributes redirectAttributes, Model model, HttpServletRequest httpServletRequest, PhysicalInfraConnectionModel physicalInfraConnectionModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraConnectionPage -- START");
		}

		String phyInfra_con_id = physicalInfraConnectionModel.getPhyInfraConId();
				
		if (StringUtils.isBlank(phyInfra_con_id)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraConnectionModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		String phyInfra_con_id1 = null;
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		
	try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			phyInfra_con_id1 = encryption.decode(key, phyInfra_con_id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		

		try {
			int result = physicalInfraConnectionService
					.deletePhysicalInfraConnectionById(Integer
							.parseInt(phyInfra_con_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraConnectionPage -- END");
		}

		return "redirect:/list-physical-infra-connection";

	}

	@RequestMapping(value = "/update-physical-infra-connection", method = RequestMethod.POST)
	public String updatePhysicalInfraConnection(Model model,
			HttpServletRequest httpServletRequest,
			@ModelAttribute PhysicalInfraConnectionModel physicalInfraConnectionModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraConnection -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }

		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraConnectionModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
				
		try {
			String phyInfraConId = encryption.decode(key, physicalInfraConnectionModel.getPhyInfraConId());
			physicalInfraConnectionModel.setPhyInfra_con_id(Integer.parseInt(phyInfraConId));
			int result = physicalInfraConnectionService
					.updatePhysicalInfraConnection(
							physicalInfraConnectionModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

			redirectAttributes.addFlashAttribute(
					"physicalInfraConnectionModel",
					physicalInfraConnectionModel);
			return ("redirect:/edit-physical-infra-connection?phyInfra_con_id=" + physicalInfraConnectionModel.getPhyInfraConId());

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraConnection -- END");
		}

		return "redirect:/list-physical-infra-connection";

	}
	@RequestMapping(value = "/fetch-ajax-physical-infra-connection", method = RequestMethod.GET)
	public String fetchAJAXPhysicalInfraConnection(HttpServletRequest httpServletRequest, @ModelAttribute ListingModel listingModel, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraConnection -- START");
		}
		LinkedList<PhysicalInfraConnectionModel> physicalInfraConnectionModels = null;

		try {
			physicalInfraConnectionModels = physicalInfraConnectionService.fetchAJAXPhysicalInfraConnection(listingModel);

			model.addAttribute("physicalInfraConnectionModels", physicalInfraConnectionModels);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraConnection -- END");
		}

		return "ajax-physical-infra-connection";

	}
}
