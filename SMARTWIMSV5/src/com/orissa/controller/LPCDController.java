/**
 * @formatter:off
 *
 */
package com.orissa.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.orissa.common.DateCalculation;
import com.orissa.common.MessageUtil;
import com.orissa.common.SortingMap;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.AjaxLpcdModel;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.ULBModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DashboardService;
import com.orissa.service.DivisionService;
import com.orissa.validation.DashboardValidation;

@Controller
public class LPCDController {

	private static Logger logger = Logger.getLogger(LPCDController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private SortingMap sortingMap;
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private DateCalculation dateCalculation;
	
	@Autowired
	private DashboardValidation dashboardValidation;
	
	@RequestMapping(value = "/lpcd", method = RequestMethod.GET)
	public String lpcdPage(Model model, HttpServletRequest httpServletRequest) {
	
		if (logger.isInfoEnabled()) {
			logger.info("lpcdPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		model.addAttribute("loginUsersModel2", loginUsersModel);
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		LinkedHashMap<String, Double> lpcdTotal = null;
		LinkedHashMap<String, DailyWaterSupplyModel> lpcdMap = null;
		List<String> datesBetween = new LinkedList<String>();
		List<Double> currentLPCD = new LinkedList<Double>();
		List<Double> previousLPCD = new LinkedList<Double>();
		double lpcdBoxValue = 0.0D;
		String monthMap = null;
		String yearMap = null;
		String startDateMap = null;
		String endDateMap = null;
		String currentMonth = "";
		String previousMonth = "";
		
		model.addAttribute("startDate", startDate);
		
		if(startDate != null) {
			
			monthMap = startDate.substring(5, 7);
			yearMap = startDate.substring(0, 4);
			
			if (monthMap != null) {
				
				List<String> dates = null;
				try {
					dates = dateCalculation.findDate(Integer.valueOf(yearMap), Integer.valueOf(monthMap));
					startDateMap = dates.get(0);
					endDateMap = dates.get(1);
					currentMonth = dates.get(3);
					previousMonth = dates.get(4);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
		try {
			
			if(flag == 1) {
				
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
				
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
					
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
						
					lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
					lpcdTotal = new LinkedHashMap<String, Double>();
					
					if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i = i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getRate_of_supply());
							}
						}
					} else {
						
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
						}
					}

						
				}
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();
					
							for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getRate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getPm_rate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getPm_rate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getRate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
						}
					
					}

								
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_rate_of_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}
				
				model.addAttribute("lpcdBoxValue", lpcdBoxValue);
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
								
				/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
				
				System.out.println("lpcdTotal: "+ lpcdTotal.size());
				int n = 3;
				if(amrutTypeId == null ) {
					 
					 n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
				 }
				
				model.addAttribute("topValue", n);
				HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
				
				//System.out.println("top10LpcdUlbs: "+ top10LpcdUlbs);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				 
				Set mapSet = (Set) top10LpcdUlbs.entrySet();
		        Iterator mapIterator = mapSet.iterator();
		        while (mapIterator.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
			        String keyValue = (String) mapEntry.getKey();
			        top10ulb.add(keyValue);
			        Double value = (Double) mapEntry.getValue();
			        top10lpcd.add(value);
		        }
		        
		        //System.out.println("top10ulb: "+ top10ulb);
				//System.out.println("top10lpcd: "+ top10lpcd);
				
				 model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
			     model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
			     
			     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
				
			     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
			     
			     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
			    
			    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
			     
			    //System.out.println("bottom10LpcdUlbs: "+ bottom10LpcdUlbs);
			     
		        LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
		        
		        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
		        Iterator mapIterator2 = mapSet2.iterator();
		        while (mapIterator2.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
			        String keyValue = (String) mapEntry.getKey();
			        bottom10ulb.add(keyValue);
			        Double value = (Double) mapEntry.getValue();
			        bottom10lpcd.add(value);
		        }
		        
		        //System.out.println("bottom10ulb: "+ bottom10ulb);
				//System.out.println("bottom10lpcd: "+ bottom10lpcd);
				 
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
				
				/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
				
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
				LinkedList<String> middleulb1 = new LinkedList<String>();
		        LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
		        
		        LinkedList<String> middleulb2 = new LinkedList<String>();
		        LinkedList<Double> middlelpcd2 = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 175) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else if(entry1.getValue() >= 135 && entry1.getValue() < 175) {
		        		
		        		middleulb1.add(entry1.getKey());
		        		middlelpcd1.add(entry1.getValue());
		        		
		        	} else if(entry1.getValue() >= 50 && entry1.getValue() < 135) {
		        		
		        		middleulb2.add(entry1.getKey());
		        		middlelpcd2.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
		        model.addAttribute("middleulb1", new ObjectMapper().writeValueAsString(middleulb1));
				model.addAttribute("middlelpcd1", new ObjectMapper().writeValueAsString(middlelpcd1));
				
				model.addAttribute("middleulb2", new ObjectMapper().writeValueAsString(middleulb2));
				model.addAttribute("middlelpcd2", new ObjectMapper().writeValueAsString(middlelpcd2));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				model.addAttribute("flag", false);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_rate_of_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_rate_of_supply());
									
					}
				}
					
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
			} else if(flag == 2) {
				
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i = i+2) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
						
						if(dailyWaterSupplyModel1.getType().equals("current")) {
						
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
							
						} else {
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getRate_of_supply());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
					}
				}

					
			}
				
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getRate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getPm_rate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getPm_rate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getRate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_rate_of_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}
				
				model.addAttribute("lpcdBoxValue", lpcdBoxValue);
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
			    LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
								
				if(lpcdTotal.size() >= 6 ) {
					
					/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					
					int n = 3;
					
					model.addAttribute("topValue", n);
					HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
					
					//System.out.println("top10LpcdUlbs: "+ top10LpcdUlbs);
					
					Set mapSet = (Set) top10LpcdUlbs.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd.add(value);
			        }
			        
			        //System.out.println("top10ulb: "+ top10ulb);
					//System.out.println("top10lpcd: "+ top10lpcd);
					
					 model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
				     model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
				    
				    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
				     
				    //System.out.println("bottom10LpcdUlbs: "+ bottom10LpcdUlbs);
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd.add(value);
			        }
			        
			        //System.out.println("bottom10ulb: "+ bottom10ulb);
					//System.out.println("bottom10lpcd: "+ bottom10lpcd);
					 
					model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
					model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
					model.addAttribute("flag", false);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
					
				} else {
					
					model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
				    model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
				     
					model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
					model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
					model.addAttribute("flag", true);
				}
					
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
				LinkedList<String> middleulb1 = new LinkedList<String>();
		        LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
		        
		        LinkedList<String> middleulb2 = new LinkedList<String>();
		        LinkedList<Double> middlelpcd2 = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 175) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else if(entry1.getValue() >= 135 && entry1.getValue() < 175) {
		        		
		        		middleulb1.add(entry1.getKey());
		        		middlelpcd1.add(entry1.getValue());
		        		
		        	} else if(entry1.getValue() >= 50 && entry1.getValue() < 135) {
		        		
		        		middleulb2.add(entry1.getKey());
		        		middlelpcd2.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
		        model.addAttribute("middleulb1", new ObjectMapper().writeValueAsString(middleulb1));
				model.addAttribute("middlelpcd1", new ObjectMapper().writeValueAsString(middlelpcd1));
				
				model.addAttribute("middleulb2", new ObjectMapper().writeValueAsString(middleulb2));
				model.addAttribute("middlelpcd2", new ObjectMapper().writeValueAsString(middlelpcd2));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				
				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_rate_of_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_rate_of_supply());
									
					}
				}
				
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
			} else if(flag == 3) {
				
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i = i+2) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
						
						if(dailyWaterSupplyModel1.getType().equals("current")) {
						
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
							
						} else {
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getRate_of_supply());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
					}
				}

					
			}
				
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getRate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getPm_rate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getPm_rate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getRate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_rate_of_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				model.addAttribute("lpcdBoxValue", lpcdBoxValue);
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
			    LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
			    
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
				LinkedList<String> middleulb1 = new LinkedList<String>();
		        LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
		        
		        LinkedList<String> middleulb2 = new LinkedList<String>();
		        LinkedList<Double> middlelpcd2 = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 175) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else if(entry1.getValue() >= 135 && entry1.getValue() < 175) {
		        		
		        		middleulb1.add(entry1.getKey());
		        		middlelpcd1.add(entry1.getValue());
		        		
		        	} else if(entry1.getValue() >= 50 && entry1.getValue() < 135) {
		        		
		        		middleulb2.add(entry1.getKey());
		        		middlelpcd2.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
			    model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
			     
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
		        
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
		        model.addAttribute("middleulb1", new ObjectMapper().writeValueAsString(middleulb1));
				model.addAttribute("middlelpcd1", new ObjectMapper().writeValueAsString(middlelpcd1));
				
				model.addAttribute("middleulb2", new ObjectMapper().writeValueAsString(middleulb2));
				model.addAttribute("middlelpcd2", new ObjectMapper().writeValueAsString(middlelpcd2));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				model.addAttribute("flag", false);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
			     
			     String month = null;
					String year = null;
					String startDate1 = null;
					String endDate1 = null;
					
					if(startDate != null) {
						
						month = startDate.substring(5, 7);
						year = startDate.substring(0, 4);
						
						if (month != null) {
							
							List<String> dates = null;
							try {
								dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
								startDate1 = dates.get(0);
								endDate1 = startDate;
							} catch (NumberFormatException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							} 
							
						}
					}
					datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
					
					for(String date:datesBetween) {
						
						LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
						
						if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
							
							// For Progress Report Current Value
							currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_rate_of_supply());
							
							// For Progress Report Previous Value
							previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_rate_of_supply());
										
						}
					}
				
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
				
			} else if(flag == 4) {
				
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
				
				listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i = i+2) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
						
						if(dailyWaterSupplyModel1.getType().equals("current")) {
						
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
							
						} else {
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getRate_of_supply());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
					}
				}

					
			}
				
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getRate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getPm_rate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getPm_rate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getRate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_rate_of_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}
				
				model.addAttribute("lpcdBoxValue", lpcdBoxValue);
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
				model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));

				LinkedList<String> bottom10ulb = new LinkedList<String>();
				LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));

				LinkedList<String> middleulb1 = new LinkedList<String>();
				LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
				LinkedList<String> middleulb2 = new LinkedList<String>();
				LinkedList<Double> middlelpcd2 = new LinkedList<Double>();

				model.addAttribute("middleulb1", new ObjectMapper().writeValueAsString(middleulb1));
				model.addAttribute("middlelpcd1", new ObjectMapper().writeValueAsString(middlelpcd1));
				model.addAttribute("middleulb2", new ObjectMapper().writeValueAsString(middleulb2));
				model.addAttribute("middlelpcd2", new ObjectMapper().writeValueAsString(middlelpcd2));
				
				LinkedList<String> topulb = new LinkedList<String>();
				LinkedList<Double> toplpcd = new LinkedList<Double>();
				
				model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
		        model.addAttribute("middleulb1", new ObjectMapper().writeValueAsString(middleulb1));
				model.addAttribute("middlelpcd1", new ObjectMapper().writeValueAsString(middlelpcd1));
				
				model.addAttribute("middleulb2", new ObjectMapper().writeValueAsString(middleulb2));
				model.addAttribute("middlelpcd2", new ObjectMapper().writeValueAsString(middlelpcd2));
				
				LinkedList<String> bottomulb = new LinkedList<String>();
				LinkedList<Double> bottomlpcd = new LinkedList<Double>();
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				model.addAttribute("flag", false);
				
				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_rate_of_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_rate_of_supply());
									
					}
				}
				
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
			}
			
			model.addAttribute("datesBetween", new ObjectMapper().writeValueAsString(datesBetween));
			model.addAttribute("currentLPCD", new ObjectMapper().writeValueAsString(currentLPCD));
			model.addAttribute("previousLPCD", new ObjectMapper().writeValueAsString(previousLPCD));
			
			/*for (Map.Entry<String, DailyWaterSupplyModel> entry : lpcdMap.entrySet()) {
				
			    System.out.println("ULB Name -- >> "+entry.getKey() + "Current LPCD --- >> " + entry.getValue().getRate_of_supply() + "Previous LPCD --- >> " + entry.getValue().getPm_rate_of_supply());
			}*/
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("lpcdPage -- END");
		}

		return "lpcd";

	}
	
	
	@RequestMapping(value = "/ajaxLpcd", method = RequestMethod.GET)
	public @ResponseBody AjaxLpcdModel ajaxLpcd(
			@ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {
	
		if (logger.isInfoEnabled()) {
			logger.info("ajaxLpcd -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		AjaxLpcdModel ajaxLpcdModel = new AjaxLpcdModel();
		
		try {
			
			
			dashboardValidation.dashboardValidation(listingModel, amrutTypeId, selectedYear, month);
			
		String startDate = null;
		String endDate = null;
		String days = null;
		String year = null;
		String startDate1 = null;
		String endDate1 = null;
		String startDateMap = null;
		String endDateMap = null;
		String currentMonth = "";
		String previousMonth = "";
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxLpcdModel.setPhDivisionModels(phDivisionModels);
					
					listingModel.setDistrictId(null);
					listingModel.setDivisionId(null);
					listingModel.setUlbId(null);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if (month != null) {
			
			List<String> dates = null;
			try {
				dates = dateCalculation.findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
				startDateMap = dates.get(0);
				endDateMap = dates.get(1);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(0);
			endDate = dates.get(1);
			days = dates.get(2);
			currentMonth = dates.get(3);
			previousMonth = dates.get(4);
			startDate1 = startDate;
			endDate1 = endDate;
			
		} else {
			
			startDate = listingModel.getDate();
			
			if(startDate != null) {
				
				month = startDate.substring(5, 7);
				year = startDate.substring(0, 4);
				
				if (month != null) {
					
					List<String> dates = null;
					try {
						dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
						startDate1 = dates.get(0);
						endDate1 = startDate;
						startDateMap = dates.get(0);
						endDateMap = dates.get(1);
						currentMonth = dates.get(3);
						previousMonth = dates.get(4);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					} 
					
				}
				
			}
		
		}
		
		LinkedHashMap<String, Double> lpcdTotal = null;
		LinkedHashMap<String, DailyWaterSupplyModel> lpcdMap = null;
		List<String> datesBetween = new LinkedList<String>();
		List<Double> currentLPCD = new LinkedList<Double>();
		List<Double> previousLPCD = new LinkedList<Double>();
		double lpcdBoxValue = 0.0D;
		
		try {
			
			if(flag == 1) {
				
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
				
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
					
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i = i+2) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
						
						if(dailyWaterSupplyModel1.getType().equals("current")) {
						
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
							
						} else {
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getRate_of_supply());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
					}
				}

					
			}
				//System.out.println("startDateMap = > "+startDateMap);System.out.println("endDateMap = > "+endDateMap);
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getRate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getPm_rate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getPm_rate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getRate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_rate_of_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}
				
				ajaxLpcdModel.setLpcdBoxValue(lpcdBoxValue);
				ajaxLpcdModel.setLpcdMap(lpcdMap);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
				
				if(lpcdTotal.size() >= 6) {
								
					/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					int n = 3;
					if((amrutTypeId == null) && (listingModel.getDivisionId() == null) && (listingModel.getDistrictId() == null) && (listingModel.getUlbId() == null)) {
						 
						 n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
					 }
					
					ajaxLpcdModel.setTopValue(n);
					HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
					
					Set mapSet = (Set) top10LpcdUlbs.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd.add(value);
			        }
			        
					//System.out.println("top10lpcd: "+ top10lpcd);
					
			        ajaxLpcdModel.setTop10ulb(top10ulb);
			        ajaxLpcdModel.setTop10lpcd(top10lpcd);
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
				    
				    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
				     
				    //System.out.println("bottom10LpcdUlbs: "+ bottom10LpcdUlbs);
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd.add(value);
			        }
			        
			        //System.out.println("bottom10ulb: "+ bottom10ulb);
					//System.out.println("bottom10lpcd: "+ bottom10lpcd);
					 
			        ajaxLpcdModel.setBottom10ulb(bottom10ulb);
			        ajaxLpcdModel.setBottom10lpcd(bottom10lpcd);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
			        
			        /* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
					
					LinkedList<String> topulb = new LinkedList<String>();
			        LinkedList<Double> toplpcd = new LinkedList<Double>();
			        
					LinkedList<String> middleulb1 = new LinkedList<String>();
			        LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
			        
			        LinkedList<String> middleulb2 = new LinkedList<String>();
			        LinkedList<Double> middlelpcd2 = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 175) {
			        		
			        		topulb.add(entry1.getKey());
			        		toplpcd.add(entry1.getValue());
			        		
			        	} else if(entry1.getValue() >= 135 && entry1.getValue() < 175) {
			        		
			        		middleulb1.add(entry1.getKey());
			        		middlelpcd1.add(entry1.getValue());
			        		
			        	} else if(entry1.getValue() >= 50 && entry1.getValue() < 135) {
			        		
			        		middleulb2.add(entry1.getKey());
			        		middlelpcd2.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb.add(entry1.getKey());
			        		bottomlpcd.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxLpcdModel.setTopulb(topulb);
			        ajaxLpcdModel.setToplpcd(toplpcd);
			        
			        ajaxLpcdModel.setMiddleulb1(middleulb1);
			        ajaxLpcdModel.setMiddlelpcd1(middlelpcd1);
					
			        ajaxLpcdModel.setMiddleulb2(middleulb2);
			        ajaxLpcdModel.setMiddlelpcd2(middlelpcd2);
			        
			        ajaxLpcdModel.setBottomulb(bottomulb);
			        ajaxLpcdModel.setBottomlpcd(bottomlpcd);
			        
			        ajaxLpcdModel.setFlag(false);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
		        
				} else {
					
					/* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
					
					LinkedList<String> topulb = new LinkedList<String>();
			        LinkedList<Double> toplpcd = new LinkedList<Double>();
			        
					LinkedList<String> middleulb1 = new LinkedList<String>();
			        LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
			        
			        LinkedList<String> middleulb2 = new LinkedList<String>();
			        LinkedList<Double> middlelpcd2 = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 175) {
			        		
			        		topulb.add(entry1.getKey());
			        		toplpcd.add(entry1.getValue());
			        		
			        	} else if(entry1.getValue() >= 135 && entry1.getValue() < 175) {
			        		
			        		middleulb1.add(entry1.getKey());
			        		middlelpcd1.add(entry1.getValue());
			        		
			        	} else if(entry1.getValue() >= 50 && entry1.getValue() < 135) {
			        		
			        		middleulb2.add(entry1.getKey());
			        		middlelpcd2.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb.add(entry1.getKey());
			        		bottomlpcd.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxLpcdModel.setTopulb(topulb);
			        ajaxLpcdModel.setToplpcd(toplpcd);
			        
			        ajaxLpcdModel.setMiddleulb1(middleulb1);
			        ajaxLpcdModel.setMiddlelpcd1(middlelpcd1);
					
			        ajaxLpcdModel.setMiddleulb2(middleulb2);
			        ajaxLpcdModel.setMiddlelpcd2(middlelpcd2);
			        
			        ajaxLpcdModel.setBottomulb(bottomulb);
			        ajaxLpcdModel.setBottomlpcd(bottomlpcd);
			        
			        ajaxLpcdModel.setTop10ulb(top10ulb);
			        ajaxLpcdModel.setTop10lpcd(top10lpcd);
			        
			        ajaxLpcdModel.setBottom10ulb(bottom10ulb);
			        ajaxLpcdModel.setBottom10lpcd(bottom10lpcd);
			        
			        ajaxLpcdModel.setFlag(true);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_rate_of_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_rate_of_supply());
									
					}
				}
					
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
			} else if(flag == 2) {
				
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i = i+2) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
						
						if(dailyWaterSupplyModel1.getType().equals("current")) {
						
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
							
						} else {
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getRate_of_supply());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
					}
				}

					
			}
				
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getRate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getPm_rate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getPm_rate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getRate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_rate_of_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				ajaxLpcdModel.setLpcdBoxValue(lpcdBoxValue);
				ajaxLpcdModel.setLpcdMap(lpcdMap);
								
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
				
				if(lpcdTotal.size() >= 6) {
								
					/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					int n = 3;
					if((amrutTypeId == null) && (listingModel.getDivisionId() == null) && (listingModel.getDistrictId() == null) && (listingModel.getUlbId() == null)) {
						 
						 n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
					 }
					
					ajaxLpcdModel.setTopValue(n);
					HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
					
					Set mapSet = (Set) top10LpcdUlbs.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd.add(value);
			        }
			        
					//System.out.println("top10lpcd: "+ top10lpcd);
					
			        ajaxLpcdModel.setTop10ulb(top10ulb);
			        ajaxLpcdModel.setTop10lpcd(top10lpcd);
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
				    
				    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
				     
				    //System.out.println("bottom10LpcdUlbs: "+ bottom10LpcdUlbs);
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd.add(value);
			        }
			        
			        //System.out.println("bottom10ulb: "+ bottom10ulb);
					//System.out.println("bottom10lpcd: "+ bottom10lpcd);
					 
			        ajaxLpcdModel.setBottom10ulb(bottom10ulb);
			        ajaxLpcdModel.setBottom10lpcd(bottom10lpcd);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
			        
			        /* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
					
					LinkedList<String> topulb = new LinkedList<String>();
			        LinkedList<Double> toplpcd = new LinkedList<Double>();
			        
					LinkedList<String> middleulb1 = new LinkedList<String>();
			        LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
			        
			        LinkedList<String> middleulb2 = new LinkedList<String>();
			        LinkedList<Double> middlelpcd2 = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 175) {
			        		
			        		topulb.add(entry1.getKey());
			        		toplpcd.add(entry1.getValue());
			        		
			        	} else if(entry1.getValue() >= 135 && entry1.getValue() < 175) {
			        		
			        		middleulb1.add(entry1.getKey());
			        		middlelpcd1.add(entry1.getValue());
			        		
			        	} else if(entry1.getValue() >= 50 && entry1.getValue() < 135) {
			        		
			        		middleulb2.add(entry1.getKey());
			        		middlelpcd2.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb.add(entry1.getKey());
			        		bottomlpcd.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxLpcdModel.setTopulb(topulb);
			        ajaxLpcdModel.setToplpcd(toplpcd);
			        
			        ajaxLpcdModel.setMiddleulb1(middleulb1);
			        ajaxLpcdModel.setMiddlelpcd1(middlelpcd1);
					
			        ajaxLpcdModel.setMiddleulb2(middleulb2);
			        ajaxLpcdModel.setMiddlelpcd2(middlelpcd2);
			        
			        ajaxLpcdModel.setBottomulb(bottomulb);
			        ajaxLpcdModel.setBottomlpcd(bottomlpcd);
			        
			        ajaxLpcdModel.setFlag(false);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
		        
				} else {
					
					/* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
					
					LinkedList<String> topulb = new LinkedList<String>();
			        LinkedList<Double> toplpcd = new LinkedList<Double>();
			        
					LinkedList<String> middleulb1 = new LinkedList<String>();
			        LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
			        
			        LinkedList<String> middleulb2 = new LinkedList<String>();
			        LinkedList<Double> middlelpcd2 = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 175) {
			        		
			        		topulb.add(entry1.getKey());
			        		toplpcd.add(entry1.getValue());
			        		
			        	} else if(entry1.getValue() >= 135 && entry1.getValue() < 175) {
			        		
			        		middleulb1.add(entry1.getKey());
			        		middlelpcd1.add(entry1.getValue());
			        		
			        	} else if(entry1.getValue() >= 50 && entry1.getValue() < 135) {
			        		
			        		middleulb2.add(entry1.getKey());
			        		middlelpcd2.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb.add(entry1.getKey());
			        		bottomlpcd.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxLpcdModel.setTopulb(topulb);
			        ajaxLpcdModel.setToplpcd(toplpcd);
			        
			        ajaxLpcdModel.setMiddleulb1(middleulb1);
			        ajaxLpcdModel.setMiddlelpcd1(middlelpcd1);
					
			        ajaxLpcdModel.setMiddleulb2(middleulb2);
			        ajaxLpcdModel.setMiddlelpcd2(middlelpcd2);
			        
			        ajaxLpcdModel.setBottomulb(bottomulb);
			        ajaxLpcdModel.setBottomlpcd(bottomlpcd);
			        
			        ajaxLpcdModel.setTop10ulb(top10ulb);
			        ajaxLpcdModel.setTop10lpcd(top10lpcd);
			        
			        ajaxLpcdModel.setBottom10ulb(bottom10ulb);
			        ajaxLpcdModel.setBottom10lpcd(bottom10lpcd);
			        
			        ajaxLpcdModel.setFlag(true);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_rate_of_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_rate_of_supply());
									
					}
				}
				
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
			} else if(flag == 3) {
				
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i = i+2) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
						
						if(dailyWaterSupplyModel1.getType().equals("current")) {
						
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
							
						} else {
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getRate_of_supply());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
					}
				}

					
			}
				
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getRate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getPm_rate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getPm_rate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getRate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_rate_of_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				ajaxLpcdModel.setLpcdBoxValue(lpcdBoxValue);
				ajaxLpcdModel.setLpcdMap(lpcdMap);
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
			    LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
			    
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
				LinkedList<String> middleulb1 = new LinkedList<String>();
		        LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
		        
		        LinkedList<String> middleulb2 = new LinkedList<String>();
		        LinkedList<Double> middlelpcd2 = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 175) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else if(entry1.getValue() >= 135 && entry1.getValue() < 175) {
		        		
		        		middleulb1.add(entry1.getKey());
		        		middlelpcd1.add(entry1.getValue());
		        		
		        	} else if(entry1.getValue() >= 50 && entry1.getValue() < 135) {
		        		
		        		middleulb2.add(entry1.getKey());
		        		middlelpcd2.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        ajaxLpcdModel.setTopulb(topulb);
		        ajaxLpcdModel.setToplpcd(toplpcd);
		        
		        ajaxLpcdModel.setMiddleulb1(middleulb1);
		        ajaxLpcdModel.setMiddlelpcd1(middlelpcd1);
				
		        ajaxLpcdModel.setMiddleulb2(middleulb2);
		        ajaxLpcdModel.setMiddlelpcd2(middlelpcd2);
		        
		        ajaxLpcdModel.setBottomulb(bottomulb);
		        ajaxLpcdModel.setBottomlpcd(bottomlpcd);
		        
		        ajaxLpcdModel.setTop10ulb(top10ulb);
		        ajaxLpcdModel.setTop10lpcd(top10lpcd);
		        
		        ajaxLpcdModel.setBottom10ulb(bottom10ulb);
		        ajaxLpcdModel.setBottom10lpcd(bottom10lpcd);
		        
		        ajaxLpcdModel.setFlag(false);
		        
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_rate_of_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_rate_of_supply());
									
					}
				}
				
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
				
			} else if(flag == 4) {
				
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
				
				listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i = i+2) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
						
						if(dailyWaterSupplyModel1.getType().equals("current")) {
						
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
							
						} else {
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getRate_of_supply());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
						
						DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
						
						// For All LPCD
						lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
					}
				}

					
			}
				
				
				LinkedList<DailyWaterSupplyModel> mapDailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterSupplyModels != null && mapDailyWaterSupplyModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterSupplyModel>();

					for(int i = 0; i<mapDailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = mapDailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = mapDailyWaterSupplyModels.get(i+1);
							DailyWaterSupplyModel dailyWaterSupplyModel3 = new DailyWaterSupplyModel();
							ULBNameModel ulbNameModel = new ULBNameModel();
							ULBModel ulbModel = new ULBModel();
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getRate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getPm_rate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							} else {
								
								dailyWaterSupplyModel3.setRate_of_supply(dailyWaterSupplyModel1.getPm_rate_of_supply());
								dailyWaterSupplyModel3.setPm_rate_of_supply(dailyWaterSupplyModel2.getRate_of_supply());
								ulbModel.setLatitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyWaterSupplyModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
		        	DailyWaterSupplyModel dailyWaterSupplyModel4 = lpcdMap.get(entry1.getKey());
		        	dailyWaterSupplyModel4.setTotal_rate_of_supply(entry1.getValue());
		        	dailyWaterSupplyModel4.setCurrentMonth(currentMonth);
		        	dailyWaterSupplyModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterSupplyModel4);
					
				}

				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();

				LinkedList<String> bottom10ulb = new LinkedList<String>();
				LinkedList<Double> bottom10lpcd = new LinkedList<Double>();

				LinkedList<String> middleulb1 = new LinkedList<String>();
				LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
				LinkedList<String> middleulb2 = new LinkedList<String>();
				LinkedList<Double> middlelpcd2 = new LinkedList<Double>();

				LinkedList<String> topulb = new LinkedList<String>();
				LinkedList<Double> toplpcd = new LinkedList<Double>();
				
				LinkedList<String> bottomulb = new LinkedList<String>();
				LinkedList<Double> bottomlpcd = new LinkedList<Double>();
				
				ajaxLpcdModel.setTopulb(topulb);
		        ajaxLpcdModel.setToplpcd(toplpcd);
		        
		        ajaxLpcdModel.setMiddleulb1(middleulb1);
		        ajaxLpcdModel.setMiddlelpcd1(middlelpcd1);
				
		        ajaxLpcdModel.setMiddleulb2(middleulb2);
		        ajaxLpcdModel.setMiddlelpcd2(middlelpcd2);
		        
		        ajaxLpcdModel.setBottomulb(bottomulb);
		        ajaxLpcdModel.setBottomlpcd(bottomlpcd);
		        
		        ajaxLpcdModel.setTop10ulb(top10ulb);
		        ajaxLpcdModel.setTop10lpcd(top10lpcd);
		        
		        ajaxLpcdModel.setBottom10ulb(bottom10ulb);
		        ajaxLpcdModel.setBottom10lpcd(bottom10lpcd);
		        
		        ajaxLpcdModel.setFlag(false);
				
				ajaxLpcdModel.setLpcdBoxValue(lpcdBoxValue);
				ajaxLpcdModel.setLpcdMap(lpcdMap);
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels2 = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterSupplyModels2 != null && dailyWaterSupplyModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getTotal_rate_of_supply());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterSupplyModels2.get(dailyWaterSupplyModels2.size() - 1).getPm_total_rate_of_supply());
									
					}
				}
				
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
			}
			
			
			
			
			ajaxLpcdModel.setDatesBetween(datesBetween);
			ajaxLpcdModel.setCurrentLPCD(currentLPCD);
			ajaxLpcdModel.setPreviousLPCD(previousLPCD);
			
			/*if(days != null) {
				
				ajaxLpcdModel.setLpcdBoxValue((double) (Math.round(lpcdBoxValue/Integer.parseInt(days)*100)/100));
			}*/
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("ajaxLpcd -- END");
		}

		return ajaxLpcdModel;

	}
}
