package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.DailyConnetionsModel;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.service.DailyConnectionsService;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;

@Controller
public class DailyConnectionsController implements SaltTracker {

	private static Logger logger = Logger.getLogger(DailyConnectionsController.class);

	@Autowired
	private DailyConnectionsService dailyConnetionsService;
	
	@Autowired
	private DivisionService divisionService;
	

	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private Encryption encryption;
	
	@RequestMapping(value = "/add-daily-connection", method = RequestMethod.GET)
	public String addDailyConnectionPage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyConnectionPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyConnectionPage -- END");
		}

		return "add-daily-connection";

	}

	@RequestMapping(value = "/add-daily-connection", method = RequestMethod.POST)
	public String addDailyConnection(Model model,HttpServletRequest httpServletRequest,@ModelAttribute DailyConnetionsModel dailyConnectionsModel,BindingResult bindingResult,final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyConnection -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyConnectionsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
				
		try {
			int result = dailyConnetionsService.addDailyConnetions(dailyConnectionsModel,httpServletRequest);
			
			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		}catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute("dailyConnectionsModel", dailyConnectionsModel);
			return ("redirect:/add-daily-connection");

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyConnection -- END");
		}

		return "redirect:/list-daily-connection";

	}
	
	@RequestMapping(value = "/list-daily-connection", method = RequestMethod.GET)
	public String fetchDailyConnection(HttpServletRequest httpServletRequest, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnection -- START");
		}
		
		try {
			LinkedList<DailyConnetionsModel> dailyConnectionsModels = dailyConnetionsService.fetchDailyConnetions(httpServletRequest);
			
			if(dailyConnectionsModels.size() > 0) {
				
				model.addAttribute("dailyConnectionsModels", dailyConnectionsModels);
			} else {
				
				model.addAttribute("emptyList", messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnection -- END");
		}

		return "list-daily-connection";

	}
	
	@RequestMapping(value = "/edit-daily-connection", method = RequestMethod.GET)
	public String editDailyConnectionPage(@RequestParam String con_id, Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editDailyConnectionPage -- START");
		}
		
		if(StringUtils.isBlank(con_id)){
			return "redirect:/error404";
		}
		
		DailyConnetionsModel dailyConnectionsModel = null;
		
		String con_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			con_id1 = encryption.decode(key, id);
			if(con_id.length() > id.length())
				throw new Exception();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		
		try {
			dailyConnectionsModel = dailyConnetionsService.fetchDailyConnetionsById(Integer.parseInt(con_id1));
			
			if(dailyConnectionsModel.getCon_id() != 0) {
				model.addAttribute("dailyConnectionsModel", dailyConnectionsModel);
			} else {
				model.addAttribute("emptyList", messageUtil.getBundle("no.record.found"));
				return "redirect:/list-daily-connection";
			}
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(dailyConnectionsModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("editDailyConnectionPage -- END");
		}

		return "edit-daily-connection";

	}

	@RequestMapping(value = "/delete-daily-connection", method = RequestMethod.POST)
	public String deleteDailyConnectionPage(final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest, DailyConnetionsModel dailyConnetionsModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyConnectionPage -- START");
		}
		
		String con_id = dailyConnetionsModel.getConId();
				
		if (StringUtils.isBlank(con_id)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyConnetionsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
			
		String con_id1 = null;
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			con_id1 = encryption.decode(key, con_id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		try {
			int result = dailyConnetionsService
					.deleteDailyConnectionById(Integer.parseInt(con_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyConnectionPage -- END");
		}

		return "redirect:/list-daily-connection";

	}
	
	@RequestMapping(value = "/isApproved-daily-connection", method = RequestMethod.GET)
	public @ResponseBody String isApprovedUpdate(@RequestParam String con_id,final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		if (StringUtils.isBlank(con_id)) {
			return "redirect:/error404";
		}

		String con_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			con_id1 = encryption.decode(key, id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			int result = dailyConnetionsService
					.isApprovedUpdate(Integer.parseInt(con_id1));

			if (result == 1) {

				System.out.println("Approved");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("Approved.done"));
			} else {
				System.out.println("Approved Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("Approved.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return "success";

	}
	
	@RequestMapping(value = "/isNotApproved-daily-connection", method = RequestMethod.GET)
	public @ResponseBody String isNotApprovedUpdate(@RequestParam String con_id,final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}

		if (StringUtils.isBlank(con_id)) {
			return "redirect:/error404";
		}

		String con_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			con_id1 = encryption.decode(key, id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			int result = dailyConnetionsService
					.isNotApprovedUpdate(Integer.parseInt(con_id1));

			if (result == 1) {

				System.out.println("Approved");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("Approved.done"));
			} else {
				System.out.println("Approved Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("Approved.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return "success";

	}

	@RequestMapping(value = "/update-daily-connection", method = RequestMethod.POST)
	public String updateDailyConnection(Model model,HttpServletRequest httpServletRequest, @ModelAttribute DailyConnetionsModel dailyConnectionsModel,BindingResult bindingResult,final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyConnection -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyConnectionsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		String key = messageUtil.getBundle("secret.key");
		try {
			String conId = encryption.decode(key, dailyConnectionsModel.getConId());
			dailyConnectionsModel.setCon_id(Integer.parseInt(conId));
			int result = dailyConnetionsService.updateDailyConnetions(dailyConnectionsModel,httpServletRequest);
			
			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		}catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute("dailyConnectionsModel", dailyConnectionsModel);
			
			return ("redirect:/edit-daily-connection?con_id="+dailyConnectionsModel.getConId());

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {
		
		throw new ServletException(messageUtil.getBundle("CSRF"));

	}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("updateDailyConnection -- END");
		}

		return "redirect:/list-daily-connection";

	}
	
	@RequestMapping(value = "/fetch-ajax-daily-connection", method = RequestMethod.GET)
	public String fetchAJAXDailyConnection(HttpServletRequest httpServletRequest, Model model, @ModelAttribute ListingModel listingModel) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyConnection -- START");
		}
		
		try {
			LinkedList<DailyConnetionsModel> dailyConnectionsModels = dailyConnetionsService.fetchAJAXDailyConnetions(listingModel);
			
			if(dailyConnectionsModels.size() > 0) {
				
				model.addAttribute("dailyConnectionsModels", dailyConnectionsModels);
			} else {
				
				model.addAttribute("emptyList", messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyConnection -- END");
		}

		return "ajax-daily-connection";

	}
}
