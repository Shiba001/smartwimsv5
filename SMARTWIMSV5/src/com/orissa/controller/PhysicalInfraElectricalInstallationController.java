package com.orissa.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraElectricalInstallationsModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraElectricalInstallationService;
import com.orissa.validation.PhysicalInfraElectricalInstallationValidation;

@Controller
public class PhysicalInfraElectricalInstallationController implements SaltTracker {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraElectricalInstallationController.class);
	
	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DivisionService divisionService;
	
	@Autowired
	private PhysicalInfraElectricalInstallationService physicalInfraElectricalInstallationService;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private PhysicalInfraElectricalInstallationValidation physicalInfraElectricalInstallationValidation;
	
	@Autowired
	private Encryption encryption;
	
	@RequestMapping(value = "/add-physical-infra-electric-installation", method = RequestMethod.GET)
	public String addPhysElectInstallation(final ModelMap modelMap, @RequestParam(required=false) String ulb_id, HttpSession httpSession, HttpServletRequest httpServletRequest, @RequestParam(required=false) String date_of_report) throws Exception  {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysElectInstallation -- START");
		}
		
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String trgt_id2 = httpServletRequest.getQueryString();
		String id = "";
		Integer ulbId = null;
		try{
			
			if(trgt_id2 != null){
				String key = messageUtil.getBundle("secret.key"); 
				id = trgt_id2.split("=")[1]+"==";
				ulbId = Integer.parseInt(encryption.decode(key, id));
			}
			if(ulb_id != null){
				if(ulb_id.length() > id.length())
					throw new Exception();
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		
		Integer ulb_id1 = ulbId;
		
		if(ulb_id1 == null && httpSession.getAttribute("loginUsersModel2") != null){
			LoginUsersModel loginUsersModel = (LoginUsersModel) httpSession.getAttribute("loginUsersModel2");
			int userLevel = loginUsersModel.getUserLevelModel().getUser_level_id();
			modelMap.addAttribute("addreq", "true");
			if(userLevel == 4){
				ulb_id1 = loginUsersModel.getUlbNameModel().getUlb_id();
			}
		}
		if(ulb_id1 != null){
			List<PhysicalInfraElectricalInstallationsModel> physicalInfraElectricalInstallationsModels = physicalInfraElectricalInstallationService.fetchElectricInstallation(ulb_id1, date_of_report);
			modelMap.addAttribute("physicalInfraElectricalInstallationsModels", physicalInfraElectricalInstallationsModels);
			modelMap.addAttribute("ulb_id", ulbId);
		}
		
		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(ulb_id1);

			if (upperLevels.size() > 0) {
				modelMap.addAttribute("upperLevels", upperLevels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			System.out.println("ulb_id is null.....");
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("addPhysElectInstallation -- END");
		}

		return "add-physical-infra-electric-installation";

	}
	
	@RequestMapping(value = "/add-physical-infra-electric-installation", method = RequestMethod.POST)
	public String addPhysElectInstallation(
			Model model,
			@ModelAttribute PhysicalInfraElectricalInstallationsModel physicalInfraElectricalInstallationsModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysElectInstallation -- START");
		}
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraElectricalInstallationsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		List<PhysicalInfraElectricalInstallationsModel> physicalInfraElectricalInstallationsModels = new ArrayList<PhysicalInfraElectricalInstallationsModel>();
		//String key = messageUtil.getBundle("secret.key");
		try {
			/*
			String ulbId = encryption.decode(key, physicalInfraElectricalInstallationsModel.getEnculbId());
			physicalInfraElectricalInstallationsModel.setUlb_id(Integer.parseInt(ulbId));*/
			physicalInfraElectricalInstallationValidation.createPhysicalInfraElectricalInstallationValidationNew(physicalInfraElectricalInstallationsModel);
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute(
					"physicalInfraElectricalInstallationsModel",
					physicalInfraElectricalInstallationsModel);
			if(physicalInfraElectricalInstallationsModel.getUlb_id() == null){
				return ("redirect:/add-physical-infra-electric-installation");
			}
			else{
				return ("redirect:/add-physical-infra-electric-installation?ulb_id="+physicalInfraElectricalInstallationsModel.getEnculbId());
			}
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int i=0; i< physicalInfraElectricalInstallationsModel.getList_loc_of_inst().size(); i++) {
			
			PhysicalInfraElectricalInstallationsModel physicalInfraElectricalInstallationsModel2 = new PhysicalInfraElectricalInstallationsModel();
		
			physicalInfraElectricalInstallationsModel2.setPhyInfra_ele_inst_slno(i+1);
			physicalInfraElectricalInstallationsModel2.setUlbId(physicalInfraElectricalInstallationsModel.getUlbId());
			physicalInfraElectricalInstallationsModel2.setDate_of_report(physicalInfraElectricalInstallationsModel.getDate_of_report());
			physicalInfraElectricalInstallationsModel2.setDueDate(physicalInfraElectricalInstallationsModel.getDueDate());
			physicalInfraElectricalInstallationsModel2.setCap_of_trnf(physicalInfraElectricalInstallationsModel.getList_cap_of_trnf().get(i));
			physicalInfraElectricalInstallationsModel2.setHp_lt(physicalInfraElectricalInstallationsModel.getList_hp_lt().get(i));
			physicalInfraElectricalInstallationsModel2.setLoc_of_inst(physicalInfraElectricalInstallationsModel.getList_loc_of_inst().get(i));
			physicalInfraElectricalInstallationsModel2.setYear_inst(physicalInfraElectricalInstallationsModel.getList_year_inst().get(i));
			
			physicalInfraElectricalInstallationsModels.add(physicalInfraElectricalInstallationsModel2);
		
		}
		
		physicalInfraElectricalInstallationsModel.setElectricalInstallationsModels(physicalInfraElectricalInstallationsModels);
		//System.out.println(physicalInfraElectricalInstallationsModel.getElectricalInstallationsModels().get(0));
		
		try {
			/*int result = physicalInfraElectricalInstallationService.addPhysElectInstallation(physicalInfraElectricalInstallationsModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}*/
			
			int result = 0;
			//System.out.println("physicalInfraElectricalInstallationsModel.getUlb_id()=="+physicalInfraElectricalInstallationsModel.getUlb_id());
			if(physicalInfraElectricalInstallationsModel.getUlb_id() == null){
				result = physicalInfraElectricalInstallationService.addPhysElectInstallation(physicalInfraElectricalInstallationsModel, httpServletRequest);
			}
			else{
				result = physicalInfraElectricalInstallationService.updatePhysElectInstallation(physicalInfraElectricalInstallationsModel, httpServletRequest);
			}
			

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute(
					"physicalInfraElectricalInstallationsModel",
					physicalInfraElectricalInstallationsModel);
			
			return ("redirect:/add-physical-infra-electric-installation");
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyComplaints -- END");
		}

		return "redirect:/list-physical-infra-electric-installation";

	}
	
	@RequestMapping(value = "/list-physical-infra-electric-installation", method = RequestMethod.GET)
	public String fetchPhysicalInfraElectricalInst(@ModelAttribute ListingModel listingModel, HttpSession httpSession, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraElectricalInst -- START");
		}

		try {
			if(httpSession.getAttribute("loginUsersModel2") != null){
				LoginUsersModel loginUsersModel = (LoginUsersModel) httpSession.getAttribute("loginUsersModel2");
				int userLevel = loginUsersModel.getUserLevelModel().getUser_level_id();
				if(userLevel == 2){
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				}
				else if(userLevel == 3){
					listingModel.setDivisionId(0);
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				}
				else if(userLevel == 4){
					listingModel.setDivisionId(0);
					listingModel.setDistrictId(0);
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				}
			}
			LinkedList<ULBNameModel>  ulbNameModels= physicalInfraElectricalInstallationService.fetchUlbS(listingModel);

			if (ulbNameModels.size() > 0) {

				model.addAttribute("ulbNameModels", ulbNameModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraElectricalInst -- END");
		}

		return "list-physical-infra-electric-installation";

	}
	
	@RequestMapping(value = "/fetch-ajax-physical-infra-electric-installation", method = RequestMethod.GET)
	public String fetchAjaxPhysicalInfraElectricalInst(@ModelAttribute ListingModel listingModel, HttpSession httpSession, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAjaxPhysicalInfraElectricalInst -- START");
		}

		try {
			
			LinkedList<ULBNameModel>  ulbNameModels= physicalInfraElectricalInstallationService.fetchUlbS(listingModel);

			if (ulbNameModels.size() > 0) {

				model.addAttribute("ulbNameModels", ulbNameModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAjaxPhysicalInfraElectricalInst -- END");
		}

		return "ajax-physical-infra-electric-installation";

	}

}
