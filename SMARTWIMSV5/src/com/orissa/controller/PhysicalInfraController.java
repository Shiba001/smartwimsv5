package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orissa.common.MessageUtil;
import com.orissa.common.SortingMap;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraElectricalInstallationsModel;
import com.orissa.model.PhysicalInfraModel;
import com.orissa.model.PhysicalInfraProductionWellsModel;
import com.orissa.model.PhysicalInfraPumpsAndMotorsModel;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraService;

@Controller
public class PhysicalInfraController {

	private static Logger logger = Logger.getLogger(PhysicalInfraController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private SortingMap sortingMap;
	
	@Autowired
	private PhysicalInfraService physicalInfraService;
	
	@RequestMapping(value = "/physical-infra", method = RequestMethod.GET)
	public String physicalInfraPage(final Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("physicalInfraPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		
		if (logger.isInfoEnabled()) {
			logger.info("physicalInfraPage -- END");
		}

		return "physical-infra";

	}
	
	@RequestMapping(value = "/physical-infra-report", method = RequestMethod.GET)
	public String physicalInfraReportPage(final Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("physicalInfraReportPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			
			LinkedList<PhysicalInfraModel> physicalInfraModels = physicalInfraService.fetchPhysicalInfraReport();
			
			model.addAttribute("physicalInfraModels", physicalInfraModels);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		
		if (logger.isInfoEnabled()) {
			logger.info("physicalInfraReportPage -- END");
		}
		

		return "physical-infra-report";

	}
	
	@RequestMapping(value = "/electrical-installation-report", method = RequestMethod.GET)
	public String electricalInstallationReportPage(final Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("electricalInstallationReportPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			
			Map<String, LinkedList<PhysicalInfraElectricalInstallationsModel>> electricInstallationReport = physicalInfraService
					.fetchElectricInstallationReport();
			
			model.addAttribute("electricInstallationReport", electricInstallationReport);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		
		

		if (logger.isInfoEnabled()) {
			logger.info("electricalInstallationReportPage -- END");
		}

		return "electrical-installation-report";

	}
	
	@RequestMapping(value = "/pump-motors-report", method = RequestMethod.GET)
	public String pumpMotorsReportPage(final Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("pumpMotorsReportPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			
			Map<String, LinkedList<PhysicalInfraPumpsAndMotorsModel>> pumpAndMotorsReport = physicalInfraService
					.fetchPumpAndMotorReport();
			
			model.addAttribute("pumpAndMotorsReport", pumpAndMotorsReport);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		

		if (logger.isInfoEnabled()) {
			logger.info("pumpMotorsReportPage -- END");
		}

		return "pump-motors-report";

	}
	
	@RequestMapping(value = "/prod-wells-report", method = RequestMethod.GET)
	public String prodWellsReportPage(final Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("prodWellsReportPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		try {
			
			Map<String, LinkedList<PhysicalInfraProductionWellsModel>> prodWellReport = physicalInfraService
					.fetchPhysicalInfraProdWellsReport();
			
			model.addAttribute("prodWellReport", prodWellReport);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		
		

		if (logger.isInfoEnabled()) {
			logger.info("prodWellsReportPage -- END");
		}

		return "prod-wells-report";

	}
}
