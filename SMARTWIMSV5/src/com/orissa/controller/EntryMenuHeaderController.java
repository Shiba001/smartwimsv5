package com.orissa.controller;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orissa.exception.DBConnectionException;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@RestController
@RequestMapping(value = "/entryHeaderMenu/**")
public class EntryMenuHeaderController {

	private static Logger logger = Logger
			.getLogger(EntryMenuHeaderController.class);

	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbService;

	@RequestMapping(value = "/distList")
	public List<PhDistrictModel> fetchDistList(@RequestParam int divisionId) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDistList -- START");
		}

		List<PhDistrictModel> districtModels = null;

		try {
			districtModels = districtService
					.fetchDistrictByDivisionId(divisionId);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDistList -- END");
		}

		return districtModels;
	}

	@RequestMapping(value = "/ULBList")
	public List<ULBNameModel> fetchULBList(@RequestParam int districtId) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBList -- START");
		}

		List<ULBNameModel> ulbNameModels = null;

		try {

			ulbNameModels = ulbService.fetchULBNameByDistrictId(districtId);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBList -- END");
		}

		return ulbNameModels;
	}
	
	@RequestMapping(value = "/free-amrut", method = RequestMethod.GET)
	public String freeAmrut(HttpServletRequest httpServletRequest) {

		if (logger.isDebugEnabled()) {
			logger.debug("freeAmrut - Start");
		}

		HttpSession httpSession = httpServletRequest.getSession();
		
		httpSession.removeAttribute("amrutTypeId");
		
		if (logger.isDebugEnabled()) {
			logger.debug("freeAmrut - End");
		}

		return null;
	}
}
