/**
 * @formatter:off
 *
 */
package com.orissa.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.orissa.common.DateCalculation;
import com.orissa.common.MessageUtil;
import com.orissa.common.SortingMap;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.AjaxComplaintsModel;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.ULBModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DashboardService;
import com.orissa.service.DivisionService;
import com.orissa.validation.DashboardValidation;

@Controller
public class ComplaintsController {

	private static Logger logger = Logger.getLogger(ComplaintsController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private SortingMap sortingMap;
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private DateCalculation dateCalculation;
	
	@Autowired
	private DashboardValidation dashboardValidation;
	
	@RequestMapping(value = "/complaints-resolved", method = RequestMethod.GET)
	public String complaintsPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("complaintsPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		model.addAttribute("loginUsersModel2", loginUsersModel);
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		LinkedHashMap<String, Double> lpcdTotal = null;
		LinkedHashMap<String, DailyComplaintsModel> lpcdMap = null;
		List<String> datesBetween = new LinkedList<String>();
		List<Double> currentLPCD = new LinkedList<Double>();
		List<Double> previousLPCD = new LinkedList<Double>();
		
		double total_comp_recveived = 0.0D;
		double total_comp_pending = 0.0D;
		double total_tw_comp_recveived = 0.0D;
		double total_tw_comp_pending = 0.0D;
		
		double totalReceived = 0.0D;
		double totalPending = 0.0D;
		
		double comp_recveived = 0.0D;
		double comp_pending = 0.0D;
		double tw_comp_recveived = 0.0D;
		double tw_comp_pending = 0.0D;
		double complaintsResolved = 0.0D;
		double received = 0.0D;
		double pending = 0.0D;
		
		double pm_comp_recveived = 0.0D;
		double pm_comp_pending = 0.0D;
		double pm_tw_comp_recveived = 0.0D;
		double pm_tw_comp_pending = 0.0D;
		double pm_complaintsResolved = 0.0D;
		double pm_received = 0.0D;
		double pm_pending = 0.0D;
		
		String monthMap = null;
		String yearMap = null;
		String startDateMap = null;
		String endDateMap = null;
		String currentMonth = "";
		String previousMonth = "";
		
		model.addAttribute("startDate", startDate);
		
		if(startDate != null) {
			
			monthMap = startDate.substring(5, 7);
			yearMap = startDate.substring(0, 4);
			
			if (monthMap != null) {
				
				List<String> dates = null;
				try {
					dates = dateCalculation.findDate(Integer.valueOf(yearMap), Integer.valueOf(monthMap));
					startDateMap = dates.get(0);
					endDateMap = dates.get(1);
					currentMonth = dates.get(3);
					previousMonth = dates.get(4);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
		try {
			
			if(flag == 1) {
				
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
				LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
					
				if ((dailyComplaintsModels != null) && (dailyComplaintsModels.size() > 0)) {
							
					total_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_recveived();
					total_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_pending();
					total_tw_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_recveived();
					total_tw_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_pending();
					
					totalReceived = total_comp_recveived + total_tw_comp_recveived;
					totalPending = total_comp_pending + total_tw_comp_pending;
					
					totalPending = (Math.round(((totalPending * 100)/totalReceived)*100.0)/100.0);
					
				}
				
				model.addAttribute("lpcdBoxValue", 100 - totalPending);
				
				if(dailyComplaintsModels != null && dailyComplaintsModels.size() > 0) {
						
					lpcdTotal = new LinkedHashMap<String, Double>();
					
					if(dailyComplaintsModels.size() > 2 && dailyComplaintsModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyComplaintsModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

						for(int i = 0; i<dailyComplaintsModels.size() - 1; i=i+2) {
								
							DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
							DailyComplaintsModel dailyComplaintsModel2 = dailyComplaintsModels.get(i+1);
								
								if(dailyComplaintsModel1.getType().equals("current")) {
									
									comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
									comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
									tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
									tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
									
									received = comp_recveived + tw_comp_recveived;
									pending = comp_pending + tw_comp_pending;
									
									pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
									
									complaintsResolved = 100 - pending;
									
									// For All LPCD
									lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
									
								} else {
									
									comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
									comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
									tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
									tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
									
									received = comp_recveived + tw_comp_recveived;
									pending = comp_pending + tw_comp_pending;
									
									pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
									
									complaintsResolved = 100 - pending;
									
									// For All LPCD
									lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
								}
						}
					} else {
						
						for(int i = 0; i<dailyComplaintsModels.size() - 1; i++) {
							
							DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
							
							comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
							comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
							tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
							tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
							
							received = comp_recveived + tw_comp_recveived;
							pending = comp_pending + tw_comp_pending;
							
							pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
							
							complaintsResolved = 100 - pending;
							
							// For All LPCD
							lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
						}
					}
				}
				
				LinkedList<DailyComplaintsModel> mapDailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyComplaintsModels != null && mapDailyComplaintsModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyComplaintsModel>();

					for(int i = 0; i<mapDailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = mapDailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = mapDailyComplaintsModels.get(i+1);
						DailyComplaintsModel dailyComplaintsModel3 = new DailyComplaintsModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
						ULBModel ulbModel = new ULBModel();
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel2.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel2.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel2.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel2.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel1.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel1.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel1.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel1.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyComplaintsModel dailyComplaintsModel4 = lpcdMap.get(entry1.getKey());
					dailyComplaintsModel4.setComplaintsResolved(entry1.getValue());
					dailyComplaintsModel4.setCurrentMonth(currentMonth);
					dailyComplaintsModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyComplaintsModel4);
					
				}
				
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
								
				/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
				
				int n = 3;
				if(amrutTypeId == null ) {
					 
					 n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
				 }
				
				model.addAttribute("topValue", n);
				HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				 
				Set mapSet = (Set) top10LpcdUlbs.entrySet();
		        Iterator mapIterator = mapSet.iterator();
		        while (mapIterator.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
			        String keyValue = (String) mapEntry.getKey();
			        Double value = (Double) mapEntry.getValue();
			        
			        if(value >= 100) {
			        	
			        	top10ulb.add(keyValue);
				        top10lpcd.add(value);
			        }
			        
		        }
		        
				 model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
			     model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
			     
			     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
				
			     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
			     
			     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
			    
			    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
			     
		        LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
		        
		        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
		        Iterator mapIterator2 = mapSet2.iterator();
		        while (mapIterator2.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
			        String keyValue = (String) mapEntry.getKey();
			        Double value = (Double) mapEntry.getValue();
			        
			        if(value < 100) {
			        	
			        	bottom10ulb.add(keyValue);
			            bottom10lpcd.add(value);
			        }
		        }
		        
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
				
				/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
				
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				model.addAttribute("flag", false);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}

				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyComplaintsModel> dailyComplaintsModels2 = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, date, null);
					
					if(dailyComplaintsModels2 != null && dailyComplaintsModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getTotal_comp_resolved());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getPm_total_comp_resolved());
									
					}
				}
					
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
			} else if(flag == 2) {
				
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
				
				if ((dailyComplaintsModels != null) && (dailyComplaintsModels.size() > 0)) {
					
					total_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_recveived();
					total_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_pending();
					total_tw_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_recveived();
					total_tw_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_pending();
					
					totalReceived = total_comp_recveived + total_tw_comp_recveived;
					totalPending = total_comp_pending + total_tw_comp_pending;
					
					totalPending = (Math.round(((totalPending * 100)/totalReceived)*100.0)/100.0);
					
				}
				
				model.addAttribute("lpcdBoxValue", 100 - totalPending);
				
				if(dailyComplaintsModels != null && dailyComplaintsModels.size() > 0) {
					
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyComplaintsModels.size() > 2 && dailyComplaintsModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyComplaintsModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = dailyComplaintsModels.get(i+1);
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
							}
					}
				} else {
					
					for(int i = 0; i<dailyComplaintsModels.size() - 1; i++) {
						
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						
						comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
						comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
						tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
						tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
						
						received = comp_recveived + tw_comp_recveived;
						pending = comp_pending + tw_comp_pending;
						
						pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
						
						complaintsResolved = 100 - pending;
						
						// For All LPCD
						lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
					}
				}
			}
				
				LinkedList<DailyComplaintsModel> mapDailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyComplaintsModels != null && mapDailyComplaintsModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyComplaintsModel>();

					for(int i = 0; i<mapDailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = mapDailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = mapDailyComplaintsModels.get(i+1);
						DailyComplaintsModel dailyComplaintsModel3 = new DailyComplaintsModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
						ULBModel ulbModel = new ULBModel();
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel2.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel2.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel2.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel2.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel1.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel1.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel1.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel1.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyComplaintsModel dailyComplaintsModel4 = lpcdMap.get(entry1.getKey());
					dailyComplaintsModel4.setComplaintsResolved(entry1.getValue());
					dailyComplaintsModel4.setCurrentMonth(currentMonth);
					dailyComplaintsModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyComplaintsModel4);
					
				}
				
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
								
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
		        
		        model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
				model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
				
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
				
				
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				model.addAttribute("flag", true);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}

				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyComplaintsModel> dailyComplaintsModels2 = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, date, null);
					
					if(dailyComplaintsModels2 != null && dailyComplaintsModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getTotal_comp_resolved());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getPm_total_comp_resolved());
									
					}
				}
				
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
			} else if(flag == 3) {
				
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
				
				if ((dailyComplaintsModels != null) && (dailyComplaintsModels.size() > 0)) {
					
					total_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_recveived();
					total_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_pending();
					total_tw_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_recveived();
					total_tw_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_pending();
					
					totalReceived = total_comp_recveived + total_tw_comp_recveived;
					totalPending = total_comp_pending + total_tw_comp_pending;
					
					totalPending = (Math.round(((totalPending * 100)/totalReceived)*100.0)/100.0);
					
				}
				
				model.addAttribute("lpcdBoxValue", 100 - totalPending);
				
				if(dailyComplaintsModels != null && dailyComplaintsModels.size() > 0) {
					
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyComplaintsModels.size() > 2 && dailyComplaintsModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyComplaintsModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = dailyComplaintsModels.get(i+1);
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
							}
					}
				} else {
					
					for(int i = 0; i<dailyComplaintsModels.size() - 1; i++) {
						
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						
						comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
						comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
						tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
						tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
						
						received = comp_recveived + tw_comp_recveived;
						pending = comp_pending + tw_comp_pending;
						
						pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
						
						complaintsResolved = 100 - pending;
						
						// For All LPCD
						lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
					}
				}
			}
				
				LinkedList<DailyComplaintsModel> mapDailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyComplaintsModels != null && mapDailyComplaintsModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyComplaintsModel>();

					for(int i = 0; i<mapDailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = mapDailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = mapDailyComplaintsModels.get(i+1);
						DailyComplaintsModel dailyComplaintsModel3 = new DailyComplaintsModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
						ULBModel ulbModel = new ULBModel();
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel2.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel2.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel2.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel2.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel1.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel1.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel1.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel1.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyComplaintsModel dailyComplaintsModel4 = lpcdMap.get(entry1.getKey());
					dailyComplaintsModel4.setComplaintsResolved(entry1.getValue());
					dailyComplaintsModel4.setCurrentMonth(currentMonth);
					dailyComplaintsModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyComplaintsModel4);
					
				}
				
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				model.addAttribute("flag", true);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
			     
			     String month = null;
					String year = null;
					String startDate1 = null;
					String endDate1 = null;
					
					if(startDate != null) {
						
						month = startDate.substring(5, 7);
						year = startDate.substring(0, 4);
						
						if (month != null) {
							
							List<String> dates = null;
							try {
								dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
								startDate1 = dates.get(0);
								endDate1 = startDate;
							} catch (NumberFormatException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							} 
							
						}
					}

					datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
					
					for(String date:datesBetween) {
						
						LinkedList<DailyComplaintsModel> dailyComplaintsModels2 = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, date, null);
						
						if(dailyComplaintsModels2 != null && dailyComplaintsModels2.size() > 0) {
							
							// For Progress Report Current Value
							currentLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getTotal_comp_resolved());
							
							// For Progress Report Previous Value
							previousLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getPm_total_comp_resolved());
										
						}
					}
				
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
				
			} else if(flag == 4) {
				
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
				
				listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
				
				if ((dailyComplaintsModels != null) && (dailyComplaintsModels.size() > 0)) {
					
					total_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_recveived();
					total_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_pending();
					total_tw_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_recveived();
					total_tw_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_pending();
					
					totalReceived = total_comp_recveived + total_tw_comp_recveived;
					totalPending = total_comp_pending + total_tw_comp_pending;
					
					totalPending = (Math.round(((totalPending * 100)/totalReceived)*100.0)/100.0);
					
				}
				
				model.addAttribute("lpcdBoxValue", 100 - totalPending);
				
				if(dailyComplaintsModels != null && dailyComplaintsModels.size() > 0) {
					
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyComplaintsModels.size() > 2 && dailyComplaintsModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyComplaintsModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = dailyComplaintsModels.get(i+1);
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
							}
					}
				} else {
					
					for(int i = 0; i<dailyComplaintsModels.size() - 1; i++) {
						
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						
						comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
						comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
						tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
						tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
						
						received = comp_recveived + tw_comp_recveived;
						pending = comp_pending + tw_comp_pending;
						
						pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
						
						complaintsResolved = 100 - pending;
						
						// For All LPCD
						lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
					}
				}
			}
				
				LinkedList<DailyComplaintsModel> mapDailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyComplaintsModels != null && mapDailyComplaintsModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyComplaintsModel>();

					for(int i = 0; i<mapDailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = mapDailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = mapDailyComplaintsModels.get(i+1);
						DailyComplaintsModel dailyComplaintsModel3 = new DailyComplaintsModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
						ULBModel ulbModel = new ULBModel();
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel2.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel2.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel2.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel2.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel1.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel1.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel1.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel1.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyComplaintsModel dailyComplaintsModel4 = lpcdMap.get(entry1.getKey());
					dailyComplaintsModel4.setComplaintsResolved(entry1.getValue());
					dailyComplaintsModel4.setCurrentMonth(currentMonth);
					dailyComplaintsModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyComplaintsModel4);
					
				}
				
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
				model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));

				LinkedList<String> bottom10ulb = new LinkedList<String>();
				LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));

				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyComplaintsModel> dailyComplaintsModels2 = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, date, null);
					
					if(dailyComplaintsModels2 != null && dailyComplaintsModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getTotal_comp_resolved());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getPm_total_comp_resolved());
									
					}
				}
				
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
			}
			
			model.addAttribute("datesBetween", new ObjectMapper().writeValueAsString(datesBetween));
			model.addAttribute("currentLPCD", new ObjectMapper().writeValueAsString(currentLPCD));
			model.addAttribute("previousLPCD", new ObjectMapper().writeValueAsString(previousLPCD));
			
			/*for (Map.Entry<String, DailyWaterSupplyModel> entry : lpcdMap.entrySet()) {
				
			    System.out.println("ULB Name -- >> "+entry.getKey() + "Current LPCD --- >> " + entry.getValue().getRate_of_supply() + "Previous LPCD --- >> " + entry.getValue().getPm_rate_of_supply());
			}*/
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("complaintsPage -- END");
		}

		return "complaints-unresolved";

	}
	
	
	
	
	@RequestMapping(value = "/ajax-complaints-resolved", method = RequestMethod.GET)
	public @ResponseBody AjaxComplaintsModel ajaxComplaints(
			@ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxComplaints -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		AjaxComplaintsModel ajaxComplaintsModel = new AjaxComplaintsModel();
		
		try {
			
			
			dashboardValidation.dashboardValidation(listingModel, amrutTypeId, selectedYear, month);
			
		String startDate = null;
		//String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		String days = null;
		String year = null;
		String startDate1 = null;
		String endDate1 = null;
		String startDateMap = null;
		String endDateMap = null;
		String currentMonth = "";
		String previousMonth = "";
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxComplaintsModel.setPhDivisionModels(phDivisionModels);
					
					listingModel.setDistrictId(null);
					listingModel.setDivisionId(null);
					listingModel.setUlbId(null);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if (month != null) {
			
			List<String> dates = null;
			try {
				dates = dateCalculation.findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
				startDateMap = dates.get(0);
				endDateMap = dates.get(1);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(0);
			endDate = dates.get(1);
			days = dates.get(2);
			currentMonth = dates.get(3);
			previousMonth = dates.get(4);
			startDate1 = startDate;
			endDate1 = endDate;
			
		} else {
			
			startDate = listingModel.getDate();
			
			if(startDate != null) {
				
				month = startDate.substring(5, 7);
				year = startDate.substring(0, 4);
				
				if (month != null) {
					
					List<String> dates = null;
					try {
						dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
						startDate1 = dates.get(0);
						endDate1 = startDate;
						startDateMap = dates.get(0);
						endDateMap = dates.get(1);
						currentMonth = dates.get(3);
						previousMonth = dates.get(4);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					} 
					
				}
				
			}
		
		}
		
		LinkedHashMap<String, Double> lpcdTotal = null;
		LinkedHashMap<String, DailyComplaintsModel> lpcdMap = null;
		List<String> datesBetween = new LinkedList<String>();
		List<Double> currentLPCD = new LinkedList<Double>();
		List<Double> previousLPCD = new LinkedList<Double>();
		
		double total_comp_recveived = 0.0D;
		double total_comp_pending = 0.0D;
		double total_tw_comp_recveived = 0.0D;
		double total_tw_comp_pending = 0.0D;
		
		double totalReceived = 0.0D;
		double totalPending = 0.0D;
		
		double comp_recveived = 0.0D;
		double comp_pending = 0.0D;
		double tw_comp_recveived = 0.0D;
		double tw_comp_pending = 0.0D;
		double complaintsResolved = 0.0D;
		double received = 0.0D;
		double pending = 0.0D;
		
		double pm_comp_recveived = 0.0D;
		double pm_comp_pending = 0.0D;
		double pm_tw_comp_recveived = 0.0D;
		double pm_tw_comp_pending = 0.0D;
		double pm_complaintsResolved = 0.0D;
		double pm_received = 0.0D;
		double pm_pending = 0.0D;
		
		
		try {
			
			if(flag == 1) {
				
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
				
				LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
					
				if ((dailyComplaintsModels != null) && (dailyComplaintsModels.size() > 0)) {
					
					total_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_recveived();
					total_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_pending();
					total_tw_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_recveived();
					total_tw_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_pending();
					
					totalReceived = total_comp_recveived + total_tw_comp_recveived;
					totalPending = total_comp_pending + total_tw_comp_pending;
					
					totalPending = (Math.round(((totalPending * 100)/totalReceived)*100.0)/100.0);
					
				}
				
				if(dailyComplaintsModels != null && dailyComplaintsModels.size() > 0) {
					
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyComplaintsModels.size() > 2 && dailyComplaintsModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyComplaintsModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = dailyComplaintsModels.get(i+1);
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
							}
					}
				} else {
					
					for(int i = 0; i<dailyComplaintsModels.size() - 1; i++) {
						
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						
						comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
						comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
						tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
						tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
						
						received = comp_recveived + tw_comp_recveived;
						pending = comp_pending + tw_comp_pending;
						
						pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
						
						complaintsResolved = 100 - pending;
						
						// For All LPCD
						lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
					}
				}
			}
				
				LinkedList<DailyComplaintsModel> mapDailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyComplaintsModels != null && mapDailyComplaintsModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyComplaintsModel>();

					for(int i = 0; i<mapDailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = mapDailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = mapDailyComplaintsModels.get(i+1);
						DailyComplaintsModel dailyComplaintsModel3 = new DailyComplaintsModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
						ULBModel ulbModel = new ULBModel();
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel2.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel2.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel2.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel2.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel1.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel1.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel1.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel1.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyComplaintsModel dailyComplaintsModel4 = lpcdMap.get(entry1.getKey());
					dailyComplaintsModel4.setComplaintsResolved(entry1.getValue());
					dailyComplaintsModel4.setCurrentMonth(currentMonth);
					dailyComplaintsModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyComplaintsModel4);
					
				}
				
				ajaxComplaintsModel.setComplaintResolved(100 - totalPending);
				ajaxComplaintsModel.setLpcdMap(lpcdMap);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
		        
		        if((amrutTypeId == null) && (listingModel.getDivisionId() == null) && (listingModel.getDistrictId() == null) && (listingModel.getUlbId() == null)) {
				
		        	/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					int n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
					
					ajaxComplaintsModel.setTopValue(n);
					
					HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
					
					Set mapSet = (Set) top10LpcdUlbs.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd.add(value);
			        }
			        
			        ajaxComplaintsModel.setTop10ulb(top10ulb);
			        ajaxComplaintsModel.setTop10lpcd(top10lpcd);
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
				    
				    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd.add(value);
			        }
			        
			        ajaxComplaintsModel.setBottom10ulb(bottom10ulb);
			        ajaxComplaintsModel.setBottom10lpcd(bottom10lpcd);
			        ajaxComplaintsModel.setFlag(false);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
					
				} else {
					
					ajaxComplaintsModel.setFlag(true);
				}
					
					/* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
					
					LinkedList<String> topulb = new LinkedList<String>();
			        LinkedList<Double> toplpcd = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 100) {
			        		
			        		topulb.add(entry1.getKey());
			        		toplpcd.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb.add(entry1.getKey());
			        		bottomlpcd.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxComplaintsModel.setTopulb(topulb);
			        ajaxComplaintsModel.setToplpcd(toplpcd);
			        ajaxComplaintsModel.setBottomulb(bottomulb);
			        ajaxComplaintsModel.setBottomlpcd(bottomlpcd);
			        
					
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyComplaintsModel> dailyComplaintsModels2 = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, date, null);
					
					if(dailyComplaintsModels2 != null && dailyComplaintsModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getTotal_comp_resolved());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getPm_total_comp_resolved());
									
					}
				}
					
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
			} else if(flag == 2) {
				
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
				
				if ((dailyComplaintsModels != null) && (dailyComplaintsModels.size() > 0)) {
					
					total_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_recveived();
					total_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_pending();
					total_tw_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_recveived();
					total_tw_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_pending();
					
					totalReceived = total_comp_recveived + total_tw_comp_recveived;
					totalPending = total_comp_pending + total_tw_comp_pending;
					
					totalPending = (Math.round(((totalPending * 100)/totalReceived)*100.0)/100.0);
					
				}
				
				if(dailyComplaintsModels != null && dailyComplaintsModels.size() > 0) {
					
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyComplaintsModels.size() > 2 && dailyComplaintsModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyComplaintsModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = dailyComplaintsModels.get(i+1);
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
							}
					}
				} else {
					
					for(int i = 0; i<dailyComplaintsModels.size() - 1; i++) {
						
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						
						comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
						comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
						tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
						tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
						
						received = comp_recveived + tw_comp_recveived;
						pending = comp_pending + tw_comp_pending;
						
						pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
						
						complaintsResolved = 100 - pending;
						
						// For All LPCD
						lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
					}
				}
			}
				
				LinkedList<DailyComplaintsModel> mapDailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyComplaintsModels != null && mapDailyComplaintsModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyComplaintsModel>();

					for(int i = 0; i<mapDailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = mapDailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = mapDailyComplaintsModels.get(i+1);
						DailyComplaintsModel dailyComplaintsModel3 = new DailyComplaintsModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
						ULBModel ulbModel = new ULBModel();
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel2.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel2.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel2.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel2.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel1.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel1.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel1.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel1.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							}
					}
				}

				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyComplaintsModel dailyComplaintsModel4 = lpcdMap.get(entry1.getKey());
					dailyComplaintsModel4.setComplaintsResolved(entry1.getValue());
					dailyComplaintsModel4.setCurrentMonth(currentMonth);
					dailyComplaintsModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyComplaintsModel4);
					
				}
				
				ajaxComplaintsModel.setComplaintResolved(100 - totalPending);
				ajaxComplaintsModel.setLpcdMap(lpcdMap);
								
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        ajaxComplaintsModel.setTopulb(topulb);
		        ajaxComplaintsModel.setToplpcd(toplpcd);
		        ajaxComplaintsModel.setBottomulb(bottomulb);
		        ajaxComplaintsModel.setBottomlpcd(bottomlpcd);
		        ajaxComplaintsModel.setFlag(true);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyComplaintsModel> dailyComplaintsModels2 = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, date, null);
					
					if(dailyComplaintsModels2 != null && dailyComplaintsModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getTotal_comp_resolved());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getPm_total_comp_resolved());
									
					}
				}
				
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
			} else if(flag == 3) {
				
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
				
				if ((dailyComplaintsModels != null) && (dailyComplaintsModels.size() > 0)) {
					
					total_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_recveived();
					total_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_pending();
					total_tw_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_recveived();
					total_tw_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_pending();
					
					totalReceived = total_comp_recveived + total_tw_comp_recveived;
					totalPending = total_comp_pending + total_tw_comp_pending;
					
					totalPending = (Math.round(((totalPending * 100)/totalReceived)*100.0)/100.0);
					
				}
				
				if(dailyComplaintsModels != null && dailyComplaintsModels.size() > 0) {
					
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyComplaintsModels.size() > 2 && dailyComplaintsModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyComplaintsModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = dailyComplaintsModels.get(i+1);
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
							}
					}
				} else {
					
					for(int i = 0; i<dailyComplaintsModels.size() - 1; i++) {
						
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						
						comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
						comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
						tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
						tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
						
						received = comp_recveived + tw_comp_recveived;
						pending = comp_pending + tw_comp_pending;
						
						pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
						
						complaintsResolved = 100 - pending;
						
						// For All LPCD
						lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
					}
				}
			}
				
				LinkedList<DailyComplaintsModel> mapDailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyComplaintsModels != null && mapDailyComplaintsModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyComplaintsModel>();

					for(int i = 0; i<mapDailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = mapDailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = mapDailyComplaintsModels.get(i+1);
						DailyComplaintsModel dailyComplaintsModel3 = new DailyComplaintsModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
						ULBModel ulbModel = new ULBModel();
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel2.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel2.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel2.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel2.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel1.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel1.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel1.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel1.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyComplaintsModel dailyComplaintsModel4 = lpcdMap.get(entry1.getKey());
					dailyComplaintsModel4.setComplaintsResolved(entry1.getValue());
					dailyComplaintsModel4.setCurrentMonth(currentMonth);
					dailyComplaintsModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyComplaintsModel4);
					
				}
				
				ajaxComplaintsModel.setComplaintResolved(100 - totalPending);
				ajaxComplaintsModel.setLpcdMap(lpcdMap);
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        ajaxComplaintsModel.setTopulb(topulb);
		        ajaxComplaintsModel.setToplpcd(toplpcd);
		        ajaxComplaintsModel.setBottomulb(bottomulb);
		        ajaxComplaintsModel.setBottomlpcd(bottomlpcd);
		        ajaxComplaintsModel.setFlag(true);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
			     
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyComplaintsModel> dailyComplaintsModels2 = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, date, null);
					
					if(dailyComplaintsModels2 != null && dailyComplaintsModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getTotal_comp_resolved());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getPm_total_comp_resolved());
									
					}
				}
				
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
				
			} else if(flag == 4) {
				
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
				
				listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
				
				if ((dailyComplaintsModels != null) && (dailyComplaintsModels.size() > 0)) {
					
					total_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_recveived();
					total_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_pending();
					total_tw_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_recveived();
					total_tw_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_pending();
					
					totalReceived = total_comp_recveived + total_tw_comp_recveived;
					totalPending = total_comp_pending + total_tw_comp_pending;
					
					totalPending = (Math.round(((totalPending * 100)/totalReceived)*100.0)/100.0);
					
				}
				
				if(dailyComplaintsModels != null && dailyComplaintsModels.size() > 0) {
					
				lpcdTotal = new LinkedHashMap<String, Double>();
				
				if(dailyComplaintsModels.size() > 2 && dailyComplaintsModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyComplaintsModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {

					for(int i = 0; i<dailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = dailyComplaintsModels.get(i+1);
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								// For All LPCD
								lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
							}
					}
				} else {
					
					for(int i = 0; i<dailyComplaintsModels.size() - 1; i++) {
						
						DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
						
						comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
						comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
						tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
						tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
						
						received = comp_recveived + tw_comp_recveived;
						pending = comp_pending + tw_comp_pending;
						
						pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
						
						complaintsResolved = 100 - pending;
						
						// For All LPCD
						lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), complaintsResolved);
					}
				}
			}
				
				LinkedList<DailyComplaintsModel> mapDailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyComplaintsModels != null && mapDailyComplaintsModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyComplaintsModel>();

					for(int i = 0; i<mapDailyComplaintsModels.size() - 1; i=i+2) {
							
						DailyComplaintsModel dailyComplaintsModel1 = mapDailyComplaintsModels.get(i);
						DailyComplaintsModel dailyComplaintsModel2 = mapDailyComplaintsModels.get(i+1);
						DailyComplaintsModel dailyComplaintsModel3 = new DailyComplaintsModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
						ULBModel ulbModel = new ULBModel();
							
							if(dailyComplaintsModel1.getType().equals("current")) {
								
								comp_recveived = dailyComplaintsModel1.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel1.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel1.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel1.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel2.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel2.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel2.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel2.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							} else {
								
								comp_recveived = dailyComplaintsModel2.getNo_ws_comp_recv();
								comp_pending = dailyComplaintsModel2.getNo_ws_comp_pending();
								tw_comp_recveived = dailyComplaintsModel2.getNo_tw_comp_recv();
								tw_comp_pending = dailyComplaintsModel2.getNo_tw_comp_pending();
								
								received = comp_recveived + tw_comp_recveived;
								pending = comp_pending + tw_comp_pending;
								
								pending = (Math.round(((pending * 100)/received)*100.0)/100.0);
								
								complaintsResolved = 100 - pending;
								
								pm_comp_recveived = dailyComplaintsModel1.getPm_no_ws_comp_recv();
								pm_comp_pending = dailyComplaintsModel1.getPm_no_ws_comp_pending();
								pm_tw_comp_recveived = dailyComplaintsModel1.getPm_no_tw_comp_recv();
								pm_tw_comp_pending = dailyComplaintsModel1.getPm_no_tw_comp_pending();
								
								pm_received = pm_comp_recveived + pm_tw_comp_recveived;
								pm_pending = pm_comp_pending + pm_tw_comp_pending;
								
								pm_pending = (Math.round(((pm_pending * 100)/pm_received)*100.0)/100.0);
								
								pm_complaintsResolved = 100 - pm_pending;
							
								dailyComplaintsModel3.setTotal_comp_resolved(complaintsResolved);
								dailyComplaintsModel3.setPm_total_comp_resolved(pm_complaintsResolved);
								ulbModel.setLatitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLatitude());
								ulbModel.setLongitude(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getLongitude());
								ulbNameModel.setUlbModel(ulbModel);
								dailyComplaintsModel3.setUlbNameModel(ulbNameModel);
								
								// For Map
								lpcdMap.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyComplaintsModel3);
								
							}
					}
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyComplaintsModel dailyComplaintsModel4 = lpcdMap.get(entry1.getKey());
					dailyComplaintsModel4.setComplaintsResolved(entry1.getValue());
					dailyComplaintsModel4.setCurrentMonth(currentMonth);
					dailyComplaintsModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyComplaintsModel4);
					
				}
				
				ajaxComplaintsModel.setComplaintResolved(100 - totalPending);
				ajaxComplaintsModel.setLpcdMap(lpcdMap);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();

				LinkedList<String> bottom10ulb = new LinkedList<String>();
				LinkedList<Double> bottom10lpcd = new LinkedList<Double>();

				LinkedList<String> middleulb1 = new LinkedList<String>();
				LinkedList<Double> middlelpcd1 = new LinkedList<Double>();
				LinkedList<String> middleulb2 = new LinkedList<String>();
				LinkedList<Double> middlelpcd2 = new LinkedList<Double>();

				LinkedList<String> topulb = new LinkedList<String>();
				LinkedList<Double> toplpcd = new LinkedList<Double>();
				
				LinkedList<String> bottomulb = new LinkedList<String>();
				LinkedList<Double> bottomlpcd = new LinkedList<Double>();
				
				ajaxComplaintsModel.setTopulb(topulb);
		        ajaxComplaintsModel.setToplpcd(toplpcd);
		        
		        ajaxComplaintsModel.setMiddleulb1(middleulb1);
		        ajaxComplaintsModel.setMiddlelpcd1(middlelpcd1);
				
		        ajaxComplaintsModel.setMiddleulb2(middleulb2);
		        ajaxComplaintsModel.setMiddlelpcd2(middlelpcd2);
		        
		        ajaxComplaintsModel.setBottomulb(bottomulb);
		        ajaxComplaintsModel.setBottomlpcd(bottomlpcd);
		        
		        ajaxComplaintsModel.setTop10ulb(top10ulb);
		        ajaxComplaintsModel.setTop10lpcd(top10lpcd);
		        
		        ajaxComplaintsModel.setBottom10ulb(bottom10ulb);
		        ajaxComplaintsModel.setBottom10lpcd(bottom10lpcd);
		        
		        ajaxComplaintsModel.setFlag(false);
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyComplaintsModel> dailyComplaintsModels2 = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, date, null);
					
					if(dailyComplaintsModels2 != null && dailyComplaintsModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getTotal_comp_resolved());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyComplaintsModels2.get(dailyComplaintsModels2.size() - 1).getPm_total_comp_resolved());
									
					}
				}
				
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
			}
			
			ajaxComplaintsModel.setDatesBetween(datesBetween);
			ajaxComplaintsModel.setCurrentLPCD(currentLPCD);
			ajaxComplaintsModel.setPreviousLPCD(previousLPCD);
			
			/*for (Map.Entry<String, DailyWaterSupplyModel> entry : lpcdMap.entrySet()) {
				
			    System.out.println("ULB Name -- >> "+entry.getKey() + "Current LPCD --- >> " + entry.getValue().getRate_of_supply() + "Previous LPCD --- >> " + entry.getValue().getPm_rate_of_supply());
			}*/
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("ajaxComplaints -- END");
		}

		return ajaxComplaintsModel;

	}
	
}
