package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.DailyWaterChargesModel;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.service.DailyWaterChargesService;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;

@Controller
public class DailyWaterChargesController implements SaltTracker {

	private static Logger logger = Logger
			.getLogger(DailyWaterChargesController.class);

	@Autowired
	private DailyWaterChargesService dailyWaterChargesService;
	
	@Autowired
	private DivisionService divisionService;

	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private Encryption encryption;

	@RequestMapping(value = "/add-daily-water-charge", method = RequestMethod.GET)
	public String addDailyWaterChargePage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterChargePage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterChargePage -- END");
		}

		return "add-daily-water-charges";

	}

	@RequestMapping(value = "/add-daily-water-charge", method = RequestMethod.POST)
	public String addDailyWaterCharge(Model model, HttpServletRequest httpServletRequest,
			DailyWaterChargesModel dailyWaterChargesModel,BindingResult bindingResult,final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterCharge -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyWaterChargesModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			int result = dailyWaterChargesService
					.addDailyWaterCharge(dailyWaterChargesModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		}catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			
			redirectAttributes.addFlashAttribute("dailyWaterChargesModel", dailyWaterChargesModel);
			return ("redirect:/add-daily-water-charge");

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterCharge -- END");
		}

		return "redirect:/list-daily-water-charge";
	}

	@RequestMapping(value = "/list-daily-water-charge", method = RequestMethod.GET)
	public String fetchDailyWaterCharge(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterCharge -- START");
		}

		try {
			LinkedList<DailyWaterChargesModel> dailyWaterChargesModels = dailyWaterChargesService
					.fetchDailyWaterCharge(httpServletRequest);

			if (dailyWaterChargesModels.size() > 0) {

				model.addAttribute("dailyWaterChargesModels",
						dailyWaterChargesModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterCharge -- END");
		}

		return "list-daily-water-charge";

	}

	@RequestMapping(value = "/edit-daily-water-charge", method = RequestMethod.GET)
	public String editDailyWaterChargePage(@RequestParam String W_tax_id,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editDailyWaterChargePage -- START");
		}

		if (StringUtils.isBlank(W_tax_id)) {
			return "redirect:/error404";
		}

		/*if (!StringUtils.isNumeric(W_tax_id)) {
			return "error404";
		}*/
		DailyWaterChargesModel dailyWaterChargesModel = null;
		String W_tax_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			W_tax_id1 = encryption.decode(key, id);
			if(W_tax_id.length() > id.length())
				throw new Exception();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		
		try {
			dailyWaterChargesModel = dailyWaterChargesService
					.fetchDailyWaterChargeById(Integer.parseInt(W_tax_id1));
			
			if (dailyWaterChargesModel.getW_tax_id() != 0) {
				model.addAttribute("dailyWaterChargeModel",
						dailyWaterChargesModel);
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return "redirect:/list-daily-water-charge";
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(dailyWaterChargesModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editDailyWaterSupplyPage -- END");
		}

		return "edit-daily-water-charge";

	}
	
	@RequestMapping(value = "/delete-daily-water-charge", method = RequestMethod.POST)
	public String deleteDailyWaterChargePage(final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest, DailyWaterChargesModel dailyWaterChargesModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterChargePage -- START");
		}

		String W_tax_id = dailyWaterChargesModel.getW_taxId();
				
		if (StringUtils.isBlank(W_tax_id)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyWaterChargesModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		String W_tax_id1 = null;
		
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			W_tax_id1 = encryption.decode(key, W_tax_id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		try {
			int result = dailyWaterChargesService
					.deleteDailyWaterChargeById(Integer.parseInt(W_tax_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterChargePage -- END");
		}

		return "redirect:/list-daily-water-charge";

	}

	
	
	@RequestMapping(value = "/update-daily-water-charge", method = RequestMethod.POST)
	public String updateDailyWaterCharge(Model model, HttpServletRequest httpServletRequest, 
			@ModelAttribute DailyWaterChargesModel dailyWaterChargeModel,BindingResult bindingResult,final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterCharge -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyWaterChargeModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			String taxId = encryption.decode(key, dailyWaterChargeModel.getW_taxId());
			dailyWaterChargeModel.setW_tax_id(Integer.parseInt(taxId));
			int result = dailyWaterChargesService.updateDailyWaterCharge(dailyWaterChargeModel,  httpServletRequest);
			
			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		}catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute("dailyWaterChargeModel", dailyWaterChargeModel);

			return ("redirect:/edit-daily-water-charge?W_tax_id="+dailyWaterChargeModel.getW_taxId());

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       } else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterCharge -- END");
		}

		return "redirect:/list-daily-water-charge";

	}
	
	@RequestMapping(value = "/isApproved-daily-water-charge", method = RequestMethod.GET)
	public @ResponseBody String isApprovedUpdate(@RequestParam String W_tax_id,final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		if (StringUtils.isBlank(W_tax_id)) {
			return "redirect:/error404";
		}

		String W_tax_id1 = null;
		
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			W_tax_id1 = encryption.decode(key, id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			int result = dailyWaterChargesService
					.isApprovedUpdate(Integer.parseInt(W_tax_id1));

			if (result == 1) {

				System.out.println("Approved");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("Approved.done"));
			} else {
				System.out.println("Approved Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("Approved.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return "success";

	}
	
	
	@RequestMapping(value = "/isNotApproved-daily-water-charge", method = RequestMethod.GET)
	public @ResponseBody String isNotApprovedUpdate(@RequestParam String W_tax_id,final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}

		if (StringUtils.isBlank(W_tax_id)) {
			return "redirect:/error404";
		}

		String W_tax_id1 = null;
		
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			W_tax_id1 = encryption.decode(key, id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			int result = dailyWaterChargesService
					.isNotApprovedUpdate(Integer.parseInt(W_tax_id1));

			if (result == 1) {

				System.out.println("Approved");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("Approved.done"));
			} else {
				System.out.println("Approved Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("Approved.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return "success";

	}
	
	@RequestMapping(value = "/fetch-ajax-daily-water-charge", method = RequestMethod.GET)
	public String fetchAJAXDailyWaterCharges(HttpServletRequest httpServletRequest, @ModelAttribute ListingModel listingModel, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterCharges -- START");
		}
		
		LinkedList<DailyWaterChargesModel> dailyWaterChargesModels = null;

		try {
			
			dailyWaterChargesModels = dailyWaterChargesService.fetchAJAXDailyWaterCharges(listingModel);

			model.addAttribute("dailyWaterChargesModels",
					dailyWaterChargesModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterCharges -- END");
		}

		return "ajax-daily-water-charges";

	}
}
