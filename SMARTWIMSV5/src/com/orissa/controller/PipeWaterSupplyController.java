/**
 * @formatter:off
 *
 */
package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.orissa.common.DateCalculation;
import com.orissa.common.MessageUtil;
import com.orissa.common.SortingMap;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.service.DashboardService;
import com.orissa.service.DivisionService;

@Controller
public class PipeWaterSupplyController {

	private static Logger logger = Logger
			.getLogger(PipeWaterSupplyController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private SortingMap sortingMap;
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private DateCalculation dateCalculation;

	@RequestMapping(value = "/pipe-water-supply", method = RequestMethod.GET)
	public String PipeWaterSupplyPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("PipeWaterSupplyPage -- START");
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		String amrutTypeId = null;
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = null;
		model.addAttribute("startDate", startDate);
		
		try {
			
				if(flag == 1) {
				
	/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
					dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
			
	/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
	/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
				
	/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
	/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
				
				
	/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
	/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */

					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
					
	/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
		
			double total_qlty_supply = 0.0D;
			int total_pop_served = 0;
			double total_Freq_supply = 0.0D;
			double total_surplus_deficit = 0.0D;
			double total_rate_of_supply = 0.0D;
			double total_coverage_by_pws = 0.0D;
			double total_tot_demand = 0.0D;
			
			if ((dailyWaterSupplyModels != null) && (dailyWaterSupplyModels.size() > 0)) {

				total_qlty_supply = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_qlty_supply();
				total_pop_served = (dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_pop_served()).intValue();
				total_Freq_supply = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				total_surplus_deficit = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_surplus_deficit();
				total_rate_of_supply = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				total_coverage_by_pws = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_coverage_by_pws();
				total_tot_demand = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size()-1).getTotal_tot_demand();
				
				DailyWaterSupplyModel dailyWaterSupplyModel = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size()-1);
				
				dailyWaterSupplyModel.setTot_demand(total_tot_demand);
				dailyWaterSupplyModel.setQlty_supply(total_qlty_supply);
				dailyWaterSupplyModel.setPop_served(Double.valueOf(total_pop_served));
				dailyWaterSupplyModel.setFreq_supply(total_Freq_supply);
			}
			
			
			model.addAttribute("total_qlty_supply", total_qlty_supply);
			model.addAttribute("total_pop_served", total_pop_served);
			model.addAttribute("total_Freq_supply", total_Freq_supply);
			model.addAttribute("total_surplus_deficit", total_surplus_deficit);
			model.addAttribute("total_rate_of_supply", total_rate_of_supply);
			model.addAttribute("total_coverage_by_pws", total_coverage_by_pws);
			model.addAttribute("total_tot_demand", total_tot_demand);
				
			model.addAttribute("dailyWaterSupplyModels", dailyWaterSupplyModels);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("PipeWaterSupplyPage -- END");
		}

		return "pipe-water-supply";

	}
	
	@RequestMapping(value = "/ajax-pipe-water-supply", method = RequestMethod.GET)
	public String ajaxPipeWaterSupplyPage(Model model, @ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxPipeWaterSupplyPage -- START");
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		String startDate = null;
		String endDate = null;
		String days = null;
		String year = null;
		String startDate1 = null;
		String endDate1 = null;
		
		if (month != null) {
			
			List<String> dates = null;
			try {
				dates = dateCalculation.findDate(Integer.parseInt(selectedYear),
						Integer.valueOf(month));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(0);
			endDate = dates.get(1);
			days = dates.get(2);
			
			startDate1 = startDate;
			endDate1 = endDate;
			
		} else {
			
			startDate = listingModel.getDate();
			
			if(startDate != null) {
				
				month = startDate.substring(5, 7);
				year = startDate.substring(0, 4);
				
				if (month != null) {
					
					List<String> dates = null;
					try {
						dates = new DateCalculation().findDate(Integer.valueOf(year), Integer.valueOf(month));
						startDate1 = dates.get(0);
						endDate1 = startDate;
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					} 
					
				}
				
			}
		
		}
		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = null;
		model.addAttribute("startDate", startDate);
		
		try {
			
				if(flag == 1) {
				
	/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
					dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
			
	/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
	/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
				
	/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
	/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
				
				
	/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
	/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */

					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
					
	/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
		
			double total_qlty_supply = 0.0D;
			int total_pop_served = 0;
			double total_Freq_supply = 0.0D;
			double total_surplus_deficit = 0.0D;
			double total_rate_of_supply = 0.0D;
			double total_coverage_by_pws = 0.0D;
			double total_tot_demand = 0.0D;
			
			if ((dailyWaterSupplyModels != null) && (dailyWaterSupplyModels.size() > 0)) {

				total_qlty_supply = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_qlty_supply();
				total_pop_served = (dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_pop_served()).intValue();
				total_Freq_supply = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				total_surplus_deficit = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_surplus_deficit();
				total_rate_of_supply = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				total_coverage_by_pws = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_coverage_by_pws();
				total_tot_demand = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size()-1).getTotal_tot_demand();
				
				DailyWaterSupplyModel dailyWaterSupplyModel = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size()-1);
				
				dailyWaterSupplyModel.setTot_demand(total_tot_demand);
				dailyWaterSupplyModel.setQlty_supply(total_qlty_supply);
				dailyWaterSupplyModel.setPop_served(Double.valueOf(total_pop_served));
				dailyWaterSupplyModel.setFreq_supply(total_Freq_supply);
			}
			
			
			model.addAttribute("total_qlty_supply", total_qlty_supply);
			model.addAttribute("total_pop_served", total_pop_served);
			model.addAttribute("total_Freq_supply", total_Freq_supply);
			model.addAttribute("total_surplus_deficit", total_surplus_deficit);
			model.addAttribute("total_rate_of_supply", total_rate_of_supply);
			model.addAttribute("total_coverage_by_pws", total_coverage_by_pws);
			model.addAttribute("total_tot_demand", total_tot_demand);
				
			model.addAttribute("dailyWaterSupplyModels", dailyWaterSupplyModels);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("ajaxPipeWaterSupplyPage -- END");
		}

		return "ajax-pipe-water-supply";

	}
	
}
