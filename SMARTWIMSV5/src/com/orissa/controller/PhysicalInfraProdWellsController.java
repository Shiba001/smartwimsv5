package com.orissa.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraProductionWellsModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraProductionWellsService;
import com.orissa.validation.PhysicalInfraProductionWellsValidation;

@Controller
public class PhysicalInfraProdWellsController implements SaltTracker {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraProdWellsController.class);

	@Autowired
	private MessageUtil messageUtil;
	@Autowired
	private DivisionService divisionService;
	@Autowired
	PhysicalInfraProductionWellsService physicalInfraProductionWellsService;
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private PhysicalInfraProductionWellsValidation physicalInfraProductionWellsValidation;
	
	@Autowired
	private Encryption encryption;


	@RequestMapping(value = "/add-physical-infra-prod-wells", method = RequestMethod.GET)
	public String addPhysicalInfraProdWellsPage(ModelMap modelMap, @RequestParam(required=false) String ulb_id, HttpSession httpSession, HttpServletRequest httpServletRequest, @RequestParam(required=false) String date_of_report) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraProdWellsPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = "";
		Integer ulbId = null;
		
		try{
			
			if(trgt_id2 != null){
				String key = messageUtil.getBundle("secret.key"); 
				id = trgt_id2.split("=")[1]+"==";
				ulbId = Integer.parseInt(encryption.decode(key, id));
			}
			if(ulb_id != null){
				if(ulb_id.length() > id.length())
					throw new Exception();
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		
		Integer ulb_id1 = ulbId;
		
		if(ulb_id1 == null && httpSession.getAttribute("loginUsersModel2") != null){
			LoginUsersModel loginUsersModel = (LoginUsersModel) httpSession.getAttribute("loginUsersModel2");
			int userLevel = loginUsersModel.getUserLevelModel().getUser_level_id();
			modelMap.addAttribute("addreq", "true");
			if(userLevel == 4){
				ulb_id1 = loginUsersModel.getUlbNameModel().getUlb_id();
			}
		}
		if(ulb_id1 != null){
			List<PhysicalInfraProductionWellsModel> physicalInfraProductionWellsModels = physicalInfraProductionWellsService.fetchPhysicalInfraProductionWells(ulb_id1, date_of_report);
			modelMap.addAttribute("physicalInfraProductionWellsModels", physicalInfraProductionWellsModels);
			modelMap.addAttribute("ulb_id", ulbId);
		}
		
		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(ulb_id1);

			if (upperLevels.size() > 0) {
				modelMap.addAttribute("upperLevels", upperLevels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			//System.out.println("ulb_id is null.....");
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraProdWellsPage -- END");
		}

		return "add-physical-infra-prod-wells";

	}

	@RequestMapping(value = "/add-physical-infra-prod-wells", method = RequestMethod.POST)
	public String addPhysicalInfraProdWells(
			Model model,
			@ModelAttribute PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest httpServletRequest) throws JsonGenerationException, JsonMappingException, IOException {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraProdWells -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		List<PhysicalInfraProductionWellsModel> productionWellsModels = new ArrayList<PhysicalInfraProductionWellsModel>();
		//String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraProductionWellsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			/*String ulbId = encryption.decode(key, physicalInfraProductionWellsModel.getEnculbId());
			physicalInfraProductionWellsModel.setUlb_id(Integer.parseInt(ulbId));*/
			physicalInfraProductionWellsValidation.createPhysicalInfraProductionWellsValidation(physicalInfraProductionWellsModel);
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute(
					"physicalInfraProductionWellsModel",
					physicalInfraProductionWellsModel);
			if(physicalInfraProductionWellsModel.getUlb_id() == null){
				return ("redirect:/add-physical-infra-prod-wells");
			}
			else{
				return ("redirect:/add-physical-infra-prod-wells?ulb_id="+physicalInfraProductionWellsModel.getEnculbId());
			}
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int i = 0; i < physicalInfraProductionWellsModel.getList_depth()
				.size(); i++) {
			PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel2 = new PhysicalInfraProductionWellsModel();
			physicalInfraProductionWellsModel2
					.setPhyInfra_prod_well_slno(i + 1);
			physicalInfraProductionWellsModel2
					.setUlbId(physicalInfraProductionWellsModel.getUlbId());
			physicalInfraProductionWellsModel2.setDueDate(physicalInfraProductionWellsModel.getDueDate());
			physicalInfraProductionWellsModel2
					.setDate_of_report(physicalInfraProductionWellsModel
							.getDate_of_report());
			physicalInfraProductionWellsModel2
					.setLoc_of_prod_well(physicalInfraProductionWellsModel
							.getList_loc_of_prod_well().get(i));
			physicalInfraProductionWellsModel2
					.setDiam_prod_well(physicalInfraProductionWellsModel
							.getList_diam_prod_well().get(i));
			physicalInfraProductionWellsModel2
					.setDischarge(physicalInfraProductionWellsModel
							.getList_discharge().get(i));
			physicalInfraProductionWellsModel2
					.setDepth(physicalInfraProductionWellsModel.getList_depth()
							.get(i));
			productionWellsModels.add(physicalInfraProductionWellsModel2);
		}
		physicalInfraProductionWellsModel
				.setProductionWellsModels(productionWellsModels);
		
		try {
			int result = 0;
			if(physicalInfraProductionWellsModel.getUlb_id() == null){
				result = physicalInfraProductionWellsService.addPhysicalInfraProductionWells(physicalInfraProductionWellsModel, httpServletRequest);
			}
			else{
				result = physicalInfraProductionWellsService.updatePhysicalInfraProductionWells(physicalInfraProductionWellsModel, httpServletRequest);
			}
			

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute(
					"physicalInfraProductionWellsModel",
					physicalInfraProductionWellsModel);
			return ("redirect:/list-physical-infra-prod-wells");
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraProdWells -- END");
		}

		return ("redirect:/list-physical-infra-prod-wells");

	}

	@RequestMapping(value = "/list-physical-infra-prod-wells", method = RequestMethod.GET)
	public String fetchPhysicalInfraProdWells(@ModelAttribute ListingModel listingModel, HttpSession httpSession, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraProdWells -- START");
		}

		try {
			if(httpSession.getAttribute("loginUsersModel2") != null){
				LoginUsersModel loginUsersModel = (LoginUsersModel) httpSession.getAttribute("loginUsersModel2");
				int userLevel = loginUsersModel.getUserLevelModel().getUser_level_id();
				if(userLevel == 2){
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				}
				else if(userLevel == 3){
					listingModel.setDivisionId(0);
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				}
				else if(userLevel == 4){
					listingModel.setDivisionId(0);
					listingModel.setDistrictId(0);
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				}
			}
			LinkedList<ULBNameModel>  ulbNameModels= physicalInfraProductionWellsService.fetchUlbS(listingModel);

			if (ulbNameModels.size() > 0) {

				model.addAttribute("ulbNameModels", ulbNameModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraProdWells -- END");
		}

		return "list-physical-infra-prod-wells";

	}

	/*@RequestMapping(value = "/edit-physical-infra-prod-wells", method = RequestMethod.GET)
	public String editPhysicalInfraProdWellsPage(
			@RequestParam String phyInfra_prod_well_slno, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraProdWellsPage -- START");
		}

		if (StringUtils.isBlank(phyInfra_prod_well_slno)) {
			return "error404";
		}

		if (!StringUtils.isNumeric(phyInfra_prod_well_slno)) {
			return "error404";
		}

		

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraProdWellsPage -- END");
		}

		return "edit-physical-infra-prod-wells";

	}*/

	/*@RequestMapping(value = "/delete-physical-infra-prod-wells", method = RequestMethod.GET)
	public String deletePhysicalInfraProdWellsPage(
			@RequestParam String phyInfra_prod_well_slno, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraProdWellsPage -- START");
		}

		if (StringUtils.isBlank(phyInfra_prod_well_slno)) {
			return "error404";
		}

		if (!StringUtils.isNumeric(phyInfra_prod_well_slno)) {
			return "error404";
		}

		

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraProdWellsPage -- END");
		}

		return "redirect:/list-physical-infra-prod-wells";

	}*/

	/*@RequestMapping(value = "/update-physical-infra-prod-wells", method = RequestMethod.POST)
	public String updatePhysicalInfraProdWells(
			Model model,
			PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraProdWells -- START");
		}

		

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraProdWells -- END");
		}

		return "redirect:/list-physical-infra-prod-wells";

	}*/
	
	@RequestMapping(value = "/fetch-ajax-physical-infra-prod-wells", method = RequestMethod.GET)
	public String fetchAjaxPhysicalInfraProdWells(@ModelAttribute ListingModel listingModel, HttpSession httpSession, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAjaxPhysicalInfraProdWells -- START");
		}

		try {
			
			LinkedList<ULBNameModel>  ulbNameModels= physicalInfraProductionWellsService.fetchUlbS(listingModel);

			if (ulbNameModels.size() > 0) {

				model.addAttribute("ulbNameModels", ulbNameModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAjaxPhysicalInfraProdWells -- END");
		}

		return "ajax-physical-infra-prod-wells";

	}
}
