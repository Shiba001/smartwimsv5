package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraLengthOfPipeModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraPipelineService;

@Controller
public class PhysicalInfraPipelineController implements SaltTracker {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraPipelineController.class);

	@Autowired
	private PhysicalInfraPipelineService physicalInfraPipelineService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private DistrictService districtService;

	@Autowired
	private DivisionService divisionService;
	
	@Autowired
	private Encryption encryption;

	@RequestMapping(value = "/add-physical-infra-pipe", method = RequestMethod.GET)
	public String addPhysicalInfraPipePage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPipePage -- START");
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPipePage -- END");
		}

		return "add-physical-infra-pipe";

	}

	@RequestMapping(value = "/add-physical-infra-pipe", method = RequestMethod.POST)
	public String addPhysicalInfraPipe(Model model,
			@ModelAttribute PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel,BindingResult bindingResult,
			HttpServletRequest httpServletRequest,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPipe -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraLengthOfPipeModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			int result = physicalInfraPipelineService.addPhysicalInfraPipeline(
					physicalInfraLengthOfPipeModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute(
					"physicalInfraLengthOfPipeModel",
					physicalInfraLengthOfPipeModel);
			return ("redirect:/add-physical-infra-pipe");
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPipe -- END");
		}

		return ("redirect:/list-physical-infra-pipe");
	}

	@RequestMapping(value = "/list-physical-infra-pipe", method = RequestMethod.GET)
	public String fetchPhysicalInfraPipe(HttpServletRequest httpServletRequest, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraPipe -- START");
		}

		try {
			LinkedList<PhysicalInfraLengthOfPipeModel> physicalInfraLengthOfPipeModels = physicalInfraPipelineService
					.fetchPhysicalInfraPipeline(httpServletRequest);

			System.out.println(physicalInfraLengthOfPipeModels.size());
			
			System.out.println(physicalInfraLengthOfPipeModels);
			
			
			
			if (physicalInfraLengthOfPipeModels.size() > 0) {

				model.addAttribute("physicalInfraLengthOfPipeModels",
						physicalInfraLengthOfPipeModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraPipe -- END");
		}

		return "list-physical-infra-pipe";

	}

	@RequestMapping(value = "/edit-physical-infra-pipe", method = RequestMethod.GET)
	public String editPhysicalInfraPipePage(@RequestParam String phyInfra_lpid,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraPipePage -- START");
		}

		if (StringUtils.isBlank(phyInfra_lpid)) {
			return "redirect:/error404";
		}

		PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel = null;
		String phyInfra_lpid1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		
		try{
			String key = messageUtil.getBundle("secret.key"); 
			
			phyInfra_lpid1 = encryption.decode(key, id);
			if(phyInfra_lpid.length() > id.length())
				throw new Exception();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			physicalInfraLengthOfPipeModel = physicalInfraPipelineService
					.fetchPhysicalInfraPipeById(Integer.parseInt(phyInfra_lpid1));

			if (physicalInfraLengthOfPipeModel.getPhyInfra_lpid() != 0) {
				model.addAttribute("physicalInfraLengthOfPipeModel",
						physicalInfraLengthOfPipeModel);
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return "redirect:/list-physical-infra-pipe";
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(physicalInfraLengthOfPipeModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraPipePage -- END");
		}

		return "edit-physical-infra-pipe";

	}

	@RequestMapping(value = "/delete-physical-infra-pipe", method = RequestMethod.POST)
	public String deletePhysicalInfraPipePage(
			Model model, final RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest, PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraPipePage -- START");
		}

		String phyInfra_lpid = physicalInfraLengthOfPipeModel.getPhyInfraLpid();
				
		if (StringUtils.isBlank(phyInfra_lpid)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraLengthOfPipeModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		String phyInfra_lpid1 = null;
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		
		try{
			String key = messageUtil.getBundle("secret.key"); 
			
			phyInfra_lpid1 = encryption.decode(key, phyInfra_lpid);
			/*if(phyInfra_lpid.length() > id.length())
				throw new Exception();*/
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			int result = physicalInfraPipelineService
					.deletePhysicalInfraPipeById(Integer
							.parseInt(phyInfra_lpid1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else {
			
			throw new ServletException(messageUtil.getBundle("CSRF"));

		}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraPipePage -- END");
		}

		return "redirect:/list-physical-infra-pipe";

	}

	@RequestMapping(value = "/update-physical-infra-pipe", method = RequestMethod.POST)
	public String updatePhysicalInfraWaterPipe(Model model,
			@ModelAttribute PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel,BindingResult bindingResult,
			HttpServletRequest httpServletRequest,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraWaterPipe -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraLengthOfPipeModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			String phyInfraLpid = encryption.decode(key, physicalInfraLengthOfPipeModel.getPhyInfraLpid());
			physicalInfraLengthOfPipeModel.setPhyInfra_lpid(Integer.parseInt(phyInfraLpid));
			int result = physicalInfraPipelineService
					.updatePhysicalInfraWaterPipe(
							physicalInfraLengthOfPipeModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute(
					"physicalInfraLengthOfPipeModel",
					physicalInfraLengthOfPipeModel);
			return ("redirect:/edit-physical-infra-pipe?phyInfra_lpid=" + physicalInfraLengthOfPipeModel.getPhyInfraLpid());
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraWaterPipe -- END");
		}

		return "redirect:/list-physical-infra-pipe";

	}
	
	
	@RequestMapping(value = "/fetch-ajax-physical-infra-pipe", method = RequestMethod.GET)
	public String fetchAJAXDailyWaterSupply(HttpServletRequest httpServletRequest, @ModelAttribute ListingModel listingModel, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraPipe -- START");
		}
		LinkedList<PhysicalInfraLengthOfPipeModel> physicalInfraLengthOfPipeModel = null;

		try {
			physicalInfraLengthOfPipeModel = physicalInfraPipelineService.fetchAJAXPhysicalInfraPipe(listingModel);

			model.addAttribute("physicalInfraLengthOfPipeModels", physicalInfraLengthOfPipeModel);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraPipe -- END");
		}

		return "ajax-physical-infra-pipe";

	}
	
}
