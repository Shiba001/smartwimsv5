package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraTubewellsModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraTubewellService;

@Controller
public class PhysicalInfraTubewellController implements SaltTracker {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraTubewellController.class);

	@Autowired
	private PhysicalInfraTubewellService physicalInfraTubewellService;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DivisionService divisionService;

	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private Encryption encryption;


	@RequestMapping(value = "/add-physical-infra-tubewell", method = RequestMethod.GET)
	public String addPhysicalInfraTubewellPage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTubewell -- START");
		}
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTubewell -- END");
		}

		return "add-physical-infra-tubewell";

	}

	@RequestMapping(value = "/add-physical-infra-tubewell", method = RequestMethod.POST)
	public String addPhysicalInfraTubewells(Model model,
			@ModelAttribute PhysicalInfraTubewellsModel physicalInfraTubewellsModel,BindingResult bindingResult, HttpServletRequest httpServletRequest,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTubewell -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraTubewellsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			int result = physicalInfraTubewellService
					.addPhysicalInfraTubewell(physicalInfraTubewellsModel,httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		}catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute("physicalInfraTubewellsModel", physicalInfraTubewellsModel);
			return ("redirect:/add-physical-infra-tubewell");

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTubewell -- END");
		}

		return "redirect:/list-physical-infra-tubewell";

	}

	@RequestMapping(value = "/list-physical-infra-tubewell", method = RequestMethod.GET)
	public String fetchPhysicalInfraTubewell(HttpServletRequest httpServletRequest,Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTubewell -- START");
		}

		try {
			LinkedList<PhysicalInfraTubewellsModel> physicalInfraTubewellsModels = physicalInfraTubewellService
					.fetchPhysicalInfraTubewells(httpServletRequest);

			if (physicalInfraTubewellsModels.size() > 0) {

				model.addAttribute("physicalInfraTubewellsModels",
						physicalInfraTubewellsModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTubewell -- END");
		}

		return "list-physical-infra-tubewell";

	}

	@RequestMapping(value = "/edit-physical-infra-tubewell", method = RequestMethod.GET)
	public String editPhysicalInfraTubewellPage(
			@RequestParam String phyInfra_tw_id, Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraTubewell -- START");
		}

		if (StringUtils.isBlank(phyInfra_tw_id)) {
			return "redirect:/error404";
		}

			PhysicalInfraTubewellsModel physicalInfraTubewellsModel = null;
			String trgt_id2 = httpServletRequest.getQueryString();
			String id = trgt_id2.split("=")[1]+"==";
			String phyInfra_tw_id1 = null;
			try{
				
				String key = messageUtil.getBundle("secret.key"); 				
				phyInfra_tw_id1 = encryption.decode(key, id);
				if(phyInfra_tw_id.length() > id.length())
					throw new Exception();
			} catch (Exception e) {
				e.printStackTrace();
				return "redirect:/error404";
			}

		try {
			physicalInfraTubewellsModel = physicalInfraTubewellService
					.fetchPhysicalInfraTubewellsById(Integer
							.parseInt(phyInfra_tw_id1));

			if (physicalInfraTubewellsModel.getPhyInfra_tw_id() != 0) {
				model.addAttribute("physicalInfraTubewellsModel",
						physicalInfraTubewellsModel);
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return "redirect:/list-physical-infra-tubewell";
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(physicalInfraTubewellsModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraTubewell -- END");
		}

		return "edit-physical-infra-tubewell";

	}
	
	@RequestMapping(value = "/delete-physical-infra-tubewell", method = RequestMethod.POST)
	public String deletePhysicalInfraTubewellsPage(final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest, PhysicalInfraTubewellsModel phyInfraTubewellsModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraTubewellsPage -- START");
		}

		String phyInfra_tw_id = phyInfraTubewellsModel.getPhyInfraTwId();
		
		if (StringUtils.isBlank(phyInfra_tw_id)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = phyInfraTubewellsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		String phyInfra_tw_id1 = null;
		try{
			
			String key = messageUtil.getBundle("secret.key"); 				
			phyInfra_tw_id1 = encryption.decode(key, phyInfra_tw_id);
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			int result = physicalInfraTubewellService
					.deletePhysicalInfraTubewellsById(Integer.parseInt(phyInfra_tw_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else {
			
			throw new ServletException(messageUtil.getBundle("CSRF"));

		}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraTubewellsPage -- END");
		}

		return "redirect:/list-physical-infra-tubewell";

	}
	
	@RequestMapping(value = "/update-physical-infra-tubewell", method = RequestMethod.POST)
	public String updatePhysicalInfraTubewell(Model model,
			@ModelAttribute PhysicalInfraTubewellsModel physicalInfraTubewellsModel,BindingResult bindingResult, HttpServletRequest httpServletRequest,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraTubewell -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraTubewellsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			String phyInfraTwId = encryption.decode(key, physicalInfraTubewellsModel.getPhyInfraTwId());
			physicalInfraTubewellsModel.setPhyInfra_tw_id(Integer.parseInt(phyInfraTwId));
			int result = physicalInfraTubewellService
					.updatePhysicalInfraTubewells(physicalInfraTubewellsModel,httpServletRequest);

			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		}catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			
			redirectAttributes.addFlashAttribute("physicalInfraTubewellsModel", physicalInfraTubewellsModel);
			return ("redirect:/edit-physical-infra-tubewell?phyInfra_tw_id="+physicalInfraTubewellsModel.getPhyInfraTwId());

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraTubewell -- END");
		}

		return "redirect:/list-physical-infra-tubewell";

	}
	@RequestMapping(value = "/fetch-ajax-physical-infra-tubewell", method = RequestMethod.GET)
	public String fetchAJAXPhysicalInfraTubewells(HttpServletRequest httpServletRequest, @ModelAttribute ListingModel listingModel, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraTubewells -- START");
		}
		LinkedList<PhysicalInfraTubewellsModel> physicalInfraTubewellsModels = null;

		try {
			physicalInfraTubewellsModels = physicalInfraTubewellService.fetchAJAXPhysicalInfraTubewells(listingModel);

			model.addAttribute("physicalInfraTubewellsModels", physicalInfraTubewellsModels);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraTubewells -- END");
		}

		return "ajax-physical-infra-tubewell";

	}
}
