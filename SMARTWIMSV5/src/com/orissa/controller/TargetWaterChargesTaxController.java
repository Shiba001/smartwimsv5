/**
 * @formatter:off
 *
 */
package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.TargetModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.TargetWaterChargesTaxService;

@Controller
public class TargetWaterChargesTaxController implements SaltTracker {

	private static Logger logger = Logger
			.getLogger(TargetWaterChargesTaxController.class);

	@Autowired
	private TargetWaterChargesTaxService targetWaterChargesTaxService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private DistrictService districtService;

	@Autowired
	private DivisionService divisionService;
	
	@Autowired
	private Encryption encryption;

	@RequestMapping(value = "/add-target-water-charges-tax", method = RequestMethod.GET)
	public String addTargetWaterChargesTax(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addTargetWaterChargesTax -- START");
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("addTargetWaterChargesTax -- END");
		}

		return "add-target-water-charges-tax";

	}

	@RequestMapping(value = "/add-target-water-charges-tax", method = RequestMethod.POST)
	public String addTargetWaterChargesTax(Model model,
			@ModelAttribute TargetModel targetModel,BindingResult bindingResult,
			HttpServletRequest httpServletRequest,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("addTargetWaterChargesTax -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = targetModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			int result = targetWaterChargesTaxService.addTargetWaterChargesTax(
					targetModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

			redirectAttributes.addFlashAttribute("targetModel", targetModel);
			return ("redirect:/add-target-water-charges-tax");

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("addTargetWaterChargesTax -- END");
		}

		return "redirect:/list-target-water-charges";

	}

	@RequestMapping(value = "/list-target-water-charges", method = RequestMethod.GET)
	public String fetchTargetWaterCharges(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterCharges -- START");
		}

		try {
			LinkedList<TargetModel> targetModels = targetWaterChargesTaxService
					.fetchTargetWaterChargesTax(httpServletRequest);

			if (targetModels.size() > 0) {

				model.addAttribute("targetModels", targetModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterCharges -- END");
		}

		return "list-target-water-charges";

	}

	@RequestMapping(value = "/edit-target-water-charges", method = RequestMethod.GET)
	public String editTargetWaterChargesTax(@RequestParam String trgt_id,
			Model model, HttpServletRequest request) {

		if (logger.isInfoEnabled()) {
			logger.info("editTargetWaterChargesTax -- START");
		}

		if (StringUtils.isBlank(trgt_id)) {
			return "redirect:/error404";
		}

		TargetModel targetModel = null;
		String trgt_id1 = null;
		String trgt_id2 = request.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			trgt_id1 = encryption.decode(key, id);
			if(trgt_id.length() > id.length())
				throw new Exception();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			targetModel = targetWaterChargesTaxService
					.fetchTargetWaterChargesTaxById(Integer.parseInt(trgt_id1));

			if (targetModel.getTrgt_id() != 0) {
				model.addAttribute("targetModel", targetModel);
				model.addAttribute("targetMode", targetModel.getTrgt_mode());
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return "redirect:/list-target-water-charges";
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(targetModel.getUlbNameModel()
							.getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editTargetWaterChargesTax -- END");
		}

		return "edit-target-water-charges";

	}

	@RequestMapping(value = "/delete-target-water-charges", method = RequestMethod.POST)
	public String deleteTargetWaterChargesTax(
			final RedirectAttributes redirectAttributes, Model model, HttpServletRequest httpServletRequest, TargetModel targetModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deleteTargetWaterChargesTax -- START");
		}

		String trgt_id = targetModel.getTrgtId();
		
		if (StringUtils.isBlank(trgt_id)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = targetModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		String trgt_id1 = null;
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		try{
			
			String key = messageUtil.getBundle("secret.key"); 			
			trgt_id1 = encryption.decode(key, trgt_id);
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		} 
		
		try {
			int result = targetWaterChargesTaxService
					.deleteTargetWaterChargesTaxById(Integer.parseInt(trgt_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else {
			
			throw new ServletException(messageUtil.getBundle("CSRF"));

		}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}

		if (logger.isInfoEnabled()) {
			logger.info("deleteTargetWaterChargesTax -- END");
		}

		return "redirect:/list-target-water-charges";

	}

	@RequestMapping(value = "/update-target-water-charges", method = RequestMethod.POST)
	public String updateTargetWaterCharges(Model model,
			HttpServletRequest httpServletRequest,
			@ModelAttribute TargetModel targetModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updateTargetWaterCharges -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = targetModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			String trgtId = encryption.decode(key, targetModel.getTrgtId());
			targetModel.setTrgt_id(Integer.parseInt(trgtId));
			int result = targetWaterChargesTaxService
					.updateTargetWaterChargesTax(targetModel,
							httpServletRequest);

			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

			redirectAttributes.addFlashAttribute("targetModel", targetModel);
			
			return ("redirect:/edit-target-water-charges?trgt_id=" + targetModel.getTrgtId());

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("updateTargetWaterCharges -- END");
		}

		return "redirect:/list-target-water-charges";

	}
	
	
	@RequestMapping(value = "/fetch-ajax-target-water-charges", method = RequestMethod.GET)
	public String fetchAJAXTargetWaterChargesTax(HttpServletRequest httpServletRequest, @ModelAttribute ListingModel listingModel, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXTargetWaterChargesTax -- START");
		}
		LinkedList<TargetModel> targetModels = null;

		try {
			targetModels = targetWaterChargesTaxService.fetchAJAXTargetWaterChargesTax(listingModel);

			model.addAttribute("targetModels", targetModels);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXTargetWaterChargesTax -- END");
		}

		return "ajax-target-water-charges-tax";

	}

}
