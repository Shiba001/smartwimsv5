package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraPumpsAndMotorsModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraPumpMotorService;
import com.orissa.service.ULBNameService;
import com.orissa.validation.PhysicalInfraPumpMotorValidation;

@Controller
public class PhysicalInfraPumpMotorController implements SaltTracker {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraPumpMotorController.class);
	
	@Autowired
	private PhysicalInfraPumpMotorService physicalInfraPumpMotorService;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DivisionService divisionService;
	
	@Autowired
	private ULBNameService ulbNameService;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private PhysicalInfraPumpMotorValidation physicalInfraPumpMotorValidation;
	
	@Autowired
	private Encryption encryption;
	
	@RequestMapping(value = "/add-physical-infra-pump-and-motor", method = RequestMethod.GET)
	public String addPhysicalInfraPumpAndMotorPage(final ModelMap modelMap, @RequestParam(required=false) String ulb_id, HttpSession httpSession,HttpServletRequest httpServletRequest, @RequestParam(required=false) String date_of_report) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPumpAndMotorPage -- START");
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = "";
		Integer ulbId = null;
		
		try{
			
			if(trgt_id2 != null){
				String key = messageUtil.getBundle("secret.key"); 
				id = trgt_id2.split("=")[1]+"==";
				ulbId = Integer.parseInt(encryption.decode(key, id));
			}
			if(ulb_id != null){
				if(ulb_id.length() > id.length())
					throw new Exception();
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		
		Integer ulb_id1 = ulbId;
		
		if(ulb_id1 == null && httpSession.getAttribute("loginUsersModel2") != null){
			LoginUsersModel loginUsersModel = (LoginUsersModel) httpSession.getAttribute("loginUsersModel2");
			int userLevel = loginUsersModel.getUserLevelModel().getUser_level_id();
			modelMap.addAttribute("addreq", "true");
			if(userLevel == 4){
				ulb_id1 = loginUsersModel.getUlbNameModel().getUlb_id();
			}
		}
		if(ulb_id1 != null){
			List<PhysicalInfraPumpsAndMotorsModel> physicalInfraPumpsAndMotorsModels = physicalInfraPumpMotorService.fetchPumpAndMotor(ulb_id1, date_of_report);
			modelMap.addAttribute("physicalInfraPumpsAndMotorsModels", physicalInfraPumpsAndMotorsModels);
			modelMap.addAttribute("ulb_id", ulbId);
		}
		
		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(ulb_id1);

			if (upperLevels.size() > 0) {
				modelMap.addAttribute("upperLevels", upperLevels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			System.out.println("ulb_id is null.....");
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPumpAndMotorPage -- END");
		}

		return "add-physical-infra-pump-and-motor";

	}
	
	@RequestMapping(value = "/add-physical-infra-pump-and-motor", method = RequestMethod.POST)
	public String addPhysicalInfraPumpAndMotor(Model model,
			@ModelAttribute PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel,BindingResult bindingResult,HttpServletRequest httpServletRequest, final RedirectAttributes redirectAttributes) {
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPumpAndMotor -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		List<PhysicalInfraPumpsAndMotorsModel> physicalInfraPumpsAndMotorsModels = new LinkedList<PhysicalInfraPumpsAndMotorsModel>();
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraPumpsAndMotorsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			/*String ulbId = encryption.decode(key, physicalInfraPumpsAndMotorsModel.getEnculbId());
			physicalInfraPumpsAndMotorsModel.setUlb_id(Integer.parseInt(ulbId));*/
			physicalInfraPumpMotorValidation.createPhysicalInfraPumpsAndMotorsValidation(physicalInfraPumpsAndMotorsModel);
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute(
					"physicalInfraPumpsAndMotorsModel",
					physicalInfraPumpsAndMotorsModel);
			
			if(physicalInfraPumpsAndMotorsModel.getUlb_id() == null){
				return ("redirect:/add-physical-infra-pump-and-motor");
			}
			else{
				return ("redirect:/add-physical-infra-pump-and-motor?ulb_id="+physicalInfraPumpsAndMotorsModel.getEnculbId());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int i=0; i< physicalInfraPumpsAndMotorsModel.getList_cap_of_motor().size(); i++) {
			
			PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel2 = new PhysicalInfraPumpsAndMotorsModel();
			
			physicalInfraPumpsAndMotorsModel2.setPhyInfra_pump_motor_slno(i+1);
			physicalInfraPumpsAndMotorsModel2.setUlbId(physicalInfraPumpsAndMotorsModel.getUlbId());
			physicalInfraPumpsAndMotorsModel2.setDueDate(physicalInfraPumpsAndMotorsModel.getDueDate());
			physicalInfraPumpsAndMotorsModel2.setDate_of_report(physicalInfraPumpsAndMotorsModel.getDate_of_report());
			physicalInfraPumpsAndMotorsModel2.setLoc_of_pum_motor(physicalInfraPumpsAndMotorsModel.getList_loc_of_pum_motor().get(i));
			physicalInfraPumpsAndMotorsModel2.setCap_of_motor(physicalInfraPumpsAndMotorsModel.getList_cap_of_motor().get(i));
			physicalInfraPumpsAndMotorsModel2.setHead_dia(physicalInfraPumpsAndMotorsModel.getList_head_dia().get(i));
			physicalInfraPumpsAndMotorsModel2.setDischarge(physicalInfraPumpsAndMotorsModel.getList_discharge().get(i));
			physicalInfraPumpsAndMotorsModel2.setType_pump(physicalInfraPumpsAndMotorsModel.getList_type_pump().get(i));
			physicalInfraPumpsAndMotorsModel2.setYear_inst(physicalInfraPumpsAndMotorsModel.getList_year_inst().get(i));
			
			physicalInfraPumpsAndMotorsModels.add(physicalInfraPumpsAndMotorsModel2);
			
			
		}
		
		physicalInfraPumpsAndMotorsModel.setPumpsAndMotorsModels(physicalInfraPumpsAndMotorsModels);
		
		try {

			int result = 0;
			if(physicalInfraPumpsAndMotorsModel.getUlb_id() == null){
				result = physicalInfraPumpMotorService.addPumpAndMotor(physicalInfraPumpsAndMotorsModel, httpServletRequest);
			}
			else{
				result = physicalInfraPumpMotorService.updatePumpAndMotor(physicalInfraPumpsAndMotorsModel, httpServletRequest);
			}
			
			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute(
					"physicalInfraPumpsAndMotorsModel",
					physicalInfraPumpsAndMotorsModel);
			
			return ("redirect:/add-physical-infra-pump-and-motor");
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPumpAndMotor -- END");
		}
		
		return "redirect:/list-physical-infra-pump-and-motor";
		
	}
	
	
	@RequestMapping(value = "/list-physical-infra-pump-and-motor", method = RequestMethod.GET)
	public String fetchPhysicalInfraPumpAndMotor(HttpSession httpSession, Model model, @ModelAttribute ListingModel listingModel) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraPumpAndMotor -- START");
		}


		try {
			if(httpSession.getAttribute("loginUsersModel2") != null){
				LoginUsersModel loginUsersModel = (LoginUsersModel) httpSession.getAttribute("loginUsersModel2");
				int userLevel = loginUsersModel.getUserLevelModel().getUser_level_id();
				if(userLevel == 2){
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				}
				else if(userLevel == 3){
					listingModel.setDivisionId(0);
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				}
				else if(userLevel == 4){
					listingModel.setDivisionId(0);
					listingModel.setDistrictId(0);
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				}
			}
			LinkedList<ULBNameModel>  ulbNameModels= physicalInfraPumpMotorService.fetchUlbS(listingModel);

			if (ulbNameModels.size() > 0) {

				model.addAttribute("ulbNameModels", ulbNameModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraPumpAndMotor -- END");
		}

		return "list-physical-infra-pump-and-motor";

	}
	
	@RequestMapping(value = "/fetch-ajax-physical-infra-pump-and-motor", method = RequestMethod.GET)
	public String fetchAjaxPhysicalInfraPumpMotor(@ModelAttribute ListingModel listingModel, HttpSession httpSession, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAjaxPhysicalInfraPumpMotor -- START");
		}

		try {
			
			LinkedList<ULBNameModel>  ulbNameModels= physicalInfraPumpMotorService.fetchUlbS(listingModel);

			if (ulbNameModels.size() > 0) {

				model.addAttribute("ulbNameModels", ulbNameModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAjaxPhysicalInfraPumpMotor -- END");
		}

		return "ajax-physical-infra-pump-and-motor";

	}
}
	
	