/**
 * @formatter:off
 *
 */
package com.orissa.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.orissa.common.DateCalculation;
import com.orissa.common.MessageUtil;
import com.orissa.common.SortingMap;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.AjaxDashboardModel;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.DailyTankersModel;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.DailyWaterTestingModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.TargetModel;
import com.orissa.model.ULBModel;
import com.orissa.service.DashboardService;
import com.orissa.service.DivisionService;
import com.orissa.service.ULBNameService;
import com.orissa.service.ULBService;
import com.orissa.validation.DashboardValidation;

@Controller
public class HomeController {

	private static Logger logger = Logger.getLogger(HomeController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private ULBService ulbService;
	
	@Autowired
	private ULBNameService ulbNameService;
	
	@Autowired
	private SortingMap sortingMap; 
	
	@Autowired
	private DashboardValidation dashboardValidation;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(HttpServletRequest request) {

		if (logger.isInfoEnabled()) {
			logger.info("index -- START");
		}

		if (logger.isInfoEnabled()) {
			logger.info("index -- END");
		}

		return "landing-page";
	}
	
	@RequestMapping(value = "/swims-login", method = RequestMethod.GET)
	public String swimsLogin(HttpServletRequest request) {

		if (logger.isInfoEnabled()) {
			logger.info("swimsLogin -- START");
		}

		HttpSession httpSession = request.getSession(false);

		if (httpSession != null) {
			Object object = httpSession.getAttribute("loginUsersModel2");
			if (object != null) {
				return "redirect:/dashboard";
			}
		}

		if (logger.isInfoEnabled()) {
			logger.info("swimsLogin -- END");
		}

		return "login";
	}
	
	@RequestMapping(value = "/get-token", method = RequestMethod.POST)
	public @ResponseBody String getToken(HttpServletRequest httpServletRequest) {
		
		if (logger.isInfoEnabled()) {
			logger.info("getToken -Start");
		}

		String generateToken = "";

		try {

			generateToken = Util.getRandom28();
			
			HttpSession httpSession1 = httpServletRequest.getSession(true);
			
			httpSession1.setAttribute("serverToken", generateToken);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		if (logger.isInfoEnabled()) {
			logger.info("getToken -End");
		}
		
		return generateToken;

	}
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String dashboard(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("dashboard -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		model.addAttribute("loginUsersModel", loginUsersModel);

		ListingModel listingModel = new ListingModel();
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		
		model.addAttribute("startDate", startDate);
		
		/* ----------------------------------------------------------------------- Water Supply Starts ------------------------------------------------------------------------------------------*/
		
		try {
			
			double lpcdBoxValue = 0.0D;
			double waterSupplyCoverageBoxValue = 0.0D;
			double surplusDeficitBoxValue = 0.0D;
			double avgHrOfSupplyBoxValue = 0.0D;
			int popServedBoxValue = 0;
			double pipeWaterSupplyBoxValue = 0.0D;
			int ulbGreater135 = 0;
			int ulbLesser135 = 0;
			int waterSupplyCoverageGreater100 = 0;
			int waterSupplyCoverageGreater75 = 0;
			int waterSupplyCoverageGreater50 = 0;
			int waterSupplyCoverageLesser50 = 0;
			int deficit = 0;
			int surplus = 0;
			int avgGreater = 0;
			int avgLesser = 0;
			
			LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
			
			if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
				
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				waterSupplyCoverageBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_coverage_by_pws();
				surplusDeficitBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_surplus_deficit();
				avgHrOfSupplyBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				popServedBoxValue = (dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_pop_served()).intValue();
				pipeWaterSupplyBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_qlty_supply();
				
			}
			
			for(int i = 0; i < dailyWaterSupplyModels.size() - 1;i++) {
				
				DailyWaterSupplyModel dailyWaterSupplyModel = dailyWaterSupplyModels.get(i);
				
				if(dailyWaterSupplyModel.getType().equals("current")) {
					
					// LPCD Pie Chart
					if(dailyWaterSupplyModel.getRate_of_supply() >= 135) {
						
						ulbGreater135 = ulbGreater135 + 1;
						
					} else {
						
						ulbLesser135 = ulbLesser135 + 1;
					}
					
					// Water Supply Coverage Pie Chart
					if(dailyWaterSupplyModel.getCoverage_by_pws() >= 80) {
						
						waterSupplyCoverageGreater100 = waterSupplyCoverageGreater100 + 1;
						
					/*} else if(dailyWaterSupplyModel.getCoverage_by_pws() >= 75 && dailyWaterSupplyModel.getCoverage_by_pws() < 100) {
						
						waterSupplyCoverageGreater75 = waterSupplyCoverageGreater75 + 1;*/
						
					} else if(dailyWaterSupplyModel.getCoverage_by_pws() >=  50 && dailyWaterSupplyModel.getCoverage_by_pws() < 80) {
						
						waterSupplyCoverageGreater50 = waterSupplyCoverageGreater50 + 1;
						
					} else {
						
						waterSupplyCoverageLesser50 = waterSupplyCoverageLesser50 + 1;
					}
					
					// Surplus/Deficit Pie Chart
					if(dailyWaterSupplyModel.getSurplus_deficit() >= 0) {
						
						deficit = deficit + 1;
						
					} else {
						
						surplus = surplus + 1;
					}
					
					// Average Hour Of Water Supply Pie Chart
					if(dailyWaterSupplyModel.getFreq_supply() >= avgHrOfSupplyBoxValue) {
						
						avgGreater = avgGreater + 1;
						
					} else {
						
						avgLesser = avgLesser + 1;
					}
					
				}
				
			}
			
			//avgHrOfSupplyBoxValue = Math.round(avgHrOfSupplyBoxValue*10)/10;
			
			
			model.addAttribute("lpcdBoxValue", lpcdBoxValue);
			model.addAttribute("waterSupplyCoverageBoxValue", waterSupplyCoverageBoxValue);
			model.addAttribute("surplusDeficitBoxValue", surplusDeficitBoxValue);
			model.addAttribute("avgHrOfSupplyBoxValue", (int)avgHrOfSupplyBoxValue);
			model.addAttribute("popServedBoxValue", popServedBoxValue);
			model.addAttribute("pipeWaterSupplyBoxValue", pipeWaterSupplyBoxValue);
			
			model.addAttribute("ulbGreater135", ulbGreater135);
			model.addAttribute("ulbLesser135", ulbLesser135);
			model.addAttribute("waterSupplyCoverageGreater100", waterSupplyCoverageGreater100);
			model.addAttribute("waterSupplyCoverageGreater75", waterSupplyCoverageGreater75);
			model.addAttribute("waterSupplyCoverageGreater50", waterSupplyCoverageGreater50);
			model.addAttribute("waterSupplyCoverageLesser50", waterSupplyCoverageLesser50);
			model.addAttribute("deficit", deficit);
			model.addAttribute("surplus", surplus);
			model.addAttribute("avgGreater", avgGreater);
			model.addAttribute("avgLesser", avgLesser);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* ----------------------------------------------------------------------- Water Supply Ends ------------------------------------------------------------------------------------------*/
		
		/* ----------------------------------------------------------------------- Water Charges Starts ---------------------------------------------------------------------------------------*/
		
		try {
			
				double totalChargesCollections = 0.0D;
				double totalTarget = 0.0D;
				int collectionGreater100 = 0;
				int collectionGreater75 = 0;
				int collectionGreater50 = 0;
				int collectionLesser50 = 0;
				double totalPercentage = 0.0D;
				
				LinkedList<TargetModel> targetModels = dashboardService.fetchTargetWaterChargesTaxReport(listingModel, amrutTypeId, startDate, endDate);
				
				if ((targetModels != null) && (targetModels.size() > 0)) {
					
					totalChargesCollections = targetModels.get(targetModels.size() - 1).getTotalChargesCollections();
					totalTarget = targetModels.get(targetModels.size() - 1).getTotalTarget();
					
					for(int i = 0; i < targetModels.size() - 1;i++) {
						
						TargetModel targetModel = targetModels.get(i);
						
						if(targetModel.getType().equals("current")) {
							
							if(targetModel.getPercentage() >= 80) {
								
								collectionGreater100 = collectionGreater100 + 1;
								
							/*} else if(targetModel.getPercentage() >= 75 && targetModel.getPercentage() < 100) {
								
								collectionGreater75 = collectionGreater75 + 1;*/
								
							} else if(targetModel.getPercentage() >= 50 && targetModel.getPercentage() < 80) {
								
								collectionGreater50 = collectionGreater50 + 1;
								
							} else {
								
								collectionLesser50 = collectionLesser50 + 1;
							}
						}
						
					}
					
				}
			
			if(totalTarget != 0.0)
				totalPercentage = totalChargesCollections * 100 /totalTarget;
			else
				totalPercentage = 0.0;
			
			totalPercentage = Math.round(totalPercentage*100.0)/100.0;
			
			model.addAttribute("totalPercentageBoxValue", totalPercentage);
			model.addAttribute("collectionGreater100", (int)collectionGreater100);
			model.addAttribute("collectionGreater75", (int)collectionGreater75);
			model.addAttribute("collectionGreater50", (int)collectionGreater50);
			model.addAttribute("collectionLesser50", (int)collectionLesser50);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* ----------------------------------------------------------------------- Water Charges Ends ---------------------------------------------------------------------------------------*/
		
		
		/* -------------------------------------------------------- Data Entry Status / Approval Starts -------------------------------------------------------------------------------------*/
		
		try {
			
			HashMap<String,HashMap<String,ULBModel>> dataEntryStatus = dashboardService.dataEntryStatus(listingModel, amrutTypeId, startDate);
			
			int complete = 0;
			int partial = 0;
			int notApproved = 0;
			int approved = 0;
			int noDataEntry = 0;
			
			for (Entry<String, HashMap<String,ULBModel>> entry1 : dataEntryStatus.entrySet()) {
				
				if(entry1.getKey().startsWith("C")){ // Complete Data Entry
					
					complete = entry1.getValue().entrySet().size();
					
				} else if(entry1.getKey().startsWith("P")){ // Partial Data ENtry
					
					partial =  entry1.getValue().entrySet().size();
							
				} else if(entry1.getKey().startsWith("NA")){ // Not Approved
					
					notApproved =  entry1.getValue().entrySet().size();
							
				} else if(entry1.getKey().startsWith("A")){ // Approved
					
					approved =  entry1.getValue().entrySet().size();
				} else { // No Data Entry
					
					noDataEntry =  entry1.getValue().entrySet().size();
				}
			}
			
			 model.addAttribute("complete",complete);
			 model.addAttribute("partial",partial);
			 model.addAttribute("notApproved",notApproved);
			 model.addAttribute("approved",approved);
			 model.addAttribute("noDataEntry",noDataEntry);
			 
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* -------------------------------------------------------- Data Entry Status / Approval Ends -------------------------------------------------------------------------------------*/
		
		/* -------------------------------------------------------------------- Complaints Starts -----------------------------------------------------------------------------------------*/
		
		try {
			
			LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
			
			double total_comp_recveived = 0.0D;
			double total_comp_pending = 0.0D;
			double total_tw_comp_recveived = 0.0D;
			double total_tw_comp_pending = 0.0D;
			
			double totalReceived = 0.0D;
			double totalPending = 0.0D;
			
			if ((dailyComplaintsModels != null) && (dailyComplaintsModels.size() > 0)) {
						
				total_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_recveived();
				total_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_pending();
				total_tw_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_recveived();
				total_tw_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_pending();
				
				totalReceived = total_comp_recveived + total_tw_comp_recveived;
				totalPending = total_comp_pending + total_tw_comp_pending;
				
				totalPending = (Math.round(((totalPending * 100)/totalReceived)*100.0)/100.0);
				
			}
			
			model.addAttribute("complaintBoxValue", 100 - totalPending);
			model.addAttribute("complaintResolved", 100 - totalPending);
			model.addAttribute("complaintUnResolved", totalPending);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/* -------------------------------------------------------------------- Complaints Ends -----------------------------------------------------------------------------------------*/
		
		
		/* -------------------------------------------------------------------- Tankers Starts ------------------------------------------------------------------------------------------*/
		
		try {
			
			LinkedList<DailyTankersModel> dailyTankersModels = dashboardService.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
			
			double total_quantity_supplied = 0.0D;
			
			if ((dailyTankersModels != null) && (dailyTankersModels.size() > 0)) {
						
				total_quantity_supplied = dailyTankersModels.get(dailyTankersModels.size() - 1).getTotal_quantity_supplied();
				total_quantity_supplied = total_quantity_supplied / 1000000;
				
				total_quantity_supplied = (Math.round(total_quantity_supplied * 100)/100.0);
			}
			
			model.addAttribute("tankerBoxValue", total_quantity_supplied);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* -------------------------------------------------------------------- Tankers Ends -------------------------------------------------------------------------------------------*/
		
		/* -------------------------------------------------------------------- Water Testing Starts -------------------------------------------------------------------------------------------*/
		
		try {
			
			LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDate, endDate);
					
			double frcFoundOK = 0.0D;
			double bacteriologicalFoundOK = 0.0D;
			double frcFoundNotOK = 0.0D;
			double bacteriologicalFoundNotOK = 0.0D;
			
			if ((dailyWaterTestingModels != null) && (dailyWaterTestingModels.size() > 0)) {
				
				frcFoundOK = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found_frc(); // FRC Found OK
				frcFoundOK = frcFoundOK - 10; // To Be Changed
				bacteriologicalFoundOK = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found(); // Bacteriological Found OK
				bacteriologicalFoundOK = bacteriologicalFoundOK - 13; // To Be Changed
				frcFoundNotOK = 100 - frcFoundOK; // FRC Found Not OK
				frcFoundNotOK = Math.round(frcFoundNotOK*100.0)/100.0;
				bacteriologicalFoundNotOK = 100 - bacteriologicalFoundOK; // Bacteriological Found Not OK
				bacteriologicalFoundNotOK = Math.round(bacteriologicalFoundNotOK*100.0)/100.0;
			}
			
			model.addAttribute("testingBoxValue", (frcFoundOK + bacteriologicalFoundOK)/2); // FRC Found OK
			model.addAttribute("frcFoundOK", frcFoundOK); // FRC Found OK
			model.addAttribute("bacteriologicalFoundOK", bacteriologicalFoundOK); // Bacteriological Found OK
			model.addAttribute("frcFoundNotOK", frcFoundNotOK); // FRC Found Not OK
			model.addAttribute("bacteriologicalFoundNotOK", bacteriologicalFoundNotOK); // Bacteriological Found Not OK
					
			} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
		
			} catch (SQLException e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		/* -------------------------------------------------------------------- Water Testing Ends -------------------------------------------------------------------------------------------*/
		
		if (logger.isInfoEnabled()) {
			logger.info("dashboard -- END");
		}

		return "dashboard";
	}
	
	/**
	 * Ajax Dashboard
	 * @param listingModel
	 * @param amrutTypeId
	 * @param month
	 * @return ajaxDashboardModel
	 */
	@RequestMapping(value = "/ajax-dashboard", method = RequestMethod.GET)
	public @ResponseBody AjaxDashboardModel ajaxDashboard(HttpServletRequest httpServletRequest,
			@ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxDashboard -- START");
		}
		
		
		HttpSession httpSession = httpServletRequest.getSession();
		
		AjaxDashboardModel ajaxDashboardModel = new AjaxDashboardModel();
		
		try {
			
			
			dashboardValidation.dashboardValidation(listingModel, amrutTypeId, selectedYear, month);
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxDashboardModel.setPhDivisionModels(phDivisionModels);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		String startDate = null;
		String endDate = null;
		String days = null;
		
		if (month != null) {
			List<String> dates = null;
			try {
				dates = new DateCalculation().findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(0);
			endDate = dates.get(1);
			days = dates.get(2);
			
		} else {
			// 12-11-2016
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
			Date d = null;
			/*try {
				d = dateFormat2.parse(listingModel.getDate());
				System.out.println("listingModel.getDate() --- >>> "+listingModel.getDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			startDate = listingModel.getDate();
		}
		
		if(amrutTypeId != null) {
			
			httpSession.setAttribute("amrutTypeId", amrutTypeId);
		}
		
		/* ----------------------------------------------------------------------- Water Supply Starts ------------------------------------------------------------------------------------------*/
		
		try {
			
			double lpcdBoxValue = 0.0D;
			double waterSupplyCoverageBoxValue = 0.0D;
			double surplusDeficitBoxValue = 0.0D;
			double avgHrOfSupplyBoxValue = 0.0D;
			int popServedBoxValue = 0;
			double pipeWaterSupplyBoxValue = 0.0D;
			int ulbGreater135 = 0;
			int ulbLesser135 = 0;
			int waterSupplyCoverageGreater100 = 0;
			int waterSupplyCoverageGreater75 = 0;
			int waterSupplyCoverageGreater50 = 0;
			int waterSupplyCoverageLesser50 = 0;
			int deficit = 0;
			int surplus = 0;
			int avgGreater = 0;
			int avgLesser = 0;
			
			LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
			
			if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
				
				lpcdBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply();
				waterSupplyCoverageBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_coverage_by_pws();
				surplusDeficitBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_surplus_deficit();
				avgHrOfSupplyBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
				popServedBoxValue = (dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_pop_served()).intValue();
				pipeWaterSupplyBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_qlty_supply();
				
			}
			
			for(int i = 0; i < dailyWaterSupplyModels.size() - 1;i++) {
				
				DailyWaterSupplyModel dailyWaterSupplyModel = dailyWaterSupplyModels.get(i);
				
				if(dailyWaterSupplyModel.getType().equals("current")) {
					
					// LPCD Pie Chart
					if(dailyWaterSupplyModel.getRate_of_supply() >= 135) {
						
						ulbGreater135 = ulbGreater135 + 1;
						
					} else {
						
						ulbLesser135 = ulbLesser135 + 1;
					}
					
					// Water Supply Coverage Pie Chart
					if(dailyWaterSupplyModel.getCoverage_by_pws() >= 80) {
						
						waterSupplyCoverageGreater100 = waterSupplyCoverageGreater100 + 1;
						
					/*} else if(dailyWaterSupplyModel.getCoverage_by_pws() >= 75 && dailyWaterSupplyModel.getCoverage_by_pws() < 100) {
						
						waterSupplyCoverageGreater75 = waterSupplyCoverageGreater75 + 1;*/
						
					} else if(dailyWaterSupplyModel.getCoverage_by_pws() >=  50 && dailyWaterSupplyModel.getCoverage_by_pws() < 80) {
						
						waterSupplyCoverageGreater50 = waterSupplyCoverageGreater50 + 1;
						
					} else {
						
						waterSupplyCoverageLesser50 = waterSupplyCoverageLesser50 + 1;
					}
					
					// Surplus/Deficit Pie Chart
					if(dailyWaterSupplyModel.getSurplus_deficit() >= 0) {
						
						deficit = deficit + 1;
						
					} else {
						
						surplus = surplus + 1;
					}
					
					// Average Hour Of Water Supply Pie Chart
					if(dailyWaterSupplyModel.getFreq_supply() >= avgHrOfSupplyBoxValue) {
						
						avgGreater = avgGreater + 1;
						
					} else {
						
						avgLesser = avgLesser + 1;
					}
					
				}
				
			}
			
			ajaxDashboardModel.setLpcdBoxValue(lpcdBoxValue);
			ajaxDashboardModel.setWaterSupplyCoverageBoxValue(waterSupplyCoverageBoxValue);
			ajaxDashboardModel.setSurplusDeficitBoxValue(surplusDeficitBoxValue);
			ajaxDashboardModel.setAvgHrOfSupplyBoxValue((int)avgHrOfSupplyBoxValue);
			ajaxDashboardModel.setPopServedBoxValue(popServedBoxValue);
			ajaxDashboardModel.setPipeWaterSupplyBoxValue(pipeWaterSupplyBoxValue);
			
			ajaxDashboardModel.setUlbGreater135(ulbGreater135);
			ajaxDashboardModel.setUlbLesser135(ulbLesser135);
			ajaxDashboardModel.setWaterSupplyCoverageGreater100(waterSupplyCoverageGreater100);
			ajaxDashboardModel.setWaterSupplyCoverageGreater75(waterSupplyCoverageGreater75);
			ajaxDashboardModel.setWaterSupplyCoverageGreater50(waterSupplyCoverageGreater50);
			ajaxDashboardModel.setWaterSupplyCoverageLesser50(waterSupplyCoverageLesser50);
			ajaxDashboardModel.setDeficit(deficit);
			ajaxDashboardModel.setSurplus(surplus);
			ajaxDashboardModel.setAvgGreater(avgGreater);
			ajaxDashboardModel.setAvgLesser(avgLesser);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* ----------------------------------------------------------------------- Water Supply Ends ------------------------------------------------------------------------------------------*/
		
		/* ----------------------------------------------------------------------- Water Charges Starts ---------------------------------------------------------------------------------------*/
		
		try {
			
				double totalChargesCollections = 0.0D;
				double totalTarget = 0.0D;
				int collectionGreater100 = 0;
				int collectionGreater75 = 0;
				int collectionGreater50 = 0;
				int collectionLesser50 = 0;
				double totalPercentage = 0.0D;
				
				LinkedList<TargetModel> targetModels = dashboardService.fetchTargetWaterChargesTaxReport(listingModel, amrutTypeId, startDate, endDate);
				
				if ((targetModels != null) && (targetModels.size() > 0)) {
					
					totalChargesCollections = targetModels.get(targetModels.size() - 1).getTotalChargesCollections();
					totalTarget = targetModels.get(targetModels.size() - 1).getTotalTarget();
					
					for(int i = 0; i < targetModels.size() - 1;i++) {
						
						TargetModel targetModel = targetModels.get(i);
						
						if(targetModel.getType().equals("current")) {
							
							if(targetModel.getPercentage() >= 80) {
								
								collectionGreater100 = collectionGreater100 + 1;
								
							/*} else if(targetModel.getPercentage() >= 75 && targetModel.getPercentage() < 100) {
								
								collectionGreater75 = collectionGreater75 + 1;*/
								
							} else if(targetModel.getPercentage() >= 50 && targetModel.getPercentage() < 80) {
								
								collectionGreater50 = collectionGreater50 + 1;
								
							} else {
								
								collectionLesser50 = collectionLesser50 + 1;
							}
						}
						
					}
					
				}
			
			if(totalTarget != 0.0)
				totalPercentage = totalChargesCollections * 100 /totalTarget;
			else
				totalPercentage = 0.0;
			
			totalPercentage = Math.round(totalPercentage*100.0)/100.0;
			
			ajaxDashboardModel.setTotalPercentageBoxValue(totalPercentage);
			ajaxDashboardModel.setCollectionGreater100(collectionGreater100);
			ajaxDashboardModel.setCollectionGreater75(collectionGreater75);
			ajaxDashboardModel.setCollectionGreater50(collectionGreater50);
			ajaxDashboardModel.setCollectionLesser50(collectionLesser50);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* ----------------------------------------------------------------------- Water Charges Ends ---------------------------------------------------------------------------------------*/
		
		
		/* -------------------------------------------------------- Data Entry Status / Approval Starts -------------------------------------------------------------------------------------*/
		
		try {
			
			HashMap<String,HashMap<String,ULBModel>> dataEntryStatus = dashboardService.dataEntryStatus(listingModel, amrutTypeId, startDate);
			
			int complete = 0;
			int partial = 0;
			int notApproved = 0;
			int approved = 0;
			int noDataEntry = 0;
			
			for (Entry<String, HashMap<String,ULBModel>> entry1 : dataEntryStatus.entrySet()) {
				
				if(entry1.getKey().startsWith("C")){ // Complete Data Entry
					
					complete = entry1.getValue().entrySet().size();
					
				} else if(entry1.getKey().startsWith("P")){ // Partial Data ENtry
					
					partial =  entry1.getValue().entrySet().size();
							
				} else if(entry1.getKey().startsWith("NA")){ // Not Approved
					
					notApproved =  entry1.getValue().entrySet().size();
							
				} else if(entry1.getKey().startsWith("A")){ // Approved
					
					approved =  entry1.getValue().entrySet().size();
				} else { // No Data Entry
					
					noDataEntry =  entry1.getValue().entrySet().size();
				}
			}
			
			ajaxDashboardModel.setComplete(complete);
			ajaxDashboardModel.setPartial(partial);
			ajaxDashboardModel.setNotApproved(notApproved);
			ajaxDashboardModel.setApproved(approved);
			ajaxDashboardModel.setNoDataEntry(noDataEntry);
			 
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* -------------------------------------------------------- Data Entry Status / Approval Ends -------------------------------------------------------------------------------------*/
		
		/* -------------------------------------------------------------------- Complaints Starts -----------------------------------------------------------------------------------------*/
		
		try {
			
			LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
			
			double total_comp_recveived = 0.0D;
			double total_comp_pending = 0.0D;
			double total_tw_comp_recveived = 0.0D;
			double total_tw_comp_pending = 0.0D;
			
			double totalReceived = 0.0D;
			double totalPending = 0.0D;
			
			if ((dailyComplaintsModels != null) && (dailyComplaintsModels.size() > 0)) {
						
				total_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_recveived();
				total_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_comp_pending();
				total_tw_comp_recveived = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_recveived();
				total_tw_comp_pending = dailyComplaintsModels.get(dailyComplaintsModels.size() - 1).getTotal_tw_comp_pending();
				
				totalReceived = total_comp_recveived + total_tw_comp_recveived;
				totalPending = total_comp_pending + total_tw_comp_pending;
				
				totalPending = (Math.round(((totalPending * 100)/totalReceived)*100.0)/100.0);
				
			}
			
			
			ajaxDashboardModel.setComplaintBoxValue(100 - totalPending);
			if(100 - totalPending > 0)
				ajaxDashboardModel.setComplaintResolved(100 - totalPending);
			else
				ajaxDashboardModel.setComplaintResolved(0);
			ajaxDashboardModel.setComplaintUnResolved(totalPending);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/* -------------------------------------------------------------------- Complaints Ends -----------------------------------------------------------------------------------------*/
		
		
		/* -------------------------------------------------------------------- Tankers Starts ------------------------------------------------------------------------------------------*/
		
		try {
			
			LinkedList<DailyTankersModel> dailyTankersModels = dashboardService.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
			
			double total_quantity_supplied = 0.0D;
			
			if ((dailyTankersModels != null) && (dailyTankersModels.size() > 0)) {
						
				total_quantity_supplied = dailyTankersModels.get(dailyTankersModels.size() - 1).getTotal_quantity_supplied();
				total_quantity_supplied = total_quantity_supplied / 1000000;
				
				total_quantity_supplied = (Math.round(total_quantity_supplied * 100)/100.0);
			}
			
			ajaxDashboardModel.setTankerBoxValue(total_quantity_supplied);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* -------------------------------------------------------------------- Tankers Ends -------------------------------------------------------------------------------------------*/
		
		/* -------------------------------------------------------------------- Water Testing Starts -------------------------------------------------------------------------------------------*/
		
		try {
			
			LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDate, endDate);
					
			double frcFoundOK = 0.0D;
			double bacteriologicalFoundOK = 0.0D;
			double frcFoundNotOK = 0.0D;
			double bacteriologicalFoundNotOK = 0.0D;
			
			if ((dailyWaterTestingModels != null) && (dailyWaterTestingModels.size() > 0)) {
				
				frcFoundOK = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found_frc(); // FRC Found OK
				frcFoundOK = frcFoundOK - 10; // To Be Changed
				bacteriologicalFoundOK = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found(); // Bacteriological Found OK
				bacteriologicalFoundOK = bacteriologicalFoundOK - 13; // To Be Changed
				frcFoundNotOK = 100 - frcFoundOK; // FRC Found Not OK
				frcFoundNotOK = Math.round(frcFoundNotOK*100.0)/100.0;
				bacteriologicalFoundNotOK = 100 - bacteriologicalFoundOK; // Bacteriological Found Not OK
				bacteriologicalFoundNotOK = Math.round(bacteriologicalFoundNotOK*100.0)/100.0;
			}
			
			ajaxDashboardModel.setTestingBoxValue((frcFoundOK + bacteriologicalFoundOK)/2);
			ajaxDashboardModel.setFrcFoundOK(frcFoundOK); // FRC Found OK
			ajaxDashboardModel.setBacteriologicalFoundOK(bacteriologicalFoundOK); // Bacteriological Found OK
			ajaxDashboardModel.setFrcFoundNotOK(frcFoundNotOK); // FRC Found Not OK
			ajaxDashboardModel.setBacteriologicalFoundNotOK(bacteriologicalFoundNotOK); // Bacteriological Found Not OK
					
			} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
		
			} catch (SQLException e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		/* -------------------------------------------------------------------- Water Testing Ends -------------------------------------------------------------------------------------------*/
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("ajaxDashboard -- END");
		}

		return ajaxDashboardModel;
	}
	
	@RequestMapping(value = "/error404", method = RequestMethod.GET)
	public String error404(HttpServletRequest request) {

		if (logger.isInfoEnabled()) {
			logger.info("error404 -- START");
		}

		if (logger.isInfoEnabled()) {
			logger.info("error404 -- END");
		}

		return "page-404";
	}

	@RequestMapping(value = { "/error500", "/error" }, method = RequestMethod.GET)
	public String error500(HttpServletRequest request) {

		if (logger.isInfoEnabled()) {
			logger.info("error500 -- START");
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("error500 -- END");
		}

		return "page-500";
	}
	
	@RequestMapping(value = { "/attackersException"}, method = RequestMethod.GET)
	public String attackersException(HttpServletRequest request) {
		
		if (logger.isInfoEnabled()) {
			logger.info("attackersException -- START");
		}
		System.out.println("attackersException -- START");
		try{
			request.getSession(false).invalidate();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		
		if (logger.isInfoEnabled()) {
			logger.info("attackersException -- END");
		}

		return "HackProtection";
	}

	@RequestMapping(value = "/error403", method = RequestMethod.GET)
	public String error403(HttpServletRequest request) {

		if (logger.isInfoEnabled()) {
			logger.info("error403 -- START");
		}

		if (logger.isInfoEnabled()) {
			logger.info("error403 -- END");
		}

		return "page-403";
	}
	
}
