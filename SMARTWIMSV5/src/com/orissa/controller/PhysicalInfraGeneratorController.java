package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraGeneretorModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraGeneratorService;

@Controller
public class PhysicalInfraGeneratorController implements SaltTracker {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraGeneratorController.class);

	@Autowired
	private PhysicalInfraGeneratorService physicalInfraGeneratorService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private DistrictService districtService;

	@Autowired
	private DivisionService divisionService;
	@Autowired
	private Encryption encryption;


	@RequestMapping(value = "/add-physical-infra-generator", method = RequestMethod.GET)
	public String addPhysicalInfraGeneratorPage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraGeneratorPage -- START");
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraGeneratorPage -- END");
		}

		return "add-physical-infra-generator";

	}

	@RequestMapping(value = "/add-physical-infra-generator", method = RequestMethod.POST)
	public String addPhysicalInfraGenerator(Model model,
			@ModelAttribute PhysicalInfraGeneretorModel physicalInfraGeneretorModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraGenerator -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraGeneretorModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			int result = physicalInfraGeneratorService
					.addPhysicalInfraGenerator(physicalInfraGeneretorModel,
							httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute("physicalInfraGeneretorModel",
					physicalInfraGeneretorModel);
			return ("redirect:/add-physical-infra-generator");

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraGenerator -- END");
		}

		return ("redirect:/list-physical-infra-generator");
	}

	@RequestMapping(value = "/list-physical-infra-generator", method = RequestMethod.GET)
	public String fetchPhysicalInfraGenerator(HttpServletRequest httpServletRequest, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraGenerator -- START");
		}

		try {
			LinkedList<PhysicalInfraGeneretorModel> physicalInfraGeneretorModels = physicalInfraGeneratorService
					.fetchPhysicalInfraGenerator(httpServletRequest);

			if (physicalInfraGeneretorModels.size() > 0) {

				model.addAttribute("physicalInfraGeneretorModels",
						physicalInfraGeneretorModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraGenerator -- END");
		}

		return "list-physical-infra-generator";

	}

	@RequestMapping(value = "/edit-physical-infra-generator", method = RequestMethod.GET)
	public String editPhysicalInfraGeneratorPage(
			@RequestParam String phyInfra_gen_id, Model model,HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraGeneratorPage -- START");
		}

		if (StringUtils.isBlank(phyInfra_gen_id)) {
			return "redirect:/error404";
		}

		PhysicalInfraGeneretorModel physicalInfraGeneretorModel = null;
		String phyInfra_gen_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			phyInfra_gen_id1 = encryption.decode(key, id);
			if(phyInfra_gen_id.length() > id.length())
				throw new Exception();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		

		try {
			physicalInfraGeneretorModel = physicalInfraGeneratorService
					.fetchPhysicalInfraGenerator(Integer
							.parseInt(phyInfra_gen_id1));

			if (physicalInfraGeneretorModel.getPhyInfra_gen_id() != 0) {
				model.addAttribute("physicalInfraGeneretorModel",
						physicalInfraGeneretorModel);
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return "redirect:/list-physical-infra-generator";
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(physicalInfraGeneretorModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraGeneratorPage -- END");
		}

		return "edit-physical-infra-generator";

	}
	//delete-physical-infra-pipe?phyInfra_gen_id=19
	@RequestMapping(value = "/delete-physical-infra-generator", method = RequestMethod.POST)
	public String deletePhysicalInfraGeneratorPage(Model model, final RedirectAttributes redirectAttributes,HttpServletRequest httpServletRequest, PhysicalInfraGeneretorModel physicalInfraGeneretorModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraGeneratorPage -- START");
		}

		String phyInfra_gen_id = physicalInfraGeneretorModel.getPhyInfraGenId();
		
		if (StringUtils.isBlank(phyInfra_gen_id)) {
			return "redirect:/error404";
		}
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraGeneretorModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {

		String phyInfra_gen_id1 = null;
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			phyInfra_gen_id1 = encryption.decode(key, phyInfra_gen_id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			int result = physicalInfraGeneratorService
					.deletePhysicalInfraGenerator(Integer
							.parseInt(phyInfra_gen_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraGeneratorPage -- END");
		}

		return "redirect:/list-physical-infra-generator";

	}

	@RequestMapping(value = "/update-physical-infra-generator", method = RequestMethod.POST)
	public String updatePhysicalInfraGenerator(Model model,
			@ModelAttribute PhysicalInfraGeneretorModel physicalInfraGeneretorModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraGenerator -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraGeneretorModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			String phyInfraGenId = encryption.decode(key, physicalInfraGeneretorModel.getPhyInfraGenId());
			physicalInfraGeneretorModel.setPhyInfra_gen_id(Integer.parseInt(phyInfraGenId));
			int result = physicalInfraGeneratorService
					.updatePhysicalInfraGenerator(physicalInfraGeneretorModel,
							httpServletRequest);

			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			
			redirectAttributes.addFlashAttribute("physicalInfraGeneretorModel", physicalInfraGeneretorModel);
			return ("redirect:/edit-physical-infra-generator?phyInfra_gen_id="+physicalInfraGeneretorModel.getPhyInfraGenId());

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} else {
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraGenerator -- END");
		}

		return "redirect:/list-physical-infra-generator";

	}

	@RequestMapping(value = "/fetch-ajax-physical-infra-generator", method = RequestMethod.GET)
	public String fetchAJAXDailyWaterSupply(HttpServletRequest httpServletRequest, @ModelAttribute ListingModel listingModel, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- START");
		}
		LinkedList<PhysicalInfraGeneretorModel> physicalInfraGeneretorModels = null;

		try {
			physicalInfraGeneretorModels = physicalInfraGeneratorService.fetchAJAXPhysicalInfraGen(listingModel);

			model.addAttribute("physicalInfraGeneretorModels", physicalInfraGeneretorModels);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- END");
		}

		 return "ajax-physical-infra-generator";

	}
	
	
}
