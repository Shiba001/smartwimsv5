/**
 * @formatter:off
 *
 */
package com.orissa.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.orissa.common.DateCalculation;
import com.orissa.common.MessageUtil;
import com.orissa.common.SortingMap;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.AjaxwaterTestingModel;
import com.orissa.model.DailyWaterTestingModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.ULBModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DashboardService;
import com.orissa.service.DivisionService;
import com.orissa.validation.DashboardValidation;

@Controller
public class WaterTestingController {

	private static Logger logger = Logger.getLogger(WaterTestingController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private SortingMap sortingMap;
	
	@Autowired
	private DashboardService dashboardService;	
	
	@Autowired
	private DateCalculation dateCalculation;
	
	@Autowired
	private DashboardValidation dashboardValidation;
	
	@RequestMapping(value = "/water-testing", method = RequestMethod.GET)
	public String waterTestingPage(final Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("waterTestingPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		model.addAttribute("loginUsersModel2", loginUsersModel);
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		LinkedHashMap<String, Double> lpcdTotal = null;
		LinkedHashMap<String, DailyWaterTestingModel> lpcdMap = null;
		LinkedHashMap<String, Double> lpcdTotal1 = null;
		List<String> datesBetween = new LinkedList<String>();
		List<Double> currentLPCD = new LinkedList<Double>();
		List<Double> previousLPCD = new LinkedList<Double>();
		double bacteriologicalBoxValue = 0.0D;
		List<Double> currentLPCD1 = new LinkedList<Double>();
		List<Double> previousLPCD1 = new LinkedList<Double>();
		double frcBoxValue = 0.0D;
		String monthMap = null;
		String yearMap = null;
		String startDateMap = null;
		String endDateMap = null;
		String currentMonth = "";
		String previousMonth = "";
		
		model.addAttribute("startDate", startDate);
		
		if(startDate != null) {
			
			monthMap = startDate.substring(5, 7);
			yearMap = startDate.substring(0, 4);
			
			if (monthMap != null) {
				
				List<String> dates = null;
				try {
					dates = dateCalculation.findDate(Integer.valueOf(yearMap), Integer.valueOf(monthMap));
					startDateMap = dates.get(0);
					endDateMap = dates.get(1);
					currentMonth = dates.get(3);
					previousMonth = dates.get(4);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
		try {
			
			if(flag == 1) {
				
				/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterTestingModels != null && dailyWaterTestingModels.size() > 0) {
						
					bacteriologicalBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found();
					frcBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found_frc();
					
					lpcdTotal = new LinkedHashMap<String, Double>();
					lpcdTotal1 = new LinkedHashMap<String, Double>();
					
					if(dailyWaterTestingModels.size() > 2 && dailyWaterTestingModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterTestingModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
						for(int i = 0; i<dailyWaterTestingModels.size() - 1; i=i+2) {
							
							DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
							DailyWaterTestingModel dailyWaterTestingModel2 = dailyWaterTestingModels.get(i+1);
						    
							if(dailyWaterTestingModel1.getType().equals("current")) {
									
								// For Bacteriological Sample Found
								lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
								
								// For FRC Sample Found
								lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
									
							} else {
									
								// For Bacteriological Sample Found
								lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found());
								
								// For FRC Sample Found
								lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found_frc());
							}
						}
					} else {
						
						for(int i = 0; i<dailyWaterTestingModels.size() - 1; i++) {
							
							DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
							
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
						}
					}
				}
				
				LinkedList<DailyWaterTestingModel> mapDailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterTestingModels != null && mapDailyWaterTestingModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterTestingModel>();
					
					for(int i = 0; i<mapDailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = mapDailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = mapDailyWaterTestingModels.get(i+1);
						DailyWaterTestingModel dailyWaterTestingModel3 = new DailyWaterTestingModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
					    ULBModel ulbModel = new ULBModel();
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel1.getPercentage_found());
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel2.getPm_percentage_found());
							
							dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel1.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel2.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						} else {
								
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel1.getPm_percentage_found());									
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel2.getPercentage_found());
							
					        dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel2.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel1.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);						        
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						}
					}
					
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyWaterTestingModel dailyWaterTestingModel4 = lpcdMap.get(entry1.getKey());
					dailyWaterTestingModel4.setTotal_percentage_found(entry1.getValue());
					dailyWaterTestingModel4.setTotal_percentage_found_frc(lpcdTotal1.get(entry1.getKey()));
		        	dailyWaterTestingModel4.setCurrentMonth(currentMonth);
		        	dailyWaterTestingModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterTestingModel4);
					
				}
				
				model.addAttribute("bacteriologicalBoxValue", bacteriologicalBoxValue);
				model.addAttribute("frcBoxValue", frcBoxValue);
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
				
				/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
				
				int n = 3;
				if(amrutTypeId == null ) {
					 
					 n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
				 }
				
				model.addAttribute("topValue", n);
				HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
				
				//System.out.println("top10LpcdUlbs: "+ top10LpcdUlbs);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				 
				Set mapSet = (Set) top10LpcdUlbs.entrySet();
		        Iterator mapIterator = mapSet.iterator();
		        while (mapIterator.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
			        String keyValue = (String) mapEntry.getKey();
			        top10ulb.add(keyValue);
			        Double value = (Double) mapEntry.getValue();
			        top10lpcd.add(value);
		        }
		        
		        //System.out.println("top10ulb: "+ top10ulb);
				//System.out.println("top10lpcd: "+ top10lpcd);
				
				 model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
			     model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
			     
			     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
				
			     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
			     
			     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
			    
			    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
			     
			    //System.out.println("bottom10LpcdUlbs: "+ bottom10LpcdUlbs);
			     
		        LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
		        
		        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
		        Iterator mapIterator2 = mapSet2.iterator();
		        while (mapIterator2.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
			        String keyValue = (String) mapEntry.getKey();
			        bottom10ulb.add(keyValue);
			        Double value = (Double) mapEntry.getValue();
			        bottom10lpcd.add(value);
		        }
		        
		        //System.out.println("bottom10ulb: "+ bottom10ulb);
				//System.out.println("bottom10lpcd: "+ bottom10lpcd);
				 
				model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
				model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
				
				/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
				
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				model.addAttribute("flag", false);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
				
				int n1 = 3;
				if(amrutTypeId == null ) {
					 
					 n1 = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
				 }
				
				model.addAttribute("topValue1", n1);
				HashMap<String, Double> top10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, n1);
				
				//System.out.println("top10LpcdUlbs: "+ top10LpcdUlbs);
				
				LinkedList<String> top10ulb1 = new LinkedList<String>();
				LinkedList<Double> top10lpcd1 = new LinkedList<Double>();
				 
				Set mapSet1 = (Set) top10LpcdUlbs1.entrySet();
		        Iterator mapIterator1 = mapSet1.iterator();
		        while (mapIterator1.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator1.next();
			        String keyValue = (String) mapEntry.getKey();
			        top10ulb1.add(keyValue);
			        Double value = (Double) mapEntry.getValue();
			        top10lpcd1.add(value);
		        }
		        
		        //System.out.println("top10ulb: "+ top10ulb);
				//System.out.println("top10lpcd: "+ top10lpcd);
				
				 model.addAttribute("top10ulb1", new ObjectMapper().writeValueAsString(top10ulb1));
			     model.addAttribute("top10lpcd1", new ObjectMapper().writeValueAsString(top10lpcd1));
			     
			     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
				
			     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
			     
			     HashMap<String, Double> bottom10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal1, n1);
			    
			    bottom10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs1, bottom10LpcdUlbs1.size());
			     
			    //System.out.println("bottom10LpcdUlbs: "+ bottom10LpcdUlbs);
			     
		        LinkedList<String> bottom10ulb1 = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd1 = new LinkedList<Double>();
		        
		        Set mapSet21 = (Set) bottom10LpcdUlbs1.entrySet();
		        Iterator mapIterator21 = mapSet21.iterator();
		        while (mapIterator21.hasNext()) 
		        {
			        Map.Entry mapEntry = (Map.Entry) mapIterator21.next();
			        String keyValue = (String) mapEntry.getKey();
			        bottom10ulb1.add(keyValue);
			        Double value = (Double) mapEntry.getValue();
			        bottom10lpcd1.add(value);
		        }
		        
		        //System.out.println("bottom10ulb: "+ bottom10ulb);
				//System.out.println("bottom10lpcd: "+ bottom10lpcd);
				 
				model.addAttribute("bottom10ulb1", new ObjectMapper().writeValueAsString(bottom10ulb1));
				model.addAttribute("bottom10lpcd1", new ObjectMapper().writeValueAsString(bottom10lpcd1));
				
				/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
				
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, lpcdTotal1.size());
				
				LinkedList<String> topulb1 = new LinkedList<String>();
		        LinkedList<Double> toplpcd1 = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb1 = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd1 = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph1.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb1.add(entry1.getKey());
		        		toplpcd1.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb1.add(entry1.getKey());
		        		bottomlpcd1.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb1", new ObjectMapper().writeValueAsString(topulb1));
				model.addAttribute("toplpcd1", new ObjectMapper().writeValueAsString(toplpcd1));
				
				model.addAttribute("bottomulb1", new ObjectMapper().writeValueAsString(bottomulb1));
				model.addAttribute("bottomlpcd1", new ObjectMapper().writeValueAsString(bottomlpcd1));
				
				model.addAttribute("flag1", false);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterTestingModel> dailyWaterTestingModels2 = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterTestingModels2 != null && dailyWaterTestingModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found());
						
						// For Progress Report Current Value
						currentLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found_frc());
						
						// For Progress Report Previous Value
						previousLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found_frc());
									
					}
				}
				

		/* -------------------------------------------------------------------- For State Level And Above Ends -----------------------------------------------------------------------*/ 	
				
			} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterTestingModels != null && dailyWaterTestingModels.size() > 0) {
					
				bacteriologicalBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found();
				frcBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found_frc();
				
				lpcdTotal = new LinkedHashMap<String, Double>();
				lpcdTotal1 = new LinkedHashMap<String, Double>();
				
				if(dailyWaterTestingModels.size() > 2 && dailyWaterTestingModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterTestingModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
				
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = dailyWaterTestingModels.get(i+1);
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
								
						} else {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found_frc());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i++) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						
						// For Bacteriological Sample Found
						lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
						
						// For FRC Sample Found
						lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
					}
				}
			}
				
				LinkedList<DailyWaterTestingModel> mapDailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterTestingModels != null && mapDailyWaterTestingModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterTestingModel>();
					
					for(int i = 0; i<mapDailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = mapDailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = mapDailyWaterTestingModels.get(i+1);
						DailyWaterTestingModel dailyWaterTestingModel3 = new DailyWaterTestingModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
					    ULBModel ulbModel = new ULBModel();
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel1.getPercentage_found());
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel2.getPm_percentage_found());
							
							dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel1.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel2.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						} else {
								
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel1.getPm_percentage_found());									
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel2.getPercentage_found());
							
					        dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel2.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel1.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);						        
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						}
					}
					
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyWaterTestingModel dailyWaterTestingModel4 = lpcdMap.get(entry1.getKey());
					dailyWaterTestingModel4.setTotal_percentage_found(entry1.getValue());
					dailyWaterTestingModel4.setTotal_percentage_found_frc(lpcdTotal1.get(entry1.getKey()));
		        	dailyWaterTestingModel4.setCurrentMonth(currentMonth);
		        	dailyWaterTestingModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterTestingModel4);
					
				}

				model.addAttribute("bacteriologicalBoxValue", bacteriologicalBoxValue);
				model.addAttribute("frcBoxValue", frcBoxValue);
				model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
			    LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
			    
			    LinkedList<String> top10ulb1 = new LinkedList<String>();
				LinkedList<Double> top10lpcd1 = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb1 = new LinkedList<String>();
			    LinkedList<Double> bottom10lpcd1 = new LinkedList<Double>();
			    
			    if(lpcdTotal.size() >= 6 ) {
					
					/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					
					int n = 3;
					
					model.addAttribute("topValue", n);
					HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
					
					//System.out.println("top10LpcdUlbs: "+ top10LpcdUlbs);
					
					Set mapSet = (Set) top10LpcdUlbs.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd.add(value);
			        }
			        
			        //System.out.println("top10ulb: "+ top10ulb);
					//System.out.println("top10lpcd: "+ top10lpcd);
					
					 model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
				     model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
				    
				    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
				     
				    //System.out.println("bottom10LpcdUlbs: "+ bottom10LpcdUlbs);
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd.add(value);
			        }
			        
			        //System.out.println("bottom10ulb: "+ bottom10ulb);
					//System.out.println("bottom10lpcd: "+ bottom10lpcd);
					 
					model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
					model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
					model.addAttribute("flag", false);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
					
				} else {
					
					model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
				    model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));
				     
					model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
					model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));
					model.addAttribute("flag", true);
				}
			    
			    /* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
				model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
				
				model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
				model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
			    
			    if(lpcdTotal1.size() >= 6 ) {
					
					/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					
					int n1 = 3;
					
					model.addAttribute("topValue", n1);
					HashMap<String, Double> top10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, n1);
					
					//System.out.println("top10LpcdUlbs: "+ top10LpcdUlbs);
					
					Set mapSet = (Set) top10LpcdUlbs1.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb1.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd1.add(value);
			        }
			        
			        //System.out.println("top10ulb: "+ top10ulb);
					//System.out.println("top10lpcd: "+ top10lpcd);
					
					 model.addAttribute("top10ulb1", new ObjectMapper().writeValueAsString(top10ulb1));
				     model.addAttribute("top10lpcd1", new ObjectMapper().writeValueAsString(top10lpcd1));
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal1, n1);
				    
				    bottom10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs1, bottom10LpcdUlbs1.size());
				     
				    //System.out.println("bottom10LpcdUlbs: "+ bottom10LpcdUlbs);
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs1.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb1.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd1.add(value);
			        }
			        
			        //System.out.println("bottom10ulb: "+ bottom10ulb);
					//System.out.println("bottom10lpcd: "+ bottom10lpcd);
					 
					model.addAttribute("bottom10ulb1", new ObjectMapper().writeValueAsString(bottom10ulb1));
					model.addAttribute("bottom10lpcd1", new ObjectMapper().writeValueAsString(bottom10lpcd1));
					model.addAttribute("flag1", false);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
					
				} else {
					
					model.addAttribute("top10ulb1", new ObjectMapper().writeValueAsString(top10ulb1));
				    model.addAttribute("top10lpcd1", new ObjectMapper().writeValueAsString(top10lpcd1));
				     
					model.addAttribute("bottom10ulb1", new ObjectMapper().writeValueAsString(bottom10ulb1));
					model.addAttribute("bottom10lpcd1", new ObjectMapper().writeValueAsString(bottom10lpcd1));
					model.addAttribute("flag1", true);
				}
			    
			    /* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, lpcdTotal1.size());
				
				LinkedList<String> topulb1 = new LinkedList<String>();
		        LinkedList<Double> toplpcd1 = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb1 = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd1 = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph1.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb1.add(entry1.getKey());
		        		toplpcd1.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb1.add(entry1.getKey());
		        		bottomlpcd1.add(entry1.getValue());
		        	}
		        }
		        
		        model.addAttribute("topulb1", new ObjectMapper().writeValueAsString(topulb1));
				model.addAttribute("toplpcd1", new ObjectMapper().writeValueAsString(toplpcd1));
				
				model.addAttribute("bottomulb1", new ObjectMapper().writeValueAsString(bottomulb1));
				model.addAttribute("bottomlpcd1", new ObjectMapper().writeValueAsString(bottomlpcd1));
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
			
				String month = null;
				String year = null;
				String startDate1 = null;
				String endDate1 = null;
				
				if(startDate != null) {
					
					month = startDate.substring(5, 7);
					year = startDate.substring(0, 4);
					
					if (month != null) {
						
						List<String> dates = null;
						try {
							dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
							startDate1 = dates.get(0);
							endDate1 = startDate;
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} 
						
					}
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterTestingModel> dailyWaterTestingModels2 = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterTestingModels2 != null && dailyWaterTestingModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found());
						
						// For Progress Report Current Value
						currentLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found_frc());
						
						// For Progress Report Previous Value
						previousLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found_frc());
									
					}
				}
				
	/*	-------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
			} else if(flag == 3) {
				
	/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterTestingModels != null && dailyWaterTestingModels.size() > 0) {
					
				bacteriologicalBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found();
				frcBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found_frc();
				
				lpcdTotal = new LinkedHashMap<String, Double>();
				lpcdTotal1 = new LinkedHashMap<String, Double>();
				
				if(dailyWaterTestingModels.size() > 2 && dailyWaterTestingModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterTestingModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
				
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = dailyWaterTestingModels.get(i+1);
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
								
						} else {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found_frc());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i++) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						
						// For Bacteriological Sample Found
						lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
						
						// For FRC Sample Found
						lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
					}
				}
			}
				
				LinkedList<DailyWaterTestingModel> mapDailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterTestingModels != null && mapDailyWaterTestingModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterTestingModel>();
					
					for(int i = 0; i<mapDailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = mapDailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = mapDailyWaterTestingModels.get(i+1);
						DailyWaterTestingModel dailyWaterTestingModel3 = new DailyWaterTestingModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
					    ULBModel ulbModel = new ULBModel();
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel1.getPercentage_found());
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel2.getPm_percentage_found());
							
							dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel1.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel2.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						} else {
								
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel1.getPm_percentage_found());									
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel2.getPercentage_found());
							
					        dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel2.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel1.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);						        
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						}
					}
					
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyWaterTestingModel dailyWaterTestingModel4 = lpcdMap.get(entry1.getKey());
					dailyWaterTestingModel4.setTotal_percentage_found(entry1.getValue());
					dailyWaterTestingModel4.setTotal_percentage_found_frc(lpcdTotal1.get(entry1.getKey()));
		        	dailyWaterTestingModel4.setCurrentMonth(currentMonth);
		        	dailyWaterTestingModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterTestingModel4);
					
				}

				
			model.addAttribute("bacteriologicalBoxValue", bacteriologicalBoxValue);
			model.addAttribute("frcBoxValue", frcBoxValue);
			model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
			
			/* ----------------------- For All ULB Graphs Starts ------------------------------- */
			
			HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
			
			LinkedList<String> topulb = new LinkedList<String>();
	        LinkedList<Double> toplpcd = new LinkedList<Double>();
	        
	        LinkedList<String> bottomulb = new LinkedList<String>();
	        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
	        
	        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
	        	
	        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
	        	
	        	if(entry1.getValue() >= 100) {
	        		
	        		topulb.add(entry1.getKey());
	        		toplpcd.add(entry1.getValue());
	        		
	        	} else {
	        		
	        		bottomulb.add(entry1.getKey());
	        		bottomlpcd.add(entry1.getValue());
	        	}
	        }
	        
	        model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
			model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
			
			model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
			model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
			
			/* -----------------------  For All ULB Graphs Ends ------------------------------- */
			
			/* ----------------------- For All ULB Graphs Starts ------------------------------- */
			
			HashMap<String, Double> allUlbSortedGraph1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, lpcdTotal1.size());
			
			LinkedList<String> topulb1 = new LinkedList<String>();
	        LinkedList<Double> toplpcd1 = new LinkedList<Double>();
	        
	        LinkedList<String> bottomulb1 = new LinkedList<String>();
	        LinkedList<Double> bottomlpcd1 = new LinkedList<Double>();
	        
	        for (Entry<String, Double> entry1 : allUlbSortedGraph1.entrySet()) {
	        	
	        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
	        	
	        	if(entry1.getValue() >= 100) {
	        		
	        		topulb1.add(entry1.getKey());
	        		toplpcd1.add(entry1.getValue());
	        		
	        	} else {
	        		
	        		bottomulb1.add(entry1.getKey());
	        		bottomlpcd1.add(entry1.getValue());
	        	}
	        }
	        
	        model.addAttribute("topulb1", new ObjectMapper().writeValueAsString(topulb1));
			model.addAttribute("toplpcd1", new ObjectMapper().writeValueAsString(toplpcd1));
			
			model.addAttribute("bottomulb1", new ObjectMapper().writeValueAsString(bottomulb1));
			model.addAttribute("bottomlpcd1", new ObjectMapper().writeValueAsString(bottomlpcd1));
			
			/* -----------------------  For All ULB Graphs Ends ------------------------------- */
		     
		    String month = null;
			String year = null;
			String startDate1 = null;
			String endDate1 = null;
			
			if(startDate != null) {
				
				month = startDate.substring(5, 7);
				year = startDate.substring(0, 4);
				
				if (month != null) {
					
					List<String> dates = null;
					try {
						dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
						startDate1 = dates.get(0);
						endDate1 = startDate;
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					} 
					
				}
			}
				
			datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
			
			for(String date:datesBetween) {
				
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels2 = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, date, null);
				
				if(dailyWaterTestingModels2 != null && dailyWaterTestingModels2.size() > 0) {
					
					// For Progress Report Current Value
					currentLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found());
					
					// For Progress Report Previous Value
					previousLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found());
					
					// For Progress Report Current Value
					currentLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found_frc());
					
					// For Progress Report Previous Value
					previousLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found_frc());
								
				}
			}
					
				
	/*	-------------------------------------------------------------------- For District Level User Ends ----------------------------------------------------------------------------*/ 
				
			} else if(flag == 4) {
				
	/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
				
				listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterTestingModels != null && dailyWaterTestingModels.size() > 0) {
					
				bacteriologicalBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found();
				frcBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found_frc();
				
				lpcdTotal = new LinkedHashMap<String, Double>();
				lpcdTotal1 = new LinkedHashMap<String, Double>();
				
				if(dailyWaterTestingModels.size() > 2 && dailyWaterTestingModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterTestingModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
				
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = dailyWaterTestingModels.get(i+1);
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
								
						} else {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found_frc());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i++) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						
						// For Bacteriological Sample Found
						lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
						
						// For FRC Sample Found
						lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
					}
				}
			}
				
				LinkedList<DailyWaterTestingModel> mapDailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterTestingModels != null && mapDailyWaterTestingModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterTestingModel>();
					
					for(int i = 0; i<mapDailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = mapDailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = mapDailyWaterTestingModels.get(i+1);
						DailyWaterTestingModel dailyWaterTestingModel3 = new DailyWaterTestingModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
					    ULBModel ulbModel = new ULBModel();
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel1.getPercentage_found());
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel2.getPm_percentage_found());
							
							dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel1.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel2.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						} else {
								
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel1.getPm_percentage_found());									
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel2.getPercentage_found());
							
					        dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel2.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel1.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);						        
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						}
					}
					
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyWaterTestingModel dailyWaterTestingModel4 = lpcdMap.get(entry1.getKey());
					dailyWaterTestingModel4.setTotal_percentage_found(entry1.getValue());
					dailyWaterTestingModel4.setTotal_percentage_found_frc(lpcdTotal1.get(entry1.getKey()));
		        	dailyWaterTestingModel4.setCurrentMonth(currentMonth);
		        	dailyWaterTestingModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterTestingModel4);
					
				}

			model.addAttribute("bacteriologicalBoxValue", bacteriologicalBoxValue);
			model.addAttribute("frcBoxValue", frcBoxValue);
			model.addAttribute("lpcdMap", new ObjectMapper().writeValueAsString(lpcdMap));
			
			LinkedList<String> top10ulb = new LinkedList<String>();
			LinkedList<Double> top10lpcd = new LinkedList<Double>();
			model.addAttribute("top10ulb", new ObjectMapper().writeValueAsString(top10ulb));
			model.addAttribute("top10lpcd", new ObjectMapper().writeValueAsString(top10lpcd));

			LinkedList<String> bottom10ulb = new LinkedList<String>();
			LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
			model.addAttribute("bottom10ulb", new ObjectMapper().writeValueAsString(bottom10ulb));
			model.addAttribute("bottom10lpcd", new ObjectMapper().writeValueAsString(bottom10lpcd));

			LinkedList<String> topulb = new LinkedList<String>();
			LinkedList<Double> toplpcd = new LinkedList<Double>();
			
			model.addAttribute("topulb", new ObjectMapper().writeValueAsString(topulb));
			model.addAttribute("toplpcd", new ObjectMapper().writeValueAsString(toplpcd));
			
			LinkedList<String> bottomulb = new LinkedList<String>();
			LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			
			model.addAttribute("bottomulb", new ObjectMapper().writeValueAsString(bottomulb));
			model.addAttribute("bottomlpcd", new ObjectMapper().writeValueAsString(bottomlpcd));
			
			model.addAttribute("flag", false);
			
			LinkedList<String> top10ulb1 = new LinkedList<String>();
			LinkedList<Double> top10lpcd1 = new LinkedList<Double>();
			model.addAttribute("top10ulb1", new ObjectMapper().writeValueAsString(top10ulb1));
			model.addAttribute("top10lpcd1", new ObjectMapper().writeValueAsString(top10lpcd1));

			LinkedList<String> bottom10ulb1 = new LinkedList<String>();
			LinkedList<Double> bottom10lpcd1 = new LinkedList<Double>();
			model.addAttribute("bottom10ulb1", new ObjectMapper().writeValueAsString(bottom10ulb1));
			model.addAttribute("bottom10lpcd1", new ObjectMapper().writeValueAsString(bottom10lpcd1));

			LinkedList<String> topulb1 = new LinkedList<String>();
			LinkedList<Double> toplpcd1 = new LinkedList<Double>();
			
			model.addAttribute("topulb1", new ObjectMapper().writeValueAsString(topulb1));
			model.addAttribute("toplpcd1", new ObjectMapper().writeValueAsString(toplpcd1));
			
			LinkedList<String> bottomulb1 = new LinkedList<String>();
			LinkedList<Double> bottomlpcd1 = new LinkedList<Double>();
			
			model.addAttribute("bottomulb1", new ObjectMapper().writeValueAsString(bottomulb1));
			model.addAttribute("bottomlpcd1", new ObjectMapper().writeValueAsString(bottomlpcd1));
			
			model.addAttribute("flag1", false);
			
			String month = null;
			String year = null;
			String startDate1 = null;
			String endDate1 = null;
			
			if(startDate != null) {
				
				month = startDate.substring(5, 7);
				year = startDate.substring(0, 4);
				
				if (month != null) {
					
					List<String> dates = null;
					try {
						dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
						startDate1 = dates.get(0);
						endDate1 = startDate;
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					} 
					
				}
			}
			
			datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
			
			for(String date:datesBetween) {
				
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels2 = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, date, null);
				
				if(dailyWaterTestingModels2 != null && dailyWaterTestingModels2.size() > 0) {
					
					// For Progress Report Current Value
					currentLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found());
					
					// For Progress Report Previous Value
					previousLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found());
					
					// For Progress Report Current Value
					currentLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found_frc());
					
					// For Progress Report Previous Value
					previousLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found_frc());
								
				}
			}
				
				
	/* -------------------------------------------------------------------- For ULB Level User Ends ---------------------------------------------------------------------------------*/ 
		}
			
			model.addAttribute("datesBetween", new ObjectMapper().writeValueAsString(datesBetween));
			model.addAttribute("currentLPCD", new ObjectMapper().writeValueAsString(currentLPCD));
			model.addAttribute("previousLPCD", new ObjectMapper().writeValueAsString(previousLPCD));
			
			model.addAttribute("currentLPCD1", new ObjectMapper().writeValueAsString(currentLPCD1));
			model.addAttribute("previousLPCD1", new ObjectMapper().writeValueAsString(previousLPCD1));
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("waterTestingPage -- END");
		}

		return "water-testing";

	}
	
	/**
	 * ajax water Testing
	 */
	@RequestMapping(value = "/ajax-water-testing", method = RequestMethod.GET)
	public @ResponseBody AjaxwaterTestingModel ajaxwaterTesting(
			@ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxwaterTesting -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		AjaxwaterTestingModel ajaxwaterTestingModel = new AjaxwaterTestingModel();
		
		try {
			
			
			dashboardValidation.dashboardValidation(listingModel, amrutTypeId, selectedYear, month);
			
		String startDate = null;
		String endDate = null;
		String days = null;
		String year = null;
		String startDate1 = null;
		String endDate1 = null;
		String startDateMap = null;
		String endDateMap = null;
		String currentMonth = "";
		String previousMonth = "";
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxwaterTestingModel.setPhDivisionModels(phDivisionModels);
					
					listingModel.setDistrictId(null);
					listingModel.setDivisionId(null);
					listingModel.setUlbId(null);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if (month != null) {
			
			List<String> dates = null;
			try {
				dates = dateCalculation.findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
				startDateMap = dates.get(0);
				endDateMap = dates.get(1);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(0);
			endDate = dates.get(1);
			days = dates.get(2);
			currentMonth = dates.get(3);
			previousMonth = dates.get(4);
			startDate1 = startDate;
			endDate1 = endDate;
			
		} else {
			
			startDate = listingModel.getDate();
			
			if(startDate != null) {
				
				month = startDate.substring(5, 7);
				year = startDate.substring(0, 4);
				
				if (month != null) {
					
					List<String> dates = null;
					try {
						dates = dateCalculation.findDate(Integer.valueOf(year), Integer.valueOf(month));
						startDate1 = dates.get(0);
						endDate1 = startDate;
						startDateMap = dates.get(0);
						endDateMap = dates.get(1);
						currentMonth = dates.get(3);
						previousMonth = dates.get(4);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					} 
					
				}
				
			}
		
		}
		
		LinkedHashMap<String, Double> lpcdTotal = null;
		LinkedHashMap<String, DailyWaterTestingModel> lpcdMap = null;
		LinkedHashMap<String, Double> lpcdTotal1 = null;
		List<String> datesBetween = new LinkedList<String>();
		List<Double> currentLPCD = new LinkedList<Double>();
		List<Double> previousLPCD = new LinkedList<Double>();
		double bacteriologicalBoxValue = 0.0D;
		List<Double> currentLPCD1 = new LinkedList<Double>();
		List<Double> previousLPCD1 = new LinkedList<Double>();
		double frcBoxValue = 0.0D;
		
		try {
			
			if(flag == 1) {
				
				/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterTestingModels != null && dailyWaterTestingModels.size() > 0) {
					
				bacteriologicalBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found();
				frcBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found_frc();
				
				lpcdTotal = new LinkedHashMap<String, Double>();
				lpcdTotal1 = new LinkedHashMap<String, Double>();
				
				if(dailyWaterTestingModels.size() > 2 && dailyWaterTestingModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterTestingModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
				
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = dailyWaterTestingModels.get(i+1);
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
								
						} else {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found_frc());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i++) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						
						// For Bacteriological Sample Found
						lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
						
						// For FRC Sample Found
						lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
					}
				}
			}
				
				LinkedList<DailyWaterTestingModel> mapDailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterTestingModels != null && mapDailyWaterTestingModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterTestingModel>();
					
					for(int i = 0; i<mapDailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = mapDailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = mapDailyWaterTestingModels.get(i+1);
						DailyWaterTestingModel dailyWaterTestingModel3 = new DailyWaterTestingModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
					    ULBModel ulbModel = new ULBModel();
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel1.getPercentage_found());
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel2.getPm_percentage_found());
							
							dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel1.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel2.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						} else {
								
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel1.getPm_percentage_found());									
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel2.getPercentage_found());
							
					        dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel2.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel1.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);						        
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						}
					}
					
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyWaterTestingModel dailyWaterTestingModel4 = lpcdMap.get(entry1.getKey());
					dailyWaterTestingModel4.setTotal_percentage_found(entry1.getValue());
					dailyWaterTestingModel4.setTotal_percentage_found_frc(lpcdTotal1.get(entry1.getKey()));
		        	dailyWaterTestingModel4.setCurrentMonth(currentMonth);
		        	dailyWaterTestingModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterTestingModel4);
					
				}

				ajaxwaterTestingModel.setBacteriologicalBoxValue(bacteriologicalBoxValue);
				ajaxwaterTestingModel.setFrcBoxValue(frcBoxValue);
				ajaxwaterTestingModel.setLpcdMap(lpcdMap);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
		        
		        LinkedList<String> top10ulb1 = new LinkedList<String>();
				LinkedList<Double> top10lpcd1 = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb1 = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd1 = new LinkedList<Double>();
				
				if(lpcdTotal.size() >= 6) {
								
					/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					int n = 3;
					if((amrutTypeId == null) && (listingModel.getDivisionId() == null) && (listingModel.getDistrictId() == null) && (listingModel.getUlbId() == null)) {
						 
						 n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
					 }
					
					ajaxwaterTestingModel.setTopValue(n);
					HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
					
					Set mapSet = (Set) top10LpcdUlbs.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd.add(value);
			        }
			        
			        ajaxwaterTestingModel.setTop10ulb(top10ulb);
			        ajaxwaterTestingModel.setTop10lpcd(top10lpcd);
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
				    
				    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd.add(value);
			        }
			        
					 
			        ajaxwaterTestingModel.setBottom10ulb(bottom10ulb);
			        ajaxwaterTestingModel.setBottom10lpcd(bottom10lpcd);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
			        
			        /* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
					
					LinkedList<String> topulb = new LinkedList<String>();
			        LinkedList<Double> toplpcd = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 100) {
			        		
			        		topulb.add(entry1.getKey());
			        		toplpcd.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb.add(entry1.getKey());
			        		bottomlpcd.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxwaterTestingModel.setTopulb(topulb);
			        ajaxwaterTestingModel.setToplpcd(toplpcd);
			        
			        ajaxwaterTestingModel.setBottomulb(bottomulb);
			        ajaxwaterTestingModel.setBottomlpcd(bottomlpcd);
			        
			        ajaxwaterTestingModel.setFlag(false);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
		        
				} else {
					
					/* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
					
					LinkedList<String> topulb = new LinkedList<String>();
			        LinkedList<Double> toplpcd = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 100) {
			        		
			        		topulb.add(entry1.getKey());
			        		toplpcd.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb.add(entry1.getKey());
			        		bottomlpcd.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxwaterTestingModel.setTopulb(topulb);
			        ajaxwaterTestingModel.setToplpcd(toplpcd);
			        
			        ajaxwaterTestingModel.setBottomulb(bottomulb);
			        ajaxwaterTestingModel.setBottomlpcd(bottomlpcd);
			        
			        ajaxwaterTestingModel.setTop10ulb(top10ulb);
			        ajaxwaterTestingModel.setTop10lpcd(top10lpcd);
			        
			        ajaxwaterTestingModel.setBottom10ulb(bottom10ulb);
			        ajaxwaterTestingModel.setBottom10lpcd(bottom10lpcd);
			        
			        ajaxwaterTestingModel.setFlag(true);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				}
				
				if(lpcdTotal1.size() >= 6) {
					
					/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					int n1 = 3;
					if((amrutTypeId == null) && (listingModel.getDivisionId() == null) && (listingModel.getDistrictId() == null) && (listingModel.getUlbId() == null)) {
						 
						 n1 = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
					 }
					
					ajaxwaterTestingModel.setTopValue1(n1);
					HashMap<String, Double> top10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, n1);
					
					Set mapSet = (Set) top10LpcdUlbs1.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb1.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd1.add(value);
			        }
			        
			        ajaxwaterTestingModel.setTop10ulb1(top10ulb1);
			        ajaxwaterTestingModel.setTop10lpcd1(top10lpcd1);
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal1, n1);
				    
				    bottom10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs1, bottom10LpcdUlbs1.size());
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs1.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb1.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd1.add(value);
			        }
			        
					 
			        ajaxwaterTestingModel.setBottom10ulb1(bottom10ulb1);
			        ajaxwaterTestingModel.setBottom10lpcd1(bottom10lpcd1);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
			        
			        /* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, lpcdTotal1.size());
					
					LinkedList<String> topulb1 = new LinkedList<String>();
			        LinkedList<Double> toplpcd1 = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb1 = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd1 = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph1.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 100) {
			        		
			        		topulb1.add(entry1.getKey());
			        		toplpcd1.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb1.add(entry1.getKey());
			        		bottomlpcd1.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxwaterTestingModel.setTopulb1(topulb1);
			        ajaxwaterTestingModel.setToplpcd1(toplpcd1);
			        
			        ajaxwaterTestingModel.setBottomulb1(bottomulb1);
			        ajaxwaterTestingModel.setBottomlpcd1(bottomlpcd1);
			        
			        ajaxwaterTestingModel.setFlag1(false);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
		        
				} else {
					
					/* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, lpcdTotal1.size());
					
					LinkedList<String> topulb1 = new LinkedList<String>();
			        LinkedList<Double> toplpcd1 = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb1 = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd1 = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph1.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 100) {
			        		
			        		topulb1.add(entry1.getKey());
			        		toplpcd1.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb1.add(entry1.getKey());
			        		bottomlpcd1.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxwaterTestingModel.setTopulb1(topulb1);
			        ajaxwaterTestingModel.setToplpcd1(toplpcd1);
			        
			        ajaxwaterTestingModel.setBottomulb1(bottomulb1);
			        ajaxwaterTestingModel.setBottomlpcd1(bottomlpcd1);
			        
			        ajaxwaterTestingModel.setTop10ulb1(top10ulb1);
			        ajaxwaterTestingModel.setTop10lpcd1(top10lpcd1);
			        
			        ajaxwaterTestingModel.setBottom10ulb1(bottom10ulb1);
			        ajaxwaterTestingModel.setBottom10lpcd1(bottom10lpcd1);
			        
			        ajaxwaterTestingModel.setFlag1(true);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterTestingModel> dailyWaterTestingModels2 = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterTestingModels2 != null && dailyWaterTestingModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found());
						
						// For Progress Report Current Value
						currentLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found_frc());
						
						// For Progress Report Previous Value
						previousLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found_frc());
									
					}
				}
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
			} else if(flag == 2) {
				
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterTestingModels != null && dailyWaterTestingModels.size() > 0) {
					
				bacteriologicalBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found();
				frcBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found_frc();
				
				lpcdTotal = new LinkedHashMap<String, Double>();
				lpcdTotal1 = new LinkedHashMap<String, Double>();
				
				if(dailyWaterTestingModels.size() > 2 && dailyWaterTestingModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterTestingModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
				
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = dailyWaterTestingModels.get(i+1);
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
								
						} else {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found_frc());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i++) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						
						// For Bacteriological Sample Found
						lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
						
						// For FRC Sample Found
						lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
					}
				}
			}
				
				LinkedList<DailyWaterTestingModel> mapDailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterTestingModels != null && mapDailyWaterTestingModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterTestingModel>();
					
					for(int i = 0; i<mapDailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = mapDailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = mapDailyWaterTestingModels.get(i+1);
						DailyWaterTestingModel dailyWaterTestingModel3 = new DailyWaterTestingModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
					    ULBModel ulbModel = new ULBModel();
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel1.getPercentage_found());
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel2.getPm_percentage_found());
							
							dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel1.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel2.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						} else {
								
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel1.getPm_percentage_found());									
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel2.getPercentage_found());
							
					        dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel2.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel1.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);						        
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						}
					}
					
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyWaterTestingModel dailyWaterTestingModel4 = lpcdMap.get(entry1.getKey());
					dailyWaterTestingModel4.setTotal_percentage_found(entry1.getValue());
					dailyWaterTestingModel4.setTotal_percentage_found_frc(lpcdTotal1.get(entry1.getKey()));
		        	dailyWaterTestingModel4.setCurrentMonth(currentMonth);
		        	dailyWaterTestingModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterTestingModel4);
					
				}

				ajaxwaterTestingModel.setBacteriologicalBoxValue(bacteriologicalBoxValue);
				ajaxwaterTestingModel.setFrcBoxValue(frcBoxValue);
				ajaxwaterTestingModel.setLpcdMap(lpcdMap);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd = new LinkedList<Double>();
		        
		        LinkedList<String> top10ulb1 = new LinkedList<String>();
				LinkedList<Double> top10lpcd1 = new LinkedList<Double>();
				
				LinkedList<String> bottom10ulb1 = new LinkedList<String>();
		        LinkedList<Double> bottom10lpcd1 = new LinkedList<Double>();
				
				if(lpcdTotal.size() >= 6) {
								
					/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					int n = 3;
					if((amrutTypeId == null) && (listingModel.getDivisionId() == null) && (listingModel.getDistrictId() == null) && (listingModel.getUlbId() == null)) {
						 
						 n = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
					 }
					
					ajaxwaterTestingModel.setTopValue(n);
					HashMap<String, Double> top10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, n);
					
					Set mapSet = (Set) top10LpcdUlbs.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd.add(value);
			        }
			        
			        ajaxwaterTestingModel.setTop10ulb(top10ulb);
			        ajaxwaterTestingModel.setTop10lpcd(top10lpcd);
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal, n);
				    
				    bottom10LpcdUlbs = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs, bottom10LpcdUlbs.size());
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd.add(value);
			        }
			        
					 
			        ajaxwaterTestingModel.setBottom10ulb(bottom10ulb);
			        ajaxwaterTestingModel.setBottom10lpcd(bottom10lpcd);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
			        
			        /* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
					
					LinkedList<String> topulb = new LinkedList<String>();
			        LinkedList<Double> toplpcd = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 100) {
			        		
			        		topulb.add(entry1.getKey());
			        		toplpcd.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb.add(entry1.getKey());
			        		bottomlpcd.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxwaterTestingModel.setTopulb(topulb);
			        ajaxwaterTestingModel.setToplpcd(toplpcd);
			        
			        ajaxwaterTestingModel.setBottomulb(bottomulb);
			        ajaxwaterTestingModel.setBottomlpcd(bottomlpcd);
			        
			        ajaxwaterTestingModel.setFlag(false);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
		        
				} else {
					
					/* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
					
					LinkedList<String> topulb = new LinkedList<String>();
			        LinkedList<Double> toplpcd = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 100) {
			        		
			        		topulb.add(entry1.getKey());
			        		toplpcd.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb.add(entry1.getKey());
			        		bottomlpcd.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxwaterTestingModel.setTopulb(topulb);
			        ajaxwaterTestingModel.setToplpcd(toplpcd);
			        
			        ajaxwaterTestingModel.setBottomulb(bottomulb);
			        ajaxwaterTestingModel.setBottomlpcd(bottomlpcd);
			        
			        ajaxwaterTestingModel.setTop10ulb(top10ulb);
			        ajaxwaterTestingModel.setTop10lpcd(top10lpcd);
			        
			        ajaxwaterTestingModel.setBottom10ulb(bottom10ulb);
			        ajaxwaterTestingModel.setBottom10lpcd(bottom10lpcd);
			        
			        ajaxwaterTestingModel.setFlag(true);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				}
				
				if(lpcdTotal1.size() >= 6) {
					
					/* ----------------------- For Top 10 LPCD Starts ------------------------------- */
					
					int n1 = 3;
					if((amrutTypeId == null) && (listingModel.getDivisionId() == null) && (listingModel.getDistrictId() == null) && (listingModel.getUlbId() == null)) {
						 
						 n1 = Integer.parseInt(messageUtil.getBundle("lpcd.sort"));
					 }
					
					ajaxwaterTestingModel.setTopValue1(n1);
					HashMap<String, Double> top10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, n1);
					
					Set mapSet = (Set) top10LpcdUlbs1.entrySet();
			        Iterator mapIterator = mapSet.iterator();
			        while (mapIterator.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator.next();
				        String keyValue = (String) mapEntry.getKey();
				        top10ulb1.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        top10lpcd1.add(value);
			        }
			        
			        ajaxwaterTestingModel.setTop10ulb1(top10ulb1);
			        ajaxwaterTestingModel.setTop10lpcd1(top10lpcd1);
				     
				     /* ----------------------- For Top 10 LPCD Ends ------------------------------- */
					
				     /* ----------------------- For Bottom 10 LPCD Starts ------------------------------- */
				     
				     HashMap<String, Double> bottom10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findBottom10LpcdUlbs(lpcdTotal1, n1);
				    
				    bottom10LpcdUlbs1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(bottom10LpcdUlbs1, bottom10LpcdUlbs1.size());
				     
			        Set mapSet2 = (Set) bottom10LpcdUlbs1.entrySet();
			        Iterator mapIterator2 = mapSet2.iterator();
			        while (mapIterator2.hasNext()) 
			        {
				        Map.Entry mapEntry = (Map.Entry) mapIterator2.next();
				        String keyValue = (String) mapEntry.getKey();
				        bottom10ulb1.add(keyValue);
				        Double value = (Double) mapEntry.getValue();
				        bottom10lpcd1.add(value);
			        }
			        
					 
			        ajaxwaterTestingModel.setBottom10ulb1(bottom10ulb1);
			        ajaxwaterTestingModel.setBottom10lpcd1(bottom10lpcd1);
					
					/* ----------------------- For Bottom 10 LPCD Ends ------------------------------- */
			        
			        /* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, lpcdTotal1.size());
					
					LinkedList<String> topulb1 = new LinkedList<String>();
			        LinkedList<Double> toplpcd1 = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb1 = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd1 = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph1.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 100) {
			        		
			        		topulb1.add(entry1.getKey());
			        		toplpcd1.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb1.add(entry1.getKey());
			        		bottomlpcd1.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxwaterTestingModel.setTopulb1(topulb1);
			        ajaxwaterTestingModel.setToplpcd1(toplpcd1);
			        
			        ajaxwaterTestingModel.setBottomulb1(bottomulb1);
			        ajaxwaterTestingModel.setBottomlpcd1(bottomlpcd1);
			        
			        ajaxwaterTestingModel.setFlag1(false);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
		        
				} else {
					
					/* ----------------------- For All ULB Graphs Starts ------------------------------- */
					
					HashMap<String, Double> allUlbSortedGraph1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, lpcdTotal1.size());
					
					LinkedList<String> topulb1 = new LinkedList<String>();
			        LinkedList<Double> toplpcd1 = new LinkedList<Double>();
			        
			        LinkedList<String> bottomulb1 = new LinkedList<String>();
			        LinkedList<Double> bottomlpcd1 = new LinkedList<Double>();
			        
			        for (Entry<String, Double> entry1 : allUlbSortedGraph1.entrySet()) {
			        	
			        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
			        	
			        	if(entry1.getValue() >= 100) {
			        		
			        		topulb1.add(entry1.getKey());
			        		toplpcd1.add(entry1.getValue());
			        		
			        	} else {
			        		
			        		bottomulb1.add(entry1.getKey());
			        		bottomlpcd1.add(entry1.getValue());
			        	}
			        }
			        
			        ajaxwaterTestingModel.setTopulb1(topulb1);
			        ajaxwaterTestingModel.setToplpcd1(toplpcd1);
			        
			        ajaxwaterTestingModel.setBottomulb1(bottomulb1);
			        ajaxwaterTestingModel.setBottomlpcd1(bottomlpcd1);
			        
			        ajaxwaterTestingModel.setTop10ulb1(top10ulb1);
			        ajaxwaterTestingModel.setTop10lpcd1(top10lpcd1);
			        
			        ajaxwaterTestingModel.setBottom10ulb1(bottom10ulb1);
			        ajaxwaterTestingModel.setBottom10lpcd1(bottom10lpcd1);
			        
			        ajaxwaterTestingModel.setFlag1(true);
			        
					/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				}
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterTestingModel> dailyWaterTestingModels2 = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterTestingModels2 != null && dailyWaterTestingModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found());
						
						// For Progress Report Current Value
						currentLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found_frc());
						
						// For Progress Report Previous Value
						previousLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found_frc());
									
					}
				}
				
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
			} else if(flag == 3) {
				
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterTestingModels != null && dailyWaterTestingModels.size() > 0) {
					
				bacteriologicalBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found();
				frcBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found_frc();
				
				lpcdTotal = new LinkedHashMap<String, Double>();
				lpcdTotal1 = new LinkedHashMap<String, Double>();
				
				if(dailyWaterTestingModels.size() > 2 && dailyWaterTestingModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterTestingModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
				
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = dailyWaterTestingModels.get(i+1);
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
								
						} else {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found_frc());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i++) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						
						// For Bacteriological Sample Found
						lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
						
						// For FRC Sample Found
						lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
					}
				}
			}
				
				LinkedList<DailyWaterTestingModel> mapDailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterTestingModels != null && mapDailyWaterTestingModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterTestingModel>();
					
					for(int i = 0; i<mapDailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = mapDailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = mapDailyWaterTestingModels.get(i+1);
						DailyWaterTestingModel dailyWaterTestingModel3 = new DailyWaterTestingModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
					    ULBModel ulbModel = new ULBModel();
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel1.getPercentage_found());
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel2.getPm_percentage_found());
							
							dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel1.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel2.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						} else {
								
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel1.getPm_percentage_found());									
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel2.getPercentage_found());
							
					        dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel2.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel1.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);						        
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						}
					}
					
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyWaterTestingModel dailyWaterTestingModel4 = lpcdMap.get(entry1.getKey());
					dailyWaterTestingModel4.setTotal_percentage_found(entry1.getValue());
					dailyWaterTestingModel4.setTotal_percentage_found_frc(lpcdTotal1.get(entry1.getKey()));
		        	dailyWaterTestingModel4.setCurrentMonth(currentMonth);
		        	dailyWaterTestingModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterTestingModel4);
					
				}

				ajaxwaterTestingModel.setBacteriologicalBoxValue(bacteriologicalBoxValue);
				ajaxwaterTestingModel.setFrcBoxValue(frcBoxValue);
				ajaxwaterTestingModel.setLpcdMap(lpcdMap);
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal, lpcdTotal.size());
				
				LinkedList<String> topulb = new LinkedList<String>();
		        LinkedList<Double> toplpcd = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb.add(entry1.getKey());
		        		toplpcd.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb.add(entry1.getKey());
		        		bottomlpcd.add(entry1.getValue());
		        	}
		        }
		        
		        ajaxwaterTestingModel.setTopulb(topulb);
		        ajaxwaterTestingModel.setToplpcd(toplpcd);
		        
		        ajaxwaterTestingModel.setBottomulb(bottomulb);
		        ajaxwaterTestingModel.setBottomlpcd(bottomlpcd);
		        
		        ajaxwaterTestingModel.setFlag(false);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
				/* ----------------------- For All ULB Graphs Starts ------------------------------- */
				
				HashMap<String, Double> allUlbSortedGraph1 = (HashMap<String, Double>) sortingMap.findTop10LpcdUlbs(lpcdTotal1, lpcdTotal1.size());
				
				LinkedList<String> topulb1 = new LinkedList<String>();
		        LinkedList<Double> toplpcd1 = new LinkedList<Double>();
		        
		        LinkedList<String> bottomulb1 = new LinkedList<String>();
		        LinkedList<Double> bottomlpcd1 = new LinkedList<Double>();
		        
		        for (Entry<String, Double> entry1 : allUlbSortedGraph1.entrySet()) {
		        	
		        	//System.out.println("entry1.getKey() = "+entry1.getKey()+"  ---- entry1.getValue() = "+entry1.getValue());
		        	
		        	if(entry1.getValue() >= 100) {
		        		
		        		topulb1.add(entry1.getKey());
		        		toplpcd1.add(entry1.getValue());
		        		
		        	} else {
		        		
		        		bottomulb1.add(entry1.getKey());
		        		bottomlpcd1.add(entry1.getValue());
		        	}
		        }
		        
		        ajaxwaterTestingModel.setTopulb1(topulb1);
		        ajaxwaterTestingModel.setToplpcd1(toplpcd1);
		        
		        ajaxwaterTestingModel.setBottomulb1(bottomulb1);
		        ajaxwaterTestingModel.setBottomlpcd1(bottomlpcd1);
		        
		        ajaxwaterTestingModel.setFlag1(false);
				
				/* -----------------------  For All ULB Graphs Ends ------------------------------- */
				
		        datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterTestingModel> dailyWaterTestingModels2 = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterTestingModels2 != null && dailyWaterTestingModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found());
						
						// For Progress Report Current Value
						currentLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found_frc());
						
						// For Progress Report Previous Value
						previousLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found_frc());
									
					}
				}
				
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
				
			} else if(flag == 4) {
				
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
				
				listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId,startDate, endDate);
				
				if(dailyWaterTestingModels != null && dailyWaterTestingModels.size() > 0) {
					
				bacteriologicalBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found();
				frcBoxValue = dailyWaterTestingModels.get(dailyWaterTestingModels.size() - 1).getTotal_percentage_found_frc();
				
				lpcdTotal = new LinkedHashMap<String, Double>();
				lpcdTotal1 = new LinkedHashMap<String, Double>();
				
				if(dailyWaterTestingModels.size() > 2 && dailyWaterTestingModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterTestingModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
				
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = dailyWaterTestingModels.get(i+1);
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
								
						} else {
								
							// For Bacteriological Sample Found
							lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found());
							
							// For FRC Sample Found
							lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel2.getPercentage_found_frc());
						}
					}
				} else {
					
					for(int i = 0; i<dailyWaterTestingModels.size() - 1; i++) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
						
						// For Bacteriological Sample Found
						lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found());
						
						// For FRC Sample Found
						lpcdTotal1.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel1.getPercentage_found_frc());
					}
				}
			}
				
				LinkedList<DailyWaterTestingModel> mapDailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDateMap, endDateMap);
				
				if(mapDailyWaterTestingModels != null && mapDailyWaterTestingModels.size() > 0) {
						
					lpcdMap = new LinkedHashMap<String, DailyWaterTestingModel>();
					
					for(int i = 0; i<mapDailyWaterTestingModels.size() - 1; i=i+2) {
						
						DailyWaterTestingModel dailyWaterTestingModel1 = mapDailyWaterTestingModels.get(i);
						DailyWaterTestingModel dailyWaterTestingModel2 = mapDailyWaterTestingModels.get(i+1);
						DailyWaterTestingModel dailyWaterTestingModel3 = new DailyWaterTestingModel();
						ULBNameModel ulbNameModel = new ULBNameModel();
					    ULBModel ulbModel = new ULBModel();
					    
						if(dailyWaterTestingModel1.getType().equals("current")) {
								
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel1.getPercentage_found());
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel2.getPm_percentage_found());
							
							dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel1.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel2.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						} else {
								
							dailyWaterTestingModel3.setPm_percentage_found(dailyWaterTestingModel1.getPm_percentage_found());									
							dailyWaterTestingModel3.setPercentage_found(dailyWaterTestingModel2.getPercentage_found());
							
					        dailyWaterTestingModel3.setPercentage_found_frc(dailyWaterTestingModel2.getPercentage_found_frc());
							dailyWaterTestingModel3.setPm_percentage_found_frc(dailyWaterTestingModel1.getPm_percentage_found_frc());
							
							ulbModel.setLatitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLatitude());
					        ulbModel.setLongitude(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getLongitude());
					        ulbNameModel.setUlbModel(ulbModel);
					        dailyWaterTestingModel3.setUlbNameModel(ulbNameModel);						        
							
							// For Map
							lpcdMap.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterTestingModel3);
							
						}
					}
					
				}
				
				for (Entry<String, Double> entry1 : lpcdTotal.entrySet()) {
		        	
					DailyWaterTestingModel dailyWaterTestingModel4 = lpcdMap.get(entry1.getKey());
					dailyWaterTestingModel4.setTotal_percentage_found(entry1.getValue());
					dailyWaterTestingModel4.setTotal_percentage_found_frc(lpcdTotal1.get(entry1.getKey()));
		        	dailyWaterTestingModel4.setCurrentMonth(currentMonth);
		        	dailyWaterTestingModel4.setPreviousMonth(previousMonth);
		        	
		        	lpcdMap.put(entry1.getKey(), dailyWaterTestingModel4);
					
				}

				ajaxwaterTestingModel.setBacteriologicalBoxValue(bacteriologicalBoxValue);
				ajaxwaterTestingModel.setFrcBoxValue(frcBoxValue);
				ajaxwaterTestingModel.setLpcdMap(lpcdMap);
				
				LinkedList<String> top10ulb = new LinkedList<String>();
				LinkedList<Double> top10lpcd = new LinkedList<Double>();

				LinkedList<String> bottom10ulb = new LinkedList<String>();
				LinkedList<Double> bottom10lpcd = new LinkedList<Double>();

				LinkedList<String> topulb = new LinkedList<String>();
				LinkedList<Double> toplpcd = new LinkedList<Double>();
				
				LinkedList<String> bottomulb = new LinkedList<String>();
				LinkedList<Double> bottomlpcd = new LinkedList<Double>();
				
				LinkedList<String> top10ulb1 = new LinkedList<String>();
				LinkedList<Double> top10lpcd1 = new LinkedList<Double>();

				LinkedList<String> bottom10ulb1 = new LinkedList<String>();
				LinkedList<Double> bottom10lpcd1 = new LinkedList<Double>();

				LinkedList<String> topulb1 = new LinkedList<String>();
				LinkedList<Double> toplpcd1 = new LinkedList<Double>();
				
				LinkedList<String> bottomulb1 = new LinkedList<String>();
				LinkedList<Double> bottomlpcd1 = new LinkedList<Double>();
				
				ajaxwaterTestingModel.setTopulb(topulb);
		        ajaxwaterTestingModel.setToplpcd(toplpcd);
		        
		        ajaxwaterTestingModel.setBottomulb(bottomulb);
		        ajaxwaterTestingModel.setBottomlpcd(bottomlpcd);
		        
		        ajaxwaterTestingModel.setTop10ulb(top10ulb);
		        ajaxwaterTestingModel.setTop10lpcd(top10lpcd);
		        
		        ajaxwaterTestingModel.setBottom10ulb(bottom10ulb);
		        ajaxwaterTestingModel.setBottom10lpcd(bottom10lpcd);
		        
		        ajaxwaterTestingModel.setTopulb1(topulb1);
		        ajaxwaterTestingModel.setToplpcd1(toplpcd1);
		        
		        ajaxwaterTestingModel.setBottomulb1(bottomulb1);
		        ajaxwaterTestingModel.setBottomlpcd1(bottomlpcd1);
		        
		        ajaxwaterTestingModel.setTop10ulb1(top10ulb1);
		        ajaxwaterTestingModel.setTop10lpcd1(top10lpcd1);
		        
		        ajaxwaterTestingModel.setBottom10ulb1(bottom10ulb1);
		        ajaxwaterTestingModel.setBottom10lpcd1(bottom10lpcd1);
		        
		        ajaxwaterTestingModel.setFlag(false);
		        ajaxwaterTestingModel.setFlag1(false);
				
				datesBetween = dateCalculation.findDatesBetween(startDate1, endDate1);	
				
				for(String date:datesBetween) {
					
					LinkedList<DailyWaterTestingModel> dailyWaterTestingModels2 = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, date, null);
					
					if(dailyWaterTestingModels2 != null && dailyWaterTestingModels2.size() > 0) {
						
						// For Progress Report Current Value
						currentLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found());
						
						// For Progress Report Previous Value
						previousLPCD.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found());
						
						// For Progress Report Current Value
						currentLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getTotal_percentage_found_frc());
						
						// For Progress Report Previous Value
						previousLPCD1.add(dailyWaterTestingModels2.get(dailyWaterTestingModels2.size() - 1).getPm_total_percentage_found_frc());
									
					}
				}
				
				/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
			}
				
				ajaxwaterTestingModel.setDatesBetween(datesBetween);
				ajaxwaterTestingModel.setCurrentLPCD(currentLPCD);
				ajaxwaterTestingModel.setPreviousLPCD(previousLPCD);
				ajaxwaterTestingModel.setCurrentLPCD1(currentLPCD1);
				ajaxwaterTestingModel.setPreviousLPCD1(previousLPCD1);
				
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		if (logger.isInfoEnabled()) {
			logger.info("ajaxwaterTesting -- END");
		}

		return ajaxwaterTestingModel;

	}
	
}
