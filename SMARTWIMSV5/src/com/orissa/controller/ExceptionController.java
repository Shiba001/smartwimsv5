/**
 * @formatter:off
 *
 */
package com.orissa.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.orissa.common.DateCalculation;
import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.AjaxDashboardModel;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.DailyWaterTestingModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.TargetModel;
import com.orissa.service.DashboardService;
import com.orissa.service.DivisionService;
import com.orissa.service.ReportService;
import com.orissa.validation.DashboardValidation;

@Controller
public class ExceptionController {

	private static Logger logger = Logger.getLogger(ExceptionController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private ReportService reportService;
	
	@Autowired
	private DateCalculation dateCalculation;
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private DashboardValidation dashboardValidation;
	
	@RequestMapping(value = "/ajax-complaints-report", method = RequestMethod.GET)
	public String complaintsAJAXReportPage(HttpServletRequest httpServletRequest,
			@ModelAttribute ListingModel listingModel, Model model,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate) {

		if (logger.isInfoEnabled()) {
			logger.info("complaintsAJAXReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		try {
			
			
			dashboardValidation.reportValidation(listingModel, amrutTypeId, startDate, endDate);
		
		AjaxDashboardModel ajaxDashboardModel = new AjaxDashboardModel();
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxDashboardModel.setPhDivisionModels(phDivisionModels);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if(amrutTypeId != null) {
			
			httpSession.setAttribute("amrutTypeId", amrutTypeId);
		}
		
		try {
			
			List<String> datesBetween = new LinkedList<String>();
			List<String> datesBetweenValue = new LinkedList<String>();
			LinkedHashMap<String, List<Double>> lpcdTotal = null;
			Map<String, LinkedHashMap<String, List<Double>>> reportValues = new LinkedHashMap<String, LinkedHashMap<String, List<Double>>>();
			boolean flag1 = true;
			datesBetween = dateCalculation.findDatesBetween(startDate, endDate);
			
			try {
				System.out.println(new ObjectMapper().writeValueAsString(datesBetween));
				for(String date:datesBetween) {
					
					
					LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, date, null);
					
					if(flag1) {
						
					if(dailyComplaintsModels != null && dailyComplaintsModels.size() > 0) {
						
						lpcdTotal = new LinkedHashMap<String, List<Double>>();
						
						if(dailyComplaintsModels.size() > 2 && dailyComplaintsModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyComplaintsModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
							datesBetweenValue.add(date);
							for(int i = 0; i<dailyComplaintsModels.size() - 1; i=i+2) {
								
								List<Double> innerList = new ArrayList<Double>();
								DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
								DailyComplaintsModel dailyComplaintsModel2 = dailyComplaintsModels.get(i+1);
								
								if(dailyComplaintsModel1.getType().equals("current")) {
									
									innerList.add(dailyComplaintsModel1.getNo_ws_comp_resolved()/dailyComplaintsModel1.getNo_ws_comp_recv()*100);
									innerList.add(dailyComplaintsModel1.getNo_tw_comp_resolved()/dailyComplaintsModel1.getNo_tw_comp_recv()*100);
									
									// For Bacteriological Sample Found
									lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), innerList);
									
								} else {
										
									innerList.add(dailyComplaintsModel2.getNo_ws_comp_resolved()/dailyComplaintsModel2.getNo_ws_comp_recv()*100);
									innerList.add(dailyComplaintsModel2.getNo_tw_comp_resolved()/dailyComplaintsModel2.getNo_tw_comp_recv()*100);
									
									// For Bacteriological Sample Found
									lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), innerList);
								}
							}
						} else {
							
							for(int i = 0; i<dailyComplaintsModels.size() - 1; i++) {
								
								List<Double> innerList = new ArrayList<Double>();
								
								DailyComplaintsModel dailyComplaintsModel1 = dailyComplaintsModels.get(i);
								if(dailyComplaintsModel1.getNo_ws_comp_resolved() != null) {
									datesBetweenValue.add(date);
									innerList.add(dailyComplaintsModel1.getNo_ws_comp_resolved()/dailyComplaintsModel1.getNo_ws_comp_recv()*100);
									innerList.add(dailyComplaintsModel1.getNo_tw_comp_resolved()/dailyComplaintsModel1.getNo_tw_comp_recv()*100);
									
									// For Bacteriological Sample Found
									lpcdTotal.put(dailyComplaintsModel1.getUlbNameModel().getUlbModel().getUlb_name(), innerList);
								} else {
									
									flag1 = false;
									break;
								}
										
								
							}
						}
						
						reportValues.put(date, lpcdTotal);
					} 
					}
					else {
						
						break;
					}
				}
			} catch (DBConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(datesBetweenValue.size() == datesBetween.size()){
				datesBetweenValue = datesBetween;
			}
			Map<String, List<List<Double>>> ultimateMap = new LinkedHashMap<String, List<List<Double>>>();
			try {
				
				for(String date:datesBetweenValue) {
					for (Entry<String, List<Double>> innerMap : reportValues.get(date).entrySet())
					{
						if(ultimateMap.containsKey(innerMap.getKey())){
							List<List<Double>> ultimateList = ultimateMap.get(innerMap.getKey());
							ultimateList.add(innerMap.getValue());
						}
						else{
							List<List<Double>> ultimateList = new ArrayList<List<Double>>();
							ultimateList.add(innerMap.getValue());
							ultimateMap.put(innerMap.getKey(), ultimateList);
						}
					}
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			    
			model.addAttribute("datesBetween", datesBetweenValue);
			model.addAttribute("ultimateMap", ultimateMap);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("complaintsAJAXReportPage -- END");
		}

		return "ajax-complaints-report";

	}
	
	@RequestMapping(value = "/ajax-sample-testing-report", method = RequestMethod.GET)
	public String testingAJAXReportPage(HttpServletRequest httpServletRequest,
			@ModelAttribute ListingModel listingModel, Model model,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate) {

		if (logger.isInfoEnabled()) {
			logger.info("testingAJAXReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		try {
			
			
			dashboardValidation.reportValidation(listingModel, amrutTypeId, startDate, endDate);
			
		AjaxDashboardModel ajaxDashboardModel = new AjaxDashboardModel();
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxDashboardModel.setPhDivisionModels(phDivisionModels);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if(amrutTypeId != null) {
			
			httpSession.setAttribute("amrutTypeId", amrutTypeId);
		}
		
		try {
			
			List<String> datesBetween = new LinkedList<String>();
			List<String> datesBetweenValue = new LinkedList<String>();
			LinkedHashMap<String, List<Double>> lpcdTotal = null;
			Map<String, LinkedHashMap<String, List<Double>>> reportValues = new LinkedHashMap<String, LinkedHashMap<String, List<Double>>>();
			boolean flag1 = true;
			datesBetween = dateCalculation.findDatesBetween(startDate, endDate);
			System.out.println(new ObjectMapper().writeValueAsString(datesBetween)); 
			for(String date:datesBetween) {
				
				LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, date, null);
				
				if(flag1) {
				if(dailyWaterTestingModels != null && dailyWaterTestingModels.size() > 0) {
					
					lpcdTotal = new LinkedHashMap<String, List<Double>>();
					
					if(dailyWaterTestingModels.size() > 2 && dailyWaterTestingModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterTestingModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
						datesBetweenValue.add(date);
						for(int i = 0; i<dailyWaterTestingModels.size() - 1; i=i+2) {
							
							List<Double> innerList = new ArrayList<Double>();
							DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
							DailyWaterTestingModel dailyWaterTestingModel2 = dailyWaterTestingModels.get(i+1);
						    
							if(dailyWaterTestingModel1.getType().equals("current")) {
									
								innerList.add(dailyWaterTestingModel1.getPercentage_found());
								innerList.add(dailyWaterTestingModel1.getPercentage_found_frc());
								
								// For Bacteriological Sample Found
								lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), innerList);
								
							} else {
								
									
								innerList.add(dailyWaterTestingModel2.getPercentage_found());
								innerList.add(dailyWaterTestingModel2.getPercentage_found_frc());
								
								// For Bacteriological Sample Found
								lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), innerList);
							}
						}
					} else {
						TreeSet<String> treeSet = new TreeSet<String>();
						for(int i = 0; i<dailyWaterTestingModels.size() - 1; i++) {
							
							List<Double> innerList = new ArrayList<Double>();
							
							DailyWaterTestingModel dailyWaterTestingModel1 = dailyWaterTestingModels.get(i);
							if(dailyWaterTestingModel1.getPercentage_found() != null){
								treeSet.add(date);
								
								innerList.add(dailyWaterTestingModel1.getPercentage_found());
								innerList.add(dailyWaterTestingModel1.getPercentage_found_frc());
								
								// For Bacteriological Sample Found
								lpcdTotal.put(dailyWaterTestingModel1.getUlbNameModel().getUlbModel().getUlb_name(), innerList);
							} else {
								
								flag1 = false;
								break;
							}
						}
						
						datesBetweenValue.addAll(treeSet);
					}
					
					reportValues.put(date, lpcdTotal);
				}
				}
				else {
					
					break;
				}
			}
			if(datesBetweenValue.size() == datesBetween.size()){
				datesBetweenValue = datesBetween;
			}
			Map<String, List<List<Double>>> ultimateMap = new LinkedHashMap<String, List<List<Double>>>();
			
			for(String date:datesBetweenValue) {
				for (Entry<String, List<Double>> innerMap : reportValues.get(date).entrySet())
				{
					if(ultimateMap.containsKey(innerMap.getKey())){
						List<List<Double>> ultimateList = ultimateMap.get(innerMap.getKey());
						ultimateList.add(innerMap.getValue());
					}
					else{
						List<List<Double>> ultimateList = new ArrayList<List<Double>>();
						ultimateList.add(innerMap.getValue());
						ultimateMap.put(innerMap.getKey(), ultimateList);
					}
				}
			}
			    
			model.addAttribute("datesBetween", datesBetweenValue);
			model.addAttribute("ultimateMap", ultimateMap);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("testingAJAXReportPage -- END");
		}

		return "ajax-sample-testing-report";

	}
	
	@RequestMapping(value = "/ajax-charges-report", method = RequestMethod.GET)
	public String chargesAJAXReportPage(HttpServletRequest httpServletRequest,
			@ModelAttribute ListingModel listingModel, Model model,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate) {

		if (logger.isInfoEnabled()) {
			logger.info("chargesAJAXReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		try {
			
			
			dashboardValidation.reportValidation(listingModel, amrutTypeId, startDate, endDate);
			
		AjaxDashboardModel ajaxDashboardModel = new AjaxDashboardModel();
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxDashboardModel.setPhDivisionModels(phDivisionModels);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if(amrutTypeId != null) {
			
			httpSession.setAttribute("amrutTypeId", amrutTypeId);
		}
		
		try {
			
			LinkedList<TargetModel> targetModels = dashboardService.fetchTargetWaterChargesTaxReport(listingModel, amrutTypeId, startDate, endDate);
			
			model.addAttribute("targetModels", targetModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("chargesAJAXReportPage -- END");
		}

		return "ajax-charges-report";

	}
	
	@RequestMapping(value = "/ajax-average-report", method = RequestMethod.GET)
	public String averageAJAXReportPage(HttpServletRequest httpServletRequest,
			@ModelAttribute ListingModel listingModel, Model model,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate) {

		if (logger.isInfoEnabled()) {
			logger.info("averageAJAXReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		double avgHrOfSupplyBoxValue = 0.0D;
		try {
			
			
			dashboardValidation.reportValidation(listingModel, amrutTypeId, startDate, endDate);
			
		AjaxDashboardModel ajaxDashboardModel = new AjaxDashboardModel();
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxDashboardModel.setPhDivisionModels(phDivisionModels);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if(amrutTypeId != null) {
			
			httpSession.setAttribute("amrutTypeId", amrutTypeId);
		}
		
		try {
			
			List<String> datesBetween = new LinkedList<String>();
			LinkedHashMap<String, Double> lpcdTotal = null;
			Map<String, LinkedHashMap<String, Double>> reportValues = new LinkedHashMap<String, LinkedHashMap<String, Double>>();
			List<Double> totalValue = new ArrayList<Double>();
			
			datesBetween = dateCalculation.findDatesBetween(startDate, endDate);
			
			for(String date:datesBetween) {
				
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
					avgHrOfSupplyBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();
					
					model.addAttribute("avgHrOfSupplyBoxValue", avgHrOfSupplyBoxValue);
					
					totalValue.add(dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply());
					lpcdTotal = new LinkedHashMap<String, Double>();
					
					if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getFreq_supply());
							}
						}
					} else {
						
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getFreq_supply());
						}
					}
					reportValues.put(date, lpcdTotal);
				}
			}
			
			Map<String, List<Double>> ultimateMap = new LinkedHashMap<String, List<Double>>();
			
			for(String date:datesBetween) {
				for (Entry<String, Double> innerMap : reportValues.get(date).entrySet())
				{
					if(ultimateMap.containsKey(innerMap.getKey())){
						List<Double> ultimateList = ultimateMap.get(innerMap.getKey());
						ultimateList.add(innerMap.getValue());
					}
					else{
						List<Double> ultimateList = new ArrayList<Double>();
						ultimateList.add(innerMap.getValue());
						ultimateMap.put(innerMap.getKey(), ultimateList);
					}
				}
			}
			    
			model.addAttribute("totalValue", totalValue);
			model.addAttribute("datesBetween", datesBetween);
			model.addAttribute("ultimateMap", ultimateMap);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("averageAJAXReportPage -- END");
		}

		return "ajax-average-report";

	}
	
	@RequestMapping(value = "/ajax-deficit-report", method = RequestMethod.GET)
	public String deficitAJAXReportPage(HttpServletRequest httpServletRequest,
			@ModelAttribute ListingModel listingModel, Model model,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate) {

		if (logger.isInfoEnabled()) {
			logger.info("deficitAJAXReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		try {
			
			
			dashboardValidation.reportValidation(listingModel, amrutTypeId, startDate, endDate);
			
		AjaxDashboardModel ajaxDashboardModel = new AjaxDashboardModel();
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxDashboardModel.setPhDivisionModels(phDivisionModels);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if(amrutTypeId != null) {
			
			httpSession.setAttribute("amrutTypeId", amrutTypeId);
		}
		
		try {
			
			List<String> datesBetween = new LinkedList<String>();
			LinkedHashMap<String, Double> lpcdTotal = null;
			Map<String, LinkedHashMap<String, Double>> reportValues = new LinkedHashMap<String, LinkedHashMap<String, Double>>();
			List<Double> totalValue = new ArrayList<Double>();
			
			datesBetween = dateCalculation.findDatesBetween(startDate, endDate);
			
			for(String date:datesBetween) {
				
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
					totalValue.add(dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_surplus_deficit());
					lpcdTotal = new LinkedHashMap<String, Double>();
					
					if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getSurplus_deficit());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getSurplus_deficit());
							}
						}
					} else {
						
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getSurplus_deficit());
						}
					}
					reportValues.put(date, lpcdTotal);
				}
			}
			
			Map<String, List<Double>> ultimateMap = new LinkedHashMap<String, List<Double>>();
			
			for(String date:datesBetween) {
				for (Entry<String, Double> innerMap : reportValues.get(date).entrySet())
				{
					if(ultimateMap.containsKey(innerMap.getKey())){
						List<Double> ultimateList = ultimateMap.get(innerMap.getKey());
						ultimateList.add(innerMap.getValue());
					}
					else{
						List<Double> ultimateList = new ArrayList<Double>();
						ultimateList.add(innerMap.getValue());
						ultimateMap.put(innerMap.getKey(), ultimateList);
					}
				}
			}
			    
			model.addAttribute("totalValue", totalValue);
			model.addAttribute("datesBetween", datesBetween);
			model.addAttribute("ultimateMap", ultimateMap);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("deficitAJAXReportPage -- END");
		}

		return "ajax-deficit-report";

	}
	
	@RequestMapping(value = "/ajax-coverage-report", method = RequestMethod.GET)
	public String coverageAJAXReportPage(HttpServletRequest httpServletRequest,
			@ModelAttribute ListingModel listingModel, Model model,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate) {

		if (logger.isInfoEnabled()) {
			logger.info("coverageAJAXReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		try {
			
			
			dashboardValidation.reportValidation(listingModel, amrutTypeId, startDate, endDate);
			
		AjaxDashboardModel ajaxDashboardModel = new AjaxDashboardModel();
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxDashboardModel.setPhDivisionModels(phDivisionModels);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if(amrutTypeId != null) {
			
			httpSession.setAttribute("amrutTypeId", amrutTypeId);
		}
		
		try {
			
			List<String> datesBetween = new LinkedList<String>();
			LinkedHashMap<String, Double> lpcdTotal = null;
			Map<String, LinkedHashMap<String, Double>> reportValues = new LinkedHashMap<String, LinkedHashMap<String, Double>>();
			List<Double> totalValue = new ArrayList<Double>();
			
			datesBetween = dateCalculation.findDatesBetween(startDate, endDate);
			
			for(String date:datesBetween) {
				
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
					totalValue.add(dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_coverage_by_pws());
					lpcdTotal = new LinkedHashMap<String, Double>();
					
					if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getCoverage_by_pws());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getCoverage_by_pws());
							}
						}
					} else {
						
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getCoverage_by_pws());
						}
					}
					reportValues.put(date, lpcdTotal);
				}
			}
			
			Map<String, List<Double>> ultimateMap = new LinkedHashMap<String, List<Double>>();
			
			for(String date:datesBetween) {
				for (Entry<String, Double> innerMap : reportValues.get(date).entrySet())
				{
					if(ultimateMap.containsKey(innerMap.getKey())){
						List<Double> ultimateList = ultimateMap.get(innerMap.getKey());
						ultimateList.add(innerMap.getValue());
					}
					else{
						List<Double> ultimateList = new ArrayList<Double>();
						ultimateList.add(innerMap.getValue());
						ultimateMap.put(innerMap.getKey(), ultimateList);
					}
				}
			}
			    
			model.addAttribute("totalValue", totalValue);
			model.addAttribute("datesBetween", datesBetween);
			model.addAttribute("ultimateMap", ultimateMap);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("coverageAJAXReportPage -- END");
		}

		return "ajax-coverage-report";

	}
	
	@RequestMapping(value = "/ajax-lpcd-report", method = RequestMethod.GET)
	public String LPCDAJAXReportPage(HttpServletRequest httpServletRequest,
			@ModelAttribute ListingModel listingModel, Model model,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate) {

		if (logger.isInfoEnabled()) {
			logger.info("LPCDAJAXReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		try {
			
			
			dashboardValidation.reportValidation(listingModel, amrutTypeId, startDate, endDate);
			
		AjaxDashboardModel ajaxDashboardModel = new AjaxDashboardModel();
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxDashboardModel.setPhDivisionModels(phDivisionModels);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if(amrutTypeId != null) {
			
			httpSession.setAttribute("amrutTypeId", amrutTypeId);
		}
		
		try {
			
			List<String> datesBetween = new LinkedList<String>();
			LinkedHashMap<String, Double> lpcdTotal = null;
			Map<String, LinkedHashMap<String, Double>> reportValues = new LinkedHashMap<String, LinkedHashMap<String, Double>>();
			List<Double> totalValue = new ArrayList<Double>();
			
			datesBetween = dateCalculation.findDatesBetween(startDate, endDate);
			
			for(String date:datesBetween) {
				
				LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, date, null);
				
				if(dailyWaterSupplyModels != null && dailyWaterSupplyModels.size() > 0) {
					
					totalValue.add(dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_rate_of_supply());
					lpcdTotal = new LinkedHashMap<String, Double>();
					
					if(dailyWaterSupplyModels.size() > 2 && dailyWaterSupplyModels.get(0).getUlbNameModel().getUlbModel().getUlb_name().equals(dailyWaterSupplyModels.get(1).getUlbNameModel().getUlbModel().getUlb_name())) {
					
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i=i+2) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							DailyWaterSupplyModel dailyWaterSupplyModel2 = dailyWaterSupplyModels.get(i+1);
							
							if(dailyWaterSupplyModel1.getType().equals("current")) {
							
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
								
							} else {
								
								// For All LPCD
								lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel2.getRate_of_supply());
							}
						}
					} else {
						
						for(int i = 0; i<dailyWaterSupplyModels.size() - 1; i++) {
							
							DailyWaterSupplyModel dailyWaterSupplyModel1 = dailyWaterSupplyModels.get(i);
							
							// For All LPCD
							lpcdTotal.put(dailyWaterSupplyModel1.getUlbNameModel().getUlbModel().getUlb_name(), dailyWaterSupplyModel1.getRate_of_supply());
						}
					}
					reportValues.put(date, lpcdTotal);
				}
			}
			
			Map<String, List<Double>> ultimateMap = new LinkedHashMap<String, List<Double>>();
			
			for(String date:datesBetween) {
				for (Entry<String, Double> innerMap : reportValues.get(date).entrySet())
				{
					if(ultimateMap.containsKey(innerMap.getKey())){
						List<Double> ultimateList = ultimateMap.get(innerMap.getKey());
						ultimateList.add(innerMap.getValue());
					}
					else{
						List<Double> ultimateList = new ArrayList<Double>();
						ultimateList.add(innerMap.getValue());
						ultimateMap.put(innerMap.getKey(), ultimateList);
					}
				}
			}
			    
			model.addAttribute("totalValue", totalValue);
			model.addAttribute("datesBetween", datesBetween);
			model.addAttribute("ultimateMap", ultimateMap);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey()
						+ " value :: " + entry.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("LPCDAJAXReportPage -- END");
		}

		return "ajax-lpcd-report";

	}
	
	@RequestMapping(value = "/complaints-report", method = RequestMethod.GET)
	public String complaintsReportPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("complaintsReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		ListingModel listingModel = new ListingModel();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("loginUsersModel", loginUsersModel);

		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		
		model.addAttribute("startDate", startDate);
		
		try {
			
			LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardService.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
			
			model.addAttribute("dailyComplaintsModels", dailyComplaintsModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("complaintsReportPage -- END");
		}

		return "complaints-report";

	}
	
	@RequestMapping(value = "/testing-report", method = RequestMethod.GET)
	public String testingReportPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("testingReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		ListingModel listingModel = new ListingModel();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("loginUsersModel", loginUsersModel);

		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		
		model.addAttribute("startDate", startDate);
		
		try {
			
			LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardService.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDate, endDate);
			
			model.addAttribute("dailyWaterTestingModels", dailyWaterTestingModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("testingReportPage -- END");
		}

		return "samples-report";

	}
	
	@RequestMapping(value = "/charges-report", method = RequestMethod.GET)
	public String chargesReportPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("chargesReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		ListingModel listingModel = new ListingModel();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("loginUsersModel", loginUsersModel);

		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		
		model.addAttribute("startDate", startDate);
		
		try {
			
			LinkedList<TargetModel> targetModels = dashboardService.fetchTargetWaterChargesTaxReport(listingModel, amrutTypeId, startDate, endDate);
			
			model.addAttribute("targetModels", targetModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("chargesReportPage -- END");
		}

		return "charges-report";

	}
	
	@RequestMapping(value = "/average-report", method = RequestMethod.GET)
	public String averageReportPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("averageReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		ListingModel listingModel = new ListingModel();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		String amrutTypeId = null;
		double avgHrOfSupplyBoxValue = 0.0D;
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("loginUsersModel", loginUsersModel);

		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		
		model.addAttribute("startDate", startDate);
		
		try {
			
			LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
			avgHrOfSupplyBoxValue = dailyWaterSupplyModels.get(dailyWaterSupplyModels.size() - 1).getTotal_Freq_supply();

			model.addAttribute("avgHrOfSupplyBoxValue", avgHrOfSupplyBoxValue);
			model.addAttribute("dailyWaterSupplyModels", dailyWaterSupplyModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("averageReportPage -- END");
		}

		return "average-report";

	}
	
	@RequestMapping(value = "/deficit-report", method = RequestMethod.GET)
	public String deficitReportPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("deficitReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		ListingModel listingModel = new ListingModel();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("loginUsersModel", loginUsersModel);

		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		
		model.addAttribute("startDate", startDate);
		
		try {
			
			LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
			
			model.addAttribute("dailyWaterSupplyModels", dailyWaterSupplyModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("deficitReportPage -- END");
		}

		return "deficit-report";

	}
	
	@RequestMapping(value = "/coverage-report", method = RequestMethod.GET)
	public String coverageReportPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("coverageReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		ListingModel listingModel = new ListingModel();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("loginUsersModel", loginUsersModel);

		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		
		model.addAttribute("startDate", startDate);
		
		try {
			
			LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
			
			model.addAttribute("dailyWaterSupplyModels", dailyWaterSupplyModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("coverageReportPage -- END");
		}

		return "coverage-report";

	}
	
	@RequestMapping(value = "/lpcd-report", method = RequestMethod.GET)
	public String LPCDReportPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("LPCDReportPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		ListingModel listingModel = new ListingModel();
		
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		if(flag == 1) {
			
			/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
					
				
			/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
				
					} else if(flag == 2) {
					
			/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
					
						listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					
			/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
					
					} else if(flag == 3) {
					
			/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
					
						listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					
			/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
						
					} else if(flag == 4) {
						
			/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
						
						listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
						
			/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
					}
		
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("loginUsersModel", loginUsersModel);

		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		
		model.addAttribute("startDate", startDate);
		
		try {
			
			LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardService.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
			
			model.addAttribute("dailyWaterSupplyModels", dailyWaterSupplyModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("LPCDReportPage -- END");
		}

		return "lpcd-report";

	}
	
	@RequestMapping(value = "/exception-report", method = RequestMethod.GET)
	public String exceptionReportPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportPage -- START");
		}
		
		
		
		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportPage -- END");
		}

		return "exception-report";

	}
	
	@RequestMapping(value = "/report-water-supply", method = RequestMethod.GET)
	public String exceptionReportWaterSupplyPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportWaterSupplyPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		String amrutTypeId = null;
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
		
		
		String tableName6 = "daily_water_supply";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryWSReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName6);
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				model.addAttribute("dataEntryWSReport", dataEntryWSReport);
				model.addAttribute("datesBetween", datesBetween);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportWaterSupplyPage -- END");
		}

		return "report-water-supply";

	}
	
	@RequestMapping(value = "/report-daily-tanker", method = RequestMethod.GET)
	public String exceptionReportTankerPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportTankerPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		String amrutTypeId = null;
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
		
		
		String tableName4 = "daily_tankers";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryTReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName4);
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				model.addAttribute("dataEntryTReport", dataEntryTReport);
				model.addAttribute("datesBetween", datesBetween);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportTankerPage -- END");
		}

		return "report-daily-tanker";

	}
	
	@RequestMapping(value = "/report-daily-complaint", method = RequestMethod.GET)
	public String exceptionReportComplaintPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportComplaintPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		String amrutTypeId = null;
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
		String tableName1 = "daily_complaints";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryComplaintsReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName1);
				
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				
				/*for (Entry<String, LinkedList<String>> entry1 : dataEntryComplaintsReport.entrySet()) {
					 
					System.out.println("Outer Map Key :: " + entry1.getKey() + "....... value :: "+ entry1.getValue());
				}*/
				
				model.addAttribute("dataEntryComplaintsReport", dataEntryComplaintsReport);
				
				model.addAttribute("datesBetween", datesBetween);
				
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportComplaintPage -- END");
		}

		return "report-daily-complaint";

	}
	
	@RequestMapping(value = "/report-daily-connection", method = RequestMethod.GET)
	public String exceptionReportConnectionPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportConnectionPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		String amrutTypeId = null;
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
	
		
		String tableName2 = "daily_connection";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryConnectionReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName2);
				
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				
				model.addAttribute("dataEntryConnectionReport", dataEntryConnectionReport);
				model.addAttribute("datesBetween", datesBetween);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportPage -- END");
		}

		return "report-daily-connection";

	}
	
	@RequestMapping(value = "/report-daily-watertesting", method = RequestMethod.GET)
	public String exceptionReportWaterTestingPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportWaterTestingPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		String amrutTypeId = null;
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
		
		
		String tableName3 = "daily_water_testing";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryWTReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName3);
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				
				model.addAttribute("dataEntryWTReport", dataEntryWTReport);
				model.addAttribute("datesBetween", datesBetween);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportWaterTestingPage -- END");
		}

		return "report-daily-watertesting";

	}
	
	@RequestMapping(value = "/report-daily-charges-tax", method = RequestMethod.GET)
	public String exceptionReportChargesPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportChargesPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		String amrutTypeId = null;
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		model.addAttribute("startDate", startDate);
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
		
		
		String tableName5 = "daily_water_charges_tax";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryWCTReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName5);
				
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				
				model.addAttribute("dataEntryWCTReport", dataEntryWCTReport);
				model.addAttribute("datesBetween", datesBetween);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportChargesPage -- END");
		}

		return "report-daily-charges-tax";

	}
	
	@RequestMapping(value = "/ajax-water-supply-report", method = RequestMethod.GET)
	public String ajaxWaterSupplyReportPage(final Model model, @ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxWaterSupplyReportPage -- START");
		}
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		String startDate = null;
		String endDate = null;
		String days = null;
		
		if (month != null) {
			List<String> dates = null;
			try {
				dates = new DateCalculation().findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(1);
			endDate = dates.get(1);
			days = dates.get(2);
			
		} else {
			
			startDate = listingModel.getDate();
			
		}
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
		String tableName6 = "daily_water_supply";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryWSReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName6);
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				model.addAttribute("dataEntryWSReport", dataEntryWSReport);
				model.addAttribute("datesBetween", datesBetween);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		if (logger.isInfoEnabled()) {
			logger.info("ajaxWaterSupplyReportPage -- END");
		}

		return "ajax-water-supply-report";

	}
	
	@RequestMapping(value = "/ajax-tanker-report", method = RequestMethod.GET)
	public String ajaxTankerReportPage(final Model model, @ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxTankerReportPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		String startDate = null;
		String endDate = null;
		String days = null;
		
		if (month != null) {
			List<String> dates = null;
			try {
				dates = new DateCalculation().findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(1);
			endDate = dates.get(1);
			days = dates.get(2);
			
		} else {
			
			startDate = listingModel.getDate();
			
		}
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				String tableName4 = "daily_tankers";		
	try {
				LinkedHashMap<String,LinkedList<String>> dataEntryTReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName4);
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				model.addAttribute("dataEntryTReport", dataEntryTReport);
				model.addAttribute("datesBetween", datesBetween);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		if (logger.isInfoEnabled()) {
			logger.info("ajaxTankerReportPage -- END");
		}

		return "ajax-tanker-report";

	}
	
	@RequestMapping(value = "/ajax-connection-report", method = RequestMethod.GET)
	public String ajaxConnectionReportPage(final Model model, @ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxConnectionReportPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		String startDate = null;
		String endDate = null;
		String days = null;
		
		if (month != null) {
			List<String> dates = null;
			try {
				dates = new DateCalculation().findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(1);
			endDate = dates.get(1);
			days = dates.get(2);
			
		} else {
			
			startDate = listingModel.getDate();
			
		}
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
		String tableName2 = "daily_connection";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryConnectionReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName2);
				
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				
				model.addAttribute("dataEntryConnectionReport", dataEntryConnectionReport);
				model.addAttribute("datesBetween", datesBetween);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if (logger.isInfoEnabled()) {
			logger.info("ajaxConnectionReportPage -- END");
		}

		return "ajax-connection-report";

	}
	
	@RequestMapping(value = "/ajax-complaint-report", method = RequestMethod.GET)
	public String ajaxComplaintReportPage(final Model model, @ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxComplaintReportPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		String startDate = null;
		String endDate = null;
		String days = null;
		
		if (month != null) {
			List<String> dates = null;
			try {
				dates = new DateCalculation().findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(1);
			endDate = dates.get(1);
			days = dates.get(2);
			
		} else {
			
			startDate = listingModel.getDate();
			
		}
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
		String tableName1 = "daily_complaints";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryComplaintsReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName1);
				
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				
				/*for (Entry<String, LinkedList<String>> entry1 : dataEntryComplaintsReport.entrySet()) {
					 
					System.out.println("Outer Map Key :: " + entry1.getKey() + "....... value :: "+ entry1.getValue());
				}*/
				
				model.addAttribute("dataEntryComplaintsReport", dataEntryComplaintsReport);
				
				model.addAttribute("datesBetween", datesBetween);
				
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if (logger.isInfoEnabled()) {
			logger.info("ajaxComplaintReportPage -- END");
		}

		return "ajax-complaint-report";

	}
	
	@RequestMapping(value = "/ajax-testing-report", method = RequestMethod.GET)
	public String ajaxTestingReportPage(final Model model, @ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxWaterSupplyReportPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		String startDate = null;
		String endDate = null;
		String days = null;
		
		if (month != null) {
			List<String> dates = null;
			try {
				dates = new DateCalculation().findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(1);
			endDate = dates.get(1);
			days = dates.get(2);
			
		} else {
			
			startDate = listingModel.getDate();
			
		}
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
		String tableName3 = "daily_water_testing";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryWTReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName3);
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				
				model.addAttribute("dataEntryWTReport", dataEntryWTReport);
				model.addAttribute("datesBetween", datesBetween);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if (logger.isInfoEnabled()) {
			logger.info("ajaxWaterSupplyReportPage -- END");
		}

		return "ajax-testing-report";

	}
	
	@RequestMapping(value = "/ajax-charges-tax-report", method = RequestMethod.GET)
	public String ajaxChargesTaxReportPage(final Model model, @ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxChargesTaxReportPage -- START");
		}
		
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));	
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		String startDate = null;
		String endDate = null;
		String days = null;
		
		if (month != null) {
			List<String> dates = null;
			try {
				dates = new DateCalculation().findDate(Integer.valueOf(selectedYear),
						Integer.valueOf(month));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(1);
			endDate = dates.get(1);
			days = dates.get(2);
			
		} else {
			
			startDate = listingModel.getDate();
			
		}
		
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
				
		String tableName5 = "daily_water_charges_tax";
		
		try {
				LinkedHashMap<String,LinkedList<String>> dataEntryWCTReport = reportService.dataEntryStatus(listingModel, amrutTypeId, startDate, tableName5);
				
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				
				model.addAttribute("dataEntryWCTReport", dataEntryWCTReport);
				model.addAttribute("datesBetween", datesBetween);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("ajaxChargesTaxReportPage -- END");
		}

		return "ajax-charges-tax-report";

	}
	
}
