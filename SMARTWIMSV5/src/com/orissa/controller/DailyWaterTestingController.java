package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.DailyWaterTestingModel;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.service.DailyWaterTestingService;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;

@Controller
public class DailyWaterTestingController implements SaltTracker {

	private static Logger logger = Logger.getLogger(DailyWaterTestingController.class);

	@Autowired
	private DailyWaterTestingService dailyWaterTestingService;
	
	@Autowired
	private DivisionService divisionService;

	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private Encryption encryption;

	
	@RequestMapping(value = "/add-daily-water-testing", method = RequestMethod.GET)
	public String addDailyWaterTestingPage(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterTestingPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterTestingPage -- END");
		}

		return "add-daily-water-testing";

	}

	@RequestMapping(value = "/add-daily-water-testing", method = RequestMethod.POST)
	public String addDailyWaterTesting(Model model,HttpServletRequest httpServletRequest, @ModelAttribute DailyWaterTestingModel dailyWaterTestingModel,BindingResult bindingResult,final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterTesting -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyWaterTestingModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			int result = dailyWaterTestingService.addDailyWaterTesting(dailyWaterTestingModel,httpServletRequest);
			
			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		}catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

				logger.debug(e1.getMessage(), e1);
			
			redirectAttributes.addFlashAttribute("dailyWaterTestingModel", dailyWaterTestingModel);
			return ("redirect:/add-daily-water-testing");

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterTesting -- END");
		}

		return ("redirect:/list-daily-water-testing");

	}
	
	@RequestMapping(value = "/list-daily-water-testing", method = RequestMethod.GET)
	public String fetchDailyWaterTesting(HttpServletRequest httpServletRequest, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTesting -- START");
		}
		
		try {
			LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dailyWaterTestingService.fetchDailyWaterTesting(httpServletRequest);
			
			if(dailyWaterTestingModels.size() > 0) {
				
				model.addAttribute("dailyWaterTestingModels", dailyWaterTestingModels);
			} else {
				
				model.addAttribute("emptyList", messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTesting -- END");
		}

		return "list-daily-water-testing";

	}
	
	@RequestMapping(value = "/edit-daily-water-testing", method = RequestMethod.GET)
	public String editDailyWaterTestingPage(@RequestParam String wt_id, Model model, HttpServletRequest httpServletRequest) {
		if (logger.isInfoEnabled()) {
			logger.info("editDailyWaterTestingPage -- START");
		}
		
		if(StringUtils.isBlank(wt_id)){
			return "redirect:/error404";
		}
		
		/*if(!StringUtils.isNumeric(wt_id)){
			return "error404";
		}*/
		
		
		DailyWaterTestingModel dailyWaterTestingModel = null;
		String wt_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			wt_id1 = encryption.decode(key, id);
			if(wt_id.length() > id.length())
				throw new Exception();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		
		try {
			dailyWaterTestingModel = dailyWaterTestingService.fetchDailyWaterTestingById(Integer.parseInt(wt_id1));
			
			if(dailyWaterTestingModel.getWt_id() != 0) {
				model.addAttribute("dailyWaterTestingModel", dailyWaterTestingModel);
			} else {
				model.addAttribute("emptyList", messageUtil.getBundle("no.record.found"));
				return "redirect:/list-daily-water-testing";
			}
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(dailyWaterTestingModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editDailyWaterTestingPage -- END");
		}

		return "edit-daily-water-testing";

	}
	
	@RequestMapping(value = "/delete-daily-water-testing", method = RequestMethod.POST)
	public String deleteDailyWaterTestingPage(final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest, DailyWaterTestingModel dailyWaterTestingModel) {
		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterTestingPage -- START");
		}

		String wt_id = dailyWaterTestingModel.getWtId();
		
		if (StringUtils.isBlank(wt_id)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyWaterTestingModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		String wt_id1 = null;
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			wt_id1 = encryption.decode(key, wt_id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			int result = dailyWaterTestingService
					.deleteDailyWaterTestingById(Integer.parseInt(wt_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterTestingPage -- END");
		}

		return "redirect:/list-daily-water-testing";

	}

	@RequestMapping(value = "/isApproved-daily-water-testing", method = RequestMethod.GET)
	public @ResponseBody String isApprovedUpdate(@RequestParam String wt_id,final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		if (StringUtils.isBlank(wt_id)) {
			return "redirect:/error404";
		}

		String wt_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			wt_id1 = encryption.decode(key, id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		try {
			int result = dailyWaterTestingService
					.isApprovedUpdate(Integer.parseInt(wt_id1));

			if (result == 1) {

				System.out.println("Approved");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("Approved.done"));
			} else {
				System.out.println("Approved Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("Approved.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return "success";

	}
	
	@RequestMapping(value = "/isNotApproved-daily-water-testing", method = RequestMethod.GET)
	public @ResponseBody String isNotApprovedUpdate(@RequestParam String wt_id,final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}

		if (StringUtils.isBlank(wt_id)) {
			return "redirect:/error404";
		}

		String wt_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			wt_id1 = encryption.decode(key, id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		try {
			int result = dailyWaterTestingService
					.isNotApprovedUpdate(Integer.parseInt(wt_id1));

			if (result == 1) {

				System.out.println("Approved");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("Approved.done"));
			} else {
				System.out.println("Approved Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("Approved.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return "success";

	}
	
	@RequestMapping(value = "/update-daily-water-testing", method = RequestMethod.POST)
	public String updateDailyWaterTesting(Model model,HttpServletRequest httpServletRequest, @ModelAttribute DailyWaterTestingModel dailyWaterTestingModel,BindingResult bindingResult,final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterTesting -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyWaterTestingModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			String wtId = encryption.decode(key, dailyWaterTestingModel.getWtId());
			dailyWaterTestingModel.setWt_id(Integer.parseInt(wtId));
			int result = dailyWaterTestingService.updateDailyWaterTesting(dailyWaterTestingModel,httpServletRequest);
			
			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		}catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			
			redirectAttributes.addFlashAttribute("dailyWaterTestingModel", dailyWaterTestingModel);
			return ("redirect:/edit-daily-water-testing?wt_id="+dailyWaterTestingModel.getWtId());

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterTesting -- END");
		}
		
		return "redirect:/list-daily-water-testing";

	}
	
	@RequestMapping(value = "/fetch-ajax-daily-water-testing", method = RequestMethod.GET)
	public String fetchAJAXDailyWaterTesting(HttpServletRequest httpServletRequest, Model model, @ModelAttribute ListingModel listingModel) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterTesting -- START");
		}
		
		try {
			LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dailyWaterTestingService.fetchAJAXDailyWaterTesting(listingModel);
			
			if(dailyWaterTestingModels.size() > 0) {
				
				model.addAttribute("dailyWaterTestingModels", dailyWaterTestingModels);
			} else {
				
				model.addAttribute("emptyList", messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterTesting -- END");
		}

		return "ajax-daily-water-testing";

	}
}
