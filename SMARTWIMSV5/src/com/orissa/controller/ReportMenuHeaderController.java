package com.orissa.controller;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orissa.exception.DBConnectionException;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.ULBModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBService;

@RestController
@RequestMapping(value = "/reportHeaderMenu/**")
public class ReportMenuHeaderController {

	private static Logger logger = Logger
			.getLogger(ReportMenuHeaderController.class);

	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBService ulbService;

	@RequestMapping(value = "/distList")
	public List<PhDistrictModel> fetchDistList(@RequestParam int divisionId) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDistList -- START");
		}

		List<PhDistrictModel> districtModels = null;

		try {
			districtModels = districtService
					.fetchDistrictByDivisionId(divisionId);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDistList -- END");
		}

		return districtModels;
	}

	@RequestMapping(value = "/ULBList")
	public List<ULBModel> fetchULBList(@RequestParam int districtId) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBList -- START");
		}

		List<ULBModel> ulbModels = null;

		try {

			ulbModels = ulbService.fetchULBByDistrictId(districtId);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBList -- END");
		}

		return ulbModels;
	}
}