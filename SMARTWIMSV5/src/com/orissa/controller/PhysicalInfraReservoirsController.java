package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.PhysicalInfraStorageReservoirsModel;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;
import com.orissa.service.PhysicalInfraReservoirsService;

@Controller
public class PhysicalInfraReservoirsController implements SaltTracker {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraReservoirsController.class);

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private PhysicalInfraReservoirsService physicalInfraReservoirsService;

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private Encryption encryption;

	/**
	 * Adding Physical Infra Strorage reserviors
	 * 
	 * @return
	 */

	@RequestMapping(value = "/add-physical-infra-reservoirs", method = RequestMethod.GET)
	public String addPhysicalInfrastoragereserviors(final ModelMap modelMap) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfrastoragereserviors -- START");
		}
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfrastoragereserviors -- END");
		}

		return "add-physical-infra-storage-reserviors";

	}

	@RequestMapping(value = "/add-physical-infra-storage-reserviors", method = RequestMethod.POST)
	public String addPhysicalInfrastoragereserviors(
			Model model,
			HttpServletRequest httpServletRequest,
			@ModelAttribute PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfrastoragereserviors -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraStorageReservoirsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			/**
			 * Sending Client Side Values to the Service layer
			 */
			int result = physicalInfraReservoirsService
					.addPhysicalInfraReservoirsService(
							physicalInfraStorageReservoirsModel,
							httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

			redirectAttributes.addFlashAttribute(
					"physicalInfraStorageReservoirsModel",
					physicalInfraStorageReservoirsModel);
			return ("redirect:/add-physical-infra-reservoirs");

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfrastoragereserviors -- END");
		}

		return "redirect:/list-physical-infra-storage-reserviors";

	}

	@RequestMapping(value = "/list-physical-infra-storage-reserviors", method = RequestMethod.GET)
	public String fetchPhysicalInfrastoragereserviors(HttpServletRequest httpServletRequest,Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfrastoragereserviors -- START");
		}

		try {
			LinkedList<PhysicalInfraStorageReservoirsModel> physicalInfraStorageReservoirsModels = physicalInfraReservoirsService
					.fetchPhysicalInfraReservoirs(httpServletRequest);
			
			if (physicalInfraStorageReservoirsModels.size() > 0) {

				model.addAttribute("physicalInfraStorageReservoirsModels",
						physicalInfraStorageReservoirsModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStorageReservoirs -- END");
		}

		return "list-physical-infra-storage-reserviors";

	}

	@RequestMapping(value = "/edit-physical-infra-storage-reserviors", method = RequestMethod.GET)
	public String editPhysicalInfraStorageReservoirs(
			@RequestParam String phyInfra_sr_id, Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraStorageReservoirs -- START");
		}

		if (StringUtils.isBlank(phyInfra_sr_id)) {
			return "redirect:/error404";
		}


		PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel = null;
		String phyInfra_sr_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
	try{
			
			String key = messageUtil.getBundle("secret.key"); 
			phyInfra_sr_id1 = encryption.decode(key, id);
			if(phyInfra_sr_id.length() > id.length())
				throw new Exception();
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}

		try {
			physicalInfraStorageReservoirsModel = physicalInfraReservoirsService
					.fetchPhysicalInfraStorageReservoirsById(Integer
							.parseInt(phyInfra_sr_id1));

			if (physicalInfraStorageReservoirsModel.getPhyInfra_sr_id() != 0) {
				model.addAttribute("physicalInfraStorageReservoirsModel",
						physicalInfraStorageReservoirsModel);
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return "redirect:/list-physical-infra-storage-reserviors";
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(physicalInfraStorageReservoirsModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editPhysicalInfraStorageReservoirse -- END");
		}

		return "edit-physical-infra-storage-reserviors";

	}

	@RequestMapping(value = "/delete-physical-infra-storage-reserviors", method = RequestMethod.POST)
	public String deletePhysicalInfraStorageReservoirsePage(			
			final RedirectAttributes redirectAttributes, Model model, HttpServletRequest httpServletRequest, PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraStorageReservoirsePage -- START");
		}

		String phyInfra_sr_id = physicalInfraStorageReservoirsModel.getPhyInfraSrId();
		
		if (StringUtils.isBlank(phyInfra_sr_id)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraStorageReservoirsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		String phyInfra_sr_id1 = null;
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
	try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			phyInfra_sr_id1 = encryption.decode(key, phyInfra_sr_id);
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		
		try {
			int result = physicalInfraReservoirsService
					.deletePhysicalInfraStorageReservoirseById(Integer
							.parseInt(phyInfra_sr_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else {
			
			throw new ServletException(messageUtil.getBundle("CSRF"));

		}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraStorageReservoirsePage -- END");
		}

		return "redirect:/list-physical-infra-storage-reserviors";

	}

	@RequestMapping(value = "/update-physical-infra-storage-reserviors", method = RequestMethod.POST)
	public String updatePhysicalInfraStorageReservoirs(
			Model model,
			HttpServletRequest httpServletRequest,
			@ModelAttribute PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraReservoirs -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = physicalInfraStorageReservoirsModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		try {
			
			String phyInfraSrId = encryption.decode(key, physicalInfraStorageReservoirsModel.getPhyInfraSrId());
			physicalInfraStorageReservoirsModel.setPhyInfra_sr_id(Integer.parseInt(phyInfraSrId));
			int result = physicalInfraReservoirsService
					.updatePhysicalInfraStorageReservoirs(
							physicalInfraStorageReservoirsModel,
							httpServletRequest);

			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);

			redirectAttributes.addFlashAttribute(
					"physicalInfraStorageReservoirsModel",
					physicalInfraStorageReservoirsModel);
			return ("redirect:/edit-physical-infra-storage-reserviors?phyInfra_sr_id=" + physicalInfraStorageReservoirsModel.getPhyInfraSrId());

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {				
				throw new ServletException(messageUtil.getBundle("CSRF"));
			}
	} catch (Exception e) {
		redirectAttributes.addFlashAttribute("error", e.getMessage());
	} finally {
		activeSalts.remove(salt);
	}
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraReservoirs -- END");
		}

		return "redirect:/list-physical-infra-storage-reserviors";

	}
	@RequestMapping(value = "/fetch-ajax-physical-infra-storage-reserviors", method = RequestMethod.GET)
	public String fetchAJAXPhysicalInfraStorageReservoirs(HttpServletRequest httpServletRequest, @ModelAttribute ListingModel listingModel, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStorageReservoirs -- START");
		}
		LinkedList<PhysicalInfraStorageReservoirsModel> physicalInfraStorageReservoirsModels = null;

		try {
			physicalInfraStorageReservoirsModels = physicalInfraReservoirsService.fetchAJAXPhysicalInfraStorageReservoirs(listingModel);

			model.addAttribute("physicalInfraStorageReservoirsModels", physicalInfraStorageReservoirsModels);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStorageReservoirs -- END");
		}

		return "ajax-physical-infra-storage-reserviors";

	}

}
