/**
 * @formatter:off
 *
 */
package com.orissa.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.orissa.common.DateCalculation;
import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.model.AjaxDataApprovalModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.ULBModel;
import com.orissa.service.DashboardService;
import com.orissa.service.DivisionService;

@Controller
public class DataApprovalController {

	private static Logger logger = Logger.getLogger(DataApprovalController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private DateCalculation dateCalculation;

	@RequestMapping(value = "/data-approval", method = RequestMethod.GET)
	public String dataApprovalPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("dataApprovalPage -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();
		String amrutTypeId = null;
		
		try {
			
			LinkedList<PhDivisionModel> phDivisionModels = null;
			
			if(httpSession.getAttribute("amrutTypeId") == null)
				phDivisionModels = divisionService.fetchDivision();
			else {
				amrutTypeId = "1";
				phDivisionModels = divisionService.fetchDivisionByAmrut();
			}

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		model.addAttribute("loginUsersModel2", loginUsersModel);
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		HashMap<String,HashMap<String,ULBModel>> dataEntryStatus = null;
		model.addAttribute("startDate", startDate);
		
		try {
			
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
					dataEntryStatus = dashboardService.dataApprovalStatusInnerPage(listingModel, amrutTypeId, startDate);
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					dataEntryStatus = dashboardService.dataApprovalStatusInnerPage(listingModel, amrutTypeId, startDate);
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					dataEntryStatus = dashboardService.dataApprovalStatusInnerPage(listingModel, amrutTypeId, startDate);
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					dataEntryStatus = dashboardService.dataApprovalStatusInnerPage(listingModel, amrutTypeId, startDate);
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
					
			int notApproved = 0;
			int approved = 0;
			
			for (Entry<String, HashMap<String,ULBModel>> entry1 : dataEntryStatus.entrySet()) {
				
				/*for(Entry<String, ULBModel> entry2:entry1.getValue().entrySet()) {  
				 
						System.out.println("Outer Map Key :: " + entry1.getKey() + ".......Inner Map Key :: " + entry2.getKey() + ".......Inner Map value :: "+ entry2.getValue().getLatitude());
				}*/
				
				if(entry1.getKey().startsWith("NA")){ // Not Approved
					
					notApproved =  entry1.getValue().entrySet().size();
							
				} else { // Approved
					
					approved =  entry1.getValue().entrySet().size();
				} 
			}
			
			 model.addAttribute("notApproved",notApproved);
			 model.addAttribute("approved",approved);
			 
			model.addAttribute("dataEntryStatus", new ObjectMapper().writeValueAsString(dataEntryStatus));
			
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("dataApprovalPage -- END");
		}

		return "data-approval";

	}
	
	
	@RequestMapping(value = "/ajax-data-approval", method = RequestMethod.GET)
	public @ResponseBody AjaxDataApprovalModel ajaxDataApproval(
			@ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("ajaxDataApproval -- START");
		}
		
		
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		AjaxDataApprovalModel ajaxDataApprovalModel = new AjaxDataApprovalModel();
		
		if(httpSession.getAttribute("amrutTypeId") == null) {
			if(amrutTypeId != null) {
				
				httpSession.setAttribute("amrutTypeId", amrutTypeId);
				
				try {
					
					LinkedList<PhDivisionModel> phDivisionModels = divisionService.fetchDivisionByAmrut();
					
					ajaxDataApprovalModel.setPhDivisionModels(phDivisionModels);
					
					listingModel.setDistrictId(null);
					listingModel.setDivisionId(null);
					listingModel.setUlbId(null);
	
				} catch (DBConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		String startDate = null;
		String endDate = null;
		String days = null;
		String year = null;
		String startDate1 = null;
		String endDate1 = null;
		
		if (month != null) {
			
			List<String> dates = null;
			try {
				dates = dateCalculation.findDate(2016,
						Integer.valueOf(month));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(0);
			endDate = dates.get(1);
			days = dates.get(2);
			
			startDate1 = startDate;
			endDate1 = endDate;
			
		} else {
			
			startDate = listingModel.getDate();
			
			if(startDate != null) {
				
				month = startDate.substring(5, 7);
				year = startDate.substring(0, 4);
				
				if (month != null) {
					
					List<String> dates = null;
					try {
						dates = new DateCalculation().findDate(Integer.valueOf(year), Integer.valueOf(month));
						startDate1 = dates.get(0);
						endDate1 = startDate;
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					} 
					
				}
				
			}
		
		}
		
		HashMap<String,HashMap<String,ULBModel>> dataEntryStatus = null;
		
		try {
			
				if(flag == 1) {
				
		/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
					dataEntryStatus = dashboardService.dataApprovalStatusInnerPage(listingModel, amrutTypeId, startDate);
			
		/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
		/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
					listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
					dataEntryStatus = dashboardService.dataApprovalStatusInnerPage(listingModel, amrutTypeId, startDate);
				
		/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
		/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
					listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
					dataEntryStatus = dashboardService.dataApprovalStatusInnerPage(listingModel, amrutTypeId, startDate);
				
		/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
		/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */
					
					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					dataEntryStatus = dashboardService.dataApprovalStatusInnerPage(listingModel, amrutTypeId, startDate);
					
		/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}
					
			int complete = 0;
			int partial = 0;
			int notApproved = 0;
			int approved = 0;
			int noDataEntry = 0;
			
			for (Entry<String, HashMap<String,ULBModel>> entry1 : dataEntryStatus.entrySet()) {
				
				/*for(Entry<String, ULBModel> entry2:entry1.getValue().entrySet()) {  
				 
						System.out.println("Outer Map Key :: " + entry1.getKey() + ".......Inner Map Key :: " + entry2.getKey() + ".......Inner Map value :: "+ entry2.getValue().getLatitude());
				}*/
				
				if(entry1.getKey().startsWith("NA")){ // Not Approved
					
					notApproved =  entry1.getValue().entrySet().size();
							
				} else { // Approved
					
					approved =  entry1.getValue().entrySet().size();
				} 
			}
			
			ajaxDataApprovalModel.setComplete(complete);
			ajaxDataApprovalModel.setPartial(partial);
			ajaxDataApprovalModel.setNotApproved(notApproved);
			ajaxDataApprovalModel.setApproved(approved);
			ajaxDataApprovalModel.setNoDataEntry(noDataEntry);
			 
			ajaxDataApprovalModel.setDataEntryStatus(dataEntryStatus);
			
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("ajaxDataApproval -- END");
		}

		return ajaxDataApprovalModel;

	}
	
}
