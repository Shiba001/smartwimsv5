package com.orissa.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.orissa.common.Encryption;
import com.orissa.common.MessageUtil;
import com.orissa.common.SaltTracker;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.service.DailyWaterSupplyService;
import com.orissa.service.DistrictService;
import com.orissa.service.DivisionService;

@Controller
public class DailyWaterSupplyController implements SaltTracker {

	private static Logger logger = Logger
			.getLogger(DailyWaterSupplyController.class);

	@Autowired
	private DailyWaterSupplyService dailyWaterSupplyService;

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private DistrictService districtService;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private Encryption encryption;

	@RequestMapping(value = "/add-daily-water-supply", method = RequestMethod.GET)
	public String addDailyWaterSupplyPage(final ModelMap modelMap, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterSupplyPage -- START");
		}
		
		
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				modelMap.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				modelMap.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			modelMap.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterSupplyPage -- END");
		}

		return "add-daily-water-supply";

	}

	@RequestMapping(value = "/add-daily-water-supply", method = RequestMethod.POST)
	public String addDailyWaterSupply(Model model,
			HttpServletRequest httpServletRequest,
			@ModelAttribute DailyWaterSupplyModel dailyWaterSupplyModel, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) {
		
		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterSupply -- START");
		}
		
		
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyWaterSupplyModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			int result = dailyWaterSupplyService.addDailyWaterSupply(
					dailyWaterSupplyModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Inserted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("insert.done"));
			} else {
				System.out.println("Insertion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("insert.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute("dailyWaterSupplyModel", dailyWaterSupplyModel);
			return ("redirect:/add-daily-water-supply");

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     } else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterSupply -- END");
		}

		return ("redirect:/list-daily-water-supply");

	}

	@RequestMapping(value = "/list-daily-water-supply", method = RequestMethod.GET)
	public String fetchDailyWaterSupply(HttpServletRequest httpServletRequest, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupply -- START");
		}

		try {
			LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dailyWaterSupplyService
					.fetchDailyWaterSupply(httpServletRequest);

			if (dailyWaterSupplyModels.size() > 0) {

				model.addAttribute("dailyWaterSupplyModels",
						dailyWaterSupplyModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupply -- END");
		}

		return "list-daily-water-supply";

	}

	@RequestMapping(value = "/edit-daily-water-supply", method = RequestMethod.GET)
	public String editDailyWaterSupplyPage(@RequestParam String ws_id,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("editDailyWaterSupplyPage -- START");
		}

		if (StringUtils.isBlank(ws_id)) {
			return "redirect:/error404";
		}

		DailyWaterSupplyModel dailyWaterSupplyModel = null;
		String ws_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			ws_id1 = encryption.decode(key, id);
			if(ws_id.length() > id.length())
				throw new Exception();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		
		try {
			
			dailyWaterSupplyModel = dailyWaterSupplyService
					.fetchDailyWaterSupplyById(Integer.parseInt(ws_id1));

			if (dailyWaterSupplyModel.getWs_id() != 0) {
				model.addAttribute("dailyWaterSupplyModel",
						dailyWaterSupplyModel);
			} else {
				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
				return "redirect:/list-daily-water-supply";
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {
				
				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}
			//String startDate = "2016-11-10";
			String startDate = Util.getYesterdayDateString();
			model.addAttribute("startDate", startDate);

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {

			LinkedList upperLevels = districtService
					.fetchUpperLevelsByULBId(dailyWaterSupplyModel
							.getUlbNameModel().getUlb_id());

			if (upperLevels.size() > 0) {
				model.addAttribute("upperLevels", upperLevels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("editDailyWaterSupplyPage -- END");
		}

		return "edit-daily-water-supply";

	}

	@RequestMapping(value = "/delete-daily-water-supply", method = RequestMethod.POST)
	public String deleteDailyWaterSupplyPage(final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest, DailyWaterSupplyModel dailyWaterSupplyModel) {

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterSupplyPage -- START");
		}
		
		String ws_id = dailyWaterSupplyModel.getWsId();
		
		if (StringUtils.isBlank(ws_id)) {
			return "redirect:/error404";
		}

		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyWaterSupplyModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		String ws_id1 = null;
		/*String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";*/
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			ws_id1 = encryption.decode(key, ws_id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		try {
			int result = dailyWaterSupplyService
					.deleteDailyWaterSupplyById(Integer.parseInt(ws_id1));

			if (result == 1) {

				System.out.println("Deleted Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("delete.done"));
			} else {
				System.out.println("Deletion Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("delete.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterSupplyPage -- END");
		}

		return "redirect:/list-daily-water-supply";

	}

	@RequestMapping(value = "/update-daily-water-supply", method = RequestMethod.POST)
	public String updateDailyWaterSupply(Model model,
			HttpServletRequest httpServletRequest,
			@ModelAttribute DailyWaterSupplyModel dailyWaterSupplyModel,BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterSupply -- START");
		}
		
		if (bindingResult.hasErrors()) {
	        return "redirect:/error";
	    }
		
		String key = messageUtil.getBundle("secret.key");
		
		String sessionId = httpServletRequest.getSession().getId();
		List<String> activeSalts = SALT_TRACKER.get(sessionId);
		String salt = "";
		try {
			salt = dailyWaterSupplyModel.getCsrfPreventionSalt();

			if (!org.springframework.util.CollectionUtils.isEmpty(activeSalts)
					& activeSalts.contains(salt)) {
		
		try {
			String wsId = encryption.decode(key, dailyWaterSupplyModel.getWsId());
			dailyWaterSupplyModel.setWs_id(Integer.parseInt(wsId));
			int result = dailyWaterSupplyService.updateDailyWaterSupply(
					dailyWaterSupplyModel, httpServletRequest);

			if (result == 1) {

				System.out.println("Updated Successfully");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("update.done"));
			} else {
				System.out.println("Updation Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("update.failed"));
			}

		} catch (FormExceptions e1) {

			for (Entry<String, Exception> entry : ((FormExceptions) e1)
					.getExceptions().entrySet()) {
				System.out.println("key :: " + entry.getKey() + " value :: "
						+ entry.getValue().getMessage());
				redirectAttributes.addFlashAttribute(entry.getKey(), entry
						.getValue().getMessage());
			}

			logger.debug(e1.getMessage(), e1);
			redirectAttributes.addFlashAttribute("dailyWaterSupplyModel", dailyWaterSupplyModel);
			return ("redirect:/edit-daily-water-supply?ws_id="+dailyWaterSupplyModel.getWsId());
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			} else {
				
				throw new ServletException(messageUtil.getBundle("CSRF"));

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} finally {
			activeSalts.remove(salt);
		}
		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterSupply -- END");
		}

		return "redirect:/list-daily-water-supply";

	}
	
	
	@RequestMapping(value = "/isApproved-daily-water-supply", method = RequestMethod.GET)
	public @ResponseBody String isApprovedUpdate(@RequestParam String ws_id, final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		if (StringUtils.isBlank(ws_id)) {
			return "redirect:/error404";
		}

		String ws_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			ws_id1 = encryption.decode(key, id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		try {
			int result = dailyWaterSupplyService
					.isApprovedUpdate(Integer.parseInt(ws_id1));

			if (result == 1) {

				System.out.println("Approved");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("Approved.done"));
			} else {
				System.out.println("Approved Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("Approved.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return "success";

	}
	
	
	@RequestMapping(value = "/isNotApproved-daily-water-supply", method = RequestMethod.GET)
	public @ResponseBody String isNotApprovedUpdate(@RequestParam String ws_id, final RedirectAttributes redirectAttributes,
			Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}

		if (StringUtils.isBlank(ws_id)) {
			return "redirect:/error404";
		}

		String ws_id1 = null;
		String trgt_id2 = httpServletRequest.getQueryString();
		String id = trgt_id2.split("=")[1]+"==";
		
		try{
			
			String key = messageUtil.getBundle("secret.key"); 
			
			ws_id1 = encryption.decode(key, id);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/error404";
		}
		try {
			int result = dailyWaterSupplyService
					.isNotApprovedUpdate(Integer.parseInt(ws_id1));

			if (result == 1) {

				System.out.println("Approved");
				redirectAttributes.addFlashAttribute("Success",
						messageUtil.getBundle("Approved.done"));
			} else {
				System.out.println("Approved Failed");
				redirectAttributes.addFlashAttribute("Failure",
						messageUtil.getBundle("Approved.failed"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return "success";

	}
	
	
	@RequestMapping(value = "/fetch-ajax-daily-water-supply", method = RequestMethod.GET)
	public String fetchAJAXDailyWaterSupply(HttpServletRequest httpServletRequest, @ModelAttribute ListingModel listingModel, Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- START");
		}
		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = null;

		try {
			dailyWaterSupplyModels = dailyWaterSupplyService.fetchAJAXDailyWaterSupply(listingModel);

			model.addAttribute("dailyWaterSupplyModels", dailyWaterSupplyModels);
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- END");
		}

		return "ajax-daily-water-supply";

	}
}
