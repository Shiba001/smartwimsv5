/*package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orissa.common.MessageUtil;
import com.orissa.exception.DBConnectionException;
import com.orissa.model.DailyConnetionsModel;
import com.orissa.model.ListingModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.service.DashboardService;
import com.orissa.service.DivisionService;

@Controller
public class ConnectionController {

	private static Logger logger = Logger.getLogger(ConnectionController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DashboardService dashboardService;
	
	@RequestMapping(value = "/connection", method = RequestMethod.GET)
	public String connectionPage(final Model model) {

		if (logger.isInfoEnabled()) {
			logger.info("connectionPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ListingModel listingModel = new ListingModel();
		String amrutTypeId = null;
		String startDate = "2016-11-10";
		//String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		
		try {
			
			LinkedList<DailyConnetionsModel> dailyConnetionsModels = dashboardService.fetchDailyConnetionsReport(listingModel, amrutTypeId, startDate, endDate);
			
			double total = 0.0D;
			int connections = 0;
			
			if ((dailyConnetionsModels != null) && (dailyConnetionsModels.size() > 0)) {
						
				total = dailyConnetionsModels.get(dailyConnetionsModels.size() - 1).getTotal();
				
				connections = (int)(total);
			}
			
			model.addAttribute("connection", connections);
			model.addAttribute("dailyConnetionsModels", dailyConnetionsModels);
			
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("connectionPage -- END");
		}

		return "connection";

	}
}
*/