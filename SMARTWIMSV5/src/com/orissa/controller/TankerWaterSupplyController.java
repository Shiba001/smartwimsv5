/**
 * @formatter:off
 *
 */
package com.orissa.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.orissa.common.DateCalculation;
import com.orissa.common.MessageUtil;
import com.orissa.common.SortingMap;
import com.orissa.common.Util;
import com.orissa.exception.DBConnectionException;
import com.orissa.model.DailyTankersModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.service.DashboardService;
import com.orissa.service.DivisionService;

@Controller
public class TankerWaterSupplyController {

	private static Logger logger = Logger.getLogger(TankerWaterSupplyController.class);

	@Autowired
	private DivisionService divisionService;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private SortingMap sortingMap;
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private DateCalculation dateCalculation;
	
	@RequestMapping(value = "/tanker", method = RequestMethod.GET)
	public String tankerPage(Model model, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("tankerPage -- START");
		}
		
		try {
			LinkedList<PhDivisionModel> phDivisionModels = divisionService
					.fetchDivision();

			if (phDivisionModels.size() > 0) {

				model.addAttribute("phDivisionModels", phDivisionModels);
			} else {

				model.addAttribute("emptyList",
						messageUtil.getBundle("no.record.found"));
			}

		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		model.addAttribute("loginUsersModel", loginUsersModel);
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		ListingModel listingModel = new ListingModel();
		String amrutTypeId = null;
		//String startDate = "2016-11-10";
		String startDate = Util.getBeforeYesterdayDateString();
		String endDate = null;
		LinkedList<DailyTankersModel> dailyTankersModels = null;
		model.addAttribute("startDate", startDate);
		
		try {
			
				if(flag == 1) {
				
	/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
					dailyTankersModels = dashboardService.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
			
	/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
	/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				dailyTankersModels = dashboardService.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
				
	/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
	/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				dailyTankersModels = dashboardService.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
				
				
	/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
	/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */

					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					dailyTankersModels = dashboardService.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
					
	/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}

			model.addAttribute("dailyTankersModels", dailyTankersModels);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("tankerPage -- END");
		}

		return "tanker";

	}
	
	@RequestMapping(value = "/ajax-tanker", method = RequestMethod.GET)
	public String ajaxTankerPage(Model model,
			@ModelAttribute ListingModel listingModel,
			@RequestParam(required = false) String selectedYear,
			@RequestParam(required = false) String amrutTypeId,
			@RequestParam(required = false) String month, HttpServletRequest httpServletRequest) {

		if (logger.isInfoEnabled()) {
			logger.info("tankerPage -- START");
		}
		
		
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		model.addAttribute("loginUsersModel", loginUsersModel);
		
		int flag = 4;
		
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			flag = 4;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District Level User
			
			flag = 3;
			
		} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division Level User
			
			flag = 2;
			
		} else { // Admin User
			
			flag = 1;
		}
		
		String startDate = null;
		String endDate = null;
		String days = null;
		String year = null;
		String startDate1 = null;
		String endDate1 = null;
		
		if (month != null) {
			
			List<String> dates = null;
			try {
				dates = dateCalculation.findDate(Integer.parseInt(selectedYear),
						Integer.valueOf(month));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			startDate = dates.get(0);
			endDate = dates.get(1);
			days = dates.get(2);
			
			startDate1 = startDate;
			endDate1 = endDate;
			
		} else {
			
			startDate = listingModel.getDate();
			
			if(startDate != null) {
				
				month = startDate.substring(5, 7);
				year = startDate.substring(0, 4);
				
				if (month != null) {
					
					List<String> dates = null;
					try {
						dates = new DateCalculation().findDate(Integer.valueOf(year), Integer.valueOf(month));
						startDate1 = dates.get(0);
						endDate1 = startDate;
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					} 
					
				}
				
			}
		
		}
		LinkedList<DailyTankersModel> dailyTankersModels = null;
		model.addAttribute("startDate", startDate);
		
		try {
			
				if(flag == 1) {
				
	/* -------------------------------------------------------------------- For State Level And Above Starts --------------------------------------------------------------------- */
				
					dailyTankersModels = dashboardService.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
			
	/* -------------------------------------------------------------------- For State Level And Above Ends ----------------------------------------------------------------------- */	
			
				} else if(flag == 2) {
				
	/* -------------------------------------------------------------------- For Division Level User Starts --------------------------------------------------------------------- */
				
				listingModel.setDivisionId(loginUsersModel.getPhDivisionModel().getDiv_id());
				dailyTankersModels = dashboardService.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
				
	/* -------------------------------------------------------------------- For Division Level User Ends ----------------------------------------------------------------------- */
				
				} else if(flag == 3) {
				
	/* -------------------------------------------------------------------- For District Level User Starts -------------------------------------------------------------------------- */
				
				listingModel.setDistrictId(loginUsersModel.getPhDistrictModel().getDist_id());
				dailyTankersModels = dashboardService.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
				
				
	/* -------------------------------------------------------------------- For District Level User Ends ---------------------------------------------------------------------------- */
					
				} else if(flag == 4) {
					
	/* -------------------------------------------------------------------- For ULB Level User Starts ------------------------------------------------------------------------------- */

					listingModel.setUlbId(loginUsersModel.getUlbNameModel().getUlb_id());
					dailyTankersModels = dashboardService.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
					
	/* -------------------------------------------------------------------- For ULB Level User Ends --------------------------------------------------------------------------------- */
				}

			model.addAttribute("dailyTankersModels", dailyTankersModels);
		
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		if (logger.isInfoEnabled()) {
			logger.info("tankerPage -- END");
		}

		return "ajax-tanker";

	}
	

	
}
