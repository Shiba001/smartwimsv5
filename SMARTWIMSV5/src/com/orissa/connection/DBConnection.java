package com.orissa.connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.exception.DBConnectionException;

import databasehub.mysql.connect.MySQLConnect;

@Component
public class DBConnection {

	@Autowired
	private DatabaseProperties databaseProperties;

	private java.sql.Connection con = null;

	public java.sql.Connection Connect() throws Exception {

		String url = databaseProperties.getBundle("database.url");
		String userName = databaseProperties.getBundle("database.user");
		String password = databaseProperties.getBundle("database.password");

		MySQLConnect mySQLConnect = new MySQLConnect(url, userName, password);

		try{
				con = mySQLConnect.getConnection();
		} catch(Exception e) {
			throw new DBConnectionException(e.getMessage());
		}

		con.setAutoCommit(false);

		//System.out.println("Connection ===== >>>>>" + con);

		return con;

	}

}