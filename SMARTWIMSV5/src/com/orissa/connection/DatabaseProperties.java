package com.orissa.connection;

import java.util.ResourceBundle;

import org.springframework.stereotype.Component;

@Component
public class DatabaseProperties {

	private static ResourceBundle resourceBundle = null;

	static {

		resourceBundle = ResourceBundle.getBundle("resources.database");
	}

	public String getBundle(String key) {

		return resourceBundle.getString(key);
	}
}
