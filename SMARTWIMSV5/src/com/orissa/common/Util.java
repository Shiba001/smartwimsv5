package com.orissa.common;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.exception.EmptyValueException;

@Component
public class Util {

	private static Logger logger = Logger.getLogger(Util.class);
	
	@Autowired
	private static MessageUtil messageUtil;

	private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
	  
	private static Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
	  
	public static boolean validatePassword(final String password){

		  Matcher matcher = pattern.matcher(password);
		  return matcher.matches();
	}
	
	public static boolean isSpecialChar(String value) {
		Pattern p = Pattern.compile("[a-zA-Z0-9 ]", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(value);
		return m.find();
	}
	
	public static boolean isEmpty(String value) {
		return value == null || value.isEmpty();
	}

	public static boolean isEmpty(Date date) {
		return date == null;
	}

	public static boolean isEmpty(Object object) {
		return object == null;
	}

	public static boolean stringIsEmpty(String value) {

		return value.equalsIgnoreCase("") || value == null;
	}

	public static boolean isNumeric(String value) {
		Pattern pattern = Pattern.compile("\\d+");

		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}
	
	public static boolean isNumber(String data){
		 String regex = "\\d+";
		 String value = "";
		    String[] result = data.split("\\.");
		    if(result[1].length() == 1){
		    	if(result[1].contains("0")){
		    		value = result[0];
		    	}
		    	else{
		    		value = data;
		    	}
		    }
		 return !(value.matches(regex));
	}
	
	public static String formatDateFromEntity(Date value) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

		String formatDate = simpleDateFormat.format(value);

		return formatDate;

	}

	/**
	 * @author Supratim Sarkar
	 * @param h
	 * @return
	 */
	public static String stringcut(String data) {
		return data.replace(" ", "-").trim().toLowerCase();
	}

	/**
	 * this method get the Input String and convert the string to UTF-8
	 * compatible
	 * 
	 * @param string
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String utfEightConverter(String string) {

		try {
			byte[] array = string.getBytes("ISO-8859-1");
			String s = new String(array, Charset.forName("UTF-8"));
			string = StringEscapeUtils.unescapeJava(s);

		} catch (Exception e) {
			logger.error(e);
		}

		return string;
	}

	/**
	 * this method is used to validate the email
	 * 
	 * @param email
	 * @return
	 */
	public static boolean emailValidator(String email) {

		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);

		return !m.matches();
	}

	/**
	 * 
	 * @param timestamp
	 * @param dateFormate
	 * @return
	 * @throws ParseException
	 */
	public static Date getDatefromTimestamp(String timestamp, String dateFormate)
			throws ParseException {
		long time = Long.valueOf(String.valueOf(timestamp));
		Date date = new Date(time);
		Format format = new SimpleDateFormat(dateFormate);
		format.format(date);
		SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormate);
		return dateFormat.parse(format.format(date));
	}

	public static String constructAppUrl(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName() + ":"
				+ request.getServerPort() + request.getContextPath();

	}

	public static String currentDay() {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		// System.out.println(new SimpleDateFormat("EE",
		// Locale.ENGLISH).format(date.getTime()));
		String day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date
				.getTime());
		return day.toUpperCase();
	}

	public static String getYesterdayDateString() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		return dateFormat.format(cal.getTime());
	}

	public static String getBeforeYesterdayDateString() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -2);

		return dateFormat.format(cal.getTime());
	}

	public static String getCurrentDateTime() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();

		String dateInString = dateFormat.format(cal.getTime());

		return dateInString;
	}
	
	public static String getCurrentDate() {

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();

		String dateInString = dateFormat.format(cal.getTime());

		return dateInString;
	}

	public static String getRandom28() {

		char[] chars = "4b195529515dbcfa525f3d3261648b4c07ef6aa6ca7f548973758a6e25bb9bf17d43104245199d6a4b5e044c4f5f89401de337e16b77a2ac72e8e8a9150cbc2e"
				.toCharArray();

		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 28; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		String output = sb.toString();

		return output;
	}

	public static String decPass(String hashPass) {

		String salt = Util.getRandomSalt();
		int saltLengthHalf = (salt.length()) / 2;
		String md5 = hashPass.substring(saltLengthHalf, hashPass.length());
		md5 = md5.substring(0, md5.length() - saltLengthHalf);

		return md5;
	}
	
	public static boolean isNotValidYear(String year) {
		try{
			int y = Integer.valueOf(year);
			if(y < 1800 || y>9999){
				return true;
			}
		}
		catch(NumberFormatException e){
			return true;
		}
		return false;
	}

	public static String getRandomSalt() {

		char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?"
				.toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 28; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}

		String output = sb.toString();

		return output;

	}
	
	public static boolean checkDueDate(String dueDate) throws ParseException{
		
		String currentDate = Util.getCurrentDate();
		boolean flag = true;
		 SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

	       

	            Date date1 = formatter.parse(dueDate);
	            Date date2 = formatter.parse(currentDate);
	            long diff = date2.getTime() - date1.getTime();
	            
	            if(diff > 0) 
	            	flag = false;

	        
	        
		return flag;
	}

	public static void main(String[] args) throws ParseException {
		
		String dueDate = "01-01-2017";
		System.out.println(checkDueDate(dueDate));
	}
	
	public static String strip_html_tags(String text, String key, Map<String, Exception> exceptions) {
		key = strip_html_tags(text, key);
		if(Util.isEmpty(key)){
			exceptions.put("strip.empty", new EmptyValueException(
					messageUtil.getBundle("strip.empty")));
		}
		return key;
	}

	public static String strip_html_tags(String text, String key) {

		text = text.replaceAll("@(.*?)<style[^>]*?>.*?</style>@siu", " ");
		text = text.replaceAll("@(.*?)<script[^>]*?.*?</script>@siu", " ");
		text = text.replaceAll("@(.*?)<iframe[^>]*?.*?</iframe>@siu", " ");
		text = text.replaceAll("@(.*?)<html[^>]*?.*?</html>@siu", " ");
		String allowed_tags = null;
		int descflg = 0;
		if (key == "firstName" || key == "middleName" || key == "lastName"
				|| key == "menuId" || key == "locationId"
				|| key == "staticBlock" || key == "node" || key == "Node"
				|| key == "locationNameEnglish" || key == "locationNameHindi"
				|| key == "name" || key == "url" || key == "menuTitle"
				|| key == "Status") {
			allowed_tags = "<a><br><b><h1><h2><h3><h4><h5><h6>";
			allowed_tags
					.concat("<table><tr><th><td><img><li><ol><p><strong><table><tr><td><th><u><ul><thead>");
			allowed_tags
					.concat("<tbody><tfoot><em><dd><dt><dl><span><div><del><add><i><hr>");
			allowed_tags
					.concat("<pre><br><blockquote><address><code><caption><abbr><acronym>");
			allowed_tags
					.concat("<cite><dfn><q><ins><sup><sub><kbd><samp><var><tt><small><big>");
			descflg = 1;
		} else {
			allowed_tags = "<br><b>";
		}

		if (isUTF8MisInterpreted(text) == true) {
			text = Jsoup.clean(
					text,
					Whitelist
							.basic()
							.addTags("img")
							.addTags("table")
							.addTags("span")
							.addTags("cite")
							.addTags("dfn")
							.addTags("acronym")
							.addTags("sup")
							.addTags("samp")
							.addTags("strong")
							.addTags("caption")
							.addTags("blockquote")
							.addTags("tfoot")
							.addTags("tbody")
							.addTags("div")
							.addTags("td")
							.addTags("tr")
							.addTags("p")
							.addAttributes("table", "border", "class", "style")
							.addAttributes("span", "style", "strong")
							.addTags("br")
							.addAttributes("img", "alt", "style", "height",
									"width", "src")
							.addAttributes("th", "colspan", "align", "*")
							.addAttributes("thead", "align"));

		}

		//System.out.println(text);
		text = text.replaceAll("/alert/i", "");
		text = text.replaceAll("/onerror=/i", "");
		text = text.replaceAll("/onmouseover=/i", "");
		text = text.replaceAll("/onmouseover/i", "");
		text = text.replaceAll("/onmouseout=/i", "");
		text = text.replaceAll("/onmouseout/i", "");
		text = text.replaceAll("/onkeydown/i", "");
		text = text.replaceAll("/onkeydown=/i", "");
		text = text.replaceAll("/onkeypress=/i", "");
		text = text.replaceAll("/onkeypress/i", "");
		text = text.replaceAll("/onkeyup=/i", "");
		text = text.replaceAll("/onkeyup/i", "");
		text = text.replaceAll("/onclick=/i", "");
		text = text.replaceAll("/onclick/i", "");
		text = text.replaceAll("/onload=/i", "");
		text = text.replaceAll("/onload/i", "");
		text = text.replaceAll("/ondblclick/i", "");
		text = text.replaceAll("/ondblclick=/i", "");
		text = text.replaceAll("/ondrag=/i", "");
		text = text.replaceAll("/ondrag/i", "");
		text = text.replaceAll("/ondragend=/i", "");
		text = text.replaceAll("/ondragend/i", "");
		text = text.replaceAll("/ondragenter=/i", "");
		text = text.replaceAll("/ondragenter/i", "");
		text = text.replaceAll("/ondragleave/i", "");
		text = text.replaceAll("/ondragleave=/i", "");
		text = text.replaceAll("/ondragover=/i", "");
		text = text.replaceAll("/ondragover/i", "");
		text = text.replaceAll("/ondragstart=/i", "");
		text = text.replaceAll("/ondragstart/i", "");
		text = text.replaceAll("/ondrop=/i", "");
		text = text.replaceAll("/ondrop/i", "");
		text = text.replaceAll("/onmousedown/i", "");
		text = text.replaceAll("/onmousedown=/i", "");
		text = text.replaceAll("/onmousemove=/i", "");
		text = text.replaceAll("/onmousemove/i", "");
		text = text.replaceAll("/onmouseup=/i", "");
		text = text.replaceAll("/onmouseup/i", "");
		text = text.replaceAll("/onmousewheel=/i", "");
		text = text.replaceAll("/onmousewheel/i", "");
		text = text.replaceAll("/onscroll=/i", "");
		text = text.replaceAll("/onscroll/i", "");
		text = text.replaceAll("/document.cookie/i", "");
		text = text.replaceAll("/prompt/i", "");
		text = text.replaceAll("/onselect/i", "");
		text = text.replaceAll("/type=/i", "");
		text = text.replaceAll("/document.domain/i", "");
		text = text.replaceAll("/confirm(domain)/i", "");
		if (descflg == 1) {
			return text;
		} else {
			return escapeHtml(text);
		}
	}

	public static boolean isUTF8MisInterpreted(String input) {
		// convenience overload for the most common UTF-8 misinterpretation
		// which is also the case in your question
		return isUTF8MisInterpreted(input, "Windows-1252");
	}

	public static boolean isUTF8MisInterpreted(String input, String encoding) {

		CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
		CharsetEncoder encoder = Charset.forName(encoding).newEncoder();
		ByteBuffer tmp;
		try {
			tmp = encoder.encode(CharBuffer.wrap(input));
		}

		catch (CharacterCodingException e) {
			return false;
		}

		try {
			decoder.decode(tmp);
			return true;
		} catch (CharacterCodingException e) {
			return false;
		}
	}
	
	/**
     * 
     * @param secretKey Key used to encrypt data
     * @param plainText Text input to be encrypted
     * @return Returns encrypted text
     * 
     */
    public String encrypt(String plainText) 
            throws NoSuchAlgorithmException, 
            InvalidKeySpecException, 
            NoSuchPaddingException, 
            InvalidKeyException,
            InvalidAlgorithmParameterException, 
            UnsupportedEncodingException, 
            IllegalBlockSizeException, 
            BadPaddingException{
        //Key generation for enc and desc
    	String secretKey="ezeon8547";
        KeySpec keySpec = new PBEKeySpec(secretKey.toCharArray(), salt, iterationCount);
        SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);        
         // Prepare the parameter to the ciphers
        AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

        //Enc process
        ecipher = Cipher.getInstance(key.getAlgorithm());
        ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);      
        String charSet="UTF-8";       
        byte[] in = plainText.getBytes(charSet);
        byte[] out = ecipher.doFinal(in);
        String encStr=new sun.misc.BASE64Encoder().encode(out);
        
        return encStr;
    }
    
    Cipher ecipher;
    Cipher dcipher;
    // 8-byte Salt
    byte[] salt = {
        (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
        (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
    };
    // Iteration count
    int iterationCount = 19;

}
