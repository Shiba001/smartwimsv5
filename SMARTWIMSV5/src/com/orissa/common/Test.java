package com.orissa.common;

public class Test {

	public static void main(String[] args) {
		
		String table[] = { "daily_complaints", "daily_connection",
				"daily_water_testing", "daily_tankers", "daily_water_charges_tax",
				"daily_water_supply" };
		
		String id[] = { "tw_id", "con_id",
				"wt_id", "tank_id", "W_tax_id",
				"ws_id" };
		
		for (int i = 0; i < table.length; i++) {
			
			int before = -46;
			String tableName = table[i];
			int k = 0;
			for(int j = 1 ; j <= 31; j++) {
		
				Test2 test2 = new Test2();
				String date_of_report = test2.getYesterdayDateString(before);
				
				System.out.println("before --- >>> " +before);
				before = -46 + j;
				System.out.println("date_of_report --- >>> " +date_of_report);
				
				int start = 1 + k;
				System.out.println("Start == "+start);
				k = j * 127;
				System.out.println("Loop Count --- >>> " +j);
				System.out.println("End --- >>> " +k);
				
				int result = test2.dateOfReportUpdate(tableName, date_of_report, start, k, id[i]);
				
				System.out.println("result --- >>> " +result);
				
			}
		}
	}
	
	

}
