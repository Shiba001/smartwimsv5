package com.orissa.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.orissa.connection.DBConnection;

public class Test2 {

	public String getYesterdayDateString(int before) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, before);

		return dateFormat.format(cal.getTime());
	}

	public int dateOfReportUpdate(String tableName, String date_of_report,
			int start, int end, String id) {

		int result = 0;
		Connection connection = null;
		try {

			DBConnection dbConnection = new DBConnection();
			connection = dbConnection.Connect();
			PreparedStatement pst = null;

			String query = "update " + tableName + " set date_of_report = '"
					+ date_of_report + "' where "+id+" between " + start
					+ " and " + end;
			pst = connection.prepareStatement(query);
			System.out.println("pst ---- >>> "+pst);
			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

				}
			} catch (Exception e) {

				e.printStackTrace();

			}

		}

		return result;

	}
}
