package com.orissa.common;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.orissa.model.PhysicalInfraElectricalInstallationsModel;
import com.orissa.model.PhysicalInfraProductionWellsModel;
import com.orissa.model.PhysicalInfraPumpsAndMotorsModel;

@Component
public class SortingMap {

	public HashMap<String, Double> getMiddleValues(
			HashMap<String, Double> actualMap,
			HashMap<String, Double> upperMap, HashMap<String, Double> lowerMap) {

		Set<String> upperMapKeySet = upperMap.keySet();
		Set<String> lowerMapKeySet = lowerMap.keySet();

		Set<String> upperLowerCombSet = new HashSet<String>();
		upperLowerCombSet.addAll(upperMapKeySet);
		upperLowerCombSet.addAll(lowerMapKeySet);

		for (String key : upperLowerCombSet) {
			actualMap.remove(key);
		}

		return actualMap;
	}

	public <K, V extends Comparable<? super V>> Map<K, V> findTop10LpcdUlbs(
			Map<K, V> map, int n) {

		Map<K, V> result = new LinkedHashMap<K, V>();

		try {
				List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(
						map.entrySet());
	
				Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
					public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
						
						int value = 0;
						try {
							value = (o1.getValue()).compareTo(o2.getValue());
							
						} catch (Exception e) {
							//e.printStackTrace();
						}
						
						return value;
					}
				});
	
				Collections.reverse(list);
	
				for (Map.Entry<K, V> entry : list) {
					result.put(entry.getKey(), entry.getValue());
					n--;
					if (n == 0)
						break;
				}
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return result;
	}

	public <K, V extends Comparable<? super V>> Map<K, V> findBottom10LpcdUlbs(
			Map<K, V> map, int n) {
		
		Map<K, V> result = new LinkedHashMap<K, V>();

		try {
				List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(
						map.entrySet());
		
				Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
					public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
						
						int value = 0;
						try {
							value = (o1.getValue()).compareTo(o2.getValue());
							
						} catch (Exception e) {
							//e.printStackTrace();
						}
						
						return value;
					}
				});
		
				for (Map.Entry<K, V> entry : list) {
					result.put(entry.getKey(), entry.getValue());
					n--;
					if (n == 0)
						break;
				}
		
			} catch (Exception e) {
				//e.printStackTrace();
			}
		return result;
	}

	public LinkedHashMap<String, LinkedList<PhysicalInfraPumpsAndMotorsModel>> pumpAndMotorsListToMap(
			LinkedList<PhysicalInfraPumpsAndMotorsModel> al) {

		LinkedHashMap<String, LinkedList<PhysicalInfraPumpsAndMotorsModel>> pumpAndMotorMap = new LinkedHashMap<String, LinkedList<PhysicalInfraPumpsAndMotorsModel>>();

		for (int i = 1; i <= al.size();) {

			pumpAndMotorMap.put(al.get(i - 1).getUlbName(), null);

			LinkedList<PhysicalInfraPumpsAndMotorsModel> inner = new LinkedList<PhysicalInfraPumpsAndMotorsModel>();

			int j = i;
			int count = 0;
			while (true) {

				inner.add(al.get(j - 1));
				count++;

				if (j == al.size())
					break;

				j++;

				if (!(al.get(j - 2).getUlbName().equals(al.get(j - 1)
						.getUlbName())))
					break;

			}

			pumpAndMotorMap.replace(al.get(i - 1).getUlbName(), inner);
			i = i + count;

		}

		return pumpAndMotorMap;

	}

	public LinkedHashMap<String, LinkedList<PhysicalInfraProductionWellsModel>> prodWellsListToMap(
			LinkedList<PhysicalInfraProductionWellsModel> al) {

		LinkedHashMap<String, LinkedList<PhysicalInfraProductionWellsModel>> pumpAndMotorMap = new LinkedHashMap<String, LinkedList<PhysicalInfraProductionWellsModel>>();

		for (int i = 1; i <= al.size();) {

			pumpAndMotorMap.put(al.get(i - 1).getUlbName(), null);

			LinkedList<PhysicalInfraProductionWellsModel> inner = new LinkedList<PhysicalInfraProductionWellsModel>();

			int j = i;
			int count = 0;
			while (true) {

				inner.add(al.get(j - 1));
				count++;

				if (j == al.size())
					break;

				j++;

				if (!(al.get(j - 2).getUlbName().equals(al.get(j - 1)
						.getUlbName())))
					break;

			}

			pumpAndMotorMap.replace(al.get(i - 1).getUlbName(), inner);
			i = i + count;

		}

		return pumpAndMotorMap;

	}

	public LinkedHashMap<String, LinkedList<PhysicalInfraElectricalInstallationsModel>> electricalInstallationListToMap(
			LinkedList<PhysicalInfraElectricalInstallationsModel> al) {

		LinkedHashMap<String, LinkedList<PhysicalInfraElectricalInstallationsModel>> pumpAndMotorMap = new LinkedHashMap<String, LinkedList<PhysicalInfraElectricalInstallationsModel>>();

		for (int i = 1; i <= al.size();) {

			pumpAndMotorMap.put(al.get(i - 1).getUlbName(), null);

			LinkedList<PhysicalInfraElectricalInstallationsModel> inner = new LinkedList<PhysicalInfraElectricalInstallationsModel>();

			int j = i;
			int count = 0;
			while (true) {

				inner.add(al.get(j - 1));
				count++;

				if (j == al.size())
					break;

				j++;

				if (!(al.get(j - 2).getUlbName().equals(al.get(j - 1)
						.getUlbName())))
					break;

			}

			pumpAndMotorMap.replace(al.get(i - 1).getUlbName(), inner);
			i = i + count;

		}

		return pumpAndMotorMap;

	}

}
