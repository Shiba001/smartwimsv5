package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.DailyTankersModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class DailyTankersDao extends DeleteDao {

	private static Logger logger = Logger.getLogger(DailyTankersDao.class);

	@Autowired
	private DBConnection dbConnection;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private ULBNameService ulbNameService;

	@Autowired
	private MessageUtil messageUtil;

	public int addDailyTankers(DailyTankersModel dailyTankersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyTankers -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "insert into daily_tankers(date_of_report,"
					+ "ulb_id," + "no_tank_engd_dept," + "no_tank_hire,"
					+ "tot_no_tank_engd," + "no_pvc_rcc_tanks_depl,"
					+ "popu_served_tanks_pvc_rcc," + "qty_mld,"
					+ "remarks," + "user_id," + "entry_date,"
					+ "IsApproved) values(?,?,?,?,?,?,?,?,?,?,?,?) ";

			pst = connection.prepareStatement(query);
			pst.setString(1, dailyTankersModel.getDate_of_report());
			pst.setInt(2, dailyTankersModel.getUlbId());
			pst.setDouble(3, dailyTankersModel.getNo_tank_engd_dept());
			pst.setDouble(4, dailyTankersModel.getNo_tank_hire());
			pst.setDouble(5, dailyTankersModel.getTot_no_tank_engd());
			pst.setDouble(6, dailyTankersModel.getNo_pvc_rcc_tanks_depl());
			pst.setDouble(7, dailyTankersModel.getPopu_served_tanks_pvc_rcc());
			pst.setDouble(8, dailyTankersModel.getQty_mld());
			pst.setString(9, dailyTankersModel.getRemarks());
			pst.setInt(10, dailyTankersModel.getLoginUsersModel().getUser_id());
			pst.setString(11, Util.getCurrentDateTime());
			pst.setInt(12, dailyTankersModel.getIsApproved());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addDailyTankers");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addDailyTankers"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyTankers -- END");
		}

		return result;

	}

	public LinkedList<DailyTankersModel> fetchDailyTankers(LoginUsersModel loginUsersModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankers -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyTankersModel> dailyTankersModels = new LinkedList<DailyTankersModel>();

		try {
			//String query = "select dt.tank_id,dt.date_of_report,dt.ulb_id,dt.no_tank_engd_dept,dt.no_tank_hire,dt.tot_no_tank_engd,dt.no_pvc_rcc_tanks_depl,dt.popu_served_tanks_pvc_rcc,dt.qty_mld,lpcd,dt.remarks,dt.modify_date,dt.user_id,dt.entry_date,dt.IsApproved,un.ulb_Name from daily_tankers dt,ulbs_name un where dt.ulb_id = un.ulb_id order by dt.entry_date desc";
			String query = "";
			if(loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
				
				query = "select dt.tank_id,dt.date_of_report,dt.ulb_id,dt.no_tank_engd_dept,dt.no_tank_hire,dt.tot_no_tank_engd,dt.no_pvc_rcc_tanks_depl,dt.popu_served_tanks_pvc_rcc,dt.qty_mld,dt.remarks,dt.modify_date,dt.user_id,dt.entry_date,dt.IsApproved,un.ulb_Name from daily_tankers dt,ulbs_name un where dt.ulb_id = un.ulb_id and dt.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dt.ulb_id ="+loginUsersModel.getUlbNameModel().getUlb_id()+" order by dt.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int districtId = (Integer)upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(districtId);
				
				String ulbs = "";
				for(ULBNameModel ulbNameModel:ulbNameModels) {
					
					ulbs = ulbs +ulbNameModel.getUlb_id()+",";
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select dt.tank_id,dt.date_of_report,dt.ulb_id,dt.no_tank_engd_dept,dt.no_tank_hire,dt.tot_no_tank_engd,dt.no_pvc_rcc_tanks_depl,dt.popu_served_tanks_pvc_rcc,dt.qty_mld,dt.remarks,dt.modify_date,dt.user_id,dt.entry_date,dt.IsApproved,un.ulb_Name from daily_tankers dt,ulbs_name un where dt.ulb_id = un.ulb_id and dt.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dt.ulb_id in ("+ulbs+")  order by dt.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int divisionId = (Integer)upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService.fetchDistrictByDivisionId(divisionId);
				
				String ulbs = "";
				for(PhDistrictModel phDistrictModel:districtModels) {
					
					LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(phDistrictModel.getDist_id());
					
					for(ULBNameModel ulbNameModel:ulbNameModels) {
						
						ulbs = ulbs +ulbNameModel.getUlb_id()+",";
					}
					
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select dt.tank_id,dt.date_of_report,dt.ulb_id,dt.no_tank_engd_dept,dt.no_tank_hire,dt.tot_no_tank_engd,dt.no_pvc_rcc_tanks_depl,dt.popu_served_tanks_pvc_rcc,dt.qty_mld,dt.remarks,dt.modify_date,dt.user_id,dt.entry_date,dt.IsApproved,un.ulb_Name from daily_tankers dt,ulbs_name un where dt.ulb_id = un.ulb_id and dt.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dt.ulb_id in ("+ulbs+")  order by dt.entry_date desc";
			
			} else { // Others All (State, PHEO, Circle, H&UD)
				
				query = "select dt.tank_id,dt.date_of_report,dt.ulb_id,dt.no_tank_engd_dept,dt.no_tank_hire,dt.tot_no_tank_engd,dt.no_pvc_rcc_tanks_depl,dt.popu_served_tanks_pvc_rcc,dt.qty_mld,dt.remarks,dt.modify_date,dt.user_id,dt.entry_date,dt.IsApproved,un.ulb_Name from daily_tankers dt,ulbs_name un where dt.ulb_id = un.ulb_id and dt.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') order by dt.entry_date desc";
			}
			
			
			pst = connection.prepareStatement(query);
			pst.setString(1, Util.getYesterdayDateString());
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyTankersModel dailyTankersModel = new DailyTankersModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(15));
				dailyTankersModel.setUlbNameModel(ulbNameModel);
				
				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(12));
				
				dailyTankersModel.setTank_id(rs.getInt(1));
				dailyTankersModel.setDate_of_report(rs.getString(2));
				dailyTankersModel.setNo_tank_engd_dept(rs.getDouble(4));
				dailyTankersModel.setNo_tank_hire(rs.getDouble(5));
				dailyTankersModel.setTot_no_tank_engd(rs.getDouble(6));
				dailyTankersModel.setNo_pvc_rcc_tanks_depl(rs.getDouble(7));
				dailyTankersModel.setPopu_served_tanks_pvc_rcc(rs.getDouble(8));
				dailyTankersModel.setQty_mld(rs.getDouble(9));
				dailyTankersModel.setRemarks(rs.getString(10));
				dailyTankersModel.setModify_date(rs.getString(11));
				dailyTankersModel.setEntry_date(rs.getString(13));
				dailyTankersModel.setIsApproved(rs.getInt(14));
				
				dailyTankersModel.setLoginUsersModel(loginUsersModel2);

				dailyTankersModels.add(dailyTankersModel);

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyTankers");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyTankers"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankers -- END");
		}

		return dailyTankersModels;

	}

	public DailyTankersModel fetchDailyTankersById(int tank_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankersById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		DailyTankersModel dailyTankersModel = new DailyTankersModel();
		try {
			String query = " select dt.tank_id,dt.date_of_report,dt.ulb_id,dt.no_tank_engd_dept,dt.no_tank_hire,dt.tot_no_tank_engd,dt.no_pvc_rcc_tanks_depl,dt.popu_served_tanks_pvc_rcc,dt.qty_mld,dt.remarks,dt.modify_date,dt.user_id,dt.entry_date, dt.IsApproved,un.ulb_Name from daily_tankers dt, ulbs_name un  where dt.ulb_id = un.ulb_id and dt.tank_id =?  order by dt.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, tank_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(15));

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(12));
				dailyTankersModel.setUlbNameModel(ulbNameModel);
				dailyTankersModel.setTank_id(rs.getInt(1));
				dailyTankersModel.setDate_of_report(rs.getString(2));
				dailyTankersModel.setNo_tank_engd_dept(rs.getDouble(4));
				dailyTankersModel.setNo_tank_hire(rs.getDouble(5));
				dailyTankersModel.setTot_no_tank_engd(rs.getDouble(6));
				dailyTankersModel.setNo_pvc_rcc_tanks_depl(rs.getDouble(7));
				dailyTankersModel.setPopu_served_tanks_pvc_rcc(rs.getDouble(8));
				dailyTankersModel.setQty_mld(rs.getDouble(9));
				dailyTankersModel.setRemarks(rs.getString(10));
				dailyTankersModel.setModify_date(rs.getString(11));
				dailyTankersModel.setEntry_date(rs.getString(13));
				dailyTankersModel.setIsApproved(rs.getInt(14));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyTankersById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyTankersById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankersById -- END");
		}

		return dailyTankersModel;

	}

	public int updateDailyTankers(DailyTankersModel dailyTankersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyTankers -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update daily_tankers " + "set ulb_id=?,"
					+ "date_of_report=?," + "no_tank_engd_dept=?,"
					+ "no_tank_hire=?," + "tot_no_tank_engd=?,"
					+ "no_pvc_rcc_tanks_depl=?,"
					+ "popu_served_tanks_pvc_rcc=?," + "qty_mld=?,"
					+ "remarks=?," + "modify_date=?," + "user_id=?"
					 + " where tank_id=? ";

			pst = connection.prepareStatement(query);
			pst.setInt(1, dailyTankersModel.getUlbId());
			pst.setString(2, dailyTankersModel.getDate_of_report());
			pst.setDouble(3, dailyTankersModel.getNo_tank_engd_dept());
			pst.setDouble(4, dailyTankersModel.getNo_tank_hire());
			pst.setDouble(5, dailyTankersModel.getTot_no_tank_engd());
			pst.setDouble(6, dailyTankersModel.getNo_pvc_rcc_tanks_depl());
			pst.setDouble(7, dailyTankersModel.getPopu_served_tanks_pvc_rcc());
			pst.setDouble(8, dailyTankersModel.getQty_mld());
			pst.setString(9, dailyTankersModel.getRemarks());
			pst.setString(10, Util.getCurrentDateTime());
			pst.setInt(11, dailyTankersModel.getLoginUsersModel().getUser_id());
			//pst.setInt(12, dailyTankersModel.getIsApproved());
			pst.setInt(12, dailyTankersModel.getTank_id());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updateDailyTankers");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updateDailyTankers"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyTankers -- END");
		}

		return result;

	}

	
	
	public LinkedList<DailyTankersModel> fetchAJAXDailyTankers(
			ListingModel listingModel) throws Exception {
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyTankers -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyTankersModel> dailyTankersModels = new LinkedList<DailyTankersModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select dt.tank_id,dt.date_of_report,dt.ulb_id,dt.no_tank_engd_dept,dt.no_tank_hire,dt.tot_no_tank_engd,dt.no_pvc_rcc_tanks_depl,dt.popu_served_tanks_pvc_rcc,dt.qty_mld,dt.remarks,dt.modify_date,dt.user_id,dt.entry_date,dt.IsApproved,un.ulb_Name from daily_tankers dt,ulbs_name un where dt.ulb_id = un.ulb_id and dt.date_of_report =(case '"
						+ listingModel.getDate()
						+ "' when 'null' then dt.date_of_report else str_to_date('"
						+ listingModel.getDate()
						+ "','%Y-%m-%d %H:%i:%s') end) order by dt.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select dt.tank_id,dt.date_of_report,dt.ulb_id,dt.no_tank_engd_dept,dt.no_tank_hire,dt.tot_no_tank_engd,dt.no_pvc_rcc_tanks_depl,dt.popu_served_tanks_pvc_rcc,dt.qty_mld,dt.remarks,dt.modify_date,dt.user_id,dt.entry_date,dt.IsApproved,un.ulb_Name from daily_tankers dt,ulbs_name un where dt.ulb_id = un.ulb_id and dt.date_of_report =(case '"
							+ listingModel.getDate()
							+ "' when 'null' then dt.date_of_report else str_to_date('"
							+ listingModel.getDate()
							+ "','%Y-%m-%d %H:%i:%s') end) and dt.ulb_id in ("
							+ ulbs + ") order by dt.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select dt.tank_id,dt.date_of_report,dt.ulb_id,dt.no_tank_engd_dept,dt.no_tank_hire,dt.tot_no_tank_engd,dt.no_pvc_rcc_tanks_depl,dt.popu_served_tanks_pvc_rcc,dt.qty_mld,dt.remarks,dt.modify_date,dt.user_id,dt.entry_date,dt.IsApproved,un.ulb_Name from daily_tankers dt,ulbs_name un where dt.ulb_id = un.ulb_id and dt.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dt.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dt.ulb_id in ("
								+ ulbs + ") order by dt.entry_date desc";

					} else { // Only For ULB

						query = "select dt.tank_id,dt.date_of_report,dt.ulb_id,dt.no_tank_engd_dept,dt.no_tank_hire,dt.tot_no_tank_engd,dt.no_pvc_rcc_tanks_depl,dt.popu_served_tanks_pvc_rcc,dt.qty_mld,dt.remarks,dt.modify_date,dt.user_id,dt.entry_date,dt.IsApproved,un.ulb_Name from daily_tankers dt,ulbs_name un where dt.ulb_id = un.ulb_id and dt.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dt.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dt.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by dt.entry_date desc";
					}

				}

			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyTankersModel dailyTankersModel = new DailyTankersModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(15));
				dailyTankersModel.setUlbNameModel(ulbNameModel);
				
				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(12));
				
				dailyTankersModel.setTank_id(rs.getInt(1));
				dailyTankersModel.setDate_of_report(rs.getString(2));
				dailyTankersModel.setNo_tank_engd_dept(rs.getDouble(4));
				dailyTankersModel.setNo_tank_hire(rs.getDouble(5));
				dailyTankersModel.setTot_no_tank_engd(rs.getDouble(6));
				dailyTankersModel.setNo_pvc_rcc_tanks_depl(rs.getDouble(7));
				dailyTankersModel.setPopu_served_tanks_pvc_rcc(rs.getDouble(8));
				dailyTankersModel.setQty_mld(rs.getDouble(9));
				dailyTankersModel.setRemarks(rs.getString(10));
				dailyTankersModel.setModify_date(rs.getString(11));
				dailyTankersModel.setEntry_date(rs.getString(13));
				dailyTankersModel.setIsApproved(rs.getInt(14));
				
				dailyTankersModel.setLoginUsersModel(loginUsersModel2);

				dailyTankersModels.add(dailyTankersModel);
			}


		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXDailyTankers");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXDailyTankers"
							+ e.getMessage());
				}

			}

		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyTankers -- END");
		}
		
		return dailyTankersModels;
	}
}
