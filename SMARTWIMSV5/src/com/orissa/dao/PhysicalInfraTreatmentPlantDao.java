package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.PhysicalInfraWaterTreatmentPlantModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class PhysicalInfraTreatmentPlantDao  extends DeleteDao{
	private static Logger logger = Logger
			.getLogger(PhysicalInfraTreatmentPlantDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private ULBNameService ulbNameService;

	public int addPhysicalInfraTreatment(
			PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTreatment -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		int t = 0;

		try {
			String query = "insert into physical_infra_water_treatment_plant(date_of_report,ulb_id,no_of_wtp,cpty_wtp,user_id,entry_date,next_maintenance_date) values(?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?) ";
			pst = connection.prepareStatement(query);
			pst.setString(1, physicalInfraWaterTreatmentPlantModel.getDate_of_report()); 
			pst.setInt(2, physicalInfraWaterTreatmentPlantModel.getUlbId());
			pst.setDouble(3, physicalInfraWaterTreatmentPlantModel.getNo_of_wtp());
			pst.setDouble(4, physicalInfraWaterTreatmentPlantModel.getCpty_wtp());
			pst.setInt(5, physicalInfraWaterTreatmentPlantModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(6, Util.getCurrentDateTime());
			pst.setString(7, physicalInfraWaterTreatmentPlantModel.getDueDate()); 

			t = pst.executeUpdate();
			if (t > 0)
				t = 1;
			else
				t = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addPhysicalInfraTreatment");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addPhysicalInfraTreatment"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTreatment -- END");
		}

		return t;

	}

	public LinkedList<PhysicalInfraWaterTreatmentPlantModel> fetchPhysicalInfraTreatment(LoginUsersModel loginUsersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTreatment -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList <PhysicalInfraWaterTreatmentPlantModel> physicalInfraWaterTreatmentPlantModels = new LinkedList<PhysicalInfraWaterTreatmentPlantModel>();
		
	try {
			
			String query = "";
			if(loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
				
				query =  "select piwtp.wtpid,piwtp.date_of_report,piwtp.ulb_id,piwtp.no_of_wtp,piwtp.cpty_wtp,piwtp.user_id,piwtp.entry_date,piwtp.modify_date, un.ulb_Name,piwtp.next_maintenance_date from physical_infra_water_treatment_plant piwtp,ulbs_name un where piwtp.ulb_id = un.ulb_id  and piwtp.ulb_id ="+loginUsersModel.getUlbNameModel().getUlb_id()+" order by piwtp.entry_date desc";
			
			}  else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int districtId = (Integer)upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(districtId);
				
				String ulbs = "";
				for(ULBNameModel ulbNameModel:ulbNameModels) {
					
					ulbs = ulbs +ulbNameModel.getUlb_id()+",";
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select piwtp.wtpid,piwtp.date_of_report,piwtp.ulb_id,piwtp.no_of_wtp,piwtp.cpty_wtp,piwtp.user_id,piwtp.entry_date,piwtp.modify_date, un.ulb_Name,piwtp.next_maintenance_date from physical_infra_water_treatment_plant piwtp,ulbs_name un where piwtp.ulb_id = un.ulb_id  and piwtp.ulb_id in ("+ulbs+")  order by piwtp.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int divisionId = (Integer)upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService.fetchDistrictByDivisionId(divisionId);
				
				String ulbs = "";
				for(PhDistrictModel phDistrictModel:districtModels) {
					
					LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(phDistrictModel.getDist_id());
					
					for(ULBNameModel ulbNameModel:ulbNameModels) {
						
						ulbs = ulbs +ulbNameModel.getUlb_id()+",";
					}
					
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select piwtp.wtpid,piwtp.date_of_report,piwtp.ulb_id,piwtp.no_of_wtp,piwtp.cpty_wtp,piwtp.user_id,piwtp.entry_date,piwtp.modify_date, un.ulb_Name,piwtp.next_maintenance_date from physical_infra_water_treatment_plant piwtp,ulbs_name un where piwtp.ulb_id = un.ulb_id  and piwtp.ulb_id in ("+ulbs+")  order by piwtp.entry_date desc";
			
			} else { // Others All (State, PHEO, Circle, H&UD)
				
				query = "select piwtp.wtpid,piwtp.date_of_report,piwtp.ulb_id,piwtp.no_of_wtp,piwtp.cpty_wtp,piwtp.user_id,piwtp.entry_date,piwtp.modify_date, un.ulb_Name,piwtp.next_maintenance_date from physical_infra_water_treatment_plant piwtp,ulbs_name un where piwtp.ulb_id = un.ulb_id  order by piwtp.entry_date desc";
			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel = new PhysicalInfraWaterTreatmentPlantModel();
				physicalInfraWaterTreatmentPlantModel.setWtpid(rs.getInt(1));
				physicalInfraWaterTreatmentPlantModel.setDate_of_report(rs
						.getString(2));

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(9));
				physicalInfraWaterTreatmentPlantModel.setUlbNameModel(ulbNameModel);

				physicalInfraWaterTreatmentPlantModel.setNo_of_wtp(rs.getDouble(4));
				physicalInfraWaterTreatmentPlantModel.setCpty_wtp(rs.getDouble(5));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(6)); // To be Changed
				physicalInfraWaterTreatmentPlantModel
						.setLoginUsersModel(loginUsersModel2);

				physicalInfraWaterTreatmentPlantModel.setEntry_date(rs.getString(7));
				physicalInfraWaterTreatmentPlantModel.setModify_date(rs.getString(8));
				physicalInfraWaterTreatmentPlantModel.setDueDate(rs.getString(10));
				physicalInfraWaterTreatmentPlantModels.add(physicalInfraWaterTreatmentPlantModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraTreatment");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraTreatment"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTreatment -- END");
		}

		return physicalInfraWaterTreatmentPlantModels;

	}

	public PhysicalInfraWaterTreatmentPlantModel fetchPhysicalInfraTreatmentById(
			int wtpid) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTreatmentById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel = new PhysicalInfraWaterTreatmentPlantModel();
		try {
			String query = "select piwtp.wtpid,"
					+ "piwtp.date_of_report,"
					+ "piwtp.ulb_id,"
					+ "piwtp.no_of_wtp,"
					+ "piwtp.cpty_wtp,"
					+ "piwtp.user_id,"
					+ "piwtp.entry_date,"
					+ "piwtp.modify_date,"
					+ "un.ulb_Name,piwtp.next_maintenance_date from physical_infra_water_treatment_plant piwtp,ulbs_name un where piwtp.ulb_id = un.ulb_id and piwtp.wtpid =? order by entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, wtpid);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				physicalInfraWaterTreatmentPlantModel.setWtpid(rs.getInt(1));
				physicalInfraWaterTreatmentPlantModel.setDate_of_report(rs
						.getString(2));

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_Name(rs.getString(9));
				ulbNameModel.setUlb_id(rs.getInt(3));
				physicalInfraWaterTreatmentPlantModel.setUlbNameModel(ulbNameModel);

				physicalInfraWaterTreatmentPlantModel.setNo_of_wtp(rs.getDouble(4));
				physicalInfraWaterTreatmentPlantModel.setCpty_wtp(rs.getDouble(5));

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(6)); // To be Changed
				physicalInfraWaterTreatmentPlantModel
						.setLoginUsersModel(loginUsersModel);

				physicalInfraWaterTreatmentPlantModel.setEntry_date(rs.getString(7));
				physicalInfraWaterTreatmentPlantModel.setModify_date(rs.getString(8));
				physicalInfraWaterTreatmentPlantModel.setDueDate(rs.getString(10));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraTreatmentById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraTreatmentById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTreatmentById -- END");
		}

		return physicalInfraWaterTreatmentPlantModel;

	}

	public int updatePhysicalInfraTreatment(
			PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraWaterPipe -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update physical_infra_water_treatment_plant set date_of_report=?,ulb_id=?,no_of_wtp=?,cpty_wtp=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s'),next_maintenance_date=? where wtpid=? ";
			pst = connection.prepareStatement(query);

			pst.setString(1, physicalInfraWaterTreatmentPlantModel.getDate_of_report()); 
			pst.setInt(2, physicalInfraWaterTreatmentPlantModel.getUlbId());

			pst.setDouble(3, physicalInfraWaterTreatmentPlantModel.getNo_of_wtp());
			pst.setDouble(4, physicalInfraWaterTreatmentPlantModel.getCpty_wtp());
			pst.setInt(5, physicalInfraWaterTreatmentPlantModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(6, Util.getCurrentDateTime());
			pst.setString(7, physicalInfraWaterTreatmentPlantModel.getDueDate());
			pst.setInt(8, physicalInfraWaterTreatmentPlantModel.getWtpid());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updatePhysicalInfraWaterPipe");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updatePhysicalInfraWaterPipe"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraWaterPipe -- END");
		}

		return result;

	}
	
	
	public LinkedList<PhysicalInfraWaterTreatmentPlantModel> fetchAJAXPhysicalInfraTreat(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraTreat -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraWaterTreatmentPlantModel> physicalInfraWaterTreatmentPlantModels = new LinkedList<PhysicalInfraWaterTreatmentPlantModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select piwtp.wtpid,piwtp.date_of_report,piwtp.ulb_id,piwtp.no_of_wtp,piwtp.cpty_wtp,piwtp.user_id,piwtp.entry_date,piwtp.modify_date, un.ulb_Name,piwtp.next_maintenance_date from physical_infra_water_treatment_plant piwtp,ulbs_name un  where piwtp.ulb_id = un.ulb_id  order by piwtp.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select piwtp.wtpid,piwtp.date_of_report,piwtp.ulb_id,piwtp.no_of_wtp,piwtp.cpty_wtp,piwtp.user_id,piwtp.entry_date,piwtp.modify_date, un.ulb_Name,piwtp.next_maintenance_date from physical_infra_water_treatment_plant piwtp,ulbs_name un  where piwtp.ulb_id = un.ulb_id  and piwtp.ulb_id in ("
							+ ulbs + ") order by piwtp.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select piwtp.wtpid,piwtp.date_of_report,piwtp.ulb_id,piwtp.no_of_wtp,piwtp.cpty_wtp,piwtp.user_id,piwtp.entry_date,piwtp.modify_date, un.ulb_Name,piwtp.next_maintenance_date from physical_infra_water_treatment_plant piwtp,ulbs_name un  where piwtp.ulb_id = un.ulb_id  and piwtp.ulb_id in ("
								+ ulbs + ") order by piwtp.entry_date desc";

					} else { // Only For ULB

						query = "select piwtp.wtpid,piwtp.date_of_report,piwtp.ulb_id,piwtp.no_of_wtp,piwtp.cpty_wtp,piwtp.user_id,piwtp.entry_date,piwtp.modify_date, un.ulb_Name,piwtp.next_maintenance_date from physical_infra_water_treatment_plant piwtp,ulbs_name un  where piwtp.ulb_id = un.ulb_id  and piwtp.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by piwtp.entry_date desc";
					}

				}

			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel = new PhysicalInfraWaterTreatmentPlantModel();
				physicalInfraWaterTreatmentPlantModel.setWtpid(rs.getInt(1));
				physicalInfraWaterTreatmentPlantModel.setDate_of_report(rs
						.getString(2));

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(9));
				physicalInfraWaterTreatmentPlantModel.setUlbNameModel(ulbNameModel);

				physicalInfraWaterTreatmentPlantModel.setNo_of_wtp(rs.getDouble(4));
				physicalInfraWaterTreatmentPlantModel.setCpty_wtp(rs.getDouble(5));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(6)); // To be Changed
				physicalInfraWaterTreatmentPlantModel
						.setLoginUsersModel(loginUsersModel2);

				physicalInfraWaterTreatmentPlantModel.setEntry_date(rs.getString(7));
				physicalInfraWaterTreatmentPlantModel.setModify_date(rs.getString(8));
				physicalInfraWaterTreatmentPlantModel.setDueDate(rs.getString(10));
				physicalInfraWaterTreatmentPlantModels.add(physicalInfraWaterTreatmentPlantModel);
			}

		} 
		finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXPhysicalInfraTreat");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXPhysicalInfraTreat"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraTreat -- END");
		}

		return physicalInfraWaterTreatmentPlantModels;

	}

	
	
}
