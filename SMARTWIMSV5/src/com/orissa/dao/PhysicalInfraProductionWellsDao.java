package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraProductionWellsModel;
import com.orissa.model.ULBNameModel;

@Repository
public class PhysicalInfraProductionWellsDao {

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	private static Logger logger = Logger
			.getLogger(PhysicalInfraProductionWellsDao.class);

	public int addPhysicalInfraProductionWells(
			PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addProdWells -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "insert into  physical_infra_production_wells(phyInfra_prod_well_slno,"
					+ "date_of_report,"
					+ "ulb_id,"
					+ "loc_of_prod_well,"
					+ "diam_prod_well,"
					+ "discharge,"
					+ "depth,"
					+ "user_id,"
					+ "entry_date,next_maintenance_date) values(?,?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?) ";

			pst = connection.prepareStatement(query);

			pst.setInt(1, physicalInfraProductionWellsModel
					.getPhyInfra_prod_well_slno());
			pst.setString(2,
					physicalInfraProductionWellsModel.getDate_of_report()); // To
																			// be
																			// Changed
			pst.setInt(3, physicalInfraProductionWellsModel.getUlbId());
			pst.setString(4,
					physicalInfraProductionWellsModel.getLoc_of_prod_well());
			pst.setString(5,
					physicalInfraProductionWellsModel.getDiam_prod_well());
			pst.setString(6, physicalInfraProductionWellsModel.getDischarge());
			pst.setString(7, physicalInfraProductionWellsModel.getDepth());
			pst.setInt(8, physicalInfraProductionWellsModel
					.getLoginUsersModel().getUser_id());
			pst.setString(9, Util.getCurrentDateTime());
			pst.setString(10, physicalInfraProductionWellsModel.getDueDate());

			result = pst.executeUpdate();

			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addProdWells");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addProdWells"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addProdWells -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraProductionWellsModel> fetchPhysicalInfraProductionWells(
			int UlbId, String date_of_report) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraProductionWells -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraProductionWellsModel> physicalInfraProductionWellsModels = new LinkedList<PhysicalInfraProductionWellsModel>();
		try {
			String query = "select pipw.phyInfra_prod_well_slno,pipw.ulb_id,pipw.date_of_report,pipw.loc_of_prod_well,pipw.diam_prod_well,pipw.discharge,pipw.depth,pipw.user_id,pipw.entry_date,pipw.modify_date,un.ulb_Name,pipw.next_maintenance_date from physical_infra_production_wells pipw, ulbs_name un where pipw.ulb_id = un.ulb_id and pipw.ulb_id=?  order by pipw.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, UlbId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel = new PhysicalInfraProductionWellsModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(11));
				physicalInfraProductionWellsModel.setUlbNameModel(ulbNameModel);

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(8));
				physicalInfraProductionWellsModel
						.setLoginUsersModel(loginUsersModel);

				physicalInfraProductionWellsModel.setPhyInfra_prod_well_slno(rs
						.getInt(1));

				physicalInfraProductionWellsModel.setDate_of_report(rs
						.getString(3));
				physicalInfraProductionWellsModel.setLoc_of_prod_well(rs
						.getString(4));
				physicalInfraProductionWellsModel.setDiam_prod_well(rs
						.getString(5));
				physicalInfraProductionWellsModel.setDischarge(rs.getString(6));
				physicalInfraProductionWellsModel.setDepth(rs.getString(7));

				physicalInfraProductionWellsModel
						.setEntry_date(rs.getString(9));
				physicalInfraProductionWellsModel.setModify_date(rs
						.getString(10));
				physicalInfraProductionWellsModel.setDueDate(rs.getString(12));

				physicalInfraProductionWellsModels
						.add(physicalInfraProductionWellsModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraProductionWells");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraProductionWells"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraProductionWells -- END");
		}

		return physicalInfraProductionWellsModels;

	}

	public int deletePhysicalInfraProductionWells(
			PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraProductionWells -- START");
		}

		int result = 0;
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		try {

			connection = dbConnection.Connect();
			String query = "delete from physical_infra_production_wells where ulb_id = ?";
			pst = connection.prepareStatement(query);

			pst.setInt(1, physicalInfraProductionWellsModel.getUlbId());
			/*pst.setString(2,
					physicalInfraProductionWellsModel.getDate_of_report());*/
			result = pst.executeUpdate();

			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for deletePhysicalInfraProductionWells");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for deletePhysicalInfraProductionWells"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraProductionWells -- END");
		}

		return result;

	}

}
