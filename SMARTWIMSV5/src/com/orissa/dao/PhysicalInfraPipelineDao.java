package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.PhysicalInfraLengthOfPipeModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class PhysicalInfraPipelineDao extends DeleteDao{
	private static Logger logger = Logger.getLogger(PhysicalInfraPipelineDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private ULBNameService ulbNameService;

	public int addPhysicalInfraPipeline(
			PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPipeline -- START");
		}

	

		Connection connection = dbConnection.Connect();
		PreparedStatement pstmt = null;
		int t = 0;

		try {
			String query = "insert into physical_infra_length_of_pipe(date_of_report,ulb_id,rising_main,distr_netw,user_id,entry_date,next_maintenance_date) values(?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?) ";
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, physicalInfraLengthOfPipeModel.getDate_of_report()); // To be Changed
			pstmt.setInt(2, physicalInfraLengthOfPipeModel.getUlbId());
			pstmt.setDouble(3, physicalInfraLengthOfPipeModel.getRising_main());
			pstmt.setDouble(4, physicalInfraLengthOfPipeModel.getDistr_netw());
			pstmt.setInt(5, physicalInfraLengthOfPipeModel.getLoginUsersModel()
					.getUser_id());
			pstmt.setString(6, Util.getCurrentDateTime());
			pstmt.setString(7, physicalInfraLengthOfPipeModel.getDueDate());

			t = pstmt.executeUpdate();
			if (t > 0)
				t = 1;
			else
				t = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();
pstmt.close();
					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addPhysicalInfraPipeline");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addPhysicalInfraPipeline"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPipeline -- END");
		}

		return t;

	}

	public LinkedList<PhysicalInfraLengthOfPipeModel> fetchPhysicalInfraPipeline(LoginUsersModel loginUsersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterCharges -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList <PhysicalInfraLengthOfPipeModel> physicalInfraLengthOfPipeModels = new LinkedList<PhysicalInfraLengthOfPipeModel>();
		
		try {
			
			String query = "";
			if(loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
				
				query = "select pilp.phyInfra_lpid, pilp.date_of_report, pilp.ulb_id, pilp.rising_main, pilp.distr_netw, pilp.user_id, pilp.entry_date, pilp.modify_date,un.ulb_Name,pilp.next_maintenance_date from physical_infra_length_of_pipe pilp,ulbs_name un where pilp.ulb_id = un.ulb_id  and pilp.ulb_id ="+loginUsersModel.getUlbNameModel().getUlb_id()+" order by pilp.entry_date desc";
				
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int districtId = (Integer)upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(districtId);
				
				String ulbs = "";
				for(ULBNameModel ulbNameModel:ulbNameModels) {
					
					ulbs = ulbs +ulbNameModel.getUlb_id()+",";
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select pilp.phyInfra_lpid, pilp.date_of_report, pilp.ulb_id, pilp.rising_main, pilp.distr_netw, pilp.user_id, pilp.entry_date, pilp.modify_date,un.ulb_Name,pilp.next_maintenance_date from physical_infra_length_of_pipe pilp,ulbs_name un where pilp.ulb_id = un.ulb_id  and pilp.ulb_id in ("+ulbs+")  order by pilp.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int divisionId = (Integer)upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService.fetchDistrictByDivisionId(divisionId);
				
				String ulbs = "";
				for(PhDistrictModel phDistrictModel:districtModels) {
					
					LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(phDistrictModel.getDist_id());
					
					for(ULBNameModel ulbNameModel:ulbNameModels) {
						
						ulbs = ulbs +ulbNameModel.getUlb_id()+",";
					}
					
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select pilp.phyInfra_lpid, pilp.date_of_report, pilp.ulb_id, pilp.rising_main, pilp.distr_netw, pilp.user_id, pilp.entry_date, pilp.modify_date,un.ulb_Name,pilp.next_maintenance_date from physical_infra_length_of_pipe pilp,ulbs_name un where pilp.ulb_id = un.ulb_id  and pilp.ulb_id in ("+ulbs+")  order by pilp.entry_date desc";
			
			} else { // Others All (State, PHEO, Circle, H&UD)
				
				query = "select pilp.phyInfra_lpid, pilp.date_of_report, pilp.ulb_id, pilp.rising_main, pilp.distr_netw, pilp.user_id, pilp.entry_date, pilp.modify_date,un.ulb_Name,pilp.next_maintenance_date from physical_infra_length_of_pipe pilp,ulbs_name un where pilp.ulb_id = un.ulb_id  order by pilp.entry_date desc";
			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel = new PhysicalInfraLengthOfPipeModel();
				
				physicalInfraLengthOfPipeModel.setPhyInfra_lpid(rs.getInt(1));
				physicalInfraLengthOfPipeModel.setDate_of_report(rs.getString(2));
				
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(9));
				physicalInfraLengthOfPipeModel.setUlbNameModel(ulbNameModel);
				
				physicalInfraLengthOfPipeModel.setRising_main(rs.getDouble(4));
				physicalInfraLengthOfPipeModel.setDistr_netw(rs.getDouble(5));
				
				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(6)); // To be Changed
				physicalInfraLengthOfPipeModel.setLoginUsersModel(loginUsersModel2);
				
				physicalInfraLengthOfPipeModel.setEntry_date(rs.getString(7));
				physicalInfraLengthOfPipeModel.setModify_date(rs.getString(8));
				physicalInfraLengthOfPipeModel.setDueDate(rs.getString(10));
				physicalInfraLengthOfPipeModels.add(physicalInfraLengthOfPipeModel);
				
				//physicalInfraLengthOfPipeModels.add(physicalInfraLengthOfPipeModel);
				
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterSupply");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterSupply"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupply -- END");
		}

		return physicalInfraLengthOfPipeModels;

	}


	
	public PhysicalInfraLengthOfPipeModel fetchPhysicalInfraPipeById(int phyInfra_lpid)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterChargeById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel = new PhysicalInfraLengthOfPipeModel();
		try {
			String query = "select pilp.phyInfra_lpid,"
					+ "pilp.date_of_report,"
					+ "pilp.ulb_id,"
					+ "pilp.rising_main,"
					+ "pilp.distr_netw,"
					+ "pilp.user_id,"
					+ "pilp.entry_date,"
					+ "pilp.modify_date,un.ulb_Name,pilp.next_maintenance_date from physical_infra_length_of_pipe pilp,ulbs_name un where pilp.ulb_id = un.ulb_id and pilp.phyInfra_lpid =? order by entry_date desc";
			
			pst = connection.prepareStatement(query);
			pst.setInt(1, phyInfra_lpid);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				physicalInfraLengthOfPipeModel.setPhyInfra_lpid(rs.getInt(1));
				physicalInfraLengthOfPipeModel.setDate_of_report(rs.getString(2));

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(9));
				physicalInfraLengthOfPipeModel.setUlbNameModel(ulbNameModel);
				
				physicalInfraLengthOfPipeModel.setRising_main(rs.getDouble(4));
				physicalInfraLengthOfPipeModel.setDistr_netw(rs.getDouble(5));
				
				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(6)); // To be Changed
				physicalInfraLengthOfPipeModel.setLoginUsersModel(loginUsersModel);
				
				physicalInfraLengthOfPipeModel.setEntry_date(rs.getString(7));
				physicalInfraLengthOfPipeModel.setModify_date(rs.getString(8));
				physicalInfraLengthOfPipeModel.setDueDate(rs.getString(10));
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterChargeById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterChargeById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterChargeById -- END");
		}

		return physicalInfraLengthOfPipeModel;

	}
	
	
	public int updatePhysicalInfraWaterPipe(
			PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraWaterPipe -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update physical_infra_length_of_pipe set date_of_report=?,ulb_id=?,rising_main=?,distr_netw=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s'),next_maintenance_date=? where phyInfra_lpid=? ";
			pst = connection.prepareStatement(query);
			
			pst.setString(1, physicalInfraLengthOfPipeModel.getDate_of_report()); 
			pst.setInt(2, physicalInfraLengthOfPipeModel.getUlbId());
			pst.setDouble(3, physicalInfraLengthOfPipeModel.getRising_main());
			pst.setDouble(4, physicalInfraLengthOfPipeModel.getDistr_netw());
			pst.setInt(5, physicalInfraLengthOfPipeModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(6,  Util.getCurrentDateTime());
			pst.setString(7, physicalInfraLengthOfPipeModel.getDueDate());
			pst.setInt(8, physicalInfraLengthOfPipeModel.getPhyInfra_lpid());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updatePhysicalInfraWaterPipe");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updatePhysicalInfraWaterPipe"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraWaterPipe -- END");
		}

		return result;

	}
	
	
	public LinkedList<PhysicalInfraLengthOfPipeModel> fetchAJAXPhysicalInfraPipe(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraLengthOfPipeModel> physicalInfraLengthOfPipeModels = new LinkedList<PhysicalInfraLengthOfPipeModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select pilp.phyInfra_lpid, pilp.date_of_report, pilp.ulb_id, pilp.rising_main, pilp.distr_netw, pilp.user_id, pilp.entry_date, pilp.modify_date,un.ulb_Name,pilp.next_maintenance_date from physical_infra_length_of_pipe pilp, ulbs_name un  where pilp.ulb_id = un.ulb_id  order by pilp.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select pilp.phyInfra_lpid, pilp.date_of_report, pilp.ulb_id, pilp.rising_main, pilp.distr_netw, pilp.user_id, pilp.entry_date, pilp.modify_date,un.ulb_Name,pilp.next_maintenance_date from physical_infra_length_of_pipe pilp,ulbs_name un  where pilp.ulb_id = un.ulb_id  and pilp.ulb_id in ("
							+ ulbs + ") order by pilp.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select pilp.phyInfra_lpid, pilp.date_of_report, pilp.ulb_id, pilp.rising_main, pilp.distr_netw, pilp.user_id, pilp.entry_date, pilp.modify_date,un.ulb_Name,pilp.next_maintenance_date from physical_infra_length_of_pipe pilp,ulbs_name un  where pilp.ulb_id = un.ulb_id  and pilp.ulb_id in ("
								+ ulbs + ") order by pilp.entry_date desc";

					} else { // Only For ULB

						query = "select pilp.phyInfra_lpid, pilp.date_of_report, pilp.ulb_id, pilp.rising_main, pilp.distr_netw, pilp.user_id, pilp.entry_date, pilp.modify_date,un.ulb_Name,pilp.next_maintenance_date from physical_infra_length_of_pipe pilp,ulbs_name un  where pilp.ulb_id = un.ulb_id and  pilp.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by pilp.entry_date desc";
					}

				}

			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel = new PhysicalInfraLengthOfPipeModel();
				
				physicalInfraLengthOfPipeModel.setPhyInfra_lpid(rs.getInt(1));
				physicalInfraLengthOfPipeModel.setDate_of_report(rs.getString(2));
				
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(9));
				physicalInfraLengthOfPipeModel.setUlbNameModel(ulbNameModel);
				
				physicalInfraLengthOfPipeModel.setRising_main(rs.getDouble(4));
				physicalInfraLengthOfPipeModel.setDistr_netw(rs.getDouble(5));
				
				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(6)); // To be Changed
				physicalInfraLengthOfPipeModel.setLoginUsersModel(loginUsersModel2);
				
				
				physicalInfraLengthOfPipeModel.setEntry_date(rs.getString(7));
				physicalInfraLengthOfPipeModel.setModify_date(rs.getString(8));
				physicalInfraLengthOfPipeModel.setDueDate(rs.getString(10));
				
				physicalInfraLengthOfPipeModels.add(physicalInfraLengthOfPipeModel);
				
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXDailyWaterSupply");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXDailyWaterSupply"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- END");
		}

		return physicalInfraLengthOfPipeModels;

	}
	
	
	
}
