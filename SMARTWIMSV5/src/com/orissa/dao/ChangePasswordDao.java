package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;

@Repository
public class ChangePasswordDao {

	private static Logger logger = Logger.getLogger(DailyComplaintsDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	public int checkOldPassword(String oldPassword, String newPassword,
			int userId) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("checkOldPassword -- START");
		}

		int result = 0;
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		try {
			String query = "select user_id from login_users where User_Password=md5(?) and IsActive=1 and user_id=?";
			pst = connection.prepareStatement(query);
			pst.setString(1, oldPassword);
			pst.setInt(2, userId);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				String query1 = "update login_users set User_Password=md5(?) where user_id=? ";
				pst1 = connection.prepareStatement(query1);
				pst1.setString(1, newPassword);
				pst1.setInt(2, userId);

				result = pst1.executeUpdate();
				if (result > 0)
					result = 1;
				else
					result = 0;
				connection.commit();
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for checkOldPassword");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for checkOldPassword"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("checkOldPassword -- END");
		}

		return result;

	}

}
