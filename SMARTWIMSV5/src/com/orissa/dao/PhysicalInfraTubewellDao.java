package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.PhysicalInfraTubewellsModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class PhysicalInfraTubewellDao extends DeleteDao {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraTubewellDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;
	@Autowired
	private ULBNameService ulbNameService;

	public int addPhysicalInfraTubewell(
			PhysicalInfraTubewellsModel physicalInfraTubewellsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTubewells -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			try {
				String query = "insert into physical_infra_tubewells(ulb_id,date_of_report,tot_no_tw,tot_no_tw_wc,tot_no_tw_ur,pro_tw_wc,user_id,entry_date,next_maintenance_date) values(?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?)";
				pst = connection.prepareStatement(query);
				pst.setInt(1, physicalInfraTubewellsModel.getUlbId());
				pst.setString(2,
						physicalInfraTubewellsModel.getDate_of_report());
				pst.setDouble(3, physicalInfraTubewellsModel.getTot_no_tw());
				pst.setDouble(4, physicalInfraTubewellsModel.getTot_no_tw_wc());
				pst.setDouble(5, physicalInfraTubewellsModel.getTot_no_tw_ur());
				pst.setDouble(6, physicalInfraTubewellsModel.getPro_tw_wc());
				pst.setInt(7, physicalInfraTubewellsModel.getLoginUsersModel()
						.getUser_id());
				pst.setString(8, Util.getCurrentDateTime());
				pst.setString(9,
						physicalInfraTubewellsModel.getDueDate());
				
				result = pst.executeUpdate();

				if (result > 0)
					result = 1;
				else
					result = 0;

				connection.commit();
			} catch (Exception e) {
				e.printStackTrace();
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addPhysicalInfraTubewells");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addPhysicalInfraTubewells"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTubewells -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraTubewellsModel> fetchPhysicalInfraTubewells(LoginUsersModel loginUsersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTubewells -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraTubewellsModel> physicalInfraTubewellsModels = new LinkedList<PhysicalInfraTubewellsModel>();
		try {
			String query = "";
			if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB
																				// User

				query = "select pitw.phyInfra_tw_id,pitw.ulb_id,pitw.date_of_report,pitw.tot_no_tw,pitw.tot_no_tw_wc,pitw.tot_no_tw_ur,pitw.pro_tw_wc,pitw.user_id,pitw.entry_date,pitw.modify_date,un.ulb_Name,pitw.next_maintenance_date from physical_infra_tubewells pitw,ulbs_name un where pitw.ulb_id = un.ulb_id  and pitw.ulb_id ="
						+ loginUsersModel.getUlbNameModel().getUlb_id()
						+ " order by pitw.entry_date desc";

			} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District
																						// User

				LinkedList upperLevels = districtService
						.fetchUpperLevelsByULBId(loginUsersModel
								.getUlbNameModel().getUlb_id());
				int districtId = (Integer) upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService
						.fetchULBNameByDistrictId(districtId);

				String ulbs = "";
				for (ULBNameModel ulbNameModel : ulbNameModels) {

					ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
				}

				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

				query = "select pitw.phyInfra_tw_id,pitw.ulb_id,pitw.date_of_report,pitw.tot_no_tw,pitw.tot_no_tw_wc,pitw.tot_no_tw_ur,pitw.pro_tw_wc,pitw.user_id,pitw.entry_date,pitw.modify_date,un.ulb_Name,pitw.next_maintenance_date from physical_infra_tubewells pitw,ulbs_name un where pitw.ulb_id = un.ulb_id  and pitw.ulb_id in ("
						+ ulbs + ")  order by pitw.entry_date desc";

			} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division
																						// User

				LinkedList upperLevels = districtService
						.fetchUpperLevelsByULBId(loginUsersModel
								.getUlbNameModel().getUlb_id());
				int divisionId = (Integer) upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService
						.fetchDistrictByDivisionId(divisionId);

				String ulbs = "";
				for (PhDistrictModel phDistrictModel : districtModels) {

					LinkedList<ULBNameModel> ulbNameModels = ulbNameService
							.fetchULBNameByDistrictId(phDistrictModel
									.getDist_id());

					for (ULBNameModel ulbNameModel : ulbNameModels) {

						ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
					}

				}

				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

				query = "select pitw.phyInfra_tw_id,pitw.ulb_id,pitw.date_of_report,pitw.tot_no_tw,pitw.tot_no_tw_wc,pitw.tot_no_tw_ur,pitw.pro_tw_wc,pitw.user_id,pitw.entry_date,pitw.modify_date,un.ulb_Name,pitw.next_maintenance_date from physical_infra_tubewells pitw,ulbs_name un where pitw.ulb_id = un.ulb_id and  pitw.ulb_id in ("
						+ ulbs + ")  order by pitw.entry_date desc";

			} else { // Others All (State, PHEO, Circle, H&UD)

				query = "select pitw.phyInfra_tw_id,pitw.ulb_id,pitw.date_of_report,pitw.tot_no_tw,pitw.tot_no_tw_wc,pitw.tot_no_tw_ur,pitw.pro_tw_wc,pitw.user_id,pitw.entry_date,pitw.modify_date,un.ulb_Name,pitw.next_maintenance_date from physical_infra_tubewells pitw,ulbs_name un where pitw.ulb_id = un.ulb_id  order by pitw.entry_date desc";
			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraTubewellsModel physicalInfraTubewellsModel = new PhysicalInfraTubewellsModel();
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(11));
				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(8));

				physicalInfraTubewellsModel.setPhyInfra_tw_id(rs.getInt(1));
				physicalInfraTubewellsModel.setUlbNameModel(ulbNameModel);
				physicalInfraTubewellsModel.setDate_of_report(rs.getString(3));
				physicalInfraTubewellsModel.setTot_no_tw(rs.getDouble(4));
				physicalInfraTubewellsModel.setTot_no_tw_wc(rs.getDouble(5));
				physicalInfraTubewellsModel.setTot_no_tw_ur(rs.getDouble(6));
				physicalInfraTubewellsModel.setPro_tw_wc(rs.getDouble(7));
				physicalInfraTubewellsModel.setLoginUsersModel(loginUsersModel);
				physicalInfraTubewellsModel.setEntry_date(rs.getString(9));
				physicalInfraTubewellsModel.setModify_date(rs.getString(10));
				physicalInfraTubewellsModel.setDueDate(rs.getString(12));
				
				physicalInfraTubewellsModel.setLoginUsersModel(loginUsersModel2);

				physicalInfraTubewellsModels.add(physicalInfraTubewellsModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraTubewells");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraTubewells"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTubewells -- END");
		}

		return physicalInfraTubewellsModels;

	}

	public PhysicalInfraTubewellsModel fetchPhysicalInfraTubewellsById(
			int phyInfra_tw_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTubewellsById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		PhysicalInfraTubewellsModel physicalInfraTubewellsModel = new PhysicalInfraTubewellsModel();
		try {
			String query = "select pitw.phyInfra_tw_id,pitw.ulb_id,pitw.date_of_report,pitw.tot_no_tw,pitw.tot_no_tw_wc,pitw.tot_no_tw_ur,pitw.pro_tw_wc,pitw.user_id,pitw.entry_date,pitw.modify_date,un.ulb_Name,pitw.next_maintenance_date from physical_infra_tubewells pitw,ulbs_name un where pitw.ulb_id = un.ulb_id and pitw.phyInfra_tw_id =? order by pitw.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, phyInfra_tw_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(11));
				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(8));

				physicalInfraTubewellsModel.setPhyInfra_tw_id(rs.getInt(1));
				physicalInfraTubewellsModel.setUlbNameModel(ulbNameModel);
				physicalInfraTubewellsModel.setDate_of_report(rs.getString(3));
				physicalInfraTubewellsModel.setTot_no_tw(rs.getDouble(4));
				physicalInfraTubewellsModel.setTot_no_tw_wc(rs.getDouble(5));
				physicalInfraTubewellsModel.setTot_no_tw_ur(rs.getDouble(6));
				physicalInfraTubewellsModel.setPro_tw_wc(rs.getDouble(7));

				physicalInfraTubewellsModel.setLoginUsersModel(loginUsersModel);
				physicalInfraTubewellsModel.setEntry_date(rs.getString(9));
				physicalInfraTubewellsModel.setModify_date(rs.getString(10));
				physicalInfraTubewellsModel.setDueDate(rs.getString(12));
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraTubewellsById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraTubewellsById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTubewellsById -- END");
		}

		return physicalInfraTubewellsModel;

	}

	public int updatePhysicalInfraTubewells(
			PhysicalInfraTubewellsModel physicalInfraTubewellsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraTubewells -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update physical_infra_tubewells set ulb_id=?,date_of_report=?,tot_no_tw=?,tot_no_tw_wc=?,tot_no_tw_ur=?,pro_tw_wc=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s'),next_maintenance_date=? where phyInfra_tw_id=? ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, physicalInfraTubewellsModel.getUlbId());
			pst.setString(2, physicalInfraTubewellsModel.getDate_of_report());
			pst.setDouble(3, physicalInfraTubewellsModel.getTot_no_tw());
			pst.setDouble(4, physicalInfraTubewellsModel.getTot_no_tw_wc());
			pst.setDouble(5, physicalInfraTubewellsModel.getTot_no_tw_ur());
			pst.setDouble(6, physicalInfraTubewellsModel.getPro_tw_wc());
			pst.setInt(7, physicalInfraTubewellsModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(8, Util.getCurrentDateTime());
			pst.setString(9, physicalInfraTubewellsModel.getDueDate());
			pst.setInt(10, physicalInfraTubewellsModel.getPhyInfra_tw_id());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updatePhysicalInfraTubewells");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updatePhysicalInfraTubewells"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraTubewells -- END");
		}

		return result;

	}
	public LinkedList<PhysicalInfraTubewellsModel> fetchAJAXPhysicalInfraTubewell(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraTubewell -- START");
		}
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraTubewellsModel> physicalInfraTubewellsModels = new LinkedList<PhysicalInfraTubewellsModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select pitw.phyInfra_tw_id,pitw.ulb_id,pitw.date_of_report,pitw.tot_no_tw,pitw.tot_no_tw_wc,pitw.tot_no_tw_ur,pitw.pro_tw_wc,pitw.user_id,pitw.entry_date,pitw.modify_date,un.ulb_Name,pitw.next_maintenance_date from physical_infra_tubewells pitw,ulbs_name un where pitw.ulb_id = un.ulb_id  order by pitw.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select pitw.phyInfra_tw_id,pitw.ulb_id,pitw.date_of_report,pitw.tot_no_tw,pitw.tot_no_tw_wc,pitw.tot_no_tw_ur,pitw.pro_tw_wc,pitw.user_id,pitw.entry_date,pitw.modify_date,un.ulb_Name,pitw.next_maintenance_date from physical_infra_tubewells pitw,ulbs_name un where pitw.ulb_id = un.ulb_id  and pitw.ulb_id in ("
							+ ulbs + ") order by pitw.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select pitw.phyInfra_tw_id,pitw.ulb_id,pitw.date_of_report,pitw.tot_no_tw,pitw.tot_no_tw_wc,pitw.tot_no_tw_ur,pitw.pro_tw_wc,pitw.user_id,pitw.entry_date,pitw.modify_date,un.ulb_Name,pitw.next_maintenance_date from physical_infra_tubewells pitw,ulbs_name un where pitw.ulb_id = un.ulb_id  and pitw.ulb_id in ("
								+ ulbs + ") order by pitw.entry_date desc";

					} else { // Only For ULB

						query = "select pitw.phyInfra_tw_id,pitw.ulb_id,pitw.date_of_report,pitw.tot_no_tw,pitw.tot_no_tw_wc,pitw.tot_no_tw_ur,pitw.pro_tw_wc,pitw.user_id,pitw.entry_date,pitw.modify_date,un.ulb_Name,pitw.next_maintenance_date from physical_infra_tubewells pitw,ulbs_name un where pitw.ulb_id = un.ulb_id  and pitw.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by pitw.entry_date desc";
					}

				}

			}
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraTubewellsModel physicalInfraTubewellsModel = new PhysicalInfraTubewellsModel();
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(11));
				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(8));

				physicalInfraTubewellsModel.setPhyInfra_tw_id(rs.getInt(1));
				physicalInfraTubewellsModel.setUlbNameModel(ulbNameModel);
				physicalInfraTubewellsModel.setDate_of_report(rs.getString(3));
				physicalInfraTubewellsModel.setTot_no_tw(rs.getDouble(4));
				physicalInfraTubewellsModel.setTot_no_tw_wc(rs.getDouble(5));
				physicalInfraTubewellsModel.setTot_no_tw_ur(rs.getDouble(6));
				physicalInfraTubewellsModel.setPro_tw_wc(rs.getDouble(7));
				physicalInfraTubewellsModel.setLoginUsersModel(loginUsersModel2);
				physicalInfraTubewellsModel.setEntry_date(rs.getString(9));
				physicalInfraTubewellsModel.setModify_date(rs.getString(10));
				physicalInfraTubewellsModel.setDueDate(rs.getString(12));

				physicalInfraTubewellsModels.add(physicalInfraTubewellsModel);
			}


		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXPhysicalInfraTubewell");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXPhysicalInfraTubewell"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraTubewell -- END");
		}

		return physicalInfraTubewellsModels;

	}


}
