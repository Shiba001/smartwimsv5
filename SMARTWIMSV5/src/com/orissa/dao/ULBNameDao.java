package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.ULBModel;
import com.orissa.model.ULBNameModel;

@Repository
public class ULBNameDao {

	private static Logger logger = Logger.getLogger(ULBNameDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	public LinkedList<ULBNameModel> fetchULBNameByDivisionId(int divisionId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByDivisionId -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBNameModel> ulbNameModels = new LinkedList<ULBNameModel>();
		try {
			String query = "select un.ulb_id,un.ulb_Name,un.latitude,un.longitude from ulbs_name un, ph_district pd where un.Dist_id=pd.Dist_id and pd.div_id=? order by un.ulb_Name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, divisionId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();

				ulbNameModel.setUlb_id(rs.getInt(1));
				ulbNameModel.setUlb_Name(rs.getString(2));
				ulbNameModel.setLatitude(rs.getString(3));
				ulbNameModel.setLongitude(rs.getString(4));

				ulbNameModels.add(ulbNameModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBNameByDivisionId");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBNameByDivisionId"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByDivisionId -- END");
		}

		return ulbNameModels;

	}
	
	
	public LinkedList<ULBNameModel> fetchULBNameByULBId(int ulbid)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByULBId -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBNameModel> ulbNameModels = new LinkedList<ULBNameModel>();
		try {
			String query = "select ulb_id,ulb_Name,latitude,longitude from ulbs_name where ulb_id =? order by ulb_Name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, ulbid);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();

				ulbNameModel.setUlb_id(rs.getInt(1));
				ulbNameModel.setUlb_Name(rs.getString(2));
				ulbNameModel.setLatitude(rs.getString(3));
				ulbNameModel.setLongitude(rs.getString(4));

				ulbNameModels.add(ulbNameModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBNameByULBId");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBNameByULBId"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByULBId -- END");
		}

		return ulbNameModels;

	}

	public LinkedList<ULBNameModel> fetchULBNameByDistrictId(int districtId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByDistrictId -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBNameModel> ulbNameModels = new LinkedList<ULBNameModel>();
		try {
			String query = "select ulb_id,ulb_Name,latitude,longitude from ulbs_name where Dist_id =? order by ulb_Name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, districtId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();

				ulbNameModel.setUlb_id(rs.getInt(1));
				ulbNameModel.setUlb_Name(rs.getString(2));
				ulbNameModel.setLatitude(rs.getString(3));
				ulbNameModel.setLongitude(rs.getString(4));

				ulbNameModels.add(ulbNameModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBNameByDistrictId");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBNameByDistrictId"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByDistrictId -- END");
		}

		return ulbNameModels;

	}
	
	
	

	public LinkedList<ULBNameModel> fetchAllULBName() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAllULBName -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBNameModel> ulbNameModels = new LinkedList<ULBNameModel>();
		try {
			String query = "select un.ulb_id,un.ulb_Name,un.latitude,un.longitude,u.m_ulb_id,u.ulb_name,pd.Dist_id,pd.Dist_Name,phd.div_id,phd.div_name from ulbs_name un,ulbs u,ph_district pd,ph_division phd where un.m_ulb_id = u.m_ulb_id and u.Dist_id = pd.Dist_id and pd.div_id = phd.div_id order by un.ulb_Name";
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();

				ulbNameModel.setUlb_id(rs.getInt(1));
				ulbNameModel.setUlb_Name(rs.getString(2));
				ulbNameModel.setLatitude(rs.getString(3));
				ulbNameModel.setLongitude(rs.getString(4));
				
				ULBModel ulbModel = new ULBModel();
				
				ulbModel.setM_ulb_id(rs.getInt(5));
				ulbModel.setUlb_name(rs.getString(6));
				
				PhDistrictModel phDistrictModel = new PhDistrictModel();
				
				phDistrictModel.setDist_id(rs.getInt(7));
				phDistrictModel.setDist_Name(rs.getString(8));
				
				PhDivisionModel phDivisionModel = new PhDivisionModel();
				
				phDivisionModel.setDiv_id(rs.getInt(9));
				phDivisionModel.setDiv_name(rs.getString(10));
				
				phDistrictModel.setPhDivisionModel(phDivisionModel);
				
				ulbNameModel.setUlbModel(ulbModel);
				ulbNameModel.setPhDistrictModel(phDistrictModel);

				ulbNameModels.add(ulbNameModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAllULBName");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAllULBName"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAllULBName -- END");
		}

		return ulbNameModels;

	}
	
	public LinkedList<ULBNameModel> fetchULBNameAmrutType(int amrutTypeId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameAmrutType -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBNameModel> ulbNameModels = new LinkedList<ULBNameModel>();
		try {
			String query = "select ulb_id,ulb_Name,latitude,longitude from ulbs_name where Amrut_type_id =? order by ulb_Name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, amrutTypeId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();

				ulbNameModel.setUlb_id(rs.getInt(1));
				ulbNameModel.setUlb_Name(rs.getString(2));
				ulbNameModel.setLatitude(rs.getString(3));
				ulbNameModel.setLongitude(rs.getString(4));

				ulbNameModels.add(ulbNameModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBNameAmrutType");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBNameAmrutType"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameAmrutType -- END");
		}

		return ulbNameModels;

	}
	
	public LinkedList<ULBNameModel> fetchULBNameByULBS(int m_ulb_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByULBS -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBNameModel> ulbNameModels = new LinkedList<ULBNameModel>();
		try {
			String query = "select ulb_id,ulb_Name,latitude,longitude from ulbs_name where m_ulb_id =? order by ulb_Name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, m_ulb_id);
			//System.out.println("fetchULBNameByULBS ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();

				ulbNameModel.setUlb_id(rs.getInt(1));
				ulbNameModel.setUlb_Name(rs.getString(2));
				ulbNameModel.setLatitude(rs.getString(3));
				ulbNameModel.setLongitude(rs.getString(4));

				ulbNameModels.add(ulbNameModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBNameByULBS");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBNameByULBS"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByULBS -- END");
		}

		return ulbNameModels;

	}
}
