package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;
import com.orissa.model.ULBNameModel;
import com.sun.istack.internal.logging.Logger;

@Repository
public class DataEntryStatusDao {

	private static Logger logger = Logger.getLogger(DataEntryStatusDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	public List getResult(List<ULBNameModel> l2, String date) throws Exception {

		List result = new LinkedList();

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		ResultSet rs = null;

		String table[] = { "daily_complaints", "daily_connection",
				"daily_standposts", "daily_tankers", "daily_water_charges_tax",
				"daily_water_supply" };

		int rows = 0;

		int x = 0, y = 0, z = 0;
		 int xr = 0, yr = 0, zr = 0;

		for (int a = 0; a < l2.size(); a++) {
			ULBNameModel listid = l2.get(a);
			int ulbid = listid.getUlb_id();
	
			//System.out.println("satya list id == " + listid);

			for (int i = 0; i < table.length; i++) {

				String tablename = table[i];

				String query = "SELECT * FROM " + tablename
						+ " WHERE date_of_report ='" + date + "' and ulb_id="
						+ ulbid + "";
				//System.out.println(query);
				pst = connection.prepareStatement(query);

				rs = (ResultSet) pst.executeQuery();
				rs.last();
				rows = rs.getRow();

				//System.out.println("row values --> " + rows);

				if (x == 5 && z == 5) {
					x = 0;
					z = 0;
					y = 5;
					break;
				}/*
				 * else if (y == 5) { y = 0; }
				 */
				else if (rows == 1) {
					x = 5;
					continue;

				} else if (rows == 0) {
					z = 5;
					continue;
				}

			}

			//System.out.println("x full value - " + x);
			//System.out.println("y Partial value - " + y);
			//System.out.println("z no value - " + z);

			if (x == 5) {
				xr += 1;
				//System.out.println("xrr -" + xr);
			} else if (y == 5 && z == 5) {
				zr += 1;
				//System.out.println("zrr new -" + zr);
			}

			else if (y == 5) {
				yr += 1;
				//System.out.println("yrr -" + yr);
			} else if (z == 5) {
				zr += 1;
				//System.out.println("zrr -" + zr);
			}

		}

		//System.out.println("full value count    - " + xr);
		//System.out.println("partial value count - " + yr);
		//System.out.println("No value count      - " + zr);

		result.add(xr);
		result.add(yr);
		result.add(zr);

		return result;

	}
	
	
	public List getApprovalStatus(List<ULBNameModel> l2, String date) throws Exception {

		List result = new LinkedList();

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		ResultSet rs = null;

		String table[] = { "daily_complaints", "daily_connection",
				"daily_tankers", "daily_water_charges_tax",
				"daily_water_supply" , "daily_water_testing" };

		int rows = 0;

		int x = 0, z = 0;
		int xr = 0, zr = 0;

		for (int a = 0; a < l2.size(); a++) {
			ULBNameModel listid = l2.get(a);
			int ulbid = listid.getUlb_id();
			// System.out.println("satya list generate "+l.get(a));
			//System.out.println("satya list id == " + listid);

			for (int i = 0; i < table.length; i++) {

				
				String tablename = table[i];

				String query = "SELECT IsApproved FROM " + tablename
						+ " WHERE date_of_report ='" + date + "' and ulb_id="
						+ ulbid +"";
				
				//String query ="select IsApproved  from "+tablename+" where date_of_report='2016-08-09' and ulb_id ="+ ulbid +"";
				
				//System.out.println(query);
				pst = connection.prepareStatement(query);
				int flag_value = 3;
				
				try{
					
				rs = (ResultSet) pst.executeQuery();
				 
				if(rs.next())
				{
				flag_value=rs.getInt("IsApproved");
				}
			
				}
				catch(Exception e){
					
				}
				
				rs.last();
				rows = rs.getRow();
				//System.out.println("row values --> " + rows);
				//System.out.println("flag_value - > " +flag_value);
				
				if(flag_value==3){
				continue;	
				}				
				else	if(rows >= 0 && flag_value ==1){
					x += 1;
					continue;
				} else {
					z += 1;
					continue;
				}
				
				
			/*	if (x == 5 && z == 5) {
					x = 0;
					z = 0;
			
					break;
				}
				 * else if (y == 5) { y = 0; }
				 
				else if (rows == 1) {
					x = 5;
					continue;

				} else if (rows == 0) {
					z = 5;
					continue;
				}*/

			}

     		//System.out.println("x IsApproved value - " + x);
			//System.out.println("z Pending value - " + z);

/*			if (x == 1) {
				xr += 1;
				System.out.println("xrr -" + xr);
			} else if (z == 1) {
				zr += 1;
				System.out.println("zrr new -" + zr);
			}*/
			
		}

		//System.out.println("IsApproved count    - " + xr);
		//System.out.println("Pending count      - " + zr);

		result.add(x);
		result.add(z);

		return result;

	}
	
}
