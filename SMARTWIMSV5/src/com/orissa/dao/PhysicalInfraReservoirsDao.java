package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.PhysicalInfraStorageReservoirsModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class PhysicalInfraReservoirsDao extends DeleteDao {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraReservoirsDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbNameService;

	/**
	 * This is used to save the physicalInfraStorageReservoirsModel Data
	 * 
	 * @param physicalInfraStorageReservoirsModel
	 * @return
	 * @throws Exception
	 */
	public int addPhysicalInfraresivors(
			PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyComplaints -- START");
		}

		int t = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "insert into physical_infra_storage_reservoirs(date_of_report,ulb_id,no_grnd_strg_res,cpty_grnd_strg_res,no_ele_strg_res,cpty_ele_strg_res,user_id,entry_date,next_maintenance_date) values(?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?)";
			pst = connection.prepareStatement(query);
			pst.setString(1, physicalInfraStorageReservoirsModel.getDate_of_report());
			pst.setInt(2, physicalInfraStorageReservoirsModel.getUlbId());
			pst.setDouble(3,
					physicalInfraStorageReservoirsModel.getNo_grnd_strg_res());
			pst.setDouble(4,
					physicalInfraStorageReservoirsModel.getCpty_grnd_strg_res());
			pst.setDouble(5,
					physicalInfraStorageReservoirsModel.getNo_ele_strg_res());
			pst.setDouble(6,
					physicalInfraStorageReservoirsModel.getCpty_ele_strg_res());
			pst.setInt(7, physicalInfraStorageReservoirsModel
					.getLoginUsersModel().getUser_id());
			pst.setString(8, Util.getCurrentDateTime());
			pst.setString(9, physicalInfraStorageReservoirsModel.getDueDate());

			t = pst.executeUpdate();
			if (t > 0)
				t = 1;
			else
				t = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addPhysicalInfraresivors");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addPhysicalInfraresivors"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraresivors -- END");
		}

		return t;

	}

	public LinkedList<PhysicalInfraStorageReservoirsModel> fetchPhysicalInfraStorageResivors(LoginUsersModel loginUsersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalStorageInfraresivors -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraStorageReservoirsModel> physicalInfraStorageReservoirsModels = new LinkedList<PhysicalInfraStorageReservoirsModel>();
		try {
			String query = "";
			if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB
																				// User

				query = "select pisr.phyInfra_sr_id, pisr.date_of_report, pisr.ulb_id, pisr.no_grnd_strg_res, pisr.cpty_grnd_strg_res, pisr.no_ele_strg_res, pisr.cpty_ele_strg_res, pisr.user_id, pisr.entry_date, pisr.modify_date, un.ulb_Name,pisr.next_maintenance_date from physical_infra_storage_reservoirs pisr,ulbs_name un  where pisr.ulb_id = un.ulb_id  and pisr.ulb_id ="
						+ loginUsersModel.getUlbNameModel().getUlb_id()
						+ " order by pisr.entry_date desc";

			} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District
																						// User

				LinkedList upperLevels = districtService
						.fetchUpperLevelsByULBId(loginUsersModel
								.getUlbNameModel().getUlb_id());
				int districtId = (Integer) upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService
						.fetchULBNameByDistrictId(districtId);

				String ulbs = "";
				for (ULBNameModel ulbNameModel : ulbNameModels) {

					ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
				}

				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

				query = "select pisr.phyInfra_sr_id, pisr.date_of_report, pisr.ulb_id, pisr.no_grnd_strg_res, pisr.cpty_grnd_strg_res, pisr.no_ele_strg_res, pisr.cpty_ele_strg_res, pisr.user_id, pisr.entry_date, pisr.modify_date, un.ulb_Name,pisr.next_maintenance_date from physical_infra_storage_reservoirs pisr,ulbs_name un  where pisr.ulb_id = un.ulb_id  and pisr.ulb_id in ("
						+ ulbs + ")  order by pisr.entry_date desc";

			} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division
																						// User

				LinkedList upperLevels = districtService
						.fetchUpperLevelsByULBId(loginUsersModel
								.getUlbNameModel().getUlb_id());
				int divisionId = (Integer) upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService
						.fetchDistrictByDivisionId(divisionId);

				String ulbs = "";
				for (PhDistrictModel phDistrictModel : districtModels) {

					LinkedList<ULBNameModel> ulbNameModels = ulbNameService
							.fetchULBNameByDistrictId(phDistrictModel
									.getDist_id());

					for (ULBNameModel ulbNameModel : ulbNameModels) {

						ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
					}

				}

				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

				query = "select pisr.phyInfra_sr_id, pisr.date_of_report, pisr.ulb_id, pisr.no_grnd_strg_res, pisr.cpty_grnd_strg_res, pisr.no_ele_strg_res, pisr.cpty_ele_strg_res, pisr.user_id, pisr.entry_date, pisr.modify_date, un.ulb_Name,pisr.next_maintenance_date from physical_infra_storage_reservoirs pisr,ulbs_name un  where pisr.ulb_id = un.ulb_id  and pisr.ulb_id in ("
						+ ulbs + ")  order by pisr.entry_date desc";

			} else { // Others All (State, PHEO, Circle, H&UD)

				query = "select pisr.phyInfra_sr_id, pisr.date_of_report, pisr.ulb_id, pisr.no_grnd_strg_res, pisr.cpty_grnd_strg_res, pisr.no_ele_strg_res, pisr.cpty_ele_strg_res, pisr.user_id, pisr.entry_date, pisr.modify_date, un.ulb_Name,pisr.next_maintenance_date from physical_infra_storage_reservoirs pisr,ulbs_name un  where pisr.ulb_id = un.ulb_id  order by pisr.entry_date desc";
			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel = new PhysicalInfraStorageReservoirsModel();
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(11));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(8));
				physicalInfraStorageReservoirsModel.setPhyInfra_sr_id(rs
						.getInt(1));
				physicalInfraStorageReservoirsModel.setDate_of_report(rs
						.getString(2));
				physicalInfraStorageReservoirsModel
						.setUlbNameModel(ulbNameModel);
				physicalInfraStorageReservoirsModel
						.setLoginUsersModel(loginUsersModel);
				physicalInfraStorageReservoirsModel.setNo_grnd_strg_res(rs
						.getDouble(4));
				physicalInfraStorageReservoirsModel.setCpty_grnd_strg_res(rs
						.getDouble(5));
				physicalInfraStorageReservoirsModel.setNo_ele_strg_res(rs
						.getDouble(6));
				physicalInfraStorageReservoirsModel.setCpty_ele_strg_res(rs
						.getDouble(7));
				physicalInfraStorageReservoirsModel.setEntry_date(rs
						.getString(9));
				physicalInfraStorageReservoirsModel.setModify_date(rs
						.getString(10));
				physicalInfraStorageReservoirsModel.setDueDate(rs
						.getString(12));

				physicalInfraStorageReservoirsModels
						.add(physicalInfraStorageReservoirsModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraStorageReservoirs");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraStorageReservoirs"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStorageReservoirs -- END");
		}

		return physicalInfraStorageReservoirsModels;

	}

	public PhysicalInfraStorageReservoirsModel fetchPhysicalInfraStorageReservoirsById(
			int phyInfra_sr_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStorageReservoirsById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel = new PhysicalInfraStorageReservoirsModel();
		try {
			String query = "select pisr.phyInfra_sr_id, pisr.date_of_report, pisr.ulb_id, pisr.no_grnd_strg_res, pisr.cpty_grnd_strg_res, pisr.no_ele_strg_res, pisr.cpty_ele_strg_res, pisr.user_id, pisr.entry_date, pisr.modify_date, un.ulb_Name,pisr.next_maintenance_date from physical_infra_storage_reservoirs pisr,ulbs_name un where pisr.ulb_id = un.ulb_id and pisr.phyInfra_sr_id =? order by pisr.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, phyInfra_sr_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(11));
				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(8));
				physicalInfraStorageReservoirsModel.setPhyInfra_sr_id(rs
						.getInt(1));
				physicalInfraStorageReservoirsModel.setDate_of_report(rs
						.getString(2));
				physicalInfraStorageReservoirsModel
						.setUlbNameModel(ulbNameModel);
				physicalInfraStorageReservoirsModel
						.setLoginUsersModel(loginUsersModel);
				physicalInfraStorageReservoirsModel.setNo_grnd_strg_res(rs
						.getDouble(4));
				physicalInfraStorageReservoirsModel.setCpty_grnd_strg_res(rs
						.getDouble(5));
				physicalInfraStorageReservoirsModel.setNo_ele_strg_res(rs
						.getDouble(6));
				physicalInfraStorageReservoirsModel.setCpty_ele_strg_res(rs
						.getDouble(7));
				physicalInfraStorageReservoirsModel.setEntry_date(rs
						.getString(9));
				physicalInfraStorageReservoirsModel.setModify_date(rs
						.getString(10));
				physicalInfraStorageReservoirsModel.setDueDate(rs
						.getString(12));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraStorageReservoirsById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraStorageReservoirsById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStorageReservoirsById -- END");
		}

		return physicalInfraStorageReservoirsModel;

	}

	public int updatePhysicalInfraStorageReservoirs(
			PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraStorageReservoirs -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update physical_infra_storage_reservoirs set ulb_id=?,date_of_report=?,no_grnd_strg_res=?,cpty_grnd_strg_res=?,no_ele_strg_res=?,cpty_ele_strg_res=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s'),next_maintenance_date=? where phyInfra_sr_id=? ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, physicalInfraStorageReservoirsModel.getUlbId());
			pst.setString(2, physicalInfraStorageReservoirsModel.getDate_of_report());
			pst.setDouble(3,
					physicalInfraStorageReservoirsModel.getNo_grnd_strg_res());
			pst.setDouble(4,
					physicalInfraStorageReservoirsModel.getCpty_grnd_strg_res());
			pst.setDouble(5,
					physicalInfraStorageReservoirsModel.getNo_ele_strg_res());
			pst.setDouble(6,
					physicalInfraStorageReservoirsModel.getCpty_ele_strg_res());
			pst.setInt(7, physicalInfraStorageReservoirsModel
					.getLoginUsersModel().getUser_id());
			pst.setString(8, Util.getCurrentDateTime());
			pst.setString(9, physicalInfraStorageReservoirsModel.getDueDate());
			pst.setInt(10, physicalInfraStorageReservoirsModel.getPhyInfra_sr_id());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updatePhysicalInfraStorageReservoirs");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updatePhysicalInfraStorageReservoirs"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraStorageReservoirs -- END");
		}

		return result;

	}
	public LinkedList<PhysicalInfraStorageReservoirsModel> fetchAJAXPhysicalInfraStorageReservoirs(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStorageReservoirs -- START");
		}
		System.out.println("ajax call");
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraStorageReservoirsModel> physicalInfraStorageReservoirsModels = new LinkedList<PhysicalInfraStorageReservoirsModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select pisr.phyInfra_sr_id, pisr.date_of_report, pisr.ulb_id, pisr.no_grnd_strg_res, pisr.cpty_grnd_strg_res, pisr.no_ele_strg_res, pisr.cpty_ele_strg_res, pisr.user_id, pisr.entry_date, pisr.modify_date, un.ulb_Name,pisr.next_maintenance_date from physical_infra_storage_reservoirs pisr,ulbs_name un  where pisr.ulb_id = un.ulb_id  order by pisr.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select pisr.phyInfra_sr_id, pisr.date_of_report, pisr.ulb_id, pisr.no_grnd_strg_res, pisr.cpty_grnd_strg_res, pisr.no_ele_strg_res, pisr.cpty_ele_strg_res, pisr.user_id, pisr.entry_date, pisr.modify_date, un.ulb_Name,pisr.next_maintenance_date from physical_infra_storage_reservoirs pisr,ulbs_name un  where pisr.ulb_id = un.ulb_id  and pisr.ulb_id in ("
							+ ulbs + ") order by pisr.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select pisr.phyInfra_sr_id, pisr.date_of_report, pisr.ulb_id, pisr.no_grnd_strg_res, pisr.cpty_grnd_strg_res, pisr.no_ele_strg_res, pisr.cpty_ele_strg_res, pisr.user_id, pisr.entry_date, pisr.modify_date, un.ulb_Name,pisr.next_maintenance_date from physical_infra_storage_reservoirs pisr,ulbs_name un  where pisr.ulb_id = un.ulb_id  and pisr.ulb_id in ("
								+ ulbs + ") order by pisr.entry_date desc";

					} else { // Only For ULB

						query = "select pisr.phyInfra_sr_id, pisr.date_of_report, pisr.ulb_id, pisr.no_grnd_strg_res, pisr.cpty_grnd_strg_res, pisr.no_ele_strg_res, pisr.cpty_ele_strg_res, pisr.user_id, pisr.entry_date, pisr.modify_date, un.ulb_Name,pisr.next_maintenance_date from physical_infra_storage_reservoirs pisr,ulbs_name un  where pisr.ulb_id = un.ulb_id  and pisr.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by pisr.entry_date desc";
					}

				}

			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel = new PhysicalInfraStorageReservoirsModel();
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(11));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(8));
				physicalInfraStorageReservoirsModel.setPhyInfra_sr_id(rs
						.getInt(1));
				physicalInfraStorageReservoirsModel.setDate_of_report(rs
						.getString(2));
				physicalInfraStorageReservoirsModel
						.setUlbNameModel(ulbNameModel);
				physicalInfraStorageReservoirsModel
						.setLoginUsersModel(loginUsersModel2);
				physicalInfraStorageReservoirsModel.setNo_grnd_strg_res(rs
						.getDouble(4));
				physicalInfraStorageReservoirsModel.setCpty_grnd_strg_res(rs
						.getDouble(5));
				physicalInfraStorageReservoirsModel.setNo_ele_strg_res(rs
						.getDouble(6));
				physicalInfraStorageReservoirsModel.setCpty_ele_strg_res(rs
						.getDouble(7));
				physicalInfraStorageReservoirsModel.setEntry_date(rs
						.getString(9));
				physicalInfraStorageReservoirsModel.setModify_date(rs
						.getString(10));
				physicalInfraStorageReservoirsModel.setDueDate(rs
						.getString(12));

				physicalInfraStorageReservoirsModels
						.add(physicalInfraStorageReservoirsModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXPhysicalInfraStorageReservoirs");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXPhysicalInfraStorageReservoirs"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStorageReservoirs -- END");
		}

		return physicalInfraStorageReservoirsModels;

	}

}
