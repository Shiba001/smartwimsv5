package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.PhysicalInfraConnectionModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class PhysicalInfraConnectionDao extends DeleteDao {
	
	private static Logger logger = Logger.getLogger(PhysicalInfraConnectionDao.class);
	
	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbNameService;
	
	public int addPhysicalInfraConnection(PhysicalInfraConnectionModel physicalInfraConnectionModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraConnection -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "insert into physical_infra_connection(ulb_id,date_of_report,no_res_cons,no_ind_cons,no_com_cons,no_inst_cons,tot_no_cons,user_id,entry_date,next_maintenance_date) values(?,?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?)";
			pst = connection.prepareStatement(query);
			pst.setInt(1, physicalInfraConnectionModel.getUlbId());
			pst.setString(2,physicalInfraConnectionModel.getDate_of_report());
			pst.setDouble(3, physicalInfraConnectionModel.getNo_com_cons());
			pst.setDouble(4, physicalInfraConnectionModel.getNo_ind_cons());
			pst.setDouble(5, physicalInfraConnectionModel.getNo_com_cons());
			pst.setDouble(6, physicalInfraConnectionModel.getNo_inst_cons());
			pst.setDouble(7, physicalInfraConnectionModel.getTot_no_cons());
			pst.setInt(8, physicalInfraConnectionModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(9, Util.getCurrentDateTime());
			pst.setString(10,physicalInfraConnectionModel.getDueDate());
			
			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addPhysicalInfraConnection");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addPhysicalInfraConnection"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraConnection -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraConnectionModel> fetchPhysicalInfraConnection(LoginUsersModel loginUsersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraConnection -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraConnectionModel> physicalInfraConnectionModels = new LinkedList<PhysicalInfraConnectionModel>();
		try {
			String query = "";
			if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB
																				// User

				query = "select pic.phyInfra_con_id,pic.ulb_id,pic.date_of_report,pic.no_res_cons,pic.no_ind_cons,pic.no_com_cons,pic.no_inst_cons,pic.tot_no_cons,pic.user_id,pic.entry_date,pic.modify_date,un.ulb_Name,pic.next_maintenance_date from physical_infra_connection pic, ulbs_name un where pic.ulb_id = un.ulb_id  and pic.ulb_id ="
						+ loginUsersModel.getUlbNameModel().getUlb_id()
						+ " order by pic.entry_date desc";

			} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District
																						// User

				LinkedList upperLevels = districtService
						.fetchUpperLevelsByULBId(loginUsersModel
								.getUlbNameModel().getUlb_id());
				int districtId = (Integer) upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService
						.fetchULBNameByDistrictId(districtId);

				String ulbs = "";
				for (ULBNameModel ulbNameModel : ulbNameModels) {

					ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
				}

				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

				query = "select pic.phyInfra_con_id,pic.ulb_id,pic.date_of_report,pic.no_res_cons,pic.no_ind_cons,pic.no_com_cons,pic.no_inst_cons,pic.tot_no_cons,pic.user_id,pic.entry_date,pic.modify_date,un.ulb_Name,pic.next_maintenance_date from physical_infra_connection pic, ulbs_name un where pic.ulb_id = un.ulb_id  and pic.ulb_id in ("
						+ ulbs + ")  order by pic.entry_date desc";

			} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division
																						// User

				LinkedList upperLevels = districtService
						.fetchUpperLevelsByULBId(loginUsersModel
								.getUlbNameModel().getUlb_id());
				int divisionId = (Integer) upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService
						.fetchDistrictByDivisionId(divisionId);

				String ulbs = "";
				for (PhDistrictModel phDistrictModel : districtModels) {

					LinkedList<ULBNameModel> ulbNameModels = ulbNameService
							.fetchULBNameByDistrictId(phDistrictModel
									.getDist_id());

					for (ULBNameModel ulbNameModel : ulbNameModels) {

						ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
					}

				}

				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

				query = "select pic.phyInfra_con_id,pic.ulb_id,pic.date_of_report,pic.no_res_cons,pic.no_ind_cons,pic.no_com_cons,pic.no_inst_cons,pic.tot_no_cons,pic.user_id,pic.entry_date,pic.modify_date,un.ulb_Name,pic.next_maintenance_date from physical_infra_connection pic, ulbs_name un where pic.ulb_id = un.ulb_id  and pic.ulb_id in ("
						+ ulbs + ")  order by pic.entry_date desc";

			} else { // Others All (State, PHEO, Circle, H&UD)

				query = "select pic.phyInfra_con_id,pic.ulb_id,pic.date_of_report,pic.no_res_cons,pic.no_ind_cons,pic.no_com_cons,pic.no_inst_cons,pic.tot_no_cons,pic.user_id,pic.entry_date,pic.modify_date,un.ulb_Name,pic.next_maintenance_date from physical_infra_connection pic, ulbs_name un where pic.ulb_id = un.ulb_id  order by pic.entry_date desc";
			}
			
			//String query = "select pic.phyInfra_con_id,pic.ulb_id,pic.date_of_report,pic.no_res_cons,pic.no_ind_cons,pic.no_com_cons,pic.no_inst_cons,pic.tot_no_cons,pic.user_id,pic.entry_date,pic.modify_date,un.ulb_Name from physical_infra_connection pic, ulbs_name un where pic.ulb_id = un.ulb_id order by pic.entry_date desc";
			pst = connection.prepareStatement(query);
			//pst.setString(1, Util.getYesterdayDateString());
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraConnectionModel physicalInfraConnectionModel = new PhysicalInfraConnectionModel();
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(12));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(9));

				physicalInfraConnectionModel.setPhyInfra_con_id(rs.getInt(1));
				physicalInfraConnectionModel.setUlbNameModel(ulbNameModel);
				physicalInfraConnectionModel.setDate_of_report(rs.getString(3));
				physicalInfraConnectionModel.setNo_res_cons(rs.getDouble(4));
				physicalInfraConnectionModel.setNo_ind_cons(rs.getDouble(5));
				physicalInfraConnectionModel.setNo_com_cons(rs.getDouble(6));
				physicalInfraConnectionModel.setNo_inst_cons(rs.getDouble(7));
				physicalInfraConnectionModel.setTot_no_cons(rs.getDouble(8));
				physicalInfraConnectionModel.setLoginUsersModel(loginUsersModel);
				physicalInfraConnectionModel.setEntry_date(rs.getString(10));
				physicalInfraConnectionModel.setModify_date(rs.getString(11));
				physicalInfraConnectionModel.setDueDate(rs.getString(13));
				
				physicalInfraConnectionModels.add(physicalInfraConnectionModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraConnection");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraConnection"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraConnection -- END");
		}

		return physicalInfraConnectionModels;

	}

	public PhysicalInfraConnectionModel fetchPhysicalInfraConnectionById(int phyInfra_con_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraConnectionById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		PhysicalInfraConnectionModel physicalInfraConnectionModel = new PhysicalInfraConnectionModel();
		try {
			String query = "select pic.phyInfra_con_id,pic.ulb_id,pic.date_of_report,pic.no_res_cons,pic.no_ind_cons,pic.no_com_cons,pic.no_inst_cons,pic.tot_no_cons,pic.user_id,pic.entry_date,pic.modify_date,un.ulb_Name,pic.next_maintenance_date from physical_infra_connection pic,ulbs_name un where pic.ulb_id = un.ulb_id and pic.phyInfra_con_id =? order by pic.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, phyInfra_con_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(12));

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(9));

				physicalInfraConnectionModel.setPhyInfra_con_id(rs.getInt(1));
				physicalInfraConnectionModel.setUlbNameModel(ulbNameModel);
				physicalInfraConnectionModel.setDate_of_report(rs.getString(3));
				physicalInfraConnectionModel.setNo_res_cons(rs.getDouble(4));
				physicalInfraConnectionModel.setNo_ind_cons(rs.getDouble(5));
				physicalInfraConnectionModel.setNo_com_cons(rs.getDouble(6));
				physicalInfraConnectionModel.setNo_inst_cons(rs.getDouble(7));
				physicalInfraConnectionModel.setTot_no_cons(rs.getDouble(8));
				physicalInfraConnectionModel.setLoginUsersModel(loginUsersModel);
				physicalInfraConnectionModel.setEntry_date(rs.getString(10));
				physicalInfraConnectionModel.setModify_date(rs.getString(11));
				physicalInfraConnectionModel.setDueDate(rs.getString(13));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraConnectionById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraConnectionById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraConnectionById -- END");
		}

		return physicalInfraConnectionModel;

	}

	public int updatePhysicalInfraConnection(
			PhysicalInfraConnectionModel physicalInfraConnectionModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraConnection -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update physical_infra_connection set ulb_id=?,date_of_report=?,no_res_cons=?,no_ind_cons=?,no_inst_cons=?,tot_no_cons=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s'),no_com_cons=?,next_maintenance_date=? where phyInfra_con_id=? ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, physicalInfraConnectionModel.getUlbId());
			pst.setString(2,physicalInfraConnectionModel.getDate_of_report());
			pst.setDouble(3, physicalInfraConnectionModel.getNo_res_cons());
			pst.setDouble(4, physicalInfraConnectionModel.getNo_ind_cons());
			pst.setDouble(5, physicalInfraConnectionModel.getNo_inst_cons());
			pst.setDouble(6, physicalInfraConnectionModel.getTot_no_cons());
			pst.setInt(7, physicalInfraConnectionModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(8, Util.getCurrentDateTime());
			pst.setDouble(9, physicalInfraConnectionModel.getNo_com_cons());
			pst.setString(10,physicalInfraConnectionModel.getDueDate());
			pst.setInt(11, physicalInfraConnectionModel.getPhyInfra_con_id());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updatePhysicalInfraConnection");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updatePhysicalInfraConnection"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraConnection -- END");
		}

		return result;

	}
	public LinkedList<PhysicalInfraConnectionModel> fetchAJAXPhysicalInfraConnection(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraConnection -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraConnectionModel> physicalInfraConnectionModels = new LinkedList<PhysicalInfraConnectionModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select pic.phyInfra_con_id,pic.ulb_id,pic.date_of_report,pic.no_res_cons,pic.no_ind_cons,pic.no_com_cons,pic.no_inst_cons,pic.tot_no_cons,pic.user_id,pic.entry_date,pic.modify_date,un.ulb_Name,pic.next_maintenance_date from physical_infra_connection pic, ulbs_name un where pic.ulb_id = un.ulb_id  order by pic.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select pic.phyInfra_con_id,pic.ulb_id,pic.date_of_report,pic.no_res_cons,pic.no_ind_cons,pic.no_com_cons,pic.no_inst_cons,pic.tot_no_cons,pic.user_id,pic.entry_date,pic.modify_date,un.ulb_Name,pic.next_maintenance_date from physical_infra_connection pic, ulbs_name un where pic.ulb_id = un.ulb_id  and pic.ulb_id in ("
							+ ulbs + ") order by pic.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select pic.phyInfra_con_id,pic.ulb_id,pic.date_of_report,pic.no_res_cons,pic.no_ind_cons,pic.no_com_cons,pic.no_inst_cons,pic.tot_no_cons,pic.user_id,pic.entry_date,pic.modify_date,un.ulb_Name,pic.next_maintenance_date from physical_infra_connection pic, ulbs_name un where pic.ulb_id = un.ulb_id  and pic.ulb_id in ("
								+ ulbs + ") order by pic.entry_date desc";

					} else { // Only For ULB

						query = "select pic.phyInfra_con_id,pic.ulb_id,pic.date_of_report,pic.no_res_cons,pic.no_ind_cons,pic.no_com_cons,pic.no_inst_cons,pic.tot_no_cons,pic.user_id,pic.entry_date,pic.modify_date,un.ulb_Name,pic.next_maintenance_date from physical_infra_connection pic, ulbs_name un where pic.ulb_id = un.ulb_id  and pic.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by pic.entry_date desc";
					}

				}

			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraConnectionModel physicalInfraConnectionModel = new PhysicalInfraConnectionModel();
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(12));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(9));

				physicalInfraConnectionModel.setPhyInfra_con_id(rs.getInt(1));
				physicalInfraConnectionModel.setUlbNameModel(ulbNameModel);
				physicalInfraConnectionModel.setDate_of_report(rs.getString(3));
				physicalInfraConnectionModel.setNo_res_cons(rs.getDouble(4));
				physicalInfraConnectionModel.setNo_ind_cons(rs.getDouble(5));
				physicalInfraConnectionModel.setNo_com_cons(rs.getDouble(6));
				physicalInfraConnectionModel.setNo_inst_cons(rs.getDouble(7));
				physicalInfraConnectionModel.setTot_no_cons(rs.getDouble(8));
				physicalInfraConnectionModel.setLoginUsersModel(loginUsersModel2);
				physicalInfraConnectionModel.setEntry_date(rs.getString(10));
				physicalInfraConnectionModel.setModify_date(rs.getString(11));
				physicalInfraConnectionModel.setDueDate(rs.getString(13));

				physicalInfraConnectionModels.add(physicalInfraConnectionModel);
			}

			for (PhysicalInfraConnectionModel physicalInfraConnectionModel : physicalInfraConnectionModels) {

				System.out.println("physicalInfraConnectionModel.getPhyInfra_con_id()---- >>>"
						+ physicalInfraConnectionModel.getPhyInfra_con_id());
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXPhysicalInfraConnection");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXPhysicalInfraConnection"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraConnection -- END");
		}

		return physicalInfraConnectionModels;

	}

}
