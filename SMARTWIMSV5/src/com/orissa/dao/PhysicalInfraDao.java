package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;
import com.orissa.model.PhysicalInfraElectricalInstallationsModel;
import com.orissa.model.PhysicalInfraModel;
import com.orissa.model.PhysicalInfraProductionWellsModel;
import com.orissa.model.PhysicalInfraPumpsAndMotorsModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class PhysicalInfraDao {

	private static Logger logger = Logger.getLogger(PhysicalInfraDao.class);
	
	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbNameService;
	
	public LinkedList<PhysicalInfraModel> fetchPhysicalInfraReport() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraReport -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraModel> physicalInfraModels = new LinkedList<PhysicalInfraModel>();
		int i = 1;
		
		try {
			
			String query = "select u.ulb_name, "
					+ "round(sum(pis.no_exst_stanposts)/count(*),2), "
					+ "round(sum(pis.no_standposts_work_cond)/count(*),2), "
					+ "round(sum(pit.tot_no_tw)/count(*),2), "
					+ "round(sum(pit.tot_no_tw_wc)/count(*),2), "
					+ "round(sum(pisr.no_grnd_strg_res)/count(*),2), "
					+ "round(sum(pisr.no_ele_strg_res)/count(*),2), "
					+ "round(sum(pisr.cpty_grnd_strg_res)/count(*),2), "
					+ "round(sum(pisr.cpty_ele_strg_res)/count(*),2), "
					+ "round(sum(pilop.rising_main)/count(*),2), "
					+ "round(sum(pilop.distr_netw)/count(*),2), "
					+ "round(sum(piwtp.no_of_wtp)/count(*),2), "
					+ "round(sum(piwtp.cpty_wtp)/count(*),2), "
					+ "round(sum(pig.tot_no_gen)/count(*),2), "
					+ "round(sum(pig.tot_no_gen_wc)/count(*),2) "
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join physical_infra_standposts pis on pis.ulb_id = un.ulb_id "
					+ "left outer join physical_infra_tubewells pit on pit.ulb_id = un.ulb_id "
					+ "left outer join  physical_infra_storage_reservoirs pisr on pisr.ulb_id = un.ulb_id "
					+ "left outer join physical_infra_length_of_pipe pilop on pilop.ulb_id = un.ulb_id "
					+ "left outer join physical_infra_water_treatment_plant piwtp on piwtp.ulb_id = un.ulb_id "
					+ "left outer join  physical_infra_generetor pig on pig.ulb_id = un.ulb_id "
					+ "inner join amrut_type at on at.Amrut_type_id = un.Amrut_type_id "
					+ "where un.Amrut_type_id = (case 'null' when 'null' then un.Amrut_type_id else 'null' end) "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "order by u.ulb_name";
			
			pst = connection.prepareStatement(query);
			System.out.println("fetchPhysicalInfraReport ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				PhysicalInfraModel physicalInfraModel = new PhysicalInfraModel();
				
				physicalInfraModel.setSlNo(i++);
				physicalInfraModel.setUlbName(rs.getString(1));
				physicalInfraModel.setNoOfStandPosts(rs.getDouble(2));
				physicalInfraModel.setNoOfStandPostsInWorkingCondition(rs.getDouble(3));
				physicalInfraModel.setNoOfTubeWells(rs.getDouble(4));
				physicalInfraModel.setNoOfTubeWellsInWorkingCondition(rs.getDouble(5));
				physicalInfraModel.setNoOfGroundReservoirs(rs.getDouble(6));
				physicalInfraModel.setNoOfElavatedReservoirs(rs.getDouble(7));
				physicalInfraModel.setCapacityOfGroundReservoirs(rs.getDouble(8));
				physicalInfraModel.setCapacityOfElavatedReservoirs(rs.getDouble(9));
				physicalInfraModel.setLengthOfRisingMain(rs.getDouble(10));
				physicalInfraModel.setLengthOfDistributionNetwork(rs.getDouble(11));
				physicalInfraModel.setNoOfTreatmentPlant(rs.getDouble(12));
				physicalInfraModel.setCapacityOfTreatmentPlant(rs.getDouble(13));
				physicalInfraModel.setNoOfGenerators(rs.getDouble(14));
				physicalInfraModel.setNoOfGeneratorsInWorkingCondition(rs.getDouble(15));
				
				physicalInfraModels.add(physicalInfraModel);
			}
			
		} finally {
			
			try { // Closing Connection Object
				if (connection != null) {
		
					if (!connection.isClosed())
						connection.close();
		
					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraReport");
					}
				}
			} catch (Exception e) {
		
				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraReport"
							+ e.getMessage());
				}
		
			}
		
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraReport -- END");
		}
		
		return physicalInfraModels;
	
	}
	
	public LinkedList<PhysicalInfraElectricalInstallationsModel> fetchElectricInstallationReport() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchElectricInstallationReport -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraElectricalInstallationsModel> physInfraElectInstallModels = new LinkedList<PhysicalInfraElectricalInstallationsModel>();
		int i = 1;
		
		try {
			
			String query = "SELECT u.ulb_name, "
					+ "piei.loc_of_inst, "
					+ "piei.cap_of_trnf, "
					+ "piei.year_inst, "
					+ "piei.hp_lt "
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join physical_infra_electrical_installations piei on un.ulb_id = piei.ulb_id "
					+ "order by u.ulb_name";
			
			pst = connection.prepareStatement(query);
			System.out.println("fetchElectricInstallationReport ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				PhysicalInfraElectricalInstallationsModel physInfraElectInstallModel = new PhysicalInfraElectricalInstallationsModel();
				
				physInfraElectInstallModel.setSlNo(i++);
				physInfraElectInstallModel.setUlbName(rs.getString(1));
				physInfraElectInstallModel.setLoc_of_inst(rs.getString(2));
				physInfraElectInstallModel.setCap_of_trnf(rs.getString(3));
				physInfraElectInstallModel.setYear_inst(rs.getString(4));
				physInfraElectInstallModel.setHp_lt(rs.getString(5));
				
				physInfraElectInstallModels.add(physInfraElectInstallModel);
				
			}
			
		} finally {
			
			try { // Closing Connection Object
				if (connection != null) {
		
					if (!connection.isClosed())
						connection.close();
		
					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchElectricInstallationReport");
					}
				}
			} catch (Exception e) {
		
				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchElectricInstallationReport"
							+ e.getMessage());
				}
		
			}
		
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchElectricInstallationReport -- END");
		}
		
		return physInfraElectInstallModels;
	
	}
	
	public LinkedList<PhysicalInfraProductionWellsModel> fetchPhysicalInfraProdWellsReport() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraProdWellsReport -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraProductionWellsModel> physicalInfraProdWellsModels = new LinkedList<PhysicalInfraProductionWellsModel>();
		int i = 1;
		
		try {
			
			String query = "SELECT u.ulb_name, "
					+ "pipw.loc_of_prod_well, "
					+ "pipw.diam_prod_well, "
					+ "pipw.discharge, "
					+ "pipw.depth "
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join physical_infra_production_wells pipw on un.ulb_id = pipw.ulb_id "
					+ "order by u.ulb_name";
			
			pst = connection.prepareStatement(query);
			System.out.println("fetchPhysicalInfraProdWellsReport ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				PhysicalInfraProductionWellsModel physicalInfraProdWellsModel = new PhysicalInfraProductionWellsModel();
				
				physicalInfraProdWellsModel.setSlNo(i++);
				physicalInfraProdWellsModel.setUlbName(rs.getString(1));
				physicalInfraProdWellsModel.setLoc_of_prod_well(rs
						.getString(2));
				physicalInfraProdWellsModel.setDiam_prod_well(rs
						.getString(3));
				physicalInfraProdWellsModel.setDischarge(rs.getString(4));
				physicalInfraProdWellsModel.setDepth(rs.getString(5));
				
				physicalInfraProdWellsModels.add(physicalInfraProdWellsModel);
				
			}
			
		} finally {
			
			try { // Closing Connection Object
				if (connection != null) {
		
					if (!connection.isClosed())
						connection.close();
		
					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraProdWellsReport");
					}
				}
			} catch (Exception e) {
		
				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraProdWellsReport"
							+ e.getMessage());
				}
		
			}
		
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraProdWellsReport -- END");
		}
		
		return physicalInfraProdWellsModels;
	
	}
	
	public LinkedList<PhysicalInfraPumpsAndMotorsModel> fetchPumpAndMotorReport() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPumpAndMotorReport -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraPumpsAndMotorsModel> physicalInfraPumpsAndMotorsModelList = new LinkedList<PhysicalInfraPumpsAndMotorsModel>();
		int i = 1;
		
		try {
			
			String query = "SELECT "
					+ "u.ulb_name, "
					+ "pipam.loc_of_pum_motor, "
					+ "pipam.cap_of_motor, "
					+ "pipam.head_dia, "
					+ "pipam.discharge, "
					+ "pipam.type_pump, "
					+ "pipam.year_inst "
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join physical_infra_pumps_and_motors pipam on un.ulb_id = pipam.ulb_id "
					+ "order by u.ulb_name";
			
			pst = connection.prepareStatement(query);
			System.out.println("fetchPumpAndMotorReport ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel = new PhysicalInfraPumpsAndMotorsModel();
				
				physicalInfraPumpsAndMotorsModel.setSlNo(i++);
				physicalInfraPumpsAndMotorsModel.setUlbName(rs.getString(1));
				physicalInfraPumpsAndMotorsModel.setLoc_of_pum_motor(rs.getString(2));
				physicalInfraPumpsAndMotorsModel.setCap_of_motor(rs.getString(3));
				physicalInfraPumpsAndMotorsModel.setHead_dia(rs.getString(4));
				physicalInfraPumpsAndMotorsModel.setDischarge(rs.getString(5));
				physicalInfraPumpsAndMotorsModel.setType_pump(rs.getString(6));
				physicalInfraPumpsAndMotorsModel.setYear_inst(rs.getString(7));
				
				physicalInfraPumpsAndMotorsModelList.add(physicalInfraPumpsAndMotorsModel);
				
			}
			
		} finally {
			
			try { // Closing Connection Object
				if (connection != null) {
		
					if (!connection.isClosed())
						connection.close();
		
					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPumpAndMotorReport");
					}
				}
			} catch (Exception e) {
		
				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPumpAndMotorReport"
							+ e.getMessage());
				}
		
			}
		
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchPumpAndMotorReport -- END");
		}
		
		return physicalInfraPumpsAndMotorsModelList;
	
	}
}
