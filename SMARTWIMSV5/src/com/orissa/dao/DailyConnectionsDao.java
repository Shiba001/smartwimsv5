package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.DailyConnetionsModel;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class DailyConnectionsDao extends DeleteDao {

	private static Logger logger = Logger.getLogger(DailyConnectionsDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbNameService;

	public int addDailyConnetions(DailyConnetionsModel dailyConnetionsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyConnetions -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "insert into daily_connection(ulb_id,date_of_report,no_new_res_cons_add,no_res_cons_disc,no_new_ind_cons_add,no_ind_cons_disc,no_new_com_cons_add,no_com_cons_disc,no_new_inst_cons_add,no_inst_cons_disc,tot_no_new_cons,tot_no_cons_disc,remarks,user_id,entry_date,IsApproved) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?) ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, dailyConnetionsModel.getUlbId());
			pst.setString(2, dailyConnetionsModel.getDate_of_report());
			pst.setDouble(3, dailyConnetionsModel.getNo_new_res_cons_add());
			pst.setDouble(4, dailyConnetionsModel.getNo_res_cons_disc());
			pst.setDouble(5, dailyConnetionsModel.getNo_new_ind_cons_add());
			pst.setDouble(6, dailyConnetionsModel.getNo_ind_cons_disc());
			pst.setDouble(7, dailyConnetionsModel.getNo_new_com_cons_add());
			pst.setDouble(8, dailyConnetionsModel.getNo_com_cons_disc());
			pst.setDouble(9, dailyConnetionsModel.getNo_new_inst_cons_add());
			pst.setDouble(10, dailyConnetionsModel.getNo_inst_cons_disc());
			pst.setDouble(11, dailyConnetionsModel.getTot_no_new_cons());
			pst.setDouble(12, dailyConnetionsModel.getTot_no_cons_disc());
			pst.setString(13, dailyConnetionsModel.getRemarks());
			pst.setInt(14, dailyConnetionsModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(15, Util.getCurrentDateTime());
			pst.setInt(16, dailyConnetionsModel.getIsApproved());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addDailyConnetions");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addDailyConnetions"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyConnetions -- END");
		}

		return result;

	}

	public LinkedList<DailyConnetionsModel> fetchDailyConnetions(LoginUsersModel loginUsersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetions -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyConnetionsModel> dailyConnetionsModels = new LinkedList<DailyConnetionsModel>();
		try {
			
			String query = "";
			if(loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
				
				query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dc.ulb_id ="+loginUsersModel.getUlbNameModel().getUlb_id()+" order by dc.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int districtId = (Integer)upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(districtId);
				
				String ulbs = "";
				for(ULBNameModel ulbNameModel:ulbNameModels) {
					
					ulbs = ulbs +ulbNameModel.getUlb_id()+",";
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dc.ulb_id in ("+ulbs+")  order by dc.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int divisionId = (Integer)upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService.fetchDistrictByDivisionId(divisionId);
				
				String ulbs = "";
				for(PhDistrictModel phDistrictModel:districtModels) {
					
					LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(phDistrictModel.getDist_id());
					
					for(ULBNameModel ulbNameModel:ulbNameModels) {
						
						ulbs = ulbs +ulbNameModel.getUlb_id()+",";
					}
					
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dc.ulb_id in ("+ulbs+")  order by dc.entry_date desc";
			
			} else { // Others All (State, PHEO, Circle, H&UD)
				
				query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') order by dc.entry_date desc";
			}
			
			pst = connection.prepareStatement(query);
			pst.setString(1, Util.getYesterdayDateString());
			
			//String query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id order by dc.entry_date desc";
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyConnetionsModel dailyConnetionsModel = new DailyConnetionsModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(19));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(15));

				dailyConnetionsModel.setCon_id(rs.getInt(1));
				dailyConnetionsModel.setUlbNameModel(ulbNameModel);
				dailyConnetionsModel.setDate_of_report(rs.getString(3));
				dailyConnetionsModel.setNo_new_res_cons_add(rs.getDouble(4));
				dailyConnetionsModel.setNo_res_cons_disc(rs.getDouble(5));
				dailyConnetionsModel.setNo_new_ind_cons_add(rs.getDouble(6));
				dailyConnetionsModel.setNo_ind_cons_disc(rs.getDouble(7));
				dailyConnetionsModel.setNo_new_com_cons_add(rs.getDouble(8));
				dailyConnetionsModel.setNo_com_cons_disc(rs.getDouble(9));
				dailyConnetionsModel.setNo_new_inst_cons_add(rs.getDouble(10));
				dailyConnetionsModel.setNo_inst_cons_disc(rs.getDouble(11));
				dailyConnetionsModel.setTot_no_new_cons(rs.getDouble(12));
				dailyConnetionsModel.setTot_no_cons_disc(rs.getDouble(13));
				dailyConnetionsModel.setRemarks(rs.getString(14));
				dailyConnetionsModel.setLoginUsersModel(loginUsersModel2);
				dailyConnetionsModel.setEntry_date(rs.getString(16));
				dailyConnetionsModel.setModify_date(rs.getString(17));
				dailyConnetionsModel.setIsApproved(rs.getInt(18));

				dailyConnetionsModels.add(dailyConnetionsModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyConnetions");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyConnetions"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetions -- END");
		}

		return dailyConnetionsModels;

	}

	public DailyConnetionsModel fetchDailyConnetionsById(int con_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetionsById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		DailyConnetionsModel dailyConnetionsModel = new DailyConnetionsModel();
		try {
			String query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.con_id =? order by dc.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, con_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(19));

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(15));

				dailyConnetionsModel.setCon_id(rs.getInt(1));
				dailyConnetionsModel.setUlbNameModel(ulbNameModel);
				dailyConnetionsModel.setDate_of_report(rs.getString(3));
				dailyConnetionsModel.setNo_new_res_cons_add(rs.getDouble(4));
				dailyConnetionsModel.setNo_res_cons_disc(rs.getDouble(5));
				dailyConnetionsModel.setNo_new_ind_cons_add(rs.getDouble(6));
				dailyConnetionsModel.setNo_ind_cons_disc(rs.getDouble(7));
				dailyConnetionsModel.setNo_new_com_cons_add(rs.getDouble(8));
				dailyConnetionsModel.setNo_com_cons_disc(rs.getDouble(9));
				dailyConnetionsModel.setNo_new_inst_cons_add(rs.getDouble(10));
				dailyConnetionsModel.setNo_inst_cons_disc(rs.getDouble(11));
				dailyConnetionsModel.setTot_no_new_cons(rs.getDouble(12));
				dailyConnetionsModel.setTot_no_cons_disc(rs.getDouble(13));
				dailyConnetionsModel.setRemarks(rs.getString(14));
				dailyConnetionsModel.setLoginUsersModel(loginUsersModel);
				dailyConnetionsModel.setEntry_date(rs.getString(16));
				dailyConnetionsModel.setModify_date(rs.getString(17));
				dailyConnetionsModel.setIsApproved(rs.getInt(18));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyConnetionsById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyConnetionsById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetionsById -- END");
		}

		return dailyConnetionsModel;

	}

	public int updateDailyConnetions(DailyConnetionsModel dailyConnetionsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyConnetions -- START");
		}
		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update daily_connection set ulb_id=?,date_of_report=?,no_new_res_cons_add=?,no_res_cons_disc=?,no_new_ind_cons_add=?,no_ind_cons_disc=?,no_new_com_cons_add=?,no_com_cons_disc=?,no_new_inst_cons_add=?,no_inst_cons_disc=?,tot_no_new_cons=?,tot_no_cons_disc=?,remarks=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s') where con_id=? ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, dailyConnetionsModel.getUlbId());
			pst.setString(2, dailyConnetionsModel.getDate_of_report());
			pst.setDouble(3, dailyConnetionsModel.getNo_new_res_cons_add());
			pst.setDouble(4, dailyConnetionsModel.getNo_res_cons_disc());
			pst.setDouble(5, dailyConnetionsModel.getNo_new_ind_cons_add());
			pst.setDouble(6, dailyConnetionsModel.getNo_ind_cons_disc());
			pst.setDouble(7, dailyConnetionsModel.getNo_new_com_cons_add());
			pst.setDouble(8, dailyConnetionsModel.getNo_com_cons_disc());
			pst.setDouble(9, dailyConnetionsModel.getNo_new_inst_cons_add());
			pst.setDouble(10, dailyConnetionsModel.getNo_inst_cons_disc());
			pst.setDouble(11, dailyConnetionsModel.getTot_no_new_cons());
			pst.setDouble(12, dailyConnetionsModel.getTot_no_cons_disc());
			pst.setString(13, dailyConnetionsModel.getRemarks());
			pst.setInt(14, dailyConnetionsModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(15, Util.getCurrentDateTime());
			pst.setInt(16, dailyConnetionsModel.getCon_id());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updateDailyConnetions");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updateDailyConnetions"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyConnetions -- END");
		}

		return result;

	}
	
	public LinkedList<DailyConnetionsModel> fetchAJAXDailyConnetions(ListingModel listingModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyConnetions -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyConnetionsModel> dailyConnetionsModels = new LinkedList<DailyConnetionsModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =(case '"
						+ listingModel.getDate()
						+ "' when 'null' then dc.date_of_report else str_to_date('"
						+ listingModel.getDate()
						+ "','%Y-%m-%d %H:%i:%s') end) order by dc.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =(case '"
							+ listingModel.getDate()
							+ "' when 'null' then dc.date_of_report else str_to_date('"
							+ listingModel.getDate()
							+ "','%Y-%m-%d %H:%i:%s') end) and dc.ulb_id in ("
							+ ulbs + ") order by dc.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dc.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dc.ulb_id in ("
								+ ulbs + ") order by dc.entry_date desc";

					} else { // Only For ULB

						query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dc.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dc.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by dc.entry_date desc";
					}

				}

			}
			
			pst = connection.prepareStatement(query);
			//String query = "select dc.con_id,dc.ulb_id,dc.date_of_report,dc.no_new_res_cons_add,dc.no_res_cons_disc,dc.no_new_ind_cons_add,dc.no_ind_cons_disc,dc.no_new_com_cons_add,dc.no_com_cons_disc,dc.no_new_inst_cons_add,dc.no_inst_cons_disc,dc.tot_no_new_cons,dc.tot_no_cons_disc,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_connection dc,ulbs_name un where dc.ulb_id = un.ulb_id order by dc.entry_date desc";
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyConnetionsModel dailyConnetionsModel = new DailyConnetionsModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(19));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(15));

				dailyConnetionsModel.setCon_id(rs.getInt(1));
				dailyConnetionsModel.setUlbNameModel(ulbNameModel);
				dailyConnetionsModel.setDate_of_report(rs.getString(3));
				dailyConnetionsModel.setNo_new_res_cons_add(rs.getDouble(4));
				dailyConnetionsModel.setNo_res_cons_disc(rs.getDouble(5));
				dailyConnetionsModel.setNo_new_ind_cons_add(rs.getDouble(6));
				dailyConnetionsModel.setNo_ind_cons_disc(rs.getDouble(7));
				dailyConnetionsModel.setNo_new_com_cons_add(rs.getDouble(8));
				dailyConnetionsModel.setNo_com_cons_disc(rs.getDouble(9));
				dailyConnetionsModel.setNo_new_inst_cons_add(rs.getDouble(10));
				dailyConnetionsModel.setNo_inst_cons_disc(rs.getDouble(11));
				dailyConnetionsModel.setTot_no_new_cons(rs.getDouble(12));
				dailyConnetionsModel.setTot_no_cons_disc(rs.getDouble(13));
				dailyConnetionsModel.setRemarks(rs.getString(14));
				dailyConnetionsModel.setLoginUsersModel(loginUsersModel2);
				dailyConnetionsModel.setEntry_date(rs.getString(16));
				dailyConnetionsModel.setModify_date(rs.getString(17));
				dailyConnetionsModel.setIsApproved(rs.getInt(18));

				dailyConnetionsModels.add(dailyConnetionsModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXDailyConnetions");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXDailyConnetions"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyConnetions -- END");
		}

		return dailyConnetionsModels;

	}
}
