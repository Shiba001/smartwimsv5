package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraPumpsAndMotorsModel;
import com.orissa.model.ULBNameModel;

@Repository
public class PhysicalInfraPumpMotorDao {

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	private static Logger logger = Logger
			.getLogger(PhysicalInfraPumpMotorDao.class);
	
	public int addPumpAndMotor(PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPumpAndMotor -- START");
		}
		
		int result = 0;
		Connection connection=null;
		
		try {
			
			connection = dbConnection.Connect();
			PreparedStatement pst = connection.prepareStatement("insert into physical_infra_pumps_and_motors(phyInfra_pump_motor_slno,"
					+ "date_of_report,"
					+ "ulb_id,"
					+ "loc_of_pum_motor,"
					+ "cap_of_motor,"
					+ "head_dia,"
					+ "discharge,"
					+ "type_pump,"
					+ "year_inst,"
					+ "user_id,"
					+ "entry_date,next_maintenance_date) values(?,?,?,?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?)");  
			
			pst.setInt(1,physicalInfraPumpsAndMotorsModel.getPhyInfra_pump_motor_slno());
			pst.setString(2,physicalInfraPumpsAndMotorsModel.getDate_of_report()); 
			pst.setInt(3,physicalInfraPumpsAndMotorsModel.getUlbId()); 
			pst.setString(4,physicalInfraPumpsAndMotorsModel.getLoc_of_pum_motor()); 
			pst.setString(5,physicalInfraPumpsAndMotorsModel.getCap_of_motor()); 
			pst.setString(6,physicalInfraPumpsAndMotorsModel.getHead_dia()); 
			pst.setString(7,physicalInfraPumpsAndMotorsModel.getDischarge());
			pst.setString(8,physicalInfraPumpsAndMotorsModel.getType_pump());
			pst.setString(9,physicalInfraPumpsAndMotorsModel.getYear_inst());
			pst.setInt(10,physicalInfraPumpsAndMotorsModel.getLoginUsersModel().getUser_id()); 
			pst.setString(11,Util.getCurrentDateTime());
			pst.setString(12,physicalInfraPumpsAndMotorsModel.getDueDate()); 
			
			result = pst.executeUpdate();
			
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addPumpAndMotor");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addPumpAndMotor"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addPumpAndMotor -- END");
		}

		return result;

	}
	
	public LinkedList<PhysicalInfraPumpsAndMotorsModel> fetchPumpAndMotor(int UlbId, String date_of_report)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPumpAndMotor -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraPumpsAndMotorsModel> physicalInfraPumpsAndMotorsModelList = new LinkedList<PhysicalInfraPumpsAndMotorsModel>();
		try {
			String query = "select pipm.phyInfra_pump_motor_slno,pipm.date_of_report,pipm.ulb_id,pipm.loc_of_pum_motor,pipm.cap_of_motor,pipm.head_dia,pipm.discharge,pipm.type_pump,pipm.year_inst,pipm.user_id,pipm.entry_date,pipm.modify_date,un.ulb_Name,pipm.next_maintenance_date from physical_infra_pumps_and_motors pipm,ulbs_name un where pipm.ulb_id = un.ulb_id and pipm.ulb_id=? order by pipm.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, UlbId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel = new PhysicalInfraPumpsAndMotorsModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(13));
				physicalInfraPumpsAndMotorsModel.setUlbNameModel(ulbNameModel);

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(10));
				physicalInfraPumpsAndMotorsModel.setLoginUsersModel(loginUsersModel);
				
				physicalInfraPumpsAndMotorsModel.setPhyInfra_pump_motor_slno(rs.getInt(1));
				physicalInfraPumpsAndMotorsModel.setDate_of_report(rs.getString(2));
				physicalInfraPumpsAndMotorsModel.setLoc_of_pum_motor(rs.getString(4));
				physicalInfraPumpsAndMotorsModel.setCap_of_motor(rs.getString(5));
				physicalInfraPumpsAndMotorsModel.setHead_dia(rs.getString(6));
				physicalInfraPumpsAndMotorsModel.setDischarge(rs.getString(7));
				physicalInfraPumpsAndMotorsModel.setType_pump(rs.getString(8));
				physicalInfraPumpsAndMotorsModel.setYear_inst(rs.getString(9));
				physicalInfraPumpsAndMotorsModel.setEntry_date(rs.getString(11));
				physicalInfraPumpsAndMotorsModel.setModify_date(rs.getString(12));
				physicalInfraPumpsAndMotorsModel.setDueDate(rs.getString(14));
				
				physicalInfraPumpsAndMotorsModel.setUlbNameModel(ulbNameModel);
				
				physicalInfraPumpsAndMotorsModelList.add(physicalInfraPumpsAndMotorsModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPumpAndMotor");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPumpAndMotor"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPumpAndMotor -- END");
		}

		return physicalInfraPumpsAndMotorsModelList;

	}
	
	public int deletePumpAndMotor(PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deletePumpAndMotor -- START");
		}
		
		int result = 0;
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		try {
			
			connection = dbConnection.Connect();
			String query = "delete from physical_infra_pumps_and_motors where ulb_id = ?";
			pst = connection.prepareStatement(query);
			
			pst.setInt(1,physicalInfraPumpsAndMotorsModel.getUlbId());
			/*pst.setString(2,physicalInfraPumpsAndMotorsModel.getDate_of_report()); */
			result = pst.executeUpdate();
			
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for deletePumpAndMotor");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for deletePumpAndMotor"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("deletePumpAndMotor -- END");
		}

		return result;

	}
}
