package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.ULBModel;

@Repository
public class ULBDao {

	private static Logger logger = Logger.getLogger(ULBDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	public LinkedList<ULBModel> fetchULBByDivisionIdAndAmrut(int divisionId, int amrutTypeId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDivisionIdAndAmrut -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBModel> ulbModels = new LinkedList<ULBModel>();
		try {
			String query = "select u.m_ulb_id,u.ulb_name,u.latitude,u.longitude,pd.Dist_id,pd.Dist_Name,phd.div_id,phd.div_name from ulbs u,ph_district pd,ph_division phd where u.Dist_id = pd.Dist_id and pd.div_id = phd.div_id and phd.div_id=? and u.Amrut_type_id=? order by u.ulb_name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, divisionId);
			pst.setInt(2, amrutTypeId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBModel ulbModel = new ULBModel();

				ulbModel.setM_ulb_id(rs.getInt(1));
				ulbModel.setUlb_name(rs.getString(2));
				ulbModel.setLatitude(rs.getString(3));
				ulbModel.setLongitude(rs.getString(4));
				
				PhDistrictModel phDistrictModel = new PhDistrictModel();
				
				phDistrictModel.setDist_id(rs.getInt(5));
				phDistrictModel.setDist_Name(rs.getString(6));
				
				PhDivisionModel phDivisionModel = new PhDivisionModel();
				
				phDivisionModel.setDiv_id(rs.getInt(7));
				phDivisionModel.setDiv_name(rs.getString(8));
				
				phDistrictModel.setPhDivisionModel(phDivisionModel);
				ulbModel.setPhDistrictModel(phDistrictModel);

				ulbModels.add(ulbModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBByDivisionIdAndAmrut");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBByDivisionIdAndAmrut"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDivisionIdAndAmrut -- END");
		}

		return ulbModels;

	}
	
	public LinkedList<ULBModel> fetchULBByDivisionId(int divisionId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDivisionId -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBModel> ulbModels = new LinkedList<ULBModel>();
		try {
			String query = "select u.m_ulb_id,u.ulb_name,u.latitude,u.longitude,pd.Dist_id,pd.Dist_Name,phd.div_id,phd.div_name from ulbs u,ph_district pd,ph_division phd where u.Dist_id = pd.Dist_id and pd.div_id = phd.div_id and phd.div_id=? order by u.ulb_name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, divisionId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBModel ulbModel = new ULBModel();

				ulbModel.setM_ulb_id(rs.getInt(1));
				ulbModel.setUlb_name(rs.getString(2));
				ulbModel.setLatitude(rs.getString(3));
				ulbModel.setLongitude(rs.getString(4));
				
				PhDistrictModel phDistrictModel = new PhDistrictModel();
				
				phDistrictModel.setDist_id(rs.getInt(5));
				phDistrictModel.setDist_Name(rs.getString(6));
				
				PhDivisionModel phDivisionModel = new PhDivisionModel();
				
				phDivisionModel.setDiv_id(rs.getInt(7));
				phDivisionModel.setDiv_name(rs.getString(8));
				
				phDistrictModel.setPhDivisionModel(phDivisionModel);
				ulbModel.setPhDistrictModel(phDistrictModel);

				ulbModels.add(ulbModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBByDivisionId");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBByDivisionId"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDivisionId -- END");
		}

		return ulbModels;

	}
	
	
	public ULBModel fetchULBByULBId(int ulbid)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByULBId -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		ULBModel ulbModel = new ULBModel();
		try {
			String query = "select u.m_ulb_id,u.ulb_name,u.latitude,u.longitude,pd.Dist_id,pd.Dist_Name,phd.div_id,phd.div_name from ulbs u,ulbs_name un,ph_district pd,ph_division phd where u.Dist_id = pd.Dist_id and pd.div_id = phd.div_id and un.m_ulb_id=u.m_ulb_id and un.ulb_id=? order by u.ulb_name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, ulbid);
			ResultSet rs = pst.executeQuery();
			System.out.println("fetchULBByULBId ---- >> "+pst);
			while (rs.next()) {


				ulbModel.setM_ulb_id(rs.getInt(1));
				ulbModel.setUlb_name(rs.getString(2));
				ulbModel.setLatitude(rs.getString(3));
				ulbModel.setLongitude(rs.getString(4));
				
				PhDistrictModel phDistrictModel = new PhDistrictModel();
				
				phDistrictModel.setDist_id(rs.getInt(5));
				phDistrictModel.setDist_Name(rs.getString(6));
				
				PhDivisionModel phDivisionModel = new PhDivisionModel();
				
				phDivisionModel.setDiv_id(rs.getInt(7));
				phDivisionModel.setDiv_name(rs.getString(8));
				
				phDistrictModel.setPhDivisionModel(phDivisionModel);
				ulbModel.setPhDistrictModel(phDistrictModel);

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBByULBId");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBByULBId"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByULBId -- END");
		}

		return ulbModel;

	}

	public LinkedList<ULBModel> fetchULBByDistrictId(int districtId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDistrictId -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBModel> ulbModels = new LinkedList<ULBModel>();
		try {
			String query = "select u.m_ulb_id,u.ulb_name,u.latitude,u.longitude,pd.Dist_id,pd.Dist_Name,phd.div_id,phd.div_name from ulbs u,ph_district pd,ph_division phd where u.Dist_id = pd.Dist_id and pd.div_id = phd.div_id and pd.Dist_id=? order by u.ulb_name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, districtId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBModel ulbModel = new ULBModel();

				ulbModel.setM_ulb_id(rs.getInt(1));
				ulbModel.setUlb_name(rs.getString(2));
				ulbModel.setLatitude(rs.getString(3));
				ulbModel.setLongitude(rs.getString(4));
				
				PhDistrictModel phDistrictModel = new PhDistrictModel();
				
				phDistrictModel.setDist_id(rs.getInt(5));
				phDistrictModel.setDist_Name(rs.getString(6));
				
				PhDivisionModel phDivisionModel = new PhDivisionModel();
				
				phDivisionModel.setDiv_id(rs.getInt(7));
				phDivisionModel.setDiv_name(rs.getString(8));
				
				phDistrictModel.setPhDivisionModel(phDivisionModel);
				ulbModel.setPhDistrictModel(phDistrictModel);

				ulbModels.add(ulbModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBByDistrictId");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBByDistrictId"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDistrictId -- END");
		}

		return ulbModels;

	}
	
	public LinkedList<ULBModel> fetchULBByDistrictIdAndAmrut(int districtId, int amrutTypeId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDistrictIdAndAmrut -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBModel> ulbModels = new LinkedList<ULBModel>();
		try {
			String query = "select u.m_ulb_id,u.ulb_name,u.latitude,u.longitude,pd.Dist_id,pd.Dist_Name,phd.div_id,phd.div_name from ulbs u,ph_district pd,ph_division phd where u.Dist_id = pd.Dist_id and pd.div_id = phd.div_id and pd.Dist_id=? and u.Amrut_type_id=? order by u.ulb_name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, districtId);
			pst.setInt(2, amrutTypeId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBModel ulbModel = new ULBModel();

				ulbModel.setM_ulb_id(rs.getInt(1));
				ulbModel.setUlb_name(rs.getString(2));
				ulbModel.setLatitude(rs.getString(3));
				ulbModel.setLongitude(rs.getString(4));
				
				PhDistrictModel phDistrictModel = new PhDistrictModel();
				
				phDistrictModel.setDist_id(rs.getInt(5));
				phDistrictModel.setDist_Name(rs.getString(6));
				
				PhDivisionModel phDivisionModel = new PhDivisionModel();
				
				phDivisionModel.setDiv_id(rs.getInt(7));
				phDivisionModel.setDiv_name(rs.getString(8));
				
				phDistrictModel.setPhDivisionModel(phDivisionModel);
				ulbModel.setPhDistrictModel(phDistrictModel);

				ulbModels.add(ulbModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBByDistrictIdAndAmrut");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBByDistrictIdAndAmrut"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDistrictIdAndAmrut -- END");
		}

		return ulbModels;

	}
	
	public LinkedList<ULBModel> fetchAllULB()
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAllULB -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBModel> ulbModels = new LinkedList<ULBModel>();
		try {
			String query = "select u.m_ulb_id,u.ulb_name,u.latitude,u.longitude,pd.Dist_id,pd.Dist_Name,phd.div_id,phd.div_name from ulbs u,ph_district pd,ph_division phd where u.Dist_id = pd.Dist_id and pd.div_id = phd.div_id order by u.ulb_name";
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBModel ulbModel = new ULBModel();

				ulbModel.setM_ulb_id(rs.getInt(1));
				ulbModel.setUlb_name(rs.getString(2));
				ulbModel.setLatitude(rs.getString(3));
				ulbModel.setLongitude(rs.getString(4));
				
				PhDistrictModel phDistrictModel = new PhDistrictModel();
				
				phDistrictModel.setDist_id(rs.getInt(5));
				phDistrictModel.setDist_Name(rs.getString(6));
				
				PhDivisionModel phDivisionModel = new PhDivisionModel();
				
				phDivisionModel.setDiv_id(rs.getInt(7));
				phDivisionModel.setDiv_name(rs.getString(8));
				
				phDistrictModel.setPhDivisionModel(phDivisionModel);
				ulbModel.setPhDistrictModel(phDistrictModel);

				ulbModels.add(ulbModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAllULB");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAllULB"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAllULB -- END");
		}

		return ulbModels;

	}
	
	public LinkedList<ULBModel> fetchULBByAmrutType(int amrutTypeId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByAmrutType -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<ULBModel> ulbModels = new LinkedList<ULBModel>();
		try {
			String query = "select u.m_ulb_id,u.ulb_name,u.latitude,u.longitude,pd.Dist_id,pd.Dist_Name,phd.div_id,phd.div_name from ulbs u,ph_district pd,ph_division phd where u.Dist_id = pd.Dist_id and pd.div_id = phd.div_id and u.Amrut_type_id=? order by u.ulb_name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, amrutTypeId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				ULBModel ulbModel = new ULBModel();

				ulbModel.setM_ulb_id(rs.getInt(1));
				ulbModel.setUlb_name(rs.getString(2));
				ulbModel.setLatitude(rs.getString(3));
				ulbModel.setLongitude(rs.getString(4));
				
				PhDistrictModel phDistrictModel = new PhDistrictModel();
				
				phDistrictModel.setDist_id(rs.getInt(5));
				phDistrictModel.setDist_Name(rs.getString(6));
				
				PhDivisionModel phDivisionModel = new PhDivisionModel();
				
				phDivisionModel.setDiv_id(rs.getInt(7));
				phDivisionModel.setDiv_name(rs.getString(8));
				
				phDistrictModel.setPhDivisionModel(phDivisionModel);
				ulbModel.setPhDistrictModel(phDistrictModel);

				ulbModels.add(ulbModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchULBByAmrutType");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchULBByAmrutType"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByAmrutType -- END");
		}

		return ulbModels;

	}
}
