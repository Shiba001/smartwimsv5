package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.PhDivisionModel;
import com.orissa.model.ULBModel;
import com.orissa.model.ULBNameModel;
import com.orissa.model.UserLevelModel;
import com.orissa.model.UserRoleModel;

@Repository
public class LoginDao {

	private static Logger logger = Logger.getLogger(LoginDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	public LoginUsersModel loginCheck(LoginUsersModel loginUsersModel) throws Exception {
		
		if (logger.isInfoEnabled()) {
			logger.info("loginCheck -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LoginUsersModel loginUsersModel2 = new LoginUsersModel();
		try {
			String query = "select lu.*,un.*,at.*,tou.*,pd.*,phd.*,u.*,at1.*,tou1.*,pd1.*,phd1.*,pm.*,ur.*,ul.* from  login_users lu left outer join ulbs_name un on lu.ulb_id  = un.ulb_id left outer join  amrut_type at on un.Amrut_type_id = at.Amrut_type_id left outer join  type_of_ulb tou on tou.type_ulb_id = un.type_ulb_id left outer join  ph_district pd on pd.Dist_id = un.Dist_id left outer join ph_division phd on phd.div_id = pd.div_id left outer join ulbs u on u.m_ulb_id = un.m_ulb_id left outer join amrut_type at1 on at1.Amrut_type_id = u.Amrut_type_id left outer join type_of_ulb tou1 on tou1.type_ulb_id = u.type_ulb_id left outer join ph_district pd1 on pd1.Dist_id = u.Dist_id left outer join ph_division phd1 on phd1.div_id = pd1.div_id left outer join project_modules pm on pm.projt_mod_id = lu.projt_mod_id left outer join user_role ur on ur.role_id = lu.role_id left outer join  user_level ul  on ul.user_level_id = lu.user_level_id where lu.User_Name=? and lu.User_Password=? and lu.IsActive=1";
			pst = connection.prepareStatement(query);
			pst.setString(1, loginUsersModel.getUser_Name());
			pst.setString(2, loginUsersModel.getUser_Password());
			ResultSet rs = pst.executeQuery();
			System.out.println("loginCheck ---- >>>"+pst);
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				UserRoleModel userRoleModel = new UserRoleModel();
				UserLevelModel userLevelModel = new UserLevelModel();
				PhDistrictModel phDistrictModel = new PhDistrictModel();
				PhDivisionModel phDivisionModel = new PhDivisionModel();
				ULBModel ulbModel = new ULBModel();
				
				ulbModel.setM_ulb_id(rs.getInt(17));
				ulbModel.setUlb_name(rs.getString(18));
				ulbNameModel.setUlbModel(ulbModel);
				loginUsersModel2.setUser_id(rs.getInt(1));
				loginUsersModel2.setUser_Name(rs.getString(2));
				loginUsersModel2.setFull_Name(rs.getString(3));
				ulbNameModel.setUlb_id(rs.getInt(5));
				ulbNameModel.setUlb_Name(rs.getString(18));
				userRoleModel.setRole_id(rs.getInt(7));
				userRoleModel.setRole_name(rs.getString(51));
				userLevelModel.setUser_level_id(rs.getInt(8));
				userLevelModel.setUser_level_name(rs.getString(53));
				phDistrictModel.setDist_id(rs.getInt(16));
				phDistrictModel.setDist_Name(rs.getString(45));
				phDivisionModel.setDiv_id(rs.getInt(27));
				phDivisionModel.setDiv_name(rs.getString(47));
				loginUsersModel2.setPhDistrictModel(phDistrictModel);
				loginUsersModel2.setPhDivisionModel(phDivisionModel);
				loginUsersModel2.setUlbNameModel(ulbNameModel);
				loginUsersModel2.setUserLevelModel(userLevelModel);
				loginUsersModel2.setUserRoleModel(userRoleModel);

			}
			
			if(loginUsersModel.getUser_Name().equals(loginUsersModel2.getUser_Name())) {
				System.out.println("NO SQL INJECTION");
				if (logger.isDebugEnabled()) {
					logger.debug("NO SQL INJECTION");
				}
			} else {
				System.out.println("SQL INJECTION");
				if (logger.isDebugEnabled()) {
					logger.debug("SQL INJECTION");
				}
				loginUsersModel2.setUser_id(0);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for loginCheck");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for loginCheck"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("loginCheck -- END");
		}

		return loginUsersModel2;

	}
}
