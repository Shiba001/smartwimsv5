package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.PhysicalInfraGeneretorModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class PhysicalInfraGeneratorDao extends DeleteDao{
	private static Logger logger = Logger
			.getLogger(PhysicalInfraGeneratorDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private ULBNameService ulbNameService;

	public int addPhysicalInfraGenerator(
			PhysicalInfraGeneretorModel physicalInfraGeneretorModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraGenerator -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		int t = 0;

		try {
			String query = "insert into physical_infra_generetor(ulb_id,date_of_report,tot_no_gen,tot_no_gen_wc,tot_no_gen_ur,pro_gen_wc,user_id,entry_date,next_maintenance_date) values(?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?) ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, physicalInfraGeneretorModel.getUlbId());
			pst.setString(2, physicalInfraGeneretorModel.getDate_of_report()); 
			pst.setDouble(3, physicalInfraGeneretorModel.getTot_no_gen());
			pst.setDouble(4, physicalInfraGeneretorModel.getTot_no_gen_wc());
			pst.setDouble(5, physicalInfraGeneretorModel.getTot_no_gen_ur());
			pst.setDouble(6, physicalInfraGeneretorModel.getPro_gen_wc());
			pst.setInt(7, physicalInfraGeneretorModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(8, Util.getCurrentDateTime());
			pst.setString(9,
					physicalInfraGeneretorModel.getDueDate());

			t = pst.executeUpdate();
			if (t > 0)
				t = 1;
			else
				t = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addPhysicalInfraGenerator");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addPhysicalInfraGenerator"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraGenerator -- END");
		}

		return t;

	}

	public LinkedList<PhysicalInfraGeneretorModel> fetchPhysicalInfraGenerator(LoginUsersModel loginUsersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraGenerator -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraGeneretorModel> physicalInfraGeneretorModels = new LinkedList<PhysicalInfraGeneretorModel>();
		
	try {
			
			String query = "";
			if(loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User		
			query = "select pig.phyInfra_gen_id, pig.ulb_id,pig.date_of_report,pig.tot_no_gen,pig.tot_no_gen_wc,pig.tot_no_gen_ur,pig.pro_gen_wc,pig.user_id,pig.entry_date,pig.modify_date,un.ulb_Name,pig.next_maintenance_date from physical_infra_generetor pig,ulbs_name un where pig.ulb_id = un.ulb_id  and pig.ulb_id ="+loginUsersModel.getUlbNameModel().getUlb_id()+" order by pig.entry_date desc";	
			}
			 else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District User
					
					LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
					int districtId = (Integer)upperLevels.get(2);
					LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(districtId);
					
					String ulbs = "";
					for(ULBNameModel ulbNameModel:ulbNameModels) {
						
						ulbs = ulbs +ulbNameModel.getUlb_id()+",";
					}
					
					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
					
					query = "select pig.phyInfra_gen_id, pig.ulb_id,pig.date_of_report,pig.tot_no_gen,pig.tot_no_gen_wc,pig.tot_no_gen_ur,pig.pro_gen_wc,pig.user_id,pig.entry_date,pig.modify_date,un.ulb_Name,pig.next_maintenance_date from physical_infra_generetor pig,ulbs_name un where pig.ulb_id = un.ulb_id  and pig.ulb_id in ("+ulbs+")  order by pig.entry_date desc";
				
				} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division User
					
					LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
					int divisionId = (Integer)upperLevels.get(3);
					LinkedList<PhDistrictModel> districtModels = districtService.fetchDistrictByDivisionId(divisionId);
					
					String ulbs = "";
					for(PhDistrictModel phDistrictModel:districtModels) {
						
						LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(phDistrictModel.getDist_id());
						
						for(ULBNameModel ulbNameModel:ulbNameModels) {
							
							ulbs = ulbs +ulbNameModel.getUlb_id()+",";
						}
						
					}
					
					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
					
					query = "select pig.phyInfra_gen_id, pig.ulb_id,pig.date_of_report,pig.tot_no_gen,pig.tot_no_gen_wc,pig.tot_no_gen_ur,pig.pro_gen_wc,pig.user_id,pig.entry_date,pig.modify_date,un.ulb_Name,pig.next_maintenance_date from physical_infra_generetor pig,ulbs_name un where pig.ulb_id = un.ulb_id  and pig.ulb_id in ("+ulbs+")  order by pig.entry_date desc";
				
				} 
				else { // Others All (State, PHEO, Circle, H&UD)
					
					query = "select pig.phyInfra_gen_id, pig.ulb_id,pig.date_of_report,pig.tot_no_gen,pig.tot_no_gen_wc,pig.tot_no_gen_ur,pig.pro_gen_wc,pig.user_id,pig.entry_date,pig.modify_date,un.ulb_Name,pig.next_maintenance_date from physical_infra_generetor pig,ulbs_name un where pig.ulb_id = un.ulb_id  order by pig.entry_date desc";
				}
				
				pst = connection.prepareStatement(query);
				//pst.setString(1, Util.getYesterdayDateString());
				ResultSet rs = pst.executeQuery();
				while (rs.next()) {

				PhysicalInfraGeneretorModel physicalInfraGeneretorModel = new PhysicalInfraGeneretorModel();
				physicalInfraGeneretorModel.setPhyInfra_gen_id(rs.getInt(1));
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_Name(rs.getString(11));
				ulbNameModel.setUlb_id(rs.getInt(2));
				physicalInfraGeneretorModel.setUlbNameModel(ulbNameModel);
				physicalInfraGeneretorModel.setDate_of_report(rs
						.getString(3));

				physicalInfraGeneretorModel.setTot_no_gen(rs.getDouble(4));
				physicalInfraGeneretorModel.setTot_no_gen_wc(rs.getDouble(5));
				physicalInfraGeneretorModel.setTot_no_gen_ur(rs.getDouble(6));
				physicalInfraGeneretorModel.setPro_gen_wc(rs.getDouble(7));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(8)); // To be Changed
				physicalInfraGeneretorModel
						.setLoginUsersModel(loginUsersModel2);

				physicalInfraGeneretorModel.setEntry_date(rs.getString(9));
				physicalInfraGeneretorModel.setModify_date(rs.getString(10));
				physicalInfraGeneretorModel.setDueDate(rs.getString(12));
				physicalInfraGeneretorModels.add(physicalInfraGeneretorModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraGenerator");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraGenerator"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraGenerator -- END");
		}

		return physicalInfraGeneretorModels;

	}

	public PhysicalInfraGeneretorModel fetchPhysicalInfraGeneratorById(
			int phyInfra_lpid) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraGeneratorById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		PhysicalInfraGeneretorModel physicalInfraGeneretorModel = new PhysicalInfraGeneretorModel();
		try {
			String query = "select pig.phyInfra_gen_id,"
					+ "pig.ulb_id,"
					+ "pig.date_of_report,"
					+ "pig.tot_no_gen,"
					+ "pig.tot_no_gen_wc,"
					+ "pig.tot_no_gen_ur,"
					+ "pig.pro_gen_wc,"
					+ "pig.user_id,"
					+ "pig.entry_date,"
					+ "pig.modify_date,"
					+ "un.ulb_Name,pig.next_maintenance_date"
					+ " from physical_infra_generetor pig,ulbs_name un where pig.ulb_id = un.ulb_id and pig.phyInfra_gen_id =? order by entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, phyInfra_lpid);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				physicalInfraGeneretorModel.setPhyInfra_gen_id(rs.getInt(1));
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_Name(rs.getString(11));
				ulbNameModel.setUlb_id(rs.getInt(2));
				physicalInfraGeneretorModel.setUlbNameModel(ulbNameModel);
				physicalInfraGeneretorModel.setDate_of_report(rs
						.getString(3));

				physicalInfraGeneretorModel.setTot_no_gen(rs.getDouble(4));
				physicalInfraGeneretorModel.setTot_no_gen_wc(rs.getDouble(5));
				physicalInfraGeneretorModel.setTot_no_gen_ur(rs.getDouble(6));
				physicalInfraGeneretorModel.setPro_gen_wc(rs.getDouble(7));

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(8)); // To be Changed
				physicalInfraGeneretorModel
						.setLoginUsersModel(loginUsersModel);

				physicalInfraGeneretorModel.setEntry_date(rs.getString(9));
				physicalInfraGeneretorModel.setModify_date(rs.getString(10));
				physicalInfraGeneretorModel.setDueDate(rs.getString(12));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraGeneratorById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraGeneratorById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraGeneratorById -- END");
		}

		return physicalInfraGeneretorModel;

	}

	public int updatePhysicalInfraGenerator(
			PhysicalInfraGeneretorModel physicalInfraGeneretorModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraGenerator -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update physical_infra_generetor set ulb_id=?,date_of_report=?,tot_no_gen=?,tot_no_gen_wc=?,tot_no_gen_ur=?,pro_gen_wc=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s'),next_maintenance_date=? where phyInfra_gen_id=? ";
			pst = connection.prepareStatement(query);

			pst.setInt(1, physicalInfraGeneretorModel.getUlbId());
			pst.setString(2, physicalInfraGeneretorModel.getDate_of_report()); // To be Changed
			
			pst.setDouble(3, physicalInfraGeneretorModel.getTot_no_gen());
			pst.setDouble(4, physicalInfraGeneretorModel.getTot_no_gen_wc());
			pst.setDouble(5, physicalInfraGeneretorModel.getTot_no_gen_ur());
			pst.setDouble(6, physicalInfraGeneretorModel.getPro_gen_wc());
			pst.setInt(7, physicalInfraGeneretorModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(8, Util.getCurrentDateTime());
			pst.setString(9,physicalInfraGeneretorModel.getDueDate());
			pst.setInt(10, physicalInfraGeneretorModel.getPhyInfra_gen_id());
			
			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updatePhysicalInfraGenerator");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updatePhysicalInfraGenerator"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraGenerator -- END");
		}

		return result;

	}
	
	
	public LinkedList<PhysicalInfraGeneretorModel> fetchAJAXPhysicalInfraGen(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraGeneretorModel> physicalInfraGeneretorModels = new LinkedList<PhysicalInfraGeneretorModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select pig.phyInfra_gen_id, pig.ulb_id,pig.date_of_report,pig.tot_no_gen,pig.tot_no_gen_wc,pig.tot_no_gen_ur,pig.pro_gen_wc,pig.user_id,pig.entry_date,pig.modify_date,un.ulb_Name,pig.next_maintenance_date from physical_infra_generetor pig,ulbs_name un  where pig.ulb_id = un.ulb_id  order by pig.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select pig.phyInfra_gen_id, pig.ulb_id,pig.date_of_report,pig.tot_no_gen,pig.tot_no_gen_wc,pig.tot_no_gen_ur,pig.pro_gen_wc,pig.user_id,pig.entry_date,pig.modify_date,un.ulb_Name,pig.next_maintenance_date from physical_infra_generetor pig,ulbs_name un where pig.ulb_id = un.ulb_id  and pig.ulb_id in ("
							+ ulbs + ") order by pig.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select pig.phyInfra_gen_id, pig.ulb_id,pig.date_of_report,pig.tot_no_gen,pig.tot_no_gen_wc,pig.tot_no_gen_ur,pig.pro_gen_wc,pig.user_id,pig.entry_date,pig.modify_date,un.ulb_Name,pig.next_maintenance_date from physical_infra_generetor pig,ulbs_name un where pig.ulb_id = un.ulb_id and  pig.ulb_id in ("
								+ ulbs + ") order by pig.entry_date desc";

					} else { // Only For ULB

						query = "select pig.phyInfra_gen_id, pig.ulb_id,pig.date_of_report,pig.tot_no_gen,pig.tot_no_gen_wc,pig.tot_no_gen_ur,pig.pro_gen_wc,pig.user_id,pig.entry_date,pig.modify_date,un.ulb_Name,pig.next_maintenance_date from physical_infra_generetor pig,ulbs_name un where pig.ulb_id = un.ulb_id and  pig.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by pig.entry_date desc";
					}

				}

			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraGeneretorModel physicalInfraGeneretorModel = new PhysicalInfraGeneretorModel();
				physicalInfraGeneretorModel.setPhyInfra_gen_id(rs.getInt(1));
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_Name(rs.getString(11));
				ulbNameModel.setUlb_id(rs.getInt(2));
				physicalInfraGeneretorModel.setUlbNameModel(ulbNameModel);
				physicalInfraGeneretorModel.setDate_of_report(rs
						.getString(3));

				physicalInfraGeneretorModel.setTot_no_gen(rs.getDouble(4));
				physicalInfraGeneretorModel.setTot_no_gen_wc(rs.getDouble(5));
				physicalInfraGeneretorModel.setTot_no_gen_ur(rs.getDouble(6));
				physicalInfraGeneretorModel.setPro_gen_wc(rs.getDouble(7));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(8)); // To be Changed
				physicalInfraGeneretorModel
						.setLoginUsersModel(loginUsersModel2);

				physicalInfraGeneretorModel.setEntry_date(rs.getString(9));
				physicalInfraGeneretorModel.setModify_date(rs.getString(10));
				physicalInfraGeneretorModel.setDueDate(rs.getString(12));
				
				physicalInfraGeneretorModels.add(physicalInfraGeneretorModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXDailyWaterSupply");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXDailyWaterSupply"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- END");
		}

		return physicalInfraGeneretorModels;

	}

}
