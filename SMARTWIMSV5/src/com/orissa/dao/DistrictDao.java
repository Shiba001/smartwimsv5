package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;
import com.orissa.model.PhDistrictModel;

@Repository
public class DistrictDao {

	private static Logger logger = Logger.getLogger(DistrictDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	public LinkedList fetchUpperLevelsByULBId(int ulbId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchUpperLevelsByULBId -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList upperLevels = new LinkedList<>();
		try {
			String query = "select un.ulb_id,un.ulb_name,pd.dist_id,pd.div_id,pd.dist_name,phd.div_id,phd.div_name from ulbs_name un,ph_district pd,ph_division phd where un.dist_id=pd.dist_id and phd.div_id=pd.div_id and un.ulb_id =?";
			pst = connection.prepareStatement(query);
			pst.setInt(1, ulbId);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				upperLevels.add(rs.getInt(1));
				upperLevels.add(rs.getString(2));
				upperLevels.add(rs.getInt(3));
				upperLevels.add(rs.getInt(4));
				upperLevels.add(rs.getString(5));
				upperLevels.add(rs.getInt(6));
				upperLevels.add(rs.getString(7));
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchUpperLevelsByULBId");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchUpperLevelsByULBId"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchUpperLevelsByULBId -- END");
		}

		return upperLevels;

	}
		
	public LinkedList<PhDistrictModel> fetchDistrictByDivisionId(int divisionId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDistrictByDivisionId -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhDistrictModel> phDistrictModels = new LinkedList<PhDistrictModel>();
		try {
			String query = "select Dist_id,Dist_Name from ph_district where div_id =? order by Dist_Name";
			pst = connection.prepareStatement(query);
			pst.setInt(1, divisionId);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhDistrictModel phDistrictModel = new PhDistrictModel();

				phDistrictModel.setDist_id(rs.getInt(1));
				phDistrictModel.setDist_Name(rs.getString(2));

				phDistrictModels.add(phDistrictModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDistrictByDivisionId");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDistrictByDivisionId"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDistrictByDivisionId -- END");
		}

		return phDistrictModels;

	}
}
