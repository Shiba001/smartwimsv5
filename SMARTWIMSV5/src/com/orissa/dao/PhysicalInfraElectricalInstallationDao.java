package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraElectricalInstallationsModel;
import com.orissa.model.ULBNameModel;

@Repository
public class PhysicalInfraElectricalInstallationDao {

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	private static Logger logger = Logger
			.getLogger(PhysicalInfraElectricalInstallationDao.class);

	public int addPhysElectInstallation(
			PhysicalInfraElectricalInstallationsModel physicalInfraElectricalInstallationsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysElectInstallation -- START");
		}

		int result = 0;
		Connection connection = null;
		try {
			connection = dbConnection.Connect();

			PreparedStatement pst = connection
					.prepareStatement("insert into physical_infra_electrical_installations(phyInfra_ele_inst_slno,"
							+ "date_of_report,"
							+ "ulb_id,"
							+ "loc_of_inst,"
							+ "cap_of_trnf,"
							+ "year_inst,"
							+ "hp_lt,"
							+ "user_id,"
							+ "entry_date,next_maintenance_date) values(?,?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?)");

			pst.setInt(1, physicalInfraElectricalInstallationsModel
					.getPhyInfra_ele_inst_slno());
			pst.setString(2, physicalInfraElectricalInstallationsModel
					.getDate_of_report());
			pst.setInt(3, physicalInfraElectricalInstallationsModel.getUlbId());
			pst.setString(4,
					physicalInfraElectricalInstallationsModel.getLoc_of_inst());
			pst.setString(5,
					physicalInfraElectricalInstallationsModel.getCap_of_trnf());
			pst.setString(6,
					physicalInfraElectricalInstallationsModel.getYear_inst());
			pst.setString(7,
					physicalInfraElectricalInstallationsModel.getHp_lt());
			pst.setInt(8, physicalInfraElectricalInstallationsModel
					.getLoginUsersModel().getUser_id());
			pst.setString(9, Util.getCurrentDateTime());
			pst.setString(10,
					physicalInfraElectricalInstallationsModel.getDueDate());
			
			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addPhysElectInstallation");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addPhysElectInstallation"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysElectInstallation -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraElectricalInstallationsModel> fetchElectricInstallation(
			int UlbId, String date_of_report) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchElectricInstallation -- START");
		}

		Connection connection = null;
		LinkedList<PhysicalInfraElectricalInstallationsModel> physicalInfraElectricalInstallationsModelList = new LinkedList<PhysicalInfraElectricalInstallationsModel>();

		try {
			connection = dbConnection.Connect();
			PreparedStatement pst = null;

			String query = " select piei.phyInfra_ele_inst_slno,piei.date_of_report,piei.ulb_id,piei.loc_of_inst,piei.cap_of_trnf,piei.year_inst,piei.hp_lt,piei.user_id,piei.entry_date,un.ulb_Name,piei.next_maintenance_date from physical_infra_electrical_installations piei,ulbs_name un where piei.ulb_id = un.ulb_id and piei.ulb_id=? order by piei.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, UlbId);
			System.out.println("PhysicalInfraElectricalInstallationsModel=="+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraElectricalInstallationsModel physInfraElectInstallModel = new PhysicalInfraElectricalInstallationsModel();

				physInfraElectInstallModel.setPhyInfra_ele_inst_slno(rs
						.getInt(1));
				physInfraElectInstallModel.setDate_of_report(rs.getString(2));

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(10));
				physInfraElectInstallModel.setUlbNameModel(ulbNameModel);

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(8));
				physInfraElectInstallModel.setLoginUsersModel(loginUsersModel);

				physInfraElectInstallModel.setLoc_of_inst(rs.getString(4));
				physInfraElectInstallModel.setCap_of_trnf(rs.getString(5));
				physInfraElectInstallModel.setYear_inst(rs.getString(6));
				physInfraElectInstallModel.setHp_lt(rs.getString(7));
				physInfraElectInstallModel.setEntry_date(rs.getString(9));
				physInfraElectInstallModel.setDueDate(rs.getString(11));

				physicalInfraElectricalInstallationsModelList
						.add(physInfraElectInstallModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchElectricInstallation");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchElectricInstallation"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchElectricInstallation -- END");
		}

		return physicalInfraElectricalInstallationsModelList;

	}

	public int deletePhysElectInstallation(
			PhysicalInfraElectricalInstallationsModel physicalInfraElectricalInstallationsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysElectInstallation -- START");
		}

		int result = 0;
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		try {

			connection = dbConnection.Connect();
			String query = "delete from physical_infra_electrical_installations where ulb_id = ?";
			pst = connection.prepareStatement(query);

			pst.setInt(1, physicalInfraElectricalInstallationsModel.getUlbId());
			/*pst.setString(2, physicalInfraElectricalInstallationsModel
					.getDate_of_report());*/

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for deletePhysElectInstallation");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for deletePhysElectInstallation"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysElectInstallation -- END");
		}

		return result;

	}
}
