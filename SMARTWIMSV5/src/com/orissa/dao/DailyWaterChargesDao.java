package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.DailyWaterChargesModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class DailyWaterChargesDao extends DeleteDao {

	private static Logger logger = Logger.getLogger(DailyWaterChargesDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbNameService;

	public int addDailyWaterCharges(
			DailyWaterChargesModel dailyWaterChargesModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterCharges -- START"); 
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		int result = 0;

		try {
			String query = "insert into daily_water_charges_tax(ulb_id,date_of_report,tot_cur_coll,	remarks,user_id,entry_date,IsApproved) values(?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?) ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, dailyWaterChargesModel.getUlbId());
			pst.setString(2, dailyWaterChargesModel.getDate_of_report());
			pst.setDouble(3, dailyWaterChargesModel.getTot_cur_coll());
			pst.setString(4, dailyWaterChargesModel.getRemarks());
			pst.setDouble(5, dailyWaterChargesModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(6, Util.getCurrentDateTime());
			pst.setInt(7, dailyWaterChargesModel.getIsApproved());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addDailyWaterCharges");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addDailyWaterCharges"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterCharges -- END");
		}

		return result;

	}

	public DailyWaterChargesModel fetchDailyWaterChargeById(int W_tax_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterChargeById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		DailyWaterChargesModel dailyWaterChargesModel = new DailyWaterChargesModel();
		try {
			String query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id and W_tax_id =? order by dwct.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, W_tax_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				dailyWaterChargesModel.setW_tax_id(rs.getInt(1));

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(10));
				
				dailyWaterChargesModel.setUlbNameModel(ulbNameModel);
				dailyWaterChargesModel.setDate_of_report(rs.getString(3));
				dailyWaterChargesModel.setTot_cur_coll(rs.getDouble(4));
				dailyWaterChargesModel.setRemarks(rs.getString(5));
				dailyWaterChargesModel.setIsApproved(rs.getInt(9));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterChargeById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterChargeById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterChargeById -- END");
		}

		return dailyWaterChargesModel;

	}

	public LinkedList<DailyWaterChargesModel> fetchDailyWaterCharges(LoginUsersModel loginUsersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterCharges -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyWaterChargesModel> dailyWaterChargesModels = new LinkedList<DailyWaterChargesModel>();
		try {
			
			String query = "";
			if(loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
				
				query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id and dwct.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dwct.ulb_id ="+loginUsersModel.getUlbNameModel().getUlb_id()+" order by dwct.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int districtId = (Integer)upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(districtId);
				
				String ulbs = "";
				for(ULBNameModel ulbNameModel:ulbNameModels) {
					
					ulbs = ulbs +ulbNameModel.getUlb_id()+",";
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id and dwct.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dwct.ulb_id in ("+ulbs+")  order by dwct.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int divisionId = (Integer)upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService.fetchDistrictByDivisionId(divisionId);
				
				String ulbs = "";
				for(PhDistrictModel phDistrictModel:districtModels) {
					
					LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(phDistrictModel.getDist_id());
					
					for(ULBNameModel ulbNameModel:ulbNameModels) {
						
						ulbs = ulbs +ulbNameModel.getUlb_id()+",";
					}
					
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id and dwct.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dwct.ulb_id in ("+ulbs+")  order by dwct.entry_date desc";
			
			} else { // Others All (State, PHEO, Circle, H&UD)
				
				query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id and dwct.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') order by dwct.entry_date desc";
			}
			
			//String query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id order by dwct.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setString(1, Util.getYesterdayDateString());
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				DailyWaterChargesModel dailyWaterChargesModel = new DailyWaterChargesModel();
				dailyWaterChargesModel.setW_tax_id(rs.getInt(1));

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(10));
				dailyWaterChargesModel.setUlbNameModel(ulbNameModel);
				dailyWaterChargesModel.setDate_of_report(rs.getString(3));
				dailyWaterChargesModel.setTot_cur_coll(rs.getDouble(4));
				dailyWaterChargesModel.setRemarks(rs.getString(5));
				dailyWaterChargesModel.setIsApproved(rs.getInt(9));
				
				dailyWaterChargesModels.add(dailyWaterChargesModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterCharges");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterCharges"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterCharges -- END");
		}

		return dailyWaterChargesModels;

	}

	public int updateDailyWaterCharges(
			DailyWaterChargesModel dailyWaterChargesModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterCharges -- START");
		}
		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update daily_water_charges_tax set ulb_id=?,date_of_report=?,tot_cur_coll=?,remarks=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s') where W_tax_id=? ";
			pst = connection.prepareStatement(query);

			pst.setInt(1, dailyWaterChargesModel.getUlbId());
			pst.setString(2, dailyWaterChargesModel.getDate_of_report());
			pst.setDouble(3, dailyWaterChargesModel.getTot_cur_coll());
			pst.setString(4, dailyWaterChargesModel.getRemarks());
			pst.setDouble(5, dailyWaterChargesModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(6, Util.getCurrentDateTime());
			pst.setInt(7, dailyWaterChargesModel.getW_tax_id());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updateDailyWaterCharges");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updateDailyWaterCharges"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterCharges -- END");
		}

		return result;

	}
	
	public LinkedList<DailyWaterChargesModel> fetchAJAXDailyWaterCharges(ListingModel listingModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterCharges -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyWaterChargesModel> dailyWaterChargesModels = new LinkedList<DailyWaterChargesModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id and dwct.date_of_report =(case '"
						+ listingModel.getDate()
						+ "' when 'null' then dwct.date_of_report else str_to_date('"
						+ listingModel.getDate()
						+ "','%Y-%m-%d %H:%i:%s') end) order by dwct.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id and dwct.date_of_report =(case '"
							+ listingModel.getDate()
							+ "' when 'null' then dwct.date_of_report else str_to_date('"
							+ listingModel.getDate()
							+ "','%Y-%m-%d %H:%i:%s') end) and dwct.ulb_id in ("
							+ ulbs + ") order by dwct.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id and dwct.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dwct.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dwct.ulb_id in ("
								+ ulbs + ") order by dwct.entry_date desc";

					} else { // Only For ULB

						query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id and dwct.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dwct.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dwct.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by dwct.entry_date desc";
					}

				}

			}

			//String query = "select dwct.W_tax_id,dwct.ulb_id,dwct.date_of_report,dwct.tot_cur_coll,dwct.remarks,dwct.user_id,dwct.entry_date,dwct.modify_date,dwct.IsApproved,un.ulb_name from daily_water_charges_tax dwct,ulbs_name un where dwct.ulb_id =un.ulb_id order by dwct.entry_date desc";
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				DailyWaterChargesModel dailyWaterChargesModel = new DailyWaterChargesModel();
				dailyWaterChargesModel.setW_tax_id(rs.getInt(1));

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(10));
				dailyWaterChargesModel.setUlbNameModel(ulbNameModel);
				dailyWaterChargesModel.setDate_of_report(rs.getString(3));
				dailyWaterChargesModel.setTot_cur_coll(rs.getDouble(4));
				dailyWaterChargesModel.setRemarks(rs.getString(5));
				dailyWaterChargesModel.setIsApproved(rs.getInt(9));
				
				dailyWaterChargesModels.add(dailyWaterChargesModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXDailyWaterCharges");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXDailyWaterCharges"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterCharges -- END");
		}

		return dailyWaterChargesModels;

	}
}
