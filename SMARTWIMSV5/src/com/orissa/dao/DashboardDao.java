/**
 * @formatter:off
 *
 */
package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.DateCalculation;
import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.DailyTankersModel;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.DailyWaterTestingModel;
import com.orissa.model.ListingModel;
import com.orissa.model.TargetModel;
import com.orissa.model.ULBModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;
import com.orissa.service.ULBService;

@Repository
public class DashboardDao extends DeleteDao {

	private static Logger logger = Logger.getLogger(DashboardDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbNameService;
	
	@Autowired
	private ULBService ulbService;
	
	@Autowired
	private DateCalculation dateCalculation;
	
	public LinkedList<DailyWaterSupplyModel> fetchDailyWaterSupplyReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyReport -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = new LinkedList<DailyWaterSupplyModel>();
		int i = 1;
		double total_tot_demand = 0.0D;
		double total_qlty_supply = 0.0D;
		double total_pop_served = 0.0D;
		double total_Freq_supply = 0.0D;
		double total_surplus_deficit = 0.0D;
		double total_rate_of_supply = 0.0D;
		double total_coverage_by_pws = 0.0D;
		
		double pm_total_tot_demand = 0.0D;
		double pm_total_qlty_supply = 0.0D;
		double pm_total_pop_served = 0.0D;
		double pm_total_Freq_supply = 0.0D;
		double pm_total_surplus_deficit = 0.0D;
		double pm_total_rate_of_supply = 0.0D;
		double pm_total_coverage_by_pws = 0.0D;
		
		try {
			
			String ulb = "";
			String ulbPopulation = "";
			
			if(listingModel.getUlbId() == null) { // No ULB ID Present
				
				if(listingModel.getDistrictId() == null) { // No District ID Present
					
					if(listingModel.getDivisionId() == null) { // No Division ID Present
						
						ulb = " = u.m_ulb_id";
						ulbPopulation = "= m_ulb_id";
						
					} else { // Division ID Present
						
						LinkedList<ULBModel> ulbModels = ulbService.fetchULBByDivisionId(listingModel
								.getDivisionId());
						
						String ulbs = "";

						for (ULBModel ulbModel : ulbModels) {

							ulbs = ulbs + ulbModel.getM_ulb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
						ulb = " in("+ulbs+")";
						ulbPopulation = ulb;
						
					}
				
				} else { // District ID Present
					
					LinkedList<ULBModel> ulbModels = ulbService.fetchULBByDistrictId(listingModel
							.getDistrictId());
					
					String ulbs = "";

					for (ULBModel ulbModel : ulbModels) {

						ulbs = ulbs + ulbModel.getM_ulb_id() + ",";
					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
					ulb = " in("+ulbs+")";
					ulbPopulation = ulb;
				}
				
			} else {//  ULB ID Present
				
				ulb = "="+String.valueOf(listingModel.getUlbId());
				ulbPopulation = ulb;
			}
			
			String populationServed = "ifnull(sum(dws.pop_served),0), ";
			String quantitySupplied = "ifnull(sum(dws.qlty_supply),0), ";
			if (endDate != null) {
				int days = dateCalculation.findDaysBetween(startDate, endDate);
				populationServed = "ifnull(sum(dws.pop_served)/"+days+",0), ";
				quantitySupplied = "ifnull(sum(dws.qlty_supply)/"+days+",0), ";
			} 
			
			String query = "select u.m_ulb_id, " // ULBS ID (Table Containing 112 values) [Current Month]
					+ "u.ulb_name, " // ULBS Name (Table Containing 112 values) [Current Month]
					+ "ifnull(sum(dws.tot_demand),0), " // Total Demand (In MLD) [Current Month]
					+ quantitySupplied // Quantity Supplied by PWS (In MLD) [Current Month]
					/*+ "ifnull(sum(dws.qlty_supply),0), " */// Quantity Supplied by PWS (In MLD) [Current Month]
					+  populationServed // Population Served by PWS [Current Month]
					+ "ifnull(sum(dws.surplus_deficit),0), " // Deficit / Surplus (In MLD) [Current Month]
					+ "ifnull(sum(dws.lpcd)/count(*),0), " // LPCD [Current Month]
					+ "ifnull(sum(dws.Freq_supply)/count(*),0), " // Average Hour of Supply [Current Month]
					+ "ifnull((ifnull(sum(dws.pop_served),0)*100)/sum(un.ulb_pop),0), " // Coverage by PWS (In %) [Current Month]
					+ "u.latitude, " // Latitude Of the ULBs [Current Month]
					+ "u.longitude, "  // Longitude Of the ULBs [Current Month]
					+ "'current' "  // [Current Month]
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join daily_water_supply dws on un.ulb_id = dws.ulb_id "
					+ "inner join  amrut_type at on at.Amrut_type_id = u.Amrut_type_id "
					+ "where u.Amrut_type_id = (case '"+amrutTypeId+"' when 'null' then u.Amrut_type_id else '"+amrutTypeId+"' end) "
					+ "and str_to_date(dws.date_of_report,'%Y-%m-%d') "
					+ "between str_to_date('"+startDate+"','%Y-%m-%d') "
					+ "and (case '"+endDate+"' "
					+ "when 'null' then str_to_date('"+startDate+"','%Y-%m-%d') "
					+ "else str_to_date('"+endDate+"','%Y-%m-%d') end) "
					+ "and u.m_ulb_id "+ulb+" "
					+ "and dws.IsApproved = 1 "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "UNION ALL "
					+ "select u.m_ulb_id, " // ULBS ID (Table Containing 112 values) [Previous Month]
					+ "u.ulb_name, " // ULBS Name (Table Containing 112 values) [Previous Month]
					+ "ifnull(sum(dws.tot_demand),0), " // Total Demand (In MLD) [Previous Month]
					+ quantitySupplied // Quantity Supplied by PWS (In MLD) [Current Month]
					/*+ "ifnull(sum(dws.qlty_supply),0), " */// Quantity Supplied by PWS (In MLD) [Current Month]
					+  populationServed // Population Served by PWS [Previous Month]
					+ "ifnull(sum(dws.surplus_deficit),0), " // Deficit / Surplus (In MLD) [Previous Month]
					+ "ifnull(sum(dws.lpcd)/count(*),0), " // LPCD [Previous Month]
					+ "ifnull(sum(dws.Freq_supply)/count(*),0), " // Average Hour of Supply [Previous Month]
					+ "ifnull((ifnull(sum(dws.pop_served),0)*100)/sum(un.ulb_pop),0), " // Coverage by PWS (In %) [Previous Month]
					+ "u.latitude, " // Latitude Of the ULBs [Previous Month]
					+ "u.longitude, " // Longitude Of the ULBs [Previous Month]
					+ "'previous' " // [Previous Month]
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join daily_water_supply dws on un.ulb_id = dws.ulb_id "
					+ "inner join  amrut_type at on at.Amrut_type_id = u.Amrut_type_id "
					+ "where u.Amrut_type_id = (case '"+amrutTypeId+"' when 'null' then u.Amrut_type_id else '"+amrutTypeId+"' end) "
					+ "and str_to_date(dws.date_of_report,'%Y-%m-%d') "
					+ "between (str_to_date('"+startDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) "
					+ "and (case '"+endDate+"' "
					+ "when 'null' then (str_to_date('"+startDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) "
					+ "else (str_to_date('"+endDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) end) "
					+ "and u.m_ulb_id "+ulb+" "
					+ "and dws.IsApproved = 1 "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "order by 2,12";
			
			pst = connection.prepareStatement(query);
			System.out.println("fetchDailyWaterSupplyReport ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			
			int current = 0;
			int previous = 0;
			
			while (rs.next()) {

				DailyWaterSupplyModel dailyWaterSupplyModel = new DailyWaterSupplyModel();
				
				dailyWaterSupplyModel.setSlNo(i++);
				ULBNameModel ulbNameModel = new ULBNameModel();
				
				ULBModel ulbModel = new ULBModel();
				ulbModel.setM_ulb_id(rs.getInt(1)); // ULBS ID (Table Containing 112 values) [Current Month]
				ulbModel.setUlb_name(rs.getString(2));  // ULBS Name (Table Containing 112 values) [Current Month]
				ulbModel.setLatitude(rs.getString(10)); // Latitude Of the ULBs [Current Month]
				ulbModel.setLongitude(rs.getString(11));  // Longitude Of the ULBs [Current Month]
				ulbNameModel.setUlbModel(ulbModel);
				dailyWaterSupplyModel.setUlbNameModel(ulbNameModel);
				dailyWaterSupplyModel.setType(rs.getString(12));
				
				if(dailyWaterSupplyModel.getType().equals("current")) {
					
					current = current + 1;
					dailyWaterSupplyModel.setTot_demand(rs.getDouble(3)); // Total Demand (In MLD) [Current Month]
					dailyWaterSupplyModel.setQlty_supply(rs.getDouble(4)); // Quantity Supplied by PWS (In MLD) [Current Month]
					dailyWaterSupplyModel.setPop_served(rs.getDouble(5)); // Population Served by PWS [Current Month]
					dailyWaterSupplyModel.setSurplus_deficit(rs.getDouble(6)); // Deficit / Surplus (In MLD) [Current Month]
					//dailyWaterSupplyModel.setRate_of_supply(Math.round(rs.getDouble(7)*100.0)/100.0); // LPCD [Current Month]
					
					dailyWaterSupplyModel.setRate_of_supply(Math.round(dailyWaterSupplyModel.getQlty_supply()*1000000/dailyWaterSupplyModel.getPop_served()*100.0)/100.0);
					
					dailyWaterSupplyModel.setFreq_supply(Math.floor(rs.getDouble(8) + 0.5));// Average Hour of Supply [Current Month]
					
					dailyWaterSupplyModel.setCoverage_by_pws(Math.round(rs.getDouble(9)*100.0)/100.0); // Coverage by PWS (In %) [Current Month]
					
					total_tot_demand = total_tot_demand + dailyWaterSupplyModel.getTot_demand(); // Total Of Total Demand (In MLD) [Current Month]
					total_qlty_supply = total_qlty_supply + dailyWaterSupplyModel.getQlty_supply(); // Total Of Quantity Supplied by PWS (In MLD) [Current Month]
					total_pop_served = total_pop_served + dailyWaterSupplyModel.getPop_served(); // Total Of Population Served by PWS [Current Month]
					total_surplus_deficit = total_surplus_deficit + dailyWaterSupplyModel.getSurplus_deficit(); // Total Of Deficit / Surplus (In MLD) [Current Month]
					total_Freq_supply = total_Freq_supply + dailyWaterSupplyModel.getFreq_supply();// Total Of Average Hour of Supply [Current Month]
					
				} else {
					
					previous = previous + 1;
					dailyWaterSupplyModel.setPm_tot_demand(rs.getDouble(3)); // Total Demand (In MLD) [Previous Month]
					dailyWaterSupplyModel.setPm_qlty_supply(rs.getDouble(4)); // Quantity Supplied by PWS (In MLD) [Previous Month]
					dailyWaterSupplyModel.setPm_pop_served(rs.getDouble(5)); // Population Served by PWS [Previous Month]
					dailyWaterSupplyModel.setPm_surplus_deficit(rs.getDouble(6)); // Deficit / Surplus (In MLD) [Previous Month]
					//dailyWaterSupplyModel.setPm_rate_of_supply(Math.round(rs.getDouble(7)*100.0)/100.0); // LPCD [Previous Month]
					
					dailyWaterSupplyModel.setPm_rate_of_supply(Math.round(dailyWaterSupplyModel.getPm_qlty_supply()*1000000/dailyWaterSupplyModel.getPm_pop_served()*100.0)/100.0);
					
					dailyWaterSupplyModel.setPm_Freq_supply(Math.floor(rs.getDouble(8) + 0.5)); // Average Hour of Supply [Previous Month]
					dailyWaterSupplyModel.setPm_coverage_by_pws(Math.round(rs.getDouble(9)*100.0)/100.0); // Coverage by PWS (In %) [Previous Month]
					
					pm_total_tot_demand = pm_total_tot_demand + dailyWaterSupplyModel.getPm_tot_demand(); // Total Of Total Demand (In MLD) [Current Month]
					pm_total_qlty_supply = pm_total_qlty_supply + dailyWaterSupplyModel.getPm_qlty_supply(); // Total Of Quantity Supplied by PWS (In MLD) [Current Month]
					pm_total_pop_served = pm_total_pop_served + dailyWaterSupplyModel.getPm_pop_served(); // Total Of Population Served by PWS [Current Month]
					pm_total_surplus_deficit = pm_total_surplus_deficit + dailyWaterSupplyModel.getPm_surplus_deficit(); // Total Of Deficit / Surplus (In MLD) [Current Month]
					pm_total_Freq_supply = pm_total_Freq_supply + dailyWaterSupplyModel.getPm_Freq_supply();// Total Of Average Hour of Supply [Current Month]
					
				}

				dailyWaterSupplyModels.add(dailyWaterSupplyModel);
			}
			
			total_Freq_supply = total_Freq_supply / current ; // [Current Month]
			pm_total_Freq_supply = pm_total_Freq_supply / previous ; // [Previous Month]
			
			total_Freq_supply = Math.floor(total_Freq_supply + 0.5);
			pm_total_Freq_supply = Math.floor(pm_total_Freq_supply + 0.5);
			
			DailyWaterSupplyModel dailyWaterSupplyModel = new DailyWaterSupplyModel();
			
			dailyWaterSupplyModel.setTotal_tot_demand(Math.round(total_tot_demand*100.0)/100.0); // Total Demand (in MLD) [Current Month]
			dailyWaterSupplyModel.setTotal_qlty_supply(Math.round(total_qlty_supply*100.0)/100.0); // Quantity Supplied by PWS (in MLD) [Current Month]
			dailyWaterSupplyModel.setTotal_pop_served(Math.round(total_pop_served*100.0)/100.0); // Population Served by PWS [Current Month]
			dailyWaterSupplyModel.setTotal_surplus_deficit(Math.round(total_surplus_deficit*100.0)/100.0); // Deficit / Surplus ( in MLD) [Current Month]
			dailyWaterSupplyModel.setTotal_Freq_supply(Math.round(total_Freq_supply*100.0)/100.0); // [Current Month]
			
			
			dailyWaterSupplyModel.setPm_total_tot_demand(Math.round(pm_total_tot_demand*100.0)/100.0); // Total Demand (in MLD) [Previous Month]
			dailyWaterSupplyModel.setPm_total_qlty_supply(Math.round(pm_total_qlty_supply*100.0)/100.0); // Quantity Supplied by PWS (in MLD) [Previous Month]
			dailyWaterSupplyModel.setPm_total_pop_served(Math.round(pm_total_pop_served*100.0)/100.0); // Population Served by PWS [Previous Month]
			dailyWaterSupplyModel.setPm_total_surplus_deficit(Math.round(pm_total_surplus_deficit*100.0)/100.0); // Deficit / Surplus ( in MLD) [Previous Month]
			dailyWaterSupplyModel.setPm_total_Freq_supply(Math.round(pm_total_Freq_supply*100.0)/100.0); // Average Hour Of Supply [Previous Month]
			
			
			// Current Month
			if(total_pop_served != 0)
				total_rate_of_supply = (total_qlty_supply * 1000000 ) / total_pop_served;
			
			dailyWaterSupplyModel.setTotal_rate_of_supply(Math.round(total_rate_of_supply*100.0)/100.0);
			
			// Previous Month
			if(pm_total_pop_served != 0)
				pm_total_rate_of_supply = (pm_total_qlty_supply * 1000000 ) / pm_total_pop_served;
			
			dailyWaterSupplyModel.setPm_total_rate_of_supply(Math.round(pm_total_rate_of_supply*100.0)/100.0);
			
			double total_ulb_population = 0;
			PreparedStatement pst2 = null;
			try {
				String query2 = "SELECT sum(ulb_pop) FROM ulbs where m_ulb_id "+ulbPopulation+" ";
				pst2 = connection.prepareStatement(query2);
				System.out.println("ULB Population ---- >>>"+pst2);
				ResultSet rs2 = pst2.executeQuery();
				if (rs2.next()) {
					total_ulb_population = rs2.getDouble(1);
				}
				
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			// Current Month
			total_coverage_by_pws = (total_pop_served * 100) / total_ulb_population;
			dailyWaterSupplyModel.setTotal_coverage_by_pws(Math.round(total_coverage_by_pws*100.0)/100.0);
			
			// Previous Month
			pm_total_coverage_by_pws = (pm_total_pop_served * 100) / total_ulb_population;
			dailyWaterSupplyModel.setPm_total_coverage_by_pws(Math.round(pm_total_coverage_by_pws*100.0)/100.0);
			
			dailyWaterSupplyModels.add(dailyWaterSupplyModel);

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterSupplyReport");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterSupplyReport"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyReport -- END");
		}

		return dailyWaterSupplyModels;

	}
	
	public LinkedList<TargetModel> fetchTargetWaterChargesTaxReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTaxReport -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<TargetModel> targetModels = new LinkedList<TargetModel>();
		int i = 1;
		double totalChargesCollections = 0.0D;
		double totalTarget = 0.0D;
		double Pm_totalChargesCollections = 0.0D;
		double Pm_totalTarget = 0.0D;
		
		try {
			

			String ulb = "";
			
			if(listingModel.getUlbId() == null) { // No ULB ID Present
				
				if(listingModel.getDistrictId() == null) { // No District ID Present
					
					if(listingModel.getDivisionId() == null) { // No Division ID Present
						
						ulb = " = u.m_ulb_id";
						
					} else { // Division ID Present
						
						LinkedList<ULBModel> ulbModels = ulbService.fetchULBByDivisionId(listingModel
								.getDivisionId());
						
						String ulbs = "";

						for (ULBModel ulbModel : ulbModels) {

							ulbs = ulbs + ulbModel.getM_ulb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
						ulb = " in("+ulbs+")";
						
					}
				
				} else { // District ID Present
					
					LinkedList<ULBModel> ulbModels = ulbService.fetchULBByDistrictId(listingModel
							.getDistrictId());
					
					String ulbs = "";

					for (ULBModel ulbModel : ulbModels) {

						ulbs = ulbs + ulbModel.getM_ulb_id() + ",";
					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
					ulb = " in("+ulbs+")";
				}
				
			} else {//  ULB ID Present
				
				ulb = "="+String.valueOf(listingModel.getUlbId());
			}
			
			String query = "select u.m_ulb_id, " // ULBS ID (Table Containing 112 values) [Current Month]
					+ "u.ulb_name, " // ULBS Name (Table Containing 112 values) [Current Month]
					+ "ifnull((sum(dwct.tot_cur_coll)/(case (select month(str_to_date('"+startDate+"','%Y-%m-%d'))-(case when month(str_to_date('"+startDate+"','%Y-%m-%d')) between 1 and 3 then -9 else 3 end)) when 1 then m_trgt_1 when 2 then m_trgt_2 when 3 then m_trgt_3 when 4 then m_trgt_4 when 5 then m_trgt_5 when 6 then m_trgt_6 when 7 then m_trgt_7 when 8 then m_trgt_8 when 9 then m_trgt_9 when 10 then m_trgt_10 when 11 then m_trgt_11 else m_trgt_12 end))*100,0) percentage, " // Percentage [Current Month]
					+ "ifnull(sum(dwct.tot_cur_coll),0), " // Total Collection [Current Month]
					+ "ifnull((case (select month(str_to_date('"+startDate+"','%Y-%m-%d'))-(case when month(str_to_date('"+startDate+"','%Y-%m-%d')) between 1 and 3 then -9 else 3 end)) when 1 then m_trgt_1 when 2 then m_trgt_2 when 3 then m_trgt_3 when 4 then m_trgt_4 when 5 then m_trgt_5 when 6 then m_trgt_6 when 7 then m_trgt_7 when 8 then m_trgt_8 when 9 then m_trgt_9 when 10 then m_trgt_10 when 11 then m_trgt_11 else m_trgt_12 end),0) tar, " // Target [Current Month]
					+ "u.latitude, " // Latitude Of the ULBs [Current Month]
					+ "u.longitude, "  // Longitude Of the ULBs [Current Month]
					+ "'current' "  // [Current Month]
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join  daily_water_charges_tax dwct on un.ulb_id = dwct.ulb_id "
					+ "inner join amrut_type at on at.Amrut_type_id = u.Amrut_type_id "
					+ "left outer join target t on dwct.ulb_id = t.ulb_id "
					+ "where u.Amrut_type_id = (case '"+amrutTypeId+"' when 'null' then u.Amrut_type_id else '"+amrutTypeId+"' end) "
					+ "and str_to_date(dwct.date_of_report,'%Y-%m-%d') "
					+ "between str_to_date('"+startDate+"','%Y-%m-%d') "
					+ "and (case '"+endDate+"' "
					+ "when 'null' then str_to_date('"+startDate+"','%Y-%m-%d') "
					+ "else str_to_date('"+endDate+"','%Y-%m-%d') end) "
					+ "and u.m_ulb_id "+ulb+" "
					+ "and dwct.IsApproved = 1 "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "UNION ALL "
					+ "select u.m_ulb_id, " // ULBS ID (Table Containing 112 values) [Previous Month]
					+ "u.ulb_name, " // ULBS Name (Table Containing 112 values) [Previous Month]
					+ "ifnull((sum(dwct.tot_cur_coll)/(case (select month(str_to_date('"+startDate+"','%Y-%m-%d'))-(case when month(str_to_date('"+startDate+"','%Y-%m-%d')) between 1 and 3 then -9 else 3 end)) when 1 then m_trgt_1 when 2 then m_trgt_2 when 3 then m_trgt_3 when 4 then m_trgt_4 when 5 then m_trgt_5 when 6 then m_trgt_6 when 7 then m_trgt_7 when 8 then m_trgt_8 when 9 then m_trgt_9 when 10 then m_trgt_10 when 11 then m_trgt_11 else m_trgt_12 end))*100,0) percentage, " // Percentage [Previous Month]
					+ "ifnull(sum(dwct.tot_cur_coll),0), " // Total Collection [Previous Month]
					+ "ifnull((case (select month(str_to_date('"+startDate+"','%Y-%m-%d'))-(case when month(str_to_date('"+startDate+"','%Y-%m-%d')) between 1 and 3 then -9 else 3 end)) when 1 then m_trgt_1 when 2 then m_trgt_2 when 3 then m_trgt_3 when 4 then m_trgt_4 when 5 then m_trgt_5 when 6 then m_trgt_6 when 7 then m_trgt_7 when 8 then m_trgt_8 when 9 then m_trgt_9 when 10 then m_trgt_10 when 11 then m_trgt_11 else m_trgt_12 end),0) tar, " // Target [Previous Month]
					+ "u.latitude, " // Latitude Of the ULBs [Previous Month]
					+ "u.longitude, "  // Longitude Of the ULBs [Previous Month]
					+ "'previous' " // [Previous Month]
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join  daily_water_charges_tax dwct on un.ulb_id = dwct.ulb_id "
					+ "inner join amrut_type at on at.Amrut_type_id = u.Amrut_type_id "
					+ "left outer join target t on dwct.ulb_id = t.ulb_id "
					+ "where u.Amrut_type_id = (case '"+amrutTypeId+"' when 'null' then u.Amrut_type_id else '"+amrutTypeId+"' end) "
					+ "and str_to_date(dwct.date_of_report,'%Y-%m-%d') "
					+ "between (str_to_date('"+startDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) "
					+ "and (case '"+endDate+"' "
					+ "when 'null' then (str_to_date('"+startDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) "
					+ "else (str_to_date('"+endDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) end) "
					+ "and u.m_ulb_id "+ulb+" "
					+ "and dwct.IsApproved = 1 "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "order by 2,8";
			
			pst = connection.prepareStatement(query);
			System.out.println("fetchTargetWaterChargesTaxReport ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				TargetModel targetModel = new TargetModel();
				
				targetModel.setSlNo(i++);
				ULBNameModel ulbNameModel = new ULBNameModel();
				
				ULBModel ulbModel = new ULBModel();
				ulbModel.setM_ulb_id(rs.getInt(1)); // ULBS ID (Table Containing 112 values)
				ulbModel.setUlb_name(rs.getString(2));  // ULBS Name (Table Containing 112 values)
				ulbModel.setLatitude(rs.getString(6));  // Latitude Of the ULBs
				ulbModel.setLongitude(rs.getString(7));  // Longitude Of the ULBs
				ulbNameModel.setUlbModel(ulbModel);
				targetModel.setUlbNameModel(ulbNameModel);
				targetModel.setType(rs.getString(8));
				
				if(targetModel.getType().equals("current")) {
					
					if(rs.getDouble(3) != 0.0)
						targetModel.setPercentage(Math.round(rs.getDouble(3)*100.0)/100.0);
					else
						targetModel.setPercentage(rs.getDouble(3));
					
					targetModel.setTotalCollection(rs.getDouble(4));
					
					if(rs.getDouble(5) != 0.0)
						targetModel.setTarget(Math.round(rs.getDouble(5)*100.0)/100.0);
					else
						targetModel.setTarget(rs.getDouble(5));
					
					totalChargesCollections = totalChargesCollections + targetModel.getTotalCollection();
					totalTarget = totalTarget + targetModel.getTarget();
					
				} else {
					
					if(rs.getDouble(3) != 0.0)
						targetModel.setPm_percentage(Math.round(rs.getDouble(3)*100.0)/100.0);
					else
						targetModel.setPm_percentage(rs.getDouble(3));
					
					targetModel.setPm_totalCollection(rs.getDouble(4));
					
					if(rs.getDouble(5) != 0.0)
						targetModel.setPm_target(Math.round(rs.getDouble(5)*100.0)/100.0);
					else
						targetModel.setPm_target(rs.getDouble(5));
					
					Pm_totalChargesCollections = Pm_totalChargesCollections + targetModel.getPm_totalCollection();
					Pm_totalTarget = Pm_totalTarget + targetModel.getPm_target();
				}
				
				targetModels.add(targetModel);
				
			}
			
			TargetModel targetModel = new TargetModel();
			
			if(totalChargesCollections != 0.0)
				targetModel.setTotalChargesCollections(Math.round(totalChargesCollections*100.0)/100.0);
			else
				targetModel.setTotalChargesCollections(0.0);
						
			if(totalTarget != 0.0) {
				
				targetModel.setTotalTarget(Math.round(totalTarget*100.0)/100.0);
				targetModel.setTotal_percentage(targetModel.getTotalChargesCollections()*100/targetModel.getTotalTarget());
				targetModel.setTotal_percentage(Math.round(targetModel.getTotal_percentage()*100.0)/100.0);
			
			} else {
				
				targetModel.setTotalTarget(0.0);
				targetModel.setTotal_percentage(0.0);
			}
				
			
			if(Pm_totalChargesCollections != 0.0)
				targetModel.setPm_totalChargesCollections(Math.round(Pm_totalChargesCollections*100.0)/100.0);
			else
				targetModel.setPm_totalChargesCollections(0.0);
			
			if(Pm_totalTarget != 0.0) {
				
				targetModel.setPm_totalTarget(Math.round(Pm_totalTarget*100.0)/100.0);
				targetModel.setPm_total_percentage(targetModel.getPm_totalChargesCollections()*100/targetModel.getPm_totalTarget());
				targetModel.setPm_total_percentage(Math.round(targetModel.getPm_total_percentage()*100.0)/100.0);
				
			} else {
				
				targetModel.setPm_totalTarget(0.0);
				targetModel.setPm_total_percentage(0.0);
			}
				
			targetModels.add(targetModel);
			
		} finally {
		
			try { // Closing Connection Object
				if (connection != null) {
		
					if (!connection.isClosed())
						connection.close();
		
					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchTargetWaterChargesTaxReport");
					}
				}
			} catch (Exception e) {
		
				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchTargetWaterChargesTaxReport"
							+ e.getMessage());
				}
		
			}
		
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTaxReport -- END");
		}
		
		return targetModels;
		
	}
	
	public LinkedList<DailyComplaintsModel> fetchDailyComplaintsReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaintsReport -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyComplaintsModel> dailyComplaintsModels = new LinkedList<DailyComplaintsModel>();
		int i = 1;
		
		double total_comp_recveived = 0.0D;
		double total_comp_resolved = 0.0D;
		double total_comp_pending = 0.0D;
		double total_tw_comp_recveived = 0.0D;
		double total_tw_comp_resolved = 0.0D;
		double total_tw_comp_pending = 0.0D;
		
		double Pm_total_comp_recveived = 0.0D;
		double Pm_total_comp_resolved = 0.0D;
		double Pm_total_comp_pending = 0.0D;
		double Pm_total_tw_comp_recveived = 0.0D;
		double Pm_total_tw_comp_resolved = 0.0D;
		double Pm_total_tw_comp_pending = 0.0D;
		
		try {
			
			String ulb = "";
			
			if(listingModel.getUlbId() == null) { // No ULB ID Present
				
				if(listingModel.getDistrictId() == null) { // No District ID Present
					
					if(listingModel.getDivisionId() == null) { // No Division ID Present
						
						ulb = " = u.m_ulb_id";
						
					} else { // Division ID Present
						
						LinkedList<ULBModel> ulbModels = ulbService.fetchULBByDivisionId(listingModel
								.getDivisionId());
						
						String ulbs = "";

						for (ULBModel ulbModel : ulbModels) {

							ulbs = ulbs + ulbModel.getM_ulb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
						ulb = " in("+ulbs+")";
						
					}
				
				} else { // District ID Present
					
					LinkedList<ULBModel> ulbModels = ulbService.fetchULBByDistrictId(listingModel
							.getDistrictId());
					
					String ulbs = "";

					for (ULBModel ulbModel : ulbModels) {

						ulbs = ulbs + ulbModel.getM_ulb_id() + ",";
					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
					ulb = " in("+ulbs+")";
				}
				
			} else {//  ULB ID Present
				
				ulb = "="+String.valueOf(listingModel.getUlbId());
			}
			
			String query = "select u.m_ulb_id, " // ULBS ID (Table Containing 112 values) [Current Month]
					+ "u.ulb_name, " // ULBS Name (Table Containing 112 values) [Current Month]
					+ "ifnull(sum(dc.no_ws_comp_recv),0), " // Water Supply Complaints Received [Current Month]
					+ "ifnull(sum(dc.no_ws_comp_resolved),0), " // Water Supply Complaints Resolved [Current Month]
					+ "ifnull(sum(dc.no_ws_comp_pending),0), " // Water Supply Complaints Pending [Current Month]
					+ "ifnull(sum(dc.no_tw_comp_recv),0), " // Tube Well Complaints Received [Current Month]
					+ "ifnull(sum(dc.no_tw_comp_resolved),0), " // Tube Well Complaints Resolved [Current Month]
					+ "ifnull(sum(dc.no_tw_comp_pending),0), " // Tube Well Complaints Pending [Current Month]
					+ "u.latitude, " // Latitude Of the ULBs [Current Month]
					+ "u.longitude, "  // Longitude Of the ULBs [Current Month]
					+ "'current' "  // [Current Month]
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join daily_complaints dc on un.ulb_id = dc.ulb_id "
					+ "inner join  amrut_type at on at.Amrut_type_id = u.Amrut_type_id "
					+ "where u.Amrut_type_id = (case '"+amrutTypeId+"' when 'null' then u.Amrut_type_id else '"+amrutTypeId+"' end) "
					+ "and str_to_date(dc.date_of_report,'%Y-%m-%d') "
					+ "between str_to_date('"+startDate+"','%Y-%m-%d') "
					+ "and (case '"+endDate+"' "
					+ "when 'null' then str_to_date('"+startDate+"','%Y-%m-%d') "
					+ "else str_to_date('"+endDate+"','%Y-%m-%d') end) "
					+ "and u.m_ulb_id "+ulb+" "
					+ "and dc.IsApproved = 1 "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "UNION ALL "
					+ "select u.m_ulb_id, " // ULBS ID (Table Containing 112 values) [Previous Month]
					+ "u.ulb_name, " // ULBS Name (Table Containing 112 values) [Previous Month]
					+ "ifnull(sum(dc.no_ws_comp_recv),0), " // Water Supply Complaints Received [Previous Month]
					+ "ifnull(sum(dc.no_ws_comp_resolved),0), " // Water Supply Complaints Resolved [Previous Month]
					+ "ifnull(sum(dc.no_ws_comp_pending),0), " // Water Supply Complaints Pending [Previous Month]
					+ "ifnull(sum(dc.no_tw_comp_recv),0), " // Tube Well Complaints Received [Previous Month]
					+ "ifnull(sum(dc.no_tw_comp_resolved),0), " // Tube Well Complaints Resolved [Previous Month]
					+ "ifnull(sum(dc.no_tw_comp_pending),0), " // Tube Well Complaints Pending [Previous Month]
					+ "u.latitude, " // Latitude Of the ULBs [Previous Month]
					+ "u.longitude, "  // Longitude Of the ULBs [Previous Month]
					+ "'previous' " // [Previous Month]
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join daily_complaints dc on un.ulb_id = dc.ulb_id "
					+ "inner join  amrut_type at on at.Amrut_type_id = u.Amrut_type_id "
					+ "where u.Amrut_type_id = (case '"+amrutTypeId+"' when 'null' then u.Amrut_type_id else '"+amrutTypeId+"' end) "
					+ "and str_to_date(dc.date_of_report,'%Y-%m-%d') "
					+ "between (str_to_date('"+startDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) "
					+ "and (case '"+endDate+"' "
					+ "when 'null' then (str_to_date('"+startDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) "
					+ "else (str_to_date('"+endDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) end) "
					+ "and u.m_ulb_id "+ulb+" "
					+ "and dc.IsApproved = 1 "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "order by 2,11";
			
			pst = connection.prepareStatement(query);
			System.out.println("fetchDailyComplaintsReport ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				DailyComplaintsModel dailyComplaintsModel = new DailyComplaintsModel();
				
				dailyComplaintsModel.setSlNo(i++);
				ULBNameModel ulbNameModel = new ULBNameModel();
				
				ULBModel ulbModel = new ULBModel();
				ulbModel.setM_ulb_id(rs.getInt(1)); // ULBS ID (Table Containing 112 values) [Current Month]
				ulbModel.setUlb_name(rs.getString(2));  // ULBS Name (Table Containing 112 values) [Current Month]
				ulbModel.setLatitude(rs.getString(9));  // Latitude Of the ULBs [Current Month]
				ulbModel.setLongitude(rs.getString(10));  // Longitude Of the ULBs [Current Month]
				ulbNameModel.setUlbModel(ulbModel);
				dailyComplaintsModel.setUlbNameModel(ulbNameModel);
				dailyComplaintsModel.setType(rs.getString(11));
				
				if(dailyComplaintsModel.getType().equals("current")) {
				
					dailyComplaintsModel.setNo_ws_comp_recv(rs.getDouble(3));
					dailyComplaintsModel.setNo_ws_comp_resolved(rs.getDouble(4));
					dailyComplaintsModel.setNo_ws_comp_pending(rs.getDouble(5));
					dailyComplaintsModel.setNo_tw_comp_recv(rs.getDouble(6));
					dailyComplaintsModel.setNo_tw_comp_resolved(rs.getDouble(7));
					dailyComplaintsModel.setNo_tw_comp_pending(rs.getDouble(8));
					
					total_comp_recveived = total_comp_recveived + dailyComplaintsModel.getNo_ws_comp_recv();
					total_comp_resolved = total_comp_resolved + dailyComplaintsModel.getNo_ws_comp_resolved();
					total_comp_pending = total_comp_pending + dailyComplaintsModel.getNo_ws_comp_pending();
					total_tw_comp_recveived = total_tw_comp_recveived + dailyComplaintsModel.getNo_tw_comp_recv();
					total_tw_comp_resolved = total_tw_comp_resolved + dailyComplaintsModel.getNo_tw_comp_resolved();
					total_tw_comp_pending = total_tw_comp_pending + dailyComplaintsModel.getNo_tw_comp_pending();
				
				} else {
					
					dailyComplaintsModel.setPm_no_ws_comp_recv(rs.getDouble(3));
					dailyComplaintsModel.setPm_no_ws_comp_resolved(rs.getDouble(4));
					dailyComplaintsModel.setPm_no_ws_comp_pending(rs.getDouble(5));
					dailyComplaintsModel.setPm_no_tw_comp_recv(rs.getDouble(6));
					dailyComplaintsModel.setPm_no_tw_comp_resolved(rs.getDouble(7));
					dailyComplaintsModel.setPm_no_tw_comp_pending(rs.getDouble(8));
					
					Pm_total_comp_recveived = Pm_total_comp_recveived + dailyComplaintsModel.getPm_no_ws_comp_recv();
					Pm_total_comp_resolved = Pm_total_comp_resolved + dailyComplaintsModel.getPm_no_ws_comp_resolved();
					Pm_total_comp_pending = Pm_total_comp_pending + dailyComplaintsModel.getPm_no_ws_comp_pending();
					Pm_total_tw_comp_recveived = Pm_total_tw_comp_recveived + dailyComplaintsModel.getPm_no_tw_comp_recv();
					Pm_total_tw_comp_resolved = Pm_total_tw_comp_resolved + dailyComplaintsModel.getPm_no_tw_comp_resolved();
					Pm_total_tw_comp_pending = Pm_total_tw_comp_pending + dailyComplaintsModel.getPm_no_tw_comp_pending();
				}
				
				dailyComplaintsModels.add(dailyComplaintsModel);
			}
			
			DailyComplaintsModel dailyComplaintsModel = new DailyComplaintsModel();
			
			dailyComplaintsModel.setTotal_comp_recveived((Math.round(total_comp_recveived*100.0)/100.0));
			dailyComplaintsModel.setTotal_comp_resolved((Math.round(total_comp_resolved*100.0)/100.0));
			dailyComplaintsModel.setTotal_comp_pending((Math.round(total_comp_pending*100.0)/100.0));
			dailyComplaintsModel.setTotal_tw_comp_recveived((Math.round(total_tw_comp_recveived*100.0)/100.0));
			dailyComplaintsModel.setTotal_tw_comp_resolved((Math.round(total_tw_comp_resolved*100.0)/100.0));
			dailyComplaintsModel.setTotal_tw_comp_pending((Math.round(total_tw_comp_pending*100.0)/100.0));
			
			dailyComplaintsModel.setPm_total_comp_recveived((Math.round(Pm_total_comp_recveived*100.0)/100.0));
			dailyComplaintsModel.setPm_total_comp_resolved((Math.round(Pm_total_comp_resolved*100.0)/100.0));
			dailyComplaintsModel.setPm_total_comp_pending((Math.round(Pm_total_comp_pending*100.0)/100.0));
			dailyComplaintsModel.setPm_total_tw_comp_recveived((Math.round(Pm_total_tw_comp_recveived*100.0)/100.0));
			dailyComplaintsModel.setPm_total_tw_comp_resolved((Math.round(Pm_total_tw_comp_resolved*100.0)/100.0));
			dailyComplaintsModel.setPm_total_tw_comp_pending((Math.round(Pm_total_tw_comp_pending*100.0)/100.0));
			
			dailyComplaintsModels.add(dailyComplaintsModel);
			

		} finally {
		
			try { // Closing Connection Object
				if (connection != null) {
		
					if (!connection.isClosed())
						connection.close();
		
					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyComplaintsReport");
					}
				}
			} catch (Exception e) {
		
				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyComplaintsReport"
							+ e.getMessage());
				}
		
			}
		
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaintsReport -- END");
		}
		
		return dailyComplaintsModels;
	
	}
	
	public LinkedList<DailyTankersModel> fetchDailyTankersReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankersReport -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyTankersModel> dailyTankersModels = new LinkedList<DailyTankersModel>();
		int i = 1;
		double total_tankers_engaged = 0.0D;
		double total_tankers_hired = 0.0D;
		double total_tanks_deployed = 0.0D;
		double total_population_served = 0.0D;
		double total_quantity_supplied = 0.0D;
		
		double Pm_total_tankers_engaged = 0.0D;
		double Pm_total_tankers_hired = 0.0D;
		double Pm_total_tanks_deployed = 0.0D;
		double Pm_total_population_served = 0.0D;
		double Pm_total_quantity_supplied = 0.0D;
		
		try {
			
			String ulb = "";
			
			if(listingModel.getUlbId() == null) { // No ULB ID Present
				
				if(listingModel.getDistrictId() == null) { // No District ID Present
					
					if(listingModel.getDivisionId() == null) { // No Division ID Present
						
						ulb = " = u.m_ulb_id";
						
					} else { // Division ID Present
						
						LinkedList<ULBModel> ulbModels = ulbService.fetchULBByDivisionId(listingModel
								.getDivisionId());
						
						String ulbs = "";

						for (ULBModel ulbModel : ulbModels) {

							ulbs = ulbs + ulbModel.getM_ulb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
						ulb = " in("+ulbs+")";
						
					}
				
				} else { // District ID Present
					
					LinkedList<ULBModel> ulbModels = ulbService.fetchULBByDistrictId(listingModel
							.getDistrictId());
					
					String ulbs = "";

					for (ULBModel ulbModel : ulbModels) {

						ulbs = ulbs + ulbModel.getM_ulb_id() + ",";
					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
					ulb = " in("+ulbs+")";
				}
				
			} else {//  ULB ID Present
				
				ulb = "="+String.valueOf(listingModel.getUlbId());
			}
			
			String query = "select u.m_ulb_id, " // ULBS ID (Table Containing 112 values) [Current Month]
					+ "u.ulb_name, " // ULBS Name (Table Containing 112 values) [Current Month]
					+ "ifnull(sum(dt.no_tank_engd_dept),0), " // No. Of Departmental Tanker Engaged [Current Month]
					+ "ifnull(sum(dt.no_tank_hire),0), " // No. Of Tanker Hired [Current Month]
					+ "ifnull(sum(dt.tot_no_tank_engd),0), " // Total Tanker Engaged [Current Month]
					+ "ifnull(sum(dt.no_pvc_rcc_tanks_depl),0), " // No. Of PVC/RCC Tanks Deployed [Current Month]
					+ "ifnull(sum(dt.popu_served_tanks_pvc_rcc),0), " // Population Served [Current Month]
					+ "ifnull(sum(dt.qty_mld),0), " // Quantity Supplied [Current Month]
					+ "u.latitude, " // Latitude Of the ULBs [Current Month]
					+ "u.longitude, "  // Longitude Of the ULBs [Current Month]
					+ "'current' "  // [Current Month]
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join daily_tankers dt on un.ulb_id = dt.ulb_id "
					+ "inner join  amrut_type at on at.Amrut_type_id = u.Amrut_type_id "
					+ "where u.Amrut_type_id = (case '"+amrutTypeId+"' when 'null' then u.Amrut_type_id else '"+amrutTypeId+"' end) "
					+ "and str_to_date(dt.date_of_report,'%Y-%m-%d') "
					+ "between str_to_date('"+startDate+"','%Y-%m-%d') "
					+ "and (case '"+endDate+"' "
					+ "when 'null' then str_to_date('"+startDate+"','%Y-%m-%d') "
					+ "else str_to_date('"+endDate+"','%Y-%m-%d') end) "
					+ "and u.m_ulb_id "+ulb+" "
					+ "and dt.IsApproved = 1 "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "UNION ALL "
					+ "select u.m_ulb_id, " // ULBS ID (Table Containing 112 values) [Previous Month]
					+ "u.ulb_name, " // ULBS Name (Table Containing 112 values) [Previous Month]
					+ "ifnull(sum(dt.no_tank_engd_dept),0), " // No. Of Departmental Tanker Engaged [Previous Month]
					+ "ifnull(sum(dt.no_tank_hire),0), " // No. Of Tanker Hired [Previous Month]
					+ "ifnull(sum(dt.tot_no_tank_engd),0), " // Total Tanker Engaged [Previous Month]
					+ "ifnull(sum(dt.no_pvc_rcc_tanks_depl),0), " // No. Of PVC/RCC Tanks Deployed [Previous Month]
					+ "ifnull(sum(dt.popu_served_tanks_pvc_rcc),0), " // Population Served [Previous Month]
					+ "ifnull(sum(dt.qty_mld),0), " // Quantity Supplied [Previous Month]
					+ "u.latitude, " // Latitude Of the ULBs [Previous Month]
					+ "u.longitude, "  // Longitude Of the ULBs [Previous Month]
					+ "'previous' " // [Previous Month]
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join daily_tankers dt on un.ulb_id = dt.ulb_id "
					+ "inner join  amrut_type at on at.Amrut_type_id = u.Amrut_type_id "
					+ "where u.Amrut_type_id = (case '"+amrutTypeId+"' when 'null' then u.Amrut_type_id else '"+amrutTypeId+"' end) "
					+ "and str_to_date(dt.date_of_report,'%Y-%m-%d') "
					+ "between (str_to_date('"+startDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) "
					+ "and (case '"+endDate+"' "
					+ "when 'null' then (str_to_date('"+startDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) "
					+ "else (str_to_date('"+endDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) end) "
					+ "and u.m_ulb_id "+ulb+" "
					+ "and dt.IsApproved = 1 "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "order by 2,11";
					
			pst = connection.prepareStatement(query);
			System.out.println("fetchDailyTankersReport ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				DailyTankersModel dailyTankersModel = new DailyTankersModel();
				
				dailyTankersModel.setSlNo(i++);
				ULBNameModel ulbNameModel = new ULBNameModel();
				
				ULBModel ulbModel = new ULBModel();
				ulbModel.setM_ulb_id(rs.getInt(1)); // ULBS ID (Table Containing 112 values)
				ulbModel.setUlb_name(rs.getString(2));  // ULBS Name (Table Containing 112 values)
				ulbModel.setLatitude(rs.getString(9));  // Latitude Of the ULBs
				ulbModel.setLongitude(rs.getString(10));  // Longitude Of the ULBs
				ulbNameModel.setUlbModel(ulbModel);
				dailyTankersModel.setUlbNameModel(ulbNameModel);
				dailyTankersModel.setType(rs.getString(11));
				
				if(dailyTankersModel.getType().equals("current")) {
					
					dailyTankersModel.setNo_tank_engd_dept(rs.getDouble(3)); // No. Of Departmental Tanker Engaged [Current Month]
					dailyTankersModel.setNo_tank_hire(rs.getDouble(4)); // No. Of Tanker Hired [Current Month]
					dailyTankersModel.setTot_no_tank_engd(rs.getDouble(5)); // Not Needed [Current Month]
					dailyTankersModel.setNo_pvc_rcc_tanks_depl(rs.getDouble(6)); // No. Of PVC/RCC Tanks Deployed [Current Month]
					dailyTankersModel.setPopu_served_tanks_pvc_rcc(rs.getDouble(7)); // Population Served [Current Month]
					dailyTankersModel.setQty_mld(rs.getDouble(8)); // Quantity Supplied [Current Month]
					
					total_tankers_engaged = total_tankers_engaged + dailyTankersModel.getNo_tank_engd_dept(); // [Current Month]
					total_tankers_hired = total_tankers_hired + dailyTankersModel.getNo_tank_hire(); // [Current Month]
					total_tanks_deployed = total_tanks_deployed + dailyTankersModel.getNo_pvc_rcc_tanks_depl(); // [Current Month]
					total_population_served = total_population_served + dailyTankersModel.getPopu_served_tanks_pvc_rcc(); // [Current Month]
					total_quantity_supplied = total_quantity_supplied + dailyTankersModel.getQty_mld(); // [Current Month]
					
				} else {
					
					dailyTankersModel.setPm_no_tank_engd_dept(rs.getDouble(3)); // No. Of Departmental Tanker Engaged [Previous Month]
					dailyTankersModel.setPm_no_tank_hire(rs.getDouble(4)); // No. Of Tanker Hired [Previous Month]
					dailyTankersModel.setPm_tot_no_tank_engd(rs.getDouble(5)); // Not Needed [Previous Month]
					dailyTankersModel.setPm_no_pvc_rcc_tanks_depl(rs.getDouble(6)); // No. Of PVC/RCC Tanks Deployed [Previous Month]
					dailyTankersModel.setPm_popu_served_tanks_pvc_rcc(rs.getDouble(7)); // Population Served [Previous Month]
					dailyTankersModel.setPm_qty_mld(rs.getDouble(8)); // Quantity Supplied [Previous Month]
					
					Pm_total_tankers_engaged = Pm_total_tankers_engaged + dailyTankersModel.getPm_no_tank_engd_dept(); // [Previous Month]
					Pm_total_tankers_hired = Pm_total_tankers_hired + dailyTankersModel.getPm_no_tank_hire(); // [Previous Month]
					Pm_total_tanks_deployed = Pm_total_tanks_deployed + dailyTankersModel.getPm_no_pvc_rcc_tanks_depl(); // [Previous Month]
					Pm_total_population_served = Pm_total_population_served + dailyTankersModel.getPm_popu_served_tanks_pvc_rcc(); // [Previous Month]
					Pm_total_quantity_supplied = Pm_total_quantity_supplied + dailyTankersModel.getPm_qty_mld(); // [Previous Month]
				}
				
				dailyTankersModels.add(dailyTankersModel);
			}
			
			DailyTankersModel dailyTankersModel = new DailyTankersModel();
			
			dailyTankersModel.setTotal_tankers_engaged(Math.round(total_tankers_engaged*100.0)/100.0);
			dailyTankersModel.setTotal_tankers_hired(Math.round(total_tankers_hired*100.0)/100.0);
			dailyTankersModel.setTotal_tanks_deployed(Math.round(total_tanks_deployed*100.0)/100.0);
			dailyTankersModel.setTotal_population_served(Math.round(total_population_served*100.0)/100.0);
			dailyTankersModel.setTotal_quantity_supplied(Math.round(total_quantity_supplied*100.0)/100.0);
			
			dailyTankersModel.setPm_tot_no_tank_engd(Math.round(Pm_total_tankers_engaged*100.0)/100.0);
			dailyTankersModel.setPm_total_tankers_hired(Math.round(Pm_total_tankers_hired*100.0)/100.0);
			dailyTankersModel.setPm_total_tanks_deployed(Math.round(Pm_total_tanks_deployed*100.0)/100.0);
			dailyTankersModel.setPm_total_population_served(Math.round(Pm_total_population_served*100.0)/100.0);
			dailyTankersModel.setPm_total_quantity_supplied(Math.round(Pm_total_quantity_supplied*100.0)/100.0);
			
			dailyTankersModels.add(dailyTankersModel);
			
		} finally {
	
			try { // Closing Connection Object
				if (connection != null) {
	
					if (!connection.isClosed())
						connection.close();
	
					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyTankersReport");
					}
				}
			} catch (Exception e) {
	
				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyTankersReport"
							+ e.getMessage());
				}
	
			}
	
		}
	
		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankersReport -- END");
		}
	
		return dailyTankersModels;

	}
	
	public LinkedList<DailyWaterTestingModel> fetchDailyWaterTestingReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTestingReport -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = new LinkedList<DailyWaterTestingModel>();
		int i = 1;
		double total_sample_tested = 0.0D;
		double total_sample_tested_frc = 0.0D;
		double total_sample_found = 0.0D;
		double total_sample_found_frc = 0.0D;
		double total_percentage_found = 0.0D;
		double total_percentage_found_frc = 0.0D;
		double Pm_total_sample_tested = 0.0D;
		double Pm_total_sample_tested_frc = 0.0D;
		double Pm_total_sample_found = 0.0D;
		double Pm_total_sample_found_frc = 0.0D;
		double Pm_total_percentage_found = 0.0D;
		double Pm_total_percentage_found_frc = 0.0D;
		
		try {
			

			String ulb = "";
			
			if(listingModel.getUlbId() == null) { // No ULB ID Present
				
				if(listingModel.getDistrictId() == null) { // No District ID Present
					
					if(listingModel.getDivisionId() == null) { // No Division ID Present
						
						ulb = " = u.m_ulb_id";
						
					} else { // Division ID Present
						
						LinkedList<ULBModel> ulbModels = ulbService.fetchULBByDivisionId(listingModel
								.getDivisionId());
						
						String ulbs = "";

						for (ULBModel ulbModel : ulbModels) {

							ulbs = ulbs + ulbModel.getM_ulb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
						ulb = " in("+ulbs+")";
						
					}
				
				} else { // District ID Present
					
					LinkedList<ULBModel> ulbModels = ulbService.fetchULBByDistrictId(listingModel
							.getDistrictId());
					
					String ulbs = "";

					for (ULBModel ulbModel : ulbModels) {

						ulbs = ulbs + ulbModel.getM_ulb_id() + ",";
					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
					ulb = " in("+ulbs+")";
				}
				
			} else {//  ULB ID Present
				
				ulb = "="+String.valueOf(listingModel.getUlbId());
			}
			
			String query = "select u.m_ulb_id, " // ULBS ID (Table Containing 112 values) [Current Month]
					+ "u.ulb_name, " // ULBS Name (Table Containing 112 values) [Current Month]
					+ "ifnull(sum(dwt.no_bact_samp_tested),0), " // Bacteria Sample Tested [Current Month]
					+ "ifnull(sum(dwt.no_bact_samp_found_ok),0), " // Bacteria Sample Found OK [Current Month]
					+ "ifnull(sum(dwt.no_other_samp_tested),0), " // Other Sample Tested [Current Month]
					+ "ifnull(sum(dwt.no_other_samp_found_ok),0), "// Other Sample Found OK [Current Month]
					+ "ifnull(sum(dwt.no_bact_samp_defet),0), "// Bacteria Sample Defet [Current Month]
					+ "ifnull(sum(dwt.no_other_samp_defet),0), "// Other Sample Defet [Current Month]
					+ "u.latitude, " // Latitude Of the ULBs [Current Month]
					+ "u.longitude, "  // Longitude Of the ULBs [Current Month]
					+ "ifnull((sum(dwt.no_bact_samp_found_ok)) * 100 /(sum(dwt.no_bact_samp_tested)),0), " // % Bacteriological [Current Month]
					+ "ifnull((sum(dwt.no_other_samp_found_ok)) * 100 /(sum(dwt.no_other_samp_tested)),0), " // % FRC [Current Month]
					+ "'current' "  // [Current Month]
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join daily_water_testing dwt on un.ulb_id = dwt.ulb_id "
					+ "inner join amrut_type at on at.Amrut_type_id = u.Amrut_type_id "
					+ "where u.Amrut_type_id = (case '"+amrutTypeId+"' when 'null' then u.Amrut_type_id else '"+amrutTypeId+"' end) "
					+ "and str_to_date(dwt.date_of_report,'%Y-%m-%d') "
					+ "between str_to_date('"+startDate+"','%Y-%m-%d') "
					+ "and (case '"+endDate+"' "
					+ "when 'null' then str_to_date('"+startDate+"','%Y-%m-%d') "
					+ "else str_to_date('"+endDate+"','%Y-%m-%d') end) "
					+ "and u.m_ulb_id "+ulb+" "
					+ "and dwt.IsApproved = 1 "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "UNION ALL "
					+ "select u.m_ulb_id, " // ULBS ID (Table Containing 112 values) [Previous Month]
					+ "u.ulb_name, " // ULBS Name (Table Containing 112 values) [Previous Month]
					+ "ifnull(sum(dwt.no_bact_samp_tested),0), " // Bacteria Sample Tested [Previous Month]
					+ "ifnull(sum(dwt.no_bact_samp_found_ok),0), " // Bacteria Sample Found OK [Previous Month]
					+ "ifnull(sum(dwt.no_other_samp_tested),0), " // Other Sample Tested [Previous Month]
					+ "ifnull(sum(dwt.no_other_samp_found_ok),0), "// Other Sample Found OK [Previous Month]
					+ "ifnull(sum(dwt.no_bact_samp_defet),0), "// Bacteria Sample Defet [Previous Month]
					+ "ifnull(sum(dwt.no_other_samp_defet),0), "// Other Sample Defet [Previous Month]
					+ "u.latitude, " // Latitude Of the ULBs [Previous Month]
					+ "u.longitude, "  // Longitude Of the ULBs [Previous Month]
					+ "ifnull((sum(dwt.no_bact_samp_found_ok)) * 100 /(sum(dwt.no_bact_samp_tested)),0), " // % Bacteriological [Previous Month]
					+ "ifnull((sum(dwt.no_other_samp_found_ok)) * 100 /(sum(dwt.no_other_samp_tested)),0), " // % FRC [Previous Month]
					+ "'previous' " // [Previous Month]
					+ "from ulbs u "
					+ "left outer join ulbs_name un on u.m_ulb_id = un.m_ulb_id "
					+ "left outer join daily_water_testing dwt on un.ulb_id = dwt.ulb_id "
					+ "inner join amrut_type at on at.Amrut_type_id = u.Amrut_type_id "
					+ "where u.Amrut_type_id = (case '"+amrutTypeId+"' when 'null' then u.Amrut_type_id else '"+amrutTypeId+"' end) "
					+ "and str_to_date(dwt.date_of_report,'%Y-%m-%d') "
					+ "between (str_to_date('"+startDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) "
					+ "and (case '"+endDate+"' "
					+ "when 'null' then (str_to_date('"+startDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) "
					+ "else (str_to_date('"+endDate+"','%Y-%m-%d')-INTERVAL 1 MONTH) end) "
					+ "and u.m_ulb_id "+ulb+" "
					+ "and dwt.IsApproved = 1 "
					+ "group by u.m_ulb_id,u.ulb_name "
					+ "order by 2,13";
			
			pst = connection.prepareStatement(query);
			System.out.println("fetchDailyWaterTestingReport ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				DailyWaterTestingModel dailyWaterTestingModel = new DailyWaterTestingModel();
				
				dailyWaterTestingModel.setSlNo(i++);
				ULBNameModel ulbNameModel = new ULBNameModel();
				
				ULBModel ulbModel = new ULBModel();
				ulbModel.setM_ulb_id(rs.getInt(1)); // ULBS ID (Table Containing 112 values)
				ulbModel.setUlb_name(rs.getString(2));  // ULBS Name (Table Containing 112 values)
				ulbModel.setLatitude(rs.getString(9));  // Latitude Of the ULBs
				ulbModel.setLongitude(rs.getString(10));  // Longitude Of the ULBs
				ulbNameModel.setUlbModel(ulbModel);
				dailyWaterTestingModel.setUlbNameModel(ulbNameModel);
				
				dailyWaterTestingModel.setType(rs.getString(13));
				
				if(dailyWaterTestingModel.getType().equals("current")) {
					
					dailyWaterTestingModel.setNo_bact_samp_tested(rs.getDouble(3)); // Bacteria Sample Tested [Current Month]
					dailyWaterTestingModel.setNo_bact_samp_found_ok(rs.getDouble(4)); // Bacteria Sample Found OK [Current Month]
					dailyWaterTestingModel.setNo_other_samp_tested(rs.getDouble(5)); // Other Sample Tested [Current Month]
					dailyWaterTestingModel.setNo_other_samp_found_ok(rs.getDouble(6)); // Other Sample Found OK [Current Month]
					dailyWaterTestingModel.setNo_bact_samp_defet(rs.getDouble(7));
					dailyWaterTestingModel.setNo_other_samp_defet(rs.getDouble(8));
					dailyWaterTestingModel.setPercentage_found(Math.round(rs.getDouble(11)*100.0)/100.0); // % Bacteriological [Current Month]
					dailyWaterTestingModel.setPercentage_found_frc(Math.round(rs.getDouble(12)*100.0)/100.0); // % FRC [Current Month]
					
					total_sample_tested = total_sample_tested + dailyWaterTestingModel.getNo_bact_samp_tested(); // Total Bacteria Sample Tested [Current Month]
					total_sample_tested_frc = total_sample_tested_frc + dailyWaterTestingModel.getNo_other_samp_tested(); // Total Other Sample Tested [Current Month]
					total_sample_found = total_sample_found + dailyWaterTestingModel.getNo_bact_samp_found_ok(); // Total Bacteria Sample Found OK [Current Month]
					total_sample_found_frc = total_sample_found_frc + dailyWaterTestingModel.getNo_other_samp_found_ok(); // Total Other Sample Found OK [Current Month]
					
				} else {
					
					dailyWaterTestingModel.setPm_no_bact_samp_tested(rs.getDouble(3));
					dailyWaterTestingModel.setPm_no_bact_samp_found_ok(rs.getDouble(4));
					dailyWaterTestingModel.setPm_no_other_samp_tested(rs.getDouble(5));
					dailyWaterTestingModel.setPm_no_other_samp_found_ok(rs.getDouble(6));
					dailyWaterTestingModel.setPm_no_bact_samp_defet(rs.getDouble(7));
					dailyWaterTestingModel.setPm_no_other_samp_defet(rs.getDouble(8));
					dailyWaterTestingModel.setPm_percentage_found(Math.round(rs.getDouble(11)*100.0)/100.0);
					dailyWaterTestingModel.setPm_percentage_found_frc(Math.round(rs.getDouble(12)*100.0)/100.0);
					
					Pm_total_sample_tested = Pm_total_sample_tested + dailyWaterTestingModel.getPm_no_bact_samp_tested();
					Pm_total_sample_tested_frc = Pm_total_sample_tested_frc + dailyWaterTestingModel.getPm_no_other_samp_tested();
					Pm_total_sample_found = Pm_total_sample_found + dailyWaterTestingModel.getPm_no_bact_samp_found_ok();
					Pm_total_sample_found_frc = Pm_total_sample_found_frc + dailyWaterTestingModel.getPm_no_other_samp_found_ok();
				}
				
				
				
				dailyWaterTestingModels.add(dailyWaterTestingModel);
			}
			
			DailyWaterTestingModel dailyWaterTestingModel = new DailyWaterTestingModel();
			
			total_percentage_found = (total_sample_found * 100) / total_sample_tested;
			total_percentage_found_frc = (total_sample_found_frc * 100) / total_sample_tested_frc;
			
			dailyWaterTestingModel.setTotal_sample_tested(Math.round(total_sample_tested*100.0)/100.0);
			dailyWaterTestingModel.setTotal_sample_tested_frc(Math.round(total_sample_tested_frc*100.0)/100.0);
			dailyWaterTestingModel.setTotal_sample_found(Math.round(total_sample_found*100.0)/100.0);
			dailyWaterTestingModel.setTotal_sample_found_frc(Math.round(total_sample_found_frc*100.0)/100.0);
			dailyWaterTestingModel.setTotal_percentage_found(Math.round(total_percentage_found*100.0)/100.0);
			dailyWaterTestingModel.setTotal_percentage_found_frc(Math.round(total_percentage_found_frc*100.0)/100.0);
			
			Pm_total_percentage_found = (Pm_total_sample_found * 100) / Pm_total_sample_tested;
			Pm_total_percentage_found_frc = (Pm_total_sample_found_frc * 100) / Pm_total_sample_tested_frc;
			
			dailyWaterTestingModel.setPm_total_sample_tested(Math.round(Pm_total_sample_tested*100.0)/100.0);
			dailyWaterTestingModel.setPm_total_sample_tested_frc(Math.round(Pm_total_sample_tested_frc*100.0)/100.0);
			dailyWaterTestingModel.setPm_total_sample_found(Math.round(Pm_total_sample_found*100.0)/100.0);
			dailyWaterTestingModel.setPm_total_sample_found_frc(Math.round(Pm_total_sample_found_frc*100.0)/100.0);
			dailyWaterTestingModel.setPm_total_percentage_found(Math.round(Pm_total_percentage_found*100.0)/100.0);
			dailyWaterTestingModel.setPm_total_percentage_found_frc(Math.round(Pm_total_percentage_found_frc*100.0)/100.0);
			
			dailyWaterTestingModels.add(dailyWaterTestingModel);

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterTestingReport");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterTestingReport"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTestingReport -- END");
		}

		return dailyWaterTestingModels;

	}
}
