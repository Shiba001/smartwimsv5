package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class DailyWaterSupplyDao extends DeleteDao {

	private static Logger logger = Logger.getLogger(DailyWaterSupplyDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbNameService;
	
	public int addDailyWaterSupply(DailyWaterSupplyModel dailyWaterSupplyModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterSupply -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "insert into daily_water_supply(ulb_id,date_of_report,tot_demand,qlty_supply,pop_served,Freq_supply,surplus_deficit,lpcd,remarks,user_id,entry_date,IsApproved) values(?,?,?,?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?) ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, dailyWaterSupplyModel.getUlbId());
			pst.setString(2, dailyWaterSupplyModel.getDate_of_report());
			pst.setDouble(3, dailyWaterSupplyModel.getTot_demand());
			pst.setDouble(4, dailyWaterSupplyModel.getQlty_supply());
			pst.setDouble(5, dailyWaterSupplyModel.getPop_served());
			pst.setDouble(6, dailyWaterSupplyModel.getFreq_supply());
			pst.setDouble(7, dailyWaterSupplyModel.getSurplus_deficit());
			pst.setDouble(8, dailyWaterSupplyModel.getRate_of_supply());
			pst.setString(9, dailyWaterSupplyModel.getRemarks());
			pst.setInt(10, dailyWaterSupplyModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(11, Util.getCurrentDateTime());
			pst.setInt(12, dailyWaterSupplyModel.getIsApproved());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addDailyWaterSupply");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addDailyWaterSupply"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterSupply -- END");
		}

		return result;

	}

	public LinkedList<DailyWaterSupplyModel> fetchDailyWaterSupply(
			LoginUsersModel loginUsersModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupply -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = new LinkedList<DailyWaterSupplyModel>();
		try {

			String query = "";
			if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB
																				// User

				query = "select dws.ws_id,dws.ulb_id,dws.date_of_report,dws.tot_demand,dws.qlty_supply,dws.pop_served,dws.Freq_supply,dws.surplus_deficit,dws.lpcd,dws.remarks,dws.user_id,dws.entry_date,dws.modify_date,dws.IsApproved,un.ulb_Name from daily_water_supply dws,ulbs_name un where dws.ulb_id = un.ulb_id and date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dws.ulb_id ="
						+ loginUsersModel.getUlbNameModel().getUlb_id()
						+ " order by dws.entry_date desc";

			} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District
																						// User

				LinkedList upperLevels = districtService
						.fetchUpperLevelsByULBId(loginUsersModel
								.getUlbNameModel().getUlb_id());
				int districtId = (Integer) upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService
						.fetchULBNameByDistrictId(districtId);

				String ulbs = "";
				for (ULBNameModel ulbNameModel : ulbNameModels) {

					ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
				}

				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

				query = "select dws.ws_id,dws.ulb_id,dws.date_of_report,dws.tot_demand,dws.qlty_supply,dws.pop_served,dws.Freq_supply,dws.surplus_deficit,dws.lpcd,dws.remarks,dws.user_id,dws.entry_date,dws.modify_date,dws.IsApproved,un.ulb_Name from daily_water_supply dws,ulbs_name un where dws.ulb_id = un.ulb_id and dws.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dws.ulb_id in ("
						+ ulbs + ")  order by dws.entry_date desc";

			} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division
																						// User

				LinkedList upperLevels = districtService
						.fetchUpperLevelsByULBId(loginUsersModel
								.getUlbNameModel().getUlb_id());
				int divisionId = (Integer) upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService
						.fetchDistrictByDivisionId(divisionId);

				String ulbs = "";
				for (PhDistrictModel phDistrictModel : districtModels) {

					LinkedList<ULBNameModel> ulbNameModels = ulbNameService
							.fetchULBNameByDistrictId(phDistrictModel
									.getDist_id());

					for (ULBNameModel ulbNameModel : ulbNameModels) {

						ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
					}

				}

				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

				query = "select dws.ws_id,dws.ulb_id,dws.date_of_report,dws.tot_demand,dws.qlty_supply,dws.pop_served,dws.Freq_supply,dws.surplus_deficit,dws.lpcd,dws.remarks,dws.user_id,dws.entry_date,dws.modify_date,dws.IsApproved,un.ulb_Name from daily_water_supply dws,ulbs_name un where dws.ulb_id = un.ulb_id and dws.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dws.ulb_id in ("
						+ ulbs + ")  order by dws.entry_date desc";

			} else { // Others All (State, PHEO, Circle, H&UD)

				query = "select dws.ws_id,dws.ulb_id,dws.date_of_report,dws.tot_demand,dws.qlty_supply,dws.pop_served,dws.Freq_supply,dws.surplus_deficit,dws.lpcd,dws.remarks,dws.user_id,dws.entry_date,dws.modify_date,dws.IsApproved,un.ulb_Name from daily_water_supply dws,ulbs_name un where dws.ulb_id = un.ulb_id and dws.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') order by dws.entry_date desc";
			}

			pst = connection.prepareStatement(query);
			pst.setString(1, Util.getYesterdayDateString());
			ResultSet rs = pst.executeQuery();
			
			while (rs.next()) {

				DailyWaterSupplyModel dailyWaterSupplyModel = new DailyWaterSupplyModel();
				
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(15));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(11));

				dailyWaterSupplyModel.setWs_id(rs.getInt(1));
				dailyWaterSupplyModel.setUlbNameModel(ulbNameModel);
				dailyWaterSupplyModel.setDate_of_report(rs.getString(3));
				dailyWaterSupplyModel.setTot_demand(rs.getDouble(4));
				dailyWaterSupplyModel.setQlty_supply(rs.getDouble(5));
				dailyWaterSupplyModel.setPop_served(rs.getDouble(6));
				dailyWaterSupplyModel.setFreq_supply(rs.getDouble(7));
				dailyWaterSupplyModel.setSurplus_deficit(rs.getDouble(8));
				dailyWaterSupplyModel.setRate_of_supply(rs.getDouble(9));
				dailyWaterSupplyModel.setRemarks(rs.getString(10));
				dailyWaterSupplyModel.setLoginUsersModel(loginUsersModel2);
				dailyWaterSupplyModel.setEntry_date(rs.getString(12));
				dailyWaterSupplyModel.setModify_date(rs.getString(13));
				dailyWaterSupplyModel.setIsApproved(rs.getInt(14));

				dailyWaterSupplyModels.add(dailyWaterSupplyModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterSupply");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterSupply"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupply -- END");
		}

		return dailyWaterSupplyModels;

	}

	public DailyWaterSupplyModel fetchDailyWaterSupplyById(int ws_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		
		DailyWaterSupplyModel dailyWaterSupplyModel = new DailyWaterSupplyModel();
		try {
			String query = "select dws.ws_id,dws.ulb_id,dws.date_of_report,dws.tot_demand,dws.qlty_supply,dws.pop_served,dws.Freq_supply,dws.surplus_deficit,dws.lpcd,dws.remarks,dws.user_id,dws.entry_date,dws.modify_date,dws.IsApproved,un.ulb_Name from daily_water_supply dws,ulbs_name un where dws.ulb_id = un.ulb_id and dws.ws_id =? order by dws.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, ws_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(15));

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(11));

				dailyWaterSupplyModel.setWs_id(rs.getInt(1));
				dailyWaterSupplyModel.setUlbNameModel(ulbNameModel);
				dailyWaterSupplyModel.setDate_of_report(rs.getString(3));
				dailyWaterSupplyModel.setTot_demand(rs.getDouble(4));
				dailyWaterSupplyModel.setQlty_supply(rs.getDouble(5));
				dailyWaterSupplyModel.setPop_served(rs.getDouble(6));
				dailyWaterSupplyModel.setFreq_supply(rs.getDouble(7));
				dailyWaterSupplyModel.setSurplus_deficit(rs.getDouble(8));
				dailyWaterSupplyModel.setRate_of_supply(rs.getDouble(9));
				dailyWaterSupplyModel.setRemarks(rs.getString(10));
				dailyWaterSupplyModel.setLoginUsersModel(loginUsersModel);
				dailyWaterSupplyModel.setEntry_date(rs.getString(12));
				dailyWaterSupplyModel.setModify_date(rs.getString(13));
				dailyWaterSupplyModel.setIsApproved(rs.getInt(14));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterSupplyById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterSupplyById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyById -- END");
		}

		return dailyWaterSupplyModel;

	}

	public int updateDailyWaterSupply(
			DailyWaterSupplyModel dailyWaterSupplyModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterSupply -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		

		try {
			String query = "update daily_water_supply set ulb_id=?,date_of_report=?,tot_demand=?,qlty_supply=?,pop_served=?,Freq_supply=?,surplus_deficit=?,lpcd=?,remarks=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s') where ws_id=? ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, dailyWaterSupplyModel.getUlbId());
			// pst.setInt(1, 24);
			pst.setString(2, dailyWaterSupplyModel.getDate_of_report());
			pst.setDouble(3, dailyWaterSupplyModel.getTot_demand());
			pst.setDouble(4, dailyWaterSupplyModel.getQlty_supply());
			pst.setDouble(5, dailyWaterSupplyModel.getPop_served());
			pst.setDouble(6, dailyWaterSupplyModel.getFreq_supply());
			pst.setDouble(7, dailyWaterSupplyModel.getSurplus_deficit());
			pst.setDouble(8, dailyWaterSupplyModel.getRate_of_supply());
			pst.setString(9, dailyWaterSupplyModel.getRemarks());
			pst.setInt(10, dailyWaterSupplyModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(11, Util.getCurrentDateTime());
			pst.setInt(12, dailyWaterSupplyModel.getWs_id());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updateDailyWaterSupply");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updateDailyWaterSupply"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterSupply -- END");
		}

		return result;

	}

	public LinkedList<DailyWaterSupplyModel> fetchAJAXDailyWaterSupply(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = new LinkedList<DailyWaterSupplyModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select dws.ws_id,dws.ulb_id,dws.date_of_report,dws.tot_demand,dws.qlty_supply,dws.pop_served,dws.Freq_supply,dws.surplus_deficit,dws.lpcd,dws.remarks,dws.user_id,dws.entry_date,dws.modify_date,dws.IsApproved,un.ulb_Name from daily_water_supply dws,ulbs_name un  where dws.ulb_id = un.ulb_id and dws.date_of_report =(case '"
						+ listingModel.getDate()
						+ "' when 'null' then dws.date_of_report else str_to_date('"
						+ listingModel.getDate()
						+ "','%Y-%m-%d %H:%i:%s') end) order by dws.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select dws.ws_id,dws.ulb_id,dws.date_of_report,dws.tot_demand,dws.qlty_supply,dws.pop_served,dws.Freq_supply,dws.surplus_deficit,dws.lpcd,dws.remarks,dws.user_id,dws.entry_date,dws.modify_date,dws.IsApproved,un.ulb_Name from daily_water_supply dws,ulbs_name un  where dws.ulb_id = un.ulb_id and dws.date_of_report =(case '"
							+ listingModel.getDate()
							+ "' when 'null' then dws.date_of_report else str_to_date('"
							+ listingModel.getDate()
							+ "','%Y-%m-%d %H:%i:%s') end) and dws.ulb_id in ("
							+ ulbs + ") order by dws.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select dws.ws_id,dws.ulb_id,dws.date_of_report,dws.tot_demand,dws.qlty_supply,dws.pop_served,dws.Freq_supply,dws.surplus_deficit,dws.lpcd,dws.remarks,dws.user_id,dws.entry_date,dws.modify_date,dws.IsApproved,un.ulb_Name from daily_water_supply dws,ulbs_name un  where dws.ulb_id = un.ulb_id and dws.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dws.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dws.ulb_id in ("
								+ ulbs + ") order by dws.entry_date desc";

					} else { // Only For ULB

						query = "select dws.ws_id,dws.ulb_id,dws.date_of_report,dws.tot_demand,dws.qlty_supply,dws.pop_served,dws.Freq_supply,dws.surplus_deficit,dws.lpcd,dws.remarks,dws.user_id,dws.entry_date,dws.modify_date,dws.IsApproved,un.ulb_Name from daily_water_supply dws,ulbs_name un  where dws.ulb_id = un.ulb_id and dws.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dws.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dws.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by dws.entry_date desc";
					}

				}

			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyWaterSupplyModel dailyWaterSupplyModel = new DailyWaterSupplyModel();
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(15));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(11));

				dailyWaterSupplyModel.setWs_id(rs.getInt(1));
				dailyWaterSupplyModel.setUlbNameModel(ulbNameModel);
				dailyWaterSupplyModel.setDate_of_report(rs.getString(3));
				dailyWaterSupplyModel.setTot_demand(rs.getDouble(4));
				dailyWaterSupplyModel.setQlty_supply(rs.getDouble(5));
				dailyWaterSupplyModel.setPop_served(rs.getDouble(6));
				dailyWaterSupplyModel.setFreq_supply(rs.getDouble(7));
				dailyWaterSupplyModel.setSurplus_deficit(rs.getDouble(8));
				dailyWaterSupplyModel.setRate_of_supply(rs.getDouble(9));
				dailyWaterSupplyModel.setRemarks(rs.getString(10));
				dailyWaterSupplyModel.setLoginUsersModel(loginUsersModel2);
				dailyWaterSupplyModel.setEntry_date(rs.getString(12));
				dailyWaterSupplyModel.setModify_date(rs.getString(13));
				dailyWaterSupplyModel.setIsApproved(rs.getInt(14));

				dailyWaterSupplyModels.add(dailyWaterSupplyModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXDailyWaterSupply");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXDailyWaterSupply"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- END");
		}

		return dailyWaterSupplyModels;

	}

}
