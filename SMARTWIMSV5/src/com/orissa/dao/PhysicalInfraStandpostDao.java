package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.PhysicalInfraStandpostsModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class PhysicalInfraStandpostDao extends DeleteDao {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraStandpostDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private ULBNameService ulbNameService;

	public int addPhysicalInfraStandpost(
			PhysicalInfraStandpostsModel physicalInfraStandpostsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraStandposts -- START");
		}


		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "insert into physical_infra_standposts(ulb_id,date_of_report,no_exst_stanposts,no_standposts_work_cond,prop_standposts_work_cond,user_id,entry_date,next_maintenance_date) values(?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?) ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, physicalInfraStandpostsModel.getUlbId());
			pst.setString(2, physicalInfraStandpostsModel.getDate_of_report()); // To be Changed
			pst.setDouble(3,
					physicalInfraStandpostsModel.getNo_exst_stanposts());
			pst.setDouble(4,
					physicalInfraStandpostsModel.getNo_standposts_work_cond());
			pst.setDouble(5,
					physicalInfraStandpostsModel.getProp_standposts_work_cond());
			pst.setInt(6, physicalInfraStandpostsModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(7, Util.getCurrentDateTime());
			pst.setString(8, physicalInfraStandpostsModel.getDueDate());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addPhysicalInfraStandposts");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addPhysicalInfraStandposts"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraStandposts -- END");
		}

		return result;

	}

	
	public LinkedList<PhysicalInfraStandpostsModel> fetchPhysicalInfraStandposts(LoginUsersModel loginUsersModel)
	throws Exception {

	if (logger.isInfoEnabled()) {
		logger.info("fetchPhysicalInfraStandposts -- START");
	}
	
	Connection connection = dbConnection.Connect();
	PreparedStatement pst = null;
	LinkedList<PhysicalInfraStandpostsModel> physicalInfraStandpostsModels = new LinkedList<PhysicalInfraStandpostsModel>();
	try {
		String query = "";
		if(loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
			
			query = "select pis.phyInfra_std_post_id,pis.ulb_id,pis.date_of_report,pis.no_exst_stanposts,pis.no_standposts_work_cond,pis.prop_standposts_work_cond,pis.user_id,pis.entry_date,pis.modify_date,un.ulb_Name,pis.next_maintenance_date from physical_infra_standposts pis ,ulbs_name un where pis.ulb_id = un.ulb_id  and pis.ulb_id ="+loginUsersModel.getUlbNameModel().getUlb_id()+" order by pis.entry_date desc";
		
		} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District User
			
			LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
			int districtId = (Integer)upperLevels.get(2);
			LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(districtId);
			
			String ulbs = "";
			for(ULBNameModel ulbNameModel:ulbNameModels) {
				
				ulbs = ulbs +ulbNameModel.getUlb_id()+",";
			}
			
			ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
			
			query = "select pis.phyInfra_std_post_id,pis.ulb_id,pis.date_of_report,pis.no_exst_stanposts,pis.no_standposts_work_cond,pis.prop_standposts_work_cond,pis.user_id,pis.entry_date,pis.modify_date,un.ulb_Name,pis.next_maintenance_date from physical_infra_standposts pis ,ulbs_name un where pis.ulb_id = un.ulb_id  and pis.ulb_id in ("+ulbs+")  order by pis.entry_date desc";
		
		} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division User
			
			LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
			int divisionId = (Integer)upperLevels.get(3);
			LinkedList<PhDistrictModel> districtModels = districtService.fetchDistrictByDivisionId(divisionId);
			
			String ulbs = "";
			for(PhDistrictModel phDistrictModel:districtModels) {
				
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(phDistrictModel.getDist_id());
				
				for(ULBNameModel ulbNameModel:ulbNameModels) {
					
					ulbs = ulbs +ulbNameModel.getUlb_id()+",";
				}
				
			}
			
			ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
			
			query = "select pis.phyInfra_std_post_id,pis.ulb_id,pis.date_of_report,pis.no_exst_stanposts,pis.no_standposts_work_cond,pis.prop_standposts_work_cond,pis.user_id,pis.entry_date,pis.modify_date,un.ulb_Name,pis.next_maintenance_date from physical_infra_standposts pis ,ulbs_name un where pis.ulb_id = un.ulb_id  and pis.ulb_id in ("+ulbs+")  order by pis.entry_date desc";
		
		} else { // Others All (State, PHEO, Circle, H&UD)
			
			query = "select pis.phyInfra_std_post_id,pis.ulb_id,pis.date_of_report,pis.no_exst_stanposts,pis.no_standposts_work_cond,pis.prop_standposts_work_cond,pis.user_id,pis.entry_date,pis.modify_date,un.ulb_Name,pis.next_maintenance_date from physical_infra_standposts pis ,ulbs_name un where pis.ulb_id = un.ulb_id  order by pis.entry_date desc";
		}
	pst = connection.prepareStatement(query);
	ResultSet rs = pst.executeQuery();
	while (rs.next()) {

		PhysicalInfraStandpostsModel physicalInfraStandpostsModel = new PhysicalInfraStandpostsModel();
		ULBNameModel ulbNameModel = new ULBNameModel();
		ulbNameModel.setUlb_id(rs.getInt(2));
		ulbNameModel.setUlb_Name(rs.getString(10));
		LoginUsersModel loginUsersModel2 = new LoginUsersModel();
		loginUsersModel2.setUser_id(rs.getInt(7));

		physicalInfraStandpostsModel.setPhyInfra_std_post_id(rs
				.getInt(1));
		physicalInfraStandpostsModel.setUlbNameModel(ulbNameModel);
		physicalInfraStandpostsModel.setDate_of_report(rs.getString(3));
		physicalInfraStandpostsModel.setNo_exst_stanposts(rs
				.getDouble(4));
		physicalInfraStandpostsModel.setNo_standposts_work_cond(rs
				.getDouble(5));
		physicalInfraStandpostsModel
				.setLoginUsersModel(loginUsersModel2);
		physicalInfraStandpostsModel.setProp_standposts_work_cond(rs
				.getDouble(6));
		physicalInfraStandpostsModel.setEntry_date(rs.getString(8));
		physicalInfraStandpostsModel.setModify_date(rs.getString(9));
		physicalInfraStandpostsModel.setDueDate(rs.getString(11));
		
		physicalInfraStandpostsModels.add(physicalInfraStandpostsModel);
	}

} finally {

	try { // Closing Connection Object
		if (connection != null) {

			if (!connection.isClosed())
				connection.close();

			if (logger.isDebugEnabled()) {
				logger.debug("Connection Closed for fetchPhysicalInfraStandposts");
			}
		}
	} catch (Exception e) {

		if (logger.isDebugEnabled()) {
			logger.debug("Connection not closed for fetchPhysicalInfraStandposts"
					+ e.getMessage());
		}

	}

}

if (logger.isInfoEnabled()) {
	logger.info("fetchPhysicalInfraStandposts -- END");
}

return physicalInfraStandpostsModels;

}
	

	public PhysicalInfraStandpostsModel fetchPhysicalInfraStandpostsById(
			int phyInfra_std_post_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStandpostsById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		PhysicalInfraStandpostsModel physicalInfraStandpostsModel = new PhysicalInfraStandpostsModel();
		try {
			String query = "select pis.phyInfra_std_post_id,pis.ulb_id,pis.date_of_report,pis.no_exst_stanposts,pis.no_standposts_work_cond,pis.prop_standposts_work_cond,pis.user_id,pis.entry_date,pis.modify_date,un.ulb_Name,pis.next_maintenance_date from physical_infra_standposts pis,ulbs_name un where pis.ulb_id = un.ulb_id and pis.phyInfra_std_post_id =? order by pis.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, phyInfra_std_post_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(7));
				ulbNameModel.setUlb_Name(rs.getString(10));
				physicalInfraStandpostsModel.setPhyInfra_std_post_id(rs
						.getInt(1));
				physicalInfraStandpostsModel.setUlbNameModel(ulbNameModel);
				physicalInfraStandpostsModel.setDate_of_report(rs.getString(3));
				physicalInfraStandpostsModel.setNo_exst_stanposts(rs
						.getDouble(4));
				physicalInfraStandpostsModel.setNo_standposts_work_cond(rs
						.getDouble(5));
				physicalInfraStandpostsModel
						.setLoginUsersModel(loginUsersModel);
				physicalInfraStandpostsModel.setProp_standposts_work_cond(rs
						.getDouble(6));
				physicalInfraStandpostsModel.setEntry_date(rs.getString(8));
				physicalInfraStandpostsModel.setModify_date(rs.getString(9));
				physicalInfraStandpostsModel.setDueDate(rs.getString(11));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchPhysicalInfraStandpostsById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchPhysicalInfraStandpostsById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStandpostsById -- END");
		}

		return physicalInfraStandpostsModel;

	}

	public int updatePhysicalInfraStandposts(
			PhysicalInfraStandpostsModel physicalInfraStandpostsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraStandposts -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update physical_infra_standposts set ulb_id=?,date_of_report=?,no_exst_stanposts=?,no_standposts_work_cond=?,prop_standposts_work_cond=?,user_id=?,modify_date=?,next_maintenance_date=? where phyInfra_std_post_id=? ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, physicalInfraStandpostsModel.getUlbId());
			pst.setString(2, physicalInfraStandpostsModel.getDate_of_report()); 
			pst.setDouble(3,
					physicalInfraStandpostsModel.getNo_exst_stanposts());
			pst.setDouble(4,
					physicalInfraStandpostsModel.getNo_standposts_work_cond());
			pst.setDouble(5,
					physicalInfraStandpostsModel.getProp_standposts_work_cond());
			pst.setInt(6, physicalInfraStandpostsModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(7, Util.getCurrentDateTime());
			pst.setString(8, physicalInfraStandpostsModel.getDueDate());
			pst.setInt(9,
					physicalInfraStandpostsModel.getPhyInfra_std_post_id());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updatePhysicalInfraStandposts");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updatePhysicalInfraStandposts"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraStandposts -- END");
		}

		return result;

	}
	
	
	public LinkedList<PhysicalInfraStandpostsModel> fetchAJAXPhysicalInfraStandpost(ListingModel listingModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStandpost -- START");
		}
		
		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhysicalInfraStandpostsModel> physicalInfraStandpostsModels = new LinkedList<PhysicalInfraStandpostsModel>();
		try {
			
			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select pis.phyInfra_std_post_id,pis.ulb_id,pis.date_of_report,pis.no_exst_stanposts,pis.no_standposts_work_cond,pis.prop_standposts_work_cond,pis.user_id,pis.entry_date,pis.modify_date,un.ulb_Name,pis.next_maintenance_date from physical_infra_standposts pis ,ulbs_name un where pis.ulb_id = un.ulb_id  order by pis.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select pis.phyInfra_std_post_id,pis.ulb_id,pis.date_of_report,pis.no_exst_stanposts,pis.no_standposts_work_cond,pis.prop_standposts_work_cond,pis.user_id,pis.entry_date,pis.modify_date,un.ulb_Name,pis.next_maintenance_date from physical_infra_standposts pis ,ulbs_name un where pis.ulb_id = un.ulb_id  and pis.ulb_id in ("
							+ ulbs + ") order by pis.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select pis.phyInfra_std_post_id,pis.ulb_id,pis.date_of_report,pis.no_exst_stanposts,pis.no_standposts_work_cond,pis.prop_standposts_work_cond,pis.user_id,pis.entry_date,pis.modify_date,un.ulb_Name,pis.next_maintenance_date from physical_infra_standposts pis ,ulbs_name un where pis.ulb_id = un.ulb_id  and pis.ulb_id in ("
								+ ulbs + ") order by pis.entry_date desc";

					} else { // Only For ULB

						query = "select pis.phyInfra_std_post_id,pis.ulb_id,pis.date_of_report,pis.no_exst_stanposts,pis.no_standposts_work_cond,pis.prop_standposts_work_cond,pis.user_id,pis.entry_date,pis.modify_date,un.ulb_Name,pis.next_maintenance_date from physical_infra_standposts pis ,ulbs_name un where pis.ulb_id = un.ulb_id  and pis.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by pis.entry_date desc";
					}

				}

			}
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhysicalInfraStandpostsModel physicalInfraStandpostsModel = new PhysicalInfraStandpostsModel();
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));
				ulbNameModel.setUlb_Name(rs.getString(10));
				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(7));

				physicalInfraStandpostsModel.setPhyInfra_std_post_id(rs
						.getInt(1));
				physicalInfraStandpostsModel.setUlbNameModel(ulbNameModel);
				physicalInfraStandpostsModel.setDate_of_report(rs.getString(3));
				physicalInfraStandpostsModel.setNo_exst_stanposts(rs
						.getDouble(4));
				physicalInfraStandpostsModel.setNo_standposts_work_cond(rs
						.getDouble(5));
				physicalInfraStandpostsModel
						.setLoginUsersModel(loginUsersModel2);
				physicalInfraStandpostsModel.setProp_standposts_work_cond(rs
						.getDouble(6));
				physicalInfraStandpostsModel.setEntry_date(rs.getString(8));
				physicalInfraStandpostsModel.setModify_date(rs.getString(9));
				physicalInfraStandpostsModel.setDueDate(rs.getString(11));
				
				physicalInfraStandpostsModels.add(physicalInfraStandpostsModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXPhysicalInfraStandpost");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXPhysicalInfraStandpost"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStandpost -- END");
		}

		return physicalInfraStandpostsModels;

	}

}
