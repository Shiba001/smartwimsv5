package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;

@Repository
public class DeleteDao {

	private static Logger logger = Logger.getLogger(DeleteDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	public int commonDelete(String tableName, String PK, int id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("commonDelete -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "delete from " + tableName + " where " + PK + "=?";
			pst = connection.prepareStatement(query);
			pst.setInt(1, id);
			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for commonDelete");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for commonDelete"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("commonDelete -- END");
		}

		return result;

	}
	
	public int isApprovedUpdate(String tableName, String PK, int id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update " + tableName + " set IsApproved = 1 where " + PK + "=?";
			pst = connection.prepareStatement(query);
			pst.setInt(1, id);
			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for isApprovedUpdate");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for isApprovedUpdate"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return result;

	}
	
	public int entryCheck(String tableName, String date_of_report, int ulbId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("entryCheck -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "select * from " + tableName + " where IsApproved = 1 and date_of_report = '" + date_of_report + "' and ulb_id = "+ulbId;
			pst = connection.prepareStatement(query);
			//System.out.println("pst ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				result = 1;
			}
			
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for entryCheck");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for entryCheck"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("entryCheck -- END");
		}

		return result;

	}
	
	public int entryCheckWithoutApproval(String tableName, String date_of_report, int ulbId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("entryCheckWithoutApproval -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "select * from " + tableName + " where date_of_report = '" + date_of_report + "' and ulb_id = "+ulbId;
			pst = connection.prepareStatement(query);
			//System.out.println("entryCheckWithoutApproval ---- >>>"+pst);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				result = 1;
			}
			
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for entryCheckWithoutApproval");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for entryCheckWithoutApproval"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("entryCheckWithoutApproval -- END");
		}

		return result;

	}
	
	public int isNotApprovedUpdate(String tableName, String PK, int id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update " + tableName + " set IsApproved = 0 where " + PK + "=?";
			pst = connection.prepareStatement(query);
			pst.setInt(1, id);
			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for isNotApprovedUpdate");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for isNotApprovedUpdate"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return result;

	}

}
