package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class DailyComplaintsDao extends DeleteDao {

	private static Logger logger = Logger.getLogger(DailyComplaintsDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbNameService;

	public int addDailyComplaints(DailyComplaintsModel dailyComplaintsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyComplaints -- START");
		}
		int t = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "insert into daily_complaints(date_of_report,ulb_id,no_ws_comp_recv,no_ws_comp_resolved,no_ws_comp_pending,no_tw_comp_recv,no_tw_comp_resolved,no_tw_comp_pending,remarks,user_id,entry_date,IsApproved) values(?,?,?,?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?) ";
			pst = connection.prepareStatement(query);
			pst.setString(1, dailyComplaintsModel.getDate_of_report()); 
			pst.setInt(2, dailyComplaintsModel.getUlbId());
			pst.setDouble(3, dailyComplaintsModel.getNo_ws_comp_recv());
			pst.setDouble(4, dailyComplaintsModel.getNo_ws_comp_resolved());
			pst.setDouble(5, dailyComplaintsModel.getNo_ws_comp_pending());
			pst.setDouble(6, dailyComplaintsModel.getNo_tw_comp_recv());
			pst.setDouble(7, dailyComplaintsModel.getNo_tw_comp_resolved());
			pst.setDouble(8, dailyComplaintsModel.getNo_tw_comp_pending());
			pst.setString(9, dailyComplaintsModel.getRemarks());
			pst.setInt(10, dailyComplaintsModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(11, Util.getCurrentDateTime());
			pst.setInt(12, dailyComplaintsModel.getIsApproved());

			t = pst.executeUpdate();
			if (t > 0)
				t = 1;
			else
				t = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addDailyComplaints");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addDailyComplaints"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyComplaints -- END");
		}

		return t;

	}

	public LinkedList<DailyComplaintsModel> fetchDailyComplaints(
			LoginUsersModel loginUsersModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaints -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyComplaintsModel> dailyComplaintsModels = new LinkedList<DailyComplaintsModel>();
		try {

			String query = "";
			if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB
																				// User

				query = "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dc.ulb_id ="
						+ loginUsersModel.getUlbNameModel().getUlb_id()
						+ " order by dc.entry_date desc";

			} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District
																						// User

				LinkedList upperLevels = districtService
						.fetchUpperLevelsByULBId(loginUsersModel
								.getUlbNameModel().getUlb_id());
				int districtId = (Integer) upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService
						.fetchULBNameByDistrictId(districtId);

				String ulbs = "";
				for (ULBNameModel ulbNameModel : ulbNameModels) {

					ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
				}

				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

				query = "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dc.ulb_id in ("
						+ ulbs + ")  order by dc.entry_date desc";

			} else if (loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division
																						// User

				LinkedList upperLevels = districtService
						.fetchUpperLevelsByULBId(loginUsersModel
								.getUlbNameModel().getUlb_id());
				int divisionId = (Integer) upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService
						.fetchDistrictByDivisionId(divisionId);

				String ulbs = "";
				for (PhDistrictModel phDistrictModel : districtModels) {

					LinkedList<ULBNameModel> ulbNameModels = ulbNameService
							.fetchULBNameByDistrictId(phDistrictModel
									.getDist_id());

					for (ULBNameModel ulbNameModel : ulbNameModels) {

						ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
					}

				}

				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

				query = "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dc.ulb_id in ("
						+ ulbs + ")  order by dc.entry_date desc";

			} else { // Others All (State, PHEO, Circle, H&UD)

				query = "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') order by dc.entry_date desc";
			}
			
			// String query =
						// "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id order by dc.entry_date desc";
						// pst = connection.prepareStatement(query);
			
			pst = connection.prepareStatement(query);
			pst.setString(1, Util.getYesterdayDateString());			
			ResultSet rs = pst.executeQuery();
			
			while (rs.next()) {
				DailyComplaintsModel dailyComplaintsModel = new DailyComplaintsModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(15));
				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(11));
				dailyComplaintsModel.setUlbNameModel(ulbNameModel);
				
				dailyComplaintsModel.setTw_id(rs.getInt(1));
				dailyComplaintsModel.setDate_of_report(rs.getString(2));
				dailyComplaintsModel.setNo_ws_comp_recv(rs.getDouble(4));
				dailyComplaintsModel.setNo_ws_comp_resolved(rs.getDouble(5));
				dailyComplaintsModel.setNo_ws_comp_pending(rs.getDouble(6));
				dailyComplaintsModel.setNo_tw_comp_recv(rs.getDouble(7));
				dailyComplaintsModel.setNo_tw_comp_resolved(rs.getDouble(8));
				dailyComplaintsModel.setNo_tw_comp_pending(rs.getDouble(9));
				dailyComplaintsModel.setRemarks(rs.getString(10));
				dailyComplaintsModel.setEntry_date(rs.getString(12));
				dailyComplaintsModel.setModify_date(rs.getString(13));
				dailyComplaintsModel.setIsApproved(rs.getInt(14));
				dailyComplaintsModel.setLoginUsersModel(loginUsersModel2);
				dailyComplaintsModels.add(dailyComplaintsModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyComplaints");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyComplaints"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaints -- END");
		}

		return dailyComplaintsModels;

	}

	public DailyComplaintsModel fetchDailyComplaintsById(int tw_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetionsById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		DailyComplaintsModel dailyComplaintsModel = new DailyComplaintsModel();
		try {
			String query = "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.tw_id =? order by dc.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, tw_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(15));
				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(11));

				dailyComplaintsModel.setTw_id(rs.getInt(1));
				dailyComplaintsModel.setUlbNameModel(ulbNameModel);
				dailyComplaintsModel.setDate_of_report(rs.getString(2));
				dailyComplaintsModel.setNo_ws_comp_recv(rs.getDouble(4));
				dailyComplaintsModel.setNo_ws_comp_resolved(rs.getDouble(5));
				dailyComplaintsModel.setNo_ws_comp_pending(rs.getDouble(6));
				dailyComplaintsModel.setNo_tw_comp_recv(rs.getDouble(7));
				dailyComplaintsModel.setNo_tw_comp_resolved(rs.getDouble(8));
				dailyComplaintsModel.setNo_tw_comp_pending(rs.getDouble(9));
				dailyComplaintsModel.setRemarks(rs.getString(10));
				dailyComplaintsModel.setEntry_date(rs.getString(12));
				dailyComplaintsModel.setModify_date(rs.getString(13));
				dailyComplaintsModel.setIsApproved(rs.getInt(14));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyComplaintsById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyComplaintsById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaintsById -- END");
		}

		return dailyComplaintsModel;

	}

	public int updateDailyComplaints(DailyComplaintsModel dailyComplaintsModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyComplaints -- START");
		}
		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update daily_complaints set ulb_id=?,date_of_report=?,no_ws_comp_recv=?,no_ws_comp_resolved=?,no_ws_comp_pending=?,no_tw_comp_recv=?,no_tw_comp_resolved=?,no_tw_comp_pending=?,remarks=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s') where tw_id=? ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, dailyComplaintsModel.getUlbId());
			pst.setString(2, dailyComplaintsModel.getDate_of_report());
			pst.setDouble(3, dailyComplaintsModel.getNo_ws_comp_recv());
			pst.setDouble(4, dailyComplaintsModel.getNo_ws_comp_resolved());
			pst.setDouble(5, dailyComplaintsModel.getNo_ws_comp_pending());
			pst.setDouble(6, dailyComplaintsModel.getNo_tw_comp_recv());
			pst.setDouble(7, dailyComplaintsModel.getNo_tw_comp_resolved());
			pst.setDouble(8, dailyComplaintsModel.getNo_tw_comp_pending());
			pst.setString(9, dailyComplaintsModel.getRemarks());
			pst.setInt(10, dailyComplaintsModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(11, Util.getCurrentDateTime());
			pst.setInt(12, dailyComplaintsModel.getTw_id());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updateDailyComplaints");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updateDailyComplaints"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyComplaints -- END");
		}

		return result;

	}
	
	public LinkedList<DailyComplaintsModel> fetchAJAXDailyComplaints(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyComplaints -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyComplaintsModel> dailyComplaintsModels = new LinkedList<DailyComplaintsModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =(case '"
						+ listingModel.getDate()
						+ "' when 'null' then dc.date_of_report else str_to_date('"
						+ listingModel.getDate()
						+ "','%Y-%m-%d %H:%i:%s') end) order by dc.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =(case '"
							+ listingModel.getDate()
							+ "' when 'null' then dc.date_of_report else str_to_date('"
							+ listingModel.getDate()
							+ "','%Y-%m-%d %H:%i:%s') end) and dc.ulb_id in ("
							+ ulbs + ") order by dc.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dc.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dc.ulb_id in ("
								+ ulbs + ") order by dc.entry_date desc";

					} else { // Only For ULB

						query = "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id and dc.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dc.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dc.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by dc.entry_date desc";
					}

				}

			}
			
			
			// String query =
						// "select dc.tw_id,dc.date_of_report,dc.ulb_id,dc.no_ws_comp_recv,dc.no_ws_comp_resolved,dc.no_ws_comp_pending,dc.no_tw_comp_recv,dc.no_tw_comp_resolved,dc.no_tw_comp_pending,dc.remarks,dc.user_id,dc.entry_date,dc.modify_date,dc.IsApproved,un.ulb_Name from daily_complaints dc,ulbs_name un where dc.ulb_id = un.ulb_id order by dc.entry_date desc";
						// pst = connection.prepareStatement(query);
			
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				DailyComplaintsModel dailyComplaintsModel = new DailyComplaintsModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(3));
				ulbNameModel.setUlb_Name(rs.getString(15));
				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(11));
				dailyComplaintsModel.setUlbNameModel(ulbNameModel);
				dailyComplaintsModel.setTw_id(rs.getInt(1));
				dailyComplaintsModel.setDate_of_report(rs.getString(2));
				dailyComplaintsModel.setNo_ws_comp_recv(rs.getDouble(4));
				dailyComplaintsModel.setNo_ws_comp_resolved(rs.getDouble(5));
				dailyComplaintsModel.setNo_ws_comp_pending(rs.getDouble(6));
				dailyComplaintsModel.setNo_tw_comp_recv(rs.getDouble(7));
				dailyComplaintsModel.setNo_tw_comp_resolved(rs.getDouble(8));
				dailyComplaintsModel.setNo_tw_comp_pending(rs.getDouble(9));
				dailyComplaintsModel.setRemarks(rs.getString(10));
				dailyComplaintsModel.setEntry_date(rs.getString(12));
				dailyComplaintsModel.setModify_date(rs.getString(13));
				dailyComplaintsModel.setIsApproved(rs.getInt(14));
				dailyComplaintsModel.setLoginUsersModel(loginUsersModel2);
				dailyComplaintsModels.add(dailyComplaintsModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXDailyComplaints");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXDailyComplaints"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyComplaints -- END");
		}

		return dailyComplaintsModels;

	}
}
