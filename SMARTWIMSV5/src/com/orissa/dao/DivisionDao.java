package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;
import com.orissa.model.PhDivisionModel;

@Repository
public class DivisionDao {

	private static Logger logger = Logger.getLogger(DivisionDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	public LinkedList<PhDivisionModel> fetchDivision() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDivision -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhDivisionModel> phDivisionModels = new LinkedList<PhDivisionModel>();
		try {
			String query = "select div_id,div_name from ph_division order by div_name";
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhDivisionModel phDivisionModel = new PhDivisionModel();

				phDivisionModel.setDiv_id(rs.getInt(1));
				phDivisionModel.setDiv_name(rs.getString(2));

				phDivisionModels.add(phDivisionModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDivision");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDivision"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDivision -- END");
		}

		return phDivisionModels;

	}
	
	public LinkedList<PhDivisionModel> fetchDivisionByAmrut() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDivision -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<PhDivisionModel> phDivisionModels = new LinkedList<PhDivisionModel>();
		try {
			String query = "SELECT distinct(pdiv.div_id),pdiv.div_name FROM ph_division pdiv,ph_district phdis,ulbs u where pdiv.div_id = phdis.div_id and phdis.dist_id = u.dist_id and u.amrut_type_id = 1";
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				PhDivisionModel phDivisionModel = new PhDivisionModel();

				phDivisionModel.setDiv_id(rs.getInt(1));
				phDivisionModel.setDiv_name(rs.getString(2));

				phDivisionModels.add(phDivisionModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDivision");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDivision"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDivision -- END");
		}

		return phDivisionModels;

	}
}
