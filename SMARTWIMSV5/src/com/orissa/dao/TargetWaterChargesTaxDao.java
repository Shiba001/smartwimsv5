package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.TargetModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class TargetWaterChargesTaxDao extends DeleteDao {

	private static Logger logger = Logger.getLogger(TargetWaterChargesTaxDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private ULBNameService ulbNameService;
	
	public int addTargetWaterChargesTax(TargetModel targetModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addTargetWaterChargesTax -- START");
		}
		
		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "insert into target(ulb_id,trgt_mode,fin_year,projects,proj_entity,m_trgt_1,m_trgt_2,m_trgt_3,m_trgt_4,m_trgt_5,m_trgt_6,m_trgt_7,m_trgt_8,m_trgt_9,m_trgt_10,m_trgt_11,m_trgt_12,y_trgt,user_id,entry_date) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, targetModel.getUlbId());
			pst.setString(2, targetModel.getTrgt_mode());
			pst.setString(3, targetModel.getFin_year());
			pst.setString(4, targetModel.getProjects());
			pst.setString(5, targetModel.getProj_entity());
			pst.setDouble(6, targetModel.getM_trgt_1());
			pst.setDouble(7, targetModel.getM_trgt_2());
			pst.setDouble(8, targetModel.getM_trgt_3());
			pst.setDouble(9, targetModel.getM_trgt_4());
			pst.setDouble(10, targetModel.getM_trgt_5());
			pst.setDouble(11, targetModel.getM_trgt_6());
			pst.setDouble(12, targetModel.getM_trgt_7());
			pst.setDouble(13, targetModel.getM_trgt_8());
			pst.setDouble(14, targetModel.getM_trgt_9());
			pst.setDouble(15, targetModel.getM_trgt_10());
			pst.setDouble(16, targetModel.getM_trgt_11());
			pst.setDouble(17, targetModel.getM_trgt_12());
			pst.setDouble(18, targetModel.getY_trgt());
			pst.setInt(19, targetModel.getLoginUsersModel().getUser_id());
			pst.setString(20, Util.getCurrentDateTime());
			
			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isInfoEnabled()) {
						logger.info("Connection Closed for addTargetWaterChargesTax");
					}
				}
			} catch (Exception e) {

				if (logger.isInfoEnabled()) {
					logger.info("Connection not closed for addTargetWaterChargesTax"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addTargetWaterChargesTax -- END");
		}

		return result;

	}
	
	public LinkedList<TargetModel> fetchTargetWaterChargesTax(LoginUsersModel loginUsersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTax -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<TargetModel> targetModels = new LinkedList<TargetModel>();
		try {
			//String query = "select t.trgt_id,t.ulb_id,t.trgt_mode,t.fin_year,t.projects,t.proj_entity,t.m_trgt_1,t.m_trgt_2,t.m_trgt_3,t.m_trgt_4,t.m_trgt_5,t.m_trgt_6,t.m_trgt_7,t.m_trgt_8,t.m_trgt_9,t.m_trgt_10,t.m_trgt_11,t.m_trgt_12,t.y_trgt,t.user_id,t.entry_date,t.modify_date,un.ulb_Name from target t ,ulbs_name un where t.ulb_id = un.ulb_id order by t.entry_date desc";
			String query = "";
			if(loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
				
				query = "select t.trgt_id,t.ulb_id,t.trgt_mode,t.fin_year,t.projects,t.proj_entity,t.m_trgt_1,t.m_trgt_2,t.m_trgt_3,t.m_trgt_4,t.m_trgt_5,t.m_trgt_6,t.m_trgt_7,t.m_trgt_8,t.m_trgt_9,t.m_trgt_10,t.m_trgt_11,t.m_trgt_12,t.y_trgt,t.user_id,t.entry_date,t.modify_date,un.ulb_Name from target t ,ulbs_name un where t.ulb_id = un.ulb_id and t.ulb_id ="+loginUsersModel.getUlbNameModel().getUlb_id()+" and t.fin_year=? order by t.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int districtId = (Integer)upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(districtId);
				
				String ulbs = "";
				for(ULBNameModel ulbNameModel:ulbNameModels) {
					
					ulbs = ulbs +ulbNameModel.getUlb_id()+",";
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select t.trgt_id,t.ulb_id,t.trgt_mode,t.fin_year,t.projects,t.proj_entity,t.m_trgt_1,t.m_trgt_2,t.m_trgt_3,t.m_trgt_4,t.m_trgt_5,t.m_trgt_6,t.m_trgt_7,t.m_trgt_8,t.m_trgt_9,t.m_trgt_10,t.m_trgt_11,t.m_trgt_12,t.y_trgt,t.user_id,t.entry_date,t.modify_date,un.ulb_Name from target t ,ulbs_name un where t.ulb_id = un.ulb_id and t.ulb_id in ("+ulbs+") and t.fin_year=?  order by t.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int divisionId = (Integer)upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService.fetchDistrictByDivisionId(divisionId);
				
				String ulbs = "";
				for(PhDistrictModel phDistrictModel:districtModels) {
					
					LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(phDistrictModel.getDist_id());
					
					for(ULBNameModel ulbNameModel:ulbNameModels) {
						
						ulbs = ulbs +ulbNameModel.getUlb_id()+",";
					}
					
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select t.trgt_id,t.ulb_id,t.trgt_mode,t.fin_year,t.projects,t.proj_entity,t.m_trgt_1,t.m_trgt_2,t.m_trgt_3,t.m_trgt_4,t.m_trgt_5,t.m_trgt_6,t.m_trgt_7,t.m_trgt_8,t.m_trgt_9,t.m_trgt_10,t.m_trgt_11,t.m_trgt_12,t.y_trgt,t.user_id,t.entry_date,t.modify_date,un.ulb_Name from target t ,ulbs_name un where t.ulb_id = un.ulb_id and t.ulb_id in ("+ulbs+") and t.fin_year=?  order by t.entry_date desc";
			
			} else { // Others All (State, PHEO, Circle, H&UD)
				
				query = "select t.trgt_id,t.ulb_id,t.trgt_mode,t.fin_year,t.projects,t.proj_entity,t.m_trgt_1,t.m_trgt_2,t.m_trgt_3,t.m_trgt_4,t.m_trgt_5,t.m_trgt_6,t.m_trgt_7,t.m_trgt_8,t.m_trgt_9,t.m_trgt_10,t.m_trgt_11,t.m_trgt_12,t.y_trgt,t.user_id,t.entry_date,t.modify_date,un.ulb_Name from target t ,ulbs_name un where t.ulb_id = un.ulb_id and t.fin_year=? order by t.entry_date desc";
			}
			pst = connection.prepareStatement(query);
			pst.setString(1, "2016-2017");
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				TargetModel targetModel = new TargetModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(20));
				
				targetModel.setTrgt_id(rs.getInt(1));
				
				targetModel.setTrgt_mode(rs.getString(3));
				targetModel.setFin_year(rs.getString(4));
				targetModel.setProjects(rs.getString(5));
				targetModel.setProj_entity(rs.getString(6));
				targetModel.setM_trgt_1(rs.getDouble(7));
				targetModel.setM_trgt_2(rs.getDouble(8));
				targetModel.setM_trgt_3(rs.getDouble(9));
				targetModel.setM_trgt_4(rs.getDouble(10));
				targetModel.setM_trgt_5(rs.getDouble(11));
				targetModel.setM_trgt_6(rs.getDouble(12));
				targetModel.setM_trgt_7(rs.getDouble(13));
				targetModel.setM_trgt_8(rs.getDouble(14));
				targetModel.setM_trgt_9(rs.getDouble(15));
				targetModel.setM_trgt_10(rs.getDouble(16));
				targetModel.setM_trgt_11(rs.getDouble(17));
				targetModel.setM_trgt_12(rs.getDouble(18));
				targetModel.setY_trgt(rs.getDouble(19));
				
				targetModel.setEntry_date(rs.getString(21));
				targetModel.setModify_date(rs.getString(22));
				ulbNameModel.setUlb_Name(rs.getString(23));
				targetModel.setUlbNameModel(ulbNameModel);
				targetModel.setLoginUsersModel(loginUsersModel2);
				
				targetModels.add(targetModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchTargetWaterChargesTax");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchTargetWaterChargesTax"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTax -- END");
		}

		return targetModels;

	}
	
	
	public TargetModel fetchTargetWaterChargesTaxById(int trgt_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTaxById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		TargetModel targetModel = new TargetModel();
		try {
			String query = "select t.trgt_id,t.ulb_id,t.trgt_mode,t.fin_year,t.projects,t.proj_entity,t.m_trgt_1,t.m_trgt_2,t.m_trgt_3,t.m_trgt_4,t.m_trgt_5,t.m_trgt_6,t.m_trgt_7,t.m_trgt_8,t.m_trgt_9,t.m_trgt_10,t.m_trgt_11,t.m_trgt_12,t.y_trgt,t.user_id,t.entry_date,t.modify_date,un.ulb_Name from target t,ulbs_name un where t.ulb_id = un.ulb_id and t.trgt_id=?  order by t.entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, trgt_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(20));
				
				targetModel.setTrgt_id(rs.getInt(1));
				
				targetModel.setTrgt_mode(rs.getString(3));
				targetModel.setFin_year(rs.getString(4));
				targetModel.setProjects(rs.getString(5));
				targetModel.setProj_entity(rs.getString(6));
				targetModel.setM_trgt_1(rs.getDouble(7));
				targetModel.setM_trgt_2(rs.getDouble(8));
				targetModel.setM_trgt_3(rs.getDouble(9));
				targetModel.setM_trgt_4(rs.getDouble(10));
				targetModel.setM_trgt_5(rs.getDouble(11));
				targetModel.setM_trgt_6(rs.getDouble(12));
				targetModel.setM_trgt_7(rs.getDouble(13));
				targetModel.setM_trgt_8(rs.getDouble(14));
				targetModel.setM_trgt_9(rs.getDouble(15));
				targetModel.setM_trgt_10(rs.getDouble(16));
				targetModel.setM_trgt_11(rs.getDouble(17));
				targetModel.setM_trgt_12(rs.getDouble(18));
				targetModel.setY_trgt(rs.getDouble(19));
				
				targetModel.setEntry_date(rs.getString(21));
				targetModel.setModify_date(rs.getString(22));
				ulbNameModel.setUlb_Name(rs.getString(23));
				targetModel.setUlbNameModel(ulbNameModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchTargetWaterChargesTaxById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchTargetWaterChargesTaxById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTaxById -- END");
		}

		return targetModel;

	}
	
	public int updateTargetWaterChargesTax(TargetModel targetModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateTargetWaterChargesTax -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update target set ulb_id=?,trgt_mode=?,fin_year=?,projects=?,proj_entity=?,m_trgt_1=?,m_trgt_2=?,m_trgt_3=?,m_trgt_4=?,m_trgt_5=?,m_trgt_6=?,m_trgt_7=?,m_trgt_8=?,m_trgt_9=?,m_trgt_10=?,m_trgt_11=?,m_trgt_12=?,y_trgt=?,user_id=?,modify_date=? where trgt_id=? ";
			pst = connection.prepareStatement(query);
			
			pst.setInt(1, targetModel.getUlbId());
			pst.setString(2, targetModel.getTrgt_mode());
			pst.setString(3, targetModel.getFin_year());
			pst.setString(4, targetModel.getProjects());
			pst.setString(5, targetModel.getProj_entity());
			pst.setDouble(6, targetModel.getM_trgt_1());
			pst.setDouble(7, targetModel.getM_trgt_2());
			pst.setDouble(8, targetModel.getM_trgt_3());
			pst.setDouble(9, targetModel.getM_trgt_4());
			pst.setDouble(10, targetModel.getM_trgt_5());
			pst.setDouble(11, targetModel.getM_trgt_6());
			pst.setDouble(12, targetModel.getM_trgt_7());
			pst.setDouble(13, targetModel.getM_trgt_8());
			pst.setDouble(14, targetModel.getM_trgt_9());
			pst.setDouble(15, targetModel.getM_trgt_10());
			pst.setDouble(16, targetModel.getM_trgt_11());
			pst.setDouble(17, targetModel.getM_trgt_12());
			pst.setDouble(18, targetModel.getY_trgt());
			pst.setInt(19, targetModel.getLoginUsersModel().getUser_id());
			pst.setString(20, Util.getCurrentDateTime());
			pst.setInt(21, targetModel.getTrgt_id());
			
			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updateTargetWaterChargesTax");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updateTargetWaterChargesTax"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updateTargetWaterChargesTax -- END");
		}

		return result;

	}
	
	
	
	public LinkedList<TargetModel> fetchAJAXTargetWaterChargesTax(ListingModel listingModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXTargetWaterChargesTax -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<TargetModel> targetModels = new LinkedList<TargetModel>();
		try {
			
			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select t.trgt_id,t.ulb_id,t.trgt_mode,t.fin_year,t.projects,t.proj_entity,t.m_trgt_1,t.m_trgt_2,t.m_trgt_3,t.m_trgt_4,t.m_trgt_5,t.m_trgt_6,t.m_trgt_7,t.m_trgt_8,t.m_trgt_9,t.m_trgt_10,t.m_trgt_11,t.m_trgt_12,t.y_trgt,t.user_id,t.entry_date,t.modify_date,un.ulb_Name from target t ,ulbs_name un where t.ulb_id = un.ulb_id and t.fin_year=? "
						+ " order by t.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select t.trgt_id,t.ulb_id,t.trgt_mode,t.fin_year,t.projects,t.proj_entity,t.m_trgt_1,t.m_trgt_2,t.m_trgt_3,t.m_trgt_4,t.m_trgt_5,t.m_trgt_6,t.m_trgt_7,t.m_trgt_8,t.m_trgt_9,t.m_trgt_10,t.m_trgt_11,t.m_trgt_12,t.y_trgt,t.user_id,t.entry_date,t.modify_date,un.ulb_Name from target t ,ulbs_name un where t.ulb_id = un.ulb_id  and t.fin_year=? "
							+ "and t.ulb_id in ("
							+ ulbs + ") order by t.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select t.trgt_id,t.ulb_id,t.trgt_mode,t.fin_year,t.projects,t.proj_entity,t.m_trgt_1,t.m_trgt_2,t.m_trgt_3,t.m_trgt_4,t.m_trgt_5,t.m_trgt_6,t.m_trgt_7,t.m_trgt_8,t.m_trgt_9,t.m_trgt_10,t.m_trgt_11,t.m_trgt_12,t.y_trgt,t.user_id,t.entry_date,t.modify_date,un.ulb_Name from target t ,ulbs_name un where t.ulb_id = un.ulb_id  and t.fin_year=? "
								+ "and t.ulb_id in ("
								+ ulbs + ") order by t.entry_date desc";

					} else { // Only For ULB

						query = "select t.trgt_id,t.ulb_id,t.trgt_mode,t.fin_year,t.projects,t.proj_entity,t.m_trgt_1,t.m_trgt_2,t.m_trgt_3,t.m_trgt_4,t.m_trgt_5,t.m_trgt_6,t.m_trgt_7,t.m_trgt_8,t.m_trgt_9,t.m_trgt_10,t.m_trgt_11,t.m_trgt_12,t.y_trgt,t.user_id,t.entry_date,t.modify_date,un.ulb_Name from target t ,ulbs_name un where t.ulb_id = un.ulb_id  and t.fin_year=? "
								+ "and t.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by t.entry_date desc";
					}

				}

			}
			pst = connection.prepareStatement(query);
			pst.setString(1, listingModel.getFin_year());
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				TargetModel targetModel = new TargetModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(20));
				
				targetModel.setTrgt_id(rs.getInt(1));
				
				targetModel.setTrgt_mode(rs.getString(3));
				targetModel.setFin_year(rs.getString(4));
				targetModel.setProjects(rs.getString(5));
				targetModel.setProj_entity(rs.getString(6));
				targetModel.setM_trgt_1(rs.getDouble(7));
				targetModel.setM_trgt_2(rs.getDouble(8));
				targetModel.setM_trgt_3(rs.getDouble(9));
				targetModel.setM_trgt_4(rs.getDouble(10));
				targetModel.setM_trgt_5(rs.getDouble(11));
				targetModel.setM_trgt_6(rs.getDouble(12));
				targetModel.setM_trgt_7(rs.getDouble(13));
				targetModel.setM_trgt_8(rs.getDouble(14));
				targetModel.setM_trgt_9(rs.getDouble(15));
				targetModel.setM_trgt_10(rs.getDouble(16));
				targetModel.setM_trgt_11(rs.getDouble(17));
				targetModel.setM_trgt_12(rs.getDouble(18));
				targetModel.setY_trgt(rs.getDouble(19));
				
				targetModel.setEntry_date(rs.getString(21));
				targetModel.setModify_date(rs.getString(22));
				ulbNameModel.setUlb_Name(rs.getString(23));
				targetModel.setUlbNameModel(ulbNameModel);
				targetModel.setLoginUsersModel(loginUsersModel2);
				
				targetModels.add(targetModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXTargetWaterChargesTax");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXTargetWaterChargesTax"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXTargetWaterChargesTax -- END");
		}

		return targetModels;

	}
}
