package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.connection.DBConnection;
import com.orissa.exception.DBConnectionException;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.DailyConnetionsModel;
import com.orissa.model.DailyTankersModel;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.DailyWaterTestingModel;
import com.orissa.model.ULBNameModel;

@Repository
public class HomeDao {

	private static Logger logger = Logger.getLogger(HomeDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;

	public String index() throws DBConnectionException {

		if (logger.isInfoEnabled()) {
			logger.info("index -- START");
		}

		String connect = "";
		Connection con = null;

		try {

			con = dbConnection.Connect();

		} catch (Exception e) {

			e.printStackTrace();
			throw new DBConnectionException(
					messageUtil.getBundle("database.not.found"));
		}
		
		connect = con.toString();

		if (logger.isInfoEnabled()) {
			logger.info("index -- END");
		}

		return connect;
	}
	
	public LinkedList<DailyWaterSupplyModel> fetchDailyWaterSupplyReport()
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyReport -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = new LinkedList<DailyWaterSupplyModel>();
		int i = 1;
		double total_tot_demand = 0.0D;
		double total_qlty_supply = 0.0D;
		double total_pop_served = 0.0D;
		double total_Freq_supply = 0.0D;
		double total_surplus_deficit = 0.0D;
		double total_rate_of_supply = 0.0D;
		try {
			String query = "SELECT u.ulb_name,round(sum(tot_demand)/count(*),2),round(sum(qlty_supply)/count(*),2) ,round(sum(pop_served)/count(*),2),round(sum(surplus_deficit)/count(*),2),round(sum(rate_of_supply)/count(*),2),round(sum(Freq_supply)/count(*),2) from daily_water_supply dws inner join ulbs u on dws.ulb_id = u.m_ulb_id where str_to_date(date_of_report,'%Y-%m-%d') between (SELECT (case when str_to_date('01-11-2016','%d-%m-%Y') between str_to_date(concat('01-01-',year(str_to_date('01-11-2016','%d-%m-%Y'))),'%d-%m-%Y') and str_to_date(concat('31-03-',year(str_to_date('01-11-2016','%d-%m-%Y'))),'%d-%m-%Y') then concat('01-04-',year(str_to_date('01-11-2016','%d-%m-%Y'))-1) else concat('01-04-',year(str_to_date('01-11-2016','%d-%m-%Y'))) end) from dual) and str_to_date('01-11-2016','%d-%m-%Y') group by u.m_ulb_id,u.ulb_name order by u.ulb_name";
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyWaterSupplyModel dailyWaterSupplyModel = new DailyWaterSupplyModel();
				
				dailyWaterSupplyModel.setSlNo(i++);
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_Name(rs.getString(1));

				dailyWaterSupplyModel.setTot_demand(rs.getDouble(2));
				dailyWaterSupplyModel.setQlty_supply(rs.getDouble(3));
				dailyWaterSupplyModel.setPop_served(rs.getDouble(4));
				dailyWaterSupplyModel.setSurplus_deficit(rs.getDouble(5));
				dailyWaterSupplyModel.setRate_of_supply(rs.getDouble(6));
				dailyWaterSupplyModel.setFreq_supply(rs.getDouble(7));

				dailyWaterSupplyModel.setUlbNameModel(ulbNameModel);
				
				total_tot_demand = total_tot_demand + dailyWaterSupplyModel.getTot_demand();
				total_qlty_supply = total_qlty_supply + dailyWaterSupplyModel.getQlty_supply();
				total_pop_served = total_pop_served + dailyWaterSupplyModel.getPop_served();
				total_Freq_supply = total_Freq_supply + dailyWaterSupplyModel.getFreq_supply();
				total_surplus_deficit = total_surplus_deficit + dailyWaterSupplyModel.getSurplus_deficit();
				total_rate_of_supply = total_rate_of_supply + dailyWaterSupplyModel.getRate_of_supply();
				
				dailyWaterSupplyModels.add(dailyWaterSupplyModel);
			}
			
			DailyWaterSupplyModel dailyWaterSupplyModel2 = new DailyWaterSupplyModel();
			
			ULBNameModel ulbNameModel2 = new ULBNameModel();
			ulbNameModel2.setUlb_Name("Total :");
			
			dailyWaterSupplyModel2.setSlNo(null);
			dailyWaterSupplyModel2.setUlbNameModel(ulbNameModel2);
			dailyWaterSupplyModel2.setTotal_Freq_supply(Math.round(total_Freq_supply*100.0)/100.0);
			dailyWaterSupplyModel2.setTotal_pop_served(Math.round(total_pop_served*100.0)/100.0);
			dailyWaterSupplyModel2.setTotal_qlty_supply(Math.round(total_qlty_supply*100.0)/100.0);
			dailyWaterSupplyModel2.setTotal_rate_of_supply(Math.round(total_rate_of_supply*100.0)/100.0);
			dailyWaterSupplyModel2.setTotal_surplus_deficit(Math.round(total_surplus_deficit*100.0)/100.0);
			dailyWaterSupplyModel2.setTotal_tot_demand(Math.round(total_tot_demand*100.0)/100.0);
			
			dailyWaterSupplyModels.add(dailyWaterSupplyModel2);

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterSupplyReport");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterSupplyReport"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyReport -- END");
		}

		return dailyWaterSupplyModels;

	}
	
	public LinkedList<DailyTankersModel> fetchDailyTankersReport() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankersReport -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyTankersModel> dailyTankersModels = new LinkedList<DailyTankersModel>();
		int i = 1;
		Double total_tankers_engaged = null;
		Double total_tankers_hired = null;
		Double total_tanks_deployed = null;
		Double total_population_served = null;
		Double total_quantity_supplied = null;
		
		try {
			String query = "SELECT u.ulb_name,round(sum(no_tank_engd_dept)/count(*),2),round(sum(no_tank_hire)/count(*),2) ,round(sum(no_pvc_rcc_tanks_depl)/count(*),2),round(sum(popu_served_tanks_pvc_rcc)/count(*),2),round(sum(qty_mld)/count(*),2) from daily_tankers dt inner join ulbs u on dt.ulb_id = u.m_ulb_id where str_to_date(date_of_report,'%Y-%m-%d') between (SELECT (case when str_to_date('01-11-2016','%d-%m-%Y') between str_to_date(concat('01-01-',year(str_to_date('01-11-2016','%d-%m-%Y'))),'%d-%m-%Y') and str_to_date(concat('31-03-',year(str_to_date('01-11-2016','%d-%m-%Y'))),'%d-%m-%Y') then concat('01-04-',year(str_to_date('01-11-2016','%d-%m-%Y'))-1) else concat('01-04-',year(str_to_date('01-11-2016','%d-%m-%Y'))) end) from dual) and str_to_date('01-11-2016','%d-%m-%Y') group by u.m_ulb_id,u.ulb_name order by u.ulb_name";
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyTankersModel dailyTankersModel = new DailyTankersModel();
				
				dailyTankersModel.setSlNo(i++);

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_Name(rs.getString(1));
				
				dailyTankersModel.setNo_tank_engd_dept(rs.getDouble(2));
				dailyTankersModel.setNo_tank_hire(rs.getDouble(3));
				dailyTankersModel.setNo_pvc_rcc_tanks_depl(rs.getDouble(4));
				dailyTankersModel.setPopu_served_tanks_pvc_rcc(rs.getDouble(5));
				dailyTankersModel.setQty_mld(rs.getDouble(6));
				
				dailyTankersModel.setUlbNameModel(ulbNameModel);
				
				total_tankers_engaged = total_tankers_engaged + dailyTankersModel.getNo_tank_engd_dept();
				total_tankers_hired = total_tankers_hired +	dailyTankersModel.getNo_tank_hire();
				total_tanks_deployed = total_tanks_deployed + dailyTankersModel.getNo_pvc_rcc_tanks_depl();
				total_population_served = total_population_served + dailyTankersModel.getPopu_served_tanks_pvc_rcc();
				total_quantity_supplied = total_quantity_supplied + dailyTankersModel.getQty_mld();

				dailyTankersModels.add(dailyTankersModel);

			}
			
			DailyTankersModel dailyTankersModel2 = new DailyTankersModel();
			
			ULBNameModel ulbNameModel2 = new ULBNameModel();
			ulbNameModel2.setUlb_Name("Total :");
			
			dailyTankersModel2.setSlNo(null);
			dailyTankersModel2.setUlbNameModel(ulbNameModel2);
			dailyTankersModel2.setTotal_tankers_engaged(Math.round(total_tankers_engaged*100.0)/100.0);
			dailyTankersModel2.setTotal_tankers_hired(Math.round(total_tankers_hired*100.0)/100.0);
			dailyTankersModel2.setTotal_tanks_deployed(Math.round(total_tanks_deployed*100.0)/100.0);
			dailyTankersModel2.setTotal_population_served(Math.round(total_population_served*100.0)/100.0);
			dailyTankersModel2.setTotal_quantity_supplied(Math.round(total_quantity_supplied*100.0)/100.0);
			
			dailyTankersModels.add(dailyTankersModel2);

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyTankersReport");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyTankersReport"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankersReport -- END");
		}

		return dailyTankersModels;

	}
	
	public LinkedList<DailyConnetionsModel> fetchDailyConnetionsReport()
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetionsReport -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyConnetionsModel> dailyConnetionsModels = new LinkedList<DailyConnetionsModel>();
		int i = 1;
		Double total_residentials_provided = null;
		Double total_residentials_diconnected = null;
		Double total_industrial_provided = null;
		Double total_industrial_diconnected = null;
		Double total_commercial_provided = null;
		Double total_commercial_diconnected = null;
		Double total_institutional_provided = null;
		Double total_institutional_diconnected = null;
		Double total = null;
		try {
			String query = "SELECT u.ulb_name,round(sum(no_new_res_cons_add)/count(*),2),round(sum(no_res_cons_disc)/count(*),2) ,round(sum(no_new_ind_cons_add)/count(*),2),round(sum(no_com_cons_disc)/count(*),2),round(sum(no_new_com_cons_add)/count(*),2),round(sum(no_com_cons_disc)/count(*),2),round(sum(no_new_inst_cons_add)/count(*),2),round(sum(no_inst_cons_disc)/count(*),2),round(sum(tot_no_new_cons)/count(*),2) from  daily_connection dc inner join ulbs u on dc.ulb_id = u.m_ulb_id where str_to_date(date_of_report,'%Y-%m-%d') between (SELECT (case when str_to_date('01-11-2016','%d-%m-%Y') between str_to_date(concat('01-01-',year(str_to_date('01-11-2016','%d-%m-%Y'))),'%d-%m-%Y') and str_to_date(concat('31-03-',year(str_to_date('01-11-2016','%d-%m-%Y'))),'%d-%m-%Y') then concat('01-04-',year(str_to_date('01-11-2016','%d-%m-%Y'))-1) else concat('01-04-',year(str_to_date('01-11-2016','%d-%m-%Y'))) end) from dual) and str_to_date('01-11-2016','%d-%m-%Y') group by u.m_ulb_id,u.ulb_name order by u.ulb_name";
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyConnetionsModel  dailyConnetionsModel = new DailyConnetionsModel();
				
				dailyConnetionsModel.setSlNo(i++);

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_Name(rs.getString(1));
				
				dailyConnetionsModel.setNo_new_res_cons_add(rs.getDouble(2));
				dailyConnetionsModel.setNo_res_cons_disc(rs.getDouble(3));
				dailyConnetionsModel.setNo_new_ind_cons_add(rs.getDouble(4));
				dailyConnetionsModel.setNo_ind_cons_disc(rs.getDouble(5));
				dailyConnetionsModel.setNo_new_com_cons_add(rs.getDouble(6));
				dailyConnetionsModel.setNo_com_cons_disc(rs.getDouble(7));
				dailyConnetionsModel.setNo_new_inst_cons_add(rs.getDouble(8));
				dailyConnetionsModel.setNo_inst_cons_disc(rs.getDouble(9));
				dailyConnetionsModel.setTotal(dailyConnetionsModel.getNo_new_res_cons_add() + dailyConnetionsModel.getNo_res_cons_disc() + dailyConnetionsModel.getNo_new_ind_cons_add() + dailyConnetionsModel.getNo_ind_cons_disc() + dailyConnetionsModel.getNo_new_com_cons_add() + dailyConnetionsModel.getNo_com_cons_disc() + dailyConnetionsModel.getNo_new_inst_cons_add() + dailyConnetionsModel.getNo_inst_cons_disc());

				dailyConnetionsModel.setUlbNameModel(ulbNameModel);
				
				total_residentials_provided = total_residentials_provided + dailyConnetionsModel.getNo_new_res_cons_add();
				total_residentials_diconnected = total_residentials_diconnected + dailyConnetionsModel.getNo_res_cons_disc();
				total_industrial_provided = total_industrial_provided + dailyConnetionsModel.getNo_new_ind_cons_add();
				total_industrial_diconnected = total_industrial_diconnected + dailyConnetionsModel.getNo_ind_cons_disc();
				total_commercial_provided = total_commercial_provided + dailyConnetionsModel.getNo_new_com_cons_add();
				total_commercial_diconnected = total_commercial_diconnected + dailyConnetionsModel.getNo_com_cons_disc();
				total_institutional_provided = total_institutional_provided + dailyConnetionsModel.getNo_new_inst_cons_add();
				total_institutional_diconnected = total_institutional_diconnected + dailyConnetionsModel.getNo_inst_cons_disc();
				
				total = total + dailyConnetionsModel.getTotal();

				dailyConnetionsModels.add(dailyConnetionsModel);
			}
			
			DailyConnetionsModel dailyConnetionsModel2 = new DailyConnetionsModel();
			
			ULBNameModel ulbNameModel2 = new ULBNameModel();
			ulbNameModel2.setUlb_Name("Total :");
			
			dailyConnetionsModel2.setSlNo(null);
			dailyConnetionsModel2.setUlbNameModel(ulbNameModel2);
			dailyConnetionsModel2.setTotal_residentials_provided(Math.round(total_residentials_provided*100.0)/100.0);
			dailyConnetionsModel2.setTotal_residentials_diconnected(Math.round(total_residentials_diconnected*100.0)/100.0);
			dailyConnetionsModel2.setTotal_industrial_provided(Math.round(total_industrial_provided*100.0)/100.0);
			dailyConnetionsModel2.setTotal_industrial_diconnected(Math.round(total_industrial_diconnected*100.0)/100.0);
			dailyConnetionsModel2.setTotal_commercial_provided(Math.round(total_commercial_provided*100.0)/100.0);
			dailyConnetionsModel2.setTotal_commercial_diconnected(Math.round(total_commercial_diconnected*100.0)/100.0);
			dailyConnetionsModel2.setTotal_institutional_provided(Math.round(total_institutional_provided*100.0)/100.0);
			dailyConnetionsModel2.setTotal_institutional_diconnected(Math.round(total_institutional_diconnected*100.0)/100.0);
			dailyConnetionsModel2.setTotal(Math.round(total*100.0)/100.0);
			
			dailyConnetionsModels.add(dailyConnetionsModel2);

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyConnetionsReport");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyConnetionsReport"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetionsReport -- END");
		}

		return dailyConnetionsModels;

	}
	
	public LinkedList<DailyWaterTestingModel> fetchDailyWaterTestingReport()
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTestingReport -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = new LinkedList<DailyWaterTestingModel>();
		int i = 1;
		Double total_sample_tested = null;
		Double total_sample_tested_frc = null;
		Double total_sample_found = null;
		Double total_sample_found_frc = null;
		Double total_percentage_found = null;
		Double total_percentage_found_frc = null;
		try {
			String query = "SELECT u.ulb_name,round(sum(no_bact_samp_tested)/count(*),2),round(sum(no_other_samp_tested)/count(*),2) ,round(sum(no_bact_samp_found_ok)/count(*),2),round(sum(no_other_samp_found_ok)/count(*),2),round(sum(no_bact_samp_found_ok/no_bact_samp_tested*100)/count(*),2),round(sum(no_other_samp_found_ok/no_other_samp_tested*100)/count(*),2) from daily_water_testing dwt inner join ulbs u on dwt.ulb_id = u.m_ulb_id where str_to_date(date_of_report,'%Y-%m-%d') between (SELECT (case when str_to_date('01-11-2016','%d-%m-%Y') between str_to_date(concat('01-01-',year(str_to_date('01-11-2016','%d-%m-%Y'))),'%d-%m-%Y') and str_to_date(concat('31-03-',year(str_to_date('01-11-2016','%d-%m-%Y'))),'%d-%m-%Y') then concat('01-04-',year(str_to_date('01-11-2016','%d-%m-%Y'))-1) else concat('01-04-',year(str_to_date('01-11-2016','%d-%m-%Y'))) end) from dual) and str_to_date('01-11-2016','%d-%m-%Y') group by u.m_ulb_id,u.ulb_name order by u.ulb_name";
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyWaterTestingModel dailyWaterTestingModel = new DailyWaterTestingModel();

				dailyWaterTestingModel.setSlNo(i++);
				
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_Name(rs.getString(1));
				
				dailyWaterTestingModel.setNo_bact_samp_tested(rs.getDouble(2));
				dailyWaterTestingModel.setNo_other_samp_tested(rs.getDouble(3));
				dailyWaterTestingModel
				.setNo_bact_samp_found_ok(rs.getDouble(4));
				dailyWaterTestingModel.setNo_other_samp_found_ok(rs
						.getDouble(5));
				dailyWaterTestingModel.setPercentage_found(rs.getDouble(6));
				dailyWaterTestingModel.setPercentage_found_frc(rs.getDouble(7));

				dailyWaterTestingModel.setUlbNameModel(ulbNameModel);
				
				total_sample_tested = total_sample_tested + dailyWaterTestingModel.getNo_bact_samp_tested();
				total_sample_tested_frc = total_sample_tested_frc + dailyWaterTestingModel.getNo_other_samp_tested();
				total_sample_found = total_sample_found + dailyWaterTestingModel.getNo_bact_samp_found_ok();
				total_sample_found_frc = total_sample_found_frc + dailyWaterTestingModel.getNo_other_samp_found_ok();
				total_percentage_found = total_percentage_found + dailyWaterTestingModel.getPercentage_found();
				total_percentage_found_frc = total_percentage_found_frc + dailyWaterTestingModel.getPercentage_found_frc();

				dailyWaterTestingModels.add(dailyWaterTestingModel);
			}
			
			DailyWaterTestingModel dailyWaterTestingModel2 = new DailyWaterTestingModel();
			
			ULBNameModel ulbNameModel2 = new ULBNameModel();
			ulbNameModel2.setUlb_Name("Total :");
			
			dailyWaterTestingModel2.setSlNo(null);
			dailyWaterTestingModel2.setUlbNameModel(ulbNameModel2);
			dailyWaterTestingModel2.setTotal_sample_tested(Math.round(total_sample_tested_frc*100.0)/100.0);
			dailyWaterTestingModel2.setTotal_sample_tested_frc(Math.round(total_sample_tested_frc*100.0)/100.0);
			dailyWaterTestingModel2.setTotal_sample_found(Math.round(total_sample_found_frc*100.0)/100.0);
			dailyWaterTestingModel2.setTotal_sample_found_frc(Math.round(total_sample_found_frc*100.0)/100.0);
			dailyWaterTestingModel2.setTotal_percentage_found(Math.round(total_percentage_found_frc*100.0)/100.0);
			dailyWaterTestingModel2.setTotal_percentage_found_frc(Math.round(total_percentage_found_frc*100.0)/100.0);
			
			dailyWaterTestingModels.add(dailyWaterTestingModel2);

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterTestingReport");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterTestingReport"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTestingReport -- END");
		}

		return dailyWaterTestingModels;

	}
	
	public LinkedList<DailyComplaintsModel> fetchDailyComplaintsReport()
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaintsReport -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyComplaintsModel> dailyComplaintsModels = new LinkedList<DailyComplaintsModel>();
		int i = 1;
		Double total_comp_recveived = null;
		Double total_comp_resolved = null;
		Double total_comp_pending = null;
		Double total_tw_comp_recveived = null;
		Double total_tw_comp_resolved = null;
		Double total_tw_comp_pending = null;
		try {
			String query = "SELECT u.ulb_name,round(sum(no_ws_comp_recv)/count(*),2),round(sum(no_ws_comp_resolved)/count(*),2) ,round(sum(no_ws_comp_pending)/count(*),2),round(sum(no_tw_comp_recv)/count(*),2),round(sum(no_tw_comp_resolved)/count(*),2),round(sum(no_tw_comp_pending)/count(*),2) from daily_complaints dc inner join ulbs u on dc.ulb_id = u.m_ulb_id where str_to_date(date_of_report,'%Y-%m-%d') between (SELECT (case when str_to_date('01-11-2016','%d-%m-%Y') between str_to_date(concat('01-01-',year(str_to_date('01-11-2016','%d-%m-%Y'))),'%d-%m-%Y') and str_to_date(concat('31-03-',year(str_to_date('01-11-2016','%d-%m-%Y'))),'%d-%m-%Y') then concat('01-04-',year(str_to_date('01-11-2016','%d-%m-%Y'))-1) else concat('01-04-',year(str_to_date('01-11-2016','%d-%m-%Y'))) end) from dual) and str_to_date('01-11-2016','%d-%m-%Y') group by u.m_ulb_id,u.ulb_name order by u.ulb_name";
			pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				
				DailyComplaintsModel dailyComplaintsModel = new DailyComplaintsModel();

				dailyComplaintsModel.setSlNo(i++);
				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_Name(rs.getString(1));
				
				dailyComplaintsModel.setNo_ws_comp_recv(rs.getDouble(2));
				dailyComplaintsModel.setNo_ws_comp_resolved(rs.getDouble(3));
				dailyComplaintsModel.setNo_ws_comp_pending(rs.getDouble(4));
				dailyComplaintsModel.setNo_tw_comp_recv(rs.getDouble(5));
				dailyComplaintsModel.setNo_tw_comp_resolved(rs.getDouble(6));
				dailyComplaintsModel.setNo_tw_comp_pending(rs.getDouble(7));

				total_comp_recveived = total_comp_recveived + dailyComplaintsModel.getNo_ws_comp_recv();
				total_comp_resolved = total_comp_resolved + dailyComplaintsModel.getNo_ws_comp_resolved();
				total_comp_pending = total_comp_pending + dailyComplaintsModel.getNo_ws_comp_pending();
				total_tw_comp_recveived = total_tw_comp_recveived + dailyComplaintsModel.getNo_tw_comp_recv();
				total_tw_comp_resolved = total_tw_comp_resolved + dailyComplaintsModel.getNo_tw_comp_resolved();
				total_tw_comp_pending = total_tw_comp_pending + dailyComplaintsModel.getNo_tw_comp_pending();
				
				dailyComplaintsModel.setUlbNameModel(ulbNameModel);
				
				dailyComplaintsModels.add(dailyComplaintsModel);
			}
			
			DailyComplaintsModel dailyComplaintsModel2 = new DailyComplaintsModel();
			
			ULBNameModel ulbNameModel2 = new ULBNameModel();
			ulbNameModel2.setUlb_Name("Total :");
			
			dailyComplaintsModel2.setSlNo(null);
			dailyComplaintsModel2.setUlbNameModel(ulbNameModel2);
			dailyComplaintsModel2.setTotal_comp_recveived(Math.round(total_comp_recveived*100.0)/100.0);
			dailyComplaintsModel2.setTotal_comp_resolved(Math.round(total_comp_resolved*100.0)/100.0);
			dailyComplaintsModel2.setTotal_comp_pending(Math.round(total_comp_pending*100.0)/100.0);
			dailyComplaintsModel2.setTotal_tw_comp_recveived(Math.round(total_tw_comp_recveived*100.0)/100.0);
			dailyComplaintsModel2.setTotal_tw_comp_resolved(Math.round(total_tw_comp_resolved*100.0)/100.0);
			dailyComplaintsModel2.setTotal_tw_comp_pending(Math.round(total_tw_comp_pending*100.0)/100.0);
			
			dailyComplaintsModels.add(dailyComplaintsModel2);

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyComplaintsReport");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyComplaintsReport"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaintsReport -- END");
		}

		return dailyComplaintsModels;

	}
}
