package com.orissa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.connection.DBConnection;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.DailyWaterTestingModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhDistrictModel;
import com.orissa.model.ULBNameModel;
import com.orissa.service.DistrictService;
import com.orissa.service.ULBNameService;

@Repository
public class DailyWaterTestingDao extends DeleteDao {

	private static Logger logger = Logger.getLogger(DailyWaterTestingDao.class);

	@Autowired
	private DBConnection dbConnection;

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbNameService;

	public int addDailyWaterTesting(
			DailyWaterTestingModel dailyWaterTestingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterTesting -- START");
		}

		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "insert into daily_water_testing(ulb_id,date_of_report,no_bact_samp_tested,no_bact_samp_found_ok,no_other_samp_tested,no_other_samp_found_ok,no_bact_samp_defet,no_other_samp_defet,remarks,user_id,entry_date,IsApproved) values(?,?,?,?,?,?,?,?,?,?,str_to_date(?,'%Y-%m-%d %H:%i:%s'),?) ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, dailyWaterTestingModel.getUlbId());
			pst.setString(2, dailyWaterTestingModel.getDate_of_report());
			pst.setDouble(3, dailyWaterTestingModel.getNo_bact_samp_tested());
			pst.setDouble(4, dailyWaterTestingModel.getNo_bact_samp_found_ok());
			pst.setDouble(5, dailyWaterTestingModel.getNo_other_samp_tested());
			pst.setDouble(6, dailyWaterTestingModel.getNo_other_samp_found_ok());
			pst.setDouble(7, dailyWaterTestingModel.getNo_bact_samp_defet());
			pst.setDouble(8, dailyWaterTestingModel.getNo_other_samp_defet());
			pst.setString(9, dailyWaterTestingModel.getRemarks());
			pst.setInt(10, dailyWaterTestingModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(11, Util.getCurrentDateTime());
			pst.setInt(12, dailyWaterTestingModel.getIsApproved());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for addDailyWaterTesting");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for addDailyWaterTesting"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterTesting -- END");
		}

		return result;

	}

	public LinkedList<DailyWaterTestingModel> fetchDailyWaterTesting(LoginUsersModel loginUsersModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTesting -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = new LinkedList<DailyWaterTestingModel>();
		try {
			
			String query = "";
			if(loginUsersModel.getUserLevelModel().getUser_level_id() == 4) { // ULB User
				
				query = "select dwt.wt_id,dwt.ulb_id,dwt.date_of_report,dwt.no_bact_samp_tested,dwt.no_bact_samp_found_ok,dwt.no_other_samp_tested,dwt.no_other_samp_found_ok,dwt.no_bact_samp_defet,dwt.no_other_samp_defet,dwt.remarks,dwt.user_id,dwt.entry_date,dwt.modify_date,dwt.IsApproved,un.ulb_Name from daily_water_testing dwt,ulbs_name un where dwt.ulb_id = un.ulb_id and dwt.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dwt.ulb_id ="+loginUsersModel.getUlbNameModel().getUlb_id()+" order by dwt.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 3) { // District User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int districtId = (Integer)upperLevels.get(2);
				LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(districtId);
				
				String ulbs = "";
				for(ULBNameModel ulbNameModel:ulbNameModels) {
					
					ulbs = ulbs +ulbNameModel.getUlb_id()+",";
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select dwt.wt_id,dwt.ulb_id,dwt.date_of_report,dwt.no_bact_samp_tested,dwt.no_bact_samp_found_ok,dwt.no_other_samp_tested,dwt.no_other_samp_found_ok,dwt.no_bact_samp_defet,dwt.no_other_samp_defet,dwt.remarks,dwt.user_id,dwt.entry_date,dwt.modify_date,dwt.IsApproved,un.ulb_Name from daily_water_testing dwt,ulbs_name un where dwt.ulb_id = un.ulb_id and dwt.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dwt.ulb_id in ("+ulbs+")  order by dwt.entry_date desc";
			
			} else if(loginUsersModel.getUserLevelModel().getUser_level_id() == 2) { // Division User
				
				LinkedList upperLevels = districtService.fetchUpperLevelsByULBId(loginUsersModel.getUlbNameModel().getUlb_id());
				int divisionId = (Integer)upperLevels.get(3);
				LinkedList<PhDistrictModel> districtModels = districtService.fetchDistrictByDivisionId(divisionId);
				
				String ulbs = "";
				for(PhDistrictModel phDistrictModel:districtModels) {
					
					LinkedList<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByDistrictId(phDistrictModel.getDist_id());
					
					for(ULBNameModel ulbNameModel:ulbNameModels) {
						
						ulbs = ulbs +ulbNameModel.getUlb_id()+",";
					}
					
				}
				
				ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));
				
				query = "select dwt.wt_id,dwt.ulb_id,dwt.date_of_report,dwt.no_bact_samp_tested,dwt.no_bact_samp_found_ok,dwt.no_other_samp_tested,dwt.no_other_samp_found_ok,dwt.no_bact_samp_defet,dwt.no_other_samp_defet,dwt.remarks,dwt.user_id,dwt.entry_date,dwt.modify_date,dwt.IsApproved,un.ulb_Name from daily_water_testing dwt,ulbs_name un where dwt.ulb_id = un.ulb_id and dwt.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') and dwt.ulb_id in ("+ulbs+")  order by dwt.entry_date desc";
			
			} else { // Others All (State, PHEO, Circle, H&UD)
				
				query = "select dwt.wt_id,dwt.ulb_id,dwt.date_of_report,dwt.no_bact_samp_tested,dwt.no_bact_samp_found_ok,dwt.no_other_samp_tested,dwt.no_other_samp_found_ok,dwt.no_bact_samp_defet,dwt.no_other_samp_defet,dwt.remarks,dwt.user_id,dwt.entry_date,dwt.modify_date,dwt.IsApproved,un.ulb_Name from daily_water_testing dwt,ulbs_name un where dwt.ulb_id = un.ulb_id and dwt.date_of_report =str_to_date(?,'%Y-%m-%d %H:%i:%s') order by dwt.entry_date desc";
			}
			
			pst = connection.prepareStatement(query);
			pst.setString(1, Util.getYesterdayDateString());
			
			
			
			
			
			
			
			
			//String query = "select dwt.wt_id,dwt.ulb_id,dwt.date_of_report,dwt.no_bact_samp_tested,dwt.no_bact_samp_found_ok,dwt.no_other_samp_tested,dwt.no_other_samp_found_ok,dwt.no_bact_samp_defet,dwt.no_other_samp_defet,dwt.remarks,dwt.user_id,dwt.entry_date,dwt.modify_date,dwt.IsApproved,un.ulb_Name from daily_water_testing dwt,ulbs_name un where dwt.ulb_id = un.ulb_id order by dwt.entry_date desc";
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyWaterTestingModel dailyWaterTestingModel = new DailyWaterTestingModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(11));
				ulbNameModel.setUlb_Name(rs.getString(15));
				dailyWaterTestingModel.setWt_id(rs.getInt(1));
				dailyWaterTestingModel.setUlbNameModel(ulbNameModel);
				dailyWaterTestingModel.setDate_of_report(rs.getString(3));
				dailyWaterTestingModel.setNo_bact_samp_tested(rs.getDouble(4));
				dailyWaterTestingModel
						.setNo_bact_samp_found_ok(rs.getDouble(5));
				dailyWaterTestingModel.setNo_other_samp_tested(rs.getDouble(6));
				dailyWaterTestingModel.setNo_other_samp_found_ok(rs
						.getDouble(7));
				dailyWaterTestingModel.setNo_bact_samp_defet(rs.getDouble(8));
				dailyWaterTestingModel.setNo_other_samp_defet(rs.getDouble(9));
				dailyWaterTestingModel.setRemarks(rs.getString(10));
				dailyWaterTestingModel.setLoginUsersModel(loginUsersModel2);
				dailyWaterTestingModel.setEntry_date(rs.getString(12));
				dailyWaterTestingModel.setModify_date(rs.getString(13));
				dailyWaterTestingModel.setIsApproved(rs.getInt(14));

				dailyWaterTestingModels.add(dailyWaterTestingModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterTesting");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterTesting"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTesting -- END");
		}

		return dailyWaterTestingModels;

	}

	public DailyWaterTestingModel fetchDailyWaterTestingById(int wt_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTestingById -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		DailyWaterTestingModel dailyWaterTestingModel = new DailyWaterTestingModel();
		try {
			String query = "select wt_id,ulb_id,date_of_report,no_bact_samp_tested,no_bact_samp_found_ok,no_other_samp_tested,no_other_samp_found_ok,no_bact_samp_defet,no_other_samp_defet,remarks,user_id,entry_date,modify_date,IsApproved from daily_water_testing where wt_id =? order by entry_date desc";
			pst = connection.prepareStatement(query);
			pst.setInt(1, wt_id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));

				LoginUsersModel loginUsersModel = new LoginUsersModel();
				loginUsersModel.setUser_id(rs.getInt(11));

				dailyWaterTestingModel.setWt_id(rs.getInt(1));
				dailyWaterTestingModel.setUlbNameModel(ulbNameModel);
				dailyWaterTestingModel.setDate_of_report(rs.getString(3));
				dailyWaterTestingModel.setNo_bact_samp_tested(rs.getDouble(4));
				dailyWaterTestingModel
						.setNo_bact_samp_found_ok(rs.getDouble(5));
				dailyWaterTestingModel.setNo_other_samp_tested(rs.getDouble(6));
				dailyWaterTestingModel.setNo_other_samp_found_ok(rs
						.getDouble(7));
				dailyWaterTestingModel.setNo_bact_samp_defet(rs.getDouble(8));
				dailyWaterTestingModel.setNo_other_samp_defet(rs.getDouble(9));
				dailyWaterTestingModel.setRemarks(rs.getString(10));
				dailyWaterTestingModel.setLoginUsersModel(loginUsersModel);
				dailyWaterTestingModel.setEntry_date(rs.getString(12));
				dailyWaterTestingModel.setModify_date(rs.getString(13));
				dailyWaterTestingModel.setIsApproved(rs.getInt(14));

			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchDailyWaterTestingById");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchDailyWaterTestingById"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTestingById -- END");
		}

		return dailyWaterTestingModel;

	}

	public int updateDailyWaterTesting(
			DailyWaterTestingModel dailyWaterTestingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterTesting -- START");
		}
		int result = 0;

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;

		try {
			String query = "update daily_water_testing set ulb_id=?,date_of_report=?,no_bact_samp_tested=?,no_bact_samp_found_ok=?,no_other_samp_tested=?,no_other_samp_found_ok=?,no_bact_samp_defet=?,no_other_samp_defet=?,remarks=?,user_id=?,modify_date=str_to_date(?,'%Y-%m-%d %H:%i:%s') where wt_id=? ";
			pst = connection.prepareStatement(query);
			pst.setInt(1, dailyWaterTestingModel.getUlbId());
			pst.setString(2, dailyWaterTestingModel.getDate_of_report());
			pst.setDouble(3, dailyWaterTestingModel.getNo_bact_samp_tested());
			pst.setDouble(4, dailyWaterTestingModel.getNo_bact_samp_found_ok());
			pst.setDouble(5, dailyWaterTestingModel.getNo_other_samp_tested());
			pst.setDouble(6, dailyWaterTestingModel.getNo_other_samp_found_ok());
			pst.setDouble(7, dailyWaterTestingModel.getNo_bact_samp_defet());
			pst.setDouble(8, dailyWaterTestingModel.getNo_other_samp_defet());
			pst.setString(9, dailyWaterTestingModel.getRemarks());
			pst.setInt(10, dailyWaterTestingModel.getLoginUsersModel()
					.getUser_id());
			pst.setString(11, Util.getCurrentDateTime());
			pst.setInt(12, dailyWaterTestingModel.getWt_id());

			result = pst.executeUpdate();
			if (result > 0)
				result = 1;
			else
				result = 0;
			connection.commit();
		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for updateDailyWaterTesting");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for updateDailyWaterTesting"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterTesting -- END");
		}

		return result;

	}
	
	public LinkedList<DailyWaterTestingModel> fetchAJAXDailyWaterTesting(ListingModel listingModel)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterTesting -- START");
		}

		Connection connection = dbConnection.Connect();
		PreparedStatement pst = null;
		LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = new LinkedList<DailyWaterTestingModel>();
		try {

			String query = "";

			if (listingModel.getDivisionId() == null) { // Only For Date

				query = "select dwt.wt_id,dwt.ulb_id,dwt.date_of_report,dwt.no_bact_samp_tested,dwt.no_bact_samp_found_ok,dwt.no_other_samp_tested,dwt.no_other_samp_found_ok,dwt.no_bact_samp_defet,dwt.no_other_samp_defet,dwt.remarks,dwt.user_id,dwt.entry_date,dwt.modify_date,dwt.IsApproved,un.ulb_Name from daily_water_testing dwt,ulbs_name un where dwt.ulb_id = un.ulb_id and dwt.date_of_report =(case '"
						+ listingModel.getDate()
						+ "' when 'null' then dwt.date_of_report else str_to_date('"
						+ listingModel.getDate()
						+ "','%Y-%m-%d %H:%i:%s') end) order by dwt.entry_date desc";

			} else {

				if (listingModel.getDistrictId() == null) { // Only For Division

					LinkedList<PhDistrictModel> districtModels = districtService
							.fetchDistrictByDivisionId(listingModel
									.getDivisionId());

					String ulbs = "";
					for (PhDistrictModel phDistrictModel : districtModels) {

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(phDistrictModel
										.getDist_id());

						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

					}

					ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

					query = "select dwt.wt_id,dwt.ulb_id,dwt.date_of_report,dwt.no_bact_samp_tested,dwt.no_bact_samp_found_ok,dwt.no_other_samp_tested,dwt.no_other_samp_found_ok,dwt.no_bact_samp_defet,dwt.no_other_samp_defet,dwt.remarks,dwt.user_id,dwt.entry_date,dwt.modify_date,dwt.IsApproved,un.ulb_Name from daily_water_testing dwt,ulbs_name un where dwt.ulb_id = un.ulb_id and dwt.date_of_report =(case '"
							+ listingModel.getDate()
							+ "' when 'null' then dwt.date_of_report else str_to_date('"
							+ listingModel.getDate()
							+ "','%Y-%m-%d %H:%i:%s') end) and dwt.ulb_id in ("
							+ ulbs + ") order by dwt.entry_date desc";

				} else {

					if (listingModel.getUlbId() == null) { // Only For District

						LinkedList<ULBNameModel> ulbNameModels = ulbNameService
								.fetchULBNameByDistrictId(listingModel
										.getDistrictId());

						String ulbs = "";
						for (ULBNameModel ulbNameModel : ulbNameModels) {

							ulbs = ulbs + ulbNameModel.getUlb_id() + ",";
						}

						ulbs = ulbs.substring(0, ulbs.lastIndexOf(","));

						query = "select dwt.wt_id,dwt.ulb_id,dwt.date_of_report,dwt.no_bact_samp_tested,dwt.no_bact_samp_found_ok,dwt.no_other_samp_tested,dwt.no_other_samp_found_ok,dwt.no_bact_samp_defet,dwt.no_other_samp_defet,dwt.remarks,dwt.user_id,dwt.entry_date,dwt.modify_date,dwt.IsApproved,un.ulb_Name from daily_water_testing dwt,ulbs_name un where dwt.ulb_id = un.ulb_id and dwt.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dwt.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dwt.ulb_id in ("
								+ ulbs + ") order by dwt.entry_date desc";

					} else { // Only For ULB

						query = "select dwt.wt_id,dwt.ulb_id,dwt.date_of_report,dwt.no_bact_samp_tested,dwt.no_bact_samp_found_ok,dwt.no_other_samp_tested,dwt.no_other_samp_found_ok,dwt.no_bact_samp_defet,dwt.no_other_samp_defet,dwt.remarks,dwt.user_id,dwt.entry_date,dwt.modify_date,dwt.IsApproved,un.ulb_Name from daily_water_testing dwt,ulbs_name un where dwt.ulb_id = un.ulb_id and dwt.date_of_report =(case '"
								+ listingModel.getDate()
								+ "' when 'null' then dwt.date_of_report else str_to_date('"
								+ listingModel.getDate()
								+ "','%Y-%m-%d %H:%i:%s') end) and dwt.ulb_id = "
								+ listingModel.getUlbId()
								+ " order by dwt.entry_date desc";
					}

				}

			}
			
			
			pst = connection.prepareStatement(query);
			//String query = "select dwt.wt_id,dwt.ulb_id,dwt.date_of_report,dwt.no_bact_samp_tested,dwt.no_bact_samp_found_ok,dwt.no_other_samp_tested,dwt.no_other_samp_found_ok,dwt.no_bact_samp_defet,dwt.no_other_samp_defet,dwt.remarks,dwt.user_id,dwt.entry_date,dwt.modify_date,dwt.IsApproved,un.ulb_Name from daily_water_testing dwt,ulbs_name un where dwt.ulb_id = un.ulb_id order by dwt.entry_date desc";
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				DailyWaterTestingModel dailyWaterTestingModel = new DailyWaterTestingModel();

				ULBNameModel ulbNameModel = new ULBNameModel();
				ulbNameModel.setUlb_id(rs.getInt(2));

				LoginUsersModel loginUsersModel2 = new LoginUsersModel();
				loginUsersModel2.setUser_id(rs.getInt(11));
				ulbNameModel.setUlb_Name(rs.getString(15));
				dailyWaterTestingModel.setWt_id(rs.getInt(1));
				dailyWaterTestingModel.setUlbNameModel(ulbNameModel);
				dailyWaterTestingModel.setDate_of_report(rs.getString(3));
				dailyWaterTestingModel.setNo_bact_samp_tested(rs.getDouble(4));
				dailyWaterTestingModel
						.setNo_bact_samp_found_ok(rs.getDouble(5));
				dailyWaterTestingModel.setNo_other_samp_tested(rs.getDouble(6));
				dailyWaterTestingModel.setNo_other_samp_found_ok(rs
						.getDouble(7));
				dailyWaterTestingModel.setNo_bact_samp_defet(rs.getDouble(8));
				dailyWaterTestingModel.setNo_other_samp_defet(rs.getDouble(9));
				dailyWaterTestingModel.setRemarks(rs.getString(10));
				dailyWaterTestingModel.setLoginUsersModel(loginUsersModel2);
				dailyWaterTestingModel.setEntry_date(rs.getString(12));
				dailyWaterTestingModel.setModify_date(rs.getString(13));
				dailyWaterTestingModel.setIsApproved(rs.getInt(14));

				dailyWaterTestingModels.add(dailyWaterTestingModel);
			}

		} finally {

			try { // Closing Connection Object
				if (connection != null) {

					if (!connection.isClosed())
						connection.close();

					if (logger.isDebugEnabled()) {
						logger.debug("Connection Closed for fetchAJAXDailyWaterTesting");
					}
				}
			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("Connection not closed for fetchAJAXDailyWaterTesting"
							+ e.getMessage());
				}

			}

		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterTesting -- END");
		}

		return dailyWaterTestingModels;

	}
}
