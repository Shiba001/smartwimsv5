package com.orissa.exception;

public class DBConnectionException extends Exception {

	private static final long serialVersionUID = 1L;

	private String name;

	public DBConnectionException(String name) {
		super(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
