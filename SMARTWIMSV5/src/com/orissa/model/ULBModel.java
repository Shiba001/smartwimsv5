package com.orissa.model;

public class ULBModel {

	private int m_ulb_id;
	private AmrutTypeModel amrutTypeModel;
	private ULBTypeModel ulbTypeModel;
	private PhDistrictModel phDistrictModel;
	private String ulb_name;
	private int ulb_pop;
	private String latitude;
	private String longitude;
	private String dataEntryStatus;
	private String dataApprovalStatus;

	public int getM_ulb_id() {
		return m_ulb_id;
	}

	public void setM_ulb_id(int m_ulb_id) {
		this.m_ulb_id = m_ulb_id;
	}

	public AmrutTypeModel getAmrutTypeModel() {
		return amrutTypeModel;
	}

	public void setAmrutTypeModel(AmrutTypeModel amrutTypeModel) {
		this.amrutTypeModel = amrutTypeModel;
	}

	public ULBTypeModel getUlbTypeModel() {
		return ulbTypeModel;
	}

	public void setUlbTypeModel(ULBTypeModel ulbTypeModel) {
		this.ulbTypeModel = ulbTypeModel;
	}

	public PhDistrictModel getPhDistrictModel() {
		return phDistrictModel;
	}

	public void setPhDistrictModel(PhDistrictModel phDistrictModel) {
		this.phDistrictModel = phDistrictModel;
	}

	public String getUlb_name() {
		return ulb_name;
	}

	public void setUlb_name(String ulb_name) {
		this.ulb_name = ulb_name;
	}

	public int getUlb_pop() {
		return ulb_pop;
	}

	public void setUlb_pop(int ulb_pop) {
		this.ulb_pop = ulb_pop;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDataEntryStatus() {
		return dataEntryStatus;
	}

	public void setDataEntryStatus(String dataEntryStatus) {
		this.dataEntryStatus = dataEntryStatus;
	}

	public String getDataApprovalStatus() {
		return dataApprovalStatus;
	}

	public void setDataApprovalStatus(String dataApprovalStatus) {
		this.dataApprovalStatus = dataApprovalStatus;
	}

}
