package com.orissa.model;

public class PhysicalInfraLengthOfPipeModel extends CommonModel {

	private Integer phyInfra_lpid;
	private String date_of_report;
	private Integer ulb_id;
	private Double rising_main;
	private Double distr_netw;
	
	private String phyInfraLpid;

	public String getPhyInfraLpid() {
		return phyInfraLpid;
	}

	public void setPhyInfraLpid(String phyInfraLpid) {
		this.phyInfraLpid = phyInfraLpid;
	}

	public Integer getPhyInfra_lpid() {
		return phyInfra_lpid;
	}

	public void setPhyInfra_lpid(Integer phyInfra_lpid) {
		this.phyInfra_lpid = phyInfra_lpid;
	}

	public String getDate_of_report() {
		return date_of_report;
	}

	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}

	public Integer getUlb_id() {
		return ulb_id;
	}

	public void setUlb_id(Integer ulb_id) {
		this.ulb_id = ulb_id;
	}

	public Double getRising_main() {
		return rising_main;
	}

	public void setRising_main(Double rising_main) {
		this.rising_main = rising_main;
	}

	public Double getDistr_netw() {
		return distr_netw;
	}

	public void setDistr_netw(Double distr_netw) {
		this.distr_netw = distr_netw;
	}

}
