package com.orissa.model;

public class PhysicalInfraConnectionModel extends CommonModel {

	private Integer phyInfra_con_id;
	private String date_of_report;
	private ULBModel ulbModel;
	private Double no_res_cons;
	private Double no_ind_cons;
	private Double no_com_cons;
	private Double no_inst_cons;
	private Double tot_no_cons;
	
	private String phyInfraConId;
	
	public String getPhyInfraConId() {
		return phyInfraConId;
	}

	public void setPhyInfraConId(String phyInfraConId) {
		this.phyInfraConId = phyInfraConId;
	}

	public Integer getPhyInfra_con_id() {
		return phyInfra_con_id;
	}

	public void setPhyInfra_con_id(Integer phyInfra_con_id) {
		this.phyInfra_con_id = phyInfra_con_id;
	}

	public String getDate_of_report() {
		return date_of_report;
	}

	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}

	public ULBModel getUlbModel() {
		return ulbModel;
	}

	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}

	public Double getNo_res_cons() {
		return no_res_cons;
	}

	public void setNo_res_cons(Double no_res_cons) {
		this.no_res_cons = no_res_cons;
	}

	public Double getNo_ind_cons() {
		return no_ind_cons;
	}

	public void setNo_ind_cons(Double no_ind_cons) {
		this.no_ind_cons = no_ind_cons;
	}

	public Double getNo_com_cons() {
		return no_com_cons;
	}

	public void setNo_com_cons(Double no_com_cons) {
		this.no_com_cons = no_com_cons;
	}

	public Double getNo_inst_cons() {
		return no_inst_cons;
	}

	public void setNo_inst_cons(Double no_inst_cons) {
		this.no_inst_cons = no_inst_cons;
	}

	public Double getTot_no_cons() {
		return tot_no_cons;
	}

	public void setTot_no_cons(Double tot_no_cons) {
		this.tot_no_cons = tot_no_cons;
	}

}
