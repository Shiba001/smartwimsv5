package com.orissa.model;

public class DailyStandpostsModel extends CommonModel {

	private int d_s_p_id;
	private int no_new_s_p_added;
	private int no_s_p_removed;

	public int getD_s_p_id() {
		return d_s_p_id;
	}

	public void setD_s_p_id(int d_s_p_id) {
		this.d_s_p_id = d_s_p_id;
	}

	public int getNo_new_s_p_added() {
		return no_new_s_p_added;
	}

	public void setNo_new_s_p_added(int no_new_s_p_added) {
		this.no_new_s_p_added = no_new_s_p_added;
	}

	public int getNo_s_p_removed() {
		return no_s_p_removed;
	}

	public void setNo_s_p_removed(int no_s_p_removed) {
		this.no_s_p_removed = no_s_p_removed;
	}

}
