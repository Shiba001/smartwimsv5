package com.orissa.model;

import java.util.HashMap;
import java.util.LinkedList;

public class AjaxDataEntryModel {

	private int complete;
	private int partial;
	private int notApproved;
	private int approved;
	private int noDataEntry;
	private HashMap<String, HashMap<String, ULBModel>> dataEntryStatus;
	private LinkedList<PhDivisionModel> phDivisionModels;

	public int getComplete() {
		return complete;
	}

	public void setComplete(int complete) {
		this.complete = complete;
	}

	public int getPartial() {
		return partial;
	}

	public void setPartial(int partial) {
		this.partial = partial;
	}

	public int getNotApproved() {
		return notApproved;
	}

	public void setNotApproved(int notApproved) {
		this.notApproved = notApproved;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}

	public int getNoDataEntry() {
		return noDataEntry;
	}

	public void setNoDataEntry(int noDataEntry) {
		this.noDataEntry = noDataEntry;
	}

	public HashMap<String, HashMap<String, ULBModel>> getDataEntryStatus() {
		return dataEntryStatus;
	}

	public void setDataEntryStatus(
			HashMap<String, HashMap<String, ULBModel>> dataEntryStatus) {
		this.dataEntryStatus = dataEntryStatus;
	}

	public LinkedList<PhDivisionModel> getPhDivisionModels() {
		return phDivisionModels;
	}

	public void setPhDivisionModels(LinkedList<PhDivisionModel> phDivisionModels) {
		this.phDivisionModels = phDivisionModels;
	}

}
