package com.orissa.model;

public class PhysicalInfraMeteringModel extends CommonModel {

	private int phyInfra_mtr_id;
	private String date_of_report;
	private ULBModel ulbModel;
	private double no_res_mtr;
	private double no_ind_mtr;
	private double no_com_mtr;
	private double no_inst_mtr;
	private double tot_con_mtr;

	public int getPhyInfra_mtr_id() {
		return phyInfra_mtr_id;
	}

	public void setPhyInfra_mtr_id(int phyInfra_mtr_id) {
		this.phyInfra_mtr_id = phyInfra_mtr_id;
	}

	public String getDate_of_report() {
		return date_of_report;
	}

	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}

	public ULBModel getUlbModel() {
		return ulbModel;
	}

	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}

	public double getNo_res_mtr() {
		return no_res_mtr;
	}

	public void setNo_res_mtr(double no_res_mtr) {
		this.no_res_mtr = no_res_mtr;
	}

	public double getNo_ind_mtr() {
		return no_ind_mtr;
	}

	public void setNo_ind_mtr(double no_ind_mtr) {
		this.no_ind_mtr = no_ind_mtr;
	}

	public double getNo_com_mtr() {
		return no_com_mtr;
	}

	public void setNo_com_mtr(double no_com_mtr) {
		this.no_com_mtr = no_com_mtr;
	}

	public double getNo_inst_mtr() {
		return no_inst_mtr;
	}

	public void setNo_inst_mtr(double no_inst_mtr) {
		this.no_inst_mtr = no_inst_mtr;
	}

	public double getTot_con_mtr() {
		return tot_con_mtr;
	}

	public void setTot_con_mtr(double tot_con_mtr) {
		this.tot_con_mtr = tot_con_mtr;
	}

}
