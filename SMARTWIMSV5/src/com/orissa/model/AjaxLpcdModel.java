package com.orissa.model;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class AjaxLpcdModel {
	
	private LinkedHashMap<String, DailyWaterSupplyModel> lpcdMap;
	private List<String> top10ulb;
	private List<Double> top10lpcd;
	private List<String> bottom10ulb;
	private List<Double> bottom10lpcd;
	private List<String> middleulb1;
	private List<Double> middlelpcd1;
	private List<String> middleulb2;
	private List<Double> middlelpcd2;
	private List<String> datesBetween;
	private List<Double> currentLPCD;
	private List<Double> previousLPCD;
	private Double lpcdBoxValue;
	private Integer topValue;

	private List<String> topulb;
	private List<Double> toplpcd;
	private List<String> bottomulb;
	private List<Double> bottomlpcd;
	private boolean flag;
	private LinkedList<PhDivisionModel> phDivisionModels;

	public LinkedHashMap<String, DailyWaterSupplyModel> getLpcdMap() {
		return lpcdMap;
	}

	public void setLpcdMap(LinkedHashMap<String, DailyWaterSupplyModel> lpcdMap) {
		this.lpcdMap = lpcdMap;
	}

	public List<String> getTop10ulb() {
		return top10ulb;
	}

	public void setTop10ulb(List<String> top10ulb) {
		this.top10ulb = top10ulb;
	}

	public List<Double> getTop10lpcd() {
		return top10lpcd;
	}

	public void setTop10lpcd(List<Double> top10lpcd) {
		this.top10lpcd = top10lpcd;
	}

	public List<String> getBottom10ulb() {
		return bottom10ulb;
	}

	public void setBottom10ulb(List<String> bottom10ulb) {
		this.bottom10ulb = bottom10ulb;
	}

	public List<Double> getBottom10lpcd() {
		return bottom10lpcd;
	}

	public void setBottom10lpcd(List<Double> bottom10lpcd) {
		this.bottom10lpcd = bottom10lpcd;
	}

	public List<String> getMiddleulb1() {
		return middleulb1;
	}

	public void setMiddleulb1(List<String> middleulb1) {
		this.middleulb1 = middleulb1;
	}

	public List<Double> getMiddlelpcd1() {
		return middlelpcd1;
	}

	public void setMiddlelpcd1(List<Double> middlelpcd1) {
		this.middlelpcd1 = middlelpcd1;
	}

	public List<String> getMiddleulb2() {
		return middleulb2;
	}

	public void setMiddleulb2(List<String> middleulb2) {
		this.middleulb2 = middleulb2;
	}

	public List<Double> getMiddlelpcd2() {
		return middlelpcd2;
	}

	public void setMiddlelpcd2(List<Double> middlelpcd2) {
		this.middlelpcd2 = middlelpcd2;
	}

	public List<String> getDatesBetween() {
		return datesBetween;
	}

	public void setDatesBetween(List<String> datesBetween) {
		this.datesBetween = datesBetween;
	}

	public List<Double> getCurrentLPCD() {
		return currentLPCD;
	}

	public void setCurrentLPCD(List<Double> currentLPCD) {
		this.currentLPCD = currentLPCD;
	}

	public List<Double> getPreviousLPCD() {
		return previousLPCD;
	}

	public void setPreviousLPCD(List<Double> previousLPCD) {
		this.previousLPCD = previousLPCD;
	}

	public Double getLpcdBoxValue() {
		return lpcdBoxValue;
	}

	public void setLpcdBoxValue(Double lpcdBoxValue) {
		this.lpcdBoxValue = lpcdBoxValue;
	}

	public Integer getTopValue() {
		return topValue;
	}

	public void setTopValue(Integer topValue) {
		this.topValue = topValue;
	}

	public List<String> getTopulb() {
		return topulb;
	}

	public void setTopulb(List<String> topulb) {
		this.topulb = topulb;
	}

	public List<Double> getToplpcd() {
		return toplpcd;
	}

	public void setToplpcd(List<Double> toplpcd) {
		this.toplpcd = toplpcd;
	}

	public List<String> getBottomulb() {
		return bottomulb;
	}

	public void setBottomulb(List<String> bottomulb) {
		this.bottomulb = bottomulb;
	}

	public List<Double> getBottomlpcd() {
		return bottomlpcd;
	}

	public void setBottomlpcd(List<Double> bottomlpcd) {
		this.bottomlpcd = bottomlpcd;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public LinkedList<PhDivisionModel> getPhDivisionModels() {
		return phDivisionModels;
	}

	public void setPhDivisionModels(LinkedList<PhDivisionModel> phDivisionModels) {
		this.phDivisionModels = phDivisionModels;
	}

}
