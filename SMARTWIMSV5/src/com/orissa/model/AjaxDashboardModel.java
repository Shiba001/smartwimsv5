package com.orissa.model;

import java.util.LinkedList;

public class AjaxDashboardModel {
	private double lpcdBoxValue;
	private double waterSupplyCoverageBoxValue;
	private double surplusDeficitBoxValue;
	private double avgHrOfSupplyBoxValue;
	private double testingBoxValue;
	private int popServedBoxValue;
	private double pipeWaterSupplyBoxValue;
	private int ulbGreater135;
	private int ulbLesser135;
	private int waterSupplyCoverageGreater100;
	private int waterSupplyCoverageGreater75;
	private int waterSupplyCoverageGreater50;
	private int waterSupplyCoverageLesser50;
	private int deficit;
	private int surplus;
	private int avgGreater;
	private int avgLesser;
	private double totalPercentageBoxValue;
	private int collectionGreater100;
	private int collectionGreater75;
	private int collectionGreater50;
	private int collectionLesser50;
	private int complete;
	private int partial;
	private int notApproved;
	private int approved;
	private int noDataEntry;
	private double complaintBoxValue;
	private double complaintResolved;
	private double complaintUnResolved;
	private double tankerBoxValue;
	private double frcFoundOK;
	private double bacteriologicalFoundOK;
	private double frcFoundNotOK;
	private double bacteriologicalFoundNotOK;
	private LinkedList<PhDivisionModel> phDivisionModels;

	public double getLpcdBoxValue() {
		return lpcdBoxValue;
	}

	public void setLpcdBoxValue(double lpcdBoxValue) {
		this.lpcdBoxValue = lpcdBoxValue;
	}

	public double getWaterSupplyCoverageBoxValue() {
		return waterSupplyCoverageBoxValue;
	}

	public void setWaterSupplyCoverageBoxValue(
			double waterSupplyCoverageBoxValue) {
		this.waterSupplyCoverageBoxValue = waterSupplyCoverageBoxValue;
	}

	public double getSurplusDeficitBoxValue() {
		return surplusDeficitBoxValue;
	}

	public void setSurplusDeficitBoxValue(double surplusDeficitBoxValue) {
		this.surplusDeficitBoxValue = surplusDeficitBoxValue;
	}

	public double getAvgHrOfSupplyBoxValue() {
		return avgHrOfSupplyBoxValue;
	}

	public void setAvgHrOfSupplyBoxValue(double avgHrOfSupplyBoxValue) {
		this.avgHrOfSupplyBoxValue = avgHrOfSupplyBoxValue;
	}

	public double getTestingBoxValue() {
		return testingBoxValue;
	}

	public void setTestingBoxValue(double testingBoxValue) {
		this.testingBoxValue = testingBoxValue;
	}

	public int getPopServedBoxValue() {
		return popServedBoxValue;
	}

	public void setPopServedBoxValue(int popServedBoxValue) {
		this.popServedBoxValue = popServedBoxValue;
	}

	public double getPipeWaterSupplyBoxValue() {
		return pipeWaterSupplyBoxValue;
	}

	public void setPipeWaterSupplyBoxValue(double pipeWaterSupplyBoxValue) {
		this.pipeWaterSupplyBoxValue = pipeWaterSupplyBoxValue;
	}

	public int getUlbGreater135() {
		return ulbGreater135;
	}

	public void setUlbGreater135(int ulbGreater135) {
		this.ulbGreater135 = ulbGreater135;
	}

	public int getUlbLesser135() {
		return ulbLesser135;
	}

	public void setUlbLesser135(int ulbLesser135) {
		this.ulbLesser135 = ulbLesser135;
	}

	public int getWaterSupplyCoverageGreater100() {
		return waterSupplyCoverageGreater100;
	}

	public void setWaterSupplyCoverageGreater100(
			int waterSupplyCoverageGreater100) {
		this.waterSupplyCoverageGreater100 = waterSupplyCoverageGreater100;
	}

	public int getWaterSupplyCoverageGreater75() {
		return waterSupplyCoverageGreater75;
	}

	public void setWaterSupplyCoverageGreater75(int waterSupplyCoverageGreater75) {
		this.waterSupplyCoverageGreater75 = waterSupplyCoverageGreater75;
	}

	public int getWaterSupplyCoverageGreater50() {
		return waterSupplyCoverageGreater50;
	}

	public void setWaterSupplyCoverageGreater50(int waterSupplyCoverageGreater50) {
		this.waterSupplyCoverageGreater50 = waterSupplyCoverageGreater50;
	}

	public int getWaterSupplyCoverageLesser50() {
		return waterSupplyCoverageLesser50;
	}

	public void setWaterSupplyCoverageLesser50(int waterSupplyCoverageLesser50) {
		this.waterSupplyCoverageLesser50 = waterSupplyCoverageLesser50;
	}

	public int getDeficit() {
		return deficit;
	}

	public void setDeficit(int deficit) {
		this.deficit = deficit;
	}

	public int getSurplus() {
		return surplus;
	}

	public void setSurplus(int surplus) {
		this.surplus = surplus;
	}

	public int getAvgGreater() {
		return avgGreater;
	}

	public void setAvgGreater(int avgGreater) {
		this.avgGreater = avgGreater;
	}

	public int getAvgLesser() {
		return avgLesser;
	}

	public void setAvgLesser(int avgLesser) {
		this.avgLesser = avgLesser;
	}

	public double getTotalPercentageBoxValue() {
		return totalPercentageBoxValue;
	}

	public void setTotalPercentageBoxValue(double totalPercentageBoxValue) {
		this.totalPercentageBoxValue = totalPercentageBoxValue;
	}

	public int getCollectionGreater100() {
		return collectionGreater100;
	}

	public void setCollectionGreater100(int collectionGreater100) {
		this.collectionGreater100 = collectionGreater100;
	}

	public int getCollectionGreater75() {
		return collectionGreater75;
	}

	public void setCollectionGreater75(int collectionGreater75) {
		this.collectionGreater75 = collectionGreater75;
	}

	public int getCollectionGreater50() {
		return collectionGreater50;
	}

	public void setCollectionGreater50(int collectionGreater50) {
		this.collectionGreater50 = collectionGreater50;
	}

	public int getCollectionLesser50() {
		return collectionLesser50;
	}

	public void setCollectionLesser50(int collectionLesser50) {
		this.collectionLesser50 = collectionLesser50;
	}

	public int getComplete() {
		return complete;
	}

	public void setComplete(int complete) {
		this.complete = complete;
	}

	public int getPartial() {
		return partial;
	}

	public void setPartial(int partial) {
		this.partial = partial;
	}

	public int getNotApproved() {
		return notApproved;
	}

	public void setNotApproved(int notApproved) {
		this.notApproved = notApproved;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}

	public int getNoDataEntry() {
		return noDataEntry;
	}

	public void setNoDataEntry(int noDataEntry) {
		this.noDataEntry = noDataEntry;
	}

	public double getComplaintBoxValue() {
		return complaintBoxValue;
	}

	public void setComplaintBoxValue(double complaintBoxValue) {
		this.complaintBoxValue = complaintBoxValue;
	}

	public double getComplaintResolved() {
		return complaintResolved;
	}

	public void setComplaintResolved(double complaintResolved) {
		this.complaintResolved = complaintResolved;
	}

	public double getComplaintUnResolved() {
		return complaintUnResolved;
	}

	public void setComplaintUnResolved(double complaintUnResolved) {
		this.complaintUnResolved = complaintUnResolved;
	}

	public double getTankerBoxValue() {
		return tankerBoxValue;
	}

	public void setTankerBoxValue(double tankerBoxValue) {
		this.tankerBoxValue = tankerBoxValue;
	}

	public double getFrcFoundOK() {
		return frcFoundOK;
	}

	public void setFrcFoundOK(double frcFoundOK) {
		this.frcFoundOK = frcFoundOK;
	}

	public double getBacteriologicalFoundOK() {
		return bacteriologicalFoundOK;
	}

	public void setBacteriologicalFoundOK(double bacteriologicalFoundOK) {
		this.bacteriologicalFoundOK = bacteriologicalFoundOK;
	}

	public double getFrcFoundNotOK() {
		return frcFoundNotOK;
	}

	public void setFrcFoundNotOK(double frcFoundNotOK) {
		this.frcFoundNotOK = frcFoundNotOK;
	}

	public double getBacteriologicalFoundNotOK() {
		return bacteriologicalFoundNotOK;
	}

	public void setBacteriologicalFoundNotOK(double bacteriologicalFoundNotOK) {
		this.bacteriologicalFoundNotOK = bacteriologicalFoundNotOK;
	}

	public LinkedList<PhDivisionModel> getPhDivisionModels() {
		return phDivisionModels;
	}

	public void setPhDivisionModels(LinkedList<PhDivisionModel> phDivisionModels) {
		this.phDivisionModels = phDivisionModels;
	}

}
