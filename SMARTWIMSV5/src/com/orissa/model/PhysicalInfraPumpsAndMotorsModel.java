package com.orissa.model;

import java.util.List;

public class PhysicalInfraPumpsAndMotorsModel extends CommonModel {

	private Integer phyInfra_pump_motor_slno;
	private String date_of_report;
	private ULBModel ulbModel;
	private String loc_of_pum_motor;
	private String cap_of_motor;
	private String head_dia;
	private String discharge;
	private String type_pump;
	private String year_inst;

	private Integer ulb_id;
	private Integer slNo;
	private String ulbName;

	private List<String> list_loc_of_pum_motor;
	private List<String> list_cap_of_motor;
	private List<String> list_head_dia;
	private List<String> list_discharge;
	private List<String> list_type_pump;
	private List<String> list_year_inst;

	private List<PhysicalInfraPumpsAndMotorsModel> pumpsAndMotorsModels;

	private String enculbId;

	public Integer getPhyInfra_pump_motor_slno() {
		return phyInfra_pump_motor_slno;
	}

	public void setPhyInfra_pump_motor_slno(Integer phyInfra_pump_motor_slno) {
		this.phyInfra_pump_motor_slno = phyInfra_pump_motor_slno;
	}

	public String getDate_of_report() {
		return date_of_report;
	}

	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}

	public ULBModel getUlbModel() {
		return ulbModel;
	}

	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}

	public String getLoc_of_pum_motor() {
		return loc_of_pum_motor;
	}

	public void setLoc_of_pum_motor(String loc_of_pum_motor) {
		this.loc_of_pum_motor = loc_of_pum_motor;
	}

	public String getCap_of_motor() {
		return cap_of_motor;
	}

	public void setCap_of_motor(String cap_of_motor) {
		this.cap_of_motor = cap_of_motor;
	}

	public String getHead_dia() {
		return head_dia;
	}

	public void setHead_dia(String head_dia) {
		this.head_dia = head_dia;
	}

	public String getDischarge() {
		return discharge;
	}

	public void setDischarge(String discharge) {
		this.discharge = discharge;
	}

	public String getType_pump() {
		return type_pump;
	}

	public void setType_pump(String type_pump) {
		this.type_pump = type_pump;
	}

	public String getYear_inst() {
		return year_inst;
	}

	public void setYear_inst(String year_inst) {
		this.year_inst = year_inst;
	}

	public Integer getUlb_id() {
		return ulb_id;
	}

	public void setUlb_id(Integer ulb_id) {
		this.ulb_id = ulb_id;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public List<String> getList_loc_of_pum_motor() {
		return list_loc_of_pum_motor;
	}

	public void setList_loc_of_pum_motor(List<String> list_loc_of_pum_motor) {
		this.list_loc_of_pum_motor = list_loc_of_pum_motor;
	}

	public List<String> getList_cap_of_motor() {
		return list_cap_of_motor;
	}

	public void setList_cap_of_motor(List<String> list_cap_of_motor) {
		this.list_cap_of_motor = list_cap_of_motor;
	}

	public List<String> getList_head_dia() {
		return list_head_dia;
	}

	public void setList_head_dia(List<String> list_head_dia) {
		this.list_head_dia = list_head_dia;
	}

	public List<String> getList_discharge() {
		return list_discharge;
	}

	public void setList_discharge(List<String> list_discharge) {
		this.list_discharge = list_discharge;
	}

	public List<String> getList_type_pump() {
		return list_type_pump;
	}

	public void setList_type_pump(List<String> list_type_pump) {
		this.list_type_pump = list_type_pump;
	}

	public List<String> getList_year_inst() {
		return list_year_inst;
	}

	public void setList_year_inst(List<String> list_year_inst) {
		this.list_year_inst = list_year_inst;
	}

	public List<PhysicalInfraPumpsAndMotorsModel> getPumpsAndMotorsModels() {
		return pumpsAndMotorsModels;
	}

	public void setPumpsAndMotorsModels(
			List<PhysicalInfraPumpsAndMotorsModel> pumpsAndMotorsModels) {
		this.pumpsAndMotorsModels = pumpsAndMotorsModels;
	}

	public String getEnculbId() {
		return enculbId;
	}

	public void setEnculbId(String enculbId) {
		this.enculbId = enculbId;
	}

	public String getUlbName() {
		return ulbName;
	}

	public void setUlbName(String ulbName) {
		this.ulbName = ulbName;
	}

}
