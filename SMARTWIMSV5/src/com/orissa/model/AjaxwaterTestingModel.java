package com.orissa.model;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class AjaxwaterTestingModel {

	private double bacteriologicalBoxValue;
	private double frcBoxValue;
	private LinkedHashMap<String, DailyWaterTestingModel> lpcdMap;
	private int topValue;
	private int topValue1;
	private List<String> top10ulb;
	private List<Double> top10lpcd;
	private List<String> top10ulb1;
	private List<Double> top10lpcd1;
	private List<String> bottom10ulb;
	private List<Double> bottom10lpcd;
	private List<String> bottom10ulb1;
	private List<Double> bottom10lpcd1;
	private List<String> datesBetween;
	private List<Double> currentLPCD;
	private List<Double> previousLPCD;
	private List<Double> currentLPCD1;
	private List<Double> previousLPCD1;
	private List<String> topulb;
	private List<Double> toplpcd;
	private List<String> bottomulb;
	private List<Double> bottomlpcd;
	private List<String> topulb1;
	private List<Double> toplpcd1;
	private List<String> bottomulb1;
	private List<Double> bottomlpcd1;
	private boolean flag;
	private boolean flag1;
	private LinkedList<PhDivisionModel> phDivisionModels;

	public double getBacteriologicalBoxValue() {
		return bacteriologicalBoxValue;
	}

	public void setBacteriologicalBoxValue(double bacteriologicalBoxValue) {
		this.bacteriologicalBoxValue = bacteriologicalBoxValue;
	}

	public double getFrcBoxValue() {
		return frcBoxValue;
	}

	public void setFrcBoxValue(double frcBoxValue) {
		this.frcBoxValue = frcBoxValue;
	}

	public LinkedHashMap<String, DailyWaterTestingModel> getLpcdMap() {
		return lpcdMap;
	}

	public void setLpcdMap(LinkedHashMap<String, DailyWaterTestingModel> lpcdMap) {
		this.lpcdMap = lpcdMap;
	}

	public int getTopValue() {
		return topValue;
	}

	public void setTopValue(int topValue) {
		this.topValue = topValue;
	}

	public int getTopValue1() {
		return topValue1;
	}

	public void setTopValue1(int topValue1) {
		this.topValue1 = topValue1;
	}

	public List<String> getTop10ulb() {
		return top10ulb;
	}

	public void setTop10ulb(List<String> top10ulb) {
		this.top10ulb = top10ulb;
	}

	public List<Double> getTop10lpcd() {
		return top10lpcd;
	}

	public void setTop10lpcd(List<Double> top10lpcd) {
		this.top10lpcd = top10lpcd;
	}

	public List<String> getTop10ulb1() {
		return top10ulb1;
	}

	public void setTop10ulb1(List<String> top10ulb1) {
		this.top10ulb1 = top10ulb1;
	}

	public List<Double> getTop10lpcd1() {
		return top10lpcd1;
	}

	public void setTop10lpcd1(List<Double> top10lpcd1) {
		this.top10lpcd1 = top10lpcd1;
	}

	public List<String> getBottom10ulb() {
		return bottom10ulb;
	}

	public void setBottom10ulb(List<String> bottom10ulb) {
		this.bottom10ulb = bottom10ulb;
	}

	public List<Double> getBottom10lpcd() {
		return bottom10lpcd;
	}

	public void setBottom10lpcd(List<Double> bottom10lpcd) {
		this.bottom10lpcd = bottom10lpcd;
	}

	public List<String> getBottom10ulb1() {
		return bottom10ulb1;
	}

	public void setBottom10ulb1(List<String> bottom10ulb1) {
		this.bottom10ulb1 = bottom10ulb1;
	}

	public List<Double> getBottom10lpcd1() {
		return bottom10lpcd1;
	}

	public void setBottom10lpcd1(List<Double> bottom10lpcd1) {
		this.bottom10lpcd1 = bottom10lpcd1;
	}

	public List<String> getDatesBetween() {
		return datesBetween;
	}

	public void setDatesBetween(List<String> datesBetween) {
		this.datesBetween = datesBetween;
	}

	public List<Double> getCurrentLPCD() {
		return currentLPCD;
	}

	public void setCurrentLPCD(List<Double> currentLPCD) {
		this.currentLPCD = currentLPCD;
	}

	public List<Double> getPreviousLPCD() {
		return previousLPCD;
	}

	public void setPreviousLPCD(List<Double> previousLPCD) {
		this.previousLPCD = previousLPCD;
	}

	public List<Double> getCurrentLPCD1() {
		return currentLPCD1;
	}

	public void setCurrentLPCD1(List<Double> currentLPCD1) {
		this.currentLPCD1 = currentLPCD1;
	}

	public List<Double> getPreviousLPCD1() {
		return previousLPCD1;
	}

	public void setPreviousLPCD1(List<Double> previousLPCD1) {
		this.previousLPCD1 = previousLPCD1;
	}

	public List<String> getTopulb() {
		return topulb;
	}

	public void setTopulb(List<String> topulb) {
		this.topulb = topulb;
	}

	public List<Double> getToplpcd() {
		return toplpcd;
	}

	public void setToplpcd(List<Double> toplpcd) {
		this.toplpcd = toplpcd;
	}

	public List<String> getBottomulb() {
		return bottomulb;
	}

	public void setBottomulb(List<String> bottomulb) {
		this.bottomulb = bottomulb;
	}

	public List<Double> getBottomlpcd() {
		return bottomlpcd;
	}

	public void setBottomlpcd(List<Double> bottomlpcd) {
		this.bottomlpcd = bottomlpcd;
	}

	public List<String> getTopulb1() {
		return topulb1;
	}

	public void setTopulb1(List<String> topulb1) {
		this.topulb1 = topulb1;
	}

	public List<Double> getToplpcd1() {
		return toplpcd1;
	}

	public void setToplpcd1(List<Double> toplpcd1) {
		this.toplpcd1 = toplpcd1;
	}

	public List<String> getBottomulb1() {
		return bottomulb1;
	}

	public void setBottomulb1(List<String> bottomulb1) {
		this.bottomulb1 = bottomulb1;
	}

	public List<Double> getBottomlpcd1() {
		return bottomlpcd1;
	}

	public void setBottomlpcd1(List<Double> bottomlpcd1) {
		this.bottomlpcd1 = bottomlpcd1;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public boolean isFlag1() {
		return flag1;
	}

	public void setFlag1(boolean flag1) {
		this.flag1 = flag1;
	}

	public LinkedList<PhDivisionModel> getPhDivisionModels() {
		return phDivisionModels;
	}

	public void setPhDivisionModels(LinkedList<PhDivisionModel> phDivisionModels) {
		this.phDivisionModels = phDivisionModels;
	}

}
