package com.orissa.model;

public class AmrutTypeModel {

	private Integer Amrut_type_id;
	private String type_Name;

	public Integer getAmrut_type_id() {
		return Amrut_type_id;
	}

	public void setAmrut_type_id(Integer amrut_type_id) {
		Amrut_type_id = amrut_type_id;
	}

	public String getType_Name() {
		return type_Name;
	}

	public void setType_Name(String type_Name) {
		this.type_Name = type_Name;
	}

}
