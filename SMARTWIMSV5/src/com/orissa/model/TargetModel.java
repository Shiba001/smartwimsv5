package com.orissa.model;

public class TargetModel extends CommonModel {

	private int trgt_id;
	private ULBModel ulbModel;
	private String trgt_mode;
	private String fin_year;
	private String projects;
	private String proj_entity;
	private Double m_trgt_1;
	private Double m_trgt_2;
	private Double m_trgt_3;
	private Double m_trgt_4;
	private Double m_trgt_5;
	private Double m_trgt_6;
	private Double m_trgt_7;
	private Double m_trgt_8;
	private Double m_trgt_9;
	private Double m_trgt_10;
	private Double m_trgt_11;
	private Double m_trgt_12;
	private Double y_trgt;

	private Integer slNo;
	private Double percentage;
	private Double totalCollection;
	private Double target;
	private String trgtId;
	private Double totalChargesCollections;
	private Double totalTarget;
	private Double total_percentage;

	private Double pm_percentage;
	private Double pm_totalCollection;
	private Double pm_target;
	private Double pm_totalChargesCollections;
	private Double pm_totalTarget;
	private Double pm_total_percentage;

	public int getTrgt_id() {
		return trgt_id;
	}

	public void setTrgt_id(int trgt_id) {
		this.trgt_id = trgt_id;
	}

	public ULBModel getUlbModel() {
		return ulbModel;
	}

	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}

	public String getTrgt_mode() {
		return trgt_mode;
	}

	public void setTrgt_mode(String trgt_mode) {
		this.trgt_mode = trgt_mode;
	}

	public String getFin_year() {
		return fin_year;
	}

	public void setFin_year(String fin_year) {
		this.fin_year = fin_year;
	}

	public String getProjects() {
		return projects;
	}

	public void setProjects(String projects) {
		this.projects = projects;
	}

	public String getProj_entity() {
		return proj_entity;
	}

	public void setProj_entity(String proj_entity) {
		this.proj_entity = proj_entity;
	}

	public Double getM_trgt_1() {
		return m_trgt_1;
	}

	public void setM_trgt_1(Double m_trgt_1) {
		this.m_trgt_1 = m_trgt_1;
	}

	public Double getM_trgt_2() {
		return m_trgt_2;
	}

	public void setM_trgt_2(Double m_trgt_2) {
		this.m_trgt_2 = m_trgt_2;
	}

	public Double getM_trgt_3() {
		return m_trgt_3;
	}

	public void setM_trgt_3(Double m_trgt_3) {
		this.m_trgt_3 = m_trgt_3;
	}

	public Double getM_trgt_4() {
		return m_trgt_4;
	}

	public void setM_trgt_4(Double m_trgt_4) {
		this.m_trgt_4 = m_trgt_4;
	}

	public Double getM_trgt_5() {
		return m_trgt_5;
	}

	public void setM_trgt_5(Double m_trgt_5) {
		this.m_trgt_5 = m_trgt_5;
	}

	public Double getM_trgt_6() {
		return m_trgt_6;
	}

	public void setM_trgt_6(Double m_trgt_6) {
		this.m_trgt_6 = m_trgt_6;
	}

	public Double getM_trgt_7() {
		return m_trgt_7;
	}

	public void setM_trgt_7(Double m_trgt_7) {
		this.m_trgt_7 = m_trgt_7;
	}

	public Double getM_trgt_8() {
		return m_trgt_8;
	}

	public void setM_trgt_8(Double m_trgt_8) {
		this.m_trgt_8 = m_trgt_8;
	}

	public Double getM_trgt_9() {
		return m_trgt_9;
	}

	public void setM_trgt_9(Double m_trgt_9) {
		this.m_trgt_9 = m_trgt_9;
	}

	public Double getM_trgt_10() {
		return m_trgt_10;
	}

	public void setM_trgt_10(Double m_trgt_10) {
		this.m_trgt_10 = m_trgt_10;
	}

	public Double getM_trgt_11() {
		return m_trgt_11;
	}

	public void setM_trgt_11(Double m_trgt_11) {
		this.m_trgt_11 = m_trgt_11;
	}

	public Double getM_trgt_12() {
		return m_trgt_12;
	}

	public void setM_trgt_12(Double m_trgt_12) {
		this.m_trgt_12 = m_trgt_12;
	}

	public Double getY_trgt() {
		return y_trgt;
	}

	public void setY_trgt(Double y_trgt) {
		this.y_trgt = y_trgt;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public Double getTotalCollection() {
		return totalCollection;
	}

	public void setTotalCollection(Double totalCollection) {
		this.totalCollection = totalCollection;
	}

	public Double getTarget() {
		return target;
	}

	public void setTarget(Double target) {
		this.target = target;
	}

	public String getTrgtId() {
		return trgtId;
	}

	public void setTrgtId(String trgtId) {
		this.trgtId = trgtId;
	}

	public Double getTotalChargesCollections() {
		return totalChargesCollections;
	}

	public void setTotalChargesCollections(Double totalChargesCollections) {
		this.totalChargesCollections = totalChargesCollections;
	}

	public Double getTotalTarget() {
		return totalTarget;
	}

	public void setTotalTarget(Double totalTarget) {
		this.totalTarget = totalTarget;
	}

	public Double getTotal_percentage() {
		return total_percentage;
	}

	public void setTotal_percentage(Double total_percentage) {
		this.total_percentage = total_percentage;
	}

	public Double getPm_percentage() {
		return pm_percentage;
	}

	public void setPm_percentage(Double pm_percentage) {
		this.pm_percentage = pm_percentage;
	}

	public Double getPm_totalCollection() {
		return pm_totalCollection;
	}

	public void setPm_totalCollection(Double pm_totalCollection) {
		this.pm_totalCollection = pm_totalCollection;
	}

	public Double getPm_target() {
		return pm_target;
	}

	public void setPm_target(Double pm_target) {
		this.pm_target = pm_target;
	}

	public Double getPm_totalChargesCollections() {
		return pm_totalChargesCollections;
	}

	public void setPm_totalChargesCollections(Double pm_totalChargesCollections) {
		this.pm_totalChargesCollections = pm_totalChargesCollections;
	}

	public Double getPm_totalTarget() {
		return pm_totalTarget;
	}

	public void setPm_totalTarget(Double pm_totalTarget) {
		this.pm_totalTarget = pm_totalTarget;
	}

	public Double getPm_total_percentage() {
		return pm_total_percentage;
	}

	public void setPm_total_percentage(Double pm_total_percentage) {
		this.pm_total_percentage = pm_total_percentage;
	}

}
