package com.orissa.model;

public class DailyMeteringModel extends CommonModel {

	private int mtr_id;
	private double no_new_res_mtr_add;
	private double no_res_mtr_rem;
	private double no_new_ind_mtr_add;
	private double no_ind_mtr_rem;
	private double no_new_com_mtr_add;
	private double no_com_mtr_rem;
	private double no_new_inst_mtr_add;
	private double no_inst_mtr_rem;
	private double tot_new_con_mtr;
	private double tot_con_mtr_rem;

	public int getMtr_id() {
		return mtr_id;
	}

	public void setMtr_id(int mtr_id) {
		this.mtr_id = mtr_id;
	}

	public double getNo_new_res_mtr_add() {
		return no_new_res_mtr_add;
	}

	public void setNo_new_res_mtr_add(double no_new_res_mtr_add) {
		this.no_new_res_mtr_add = no_new_res_mtr_add;
	}

	public double getNo_res_mtr_rem() {
		return no_res_mtr_rem;
	}

	public void setNo_res_mtr_rem(double no_res_mtr_rem) {
		this.no_res_mtr_rem = no_res_mtr_rem;
	}

	public double getNo_new_ind_mtr_add() {
		return no_new_ind_mtr_add;
	}

	public void setNo_new_ind_mtr_add(double no_new_ind_mtr_add) {
		this.no_new_ind_mtr_add = no_new_ind_mtr_add;
	}

	public double getNo_ind_mtr_rem() {
		return no_ind_mtr_rem;
	}

	public void setNo_ind_mtr_rem(double no_ind_mtr_rem) {
		this.no_ind_mtr_rem = no_ind_mtr_rem;
	}

	public double getNo_new_com_mtr_add() {
		return no_new_com_mtr_add;
	}

	public void setNo_new_com_mtr_add(double no_new_com_mtr_add) {
		this.no_new_com_mtr_add = no_new_com_mtr_add;
	}

	public double getNo_com_mtr_rem() {
		return no_com_mtr_rem;
	}

	public void setNo_com_mtr_rem(double no_com_mtr_rem) {
		this.no_com_mtr_rem = no_com_mtr_rem;
	}

	public double getNo_new_inst_mtr_add() {
		return no_new_inst_mtr_add;
	}

	public void setNo_new_inst_mtr_add(double no_new_inst_mtr_add) {
		this.no_new_inst_mtr_add = no_new_inst_mtr_add;
	}

	public double getNo_inst_mtr_rem() {
		return no_inst_mtr_rem;
	}

	public void setNo_inst_mtr_rem(double no_inst_mtr_rem) {
		this.no_inst_mtr_rem = no_inst_mtr_rem;
	}

	public double getTot_new_con_mtr() {
		return tot_new_con_mtr;
	}

	public void setTot_new_con_mtr(double tot_new_con_mtr) {
		this.tot_new_con_mtr = tot_new_con_mtr;
	}

	public double getTot_con_mtr_rem() {
		return tot_con_mtr_rem;
	}

	public void setTot_con_mtr_rem(double tot_con_mtr_rem) {
		this.tot_con_mtr_rem = tot_con_mtr_rem;
	}

}
