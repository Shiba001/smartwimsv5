package com.orissa.model;

public class LoginUsersModel {

	private int user_id;
	private String User_Name;
	private String Full_Name;
	private String User_Password;
	private ULBNameModel ulbNameModel;
	private ProjectModulesModel projectModulesModel;
	private UserRoleModel userRoleModel;
	private UserLevelModel userLevelModel;
	private int dashboard_per;
	private int report_per;
	private int entry_per;
	private int IsActive;
	private PhDistrictModel phDistrictModel;
	private PhDivisionModel phDivisionModel;
	private String code;

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_Name() {
		return User_Name;
	}

	public void setUser_Name(String user_Name) {
		User_Name = user_Name;
	}

	public String getFull_Name() {
		return Full_Name;
	}

	public void setFull_Name(String full_Name) {
		Full_Name = full_Name;
	}

	public String getUser_Password() {
		return User_Password;
	}

	public void setUser_Password(String user_Password) {
		User_Password = user_Password;
	}

	public ULBNameModel getUlbNameModel() {
		return ulbNameModel;
	}

	public void setUlbNameModel(ULBNameModel ulbNameModel) {
		this.ulbNameModel = ulbNameModel;
	}

	public ProjectModulesModel getProjectModulesModel() {
		return projectModulesModel;
	}

	public void setProjectModulesModel(ProjectModulesModel projectModulesModel) {
		this.projectModulesModel = projectModulesModel;
	}

	public UserRoleModel getUserRoleModel() {
		return userRoleModel;
	}

	public void setUserRoleModel(UserRoleModel userRoleModel) {
		this.userRoleModel = userRoleModel;
	}

	public UserLevelModel getUserLevelModel() {
		return userLevelModel;
	}

	public void setUserLevelModel(UserLevelModel userLevelModel) {
		this.userLevelModel = userLevelModel;
	}

	public int getDashboard_per() {
		return dashboard_per;
	}

	public void setDashboard_per(int dashboard_per) {
		this.dashboard_per = dashboard_per;
	}

	public int getReport_per() {
		return report_per;
	}

	public void setReport_per(int report_per) {
		this.report_per = report_per;
	}

	public int getEntry_per() {
		return entry_per;
	}

	public void setEntry_per(int entry_per) {
		this.entry_per = entry_per;
	}

	public int getIsActive() {
		return IsActive;
	}

	public void setIsActive(int isActive) {
		IsActive = isActive;
	}

	public PhDistrictModel getPhDistrictModel() {
		return phDistrictModel;
	}

	public void setPhDistrictModel(PhDistrictModel phDistrictModel) {
		this.phDistrictModel = phDistrictModel;
	}

	public PhDivisionModel getPhDivisionModel() {
		return phDivisionModel;
	}

	public void setPhDivisionModel(PhDivisionModel phDivisionModel) {
		this.phDivisionModel = phDivisionModel;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
