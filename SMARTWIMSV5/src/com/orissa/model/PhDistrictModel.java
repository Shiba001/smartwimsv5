package com.orissa.model;

public class PhDistrictModel {
	private int Dist_id;
	private PhDivisionModel phDivisionModel;
	private String Dist_Name;

	public int getDist_id() {
		return Dist_id;
	}

	public void setDist_id(int dist_id) {
		Dist_id = dist_id;
	}

	public PhDivisionModel getPhDivisionModel() {
		return phDivisionModel;
	}

	public void setPhDivisionModel(PhDivisionModel phDivisionModel) {
		this.phDivisionModel = phDivisionModel;
	}

	public String getDist_Name() {
		return Dist_Name;
	}

	public void setDist_Name(String dist_Name) {
		Dist_Name = dist_Name;
	}

}
