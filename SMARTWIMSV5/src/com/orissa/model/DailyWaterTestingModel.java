package com.orissa.model;

public class DailyWaterTestingModel extends CommonModel {

	private int wt_id;
	private Double no_bact_samp_tested;
	private Double no_bact_samp_found_ok;
	private Double no_other_samp_tested;
	private Double no_other_samp_found_ok;
	private Double no_bact_samp_defet;
	private Double no_other_samp_defet;

	private Integer slNo;
	private Double total_sample_tested;
	private Double total_sample_tested_frc;
	private Double total_sample_found;
	private Double total_sample_found_frc;
	private Double percentage_found;
	private Double percentage_found_frc;
	private Double total_percentage_found;
	private Double total_percentage_found_frc;

	private Double pm_no_bact_samp_tested;
	private Double pm_no_bact_samp_found_ok;
	private Double pm_no_other_samp_tested;
	private Double pm_no_other_samp_found_ok;
	private Double pm_no_bact_samp_defet;
	private Double pm_no_other_samp_defet;

	private Double pm_total_sample_tested;
	private Double pm_total_sample_tested_frc;
	private Double pm_total_sample_found;
	private Double pm_total_sample_found_frc;
	private Double pm_percentage_found;
	private Double pm_percentage_found_frc;
	private Double pm_total_percentage_found;
	private Double pm_total_percentage_found_frc;

	private String wtId;

	public int getWt_id() {
		return wt_id;
	}

	public void setWt_id(int wt_id) {
		this.wt_id = wt_id;
	}

	public Double getNo_bact_samp_tested() {
		return no_bact_samp_tested;
	}

	public void setNo_bact_samp_tested(Double no_bact_samp_tested) {
		this.no_bact_samp_tested = no_bact_samp_tested;
	}

	public Double getNo_bact_samp_found_ok() {
		return no_bact_samp_found_ok;
	}

	public void setNo_bact_samp_found_ok(Double no_bact_samp_found_ok) {
		this.no_bact_samp_found_ok = no_bact_samp_found_ok;
	}

	public Double getNo_other_samp_tested() {
		return no_other_samp_tested;
	}

	public void setNo_other_samp_tested(Double no_other_samp_tested) {
		this.no_other_samp_tested = no_other_samp_tested;
	}

	public Double getNo_other_samp_found_ok() {
		return no_other_samp_found_ok;
	}

	public void setNo_other_samp_found_ok(Double no_other_samp_found_ok) {
		this.no_other_samp_found_ok = no_other_samp_found_ok;
	}

	public Double getNo_bact_samp_defet() {
		return no_bact_samp_defet;
	}

	public void setNo_bact_samp_defet(Double no_bact_samp_defet) {
		this.no_bact_samp_defet = no_bact_samp_defet;
	}

	public Double getNo_other_samp_defet() {
		return no_other_samp_defet;
	}

	public void setNo_other_samp_defet(Double no_other_samp_defet) {
		this.no_other_samp_defet = no_other_samp_defet;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public Double getTotal_sample_tested() {
		return total_sample_tested;
	}

	public void setTotal_sample_tested(Double total_sample_tested) {
		this.total_sample_tested = total_sample_tested;
	}

	public Double getTotal_sample_tested_frc() {
		return total_sample_tested_frc;
	}

	public void setTotal_sample_tested_frc(Double total_sample_tested_frc) {
		this.total_sample_tested_frc = total_sample_tested_frc;
	}

	public Double getTotal_sample_found() {
		return total_sample_found;
	}

	public void setTotal_sample_found(Double total_sample_found) {
		this.total_sample_found = total_sample_found;
	}

	public Double getTotal_sample_found_frc() {
		return total_sample_found_frc;
	}

	public void setTotal_sample_found_frc(Double total_sample_found_frc) {
		this.total_sample_found_frc = total_sample_found_frc;
	}

	public Double getPercentage_found() {
		return percentage_found;
	}

	public void setPercentage_found(Double percentage_found) {
		this.percentage_found = percentage_found;
	}

	public Double getPercentage_found_frc() {
		return percentage_found_frc;
	}

	public void setPercentage_found_frc(Double percentage_found_frc) {
		this.percentage_found_frc = percentage_found_frc;
	}

	public Double getTotal_percentage_found() {
		return total_percentage_found;
	}

	public void setTotal_percentage_found(Double total_percentage_found) {
		this.total_percentage_found = total_percentage_found;
	}

	public Double getTotal_percentage_found_frc() {
		return total_percentage_found_frc;
	}

	public void setTotal_percentage_found_frc(Double total_percentage_found_frc) {
		this.total_percentage_found_frc = total_percentage_found_frc;
	}

	public Double getPm_no_bact_samp_tested() {
		return pm_no_bact_samp_tested;
	}

	public void setPm_no_bact_samp_tested(Double pm_no_bact_samp_tested) {
		this.pm_no_bact_samp_tested = pm_no_bact_samp_tested;
	}

	public Double getPm_no_bact_samp_found_ok() {
		return pm_no_bact_samp_found_ok;
	}

	public void setPm_no_bact_samp_found_ok(Double pm_no_bact_samp_found_ok) {
		this.pm_no_bact_samp_found_ok = pm_no_bact_samp_found_ok;
	}

	public Double getPm_no_other_samp_tested() {
		return pm_no_other_samp_tested;
	}

	public void setPm_no_other_samp_tested(Double pm_no_other_samp_tested) {
		this.pm_no_other_samp_tested = pm_no_other_samp_tested;
	}

	public Double getPm_no_other_samp_found_ok() {
		return pm_no_other_samp_found_ok;
	}

	public void setPm_no_other_samp_found_ok(Double pm_no_other_samp_found_ok) {
		this.pm_no_other_samp_found_ok = pm_no_other_samp_found_ok;
	}

	public Double getPm_no_bact_samp_defet() {
		return pm_no_bact_samp_defet;
	}

	public void setPm_no_bact_samp_defet(Double pm_no_bact_samp_defet) {
		this.pm_no_bact_samp_defet = pm_no_bact_samp_defet;
	}

	public Double getPm_no_other_samp_defet() {
		return pm_no_other_samp_defet;
	}

	public void setPm_no_other_samp_defet(Double pm_no_other_samp_defet) {
		this.pm_no_other_samp_defet = pm_no_other_samp_defet;
	}

	public Double getPm_total_sample_tested() {
		return pm_total_sample_tested;
	}

	public void setPm_total_sample_tested(Double pm_total_sample_tested) {
		this.pm_total_sample_tested = pm_total_sample_tested;
	}

	public Double getPm_total_sample_tested_frc() {
		return pm_total_sample_tested_frc;
	}

	public void setPm_total_sample_tested_frc(Double pm_total_sample_tested_frc) {
		this.pm_total_sample_tested_frc = pm_total_sample_tested_frc;
	}

	public Double getPm_total_sample_found() {
		return pm_total_sample_found;
	}

	public void setPm_total_sample_found(Double pm_total_sample_found) {
		this.pm_total_sample_found = pm_total_sample_found;
	}

	public Double getPm_total_sample_found_frc() {
		return pm_total_sample_found_frc;
	}

	public void setPm_total_sample_found_frc(Double pm_total_sample_found_frc) {
		this.pm_total_sample_found_frc = pm_total_sample_found_frc;
	}

	public Double getPm_percentage_found() {
		return pm_percentage_found;
	}

	public void setPm_percentage_found(Double pm_percentage_found) {
		this.pm_percentage_found = pm_percentage_found;
	}

	public Double getPm_percentage_found_frc() {
		return pm_percentage_found_frc;
	}

	public void setPm_percentage_found_frc(Double pm_percentage_found_frc) {
		this.pm_percentage_found_frc = pm_percentage_found_frc;
	}

	public Double getPm_total_percentage_found() {
		return pm_total_percentage_found;
	}

	public void setPm_total_percentage_found(Double pm_total_percentage_found) {
		this.pm_total_percentage_found = pm_total_percentage_found;
	}

	public Double getPm_total_percentage_found_frc() {
		return pm_total_percentage_found_frc;
	}

	public void setPm_total_percentage_found_frc(
			Double pm_total_percentage_found_frc) {
		this.pm_total_percentage_found_frc = pm_total_percentage_found_frc;
	}

	public String getWtId() {
		return wtId;
	}

	public void setWtId(String wtId) {
		this.wtId = wtId;
	}

}
