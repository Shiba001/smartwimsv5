package com.orissa.model;

public class PhysicalInfraGeneretorModel extends CommonModel {

	private Integer phyInfra_gen_id;
	private ULBModel ulbModel;
	private String date_of_report;
	private Double tot_no_gen;
	private Double tot_no_gen_wc;
	private Double tot_no_gen_ur;
	private Double pro_gen_wc;

	private String phyInfraGenId;
	
	public String getPhyInfraGenId() {
		return phyInfraGenId;
	}

	public void setPhyInfraGenId(String phyInfraGenId) {
		this.phyInfraGenId = phyInfraGenId;
	}

	public Integer getPhyInfra_gen_id() {
		return phyInfra_gen_id;
	}

	public void setPhyInfra_gen_id(Integer phyInfra_gen_id) {
		this.phyInfra_gen_id = phyInfra_gen_id;
	}

	public ULBModel getUlbModel() {
		return ulbModel;
	}

	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}

	public String getDate_of_report() {
		return date_of_report;
	}

	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}

	public Double getTot_no_gen() {
		return tot_no_gen;
	}

	public void setTot_no_gen(Double tot_no_gen) {
		this.tot_no_gen = tot_no_gen;
	}

	public Double getTot_no_gen_wc() {
		return tot_no_gen_wc;
	}

	public void setTot_no_gen_wc(Double tot_no_gen_wc) {
		this.tot_no_gen_wc = tot_no_gen_wc;
	}

	public Double getTot_no_gen_ur() {
		return tot_no_gen_ur;
	}

	public void setTot_no_gen_ur(Double tot_no_gen_ur) {
		this.tot_no_gen_ur = tot_no_gen_ur;
	}

	public Double getPro_gen_wc() {
		return pro_gen_wc;
	}

	public void setPro_gen_wc(Double pro_gen_wc) {
		this.pro_gen_wc = pro_gen_wc;
	}

}
