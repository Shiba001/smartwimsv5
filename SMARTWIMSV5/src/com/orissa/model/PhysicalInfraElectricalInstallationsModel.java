package com.orissa.model;

import java.util.List;

public class PhysicalInfraElectricalInstallationsModel extends CommonModel {

	private Integer phyInfra_ele_inst_slno;
	private String date_of_report;
	private ULBModel ulbModel;
	private String loc_of_inst;
	private String cap_of_trnf;
	private String year_inst;
	private String hp_lt;

	private Integer ulb_id;

	private List<String> list_loc_of_inst;
	private List<String> list_cap_of_trnf;
	private List<String> list_year_inst;
	private List<String> list_hp_lt;

	private Integer slNo;
	private String ulbName;

	private List<PhysicalInfraElectricalInstallationsModel> electricalInstallationsModels;

	private String enculbId;

	public Integer getPhyInfra_ele_inst_slno() {
		return phyInfra_ele_inst_slno;
	}

	public void setPhyInfra_ele_inst_slno(Integer phyInfra_ele_inst_slno) {
		this.phyInfra_ele_inst_slno = phyInfra_ele_inst_slno;
	}

	public String getDate_of_report() {
		return date_of_report;
	}

	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}

	public ULBModel getUlbModel() {
		return ulbModel;
	}

	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}

	public String getLoc_of_inst() {
		return loc_of_inst;
	}

	public void setLoc_of_inst(String loc_of_inst) {
		this.loc_of_inst = loc_of_inst;
	}

	public String getCap_of_trnf() {
		return cap_of_trnf;
	}

	public void setCap_of_trnf(String cap_of_trnf) {
		this.cap_of_trnf = cap_of_trnf;
	}

	public String getYear_inst() {
		return year_inst;
	}

	public void setYear_inst(String year_inst) {
		this.year_inst = year_inst;
	}

	public String getHp_lt() {
		return hp_lt;
	}

	public void setHp_lt(String hp_lt) {
		this.hp_lt = hp_lt;
	}

	public Integer getUlb_id() {
		return ulb_id;
	}

	public void setUlb_id(Integer ulb_id) {
		this.ulb_id = ulb_id;
	}

	public List<String> getList_loc_of_inst() {
		return list_loc_of_inst;
	}

	public void setList_loc_of_inst(List<String> list_loc_of_inst) {
		this.list_loc_of_inst = list_loc_of_inst;
	}

	public List<String> getList_cap_of_trnf() {
		return list_cap_of_trnf;
	}

	public void setList_cap_of_trnf(List<String> list_cap_of_trnf) {
		this.list_cap_of_trnf = list_cap_of_trnf;
	}

	public List<String> getList_year_inst() {
		return list_year_inst;
	}

	public void setList_year_inst(List<String> list_year_inst) {
		this.list_year_inst = list_year_inst;
	}

	public List<String> getList_hp_lt() {
		return list_hp_lt;
	}

	public void setList_hp_lt(List<String> list_hp_lt) {
		this.list_hp_lt = list_hp_lt;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public String getUlbName() {
		return ulbName;
	}

	public void setUlbName(String ulbName) {
		this.ulbName = ulbName;
	}

	public List<PhysicalInfraElectricalInstallationsModel> getElectricalInstallationsModels() {
		return electricalInstallationsModels;
	}

	public void setElectricalInstallationsModels(
			List<PhysicalInfraElectricalInstallationsModel> electricalInstallationsModels) {
		this.electricalInstallationsModels = electricalInstallationsModels;
	}

	public String getEnculbId() {
		return enculbId;
	}

	public void setEnculbId(String enculbId) {
		this.enculbId = enculbId;
	}

}
