package com.orissa.model;

public class Websites_ {

	private Boolean example;
	private Boolean google;
	private Boolean amazon;

	public Boolean getExample() {
		return example;
	}

	public void setExample(Boolean example) {
		this.example = example;
	}

	public Boolean getGoogle() {
		return google;
	}

	public void setGoogle(Boolean google) {
		this.google = google;
	}

	public Boolean getAmazon() {
		return amazon;
	}

	public void setAmazon(Boolean amazon) {
		this.amazon = amazon;
	}

}
