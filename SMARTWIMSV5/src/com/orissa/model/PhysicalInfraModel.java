package com.orissa.model;

public class PhysicalInfraModel {

	private int slNo;
	private String ulbName;
	private Double noOfStandPosts;
	private Double noOfStandPostsInWorkingCondition;
	private Double noOfTubeWells;
	private Double noOfTubeWellsInWorkingCondition;
	private Double noOfGroundReservoirs;
	private Double noOfElavatedReservoirs;
	private Double capacityOfGroundReservoirs;
	private Double capacityOfElavatedReservoirs;
	private Double lengthOfRisingMain;
	private Double lengthOfDistributionNetwork;
	private Double noOfTreatmentPlant;
	private Double capacityOfTreatmentPlant;
	private Double noOfGenerators;
	private Double noOfGeneratorsInWorkingCondition;

	public int getSlNo() {
		return slNo;
	}

	public void setSlNo(int slNo) {
		this.slNo = slNo;
	}

	public String getUlbName() {
		return ulbName;
	}

	public void setUlbName(String ulbName) {
		this.ulbName = ulbName;
	}

	public Double getNoOfStandPosts() {
		return noOfStandPosts;
	}

	public void setNoOfStandPosts(Double noOfStandPosts) {
		this.noOfStandPosts = noOfStandPosts;
	}

	public Double getNoOfStandPostsInWorkingCondition() {
		return noOfStandPostsInWorkingCondition;
	}

	public void setNoOfStandPostsInWorkingCondition(
			Double noOfStandPostsInWorkingCondition) {
		this.noOfStandPostsInWorkingCondition = noOfStandPostsInWorkingCondition;
	}

	public Double getNoOfTubeWells() {
		return noOfTubeWells;
	}

	public void setNoOfTubeWells(Double noOfTubeWells) {
		this.noOfTubeWells = noOfTubeWells;
	}

	public Double getNoOfTubeWellsInWorkingCondition() {
		return noOfTubeWellsInWorkingCondition;
	}

	public void setNoOfTubeWellsInWorkingCondition(
			Double noOfTubeWellsInWorkingCondition) {
		this.noOfTubeWellsInWorkingCondition = noOfTubeWellsInWorkingCondition;
	}

	public Double getNoOfGroundReservoirs() {
		return noOfGroundReservoirs;
	}

	public void setNoOfGroundReservoirs(Double noOfGroundReservoirs) {
		this.noOfGroundReservoirs = noOfGroundReservoirs;
	}

	public Double getNoOfElavatedReservoirs() {
		return noOfElavatedReservoirs;
	}

	public void setNoOfElavatedReservoirs(Double noOfElavatedReservoirs) {
		this.noOfElavatedReservoirs = noOfElavatedReservoirs;
	}

	public Double getCapacityOfGroundReservoirs() {
		return capacityOfGroundReservoirs;
	}

	public void setCapacityOfGroundReservoirs(Double capacityOfGroundReservoirs) {
		this.capacityOfGroundReservoirs = capacityOfGroundReservoirs;
	}

	public Double getCapacityOfElavatedReservoirs() {
		return capacityOfElavatedReservoirs;
	}

	public void setCapacityOfElavatedReservoirs(
			Double capacityOfElavatedReservoirs) {
		this.capacityOfElavatedReservoirs = capacityOfElavatedReservoirs;
	}

	public Double getLengthOfRisingMain() {
		return lengthOfRisingMain;
	}

	public void setLengthOfRisingMain(Double lengthOfRisingMain) {
		this.lengthOfRisingMain = lengthOfRisingMain;
	}

	public Double getLengthOfDistributionNetwork() {
		return lengthOfDistributionNetwork;
	}

	public void setLengthOfDistributionNetwork(
			Double lengthOfDistributionNetwork) {
		this.lengthOfDistributionNetwork = lengthOfDistributionNetwork;
	}

	public Double getNoOfTreatmentPlant() {
		return noOfTreatmentPlant;
	}

	public void setNoOfTreatmentPlant(Double noOfTreatmentPlant) {
		this.noOfTreatmentPlant = noOfTreatmentPlant;
	}

	public Double getCapacityOfTreatmentPlant() {
		return capacityOfTreatmentPlant;
	}

	public void setCapacityOfTreatmentPlant(Double capacityOfTreatmentPlant) {
		this.capacityOfTreatmentPlant = capacityOfTreatmentPlant;
	}

	public Double getNoOfGenerators() {
		return noOfGenerators;
	}

	public void setNoOfGenerators(Double noOfGenerators) {
		this.noOfGenerators = noOfGenerators;
	}

	public Double getNoOfGeneratorsInWorkingCondition() {
		return noOfGeneratorsInWorkingCondition;
	}

	public void setNoOfGeneratorsInWorkingCondition(
			Double noOfGeneratorsInWorkingCondition) {
		this.noOfGeneratorsInWorkingCondition = noOfGeneratorsInWorkingCondition;
	}

}
