package com.orissa.model;

public class UserLevelModel {
	private int user_level_id;
	private String user_level_name;

	public int getUser_level_id() {
		return user_level_id;
	}

	public void setUser_level_id(int user_level_id) {
		this.user_level_id = user_level_id;
	}

	public String getUser_level_name() {
		return user_level_name;
	}

	public void setUser_level_name(String user_level_name) {
		this.user_level_name = user_level_name;
	}

}
