package com.orissa.model;

public class DailyWaterSupplyModel extends CommonModel {

	private int ws_id;
	private Double tot_demand;
	private Double qlty_supply;
	private Double pop_served;
	private Double freq_supply;
	private Double surplus_deficit;
	private Double rate_of_supply;
	private Double coverage_by_pws;

	private Integer slNo;
	private Double total_tot_demand;
	private Double total_qlty_supply;
	private Double total_pop_served;
	private Double total_Freq_supply;
	private Double total_surplus_deficit;
	private Double total_rate_of_supply;
	private Double total_coverage_by_pws;

	private Double pm_tot_demand;
	private Double pm_qlty_supply;
	private Double pm_pop_served;
	private Double pm_Freq_supply;
	private Double pm_surplus_deficit;
	private Double pm_rate_of_supply;
	private Double pm_coverage_by_pws;

	private Double pm_total_tot_demand;
	private Double pm_total_qlty_supply;
	private Double pm_total_pop_served;
	private Double pm_total_Freq_supply;
	private Double pm_total_surplus_deficit;
	private Double pm_total_rate_of_supply;
	private Double pm_total_coverage_by_pws;

	private String wsId;

	public int getWs_id() {
		return ws_id;
	}

	public void setWs_id(int ws_id) {
		this.ws_id = ws_id;
	}

	public Double getTot_demand() {
		return tot_demand;
	}

	public void setTot_demand(Double tot_demand) {
		this.tot_demand = tot_demand;
	}

	public Double getQlty_supply() {
		return qlty_supply;
	}

	public void setQlty_supply(Double qlty_supply) {
		this.qlty_supply = qlty_supply;
	}

	public Double getPop_served() {
		return pop_served;
	}

	public void setPop_served(Double pop_served) {
		this.pop_served = pop_served;
	}

	public Double getFreq_supply() {
		return freq_supply;
	}

	public void setFreq_supply(Double freq_supply) {
		this.freq_supply = freq_supply;
	}

	public Double getSurplus_deficit() {
		return surplus_deficit;
	}

	public void setSurplus_deficit(Double surplus_deficit) {
		this.surplus_deficit = surplus_deficit;
	}

	public Double getRate_of_supply() {
		return rate_of_supply;
	}

	public void setRate_of_supply(Double rate_of_supply) {
		this.rate_of_supply = rate_of_supply;
	}

	public Double getCoverage_by_pws() {
		return coverage_by_pws;
	}

	public void setCoverage_by_pws(Double coverage_by_pws) {
		this.coverage_by_pws = coverage_by_pws;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public Double getTotal_tot_demand() {
		return total_tot_demand;
	}

	public void setTotal_tot_demand(Double total_tot_demand) {
		this.total_tot_demand = total_tot_demand;
	}

	public Double getTotal_qlty_supply() {
		return total_qlty_supply;
	}

	public void setTotal_qlty_supply(Double total_qlty_supply) {
		this.total_qlty_supply = total_qlty_supply;
	}

	public Double getTotal_pop_served() {
		return total_pop_served;
	}

	public void setTotal_pop_served(Double total_pop_served) {
		this.total_pop_served = total_pop_served;
	}

	public Double getTotal_Freq_supply() {
		return total_Freq_supply;
	}

	public void setTotal_Freq_supply(Double total_Freq_supply) {
		this.total_Freq_supply = total_Freq_supply;
	}

	public Double getTotal_surplus_deficit() {
		return total_surplus_deficit;
	}

	public void setTotal_surplus_deficit(Double total_surplus_deficit) {
		this.total_surplus_deficit = total_surplus_deficit;
	}

	public Double getTotal_rate_of_supply() {
		return total_rate_of_supply;
	}

	public void setTotal_rate_of_supply(Double total_rate_of_supply) {
		this.total_rate_of_supply = total_rate_of_supply;
	}

	public Double getTotal_coverage_by_pws() {
		return total_coverage_by_pws;
	}

	public void setTotal_coverage_by_pws(Double total_coverage_by_pws) {
		this.total_coverage_by_pws = total_coverage_by_pws;
	}

	public Double getPm_tot_demand() {
		return pm_tot_demand;
	}

	public void setPm_tot_demand(Double pm_tot_demand) {
		this.pm_tot_demand = pm_tot_demand;
	}

	public Double getPm_qlty_supply() {
		return pm_qlty_supply;
	}

	public void setPm_qlty_supply(Double pm_qlty_supply) {
		this.pm_qlty_supply = pm_qlty_supply;
	}

	public Double getPm_pop_served() {
		return pm_pop_served;
	}

	public void setPm_pop_served(Double pm_pop_served) {
		this.pm_pop_served = pm_pop_served;
	}

	public Double getPm_Freq_supply() {
		return pm_Freq_supply;
	}

	public void setPm_Freq_supply(Double pm_Freq_supply) {
		this.pm_Freq_supply = pm_Freq_supply;
	}

	public Double getPm_surplus_deficit() {
		return pm_surplus_deficit;
	}

	public void setPm_surplus_deficit(Double pm_surplus_deficit) {
		this.pm_surplus_deficit = pm_surplus_deficit;
	}

	public Double getPm_rate_of_supply() {
		return pm_rate_of_supply;
	}

	public void setPm_rate_of_supply(Double pm_rate_of_supply) {
		this.pm_rate_of_supply = pm_rate_of_supply;
	}

	public Double getPm_coverage_by_pws() {
		return pm_coverage_by_pws;
	}

	public void setPm_coverage_by_pws(Double pm_coverage_by_pws) {
		this.pm_coverage_by_pws = pm_coverage_by_pws;
	}

	public Double getPm_total_tot_demand() {
		return pm_total_tot_demand;
	}

	public void setPm_total_tot_demand(Double pm_total_tot_demand) {
		this.pm_total_tot_demand = pm_total_tot_demand;
	}

	public Double getPm_total_qlty_supply() {
		return pm_total_qlty_supply;
	}

	public void setPm_total_qlty_supply(Double pm_total_qlty_supply) {
		this.pm_total_qlty_supply = pm_total_qlty_supply;
	}

	public Double getPm_total_pop_served() {
		return pm_total_pop_served;
	}

	public void setPm_total_pop_served(Double pm_total_pop_served) {
		this.pm_total_pop_served = pm_total_pop_served;
	}

	public Double getPm_total_Freq_supply() {
		return pm_total_Freq_supply;
	}

	public void setPm_total_Freq_supply(Double pm_total_Freq_supply) {
		this.pm_total_Freq_supply = pm_total_Freq_supply;
	}

	public Double getPm_total_surplus_deficit() {
		return pm_total_surplus_deficit;
	}

	public void setPm_total_surplus_deficit(Double pm_total_surplus_deficit) {
		this.pm_total_surplus_deficit = pm_total_surplus_deficit;
	}

	public Double getPm_total_rate_of_supply() {
		return pm_total_rate_of_supply;
	}

	public void setPm_total_rate_of_supply(Double pm_total_rate_of_supply) {
		this.pm_total_rate_of_supply = pm_total_rate_of_supply;
	}

	public Double getPm_total_coverage_by_pws() {
		return pm_total_coverage_by_pws;
	}

	public void setPm_total_coverage_by_pws(Double pm_total_coverage_by_pws) {
		this.pm_total_coverage_by_pws = pm_total_coverage_by_pws;
	}

	public String getWsId() {
		return wsId;
	}

	public void setWsId(String wsId) {
		this.wsId = wsId;
	}

}
