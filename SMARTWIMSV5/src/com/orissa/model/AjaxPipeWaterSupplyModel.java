package com.orissa.model;

import java.util.LinkedList;

public class AjaxPipeWaterSupplyModel {
	private double total_qlty_supply;
	private int total_pop_served;
	private double total_Freq_supply;
	private double total_surplus_deficit;
	private double total_rate_of_supply;
	private double total_coverage_by_pws;
	private double total_tot_demand;
	private LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels;

	public double getTotal_qlty_supply() {
		return total_qlty_supply;
	}

	public void setTotal_qlty_supply(double total_qlty_supply) {
		this.total_qlty_supply = total_qlty_supply;
	}

	public int getTotal_pop_served() {
		return total_pop_served;
	}

	public void setTotal_pop_served(int total_pop_served) {
		this.total_pop_served = total_pop_served;
	}

	public double getTotal_Freq_supply() {
		return total_Freq_supply;
	}

	public void setTotal_Freq_supply(double total_Freq_supply) {
		this.total_Freq_supply = total_Freq_supply;
	}

	public double getTotal_surplus_deficit() {
		return total_surplus_deficit;
	}

	public void setTotal_surplus_deficit(double total_surplus_deficit) {
		this.total_surplus_deficit = total_surplus_deficit;
	}

	public double getTotal_rate_of_supply() {
		return total_rate_of_supply;
	}

	public void setTotal_rate_of_supply(double total_rate_of_supply) {
		this.total_rate_of_supply = total_rate_of_supply;
	}

	public double getTotal_coverage_by_pws() {
		return total_coverage_by_pws;
	}

	public void setTotal_coverage_by_pws(double total_coverage_by_pws) {
		this.total_coverage_by_pws = total_coverage_by_pws;
	}

	public double getTotal_tot_demand() {
		return total_tot_demand;
	}

	public void setTotal_tot_demand(double total_tot_demand) {
		this.total_tot_demand = total_tot_demand;
	}

	public LinkedList<DailyWaterSupplyModel> getDailyWaterSupplyModels() {
		return dailyWaterSupplyModels;
	}

	public void setDailyWaterSupplyModels(
			LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels) {
		this.dailyWaterSupplyModels = dailyWaterSupplyModels;
	}

}
