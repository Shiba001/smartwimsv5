package com.orissa.model;

public class MonthlyPropertyTaxModel {
	private int Mon_P_tax_id;
	private ULBModel ulbModel;
	private int mon_id;
	private double tot_cur_coll;
	private double tot_arr_coll;
	private LoginUsersModel loginUsersModel;
	private String entry_date;
	private String modify_date;

	public int getMon_P_tax_id() {
		return Mon_P_tax_id;
	}

	public void setMon_P_tax_id(int mon_P_tax_id) {
		Mon_P_tax_id = mon_P_tax_id;
	}

	public ULBModel getUlbModel() {
		return ulbModel;
	}

	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}

	public int getMon_id() {
		return mon_id;
	}

	public void setMon_id(int mon_id) {
		this.mon_id = mon_id;
	}

	public double getTot_cur_coll() {
		return tot_cur_coll;
	}

	public void setTot_cur_coll(double tot_cur_coll) {
		this.tot_cur_coll = tot_cur_coll;
	}

	public double getTot_arr_coll() {
		return tot_arr_coll;
	}

	public void setTot_arr_coll(double tot_arr_coll) {
		this.tot_arr_coll = tot_arr_coll;
	}

	public LoginUsersModel getLoginUsersModel() {
		return loginUsersModel;
	}

	public void setLoginUsersModel(LoginUsersModel loginUsersModel) {
		this.loginUsersModel = loginUsersModel;
	}

	public String getEntry_date() {
		return entry_date;
	}

	public void setEntry_date(String entry_date) {
		this.entry_date = entry_date;
	}

	public String getModify_date() {
		return modify_date;
	}

	public void setModify_date(String modify_date) {
		this.modify_date = modify_date;
	}

}
