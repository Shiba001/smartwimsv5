package com.orissa.model;

public class PhysicalInfraTubewellsModel extends CommonModel {
	
	private Integer phyInfra_tw_id;
	private ULBModel ulbModel;
	private String date_of_report;
	private Double tot_no_tw;
	private Double tot_no_tw_wc;
	private Double tot_no_tw_ur;
	private Double pro_tw_wc;
	
	private String phyInfraTwId;
	
	public String getPhyInfraTwId() {
		return phyInfraTwId;
	}
	public void setPhyInfraTwId(String phyInfraTwId) {
		this.phyInfraTwId = phyInfraTwId;
	}
	public Integer getPhyInfra_tw_id() {
		return phyInfra_tw_id;
	}
	public void setPhyInfra_tw_id(Integer phyInfra_tw_id) {
		this.phyInfra_tw_id = phyInfra_tw_id;
	}
	public ULBModel getUlbModel() {
		return ulbModel;
	}
	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}
	public String getDate_of_report() {
		return date_of_report;
	}
	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}
	public Double getTot_no_tw() {
		return tot_no_tw;
	}
	public void setTot_no_tw(Double tot_no_tw) {
		this.tot_no_tw = tot_no_tw;
	}
	public Double getTot_no_tw_wc() {
		return tot_no_tw_wc;
	}
	public void setTot_no_tw_wc(Double tot_no_tw_wc) {
		this.tot_no_tw_wc = tot_no_tw_wc;
	}
	public Double getTot_no_tw_ur() {
		return tot_no_tw_ur;
	}
	public void setTot_no_tw_ur(Double tot_no_tw_ur) {
		this.tot_no_tw_ur = tot_no_tw_ur;
	}
	public Double getPro_tw_wc() {
		return pro_tw_wc;
	}
	public void setPro_tw_wc(Double pro_tw_wc) {
		this.pro_tw_wc = pro_tw_wc;
	}

}