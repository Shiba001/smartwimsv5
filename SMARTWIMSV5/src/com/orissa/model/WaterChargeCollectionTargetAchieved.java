package com.orissa.model;

import java.util.ArrayList;

public class WaterChargeCollectionTargetAchieved {
	private String uLBName;
	private double totalWaterChargesCollection;
	private double TargetfortheMonth;
	// achieved in percentage %
	private String achieved;

	private ArrayList<Double> m_trgts;

	public ArrayList<Double> getM_trgts() {
		return m_trgts;
	}

	public void setM_trgts(ArrayList<Double> m_trgts) {
		this.m_trgts = m_trgts;
	}

	public String getuLBName() {
		return uLBName;
	}

	public void setuLBName(String uLBName) {
		this.uLBName = uLBName;
	}

	public double getTotalWaterChargesCollection() {
		return totalWaterChargesCollection;
	}

	public void setTotalWaterChargesCollection(
			double totalWaterChargesCollection) {
		this.totalWaterChargesCollection = totalWaterChargesCollection;
	}

	public double getTargetfortheMonth() {
		return TargetfortheMonth;
	}

	public void setTargetfortheMonth(double targetfortheMonth) {
		TargetfortheMonth = targetfortheMonth;
	}

	public String getAchieved() {
		return achieved;
	}

	public void setAchieved(String achieved) {
		this.achieved = achieved;
	}

}
