package com.orissa.model;

import java.util.List;

public class PhysicalInfraProductionWellsModel extends CommonModel {

	private Integer phyInfra_prod_well_slno;
	private String date_of_report;
	private String loc_of_prod_well;
	private String diam_prod_well;
	private String discharge;
	private String depth;
	private Integer ulb_id;

	private List<String> list_loc_of_prod_well;
	private List<String> list_diam_prod_well;
	private List<String> list_discharge;
	private List<String> list_depth;

	private List<PhysicalInfraProductionWellsModel> productionWellsModels;

	private Integer slNo;
	private String ulbName;

	private String enculbId;

	public Integer getPhyInfra_prod_well_slno() {
		return phyInfra_prod_well_slno;
	}

	public void setPhyInfra_prod_well_slno(Integer phyInfra_prod_well_slno) {
		this.phyInfra_prod_well_slno = phyInfra_prod_well_slno;
	}

	public String getDate_of_report() {
		return date_of_report;
	}

	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}

	public String getLoc_of_prod_well() {
		return loc_of_prod_well;
	}

	public void setLoc_of_prod_well(String loc_of_prod_well) {
		this.loc_of_prod_well = loc_of_prod_well;
	}

	public String getDiam_prod_well() {
		return diam_prod_well;
	}

	public void setDiam_prod_well(String diam_prod_well) {
		this.diam_prod_well = diam_prod_well;
	}

	public String getDischarge() {
		return discharge;
	}

	public void setDischarge(String discharge) {
		this.discharge = discharge;
	}

	public String getDepth() {
		return depth;
	}

	public void setDepth(String depth) {
		this.depth = depth;
	}

	public Integer getUlb_id() {
		return ulb_id;
	}

	public void setUlb_id(Integer ulb_id) {
		this.ulb_id = ulb_id;
	}

	public List<String> getList_loc_of_prod_well() {
		return list_loc_of_prod_well;
	}

	public void setList_loc_of_prod_well(List<String> list_loc_of_prod_well) {
		this.list_loc_of_prod_well = list_loc_of_prod_well;
	}

	public List<String> getList_diam_prod_well() {
		return list_diam_prod_well;
	}

	public void setList_diam_prod_well(List<String> list_diam_prod_well) {
		this.list_diam_prod_well = list_diam_prod_well;
	}

	public List<String> getList_discharge() {
		return list_discharge;
	}

	public void setList_discharge(List<String> list_discharge) {
		this.list_discharge = list_discharge;
	}

	public List<String> getList_depth() {
		return list_depth;
	}

	public void setList_depth(List<String> list_depth) {
		this.list_depth = list_depth;
	}

	public List<PhysicalInfraProductionWellsModel> getProductionWellsModels() {
		return productionWellsModels;
	}

	public void setProductionWellsModels(
			List<PhysicalInfraProductionWellsModel> productionWellsModels) {
		this.productionWellsModels = productionWellsModels;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public String getUlbName() {
		return ulbName;
	}

	public void setUlbName(String ulbName) {
		this.ulbName = ulbName;
	}

	public String getEnculbId() {
		return enculbId;
	}

	public void setEnculbId(String enculbId) {
		this.enculbId = enculbId;
	}

}
