package com.orissa.model;

public class ULBNameModel {

	private int ulb_id;
	private AmrutTypeModel amrutTypeModel;
	private ULBTypeModel ulbTypeModel;
	private PhDistrictModel phDistrictModel;
	private ULBModel ulbModel;
	private String ulb_Name;
	private String latitude;
	private String longitude;

	public int getUlb_id() {
		return ulb_id;
	}

	public void setUlb_id(int ulb_id) {
		this.ulb_id = ulb_id;
	}

	public AmrutTypeModel getAmrutTypeModel() {
		return amrutTypeModel;
	}

	public void setAmrutTypeModel(AmrutTypeModel amrutTypeModel) {
		this.amrutTypeModel = amrutTypeModel;
	}

	public ULBTypeModel getUlbTypeModel() {
		return ulbTypeModel;
	}

	public void setUlbTypeModel(ULBTypeModel ulbTypeModel) {
		this.ulbTypeModel = ulbTypeModel;
	}

	public PhDistrictModel getPhDistrictModel() {
		return phDistrictModel;
	}

	public void setPhDistrictModel(PhDistrictModel phDistrictModel) {
		this.phDistrictModel = phDistrictModel;
	}

	public ULBModel getUlbModel() {
		return ulbModel;
	}

	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}

	public String getUlb_Name() {
		return ulb_Name;
	}

	public void setUlb_Name(String ulb_Name) {
		this.ulb_Name = ulb_Name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}
