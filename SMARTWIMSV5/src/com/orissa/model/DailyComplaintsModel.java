package com.orissa.model;

public class DailyComplaintsModel extends CommonModel {

	private int tw_id;
	private Double no_ws_comp_recv;
	private Double no_ws_comp_resolved;
	private Double no_ws_comp_pending;
	private Double no_tw_comp_recv;
	private Double no_tw_comp_resolved;
	private Double no_tw_comp_pending;

	private Integer slNo;
	private Double total_comp_recveived;
	private Double total_comp_resolved;
	private Double total_comp_pending;
	private Double total_tw_comp_recveived;
	private Double total_tw_comp_resolved;
	private Double total_tw_comp_pending;

	private Double pm_no_ws_comp_recv;
	private Double pm_no_ws_comp_resolved;
	private Double pm_no_ws_comp_pending;
	private Double pm_no_tw_comp_recv;
	private Double pm_no_tw_comp_resolved;
	private Double pm_no_tw_comp_pending;

	private Double pm_total_comp_recveived;
	private Double pm_total_comp_resolved;
	private Double pm_total_comp_pending;
	private Double pm_total_tw_comp_recveived;
	private Double pm_total_tw_comp_resolved;
	private Double pm_total_tw_comp_pending;

	private Double complaintsResolved;

	private String twId;

	public int getTw_id() {
		return tw_id;
	}

	public void setTw_id(int tw_id) {
		this.tw_id = tw_id;
	}

	public Double getNo_ws_comp_recv() {
		return no_ws_comp_recv;
	}

	public void setNo_ws_comp_recv(Double no_ws_comp_recv) {
		this.no_ws_comp_recv = no_ws_comp_recv;
	}

	public Double getNo_ws_comp_resolved() {
		return no_ws_comp_resolved;
	}

	public void setNo_ws_comp_resolved(Double no_ws_comp_resolved) {
		this.no_ws_comp_resolved = no_ws_comp_resolved;
	}

	public Double getNo_ws_comp_pending() {
		return no_ws_comp_pending;
	}

	public void setNo_ws_comp_pending(Double no_ws_comp_pending) {
		this.no_ws_comp_pending = no_ws_comp_pending;
	}

	public Double getNo_tw_comp_recv() {
		return no_tw_comp_recv;
	}

	public void setNo_tw_comp_recv(Double no_tw_comp_recv) {
		this.no_tw_comp_recv = no_tw_comp_recv;
	}

	public Double getNo_tw_comp_resolved() {
		return no_tw_comp_resolved;
	}

	public void setNo_tw_comp_resolved(Double no_tw_comp_resolved) {
		this.no_tw_comp_resolved = no_tw_comp_resolved;
	}

	public Double getNo_tw_comp_pending() {
		return no_tw_comp_pending;
	}

	public void setNo_tw_comp_pending(Double no_tw_comp_pending) {
		this.no_tw_comp_pending = no_tw_comp_pending;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public Double getTotal_comp_recveived() {
		return total_comp_recveived;
	}

	public void setTotal_comp_recveived(Double total_comp_recveived) {
		this.total_comp_recveived = total_comp_recveived;
	}

	public Double getTotal_comp_resolved() {
		return total_comp_resolved;
	}

	public void setTotal_comp_resolved(Double total_comp_resolved) {
		this.total_comp_resolved = total_comp_resolved;
	}

	public Double getTotal_comp_pending() {
		return total_comp_pending;
	}

	public void setTotal_comp_pending(Double total_comp_pending) {
		this.total_comp_pending = total_comp_pending;
	}

	public Double getTotal_tw_comp_recveived() {
		return total_tw_comp_recveived;
	}

	public void setTotal_tw_comp_recveived(Double total_tw_comp_recveived) {
		this.total_tw_comp_recveived = total_tw_comp_recveived;
	}

	public Double getTotal_tw_comp_resolved() {
		return total_tw_comp_resolved;
	}

	public void setTotal_tw_comp_resolved(Double total_tw_comp_resolved) {
		this.total_tw_comp_resolved = total_tw_comp_resolved;
	}

	public Double getTotal_tw_comp_pending() {
		return total_tw_comp_pending;
	}

	public void setTotal_tw_comp_pending(Double total_tw_comp_pending) {
		this.total_tw_comp_pending = total_tw_comp_pending;
	}

	public Double getPm_no_ws_comp_recv() {
		return pm_no_ws_comp_recv;
	}

	public void setPm_no_ws_comp_recv(Double pm_no_ws_comp_recv) {
		this.pm_no_ws_comp_recv = pm_no_ws_comp_recv;
	}

	public Double getPm_no_ws_comp_resolved() {
		return pm_no_ws_comp_resolved;
	}

	public void setPm_no_ws_comp_resolved(Double pm_no_ws_comp_resolved) {
		this.pm_no_ws_comp_resolved = pm_no_ws_comp_resolved;
	}

	public Double getPm_no_ws_comp_pending() {
		return pm_no_ws_comp_pending;
	}

	public void setPm_no_ws_comp_pending(Double pm_no_ws_comp_pending) {
		this.pm_no_ws_comp_pending = pm_no_ws_comp_pending;
	}

	public Double getPm_no_tw_comp_recv() {
		return pm_no_tw_comp_recv;
	}

	public void setPm_no_tw_comp_recv(Double pm_no_tw_comp_recv) {
		this.pm_no_tw_comp_recv = pm_no_tw_comp_recv;
	}

	public Double getPm_no_tw_comp_resolved() {
		return pm_no_tw_comp_resolved;
	}

	public void setPm_no_tw_comp_resolved(Double pm_no_tw_comp_resolved) {
		this.pm_no_tw_comp_resolved = pm_no_tw_comp_resolved;
	}

	public Double getPm_no_tw_comp_pending() {
		return pm_no_tw_comp_pending;
	}

	public void setPm_no_tw_comp_pending(Double pm_no_tw_comp_pending) {
		this.pm_no_tw_comp_pending = pm_no_tw_comp_pending;
	}

	public Double getPm_total_comp_recveived() {
		return pm_total_comp_recveived;
	}

	public void setPm_total_comp_recveived(Double pm_total_comp_recveived) {
		this.pm_total_comp_recveived = pm_total_comp_recveived;
	}

	public Double getPm_total_comp_resolved() {
		return pm_total_comp_resolved;
	}

	public void setPm_total_comp_resolved(Double pm_total_comp_resolved) {
		this.pm_total_comp_resolved = pm_total_comp_resolved;
	}

	public Double getPm_total_comp_pending() {
		return pm_total_comp_pending;
	}

	public void setPm_total_comp_pending(Double pm_total_comp_pending) {
		this.pm_total_comp_pending = pm_total_comp_pending;
	}

	public Double getPm_total_tw_comp_recveived() {
		return pm_total_tw_comp_recveived;
	}

	public void setPm_total_tw_comp_recveived(Double pm_total_tw_comp_recveived) {
		this.pm_total_tw_comp_recveived = pm_total_tw_comp_recveived;
	}

	public Double getPm_total_tw_comp_resolved() {
		return pm_total_tw_comp_resolved;
	}

	public void setPm_total_tw_comp_resolved(Double pm_total_tw_comp_resolved) {
		this.pm_total_tw_comp_resolved = pm_total_tw_comp_resolved;
	}

	public Double getPm_total_tw_comp_pending() {
		return pm_total_tw_comp_pending;
	}

	public void setPm_total_tw_comp_pending(Double pm_total_tw_comp_pending) {
		this.pm_total_tw_comp_pending = pm_total_tw_comp_pending;
	}

	public String getTwId() {
		return twId;
	}

	public void setTwId(String twId) {
		this.twId = twId;
	}

	public Double getComplaintsResolved() {
		return complaintsResolved;
	}

	public void setComplaintsResolved(Double complaintsResolved) {
		this.complaintsResolved = complaintsResolved;
	}

}
