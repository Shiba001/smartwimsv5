package com.orissa.model;

public class PhysicalInfraWaterTreatmentPlantModel extends CommonModel {

	private Integer wtpid;
	private String date_of_report;
	private Integer ulb_id;
	private Double no_of_wtp;
	private Double cpty_wtp;

	private String wtpipeId;
	
	public String getWtpipeId() {
		return wtpipeId;
	}

	public void setWtpipeId(String wtpipeId) {
		this.wtpipeId = wtpipeId;
	}

	public Integer getWtpid() {
		return wtpid;
	}

	public void setWtpid(Integer wtpid) {
		this.wtpid = wtpid;
	}

	public String getDate_of_report() {
		return date_of_report;
	}

	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}

	public Integer getUlb_id() {
		return ulb_id;
	}

	public void setUlb_id(Integer ulb_id) {
		this.ulb_id = ulb_id;
	}

	public Double getNo_of_wtp() {
		return no_of_wtp;
	}

	public void setNo_of_wtp(Double no_of_wtp) {
		this.no_of_wtp = no_of_wtp;
	}

	public Double getCpty_wtp() {
		return cpty_wtp;
	}

	public void setCpty_wtp(Double cpty_wtp) {
		this.cpty_wtp = cpty_wtp;
	}

}
