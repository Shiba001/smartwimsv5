package com.orissa.model;

public class DailyTankersModel extends CommonModel {

	private int tank_id;
	private Double no_tank_engd_dept;
	private Double no_tank_hire;
	private Double tot_no_tank_engd;
	private Double no_pvc_rcc_tanks_depl;
	private Double popu_served_tanks_pvc_rcc;
	private Double qty_mld;
	private Double lpcd;
	
	private String tankId;

	private Integer slNo;
	private Double total_tankers_engaged;
	private Double total_tankers_hired;
	private Double total_tanks_deployed;
	private Double total_population_served;
	private Double total_quantity_supplied;
	
	private Double Pm_no_tank_engd_dept;
	private Double Pm_no_tank_hire;
	private Double Pm_tot_no_tank_engd;
	private Double Pm_no_pvc_rcc_tanks_depl;
	private Double Pm_popu_served_tanks_pvc_rcc;
	private Double Pm_qty_mld;
	private Double Pm_lpcd;
	
	private Double Pm_total_tankers_engaged;
	private Double Pm_total_tankers_hired;
	private Double Pm_total_tanks_deployed;
	private Double Pm_total_population_served;
	private Double Pm_total_quantity_supplied;
	public int getTank_id() {
		return tank_id;
	}
	public void setTank_id(int tank_id) {
		this.tank_id = tank_id;
	}
	public Double getNo_tank_engd_dept() {
		return no_tank_engd_dept;
	}
	public void setNo_tank_engd_dept(Double no_tank_engd_dept) {
		this.no_tank_engd_dept = no_tank_engd_dept;
	}
	public Double getNo_tank_hire() {
		return no_tank_hire;
	}
	public void setNo_tank_hire(Double no_tank_hire) {
		this.no_tank_hire = no_tank_hire;
	}
	public Double getTot_no_tank_engd() {
		return tot_no_tank_engd;
	}
	public void setTot_no_tank_engd(Double tot_no_tank_engd) {
		this.tot_no_tank_engd = tot_no_tank_engd;
	}
	public Double getNo_pvc_rcc_tanks_depl() {
		return no_pvc_rcc_tanks_depl;
	}
	public void setNo_pvc_rcc_tanks_depl(Double no_pvc_rcc_tanks_depl) {
		this.no_pvc_rcc_tanks_depl = no_pvc_rcc_tanks_depl;
	}
	public Double getPopu_served_tanks_pvc_rcc() {
		return popu_served_tanks_pvc_rcc;
	}
	public void setPopu_served_tanks_pvc_rcc(Double popu_served_tanks_pvc_rcc) {
		this.popu_served_tanks_pvc_rcc = popu_served_tanks_pvc_rcc;
	}
	public Double getQty_mld() {
		return qty_mld;
	}
	public void setQty_mld(Double qty_mld) {
		this.qty_mld = qty_mld;
	}
	public Double getLpcd() {
		return lpcd;
	}
	public void setLpcd(Double lpcd) {
		this.lpcd = lpcd;
	}
	public String getTankId() {
		return tankId;
	}
	public void setTankId(String tankId) {
		this.tankId = tankId;
	}
	public Integer getSlNo() {
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public Double getTotal_tankers_engaged() {
		return total_tankers_engaged;
	}
	public void setTotal_tankers_engaged(Double total_tankers_engaged) {
		this.total_tankers_engaged = total_tankers_engaged;
	}
	public Double getTotal_tankers_hired() {
		return total_tankers_hired;
	}
	public void setTotal_tankers_hired(Double total_tankers_hired) {
		this.total_tankers_hired = total_tankers_hired;
	}
	public Double getTotal_tanks_deployed() {
		return total_tanks_deployed;
	}
	public void setTotal_tanks_deployed(Double total_tanks_deployed) {
		this.total_tanks_deployed = total_tanks_deployed;
	}
	public Double getTotal_population_served() {
		return total_population_served;
	}
	public void setTotal_population_served(Double total_population_served) {
		this.total_population_served = total_population_served;
	}
	public Double getTotal_quantity_supplied() {
		return total_quantity_supplied;
	}
	public void setTotal_quantity_supplied(Double total_quantity_supplied) {
		this.total_quantity_supplied = total_quantity_supplied;
	}
	public Double getPm_no_tank_engd_dept() {
		return Pm_no_tank_engd_dept;
	}
	public void setPm_no_tank_engd_dept(Double pm_no_tank_engd_dept) {
		Pm_no_tank_engd_dept = pm_no_tank_engd_dept;
	}
	public Double getPm_no_tank_hire() {
		return Pm_no_tank_hire;
	}
	public void setPm_no_tank_hire(Double pm_no_tank_hire) {
		Pm_no_tank_hire = pm_no_tank_hire;
	}
	public Double getPm_tot_no_tank_engd() {
		return Pm_tot_no_tank_engd;
	}
	public void setPm_tot_no_tank_engd(Double pm_tot_no_tank_engd) {
		Pm_tot_no_tank_engd = pm_tot_no_tank_engd;
	}
	public Double getPm_no_pvc_rcc_tanks_depl() {
		return Pm_no_pvc_rcc_tanks_depl;
	}
	public void setPm_no_pvc_rcc_tanks_depl(Double pm_no_pvc_rcc_tanks_depl) {
		Pm_no_pvc_rcc_tanks_depl = pm_no_pvc_rcc_tanks_depl;
	}
	public Double getPm_popu_served_tanks_pvc_rcc() {
		return Pm_popu_served_tanks_pvc_rcc;
	}
	public void setPm_popu_served_tanks_pvc_rcc(Double pm_popu_served_tanks_pvc_rcc) {
		Pm_popu_served_tanks_pvc_rcc = pm_popu_served_tanks_pvc_rcc;
	}
	public Double getPm_qty_mld() {
		return Pm_qty_mld;
	}
	public void setPm_qty_mld(Double pm_qty_mld) {
		Pm_qty_mld = pm_qty_mld;
	}
	public Double getPm_lpcd() {
		return Pm_lpcd;
	}
	public void setPm_lpcd(Double pm_lpcd) {
		Pm_lpcd = pm_lpcd;
	}
	public Double getPm_total_tankers_engaged() {
		return Pm_total_tankers_engaged;
	}
	public void setPm_total_tankers_engaged(Double pm_total_tankers_engaged) {
		Pm_total_tankers_engaged = pm_total_tankers_engaged;
	}
	public Double getPm_total_tankers_hired() {
		return Pm_total_tankers_hired;
	}
	public void setPm_total_tankers_hired(Double pm_total_tankers_hired) {
		Pm_total_tankers_hired = pm_total_tankers_hired;
	}
	public Double getPm_total_tanks_deployed() {
		return Pm_total_tanks_deployed;
	}
	public void setPm_total_tanks_deployed(Double pm_total_tanks_deployed) {
		Pm_total_tanks_deployed = pm_total_tanks_deployed;
	}
	public Double getPm_total_population_served() {
		return Pm_total_population_served;
	}
	public void setPm_total_population_served(Double pm_total_population_served) {
		Pm_total_population_served = pm_total_population_served;
	}
	public Double getPm_total_quantity_supplied() {
		return Pm_total_quantity_supplied;
	}
	public void setPm_total_quantity_supplied(Double pm_total_quantity_supplied) {
		Pm_total_quantity_supplied = pm_total_quantity_supplied;
	}

}
