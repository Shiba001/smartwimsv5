package com.orissa.model;

public class DailyWaterChargesModel extends CommonModel {

	private int W_tax_id;
	private Double tot_cur_coll;

	private String W_taxId;
	public int getW_tax_id() {
		return W_tax_id;
	}

	public void setW_tax_id(int w_tax_id) {
		W_tax_id = w_tax_id;
	}

	public Double getTot_cur_coll() {
		return tot_cur_coll;
	}

	public void setTot_cur_coll(Double tot_cur_coll) {
		this.tot_cur_coll = tot_cur_coll;
	}

	public String getW_taxId() {
		return W_taxId;
	}

	public void setW_taxId(String w_taxId) {
		W_taxId = w_taxId;
	}
	
}
