package com.orissa.model;

import java.util.ArrayList;

public class WaterChargeCollectionTargetAchievedPie {
	// [ "<25%", "<49%", "<74%", "<100%", ">=100%" ]
	private int totalNoLt25;
	private int totalNoLt49;
	private int totalNoLt74;
	private int totalNoLt100;
	private int totalNoGe100;

	public int getTotalNoLt25() {
		return totalNoLt25;
	}

	public void setTotalNoLt25(int totalNoLt25) {
		this.totalNoLt25 = totalNoLt25;
	}

	public int getTotalNoLt49() {
		return totalNoLt49;
	}

	public void setTotalNoLt49(int totalNoLt49) {
		this.totalNoLt49 = totalNoLt49;
	}

	public int getTotalNoLt74() {
		return totalNoLt74;
	}

	public void setTotalNoLt74(int totalNoLt74) {
		this.totalNoLt74 = totalNoLt74;
	}

	public int getTotalNoLt100() {
		return totalNoLt100;
	}

	public void setTotalNoLt100(int totalNoLt100) {
		this.totalNoLt100 = totalNoLt100;
	}

	public int getTotalNoGe100() {
		return totalNoGe100;
	}

	public void setTotalNoGe100(int totalNoGe100) {
		this.totalNoGe100 = totalNoGe100;
	}

}
