package com.orissa.model;

public class PhysicalInfraStorageReservoirsModel extends CommonModel {

	private Integer phyInfra_sr_id;
	private String date_of_report;
	private ULBModel ulbModel;
	private Double no_grnd_strg_res;
	private Double cpty_grnd_strg_res;
	private Double no_ele_strg_res;
	private Double cpty_ele_strg_res;
	
	private String phyInfraSrId;
	
	public String getPhyInfraSrId() {
		return phyInfraSrId;
	}
	public void setPhyInfraSrId(String phyInfraSrId) {
		this.phyInfraSrId = phyInfraSrId;
	}
	public Integer getPhyInfra_sr_id() {
		return phyInfra_sr_id;
	}
	public void setPhyInfra_sr_id(Integer phyInfra_sr_id) {
		this.phyInfra_sr_id = phyInfra_sr_id;
	}
	public String getDate_of_report() {
		return date_of_report;
	}
	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}
	public ULBModel getUlbModel() {
		return ulbModel;
	}
	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}
	public Double getNo_grnd_strg_res() {
		return no_grnd_strg_res;
	}
	public void setNo_grnd_strg_res(Double no_grnd_strg_res) {
		this.no_grnd_strg_res = no_grnd_strg_res;
	}
	public Double getCpty_grnd_strg_res() {
		return cpty_grnd_strg_res;
	}
	public void setCpty_grnd_strg_res(Double cpty_grnd_strg_res) {
		this.cpty_grnd_strg_res = cpty_grnd_strg_res;
	}
	public Double getNo_ele_strg_res() {
		return no_ele_strg_res;
	}
	public void setNo_ele_strg_res(Double no_ele_strg_res) {
		this.no_ele_strg_res = no_ele_strg_res;
	}
	public Double getCpty_ele_strg_res() {
		return cpty_ele_strg_res;
	}
	public void setCpty_ele_strg_res(Double cpty_ele_strg_res) {
		this.cpty_ele_strg_res = cpty_ele_strg_res;
	}

	
}
