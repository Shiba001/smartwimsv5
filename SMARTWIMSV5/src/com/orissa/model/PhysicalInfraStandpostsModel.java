package com.orissa.model;

public class PhysicalInfraStandpostsModel extends CommonModel {

	private Integer phyInfra_std_post_id;
	private ULBModel ulbModel;
	private Double no_exst_stanposts;
	private Double no_standposts_work_cond;
	private Double prop_standposts_work_cond;
	
	private String phyInfraStdPostId;
	
	public String getPhyInfraStdPostId() {
		return phyInfraStdPostId;
	}
	public void setPhyInfraStdPostId(String phyInfraStdPostId) {
		this.phyInfraStdPostId = phyInfraStdPostId;
	}
	public Integer getPhyInfra_std_post_id() {
		return phyInfra_std_post_id;
	}
	public void setPhyInfra_std_post_id(Integer phyInfra_std_post_id) {
		this.phyInfra_std_post_id = phyInfra_std_post_id;
	}
	public ULBModel getUlbModel() {
		return ulbModel;
	}
	public void setUlbModel(ULBModel ulbModel) {
		this.ulbModel = ulbModel;
	}
	public Double getNo_exst_stanposts() {
		return no_exst_stanposts;
	}
	public void setNo_exst_stanposts(Double no_exst_stanposts) {
		this.no_exst_stanposts = no_exst_stanposts;
	}
	public Double getNo_standposts_work_cond() {
		return no_standposts_work_cond;
	}
	public void setNo_standposts_work_cond(Double no_standposts_work_cond) {
		this.no_standposts_work_cond = no_standposts_work_cond;
	}
	public Double getProp_standposts_work_cond() {
		return prop_standposts_work_cond;
	}
	public void setProp_standposts_work_cond(Double prop_standposts_work_cond) {
		this.prop_standposts_work_cond = prop_standposts_work_cond;
	}

	
	
}
