package com.orissa.model;

import java.util.List;

public class IpModel {

	private Boolean get;
	private Boolean post;
	private Boolean cookies;
	private Boolean referer;
	private Boolean userAgent;
	private Integer anonymityLevel;
	private Boolean supportsHttps;
	private String protocol;
	private String ip;
	private String port;
	private String ipPort;
	private Integer tsChecked;
	private Websites websites;
	private String country;
	private String curl;
	private String type;
	private OtherProtocol otherProtocols = null;

	public Boolean getGet() {
		return get;
	}

	public void setGet(Boolean get) {
		this.get = get;
	}

	public Boolean getPost() {
		return post;
	}

	public void setPost(Boolean post) {
		this.post = post;
	}

	public Boolean getCookies() {
		return cookies;
	}

	public void setCookies(Boolean cookies) {
		this.cookies = cookies;
	}

	public Boolean getReferer() {
		return referer;
	}

	public void setReferer(Boolean referer) {
		this.referer = referer;
	}

	public Boolean getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(Boolean userAgent) {
		this.userAgent = userAgent;
	}

	public Integer getAnonymityLevel() {
		return anonymityLevel;
	}

	public void setAnonymityLevel(Integer anonymityLevel) {
		this.anonymityLevel = anonymityLevel;
	}

	public Boolean getSupportsHttps() {
		return supportsHttps;
	}

	public void setSupportsHttps(Boolean supportsHttps) {
		this.supportsHttps = supportsHttps;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getIpPort() {
		return ipPort;
	}

	public void setIpPort(String ipPort) {
		this.ipPort = ipPort;
	}

	public Integer getTsChecked() {
		return tsChecked;
	}

	public void setTsChecked(Integer tsChecked) {
		this.tsChecked = tsChecked;
	}

	public Websites getWebsites() {
		return websites;
	}

	public void setWebsites(Websites websites) {
		this.websites = websites;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCurl() {
		return curl;
	}

	public void setCurl(String curl) {
		this.curl = curl;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public OtherProtocol getOtherProtocols() {
		return otherProtocols;
	}

	public void setOtherProtocols(OtherProtocol otherProtocols) {
		this.otherProtocols = otherProtocols;
	}

}
