package com.orissa.model;

import java.util.LinkedList;

public class AjaxTankerModel {
	private LinkedList<DailyTankersModel> dailyTankersModels;

	public LinkedList<DailyTankersModel> getDailyTankersModels() {
		return dailyTankersModels;
	}

	public void setDailyTankersModels(
			LinkedList<DailyTankersModel> dailyTankersModels) {
		this.dailyTankersModels = dailyTankersModels;
	}

}
