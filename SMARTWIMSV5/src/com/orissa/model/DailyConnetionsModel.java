package com.orissa.model;

public class DailyConnetionsModel extends CommonModel {

	private int con_id;
	private Double no_new_res_cons_add;
	private Double no_res_cons_disc;
	private Double no_new_ind_cons_add;
	private Double no_ind_cons_disc;
	private Double no_new_com_cons_add;
	private Double no_com_cons_disc;
	private Double no_new_inst_cons_add;
	private Double no_inst_cons_disc;
	private Double tot_no_new_cons;
	private Double tot_no_cons_disc;
	private Integer pic;

	private Integer slNo;
	private Double total_residentials_provided;
	private Double total_residentials_diconnected;
	private Double total_industrial_provided;
	private Double total_industrial_diconnected;
	private Double total_commercial_provided;
	private Double total_commercial_diconnected;
	private Double total_institutional_provided;
	private Double total_institutional_diconnected;
	private Double total;
	private Integer total_pic;

	private String conId;

	public String getConId() {
		return conId;
	}

	public void setConId(String conId) {
		this.conId = conId;
	}

	public int getCon_id() {
		return con_id;
	}

	public void setCon_id(int con_id) {
		this.con_id = con_id;
	}

	public Double getNo_new_res_cons_add() {
		return no_new_res_cons_add;
	}

	public void setNo_new_res_cons_add(Double no_new_res_cons_add) {
		this.no_new_res_cons_add = no_new_res_cons_add;
	}

	public Double getNo_res_cons_disc() {
		return no_res_cons_disc;
	}

	public void setNo_res_cons_disc(Double no_res_cons_disc) {
		this.no_res_cons_disc = no_res_cons_disc;
	}

	public Double getNo_new_ind_cons_add() {
		return no_new_ind_cons_add;
	}

	public void setNo_new_ind_cons_add(Double no_new_ind_cons_add) {
		this.no_new_ind_cons_add = no_new_ind_cons_add;
	}

	public Double getNo_ind_cons_disc() {
		return no_ind_cons_disc;
	}

	public void setNo_ind_cons_disc(Double no_ind_cons_disc) {
		this.no_ind_cons_disc = no_ind_cons_disc;
	}

	public Double getNo_new_com_cons_add() {
		return no_new_com_cons_add;
	}

	public void setNo_new_com_cons_add(Double no_new_com_cons_add) {
		this.no_new_com_cons_add = no_new_com_cons_add;
	}

	public Double getNo_com_cons_disc() {
		return no_com_cons_disc;
	}

	public void setNo_com_cons_disc(Double no_com_cons_disc) {
		this.no_com_cons_disc = no_com_cons_disc;
	}

	public Double getNo_new_inst_cons_add() {
		return no_new_inst_cons_add;
	}

	public void setNo_new_inst_cons_add(Double no_new_inst_cons_add) {
		this.no_new_inst_cons_add = no_new_inst_cons_add;
	}

	public Double getNo_inst_cons_disc() {
		return no_inst_cons_disc;
	}

	public void setNo_inst_cons_disc(Double no_inst_cons_disc) {
		this.no_inst_cons_disc = no_inst_cons_disc;
	}

	public Double getTot_no_new_cons() {
		return tot_no_new_cons;
	}

	public void setTot_no_new_cons(Double tot_no_new_cons) {
		this.tot_no_new_cons = tot_no_new_cons;
	}

	public Double getTot_no_cons_disc() {
		return tot_no_cons_disc;
	}

	public void setTot_no_cons_disc(Double tot_no_cons_disc) {
		this.tot_no_cons_disc = tot_no_cons_disc;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public Double getTotal_residentials_provided() {
		return total_residentials_provided;
	}

	public void setTotal_residentials_provided(
			Double total_residentials_provided) {
		this.total_residentials_provided = total_residentials_provided;
	}

	public Double getTotal_residentials_diconnected() {
		return total_residentials_diconnected;
	}

	public void setTotal_residentials_diconnected(
			Double total_residentials_diconnected) {
		this.total_residentials_diconnected = total_residentials_diconnected;
	}

	public Double getTotal_industrial_provided() {
		return total_industrial_provided;
	}

	public void setTotal_industrial_provided(Double total_industrial_provided) {
		this.total_industrial_provided = total_industrial_provided;
	}

	public Double getTotal_industrial_diconnected() {
		return total_industrial_diconnected;
	}

	public void setTotal_industrial_diconnected(
			Double total_industrial_diconnected) {
		this.total_industrial_diconnected = total_industrial_diconnected;
	}

	public Double getTotal_commercial_provided() {
		return total_commercial_provided;
	}

	public void setTotal_commercial_provided(Double total_commercial_provided) {
		this.total_commercial_provided = total_commercial_provided;
	}

	public Double getTotal_commercial_diconnected() {
		return total_commercial_diconnected;
	}

	public void setTotal_commercial_diconnected(
			Double total_commercial_diconnected) {
		this.total_commercial_diconnected = total_commercial_diconnected;
	}

	public Double getTotal_institutional_provided() {
		return total_institutional_provided;
	}

	public void setTotal_institutional_provided(
			Double total_institutional_provided) {
		this.total_institutional_provided = total_institutional_provided;
	}

	public Double getTotal_institutional_diconnected() {
		return total_institutional_diconnected;
	}

	public void setTotal_institutional_diconnected(
			Double total_institutional_diconnected) {
		this.total_institutional_diconnected = total_institutional_diconnected;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Integer getPic() {
		return pic;
	}

	public void setPic(Integer pic) {
		this.pic = pic;
	}

	public Integer getTotal_pic() {
		return total_pic;
	}

	public void setTotal_pic(Integer total_pic) {
		this.total_pic = total_pic;
	}

}
