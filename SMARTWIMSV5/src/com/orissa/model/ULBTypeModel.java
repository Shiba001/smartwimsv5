package com.orissa.model;

public class ULBTypeModel {

	private int type_ulb_id;
	private String ULB_Type;

	public int getType_ulb_id() {
		return type_ulb_id;
	}

	public void setType_ulb_id(int type_ulb_id) {
		this.type_ulb_id = type_ulb_id;
	}

	public String getULB_Type() {
		return ULB_Type;
	}

	public void setULB_Type(String uLB_Type) {
		ULB_Type = uLB_Type;
	}

}
