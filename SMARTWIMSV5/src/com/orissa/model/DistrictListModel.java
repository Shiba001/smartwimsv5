package com.orissa.model;

public class DistrictListModel {
	private int dist_id;
	private int dist_Name;

	public int getDist_id() {
		return dist_id;
	}

	public void setDist_id(int dist_id) {
		this.dist_id = dist_id;
	}

	public int getDist_Name() {
		return dist_Name;
	}

	public void setDist_Name(int dist_Name) {
		this.dist_Name = dist_Name;
	}

}
