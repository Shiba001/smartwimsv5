package com.orissa.model;

public class CommonModel {

	private String date_of_report;
	private ULBNameModel ulbNameModel;
	private LoginUsersModel loginUsersModel;
	private String remarks;
	private String entry_date;
	private String modify_date;
	private Integer IsApproved;
	private int ulbId;
	private String type;
	private String currentMonth;
	private String previousMonth;
	private String csrfPreventionSalt;
	private String dueDate;

	public String getDate_of_report() {
		return date_of_report;
	}

	public void setDate_of_report(String date_of_report) {
		this.date_of_report = date_of_report;
	}

	public ULBNameModel getUlbNameModel() {
		return ulbNameModel;
	}

	public void setUlbNameModel(ULBNameModel ulbNameModel) {
		this.ulbNameModel = ulbNameModel;
	}

	public LoginUsersModel getLoginUsersModel() {
		return loginUsersModel;
	}

	public void setLoginUsersModel(LoginUsersModel loginUsersModel) {
		this.loginUsersModel = loginUsersModel;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getEntry_date() {
		return entry_date;
	}

	public void setEntry_date(String entry_date) {
		this.entry_date = entry_date;
	}

	public String getModify_date() {
		return modify_date;
	}

	public void setModify_date(String modify_date) {
		this.modify_date = modify_date;
	}

	public Integer getIsApproved() {
		return IsApproved;
	}

	public void setIsApproved(Integer isApproved) {
		IsApproved = isApproved;
	}

	public int getUlbId() {
		return ulbId;
	}

	public void setUlbId(int ulbId) {
		this.ulbId = ulbId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCurrentMonth() {
		return currentMonth;
	}

	public void setCurrentMonth(String currentMonth) {
		this.currentMonth = currentMonth;
	}

	public String getPreviousMonth() {
		return previousMonth;
	}

	public void setPreviousMonth(String previousMonth) {
		this.previousMonth = previousMonth;
	}

	public String getCsrfPreventionSalt() {
		return csrfPreventionSalt;
	}

	public void setCsrfPreventionSalt(String csrfPreventionSalt) {
		this.csrfPreventionSalt = csrfPreventionSalt;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

}
