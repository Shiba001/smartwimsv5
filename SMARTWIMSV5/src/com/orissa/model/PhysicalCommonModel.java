package com.orissa.model;

public class PhysicalCommonModel {
	private LoginUsersModel loginUsersModel;
	private String entry_date;
	private String modify_date;

	public LoginUsersModel getLoginUsersModel() {
		return loginUsersModel;
	}

	public void setLoginUsersModel(LoginUsersModel loginUsersModel) {
		this.loginUsersModel = loginUsersModel;
	}

	public String getEntry_date() {
		return entry_date;
	}

	public void setEntry_date(String entry_date) {
		this.entry_date = entry_date;
	}

	public String getModify_date() {
		return modify_date;
	}

	public void setModify_date(String modify_date) {
		this.modify_date = modify_date;
	}

}
