package com.orissa.validation;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.controller.ComplaintsController;
import com.orissa.dao.DailyComplaintsDao;
import com.orissa.dao.DeleteDao;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.DailyComplaintsModel;

@Component
public class DailyComplaintsValidation {

	private static Logger logger = Logger
			.getLogger(DailyComplaintsValidation.class);
	
	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DeleteDao deleteDao;
	
	@Autowired
	private DailyComplaintsDao dailyComplaintsDao;
	
	public void createDailyComplaintsValidation(DailyComplaintsModel dailyComplaintsModel) throws FormExceptions {
		
		if (logger.isInfoEnabled()) {
			logger.info("createDailyComplaintsValidation -- START");
		}
		
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if(Util.isEmpty(dailyComplaintsModel.getNo_ws_comp_recv())){
			exceptions.put("water.supply.complaints.received", new EmptyValueException(
					messageUtil.getBundle("water.supply.complaints.received")));
		} else if (Util.isNumber(String.valueOf(dailyComplaintsModel.getNo_ws_comp_recv()))) {
			exceptions.put("water.supply.complaints.received.numeric", new NumericException(
					messageUtil.getBundle("water.supply.complaints.received.numeric")));
		} else if(String.valueOf(dailyComplaintsModel.getNo_ws_comp_recv()).length() > 18){ 
			exceptions.put("water.supply.complaints.received.maxlength", new NumericException(messageUtil.getBundle("water.supply.complaints.received.maxlength"))); 
		} else {
			dailyComplaintsModel.setNo_ws_comp_recv(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyComplaintsModel.getNo_ws_comp_recv()), "url")));
		}
		
		if(Util.isEmpty(dailyComplaintsModel.getNo_ws_comp_resolved())){
			exceptions.put("water.supply.complaints.resolved", new EmptyValueException(
					messageUtil.getBundle("water.supply.complaints.resolved")));
		} else if (Util.isNumber(String.valueOf(dailyComplaintsModel.getNo_ws_comp_resolved()))) {
			exceptions.put("water.supply.complaints.resolved.numeric", new NumericException(
					messageUtil.getBundle("water.supply.complaints.resolved.numeric")));
		} else if(String.valueOf(dailyComplaintsModel.getNo_ws_comp_resolved()).length() > 18){
			exceptions.put("water.supply.complaints.resolved.maxlength", new NumericException(messageUtil.getBundle("water.supply.complaints.resolved.maxlength"))); 
		} else{
			dailyComplaintsModel.setNo_ws_comp_resolved(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyComplaintsModel.getNo_ws_comp_resolved()), "url")));
		}
		
		if(Util.isEmpty(dailyComplaintsModel.getNo_tw_comp_recv())){
			exceptions.put("tubewell.complaints.received", new EmptyValueException(
					messageUtil.getBundle("tubewell.complaints.received")));
		} else if (Util.isNumber(String.valueOf(dailyComplaintsModel.getNo_tw_comp_recv()))) {
			exceptions.put("tubewell.complaints.received.numeric", new NumericException(
					messageUtil.getBundle("tubewell.complaints.received.numeric")));
		} else if(String.valueOf(dailyComplaintsModel.getNo_tw_comp_recv()).length() > 18){
			exceptions.put("tubewell.complaints.received.maxlength", new NumericException(
					messageUtil.getBundle("tubewell.complaints.received.maxlength")));
		} else{
			dailyComplaintsModel.setNo_tw_comp_recv(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyComplaintsModel.getNo_tw_comp_recv()), "url")));
		}
		
		if(Util.isEmpty(dailyComplaintsModel.getNo_tw_comp_resolved())){
			exceptions.put("tubewell.complaints.resolved", new EmptyValueException(
					messageUtil.getBundle("tubewell.complaints.resolved")));
		} else if (Util.isNumber(String.valueOf(dailyComplaintsModel.getNo_tw_comp_resolved()))) {
			exceptions.put("tubewell.complaints.resolved.numeric", new NumericException(
					messageUtil.getBundle("tubewell.complaints.resolved.numeric")));
		} else if(String.valueOf(dailyComplaintsModel.getNo_tw_comp_resolved()).length() > 18){
			exceptions.put("tubewell.complaints.resolved.maxlength", new NumericException(
					messageUtil.getBundle("tubewell.complaints.resolved.maxlength")));
		}else{
			dailyComplaintsModel.setNo_tw_comp_resolved(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyComplaintsModel.getNo_tw_comp_resolved()), "url")));
		}
		
		
		if(dailyComplaintsModel.getRemarks().trim() != null){
			if(dailyComplaintsModel.getRemarks().trim().length() > 200){
				exceptions.put("remarks.maxlength", new NumericException(
						messageUtil.getBundle("remarks.maxlength")));
			}else{
				dailyComplaintsModel.setRemarks(Util.strip_html_tags(dailyComplaintsModel.getRemarks(), "url"));
			}
		}
		
		if(dailyComplaintsModel.getUlbId() != 0 && dailyComplaintsModel.getDate_of_report() != null) {
			
			DailyComplaintsModel dailyComplaintsModel2 = null;
			try {
				dailyComplaintsModel2 = dailyComplaintsDao.fetchDailyComplaintsById(dailyComplaintsModel.getTw_id());
				if(dailyComplaintsModel2 != null){
					
					if(!(dailyComplaintsModel.getDate_of_report().equals(dailyComplaintsModel2.getDate_of_report()))) {
						
						String tableName = "daily_complaints";
						int result = 0;
						try {
							result = deleteDao.entryCheckWithoutApproval(tableName, dailyComplaintsModel.getDate_of_report(), dailyComplaintsModel.getUlbId());
							if(result == 1){
								exceptions.put("data.entry.exist", new NumericException(
										messageUtil.getBundle("data.entry.exist")));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
						
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else {
			
			if(dailyComplaintsModel.getUlbId() == 0) {
				
				exceptions.put("ULBID.needed", new EmptyValueException(
						messageUtil.getBundle("ULBID.needed")));
			}else{
				dailyComplaintsModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(dailyComplaintsModel.getUlbId()), "url")));
			}
			
			if(dailyComplaintsModel.getDate_of_report() == null) {
				
				exceptions.put("dateofreport.needed", new EmptyValueException(
						messageUtil.getBundle("dateofreport.needed")));
			}else{
				dailyComplaintsModel.setDate_of_report(Util.strip_html_tags(dailyComplaintsModel.getDate_of_report(), "url"));
			}
		}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);
		
		if (logger.isInfoEnabled()) {
			logger.info("createDailyComplaintsValidation -- END");
		}
		
	}
}
