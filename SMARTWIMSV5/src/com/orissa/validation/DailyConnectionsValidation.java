package com.orissa.validation;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.dao.DailyConnectionsDao;
import com.orissa.dao.DeleteDao;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.DailyConnetionsModel;
import com.sun.org.apache.bcel.internal.generic.DALOAD;

@Component
public class DailyConnectionsValidation {

	private static Logger logger = Logger
			.getLogger(DailyConnectionsValidation.class);
	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DeleteDao deleteDao;
	
	@Autowired
	private DailyConnectionsDao dailyConnectionsDao;

	public void createDailyConnectionsValidation(
			DailyConnetionsModel dailyConnetionsModel) throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createDailyConnectionsValidation -- START");
		}

		Map<String, Exception> exceptions = new HashMap<String, Exception>();

		if (Util.isEmpty(dailyConnetionsModel.getNo_new_res_cons_add())) {
			exceptions.put(
					"new.residential.connections.added",
					new EmptyValueException(messageUtil
							.getBundle("new.residential.connections.added")));
		} else if (Util.isNumber(String.valueOf(dailyConnetionsModel
				.getNo_new_res_cons_add()))) {
			exceptions
					.put("new.residential.connections.added.numeric",
							new NumericException(
									messageUtil
											.getBundle("new.residential.connections.added.numeric")));
		}else if(String.valueOf(dailyConnetionsModel.getNo_new_res_cons_add()).length() > 18){ 
			exceptions.put("new.residential.connections.added.maxlength", new NumericException(messageUtil.getBundle("new.residential.connections.added.maxlength"))); 
		} else{
			dailyConnetionsModel.setNo_new_res_cons_add(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyConnetionsModel.getNo_new_res_cons_add()), "url")));
		}

		if (Util.isEmpty(dailyConnetionsModel.getNo_res_cons_disc())) {
			exceptions
					.put("residential.connections.disconnected",
							new EmptyValueException(
									messageUtil
											.getBundle("residential.connections.disconnected")));
		} else if (Util.isNumber(String.valueOf(dailyConnetionsModel
				.getNo_res_cons_disc()))) {
			exceptions
					.put("residential.connections.disconnected.numeric",
							new NumericException(
									messageUtil
											.getBundle("residential.connections.disconnected.numeric")));
		}else if(String.valueOf(dailyConnetionsModel.getNo_res_cons_disc()).length() > 18){ 
			exceptions.put("residential.connections.disconnected.maxlength", new NumericException(messageUtil.getBundle("residential.connections.disconnected.maxlength"))); 
		} else{
			dailyConnetionsModel.setNo_res_cons_disc(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyConnetionsModel.getNo_res_cons_disc()), "url")));
		}

		if (Util.isEmpty(dailyConnetionsModel.getNo_new_ind_cons_add())) {
			exceptions.put(
					"New.Industrial.Connections.Added",
					new EmptyValueException(messageUtil
							.getBundle("New.Industrial.Connections.Added")));
		} else if (Util.isNumber(String.valueOf(dailyConnetionsModel
				.getNo_new_ind_cons_add()))) {
			exceptions
					.put("New.Industrial.Connections.Added.numeric",
							new NumericException(
									messageUtil
											.getBundle("New.Industrial.Connections.Added.numeric")));
		}else if(String.valueOf(dailyConnetionsModel.getNo_new_ind_cons_add()).length() > 18){ 
			exceptions.put("New.Industrial.Connections.Added.maxlength", new NumericException(messageUtil.getBundle("New.Industrial.Connections.Added.maxlength"))); 
		} else{
			dailyConnetionsModel.setNo_new_ind_cons_add(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyConnetionsModel.getNo_new_ind_cons_add()), "url")));
		}

		if (Util.isEmpty(dailyConnetionsModel.getNo_ind_cons_disc())) {
			exceptions.put(
					"Industrial.Connections.Disconnected",
					new EmptyValueException(messageUtil
							.getBundle("Industrial.Connections.Disconnected")));
		} else if (Util.isNumber(String.valueOf(dailyConnetionsModel
				.getNo_ind_cons_disc()))) {
			exceptions
					.put("Industrial.Connections.Disconnected.numeric",
							new NumericException(
									messageUtil
											.getBundle("Industrial.Connections.Disconnected.numeric")));
		}else if(String.valueOf(dailyConnetionsModel.getNo_ind_cons_disc()).length() > 18){ 
			exceptions.put("Industrial.Connections.Disconnected.maxlength", new NumericException(messageUtil.getBundle("Industrial.Connections.Disconnected.maxlength"))); 
		} else{
			dailyConnetionsModel.setNo_ind_cons_disc(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyConnetionsModel.getNo_ind_cons_disc()), "url")));
		}

		if (Util.isEmpty(dailyConnetionsModel.getNo_new_com_cons_add())) {
			exceptions.put(
					"New.Commercial.Connections.Added",
					new EmptyValueException(messageUtil
							.getBundle("New.Commercial.Connections.Added")));
		} else if (Util.isNumber(String.valueOf(dailyConnetionsModel
				.getNo_new_com_cons_add()))) {
			exceptions
					.put("New.Commercial.Connections.Added.numeric",
							new NumericException(
									messageUtil
											.getBundle("New.Commercial.Connections.Added.numeric")));
		}else if(String.valueOf(dailyConnetionsModel.getNo_new_com_cons_add()).length() > 18){ 
			exceptions.put("New.Commercial.Connections.Added.maxlength", new NumericException(messageUtil.getBundle("New.Commercial.Connections.Added.maxlength"))); 
		} else{
			dailyConnetionsModel.setNo_new_com_cons_add(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyConnetionsModel.getNo_new_com_cons_add()), "url")));
		}

		if (Util.isEmpty(dailyConnetionsModel.getNo_com_cons_disc())) {
			exceptions.put(
					"Commercial.Connections.Disconnected",
					new EmptyValueException(messageUtil
							.getBundle("Commercial.Connections.Disconnected")));
		} else if (Util.isNumber(String.valueOf(dailyConnetionsModel
				.getNo_com_cons_disc()))) {
			exceptions
					.put("Commercial.Connections.Disconnected.numeric",
							new NumericException(
									messageUtil
											.getBundle("Commercial.Connections.Disconnected.numeric")));
		}else if(String.valueOf(dailyConnetionsModel.getNo_com_cons_disc()).length() > 18){ 
			exceptions.put("Commercial.Connections.Disconnected.maxlength", new NumericException(messageUtil.getBundle("Commercial.Connections.Disconnected.maxlength"))); 
		} else{
			dailyConnetionsModel.setNo_com_cons_disc(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyConnetionsModel.getNo_com_cons_disc()), "url")));
		}

		if (Util.isEmpty(dailyConnetionsModel.getNo_new_inst_cons_add())) {
			exceptions.put(
					"New.Institutional.Connections.Added",
					new EmptyValueException(messageUtil
							.getBundle("New.Institutional.Connections.Added")));
		} else if (Util.isNumber(String.valueOf(dailyConnetionsModel
				.getNo_new_inst_cons_add()))) {
			exceptions
					.put("New.Institutional.Connections.Added.numeric",
							new NumericException(
									messageUtil
											.getBundle("New.Institutional.Connections.Added.numeric")));
		}else if(String.valueOf(dailyConnetionsModel.getNo_new_inst_cons_add()).length() > 18){ 
			exceptions.put("New.Institutional.Connections.Added.maxlength", new NumericException(messageUtil.getBundle("New.Institutional.Connections.Added.maxlength"))); 
		} else{
			dailyConnetionsModel.setNo_new_inst_cons_add(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyConnetionsModel.getNo_new_inst_cons_add()), "url")));
		}

		if (Util.isEmpty(dailyConnetionsModel.getNo_inst_cons_disc())) {
			exceptions
					.put("New.Institutional.Connections.Disconnected",
							new EmptyValueException(
									messageUtil
											.getBundle("New.Institutional.Connections.Disconnected")));
		} else if (Util.isNumber(String.valueOf(dailyConnetionsModel
				.getNo_inst_cons_disc()))) {
			exceptions
					.put("New.Institutional.Connections.Disconnected.numeric",
							new NumericException(
									messageUtil
											.getBundle("New.Institutional.Connections.Disconnected.numeric")));
		}else if(String.valueOf(dailyConnetionsModel.getNo_inst_cons_disc()).length() > 18){ 
			exceptions.put("New.Institutional.Connections.Disconnected.maxlength", new NumericException(messageUtil.getBundle("New.Institutional.Connections.Disconnected.maxlength"))); 
		} else{
			dailyConnetionsModel.setNo_inst_cons_disc(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyConnetionsModel.getNo_inst_cons_disc()), "url")));
		}
		
		if(dailyConnetionsModel.getRemarks().trim() != null){
			if(dailyConnetionsModel.getRemarks().trim().length() > 200){
				exceptions.put("remarks.maxlength", new NumericException(
						messageUtil.getBundle("remarks.maxlength")));
			} else{
				dailyConnetionsModel.setRemarks(Util.strip_html_tags(dailyConnetionsModel.getRemarks(), "url"));
			}
		}
		if(dailyConnetionsModel.getUlbId() != 0 && dailyConnetionsModel.getDate_of_report() != null){
			DailyConnetionsModel dailyConnetionsModel2 = null;
			try{
				dailyConnetionsModel2 = dailyConnectionsDao.fetchDailyConnetionsById(dailyConnetionsModel.getCon_id());
				if(dailyConnetionsModel2 != null){
					if(!(dailyConnetionsModel.getDate_of_report().equals(dailyConnetionsModel2.getDate_of_report()))){
						String tableName = " daily_connection";
						int result = 0;
						try {
							result = deleteDao.entryCheckWithoutApproval(tableName, dailyConnetionsModel.getDate_of_report(), dailyConnetionsModel.getUlbId());
							if(result == 1){
								exceptions.put("data.entry.exist", new NumericException(
										messageUtil.getBundle("data.entry.exist")));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			} catch(Exception e){
				e.printStackTrace();
			}
			
		}
		else {
			
			if(dailyConnetionsModel.getUlbId() == 0) {
				
				exceptions.put("ULBID.needed", new EmptyValueException(
						messageUtil.getBundle("ULBID.needed")));
			}else{
				dailyConnetionsModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(dailyConnetionsModel.getUlbId()), "url")));
			}
			
			if(dailyConnetionsModel.getDate_of_report() == null) {
				
				exceptions.put("dateofreport.needed", new EmptyValueException(
						messageUtil.getBundle("dateofreport.needed")));
			}else{
				dailyConnetionsModel.setDate_of_report(Util.strip_html_tags(dailyConnetionsModel.getDate_of_report(), "url"));
			}
		}

		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		if (logger.isInfoEnabled()) {
			logger.info("createDailyConnectionsValidation -- END");
		}

	}
}
