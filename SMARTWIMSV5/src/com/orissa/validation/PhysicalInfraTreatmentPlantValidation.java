package com.orissa.validation;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.PhysicalInfraWaterTreatmentPlantModel;

@Component
public class PhysicalInfraTreatmentPlantValidation {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraTreatmentPlantValidation.class);

	@Autowired
	private MessageUtil messageUtil;

	public void createPhysicalInfraTreatmentPlantValidation(
			PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel) throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraTreatmentPlantValidation -- START");
		}

		Map<String, Exception> exceptions = new HashMap<String, Exception>();

		if (Util.isEmpty(physicalInfraWaterTreatmentPlantModel.getNo_of_wtp())) {
			exceptions.put(
					"No..of.Water.Treatment.Plants",
					new EmptyValueException(messageUtil
							.getBundle("No..of.Water.Treatment.Plants")));
		} else if (Util.isNumber(String.valueOf(physicalInfraWaterTreatmentPlantModel.getNo_of_wtp()))) {
			exceptions
					.put("No..of.Water.Treatment.Plants.numeric",
							new NumericException(
									messageUtil
											.getBundle("No..of.Water.Treatment.Plants.numeric")));
		} else if(String.valueOf(physicalInfraWaterTreatmentPlantModel.getNo_of_wtp()).length() > 18){
			exceptions.put("No..of.Water.Treatment.Plants.maxlength", new EmptyValueException(
					messageUtil.getBundle("No..of.Water.Treatment.Plants.maxlength")));
		} else{
			physicalInfraWaterTreatmentPlantModel.setNo_of_wtp(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraWaterTreatmentPlantModel.getNo_of_wtp()), "url")));
		}

		if (Util.isEmpty(physicalInfraWaterTreatmentPlantModel.getCpty_wtp())) {
			exceptions.put(
					"Capacity.of.WTP",
					new EmptyValueException(messageUtil
							.getBundle("Capacity.of.WTP")));
		} else if (Util.isNumeric(String.valueOf(physicalInfraWaterTreatmentPlantModel.getCpty_wtp()))) {
			exceptions
					.put("Capacity.of.WTP.numeric",
							new NumericException(
									messageUtil
											.getBundle("Capacity.of.WTP.numeric")));
		} else if(String.valueOf(physicalInfraWaterTreatmentPlantModel.getCpty_wtp()).length() > 18){
			exceptions.put("Capacity.of.WTP.maxlength", new EmptyValueException(
					messageUtil.getBundle("Capacity.of.WTP.maxlength")));
		}else{
			physicalInfraWaterTreatmentPlantModel.setCpty_wtp(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraWaterTreatmentPlantModel.getCpty_wtp()), "url")));
		}

		if(physicalInfraWaterTreatmentPlantModel.getUlbId() == 0) {
			
			exceptions.put("ULBID.needed", new EmptyValueException(
					messageUtil.getBundle("ULBID.needed")));
		}else{
			physicalInfraWaterTreatmentPlantModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(physicalInfraWaterTreatmentPlantModel.getUlbId()), "url")));
		}
		
		if(physicalInfraWaterTreatmentPlantModel.getDate_of_report() == null) {
			
			exceptions.put("dateofreport.needed", new EmptyValueException(
					messageUtil.getBundle("dateofreport.needed")));
		}else{
			physicalInfraWaterTreatmentPlantModel.setDate_of_report(Util.strip_html_tags(physicalInfraWaterTreatmentPlantModel.getDate_of_report(), "url"));
		}

		if(physicalInfraWaterTreatmentPlantModel.getDueDate() == null){
			exceptions.put("duedate.needed", new EmptyValueException(
					messageUtil.getBundle("duedate.needed")));
		} else
			try {
				if(!(Util.checkDueDate(physicalInfraWaterTreatmentPlantModel.getDueDate()))){
					exceptions.put("duedate.greater", new EmptyValueException(
							messageUtil.getBundle("duedate.greater")));
				} else{
					physicalInfraWaterTreatmentPlantModel.setDueDate(Util.strip_html_tags(physicalInfraWaterTreatmentPlantModel.getDueDate(), "url"));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				exceptions.put("strip.empty", new EmptyValueException(
						messageUtil.getBundle("strip.empty")));
			}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraTreatmentPlantValidation -- END");
		}

	}
}
