package com.orissa.validation;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.PhysicalInfraStandpostsModel;
import com.orissa.model.PhysicalInfraStorageReservoirsModel;
@Component
public class PhysicalInfraReservoirsValidation {
	
	private static Logger logger = Logger
			.getLogger(PhysicalInfraStandpostValidation.class);
	
	@Autowired
	private MessageUtil messageUtil;
	
	public void createPhysicalInfraReservoirsValidation(PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel) throws FormExceptions {
		
		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraReservoirsValidation -- START");
		}
		
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if(Util.isEmpty(physicalInfraStorageReservoirsModel.getNo_grnd_strg_res())){
			exceptions.put("number.of.ground.storage.reservoirs", new EmptyValueException(
					messageUtil.getBundle("number.of.ground.storage.reservoirs")));
		} else if (Util.isNumber(String.valueOf(physicalInfraStorageReservoirsModel.getNo_grnd_strg_res()))) {
			exceptions.put("number.of.ground.storage.reservoirs.numeric", new NumericException(
					messageUtil.getBundle("number.of.ground.storage.reservoirs.numeric")));
		}else if(String.valueOf(physicalInfraStorageReservoirsModel.getNo_grnd_strg_res()).length() > 18){ 
			exceptions.put("number.of.ground.storage.reservoirs.maxlength", new NumericException(messageUtil.getBundle("number.of.ground.storage.reservoirs.maxlength"))); 
		} else{
			physicalInfraStorageReservoirsModel.setNo_grnd_strg_res(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraStorageReservoirsModel.getNo_grnd_strg_res()), "url")));
		}
		
		if(Util.isEmpty(physicalInfraStorageReservoirsModel.getCpty_grnd_strg_res())){
			exceptions.put("capacity.of.ground.storage.reservoirs", new EmptyValueException(
					messageUtil.getBundle("capacity.of.ground.storage.reservoirs")));
		} else if (Util.isNumeric(String.valueOf(physicalInfraStorageReservoirsModel.getCpty_grnd_strg_res()))) {
			exceptions.put("capacity.of.ground.storage.reservoirs.numeric", new NumericException(
					messageUtil.getBundle("capacity.of.ground.storage.reservoirs.numeric")));
		}else if(String.valueOf(physicalInfraStorageReservoirsModel.getCpty_grnd_strg_res()).length() > 18){ 
			exceptions.put("capacity.of.ground.storage.reservoirs.maxlength", new NumericException(messageUtil.getBundle("capacity.of.ground.storage.reservoirs.maxlength"))); 
		} else{
			physicalInfraStorageReservoirsModel.setCpty_grnd_strg_res(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraStorageReservoirsModel.getCpty_grnd_strg_res()), "url")));
		}
		
		
		if(Util.isEmpty(physicalInfraStorageReservoirsModel.getNo_ele_strg_res())){
			exceptions.put("number.of.elevated.storage.reservoirs", new EmptyValueException(
					messageUtil.getBundle("number.of.elevated.storage.reservoirs")));
		} else if (Util.isNumber(String.valueOf(physicalInfraStorageReservoirsModel.getNo_ele_strg_res()))) {
			exceptions.put("number.of.elevated.storage.reservoirs.numeric", new NumericException(
					messageUtil.getBundle("number.of.elevated.storage.reservoirs.numeric")));
		}else if(String.valueOf(physicalInfraStorageReservoirsModel.getNo_ele_strg_res()).length() > 18){ 
			exceptions.put("number.of.elevated.storage.reservoirs.maxlength", new NumericException(messageUtil.getBundle("number.of.elevated.storage.reservoirs.maxlength"))); 
		} else{
			physicalInfraStorageReservoirsModel.setNo_ele_strg_res(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraStorageReservoirsModel.getNo_ele_strg_res()), "url")));
		}
		
		
		if(Util.isEmpty(physicalInfraStorageReservoirsModel.getCpty_ele_strg_res())){
			exceptions.put("capacity.of.elevated.storage.reservoirs", new EmptyValueException(
					messageUtil.getBundle("capacity.of.elevated.storage.reservoirs")));
		} else if (Util.isNumeric(String.valueOf(physicalInfraStorageReservoirsModel.getCpty_ele_strg_res()))) {
			exceptions.put("capacity.of.elevated.storage.reservoirs.numeric", new NumericException(
					messageUtil.getBundle("capacity.of.elevated.storage.reservoirs.numeric")));
		}else if(String.valueOf(physicalInfraStorageReservoirsModel.getCpty_ele_strg_res()).length() > 18){ 
			exceptions.put("capacity.of.elevated.storage.reservoirs.maxlength", new NumericException(messageUtil.getBundle("capacity.of.elevated.storage.reservoirs.maxlength"))); 
		} else{
			physicalInfraStorageReservoirsModel.setCpty_ele_strg_res(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraStorageReservoirsModel.getCpty_ele_strg_res()), "url")));
		}
		if(physicalInfraStorageReservoirsModel.getUlbId() == 0) {
			
			exceptions.put("ULBID.needed", new EmptyValueException(
					messageUtil.getBundle("ULBID.needed")));
		}else{
			physicalInfraStorageReservoirsModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(physicalInfraStorageReservoirsModel.getUlbId()), "url")));
		}
		
		if(physicalInfraStorageReservoirsModel.getDate_of_report() == null) {
			
			exceptions.put("dateofreport.needed", new EmptyValueException(
					messageUtil.getBundle("dateofreport.needed")));
		}else{
			physicalInfraStorageReservoirsModel.setDate_of_report(Util.strip_html_tags(physicalInfraStorageReservoirsModel.getDate_of_report(), "url"));
		}
		
		if(physicalInfraStorageReservoirsModel.getDueDate() == null){
			exceptions.put("duedate.needed", new EmptyValueException(
					messageUtil.getBundle("duedate.needed")));
		} else
			try {
				if(!(Util.checkDueDate(physicalInfraStorageReservoirsModel.getDueDate()))){
					exceptions.put("duedate.greater", new EmptyValueException(
							messageUtil.getBundle("duedate.greater")));
				} else{
					physicalInfraStorageReservoirsModel.setDueDate(Util.strip_html_tags(physicalInfraStorageReservoirsModel.getDueDate(), "url"));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				exceptions.put("strip.empty", new EmptyValueException(
						messageUtil.getBundle("strip.empty")));
			}
	
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);
		
		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraReservoirsValidation -- END");
		}
		
	}


}
