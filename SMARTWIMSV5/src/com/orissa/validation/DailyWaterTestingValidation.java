package com.orissa.validation;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.dao.DailyWaterTestingDao;
import com.orissa.dao.DeleteDao;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.DailyWaterTestingModel;

@Component
public class DailyWaterTestingValidation {

	private static Logger logger = Logger
			.getLogger(DailyWaterTestingValidation.class);

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DeleteDao deleteDao;
	
	@Autowired
	private DailyWaterTestingDao dailyWaterTestingDao;

	public void createDailyWaterTestingValidation(
			DailyWaterTestingModel dailyWaterTestingModel)
			throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createDailyWaterTestingValidation -- START");
		}

		Map<String, Exception> exceptions = new HashMap<String, Exception>();

		if (Util.isEmpty(dailyWaterTestingModel.getNo_bact_samp_tested())) {
			exceptions
					.put("No.of.Bacteriological.Samples.Tested",
							new EmptyValueException(
									messageUtil
											.getBundle("No.of.Bacteriological.Samples.Tested")));
		} else if (Util.isNumber(String.valueOf(dailyWaterTestingModel
				.getNo_bact_samp_tested()))) {
			exceptions
					.put("No.of.Bacteriological.Samples.Tested.numeric",
							new NumericException(
									messageUtil
											.getBundle("No.of.Bacteriological.Samples.Tested.numeric")));
		}else if(String.valueOf(dailyWaterTestingModel.getNo_bact_samp_tested()).length() > 18){
			exceptions.put("No.of.Bacteriological.Samples.Tested.maxlength", new EmptyValueException(
					messageUtil.getBundle("No.of.Bacteriological.Samples.Tested.maxlength")));
		} else{
			dailyWaterTestingModel.setNo_bact_samp_tested(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyWaterTestingModel.getNo_bact_samp_tested()), "url")));
		}

		if (Util.isEmpty(dailyWaterTestingModel.getNo_bact_samp_found_ok())) {
			exceptions
					.put("No.of.Bacteriological.Samples.Found.OK",
							new EmptyValueException(
									messageUtil
											.getBundle("No.of.Bacteriological.Samples.Found.OK")));
		} else if (Util.isNumber(String.valueOf(dailyWaterTestingModel
				.getNo_bact_samp_found_ok()))) {
			exceptions
					.put("No.of.Bacteriological.Samples.Found.OK.numeric",
							new NumericException(
									messageUtil
											.getBundle("No.of.Bacteriological.Samples.Found.OK.numeric")));
		}else if(String.valueOf(dailyWaterTestingModel.getNo_bact_samp_found_ok()).length() > 18){
			exceptions.put("No.of.Bacteriological.Samples.Found.OK.maxlength", new EmptyValueException(
					messageUtil.getBundle("No.of.Bacteriological.Samples.Found.OK.maxlength")));
		} else{
			dailyWaterTestingModel.setNo_bact_samp_found_ok(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyWaterTestingModel.getNo_bact_samp_found_ok()), "url")));
		}

		if (Util.isEmpty(dailyWaterTestingModel.getNo_other_samp_tested())) {
			exceptions.put("No.of.FRC.Samples.Tested", new EmptyValueException(
					messageUtil.getBundle("No.of.FRC.Samples.Tested")));
		} else if (Util.isNumber(String.valueOf(dailyWaterTestingModel
				.getNo_other_samp_tested()))) {
			exceptions.put(
					"No.of.FRC.Samples.Tested.numeric",
					new NumericException(messageUtil
							.getBundle("No.of.FRC.Samples.Tested.numeric")));
		} else if(String.valueOf(dailyWaterTestingModel.getNo_other_samp_tested()).length() > 18){
			exceptions.put("No.of.FRC.Samples.Tested.maxlength", new EmptyValueException(
					messageUtil.getBundle("No.of.FRC.Samples.Tested.maxlength")));
		} else{
			dailyWaterTestingModel.setNo_other_samp_tested(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyWaterTestingModel.getNo_other_samp_tested()), "url")));
		}

		if (Util.isEmpty(dailyWaterTestingModel.getNo_other_samp_found_ok())) {
			exceptions.put(
					"No.of.FRC.Samples.Found.OK",
					new EmptyValueException(messageUtil
							.getBundle("No.of.FRC.Samples.Found.OK")));
		} else if (Util.isNumber(String.valueOf(dailyWaterTestingModel
				.getNo_other_samp_found_ok()))) {
			exceptions.put(
					"No.of.FRC.Samples.Found.OK.numeric",
					new NumericException(messageUtil
							.getBundle("No.of.FRC.Samples.Found.OK.numeric")));
		} else if(String.valueOf(dailyWaterTestingModel.getNo_other_samp_found_ok()).length() > 18){
			exceptions.put("No.of.FRC.Samples.Found.OK.maxlength", new EmptyValueException(
					messageUtil.getBundle("No.of.FRC.Samples.Found.OK.maxlength")));
		} else{
			dailyWaterTestingModel.setNo_other_samp_found_ok(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyWaterTestingModel.getNo_other_samp_found_ok()), "url")));
		}
		
		if(dailyWaterTestingModel.getRemarks().trim() != null){
			if(dailyWaterTestingModel.getRemarks().trim().length() > 200){
				exceptions.put("remarks.maxlength", new EmptyValueException(
						messageUtil.getBundle("remarks.maxlength")));
			}else{
				dailyWaterTestingModel.setRemarks(Util.strip_html_tags(dailyWaterTestingModel.getRemarks(), "url"));
			}
		}
		
		if(dailyWaterTestingModel.getUlbId() != 0 && dailyWaterTestingModel.getDate_of_report() != null){
			DailyWaterTestingModel dailyWaterTestingModel2 = null;
			try{
				dailyWaterTestingModel2 = dailyWaterTestingDao.fetchDailyWaterTestingById(dailyWaterTestingModel.getWt_id());
				if(dailyWaterTestingModel2 != null){
					if(!(dailyWaterTestingModel.getDate_of_report().equals(dailyWaterTestingModel2.getDate_of_report()))){
						String tableName = "daily_water_testing";
						int result = 0;
						try {
							result = deleteDao.entryCheckWithoutApproval(tableName, dailyWaterTestingModel.getDate_of_report(), dailyWaterTestingModel.getUlbId());
							if(result == 1){
								exceptions.put("data.entry.exist", new NumericException(
										messageUtil.getBundle("data.entry.exist")));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			} catch(Exception e){
				
			}
			
		} else {
			
			if(dailyWaterTestingModel.getUlbId() == 0) {
				
				exceptions.put("ULBID.needed", new EmptyValueException(
						messageUtil.getBundle("ULBID.needed")));
			} else{
				dailyWaterTestingModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(dailyWaterTestingModel.getUlbId()), "url")));
			}
			
			if(dailyWaterTestingModel.getDate_of_report() == null) {
				
				exceptions.put("dateofreport.needed", new EmptyValueException(
						messageUtil.getBundle("dateofreport.needed")));
			} else{
				dailyWaterTestingModel.setDate_of_report(Util.strip_html_tags(dailyWaterTestingModel.getDate_of_report(), "url"));
			}
		}

		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		if (logger.isInfoEnabled()) {
			logger.info("createDailyWaterTestingValidation -- END");
		}

	}
}
