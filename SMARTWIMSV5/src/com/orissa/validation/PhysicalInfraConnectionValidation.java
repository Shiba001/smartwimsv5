package com.orissa.validation;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.PhysicalInfraConnectionModel;


@Component
public class PhysicalInfraConnectionValidation {
	

	private static Logger logger = Logger
			.getLogger(PhysicalInfraConnectionValidation .class);
	
	@Autowired
	private MessageUtil messageUtil;

	public void createPhysicalInfraConnectionValidation(PhysicalInfraConnectionModel physicalInfraConnectionModel) throws FormExceptions {
		
		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraConnectionValidation -- START");
		}
		
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if(Util.isEmpty(physicalInfraConnectionModel.getNo_res_cons())){
			exceptions.put("no.of.residential.connection.provided", new EmptyValueException(
					messageUtil.getBundle("no.of.residential.connection.provided")));
		} else if (Util.isNumber(String.valueOf(physicalInfraConnectionModel.getNo_res_cons()))){
			exceptions.put("no.of.residential.connection.provided.numeric", new NumericException(
					messageUtil.getBundle("no.of.residential.connection.provided.numeric")));
		}else if(String.valueOf(physicalInfraConnectionModel.getNo_res_cons()).length() > 18){ 
			exceptions.put("no.of.residential.connection.provided.maxlength", new NumericException(messageUtil.getBundle("total.no.tubewells.in.working.condition.maxlength"))); 
		} else{
			physicalInfraConnectionModel.setNo_res_cons(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraConnectionModel.getNo_res_cons()), "url")));
		}
		
		
		if(Util.isEmpty(physicalInfraConnectionModel.getNo_ind_cons())){
			exceptions.put("no.of.industrial.connection.provided", new EmptyValueException(
					messageUtil.getBundle("no.of.industrial.connection.provided")));
		} else if (Util.isNumber(String.valueOf(physicalInfraConnectionModel.getNo_ind_cons()))) {
			exceptions.put("no.of.industrial.connection.provided.numeric", new NumericException(
					messageUtil.getBundle("no.of.industrial.connection.provided.numeric")));
		}else if(String.valueOf(physicalInfraConnectionModel.getNo_ind_cons()).length() > 18){ 
			exceptions.put("no.of.industrial.connection.provided.maxlength", new NumericException(messageUtil.getBundle("total.no.tubewells.in.working.condition.maxlength"))); 
		} else{
			physicalInfraConnectionModel.setNo_ind_cons(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraConnectionModel.getNo_ind_cons()), "url")));
		}
		
		if(Util.isEmpty(physicalInfraConnectionModel.getNo_com_cons())){
			exceptions.put("no.of.commercial.connection.provided", new EmptyValueException(
					messageUtil.getBundle("no.of.commercial.connection.provided")));
		} else if (Util.isNumber(String.valueOf(physicalInfraConnectionModel.getNo_com_cons()))){
			exceptions.put("no.of.commercial.connection.provided.numeric", new NumericException(
					messageUtil.getBundle("no.of.commercial.connection.provided.numeric")));
		}else if(String.valueOf(physicalInfraConnectionModel.getNo_com_cons()).length() > 18){ 
			exceptions.put("no.of.commercial.connection.provided.maxlength", new NumericException(messageUtil.getBundle("total.no.tubewells.in.working.condition.maxlength"))); 
		} else{
			physicalInfraConnectionModel.setNo_com_cons(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraConnectionModel.getNo_com_cons()), "url")));
		}
		
		if(Util.isEmpty(physicalInfraConnectionModel.getNo_inst_cons())){
			exceptions.put("no.of.institutional.connection.provided", new EmptyValueException(
					messageUtil.getBundle("no.of.institutional.connection.provided")));
		} else if (Util.isNumber(String.valueOf(physicalInfraConnectionModel.getNo_inst_cons()))) {
			exceptions.put("no.of.institutional.connection.provided.numeric", new NumericException(
					messageUtil.getBundle("no.of.institutional.connection.provided.numeric")));
		}else if(String.valueOf(physicalInfraConnectionModel.getNo_inst_cons()).length() > 18){ 
			exceptions.put("no.of.institutional.connection.provided.maxlength", new NumericException(messageUtil.getBundle("total.no.tubewells.in.working.condition.maxlength"))); 
		} else{
			physicalInfraConnectionModel.setNo_inst_cons(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraConnectionModel.getNo_inst_cons()), "url")));
		}
		
		if(physicalInfraConnectionModel.getUlbId() == 0) {
			
			exceptions.put("ULBID.needed", new EmptyValueException(
					messageUtil.getBundle("ULBID.needed")));
		}else{
			physicalInfraConnectionModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(physicalInfraConnectionModel.getUlbId()), "url")));
		}
		
		if(Util.isEmpty(physicalInfraConnectionModel.getDate_of_report())) {
			
			exceptions.put("dateofreport.needed", new EmptyValueException(
					messageUtil.getBundle("dateofreport.needed")));
		}else{
			physicalInfraConnectionModel.setDate_of_report(Util.strip_html_tags(physicalInfraConnectionModel.getDate_of_report(), "url"));
		}
		
		if(Util.isEmpty(physicalInfraConnectionModel.getDueDate())){
			exceptions.put("duedate.needed", new EmptyValueException(
					messageUtil.getBundle("duedate.needed")));
		} else
			try {
				if(!(Util.checkDueDate(physicalInfraConnectionModel.getDueDate()))){
					exceptions.put("duedate.greater", new EmptyValueException(
							messageUtil.getBundle("duedate.greater")));
				} else{
					physicalInfraConnectionModel.setDueDate(Util.strip_html_tags(physicalInfraConnectionModel.getDueDate(), "url"));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				exceptions.put("strip.empty", new EmptyValueException(
						messageUtil.getBundle("strip.empty")));
			}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);
		
		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraConnectionValidation -- END");
		}
		
	}
}
