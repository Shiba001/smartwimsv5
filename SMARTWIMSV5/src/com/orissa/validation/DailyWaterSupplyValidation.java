package com.orissa.validation;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.dao.DailyWaterSupplyDao;
import com.orissa.dao.DeleteDao;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.DailyWaterSupplyModel;

@Component
public class DailyWaterSupplyValidation {

	private static Logger logger = Logger
			.getLogger(DailyWaterSupplyValidation.class);
	
	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DeleteDao deleteDao;
	
	@Autowired
	private DailyWaterSupplyDao dailyWaterSupplyDao;
	
	public void createDailyWaterSupplyValidation(DailyWaterSupplyModel dailyWaterSupplyModel) throws FormExceptions {
		
		if (logger.isInfoEnabled()) {
			logger.info("createDailyWaterSupplyValidation -- START");
		}
		
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if(Util.isEmpty(dailyWaterSupplyModel.getTot_demand())){
			exceptions.put("water.supply.totdemand", new EmptyValueException(
					messageUtil.getBundle("water.supply.totdemand")));
		} else if (Util.isNumeric(String.valueOf(dailyWaterSupplyModel.getTot_demand()))) {
			exceptions.put("water.supply.totdemand.numeric", new NumericException(
					messageUtil.getBundle("water.supply.totdemand.numeric")));
		}else if(String.valueOf(dailyWaterSupplyModel.getTot_demand()).length() > 18){ 
			exceptions.put("water.supply.totdemand.maxlength", new NumericException(messageUtil.getBundle("water.supply.totdemand.maxlength"))); 
		} else{
			dailyWaterSupplyModel.setTot_demand(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyWaterSupplyModel.getTot_demand()), "url")));
		}
		
		
		if(Util.isEmpty(dailyWaterSupplyModel.getQlty_supply())){
			exceptions.put("water.supply.Qlty_supply", new EmptyValueException(
					messageUtil.getBundle("water.supply.Qlty_supply")));
		} else if (Util.isNumeric(String.valueOf(dailyWaterSupplyModel.getQlty_supply()))) {
			exceptions.put("water.supply.Qlty_supply.numeric", new NumericException(
					messageUtil.getBundle("water.supply.Qlty_supply.numeric")));
		}else if(String.valueOf(dailyWaterSupplyModel.getQlty_supply()).length() > 18){ 
			exceptions.put("water.supply.Qlty_supply.maxlength", new NumericException(messageUtil.getBundle("water.supply.Qlty_supply.maxlength"))); 
		} else{
			dailyWaterSupplyModel.setQlty_supply(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyWaterSupplyModel.getQlty_supply()), "url")));
		}
		
		if(Util.isEmpty(dailyWaterSupplyModel.getPop_served())){
			exceptions.put("water.supply.Pop_served", new EmptyValueException(
					messageUtil.getBundle("water.supply.Pop_served")));
		} else if (Util.isNumber(String.valueOf(dailyWaterSupplyModel.getPop_served()))) {
			exceptions.put("water.supply.Pop_served.numeric", new NumericException(
					messageUtil.getBundle("water.supply.Pop_served.numeric")));
		}else if(String.valueOf(dailyWaterSupplyModel.getPop_served()).length() > 18){ 
			exceptions.put("water.supply.Pop_served.maxlength", new NumericException(messageUtil.getBundle("water.supply.Pop_served.maxlength"))); 
		} else{
			dailyWaterSupplyModel.setPop_served(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyWaterSupplyModel.getPop_served()), "url")));
		}
		
		if(Util.isEmpty(dailyWaterSupplyModel.getFreq_supply())){
			exceptions.put("water.supply.Freq_supply", new EmptyValueException(
					messageUtil.getBundle("water.supply.Freq_supply")));
		} else if (Util.isNumeric(String.valueOf(dailyWaterSupplyModel.getFreq_supply()))) {
			exceptions.put("water.supply.Freq_supply.numeric", new NumericException(
					messageUtil.getBundle("water.supply.Freq_supply.numeric")));
		}else if(String.valueOf(dailyWaterSupplyModel.getFreq_supply()).length() > 18){ 
			exceptions.put("water.supply.Freq_supply.maxlength", new NumericException(messageUtil.getBundle("water.supply.Freq_supply.maxlength"))); 
		} else{
			dailyWaterSupplyModel.setFreq_supply(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyWaterSupplyModel.getFreq_supply()), "url")));
		}
		
		if(dailyWaterSupplyModel.getRemarks().trim() != null){
			if(dailyWaterSupplyModel.getRemarks().trim().length() > 200){
				exceptions.put("remarks.maxlength", new NumericException(
						messageUtil.getBundle("remarks.maxlength")));
			} else{
				dailyWaterSupplyModel.setRemarks(Util.strip_html_tags(dailyWaterSupplyModel.getRemarks(), "url"));
			}
		}

		if(dailyWaterSupplyModel.getUlbId() != 0 && dailyWaterSupplyModel.getDate_of_report() != null){
			DailyWaterSupplyModel dailyWaterSupplyModel2 = null;
			try{
				dailyWaterSupplyModel2 = dailyWaterSupplyDao.fetchDailyWaterSupplyById(dailyWaterSupplyModel.getWs_id());
				if(dailyWaterSupplyModel2 != null){
					if(!(dailyWaterSupplyModel.getDate_of_report().equals(dailyWaterSupplyModel2.getDate_of_report()))){
						String tableName = "daily_water_supply";
						int result = 0;
						try {
							result = deleteDao.entryCheckWithoutApproval(tableName, dailyWaterSupplyModel.getDate_of_report(), dailyWaterSupplyModel.getUlbId());
							if(result == 1){
								exceptions.put("data.entry.exist", new NumericException(
										messageUtil.getBundle("data.entry.exist")));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}catch(Exception e){
				
			}
			
		} else {
			
			if(dailyWaterSupplyModel.getUlbId() == 0) {
				
				exceptions.put("ULBID.needed", new EmptyValueException(
						messageUtil.getBundle("ULBID.needed")));
			} else{
				dailyWaterSupplyModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(dailyWaterSupplyModel.getUlbId()), "url")));
			}
			
			if(dailyWaterSupplyModel.getDate_of_report() == null) {
				
				exceptions.put("dateofreport.needed", new EmptyValueException(
						messageUtil.getBundle("dateofreport.needed")));
			} else{
				dailyWaterSupplyModel.setDate_of_report(Util.strip_html_tags(dailyWaterSupplyModel.getDate_of_report(), "url"));
			}
		}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);
		
		if (logger.isInfoEnabled()) {
			logger.info("createDailyWaterSupplyValidation -- END");
		}
		
	}
}
