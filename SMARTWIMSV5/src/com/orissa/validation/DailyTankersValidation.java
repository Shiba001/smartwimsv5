package com.orissa.validation;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.dao.DailyTankersDao;
import com.orissa.dao.DeleteDao;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.DailyTankersModel;

@Component
public class DailyTankersValidation {

	private static Logger logger = Logger
			.getLogger(DailyTankersValidation.class);

	@Autowired
	private MessageUtil messageUtil;
	@Autowired
	private DeleteDao deleteDao;
	
	@Autowired
	private DailyTankersDao dailyTankersDao;

	public void createDailyTankersValidation(DailyTankersModel dailyTankersModel)
			throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createDailyTankersValidation -- START");
		}

		Map<String, Exception> exceptions = new HashMap<String, Exception>();

		if (Util.isEmpty(dailyTankersModel.getNo_tank_engd_dept())) {
			exceptions.put(
					"No.of.Departmental.Tankers.Engaged",
					new EmptyValueException(messageUtil
							.getBundle("No.of.Departmental.Tankers.Engaged")));
		} else if (Util.isNumber(String.valueOf(dailyTankersModel
				.getNo_tank_engd_dept()))) {
			exceptions
					.put("No.of.Departmental.Tankers.Engaged.numeric",
							new NumericException(
									messageUtil
											.getBundle("No.of.Departmental.Tankers.Engaged.numeric")));
		}else if(String.valueOf(dailyTankersModel.getNo_tank_engd_dept()).length() > 18){ 
			exceptions.put("No.of.Departmental.Tankers.Engaged.maxlength", new NumericException(messageUtil.getBundle("No.of.Departmental.Tankers.Engaged.maxlength"))); 
		} else{
			dailyTankersModel.setNo_tank_engd_dept(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyTankersModel.getNo_tank_engd_dept()), "url")));
		}

		if (Util.isEmpty(dailyTankersModel.getNo_tank_hire())) {
			exceptions.put("No.of.Tankers.Hired", new EmptyValueException(
					messageUtil.getBundle("No.of.Tankers.Hired")));
		} else if (Util.isNumber(String.valueOf(dailyTankersModel
				.getNo_tank_hire()))) {
			exceptions.put("No.of.Tankers.Hired.numeric", new NumericException(
					messageUtil.getBundle("No.of.Tankers.Hired.numeric")));
		}else if(String.valueOf(dailyTankersModel.getNo_tank_hire()).length() > 18){ 
			exceptions.put("No.of.Tankers.Hired.maxlength", new NumericException(messageUtil.getBundle("No.of.Tankers.Hired.maxlength"))); 
		} else{
			dailyTankersModel.setNo_tank_hire(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyTankersModel.getNo_tank_hire()), "url")));
		}

		if (Util.isEmpty(dailyTankersModel.getNo_pvc_rcc_tanks_depl())) {
			exceptions.put(
					"No.of.PVC.RCC.Tanks.Deployed",
					new EmptyValueException(messageUtil
							.getBundle("No.of.PVC.RCC.Tanks.Deployed")));
		} else if (Util.isNumber(String.valueOf(dailyTankersModel
				.getNo_pvc_rcc_tanks_depl()))) {
			exceptions
					.put("No.of.PVC.RCC.Tanks.Deployed.numeric",
							new NumericException(
									messageUtil
											.getBundle("No.of.PVC.RCC.Tanks.Deployed.numeric")));
		}else if(String.valueOf(dailyTankersModel.getNo_pvc_rcc_tanks_depl()).length() > 18){ 
			exceptions.put("No.of.PVC.RCC.Tanks.Deployed.maxlength", new NumericException(messageUtil.getBundle("No.of.PVC.RCC.Tanks.Deployed.maxlength"))); 
		} else{
			dailyTankersModel.setNo_pvc_rcc_tanks_depl(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyTankersModel.getNo_pvc_rcc_tanks_depl()), "url")));
		}

		if (Util.isEmpty(dailyTankersModel.getPopu_served_tanks_pvc_rcc())) {
			exceptions
					.put("Population.Served.by.Tankers.PVC.RCC.Tanks",
							new EmptyValueException(
									messageUtil
											.getBundle("Population.Served.by.Tankers.PVC.RCC.Tanks")));
		} else if (Util.isNumber(String.valueOf(dailyTankersModel
				.getPopu_served_tanks_pvc_rcc()))) {
			exceptions
					.put("Population.Served.by.Tankers.PVC.RCC.Tanks.numeric",
							new NumericException(
									messageUtil
											.getBundle("Population.Served.by.Tankers.PVC.RCC.Tanks.numeric")));
		}else if(String.valueOf(dailyTankersModel.getPopu_served_tanks_pvc_rcc()).length() > 18){ 
			exceptions.put("Population.Served.by.Tankers.PVC.RCC.Tanks.maxlength", new NumericException(messageUtil.getBundle("Population.Served.by.Tankers.PVC.RCC.Tanks.maxlength"))); 
		} else{
			dailyTankersModel.setPopu_served_tanks_pvc_rcc(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyTankersModel.getPopu_served_tanks_pvc_rcc()), "url")));
		}

		if (Util.isEmpty(dailyTankersModel.getQty_mld())) {
			exceptions.put("Quantity.supplied", new EmptyValueException(
					messageUtil.getBundle("Quantity.supplied")));
		} else if (Util
				.isNumeric(String.valueOf(dailyTankersModel.getQty_mld()))) {
			exceptions.put("Quantity.supplied.numeric", new NumericException(
					messageUtil.getBundle("Quantity.supplied.numeric")));
		}else if(String.valueOf(dailyTankersModel.getQty_mld()).length() > 18){ 
			exceptions.put("Quantity.supplied.maxlength", new NumericException(messageUtil.getBundle("Quantity.supplied.maxlength"))); 
		} else{
			dailyTankersModel.setQty_mld(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyTankersModel.getQty_mld()), "url")));
		}
		
		if(dailyTankersModel.getRemarks().trim() != null){
			if(dailyTankersModel.getRemarks().trim().length() > 200){
				exceptions.put("remarks.maxlength", new NumericException(
						messageUtil.getBundle("remarks.maxlength")));
			} else{
				dailyTankersModel.setRemarks(Util.strip_html_tags(dailyTankersModel.getRemarks(), "url"));
			}
		}
		if(dailyTankersModel.getUlbId() != 0 && dailyTankersModel.getDate_of_report() != null){
			DailyTankersModel dailyTankersModel2 = null;
			try{
				dailyTankersModel2 = dailyTankersDao.fetchDailyTankersById(dailyTankersModel.getTank_id());
				if(dailyTankersModel2 != null){
					if(!(dailyTankersModel.getDate_of_report().equals(dailyTankersModel2.getDate_of_report()))){
						String tableName = "daily_tankers";
						int result = 0;
						try {
							result = deleteDao.entryCheckWithoutApproval(tableName, dailyTankersModel.getDate_of_report(), dailyTankersModel.getUlbId());
							if(result == 1){
								exceptions.put("data.entry.exist", new NumericException(
										messageUtil.getBundle("data.entry.exist")));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}				
			}catch(Exception e){
				
			}			
		}else {
			
			if(dailyTankersModel.getUlbId() == 0) {
				
				exceptions.put("ULBID.needed", new EmptyValueException(
						messageUtil.getBundle("ULBID.needed")));
			}else{
				dailyTankersModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(dailyTankersModel.getUlbId()), "url")));
			}
			
			if(dailyTankersModel.getDate_of_report() == null) {
				
				exceptions.put("dateofreport.needed", new EmptyValueException(
						messageUtil.getBundle("dateofreport.needed")));
			}else{
				dailyTankersModel.setDate_of_report(Util.strip_html_tags(dailyTankersModel.getDate_of_report(), "url"));
			}
		}

		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		if (logger.isInfoEnabled()) {
			logger.info("createDailyTankersValidation -- END");
		}

	}
}
