package com.orissa.validation;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.PhysicalInfraStandpostsModel;
@Component
public class PhysicalInfraStandpostValidation {
	
	private static Logger logger = Logger
			.getLogger(PhysicalInfraStandpostValidation.class);
	
	@Autowired
	private MessageUtil messageUtil;
	
	public void createPhysicalInfraStandpostValidation(PhysicalInfraStandpostsModel physicalInfraStandpostsModel) throws FormExceptions {
		
		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraStandpostValidation -- START");
		}
		
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if(Util.isEmpty(physicalInfraStandpostsModel.getNo_exst_stanposts())){
			exceptions.put("number.of.existing.transport", new EmptyValueException(
					messageUtil.getBundle("number.of.existing.transport")));
		} else if (Util.isNumber(String.valueOf(physicalInfraStandpostsModel.getNo_exst_stanposts()))) {
			exceptions.put("number.of.existing.transport.numeric", new NumericException(
					messageUtil.getBundle("number.of.existing.transport.numeric")));
		} else if(String.valueOf(physicalInfraStandpostsModel.getNo_exst_stanposts()).length() > 18){
			exceptions.put("number.of.existing.transport.maxlength", new EmptyValueException(
					messageUtil.getBundle("number.of.existing.transport.maxlength")));
		} else{
			physicalInfraStandpostsModel.setNo_exst_stanposts(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraStandpostsModel.getNo_exst_stanposts()), "url")));
		}
		
		if(Util.isEmpty(physicalInfraStandpostsModel.getNo_standposts_work_cond())){
			exceptions.put("number.of.standposts.working.condition", new EmptyValueException(
					messageUtil.getBundle("number.of.standposts.working.condition")));
		} else if (Util.isNumber(String.valueOf(physicalInfraStandpostsModel.getNo_standposts_work_cond()))) {
			exceptions.put("number.of.standposts.working.condition.numeric", new NumericException(
					messageUtil.getBundle("number.of.standposts.working.condition.numeric")));
		} else if(String.valueOf(physicalInfraStandpostsModel.getNo_standposts_work_cond()).length() > 18){
			exceptions.put("number.of.standposts.working.condition.maxlength", new EmptyValueException(
					messageUtil.getBundle("number.of.standposts.working.condition.maxlength")));
		} else{
			physicalInfraStandpostsModel.setNo_exst_stanposts(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraStandpostsModel.getNo_standposts_work_cond()), "url")));
		}
			
		if(physicalInfraStandpostsModel.getUlbId() == 0) {
			
			exceptions.put("ULBID.needed", new EmptyValueException(
					messageUtil.getBundle("ULBID.needed")));
		}else{
			physicalInfraStandpostsModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(physicalInfraStandpostsModel.getUlbId()), "url")));
		}
		
		if(Util.isEmpty(physicalInfraStandpostsModel.getDate_of_report())) {
			
			exceptions.put("dateofreport.needed", new EmptyValueException(
					messageUtil.getBundle("dateofreport.needed")));
		}else{
			physicalInfraStandpostsModel.setDate_of_report(Util.strip_html_tags(physicalInfraStandpostsModel.getDate_of_report(), "url"));
		}
		
		
		if(Util.isEmpty(physicalInfraStandpostsModel.getDueDate())){
			exceptions.put("duedate.needed", new EmptyValueException(
					messageUtil.getBundle("duedate.needed")));
		} else
			try {
				if(!(Util.checkDueDate(physicalInfraStandpostsModel.getDueDate()))){
					exceptions.put("duedate.greater", new EmptyValueException(
							messageUtil.getBundle("duedate.greater")));
				} else{
					physicalInfraStandpostsModel.setDueDate(Util.strip_html_tags(physicalInfraStandpostsModel.getDueDate(), "url"));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				exceptions.put("strip.empty", new EmptyValueException(
						messageUtil.getBundle("strip.empty")));
			}
		
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);
		
		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraStandpostValidation -- END");
		}
		
	}

}
