package com.orissa.validation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.PhysicalInfraPumpsAndMotorsModel;

@Component
public class PhysicalInfraPumpMotorValidation {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraPumpMotorValidation.class);

	@Autowired
	private MessageUtil messageUtil;

	public void createPhysicalInfraPumpsAndMotorsValidation(
			PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel) throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraPumpsAndMotorsValidation -- START");
		}

		Map<String, Exception> exceptions = new HashMap<String, Exception>();

		if (physicalInfraPumpsAndMotorsModel.getList_loc_of_pum_motor()
				.size() != physicalInfraPumpsAndMotorsModel
				.getList_cap_of_motor().size()) {
			
			exceptions.put("Power.(HP/KW)", new EmptyValueException(
					messageUtil.getBundle("Power.(HP/KW)")));
			
		} else if (physicalInfraPumpsAndMotorsModel
				.getList_cap_of_motor().size() != physicalInfraPumpsAndMotorsModel
				.getList_head_dia().size()) {

			exceptions.put("Head.in.(meter)", new EmptyValueException(
					messageUtil.getBundle("Head.in.(meter)")));
			
		} 
		else if (physicalInfraPumpsAndMotorsModel
				.getList_head_dia().size() != physicalInfraPumpsAndMotorsModel
				.getList_discharge().size()) {
			
			exceptions.put("Discharge.in(lps)", new EmptyValueException(
					messageUtil.getBundle("Discharge.in(lps)")));
		}
		//
		else if (physicalInfraPumpsAndMotorsModel
				.getList_discharge().size() != physicalInfraPumpsAndMotorsModel
				.getList_type_pump().size()) {

			exceptions.put("Type.of.Pump.&.Motor", new EmptyValueException(
					messageUtil.getBundle("Type.of.Pump.&.Motor")));
			
		} 		
		else if (physicalInfraPumpsAndMotorsModel
				.getList_type_pump().size() != physicalInfraPumpsAndMotorsModel
				.getList_year_inst().size()) {

			exceptions.put("Year.of.Installation", new EmptyValueException(
					messageUtil.getBundle("Year.of.Installation")));
			
		} 
		else {
			List<String> list_loc_of_pum_motor = new ArrayList<String>();
			List<String> list_cap_of_motor = new ArrayList<String>();
			List<String> list_head_dia = new ArrayList<String>();
			List<String> list_discharge = new ArrayList<String>();
			List<String> list_type_pump = new ArrayList<String>();
			List<String> list_year_inst = new ArrayList<String>();
			boolean flag = true;
			for (int i=0; i< physicalInfraPumpsAndMotorsModel.getList_loc_of_pum_motor().size(); i++) {
				
				if (Util.isEmpty(physicalInfraPumpsAndMotorsModel.getList_loc_of_pum_motor().get(i))) {
					flag = false;
					exceptions.put(
							"Location.of.the.Pump/Motor",
							new EmptyValueException(messageUtil
									.getBundle("Location.of.the.Pump/Motor")));
				} else if(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_loc_of_pum_motor().get(i)).length() > 100){
					flag = false;
					exceptions.put("Location.of.the.Pump/Motor.maxlength",
							new EmptyValueException(messageUtil.getBundle("Location.of.the.Pump/Motor.maxlength")));
					
				}else if(!Util.isSpecialChar(physicalInfraPumpsAndMotorsModel.getList_loc_of_pum_motor().get(i)) && flag){
					flag = false;
					exceptions.put("Location.of.the.Pump/Motor.specialChar",
							new EmptyValueException(messageUtil.getBundle("Location.of.the.Pump/Motor.specialChar")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_loc_of_pum_motor().get(i)), "url").trim();
					if(str.equals("")){
						exceptions.put(
								"Location.of.the.Pump/Motor",
								new EmptyValueException(messageUtil
										.getBundle("Location.of.the.Pump/Motor")));
					}
					list_loc_of_pum_motor.add(str);
				} 
				
				if (Util.isEmpty(physicalInfraPumpsAndMotorsModel.getList_cap_of_motor().get(i))) {
					flag = false;
					exceptions.put(
							"Power.(HP/KW)",
							new EmptyValueException(messageUtil
									.getBundle("Power.(HP/KW)")));
				}else if(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_cap_of_motor().get(i)).length() > 50){
					flag = false;
					exceptions.put("Power.(HP/KW).maxlength",
							new EmptyValueException(messageUtil.getBundle("Power.(HP/KW).maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraPumpsAndMotorsModel.getList_cap_of_motor().get(i)) && flag){
					flag = false;
					exceptions.put("Power.(HP/KW).specialChar",
							new EmptyValueException(messageUtil.getBundle("Power.(HP/KW).specialChar")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_cap_of_motor().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Power.(HP/KW)",
								new EmptyValueException(messageUtil
										.getBundle("Power.(HP/KW)")));
					}
					list_cap_of_motor.add(str);
				} 
				
				if (Util.isEmpty(physicalInfraPumpsAndMotorsModel.getList_head_dia().get(i))) {
					flag = false;
					exceptions.put(
							"Head.in.(meter)",
							new EmptyValueException(messageUtil
									.getBundle("Head.in.(meter)")));
				}
				else if(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_head_dia().get(i)).length() > 50){
					flag = false;
					exceptions.put("Head.in.(meter).maxlength",
							new EmptyValueException(messageUtil.getBundle("Head.in.(meter).maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraPumpsAndMotorsModel.getList_head_dia().get(i)) && flag){
					flag = false;
					exceptions.put("Head.in.(meter).specialChar",
							new EmptyValueException(messageUtil.getBundle("Head.in.(meter).specialChar")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_head_dia().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Head.in.(meter)",
								new EmptyValueException(messageUtil
										.getBundle("Head.in.(meter)")));
					}
					list_head_dia.add(str);
				} 
				
				if (Util.isEmpty(physicalInfraPumpsAndMotorsModel.getList_discharge().get(i))) {
					flag = false;
					exceptions.put(
							"Discharge.in(lps)",
							new EmptyValueException(messageUtil
									.getBundle("Discharge.in(lps)")));
				}else if(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_discharge().get(i)).length() > 50){
					flag = false;
					exceptions.put("Discharge.in(lps).maxlength",
							new EmptyValueException(messageUtil.getBundle("Discharge.in(lps).maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraPumpsAndMotorsModel.getList_discharge().get(i)) && flag){
					flag = false;
					exceptions.put("Discharge.in(lps).specialChar",
							new EmptyValueException(messageUtil.getBundle("Discharge.in(lps).specialChar")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_discharge().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Discharge.in(lps)",
								new EmptyValueException(messageUtil
										.getBundle("Discharge.in(lps)")));
					}
					list_discharge.add(str);
				} 
				
				if (Util.isEmpty(physicalInfraPumpsAndMotorsModel.getList_type_pump().get(i))) {
					flag = false;
					exceptions.put(
							"Type.of.Pump.&.Motor",
							new EmptyValueException(messageUtil
									.getBundle("Type.of.Pump.&.Motor")));
				}else if(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_type_pump().get(i)).length() > 50){
					flag = false;
					exceptions.put("Type.of.Pump.&.Motor.maxlength",
							new EmptyValueException(messageUtil.getBundle("Type.of.Pump.&.Motor.maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraPumpsAndMotorsModel.getList_type_pump().get(i)) && flag){
					flag = false;
					exceptions.put("Type.of.Pump.&.Motor.specialChar",
							new EmptyValueException(messageUtil.getBundle("Type.of.Pump.&.Motor.specialChar")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_type_pump().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Type.of.Pump.&.Motor",
								new EmptyValueException(messageUtil
										.getBundle("Type.of.Pump.&.Motor")));
					}
					list_type_pump.add(str);
				} 
				
				if (Util.isEmpty(physicalInfraPumpsAndMotorsModel.getList_year_inst().get(i))) {
					flag = false;
					exceptions.put(
							"Year.of.Installation",
							new EmptyValueException(messageUtil
									.getBundle("Year.of.Installation")));
				}else if(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_year_inst().get(i)).length() > 50){
					flag = false;
					exceptions.put("Year.of.Installation.maxlength",
							new EmptyValueException(messageUtil.getBundle("Year.of.Installation.maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraPumpsAndMotorsModel.getList_year_inst().get(i)) && flag){
					flag = false;
					exceptions.put("Year.of.Installation.specialChar",
							new EmptyValueException(messageUtil.getBundle("Year.of.Installation.specialChar")));
				}else if(Util.isNotValidYear(physicalInfraPumpsAndMotorsModel.getList_year_inst().get(i)) && flag){
					flag = false;
					exceptions.put("Year.of.Installation.validYear",
							new EmptyValueException(messageUtil.getBundle("Year.of.Installation.validYear")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraPumpsAndMotorsModel.getList_year_inst().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Year.of.Installation",
								new EmptyValueException(messageUtil
										.getBundle("Year.of.Installation")));
					}
					list_year_inst.add(str);
				}
			}
			
			physicalInfraPumpsAndMotorsModel.setList_loc_of_pum_motor(list_loc_of_pum_motor);
			physicalInfraPumpsAndMotorsModel.setList_cap_of_motor(list_cap_of_motor);
			physicalInfraPumpsAndMotorsModel.setList_head_dia(list_head_dia);
			physicalInfraPumpsAndMotorsModel.setList_discharge(list_discharge);
			physicalInfraPumpsAndMotorsModel.setList_type_pump(list_type_pump);
			physicalInfraPumpsAndMotorsModel.setList_year_inst(list_year_inst);
			
			if(physicalInfraPumpsAndMotorsModel
					.getList_loc_of_pum_motor().size() == 0){
				exceptions.put(
						"Location.of.the.Pump/Motor",
						new EmptyValueException(messageUtil
								.getBundle("Location.of.the.Pump/Motor")));
			}
		}
		
		if(physicalInfraPumpsAndMotorsModel.getUlbId() == 0) {
			
			exceptions.put("ULBID.needed", new EmptyValueException(
					messageUtil.getBundle("ULBID.needed")));
		}else{
			physicalInfraPumpsAndMotorsModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(physicalInfraPumpsAndMotorsModel.getUlbId()), "url")));
		}
		
		if(physicalInfraPumpsAndMotorsModel.getDate_of_report() == null) {
			
			exceptions.put("dateofreport.needed", new EmptyValueException(
					messageUtil.getBundle("dateofreport.needed")));
		}else{
			String str = Util.strip_html_tags(physicalInfraPumpsAndMotorsModel.getDate_of_report(), "url");
			if(str.equals("")){
				exceptions.put(
						"dateofreport.needed",
						new EmptyValueException(messageUtil
								.getBundle("dateofreport.needed")));
			}
			physicalInfraPumpsAndMotorsModel.setDate_of_report(str);
		}
		
		if(physicalInfraPumpsAndMotorsModel.getDueDate() == null){
			exceptions.put("duedate.needed", new EmptyValueException(
					messageUtil.getBundle("duedate.needed")));
		} else
			try {
				if(!(Util.checkDueDate(physicalInfraPumpsAndMotorsModel.getDueDate()))){
					exceptions.put("duedate.greater", new EmptyValueException(
							messageUtil.getBundle("duedate.greater")));
				} else{
					physicalInfraPumpsAndMotorsModel.setDueDate(Util.strip_html_tags(physicalInfraPumpsAndMotorsModel.getDueDate(), "url"));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				exceptions.put("strip.empty", new EmptyValueException(
						messageUtil.getBundle("strip.empty")));
			}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraPumpsAndMotorsValidation -- END");
		}

	}
}
