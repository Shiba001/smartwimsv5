package com.orissa.validation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.PhysicalInfraProductionWellsModel;

@Component
public class PhysicalInfraProductionWellsValidation {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraProductionWellsValidation.class);

	@Autowired
	private MessageUtil messageUtil;

	public void createPhysicalInfraProductionWellsValidation(
			PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel) throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraProductionWellsValidation -- START");
		}

		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if (physicalInfraProductionWellsModel.getList_loc_of_prod_well()
				.size() != physicalInfraProductionWellsModel
				.getList_diam_prod_well().size()) {
			
			exceptions.put("Diameter.of.the.Production.Well", new EmptyValueException(
					messageUtil.getBundle("Diameter.of.the.Production.Well")));
			
		} else if (physicalInfraProductionWellsModel
				.getList_diam_prod_well().size() != physicalInfraProductionWellsModel
				.getList_discharge().size()) {

			exceptions.put("Discharge.(in.lpm)", new EmptyValueException(
					messageUtil.getBundle("Discharge.(in.lpm)")));
			
		} 
		else if (physicalInfraProductionWellsModel
				.getList_discharge().size() != physicalInfraProductionWellsModel
				.getList_depth().size()) {
			
			exceptions.put("Depth.(in.metre)", new EmptyValueException(
					messageUtil.getBundle("Depth.(in.metre)")));
		}
		else {
			List<String> list_loc_of_prod_well = new ArrayList<String>();
			List<String> list_diam_prod_well = new ArrayList<String>();
			List<String> list_discharge = new ArrayList<String>();
			List<String> list_depth = new ArrayList<String>();
			boolean flag = true;
			for (int i=0; i< physicalInfraProductionWellsModel.getList_loc_of_prod_well().size() && flag; i++) {
				
				if (Util.isEmpty(physicalInfraProductionWellsModel.getList_loc_of_prod_well().get(i)) && flag) {
					flag = false;
					exceptions.put(
							"Location.of.the.Production.Well",
							new EmptyValueException(messageUtil
									.getBundle("Location.of.the.Production.Well")));
				} else if(String.valueOf(physicalInfraProductionWellsModel.getList_loc_of_prod_well().get(i)).length() > 100 && flag){
					flag = false;
					exceptions.put("Location.of.the.Production.Well.maxlength",
							new EmptyValueException(messageUtil.getBundle("Location.of.the.Production.Well.maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraProductionWellsModel.getList_loc_of_prod_well().get(i)) && flag){
					flag = false;
					exceptions.put("Location.of.the.Production.Well.specialChar",
							new EmptyValueException(messageUtil.getBundle("Location.of.the.Production.Well.specialChar")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraProductionWellsModel.getList_loc_of_prod_well().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Location.of.the.Production.Well",
								new EmptyValueException(messageUtil
										.getBundle("Location.of.the.Production.Well")));
					}
					list_loc_of_prod_well.add(str);
				} 
				
				if (Util.isEmpty(physicalInfraProductionWellsModel.getList_diam_prod_well().get(i)) && flag) {
					flag = false;
					exceptions.put(
							"Diameter.of.the.Production.Well",
							new EmptyValueException(messageUtil
									.getBundle("Diameter.of.the.Production.Well")));
				}else if(String.valueOf(physicalInfraProductionWellsModel.getList_diam_prod_well().get(i)).length() > 50 && flag){
					flag = false;
					exceptions.put("Diameter.of.the.Production.Well.maxlength",
							new EmptyValueException(messageUtil.getBundle("Diameter.of.the.Production.Well.maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraProductionWellsModel.getList_diam_prod_well().get(i)) && flag){
					flag = false;
					exceptions.put("Diameter.of.the.Production.Well.specialChar",
							new EmptyValueException(messageUtil.getBundle("Diameter.of.the.Production.Well.specialChar")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraProductionWellsModel.getList_diam_prod_well().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Diameter.of.the.Production.Well",
								new EmptyValueException(messageUtil
										.getBundle("Diameter.of.the.Production.Well")));
					}
					list_diam_prod_well.add(str);
				} 
				
				if (Util.isEmpty(physicalInfraProductionWellsModel.getList_discharge().get(i)) && flag) {
					flag = false;
					exceptions.put(
							"Discharge.(in.lpm)",
							new EmptyValueException(messageUtil
									.getBundle("Discharge.(in.lpm)")));
				}else if(String.valueOf(physicalInfraProductionWellsModel.getList_discharge().get(i)).length() > 50 && flag){
					flag = false;
					exceptions.put("Discharge.(in.lpm).maxlength",
							new EmptyValueException(messageUtil.getBundle("Discharge.(in.lpm).maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraProductionWellsModel.getList_discharge().get(i)) && flag){
					flag = false;
					exceptions.put("Discharge.(in.lpm).specialChar",
							new EmptyValueException(messageUtil.getBundle("Discharge.(in.lpm).specialChar")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraProductionWellsModel.getList_discharge().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Discharge.(in.lpm)",
								new EmptyValueException(messageUtil
										.getBundle("Discharge.(in.lpm)")));
					}
					list_discharge.add(str);
				} 
				if (Util.isEmpty(physicalInfraProductionWellsModel.getList_depth().get(i)) && flag) {
					flag = false;
					exceptions.put(
							"Depth.(in.metre)",
							new EmptyValueException(messageUtil
									.getBundle("Depth.(in.metre)")));
				}else if(String.valueOf(physicalInfraProductionWellsModel.getList_depth().get(i)).length() > 50 && flag){
					flag = false;
					exceptions.put("Depth.(in.metre).maxlength",
							new EmptyValueException(messageUtil.getBundle("Depth.(in.metre).maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraProductionWellsModel.getList_depth().get(i)) && flag){
					flag = false;
					exceptions.put("Depth.(in.metre).specialChar",
							new EmptyValueException(messageUtil.getBundle("Depth.(in.metre).specialChar")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraProductionWellsModel.getList_depth().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Depth.(in.metre)",
								new EmptyValueException(messageUtil
										.getBundle("Depth.(in.metre)")));
					}
					list_depth.add(str);
				}
				
			}	
			
			physicalInfraProductionWellsModel.setList_loc_of_prod_well(list_loc_of_prod_well);
			physicalInfraProductionWellsModel.setList_diam_prod_well(list_diam_prod_well);
			physicalInfraProductionWellsModel.setList_discharge(list_discharge);
			physicalInfraProductionWellsModel.setList_depth(list_depth);
			
			if(physicalInfraProductionWellsModel
					.getList_loc_of_prod_well().size() == 0){
				exceptions.put(
						"Location.of.the.Production.Well",
						new EmptyValueException(messageUtil
								.getBundle("Location.of.the.Production.Well")));
			}
		}
		
		
		if(physicalInfraProductionWellsModel.getUlbId() == 0) {
			
			exceptions.put("ULBID.needed", new EmptyValueException(
					messageUtil.getBundle("ULBID.needed")));
		}else{
			physicalInfraProductionWellsModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(physicalInfraProductionWellsModel.getUlbId()), "url")));
		}
		
		if(physicalInfraProductionWellsModel.getDate_of_report() == null) {
			
			exceptions.put("dateofreport.needed", new EmptyValueException(
					messageUtil.getBundle("dateofreport.needed")));
		}else{
			String str = Util.strip_html_tags(physicalInfraProductionWellsModel.getDate_of_report(), "url");
			if(str.equals("")){
				exceptions.put(
						"dateofreport.needed",
						new EmptyValueException(messageUtil
								.getBundle("dateofreport.needed")));
			}
			physicalInfraProductionWellsModel.setDate_of_report(str);
		}
		
		if(physicalInfraProductionWellsModel.getDueDate() == null){
			exceptions.put("duedate.needed", new EmptyValueException(
					messageUtil.getBundle("duedate.needed")));
		} else
			try {
				if(!(Util.checkDueDate(physicalInfraProductionWellsModel.getDueDate()))){
					exceptions.put("duedate.greater", new EmptyValueException(
							messageUtil.getBundle("duedate.greater")));
				} else{
					physicalInfraProductionWellsModel.setDueDate(Util.strip_html_tags(physicalInfraProductionWellsModel.getDueDate(), "url"));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				exceptions.put("strip.empty", new EmptyValueException(
						messageUtil.getBundle("strip.empty")));
			}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		
		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraProductionWellsValidation -- END");
		}

	}
}
