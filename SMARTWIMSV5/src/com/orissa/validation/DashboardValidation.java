package com.orissa.validation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ListingModel;

@Component
public class DashboardValidation {

	private static Logger logger = Logger
			.getLogger(DashboardValidation.class);
	
	@Autowired
	private MessageUtil messageUtil;
	
	public void dashboardValidation(ListingModel listingModel, String amrutTypeId, String selectedYear, String month) throws FormExceptions {
		
		if (logger.isInfoEnabled()) {
			logger.info("dashboardValidation -- START");
		}
		
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if(!(Util.isEmpty(amrutTypeId))){
			
			Util.strip_html_tags(amrutTypeId, "url");
			
			if(!(Util.isNumeric(amrutTypeId))){
				
				exceptions.put("amrutTypeId.numeric", new EmptyValueException(
						messageUtil.getBundle("amrutTypeId.numeric")));
			}
		}
		
		if(!(Util.isEmpty(selectedYear))){
			
			Util.strip_html_tags(selectedYear, "url");
			
			if(!(Util.isNumeric(selectedYear))){
				
				exceptions.put("selectedYear.numeric", new EmptyValueException(
						messageUtil.getBundle("selectedYear.numeric")));
			}
		}
		
		if(!(Util.isEmpty(month))){
			
			Util.strip_html_tags(month, "url");
			
			if(!(Util.isNumeric(month))){
				
				exceptions.put("month.numeric", new EmptyValueException(
						messageUtil.getBundle("month.numeric")));
			}
		}
		
		if(listingModel != null) {
			
			if(!(Util.isEmpty(listingModel.getDistrictId()))){
				
				listingModel.setDistrictId(Integer.parseInt(Util.strip_html_tags(String.valueOf(listingModel.getDistrictId()), "url")));
				
				if(!(Util.isNumeric(String.valueOf(listingModel.getDistrictId())))){
					
					exceptions.put("districtID.numeric", new EmptyValueException(
							messageUtil.getBundle("districtID.numeric")));
				}
			}
			
			if(!(Util.isEmpty(listingModel.getDivisionId()))){
				
				listingModel.setDivisionId(Integer.parseInt(Util.strip_html_tags(String.valueOf(listingModel.getDivisionId()), "url")));
				
				if(!(Util.isNumeric(String.valueOf(listingModel.getDivisionId())))) {
					
					exceptions.put("divisionID.numeric", new EmptyValueException(
							messageUtil.getBundle("divisionID.numeric")));
				}
			}

			if(!(Util.isEmpty(listingModel.getUlbId()))){
				
				listingModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(listingModel.getUlbId()), "url")));
				
				if(!(Util.isNumeric(String.valueOf(listingModel.getUlbId())))) {
					
					exceptions.put("UlbID.numeric", new EmptyValueException(
							messageUtil.getBundle("UlbID.numeric")));
				}
			}
			
		}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);
		
		if (logger.isInfoEnabled()) {
			logger.info("dashboardValidation -- END");
		}
		
	}
	
	public void reportValidation(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws FormExceptions {
		
		if (logger.isInfoEnabled()) {
			logger.info("reportValidation -- START");
		}
		
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if(!(Util.isEmpty(amrutTypeId))){
			
			Util.strip_html_tags(amrutTypeId, "url");
			
			if(!(Util.isNumeric(amrutTypeId))){
				
				exceptions.put("amrutTypeId.numeric", new EmptyValueException(
						messageUtil.getBundle("amrutTypeId.numeric")));
			}
		}
		
		if(Util.isEmpty(startDate)){
			
				exceptions.put("startDate.null", new EmptyValueException(
						messageUtil.getBundle("startDate.null")));
		}
		
		if(!(Util.isEmpty(endDate))){
			
			if(Util.isEmpty(startDate)){
				
				exceptions.put("startDate.null", new EmptyValueException(
						messageUtil.getBundle("startDate.null")));
			}
		}
		
		if((!(Util.isEmpty(startDate))) && (!(Util.isEmpty(endDate)))) {
			
			try {
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
					Date startDate1 = formatter.parse(startDate); // Set start date
					Date endDate1   = formatter.parse(endDate); // Set end date
					long duration  = endDate1.getTime() - startDate1.getTime();
					if(duration < 0) {
						
						exceptions.put("endDate.smaller", new EmptyValueException(
								messageUtil.getBundle("endDate.smaller")));
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
		
		
		if(listingModel != null) {
			
			if(!(Util.isEmpty(listingModel.getDistrictId()))){
				
				listingModel.setDistrictId(Integer.parseInt(Util.strip_html_tags(String.valueOf(listingModel.getDistrictId()), "url")));
				
				if(!(Util.isNumeric(String.valueOf(listingModel.getDistrictId())))){
					
					exceptions.put("districtID.numeric", new EmptyValueException(
							messageUtil.getBundle("districtID.numeric")));
				}
			}
			
			if(!(Util.isEmpty(listingModel.getDivisionId()))){
				
				listingModel.setDivisionId(Integer.parseInt(Util.strip_html_tags(String.valueOf(listingModel.getDivisionId()), "url")));
				
				if(!(Util.isNumeric(String.valueOf(listingModel.getDivisionId())))) {
					
					exceptions.put("divisionID.numeric", new EmptyValueException(
							messageUtil.getBundle("divisionID.numeric")));
				}
			}

			if(!(Util.isEmpty(listingModel.getUlbId()))){
				
				listingModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(listingModel.getUlbId()), "url")));
				
				if(!(Util.isNumeric(String.valueOf(listingModel.getUlbId())))) {
					
					exceptions.put("UlbID.numeric", new EmptyValueException(
							messageUtil.getBundle("UlbID.numeric")));
				}
			}
			
		}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);
		
		if (logger.isInfoEnabled()) {
			logger.info("reportValidation -- END");
		}
		
	}
}
