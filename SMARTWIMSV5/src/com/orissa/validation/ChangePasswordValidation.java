package com.orissa.validation;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.ChangePasswordModel;

@Component
public class ChangePasswordValidation {
	
	private static Logger logger = Logger
			.getLogger(ChangePasswordValidation.class);
	
	@Autowired
	private MessageUtil messageUtil;
	
	public void changePassword(ChangePasswordModel changePasswordModel) throws FormExceptions {
		
		if (logger.isInfoEnabled()) {
			logger.info("createDailyComplaintsValidation -- START");
		}
		
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if(Util.isEmpty(changePasswordModel.getOldPassword())){
			exceptions.put("oldPassword.null", new EmptyValueException(
					messageUtil.getBundle("oldPassword.null")));
		} else{
			
			try{
				changePasswordModel.setOldPassword(Util.strip_html_tags(Util.decPass(changePasswordModel.getOldPassword()), "url"));
			} catch(Exception e){
				exceptions.put("oldpassword.format", new EmptyValueException(
						messageUtil.getBundle("oldpassword.format")));
			}
			
		}
		
		if(Util.isEmpty(changePasswordModel.getNewPassword())){
			exceptions.put("newPassword.null", new EmptyValueException(
					messageUtil.getBundle("newPassword.null")));
		} else{
			
			try{
				changePasswordModel.setNewPassword(Util.strip_html_tags(Util.decPass(changePasswordModel.getNewPassword()), "url"));
			} catch(Exception e){
				exceptions.put("newpassword.format", new EmptyValueException(
						messageUtil.getBundle("newpassword.format")));
			}
			/*if(!(Util.validatePassword(changePasswordModel.getNewPassword()))) {
				
				exceptions.put("newpassword.format", new EmptyValueException(
						messageUtil.getBundle("newpassword.format")));
			}*/
		}
		
		if(Util.isEmpty(changePasswordModel.getConfirmNewPassword())){
			exceptions.put("confirm.newPassword.null", new EmptyValueException(
					messageUtil.getBundle("confirm.newPassword.null")));
		} else{
			
			try{
				changePasswordModel.setConfirmNewPassword(Util.strip_html_tags(Util.decPass(changePasswordModel.getConfirmNewPassword()), "url"));
			} catch(Exception e){
				exceptions.put("confirm.password.format", new EmptyValueException(
						messageUtil.getBundle("confirm.password.format")));
			}
			/*if(!(Util.validatePassword(changePasswordModel.getConfirmNewPassword()))) {
				
				exceptions.put("confirm.password.format", new EmptyValueException(
						messageUtil.getBundle("confirm.password.format")));
			}*/
		}
		
		if(!(Util.isEmpty(changePasswordModel.getOldPassword())) && (!(Util.isEmpty(changePasswordModel.getNewPassword())))){
			
			if(!(changePasswordModel.getNewPassword().equals(changePasswordModel.getConfirmNewPassword()))){
				exceptions.put("password.mismatch", new EmptyValueException(
						messageUtil.getBundle("password.mismatch")));
			}
		}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);
		
		if (logger.isInfoEnabled()) {
			logger.info("createDailyComplaintsValidation -- END");
		}
		
	}
}
