package com.orissa.validation;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.PhysicalInfraGeneretorModel;
import com.orissa.model.PhysicalInfraLengthOfPipeModel;

@Component
public class PhysicalInfraPipelineValidation {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraPipelineValidation.class);

	@Autowired
	private MessageUtil messageUtil;

	public void createPhysicalInfraPipelineValidation(
			PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel)
			throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraPipelineValidation -- START");
		}

		Map<String, Exception> exceptions = new HashMap<String, Exception>();

		if (Util.isEmpty(physicalInfraLengthOfPipeModel.getRising_main())) {
			exceptions.put("Length.of.Rising.Main.(in.Kms)", new EmptyValueException(
					messageUtil.getBundle("Length.of.Rising.Main.(in.Kms)")));
		} else if (Util.isNumeric(String.valueOf(physicalInfraLengthOfPipeModel.getRising_main()))) {
			exceptions.put(
					"Length.of.Rising.Main.(in.Kms).numeric",
					new NumericException(messageUtil
							.getBundle("Length.of.Rising.Main.(in.Kms).numeric")));
		}else if(String.valueOf(physicalInfraLengthOfPipeModel.getRising_main()).length() > 18){ 
			exceptions.put("Length.of.Rising.Main.(in.Kms).maxlength", new NumericException(messageUtil.getBundle("Length.of.Rising.Main.(in.Kms).maxlength"))); 
		} else{
			physicalInfraLengthOfPipeModel.setRising_main(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraLengthOfPipeModel.getRising_main()), "url")));
		}

		if (Util.isEmpty(physicalInfraLengthOfPipeModel.getDistr_netw())) {
			exceptions
					.put("Length.of.Distribution.Network.(in.Kms)",
							new EmptyValueException(
									messageUtil
											.getBundle("Length.of.Distribution.Network.(in.Kms)")));
		} else if (Util.isNumeric(String.valueOf(physicalInfraLengthOfPipeModel.getDistr_netw()))) {
			exceptions
					.put("Length.of.Distribution.Network.(in.Kms).numeric",
							new NumericException(
									messageUtil
											.getBundle("Length.of.Distribution.Network.(in.Kms).numeric")));
		}
		else if(String.valueOf(physicalInfraLengthOfPipeModel.getRising_main()).length() > 18){ 
			exceptions.put("Length.of.Distribution.Network.(in.Kms).maxlength", new NumericException(messageUtil.getBundle("Length.of.Distribution.Network.(in.Kms).maxlength"))); 
		} else{
			physicalInfraLengthOfPipeModel.setDistr_netw(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraLengthOfPipeModel.getDistr_netw()), "url")));
		}

		if(physicalInfraLengthOfPipeModel.getUlbId() == 0) {
			
			exceptions.put("ULBID.needed", new EmptyValueException(
					messageUtil.getBundle("ULBID.needed")));
		}else{
			physicalInfraLengthOfPipeModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(physicalInfraLengthOfPipeModel.getUlbId()), "url")));
		}
		
		if(physicalInfraLengthOfPipeModel.getDate_of_report() == null) {
			
			exceptions.put("dateofreport.needed", new EmptyValueException(
					messageUtil.getBundle("dateofreport.needed")));
		}else{
			physicalInfraLengthOfPipeModel.setDate_of_report(Util.strip_html_tags(physicalInfraLengthOfPipeModel.getDate_of_report(), "url"));
		}
		
		if(physicalInfraLengthOfPipeModel.getDueDate() == null){
			exceptions.put("duedate.needed", new EmptyValueException(
					messageUtil.getBundle("duedate.needed")));
		} else
			try {
				if(!(Util.checkDueDate(physicalInfraLengthOfPipeModel.getDueDate()))){
					exceptions.put("duedate.greater", new EmptyValueException(
							messageUtil.getBundle("duedate.greater")));
				} else{
					physicalInfraLengthOfPipeModel.setDueDate(Util.strip_html_tags(physicalInfraLengthOfPipeModel.getDueDate(), "url"));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				exceptions.put("strip.empty", new EmptyValueException(
						messageUtil.getBundle("strip.empty")));
			}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraPipelineValidation -- END");
		}

	}
}
