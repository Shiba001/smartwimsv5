package com.orissa.validation;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.dao.DailyWaterChargesDao;
import com.orissa.dao.DeleteDao;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.DailyWaterChargesModel;

@Component
public class DailyWaterChargesValidation {

	private static Logger logger = Logger
			.getLogger(DailyWaterChargesValidation.class);

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private DeleteDao deleteDao;
	
	@Autowired
	private DailyWaterChargesDao dailyWaterChargesDao;

	public void createDailyWaterChargesValidation(
			DailyWaterChargesModel dailyWaterChargesModel)
			throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createDailyWaterChargesValidation -- START");
		}

		Map<String, Exception> exceptions = new HashMap<String, Exception>();

		if (Util.isEmpty(dailyWaterChargesModel.getTot_cur_coll())) {
			exceptions.put(
					"Total.Water.Charges.Collection",
					new EmptyValueException(messageUtil
							.getBundle("Total.Water.Charges.Collection")));
		} else if (Util.isNumeric(String.valueOf(dailyWaterChargesModel
				.getTot_cur_coll()))) {
			exceptions
					.put("Total.Water.Charges.Collection.numeric",
							new NumericException(
									messageUtil
											.getBundle("Total.Water.Charges.Collection.numeric")));
		} else if(String.valueOf(dailyWaterChargesModel.getTot_cur_coll()).length() > 18){
			exceptions.put("Total.Water.Charges.Collection.maxlength", new NumericException(
					messageUtil.getBundle("Total.Water.Charges.Collection.maxlength")));
		} else{
			dailyWaterChargesModel.setTot_cur_coll(Double.parseDouble(Util.strip_html_tags(String.valueOf(dailyWaterChargesModel.getTot_cur_coll()), "url")));
		}
		
		if(dailyWaterChargesModel.getRemarks() != null){
			if(dailyWaterChargesModel.getRemarks().length() > 200){
				exceptions.put("remarks.maxlength", new NumericException(
						messageUtil.getBundle("remarks.maxlength")));
			} else{
				dailyWaterChargesModel.setRemarks(Util.strip_html_tags(dailyWaterChargesModel.getRemarks(), "url"));
			}
		}

		if(dailyWaterChargesModel.getUlbId() != 0 && dailyWaterChargesModel.getDate_of_report() != null){
			DailyWaterChargesModel dailyWaterChargesModel2 = null;
			try{
				dailyWaterChargesModel2 = dailyWaterChargesDao.fetchDailyWaterChargeById(dailyWaterChargesModel.getW_tax_id());
				if(dailyWaterChargesModel2 != null){
					if(!(dailyWaterChargesModel.getDate_of_report().equals(dailyWaterChargesModel2.getDate_of_report()))){
						String tableName = " daily_water_charges_tax";
						int result = 0;
						try {
							result = deleteDao.entryCheckWithoutApproval(tableName, dailyWaterChargesModel.getDate_of_report(), dailyWaterChargesModel.getUlbId());
							if(result == 1){
								exceptions.put("data.entry.exist", new NumericException(
										messageUtil.getBundle("data.entry.exist")));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}				
			} catch(Exception e){
				
			}
			
		} else {
			
			if(dailyWaterChargesModel.getUlbId() == 0) {
				
				exceptions.put("ULBID.needed", new EmptyValueException(
						messageUtil.getBundle("ULBID.needed")));
			} else{
				dailyWaterChargesModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(dailyWaterChargesModel.getUlbId()), "url")));
			}
			
			if(dailyWaterChargesModel.getDate_of_report() == null) {
				
				exceptions.put("dateofreport.needed", new EmptyValueException(
						messageUtil.getBundle("dateofreport.needed")));
			} else{
				dailyWaterChargesModel.setDate_of_report(Util.strip_html_tags(dailyWaterChargesModel.getDate_of_report(), "url"));
			}
		}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		if (logger.isInfoEnabled()) {
			logger.info("createDailyWaterChargesValidation -- END");
		}

	}
}
