package com.orissa.validation;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.LoginUsersModel;

@Component
public class LoginValidation {

	private static Logger logger = Logger
			.getLogger(LoginValidation.class);
	
	@Autowired
	private MessageUtil messageUtil;
	
	public void loginCheck(LoginUsersModel loginUsersModel, HttpServletRequest request) throws FormExceptions {
		
		if (logger.isInfoEnabled()) {
			logger.info("createDailyComplaintsValidation -- START");
		}
		
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if(Util.isEmpty(loginUsersModel.getUser_Name())){
			exceptions.put("username.null", new EmptyValueException(
					messageUtil.getBundle("username.null")));
		} else{
			loginUsersModel.setUser_Name(Util.strip_html_tags(loginUsersModel.getUser_Name(), "url"));
		}
		
		if(Util.isEmpty(loginUsersModel.getUser_Password())){
			exceptions.put("password.null", new EmptyValueException(
					messageUtil.getBundle("password.null")));
		} else{
			
			try{
				loginUsersModel.setUser_Password(Util.strip_html_tags(Util.decPass(loginUsersModel.getUser_Password()), "url"));
			} catch(Exception e){
				exceptions.put("password.format", new EmptyValueException(
						messageUtil.getBundle("password.format")));
			}
			
		}
		
		if(Util.isEmpty(loginUsersModel.getCode())){
			
			exceptions.put("captcha.null", new EmptyValueException(
					messageUtil.getBundle("captcha.null")));
		} else {
			
			HttpSession httpSession = request.getSession(false);

			String captcha = (String) httpSession.getAttribute("captcha");
			String code = loginUsersModel.getCode();
			
			if (captcha.equals(code)) {
				System.out.println("Correct Captcha");
				httpSession.removeAttribute("captcha");
			} else {
				System.out.println("Incorrect Captcha");
				httpSession.removeAttribute("captcha");
				exceptions.put("captcha.failed", new EmptyValueException(
						messageUtil.getBundle("captcha.failed")));
			}
		}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);
		
		if (logger.isInfoEnabled()) {
			logger.info("createDailyComplaintsValidation -- END");
		}
		
	}
}
