package com.orissa.validation;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.PhysicalInfraTubewellsModel;

@Component
public class PhysicalInfraTubewellValidation {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraTubewellValidation .class);
	
	@Autowired
	private MessageUtil messageUtil;
public void createPhysicalInfraTubewelValidation(PhysicalInfraTubewellsModel physicalInfraTubewellsModel) throws FormExceptions {
		
		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraTubewelValidation -- START");
		}
		
		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if(Util.isEmpty(physicalInfraTubewellsModel.getTot_no_tw())){
			exceptions.put("total.no.tubewells", new EmptyValueException(
					messageUtil.getBundle("total.no.tubewells")));
		} else if (Util.isNumber(String.valueOf(physicalInfraTubewellsModel.getTot_no_tw()))){
			exceptions.put("total.no.tubewells.numeric", new NumericException(
					messageUtil.getBundle("total.no.tubewells.numeric")));
		}else if(String.valueOf(physicalInfraTubewellsModel.getTot_no_tw()).length() > 18){ 
			exceptions.put("total.no.tubewells.maxlength", new NumericException(messageUtil.getBundle("total.no.tubewells.maxlength"))); 
		} else{
			physicalInfraTubewellsModel.setTot_no_tw(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraTubewellsModel.getTot_no_tw()), "url")));
		}
		
		if(Util.isEmpty(physicalInfraTubewellsModel.getTot_no_tw_wc())){
			exceptions.put("total.no.tubewells.in.working.condition", new EmptyValueException(
					messageUtil.getBundle("total.no.tubewells.in.working.condition")));
		} else if (Util.isNumber(String.valueOf(physicalInfraTubewellsModel.getTot_no_tw_wc()))) {
			exceptions.put("total.no.tubewells.in.working.condition.numeric", new NumericException(
					messageUtil.getBundle("total.no.tubewells.in.working.condition.numeric")));
		}else if(String.valueOf(physicalInfraTubewellsModel.getTot_no_tw_wc()).length() > 18){ 
			exceptions.put("total.no.tubewells.in.working.condition.maxlength", new NumericException(messageUtil.getBundle("total.no.tubewells.in.working.condition.maxlength"))); 
		} else{
			physicalInfraTubewellsModel.setTot_no_tw_wc(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraTubewellsModel.getTot_no_tw_wc()), "url")));
		}
		
		if(physicalInfraTubewellsModel.getUlbId() == 0) {
			
			exceptions.put("ULBID.needed", new EmptyValueException(
					messageUtil.getBundle("ULBID.needed")));
		}else{
			physicalInfraTubewellsModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(physicalInfraTubewellsModel.getUlbId()), "url")));
		}
		
		if(physicalInfraTubewellsModel.getDate_of_report() == null) {
			
			exceptions.put("dateofreport.needed", new EmptyValueException(
					messageUtil.getBundle("dateofreport.needed")));
		}else{
			physicalInfraTubewellsModel.setDate_of_report(Util.strip_html_tags(physicalInfraTubewellsModel.getDate_of_report(), "url"));
		}
		
		if(physicalInfraTubewellsModel.getDueDate() == null){
			exceptions.put("duedate.needed", new EmptyValueException(
					messageUtil.getBundle("duedate.needed")));
		} else
			try {
				if(!(Util.checkDueDate(physicalInfraTubewellsModel.getDueDate()))){
					exceptions.put("duedate.greater", new EmptyValueException(
							messageUtil.getBundle("duedate.greater")));
				} else{
					physicalInfraTubewellsModel.setDueDate(Util.strip_html_tags(physicalInfraTubewellsModel.getDueDate(), "url"));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				exceptions.put("strip.empty", new EmptyValueException(
						messageUtil.getBundle("strip.empty")));
			}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);
		
		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraTubewelValidation -- END");
		}
		
	}
}
