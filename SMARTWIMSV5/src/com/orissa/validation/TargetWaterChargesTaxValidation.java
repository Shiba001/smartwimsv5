package com.orissa.validation;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.TargetModel;

@Component
public class TargetWaterChargesTaxValidation {

	private static Logger logger = Logger
			.getLogger(TargetWaterChargesTaxValidation.class);

	@Autowired
	private MessageUtil messageUtil;

	public void createTargetWaterChargesTaxValidation(TargetModel targetModel)
			throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createTargetWaterChargesTaxValidation -- START");
		}

		Map<String, Exception> exceptions = new HashMap<String, Exception>();
		
		if (Util.isEmpty(targetModel.getFin_year())){
			
			exceptions.put("yearselection.needed", new EmptyValueException(
					messageUtil.getBundle("yearselection.needed")));
			
		} else if(targetModel.getFin_year().equals("2017-2018") || targetModel.getFin_year().equals("2016-2017")) {
				
			if ("Monthly".equals(targetModel.getTrgt_mode())) {
				if (Util.isEmpty(targetModel.getM_trgt_1())) {
					exceptions.put("target.month.april", new EmptyValueException(
							messageUtil.getBundle("target.month.april")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_1()))) {
					exceptions.put(
							"target.month.april.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.april.numeric")));
				} else if(String.valueOf(targetModel.getM_trgt_1()).length() > 18){
					exceptions.put("target.month.april.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.april.maxlength")));
				}else{
					targetModel.setM_trgt_1(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_1()), "url")));
				}
				
				
				if (Util.isEmpty(targetModel.getM_trgt_2())) {
					exceptions.put("target.month.may", new EmptyValueException(
							messageUtil.getBundle("target.month.may")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_2()))) {
					exceptions.put(
							"target.month.may.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.may.numeric")));
				} else if(String.valueOf(targetModel.getM_trgt_2()).length() > 18){
					exceptions.put("target.month.may.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.may.maxlength")));
				} else{
					targetModel.setM_trgt_2(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_2()), "url")));
				}
	
				if (Util.isEmpty(targetModel.getM_trgt_3())) {
					exceptions.put("target.month.june", new EmptyValueException(
							messageUtil.getBundle("target.month.june")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_3()))) {
					exceptions.put(
							"target.month.june.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.june.numeric")));
				}  else if(String.valueOf(targetModel.getM_trgt_3()).length() > 18){
					exceptions.put("target.month.june.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.june.maxlength")));
				} else{
					targetModel.setM_trgt_3(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_3()), "url")));
				}
	
				if (Util.isEmpty(targetModel.getM_trgt_4())) {
					exceptions.put("target.month.july", new EmptyValueException(
							messageUtil.getBundle("target.month.july")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_4()))) {
					exceptions.put(
							"target.month.july.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.july.numeric")));
				} else if(String.valueOf(targetModel.getM_trgt_4()).length() > 18){
					exceptions.put("target.month.july.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.july.maxlength")));
				} else{
					targetModel.setM_trgt_4(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_4()), "url")));
				}
				
				
				if (Util.isEmpty(targetModel.getM_trgt_5())) {
					exceptions.put("target.month.august", new EmptyValueException(
							messageUtil.getBundle("target.month.august")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_5()))) {
					exceptions.put(
							"target.month.august.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.august.numeric")));
				} else if(String.valueOf(targetModel.getM_trgt_5()).length() > 18){
					exceptions.put("target.month.august.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.august.maxlength")));
				} else{
					targetModel.setM_trgt_5(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_5()), "url")));
				}
	
				if (Util.isEmpty(targetModel.getM_trgt_6())) {
					exceptions.put(
							"target.month.september",
							new EmptyValueException(messageUtil
									.getBundle("target.month.september")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_6()))) {
					exceptions.put(
							"target.month.september.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.september.numeric")));
				}  else if(String.valueOf(targetModel.getM_trgt_6()).length() > 18){
					exceptions.put("target.month.september.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.september.maxlength")));
				} else{
					targetModel.setM_trgt_6(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_6()), "url")));
				}
	
				if (Util.isEmpty(targetModel.getM_trgt_7())) {
					exceptions.put("target.month.october", new EmptyValueException(
							messageUtil.getBundle("target.month.october")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_7()))) {
					exceptions.put(
							"target.month.october.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.october.numeric")));
				}  else if(String.valueOf(targetModel.getM_trgt_7()).length() > 18){
					exceptions.put("target.month.october.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.october.maxlength")));
				} else{
					targetModel.setM_trgt_7(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_7()), "url")));
				}
	
				if (Util.isEmpty(targetModel.getM_trgt_8())) {
					exceptions.put(
							"target.month.november",
							new EmptyValueException(messageUtil
									.getBundle("target.month.november")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_8()))) {
					exceptions.put(
							"target.month.november.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.november.numeric")));
				}  else if(String.valueOf(targetModel.getM_trgt_8()).length() > 18){
					exceptions.put("target.month.november.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.november.maxlength")));
				} else{
					targetModel.setM_trgt_8(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_8()), "url")));
				}
	
				if (Util.isEmpty(targetModel.getM_trgt_9())) {
					exceptions.put(
							"target.month.december",
							new EmptyValueException(messageUtil
									.getBundle("target.month.december")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_9()))) {
					exceptions.put(
							"target.month.december.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.december.numeric")));
				}  else if(String.valueOf(targetModel.getM_trgt_9()).length() > 18){
					exceptions.put("target.month.december.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.december.maxlength")));
				} else{
					targetModel.setM_trgt_9(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_9()), "url")));
				}
	
				if (Util.isEmpty(targetModel.getM_trgt_10())) {
					exceptions.put("target.month.january", new EmptyValueException(
							messageUtil.getBundle("target.month.january")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_10()))) {
					exceptions.put(
							"target.month.january.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.january.numeric")));
				}  else if(String.valueOf(targetModel.getM_trgt_10()).length() > 18){
					exceptions.put("target.month.january.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.january.maxlength")));
				} else{
					targetModel.setM_trgt_10(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_10()), "url")));
				}
	 
				if (Util.isEmpty(targetModel.getM_trgt_11())) {
					exceptions.put(
							"target.month.february",
							new EmptyValueException(messageUtil
									.getBundle("target.month.february")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_11()))) {
					exceptions.put(
							"target.month.february.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.february.numeric")));
				}  else if(String.valueOf(targetModel.getM_trgt_11()).length() > 18){
					exceptions.put("target.month.february.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.february.maxlength")));
				} else{
					targetModel.setM_trgt_11(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_11()), "url")));
				}
	
				if (Util.isEmpty(targetModel.getM_trgt_12())) {
					exceptions.put("target.month.march", new EmptyValueException(
							messageUtil.getBundle("target.month.march")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getM_trgt_12()))) {
					exceptions.put(
							"target.month.march.numeric",
							new NumericException(messageUtil
									.getBundle("target.month.march.numeric")));
				} else if(String.valueOf(targetModel.getM_trgt_12()).length() > 18){
					exceptions.put("target.month.march.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.month.march.maxlength")));
				} else{
					targetModel.setM_trgt_12(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getM_trgt_12()), "url")));
				}
				
			}
			if("Yearly".equals(targetModel.getTrgt_mode())){
				if (Util.isEmpty(targetModel.getY_trgt())) {
					exceptions.put("target.yearly.annual", new EmptyValueException(
							messageUtil.getBundle("target.yearly.annual")));
				} else if (Util
						.isNumeric(String.valueOf(targetModel.getY_trgt()))) {
					exceptions.put(
							"target.yearly.annual.numeric",
							new NumericException(messageUtil
									.getBundle("target.yearly.annual.numeric")));
				}  else if(String.valueOf(targetModel.getY_trgt()).length() > 18){
					exceptions.put("target.yearly.annual.maxlength", new EmptyValueException(
							messageUtil.getBundle("target.yearly.annual.maxlength")));
				} else{
					targetModel.setY_trgt(Double.parseDouble(Util.strip_html_tags(String.valueOf(targetModel.getY_trgt()), "url")));
				}
			}
			
			if(targetModel.getUlbId() == 0) {
				
				exceptions.put("ULBID.needed", new EmptyValueException(
						messageUtil.getBundle("ULBID.needed")));
			}else{
				targetModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(targetModel.getUlbId()), "url")));
			}
			
			if(targetModel.getDate_of_report() == null) {
				
				exceptions.put("dateofreport.needed", new EmptyValueException(
						messageUtil.getBundle("dateofreport.needed")));
			}else{
				targetModel.setDate_of_report(Util.strip_html_tags(targetModel.getDate_of_report(), "url"));
			}
			
		} else {
			
			exceptions.put("yearselection.valid", new EmptyValueException(
					messageUtil.getBundle("yearselection.valid")));
		}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		if (logger.isInfoEnabled()) {
			logger.info("createTargetWaterChargesTaxValidation -- END");
		}

	}
}
