package com.orissa.validation;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.exception.NumericException;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.PhysicalInfraGeneretorModel;

@Component
public class PhysicalInfraGeneratorValidation {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraGeneratorValidation.class);

	@Autowired
	private MessageUtil messageUtil;

	public void createPhysicalInfraGeneratorValidation(
			PhysicalInfraGeneretorModel physicalInfraGeneretorModel) throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraGeneratorValidation -- START");
		}

		Map<String, Exception> exceptions = new HashMap<String, Exception>();

		if (Util.isEmpty(physicalInfraGeneretorModel.getTot_no_gen())) {
			exceptions.put(
					"Total.No..of.Generators",
					new EmptyValueException(messageUtil
							.getBundle("Total.No..of.Generators")));
		} else if (Util.isNumber(String.valueOf(physicalInfraGeneretorModel.getTot_no_gen()))) {
			exceptions
					.put("Total.No..of.Generators.numeric",
							new NumericException(
									messageUtil
											.getBundle("Total.No..of.Generators.numeric")));
		} else if(String.valueOf(physicalInfraGeneretorModel.getTot_no_gen()).length() > 18){
			exceptions.put("Total.No..of.Generators.maxlength",
					new EmptyValueException(messageUtil.getBundle("Total.No..of.Generators.maxlength")));
		} else{
			physicalInfraGeneretorModel.setTot_no_gen(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraGeneretorModel.getTot_no_gen()), "url")));
		}

		if (Util.isEmpty(physicalInfraGeneretorModel.getTot_no_gen_wc())) {
			exceptions.put(
					"Total.No..of.Generators.in.Working.Condition",
					new EmptyValueException(messageUtil
							.getBundle("Total.No..of.Generators.in.Working.Condition")));
		} else if (Util.isNumber(String.valueOf(physicalInfraGeneretorModel.getTot_no_gen_wc()))) {
			exceptions
					.put("Total.No..of.Generators.in.Working.Condition.numeric",
							new NumericException(
									messageUtil
											.getBundle("Total.No..of.Generators.in.Working.Condition.numeric")));
		} else if(String.valueOf(physicalInfraGeneretorModel.getTot_no_gen_wc()).length() > 18){
			exceptions.put("Total.No..of.Generators.in.Working.Condition.maxlength",
					new EmptyValueException(messageUtil.getBundle("Total.No..of.Generators.in.Working.Condition.maxlength")));
		} else{
			physicalInfraGeneretorModel.setTot_no_gen_wc(Double.parseDouble(Util.strip_html_tags(String.valueOf(physicalInfraGeneretorModel.getTot_no_gen_wc()), "url")));
		}

		if(physicalInfraGeneretorModel.getUlbId() == 0) {
			
			exceptions.put("ULBID.needed", new EmptyValueException(
					messageUtil.getBundle("ULBID.needed")));
		}else{
			physicalInfraGeneretorModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(physicalInfraGeneretorModel.getUlbId()), "url")));
		}
		
		if(physicalInfraGeneretorModel.getDate_of_report() == null) {
			
			exceptions.put("dateofreport.needed", new EmptyValueException(
					messageUtil.getBundle("dateofreport.needed")));
		}else{
			physicalInfraGeneretorModel.setDate_of_report(Util.strip_html_tags(physicalInfraGeneretorModel.getDate_of_report(), "url"));
		}
		
		if(physicalInfraGeneretorModel.getDueDate() == null){
			exceptions.put("duedate.needed", new EmptyValueException(
					messageUtil.getBundle("duedate.needed")));
		} else
			try {
				if(!(Util.checkDueDate(physicalInfraGeneretorModel.getDueDate()))){
					exceptions.put("duedate.greater", new EmptyValueException(
							messageUtil.getBundle("duedate.greater")));
				} else{
					physicalInfraGeneretorModel.setDueDate(Util.strip_html_tags(physicalInfraGeneretorModel.getDueDate(), "url"));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				exceptions.put("strip.empty", new EmptyValueException(
						messageUtil.getBundle("strip.empty")));
			}
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraGeneratorValidation -- END");
		}

	}
}
