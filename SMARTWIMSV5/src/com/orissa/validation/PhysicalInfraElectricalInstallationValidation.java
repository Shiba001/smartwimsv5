package com.orissa.validation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orissa.common.MessageUtil;
import com.orissa.common.Util;
import com.orissa.exception.EmptyValueException;
import com.orissa.exception.FormExceptions;
import com.orissa.model.PhysicalInfraElectricalInstallationsModel;

@Component
public class PhysicalInfraElectricalInstallationValidation {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraElectricalInstallationValidation.class);

	@Autowired
	private MessageUtil messageUtil;	

	public void createPhysicalInfraElectricalInstallationValidationNew(
			PhysicalInfraElectricalInstallationsModel physicalInfraElectricalInstallationsModel)
			throws FormExceptions {

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraElectricalInstallationValidationNew -- START");
		}
		Map<String, Exception> exceptions = new HashMap<String, Exception>();

		if (physicalInfraElectricalInstallationsModel.getList_loc_of_inst()
				.size() != physicalInfraElectricalInstallationsModel
				.getList_cap_of_trnf().size()) {
			
			exceptions.put("Capacity.Of.Transformer(in.KVA)", new EmptyValueException(
					messageUtil.getBundle("Capacity.Of.Transformer(in.KVA)")));
			
		} else if (physicalInfraElectricalInstallationsModel
				.getList_cap_of_trnf().size() != physicalInfraElectricalInstallationsModel
				.getList_year_inst().size()) {

			exceptions.put("Year.Of.Installation", new EmptyValueException(
					messageUtil.getBundle("Year.Of.Installation")));
			
		} 
		else if (physicalInfraElectricalInstallationsModel
				.getList_year_inst().size() != physicalInfraElectricalInstallationsModel
				.getList_hp_lt().size()) {
			
			exceptions.put("Type.Of.Transformer(HL/LT)", new EmptyValueException(
					messageUtil.getBundle("Type.Of.Transformer(HL/LT)")));
		}
		else {
			boolean flag = true;
			List<String> list_loc_of_inst = new ArrayList<String>();
			List<String> list_cap_of_trnf = new ArrayList<String>();
			List<String> list_year_inst = new ArrayList<String>();
			List<String> list_hp_lt = new ArrayList<String>();
			for (int i=0; i< physicalInfraElectricalInstallationsModel.getList_loc_of_inst().size() && flag; i++) {
				
				if (Util.isEmpty(physicalInfraElectricalInstallationsModel.getList_loc_of_inst().get(i)) && flag) {
					flag = false;
					exceptions.put(
							"Location.Of.Installation",
							new EmptyValueException(messageUtil
									.getBundle("Location.Of.Installation")));
				} else if(String.valueOf(physicalInfraElectricalInstallationsModel.getList_loc_of_inst().get(i)).length() > 100 && flag){
					flag = false;
					exceptions.put("Location.Of.Installation.maxlength", new EmptyValueException(
							messageUtil.getBundle("Location.Of.Installation.maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraElectricalInstallationsModel.getList_loc_of_inst().get(i)) && flag){
					flag = false;
					exceptions.put("Location.Of.Installation.specialChar",
							new EmptyValueException(messageUtil.getBundle("Location.Of.Installation.specialChar")));
				} else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraElectricalInstallationsModel.getList_loc_of_inst().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Location.Of.Installation",
								new EmptyValueException(messageUtil
										.getBundle("Location.Of.Installation")));
					}
					list_loc_of_inst.add(str);
				}
				
				if (Util.isEmpty(physicalInfraElectricalInstallationsModel.getList_cap_of_trnf().get(i)) && flag) {
					flag = false;
					exceptions.put(
							"Capacity.Of.Transformer(in.KVA)",
							new EmptyValueException(messageUtil
									.getBundle("Capacity.Of.Transformer(in.KVA)")));
				} else if(String.valueOf(physicalInfraElectricalInstallationsModel.getList_cap_of_trnf().get(i)).length() > 50 && flag){
					flag = false;
					exceptions.put("Capacity.Of.Transformer(in.KVA).maxlength", new EmptyValueException(
							messageUtil.getBundle("Capacity.Of.Transformer(in.KVA).maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraElectricalInstallationsModel.getList_cap_of_trnf().get(i)) && flag){
					flag = false;
					exceptions.put("Capacity.Of.Transformer(in.KVA).specialChar",
							new EmptyValueException(messageUtil.getBundle("Capacity.Of.Transformer(in.KVA).specialChar")));
				} else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraElectricalInstallationsModel.getList_cap_of_trnf().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Capacity.Of.Transformer(in.KVA)",
								new EmptyValueException(messageUtil
										.getBundle("Capacity.Of.Transformer(in.KVA)")));
					}
					list_cap_of_trnf.add(str);
				} 
				
				if (Util.isEmpty(physicalInfraElectricalInstallationsModel.getList_year_inst().get(i)) && flag) {
					flag = false;
					exceptions.put(
							"Year.Of.Installation",
							new EmptyValueException(messageUtil
									.getBundle("Year.Of.Installation")));
				}else if(String.valueOf(physicalInfraElectricalInstallationsModel.getList_year_inst().get(i)).length() > 50 && flag){
					flag = false;
					exceptions.put("Year.Of.Installation.maxlength", new EmptyValueException(
							messageUtil.getBundle("Year.Of.Installation.maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraElectricalInstallationsModel.getList_year_inst().get(i)) && flag){
					flag = false;
					exceptions.put("Year.Of.Installation.specialChar",
							new EmptyValueException(messageUtil.getBundle("Year.Of.Installation.specialChar")));
				}else if(Util.isNotValidYear(physicalInfraElectricalInstallationsModel.getList_year_inst().get(i)) && flag){
					flag = false;
					exceptions.put("Year.of.Installation.validYear",
							new EmptyValueException(messageUtil.getBundle("Year.of.Installation.validYear")));
				} else {
					String str = Util.strip_html_tags(String.valueOf(physicalInfraElectricalInstallationsModel.getList_year_inst().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Year.Of.Installation",
								new EmptyValueException(messageUtil
										.getBundle("Year.Of.Installation")));
					}
					list_year_inst.add(str);
				} 
				
				if (Util.isEmpty(physicalInfraElectricalInstallationsModel.getList_hp_lt().get(i)) && flag) {
					flag = false;
					exceptions.put(
							"Type.Of.Transformer(HL/LT)",
							new EmptyValueException(messageUtil
									.getBundle("Type.Of.Transformer(HL/LT)")));
				} else if(String.valueOf(physicalInfraElectricalInstallationsModel.getList_hp_lt().get(i)).length() > 50 && flag){
					flag = false;
					exceptions.put("Type.Of.Transformer(HL/LT).maxlength", new EmptyValueException(
							messageUtil.getBundle("Type.Of.Transformer(HL/LT).maxlength")));
				}else if(!Util.isSpecialChar(physicalInfraElectricalInstallationsModel.getList_hp_lt().get(i)) && flag){
					flag = false;
					exceptions.put("Type.Of.Transformer(HL/LT).specialChar",
							new EmptyValueException(messageUtil.getBundle("Type.Of.Transformer(HL/LT).specialChar")));
				}else{
					String str = Util.strip_html_tags(String.valueOf(physicalInfraElectricalInstallationsModel.getList_hp_lt().get(i)), "url");
					if(str.equals("")){
						exceptions.put(
								"Type.Of.Transformer(HL/LT)",
								new EmptyValueException(messageUtil
										.getBundle("Type.Of.Transformer(HL/LT)")));
					}
					list_hp_lt.add(str);
				}
			}
			
			physicalInfraElectricalInstallationsModel.setList_loc_of_inst(list_loc_of_inst);
			physicalInfraElectricalInstallationsModel.setList_cap_of_trnf(list_cap_of_trnf);
			physicalInfraElectricalInstallationsModel.setList_year_inst(list_year_inst);
			physicalInfraElectricalInstallationsModel.setList_hp_lt(list_hp_lt);
			
			if(physicalInfraElectricalInstallationsModel
					.getList_loc_of_inst().size() == 0){
				exceptions.put(
						"Location.Of.Installation",
						new EmptyValueException(messageUtil
								.getBundle("Location.Of.Installation")));
			}
		}
		
		if(physicalInfraElectricalInstallationsModel.getUlbId() == 0) {
			
			exceptions.put("ULBID.needed", new EmptyValueException(
					messageUtil.getBundle("ULBID.needed")));
		}else{
			physicalInfraElectricalInstallationsModel.setUlbId(Integer.parseInt(Util.strip_html_tags(String.valueOf(physicalInfraElectricalInstallationsModel.getUlbId()), "url")));
		}
		
		if(physicalInfraElectricalInstallationsModel.getDate_of_report() == null) {
			
			exceptions.put("dateofreport.needed", new EmptyValueException(
					messageUtil.getBundle("dateofreport.needed")));
		}else{
			String str = Util.strip_html_tags(physicalInfraElectricalInstallationsModel.getDate_of_report(), "url");
			if(str.equals("")){
				exceptions.put(
						"dateofreport.needed",
						new EmptyValueException(messageUtil
								.getBundle("dateofreport.needed")));
			}
			physicalInfraElectricalInstallationsModel.setDate_of_report(str);
		}
		
		if(physicalInfraElectricalInstallationsModel.getDueDate() == null){
			exceptions.put("duedate.needed", new EmptyValueException(
					messageUtil.getBundle("duedate.needed")));
		} else
			try {
				if(!(Util.checkDueDate(physicalInfraElectricalInstallationsModel.getDueDate()))){
					exceptions.put("duedate.greater", new EmptyValueException(
							messageUtil.getBundle("duedate.greater")));
				} else{
					physicalInfraElectricalInstallationsModel.setDueDate(Util.strip_html_tags(physicalInfraElectricalInstallationsModel.getDueDate(), "url"));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				exceptions.put("strip.empty", new EmptyValueException(
						messageUtil.getBundle("strip.empty")));
			}
		
		if (exceptions.size() > 0)
			throw new FormExceptions(exceptions);

		if (logger.isInfoEnabled()) {
			logger.info("createPhysicalInfraElectricalInstallationValidationNew -- END");
		}
	}
}
