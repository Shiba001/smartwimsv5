package com.orissa.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class NoCacheFilter implements Filter {

	public void init(FilterConfig config) {
		this.filterConfig = config;
	}

	private FilterConfig filterConfig;

	public FilterConfig getFilterConfig() {
		return this.filterConfig;
	}

	public void setFilterConfig(FilterConfig filterConfig) {
		this.filterConfig = filterConfig;
	}

	public void destroy() {
		this.filterConfig = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) {
		
		try {
			HttpServletRequest httpServletRequest = ((HttpServletRequest) request);
			String conn = httpServletRequest.getHeader("connection");
			
			//System.out.println("conn.equals keep-alive===>"+conn.equals("keep-alive"));
			String uri = httpServletRequest.getRequestURI();

			String pageName = uri.substring(uri.lastIndexOf("/")+1);


			
			if(!conn.equals("keep-alive") && !pageName.equals("attackersException")){
				HttpServletResponse httpResponse = (HttpServletResponse) response;
				httpResponse.sendRedirect("./attackersException");
				RequestDispatcher view = request
						.getRequestDispatcher("attackersException");
				view.forward(request, response);
				return;
			}
			
			if (response instanceof HttpServletResponse && !pageName.equals("attackersException")) {
				HttpServletResponse httpresponse = (HttpServletResponse) response;
				// Set the C ache-Control and Expires header

				// Print out the URL we're filtering
				String name = ((HttpServletRequest) request).getRequestURI();
				//System.out.println("URL Name ::: " + name);
				HttpSession session = ((HttpServletRequest) request)
						.getSession();

				if (session != null) {

					httpresponse.setHeader("Cache-Control",
							"no-cache, no-store, must-revalidate"); // HTTP 1.1
					httpresponse.setHeader("Pragma", "no-cache"); // HTTP 1.0
					httpresponse.setHeader("Cache-Control", "no-store");
					httpresponse.setDateHeader("Expires", 0);
					
					String projectName = ((HttpServletRequest) request).getContextPath();
					
					//System.out.println("projectName ---- >>>>"+projectName);
					
					if (!name.contains("resources") && !name.contains("swims-login") && !name.contains("get-token") && !name.contains("ImageCreator")) {
						if (session.getAttribute("loginUsersModel2") == null) {

							if (name.equals(projectName)
									|| name.equals(projectName+"/index")) { // For
																			// First
																			// Time
																			// User

								// System.out.println("For First Time User");
								RequestDispatcher view = request
										.getRequestDispatcher("index");
								view.forward(request, response);
								return;

							} else if (name.equals(projectName+"/login")) { // For
																			// Login

								// System.out.println("For Login");
								chain.doFilter(request, response);
								return;

							} else if ((name.equals(projectName+"/")) || (name.equals(projectName))) { // For First Time
								
								session.invalidate();
								RequestDispatcher view = request
										.getRequestDispatcher("index");
								view.forward(request, response);
								return;
								
							} else {
								// System.out.println("Session Timeout");
								session.invalidate();
								RequestDispatcher view = request
										.getRequestDispatcher("index1");
								view.forward(request, response);
								return;
							}

						} else {

							// For Logged In User
							// System.out.println("For Logged In User");
							if (name.equals(projectName+"/index")) { // For
																		// wrong
																		// inputs
								session.invalidate();
								RequestDispatcher view = request
										.getRequestDispatcher("index");
								view.forward(request, response);
								return;
							}
						}
					}

				} else {
					
					System.out.println("Here");
					
				}
			}
			chain.doFilter(request, response);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
