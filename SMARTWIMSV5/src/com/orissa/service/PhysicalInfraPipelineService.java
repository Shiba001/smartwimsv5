package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.PhysicalInfraPipelineDao;
import com.orissa.dao.ULBNameDao;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraLengthOfPipeModel;
import com.orissa.validation.PhysicalInfraPipelineValidation;

@Transactional
@Service
public class PhysicalInfraPipelineService {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraPipelineService.class);

	@Autowired
	private PhysicalInfraPipelineDao physicalInfraPipelineDao;
	@Autowired
	PhysicalInfraPipelineValidation physicalInfraPipelineValidation;
	@Autowired
	ULBNameDao ulbNameDao;

	public int addPhysicalInfraPipeline(
			PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPipeline -- START");
		}

		physicalInfraPipelineValidation
				.createPhysicalInfraPipelineValidation(physicalInfraLengthOfPipeModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		physicalInfraLengthOfPipeModel.setLoginUsersModel(loginUsersModel);

		int result = physicalInfraPipelineDao
				.addPhysicalInfraPipeline(physicalInfraLengthOfPipeModel);

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraPipeline -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraLengthOfPipeModel> fetchPhysicalInfraPipeline(HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraPipeline -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		LinkedList<PhysicalInfraLengthOfPipeModel> physicalInfraLengthOfPipeModels = physicalInfraPipelineDao
				.fetchPhysicalInfraPipeline(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraPipeline -- END");
		}

		return physicalInfraLengthOfPipeModels;

	}

	public PhysicalInfraLengthOfPipeModel fetchPhysicalInfraPipeById(
			int phyInfra_lpid) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraPipeById -- START");
		}

		PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel = physicalInfraPipelineDao
				.fetchPhysicalInfraPipeById(phyInfra_lpid);
		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraPipeById -- END");
		}

		return physicalInfraLengthOfPipeModel;

	}

	public int deletePhysicalInfraPipeById(int phyInfra_lpid) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraPipeById -- START");
		}

		int result = physicalInfraPipelineDao
				.commonDelete("physical_infra_length_of_pipe", "phyInfra_lpid",
						phyInfra_lpid);

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraPipeById -- END");
		}

		return result;

	}

	public int updatePhysicalInfraWaterPipe(
			PhysicalInfraLengthOfPipeModel physicalInfraLengthOfPipeModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterCharge -- START");
		}
		
		physicalInfraPipelineValidation
		.createPhysicalInfraPipelineValidation(physicalInfraLengthOfPipeModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		physicalInfraLengthOfPipeModel.setLoginUsersModel(loginUsersModel);

		int result = physicalInfraPipelineDao
				.updatePhysicalInfraWaterPipe(physicalInfraLengthOfPipeModel);
		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterCharge -- END");
		}

		return result;

	}
	
	public LinkedList<PhysicalInfraLengthOfPipeModel> fetchAJAXPhysicalInfraPipe(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- START");
		}

		LinkedList<PhysicalInfraLengthOfPipeModel> physicalInfraLengthOfPipeModel = physicalInfraPipelineDao
				.fetchAJAXPhysicalInfraPipe(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- END");
		}

		return physicalInfraLengthOfPipeModel;

	}
	
	
}
