package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.PhysicalInfraPumpMotorDao;
import com.orissa.dao.ULBNameDao;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraPumpsAndMotorsModel;
import com.orissa.model.ULBNameModel;

@Transactional
@Service
public class PhysicalInfraPumpMotorService {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraPumpMotorService.class);

	@Autowired
	private PhysicalInfraPumpMotorDao physicalInfraPumpMotorDao;
	
	@Autowired
	ULBNameDao ulbNameDao;

	public int addPumpAndMotor(
			PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPumpAndMotor -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		int tempResult = 0;
		int result = 0;

		if (physicalInfraPumpsAndMotorsModel.getPumpsAndMotorsModels().size() > 0) {

			for (PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel2 : physicalInfraPumpsAndMotorsModel
					.getPumpsAndMotorsModels()) {

				physicalInfraPumpsAndMotorsModel2
						.setLoginUsersModel(loginUsersModel);
				tempResult = tempResult
						+ physicalInfraPumpMotorDao
								.addPumpAndMotor(physicalInfraPumpsAndMotorsModel2);
			}

			if (tempResult == physicalInfraPumpsAndMotorsModel
					.getPumpsAndMotorsModels().size()) {

				System.out.println("All Rows Inserted Successfully");
				result = 1;
			} else {

				System.out.println("Some Rows Are Missing");
				result = 0;
			}
		}

		if (logger.isInfoEnabled()) {
			logger.info("addPumpAndMotor -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraPumpsAndMotorsModel> fetchPumpAndMotor(
			 int UlbId, String date_of_report) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPumpAndMotor -- START");
		}

		
		
		LinkedList<PhysicalInfraPumpsAndMotorsModel> physicalInfraPumpsAndMotorsModelList = physicalInfraPumpMotorDao
				.fetchPumpAndMotor(UlbId, date_of_report);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPumpAndMotor -- END");
		}

		return physicalInfraPumpsAndMotorsModelList;

	}

	public LinkedList<ULBNameModel> fetchUlbS(ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchUlbS -- START");
		}

		LinkedList<ULBNameModel> ulbNameModels = null;
		if(listingModel.getDivisionId() == null){
			ulbNameModels = ulbNameDao.fetchAllULBName();
		}
		else if(listingModel.getDistrictId() == null){
			ulbNameModels = ulbNameDao.fetchULBNameByDivisionId(listingModel.getDivisionId());
		}
		else if(listingModel.getUlbId() == null){
			ulbNameModels = ulbNameDao.fetchULBNameByDistrictId(listingModel.getDistrictId());
		}
		else{
			ulbNameModels = ulbNameDao.fetchULBNameByULBId(listingModel.getUlbId());
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchUlbS -- END");
		}

		return ulbNameModels;

	}
	
	public int updatePumpAndMotor(
			PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePumpAndMotor -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		int tempResult = 0;
		int result = 0;

		if (physicalInfraPumpsAndMotorsModel.getPumpsAndMotorsModels().size() > 0) {
			System.out.println("Test Value"+physicalInfraPumpsAndMotorsModel
							.getPumpsAndMotorsModels().get(0));
			int deleteResult = physicalInfraPumpMotorDao
					.deletePumpAndMotor(physicalInfraPumpsAndMotorsModel
							.getPumpsAndMotorsModels().get(0));

			if (deleteResult == 1) {
				System.out.println("Delete Successfull");
				for (PhysicalInfraPumpsAndMotorsModel physicalInfraPumpsAndMotorsModel2 : physicalInfraPumpsAndMotorsModel
						.getPumpsAndMotorsModels()) {

					physicalInfraPumpsAndMotorsModel2
							.setLoginUsersModel(loginUsersModel);
					tempResult = tempResult
							+ physicalInfraPumpMotorDao
									.addPumpAndMotor(physicalInfraPumpsAndMotorsModel2);
					
				}
				
			} else {
				System.out.println("Delete UnSuccessfull");
			}

			

			if (tempResult == physicalInfraPumpsAndMotorsModel
					.getPumpsAndMotorsModels().size()) {

				System.out.println("All Rows Inserted Successfully");
				result = 1;
			} else {

				System.out.println("Some Rows Are Missing");
				result = 0;
			}
		}

		if (logger.isInfoEnabled()) {
			logger.info("updatePumpAndMotor -- END");
		}

		return result;

	}

}