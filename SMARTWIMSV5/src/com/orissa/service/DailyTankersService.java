package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.DailyTankersDao;
import com.orissa.model.DailyTankersModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.validation.DailyTankersValidation;

@Transactional
@Service
public class DailyTankersService {

	private static Logger logger = Logger.getLogger(DailyTankersService.class);

	@Autowired
	private DailyTankersDao dailyTankersDao;

	@Autowired
	private DailyTankersValidation dailyTankersValidation;

	public int addDailyTankers(DailyTankersModel dailyTankersModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyTankers -- START");
		}

		dailyTankersValidation.createDailyTankersValidation(dailyTankersModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		dailyTankersModel.setLoginUsersModel(loginUsersModel);
		dailyTankersModel.setTot_no_tank_engd(dailyTankersModel
				.getNo_tank_engd_dept() + dailyTankersModel.getNo_tank_hire());

		/*if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4)
			dailyTankersModel.setIsApproved(0); // To be Changed
		else*/
			dailyTankersModel.setIsApproved(0); // To be Changed

		int result = dailyTankersDao.addDailyTankers(dailyTankersModel);

		if (logger.isInfoEnabled()) {
			logger.info("addDailyTankers -- END");
		}

		return result;

	}

	public LinkedList<DailyTankersModel> fetchDailyTankers(HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankers -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		LinkedList<DailyTankersModel> dailyTankersModels = dailyTankersDao
				.fetchDailyTankers(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankers -- END");
		}

		return dailyTankersModels;

	}

	public DailyTankersModel fetchDailyTankersById(int tank_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankersById -- START");
		}

		DailyTankersModel dailyTankersModel = dailyTankersDao
				.fetchDailyTankersById(tank_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankersById -- END");
		}

		return dailyTankersModel;

	}

	public int updateDailyTankers(DailyTankersModel dailyTankersModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyTankers -- START");
		}

		dailyTankersValidation.createDailyTankersValidation(dailyTankersModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		dailyTankersModel.setLoginUsersModel(loginUsersModel);
		dailyTankersModel.setTot_no_tank_engd(dailyTankersModel
				.getNo_tank_engd_dept() + dailyTankersModel.getNo_tank_hire());

		int result = dailyTankersDao.updateDailyTankers(dailyTankersModel);
		
		if (logger.isInfoEnabled()) {
			logger.info("updateDailyTankers -- END");
		}

		return result;

	}

	public int deleteDailyTankersPageById(int tank_id) throws Exception {
		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyTankersPageById -- START");
		}

		int result = dailyTankersDao.commonDelete("daily_tankers", "tank_id",
				tank_id);

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyTankersPageById -- END");
		}

		return result;

	}
	
	public int isApprovedUpdate(int tank_id) throws Exception {
		
		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		int result = dailyTankersDao.isApprovedUpdate("daily_tankers", "tank_id",
				tank_id);

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return result;

	}
	
public int isNotApprovedUpdate(int tank_id) throws Exception {
		
		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}

		int result = dailyTankersDao.isNotApprovedUpdate("daily_tankers", "tank_id",
				tank_id);

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return result;

	}

	public LinkedList<DailyTankersModel> fetchAJAXDailyTankers(
			ListingModel listingModel) throws Exception {
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyTankers -- START");
		}
		
		LinkedList<DailyTankersModel> dailyTankersModels = dailyTankersDao.fetchAJAXDailyTankers(listingModel);
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyTankers -- END");
		}
		
		return dailyTankersModels;
	}

	
}
