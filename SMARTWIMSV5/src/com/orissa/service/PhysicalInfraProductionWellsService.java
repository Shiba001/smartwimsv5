package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.PhysicalInfraProductionWellsDao;
import com.orissa.dao.ULBNameDao;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraProductionWellsModel;
import com.orissa.model.ULBNameModel;

@Transactional
@Service
public class PhysicalInfraProductionWellsService {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraProductionWellsService.class);

	@Autowired
	private PhysicalInfraProductionWellsDao physicalInfraProductionWellsDao;
	@Autowired
	ULBNameDao ulbNameDao;

	public int addPhysicalInfraProductionWells(
			PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addProdWells -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		int tempResult = 0;
		int result = 0;

		if (physicalInfraProductionWellsModel.getProductionWellsModels().size() > 0) {

			for (PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel2 : physicalInfraProductionWellsModel
					.getProductionWellsModels()) {

				physicalInfraProductionWellsModel2
						.setLoginUsersModel(loginUsersModel);
				tempResult = tempResult
						+ physicalInfraProductionWellsDao
								.addPhysicalInfraProductionWells(physicalInfraProductionWellsModel2);
			}

			if (tempResult == physicalInfraProductionWellsModel
					.getProductionWellsModels().size()) {

				System.out.println("All Rows Inserted Successfully");
				result = 1;
			} else {

				System.out.println("Some Rows Are Missing");
				result = 0;
			}
		}

		if (logger.isInfoEnabled()) {
			logger.info("addProdWells -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraProductionWellsModel> fetchPhysicalInfraProductionWells(
			int UlbId, String date_of_report) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraProductionWells -- START");
		}

		LinkedList<PhysicalInfraProductionWellsModel> physicalInfraProductionWellsModels = physicalInfraProductionWellsDao
				.fetchPhysicalInfraProductionWells(UlbId, date_of_report);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraProductionWells -- END");
		}

		return physicalInfraProductionWellsModels;

	}
	
	public LinkedList<ULBNameModel> fetchUlbS(ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchUlbS -- START");
		}

		LinkedList<ULBNameModel> ulbNameModels = null;
		if(listingModel.getDivisionId() == null){
			ulbNameModels = ulbNameDao.fetchAllULBName();
		}
		else if(listingModel.getDistrictId() == null){
			ulbNameModels = ulbNameDao.fetchULBNameByDivisionId(listingModel.getDivisionId());
		}
		else if(listingModel.getUlbId() == null){
			ulbNameModels = ulbNameDao.fetchULBNameByDistrictId(listingModel.getDistrictId());
		}
		else{
			ulbNameModels = ulbNameDao.fetchULBNameByULBId(listingModel.getUlbId());
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchUlbS -- END");
		}

		return ulbNameModels;

	}

	public int updatePhysicalInfraProductionWells(
			PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraProductionWells -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		int tempResult = 0;
		int result = 0;

		if (physicalInfraProductionWellsModel.getProductionWellsModels().size() > 0) {
			
			
			int deleteResult = physicalInfraProductionWellsDao
					.deletePhysicalInfraProductionWells(physicalInfraProductionWellsModel
							.getProductionWellsModels().get(0));

			if (deleteResult == 1) {
				System.out.println("Delete Successfull");
				for (PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel2 : physicalInfraProductionWellsModel
						.getProductionWellsModels()) {

					physicalInfraProductionWellsModel2
							.setLoginUsersModel(loginUsersModel);
					tempResult = tempResult
							+ physicalInfraProductionWellsDao
									.addPhysicalInfraProductionWells(physicalInfraProductionWellsModel2);
					
				}
				
			} else {
				System.out.println("Delete UnSuccessfull");
			}

			

			if (tempResult == physicalInfraProductionWellsModel
					.getProductionWellsModels().size()) {

				System.out.println("All Rows Inserted Successfully");
				result = 1;
			} else {

				System.out.println("Some Rows Are Missing");
				result = 0;
			}
		}

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraProductionWells -- END");
		}

		return result;

	}
}
