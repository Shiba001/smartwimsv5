package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.PhysicalInfraGeneratorDao;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraGeneretorModel;
import com.orissa.validation.PhysicalInfraGeneratorValidation;

@Transactional
@Service
public class PhysicalInfraGeneratorService {
	
	private static Logger logger = Logger
			.getLogger(PhysicalInfraGeneratorService.class);

	@Autowired
	private PhysicalInfraGeneratorDao physicalInfraGeneratorDao;
	
	@Autowired
	private PhysicalInfraGeneratorValidation physicalInfraGeneratorValidation;

	public int addPhysicalInfraGenerator(
			PhysicalInfraGeneretorModel physicalInfraGeneretorModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraGenerator -- START");
		}

		physicalInfraGeneratorValidation
				.createPhysicalInfraGeneratorValidation(physicalInfraGeneretorModel);
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		physicalInfraGeneretorModel.setLoginUsersModel(loginUsersModel);

		physicalInfraGeneretorModel
				.setTot_no_gen_ur(physicalInfraGeneretorModel.getTot_no_gen()
						- physicalInfraGeneretorModel.getTot_no_gen_wc());
		physicalInfraGeneretorModel.setPro_gen_wc(physicalInfraGeneretorModel
				.getTot_no_gen_wc()
				/ physicalInfraGeneretorModel.getTot_no_gen());

		int result = physicalInfraGeneratorDao
				.addPhysicalInfraGenerator(physicalInfraGeneretorModel);

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraGenerator -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraGeneretorModel> fetchPhysicalInfraGenerator(HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraGenerator -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		
		LinkedList<PhysicalInfraGeneretorModel> physicalInfraGeneretorModels = physicalInfraGeneratorDao
				.fetchPhysicalInfraGenerator(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraGenerator -- END");
		}

		return physicalInfraGeneretorModels;

	}

	public PhysicalInfraGeneretorModel fetchPhysicalInfraGenerator(
			int phyInfra_lpid) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraGenerator -- START");
		}

		PhysicalInfraGeneretorModel physicalInfraGeneretorModel = physicalInfraGeneratorDao
				.fetchPhysicalInfraGeneratorById(phyInfra_lpid);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraGenerator -- END");
		}

		return physicalInfraGeneretorModel;

	}

	public int deletePhysicalInfraGenerator(int phyInfra_gen_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraGenerator -- START");
		}

		int result = physicalInfraGeneratorDao.commonDelete(
				"physical_infra_generetor", "phyInfra_gen_id", phyInfra_gen_id);

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraGenerator -- END");
		}

		return result;

	}

	public int updatePhysicalInfraGenerator(
			PhysicalInfraGeneretorModel physicalInfraGeneretorModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraGenerator -- START");
		}
		physicalInfraGeneratorValidation
				.createPhysicalInfraGeneratorValidation(physicalInfraGeneretorModel);
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		physicalInfraGeneretorModel.setLoginUsersModel(loginUsersModel);

		physicalInfraGeneretorModel
				.setTot_no_gen_ur(physicalInfraGeneretorModel.getTot_no_gen()
						- physicalInfraGeneretorModel.getTot_no_gen_wc());
		physicalInfraGeneretorModel.setPro_gen_wc(physicalInfraGeneretorModel
				.getTot_no_gen_wc()
				/ physicalInfraGeneretorModel.getTot_no_gen());

		int result = physicalInfraGeneratorDao
				.updatePhysicalInfraGenerator(physicalInfraGeneretorModel);
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraGenerator -- END");
		}

		return result;

	}
	
	public LinkedList<PhysicalInfraGeneretorModel> fetchAJAXPhysicalInfraGen(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- START");
		}

		LinkedList<PhysicalInfraGeneretorModel> physicalInfraGeneretorModels = physicalInfraGeneratorDao
				.fetchAJAXPhysicalInfraGen(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- END");
		}

		return physicalInfraGeneretorModels;

	}

}
