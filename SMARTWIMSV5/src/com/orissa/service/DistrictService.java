package com.orissa.service;

import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.DistrictDao;
import com.orissa.model.PhDistrictModel;

@Transactional
@Service
public class DistrictService {

	private static Logger logger = Logger.getLogger(DistrictService.class);

	@Autowired
	private DistrictDao districtDao;

	public LinkedList<PhDistrictModel> fetchDistrictByDivisionId(int divisionId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDistrictByDivisionId -- START");
		}

		LinkedList<PhDistrictModel> phDistrictModels = districtDao
				.fetchDistrictByDivisionId(divisionId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDistrictByDivisionId -- END");
		}

		return phDistrictModels;

	}
	
	public LinkedList fetchUpperLevelsByULBId(int ulbId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchUpperLevelsByULBId -- START");
		}

		LinkedList upperLevels = districtDao
				.fetchUpperLevelsByULBId(ulbId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchUpperLevelsByULBId -- END");
		}

		return upperLevels;

	}
	
	
}
