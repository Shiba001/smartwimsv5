package com.orissa.service;

import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.ULBNameDao;
import com.orissa.model.ULBNameModel;

@Transactional
@Service
public class ULBNameService {

	private static Logger logger = Logger.getLogger(ULBNameService.class);

	@Autowired
	private ULBNameDao ulbNameDao;

	public LinkedList<ULBNameModel> fetchULBNameByDistrictId(int districtId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByDistrictId -- START");
		}

		LinkedList<ULBNameModel> ulbNameModels = ulbNameDao
				.fetchULBNameByDistrictId(districtId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByDistrictId -- END");
		}

		return ulbNameModels;

	}

	public LinkedList<ULBNameModel> fetchAllULBName() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAllULBName -- START");
		}

		LinkedList<ULBNameModel> ulbNameModels = ulbNameDao.fetchAllULBName();

		if (logger.isInfoEnabled()) {
			logger.info("fetchAllULBName -- END");
		}

		return ulbNameModels;

	}

	public LinkedList<ULBNameModel> fetchULBNameAmrutType(int amrutTypeId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameAmrutType -- START");
		}

		LinkedList<ULBNameModel> ulbNameModels = ulbNameDao
				.fetchULBNameAmrutType(amrutTypeId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameAmrutType -- END");
		}

		return ulbNameModels;

	}
	
	public LinkedList<ULBNameModel> fetchULBNameByULBS(int m_ulb_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByULBS -- START");
		}

		LinkedList<ULBNameModel> ulbNameModels = ulbNameDao
				.fetchULBNameByULBS(m_ulb_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByULBS -- END");
		}

		return ulbNameModels;

	}
	
	public LinkedList<ULBNameModel> fetchULBNameByULBId(int ulbId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByULBId -- START");
		}

		LinkedList<ULBNameModel> ulbNameModels = ulbNameDao
				.fetchULBNameByULBId(ulbId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByULBId -- END");
		}

		return ulbNameModels;

	}
	
	public LinkedList<ULBNameModel> fetchULBNameByDivisionId(int divisionId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByDivisionId -- START");
		}

		LinkedList<ULBNameModel> ulbNameModels = ulbNameDao
				.fetchULBNameByDivisionId(divisionId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBNameByDivisionId -- END");
		}

		return ulbNameModels;

	}
}
