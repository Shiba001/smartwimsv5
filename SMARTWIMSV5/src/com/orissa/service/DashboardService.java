/**
 * @formatter:off
 *
 */
package com.orissa.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.common.DateCalculation;
import com.orissa.dao.DashboardDao;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.DailyTankersModel;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.DailyWaterTestingModel;
import com.orissa.model.ListingModel;
import com.orissa.model.TargetModel;
import com.orissa.model.ULBModel;
import com.orissa.model.ULBNameModel;

@Transactional
@Service
public class DashboardService {

	private static Logger logger = Logger.getLogger(DashboardService.class);

	@Autowired
	private DashboardDao dashboardDao;
	
	@Autowired
	private DistrictService districtService;

	@Autowired
	private ULBNameService ulbNameService;
	
	@Autowired
	private ULBService ulbService;
	
	public LinkedList<DailyWaterSupplyModel> fetchDailyWaterSupplyReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyReport -- START");
		}
		
		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dashboardDao.fetchDailyWaterSupplyReport(listingModel, amrutTypeId, startDate, endDate);
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyReport -- END");
		}

		return dailyWaterSupplyModels;

	}
	
	public LinkedList<DailyComplaintsModel> fetchDailyComplaintsReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaintsReport -- START");
		}
		
		LinkedList<DailyComplaintsModel> dailyComplaintsModels = dashboardDao.fetchDailyComplaintsReport(listingModel, amrutTypeId, startDate, endDate);
				
		if (logger.isInfoEnabled()) {
				logger.info("fetchDailyComplaintsReport -- END");
		}
				
		return dailyComplaintsModels;
			
	}
	
	public LinkedList<TargetModel> fetchTargetWaterChargesTaxReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTaxReport -- START");
		}
		
		String month = null;
		String year = null;
		
		if(startDate != null) {
			
			month = startDate.substring(5, 7);
			year = startDate.substring(0, 4);
			
			if (month != null) {
				
				List<String> dates = null;
				try {
					dates = new DateCalculation().findDate(Integer.valueOf(year), Integer.valueOf(month));
					startDate = dates.get(0);
					endDate = dates.get(1);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				} 
				
			}
		}
		
		LinkedList<TargetModel> targetModels = dashboardDao.fetchTargetWaterChargesTaxReport(listingModel, amrutTypeId, startDate, endDate);
	
		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTaxReport -- END");
		}
	
		return targetModels;
	
	}
	
	public LinkedList<DailyTankersModel> fetchDailyTankersReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankersReport -- START");
		}

		LinkedList<DailyTankersModel> dailyTankersModels = dashboardDao.fetchDailyTankersReport(listingModel, amrutTypeId, startDate, endDate);
				
		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyTankersReport -- END");
		}
			
		return dailyTankersModels;

	}
	
	public LinkedList<DailyWaterTestingModel> fetchDailyWaterTestingReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTestingReport -- START");
		}
		
		LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dashboardDao.fetchDailyWaterTestingReport(listingModel, amrutTypeId, startDate, endDate);
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTestingReport -- END");
		}

		return dailyWaterTestingModels;

	}
	
	public HashMap<String,HashMap<String,ULBModel>> dataEntryStatus(ListingModel listingModel, String amrutTypeId, String startDate)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("dataEntryStatus -- START");
		}
		
		HashMap<String,HashMap<String,ULBModel>> dataEntryStatus = new HashMap<String, HashMap<String,ULBModel>>();
		
		HashMap<String,ULBModel> completeDataEntryRecord = new HashMap<String,ULBModel>();
		
		HashMap<String,ULBModel> partialDataEntryRecord = new HashMap<String,ULBModel>();
		
		HashMap<String,ULBModel> noDataEntryRecord = new HashMap<String,ULBModel>();
		
		HashMap<String,ULBModel> approvedRecord = new HashMap<String,ULBModel>();
		
		HashMap<String,ULBModel> noApprovedRecord = new HashMap<String,ULBModel>();
		
		List<ULBModel> ulbModels = null;
		
		if(listingModel.getUlbId() == null) { // No ULB ID Present
			
			if(listingModel.getDistrictId() == null) { // No Division ID Present
			
				if(listingModel.getDivisionId() == null) { // No District ID Present
				
					if(amrutTypeId == null)
						ulbModels = ulbService.fetchAllULB(); // Fetch All ULB Name
					else
						ulbModels = ulbService.fetchULBByAmrutType(Integer.valueOf(amrutTypeId)); // Fetch All ULB Name By ULB Name
					
				} else { // Division ID Present
					
					if(amrutTypeId == null)
						ulbModels = ulbService.fetchULBByDivisionId(listingModel.getDivisionId());
					else
						ulbModels = ulbService.fetchULBByDivisionIdAndAmrut(listingModel.getDivisionId(), Integer.valueOf(amrutTypeId));
				}
			
			} else { // District ID Present
				
				if(amrutTypeId == null)
					ulbModels = ulbService.fetchULBByDistrictId(listingModel.getDistrictId());
				else
					ulbModels = ulbService.fetchULBByDistrictIdAndAmrut(listingModel.getDistrictId(), Integer.valueOf(amrutTypeId));
			}
			
		} else {//  ULB ID Present
			
			ulbModels = new ArrayList<ULBModel>();
			ULBModel ulbModel = ulbService.fetchULBByULBId(listingModel.getUlbId());
			ulbModels.add(ulbModel);
		}
		
		//List<ULBModel> ulbModels = ulbService.fetchAllULB();
		
		int completeDataEntry = 0;
		int partialDataEntry = 0;
		int noDataEntry = 0;
		int approved = 0;
		int notApproved = 0;
		
		for(ULBModel ulbModel : ulbModels) {
			
			List<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByULBS(ulbModel.getM_ulb_id());
			
			int flag = 0;
			int flag2 = 0;
			int count = 1;
			
			String table[] = { "daily_complaints", "daily_connection",
					"daily_water_testing", "daily_tankers", "daily_water_charges_tax",
					"daily_water_supply" };
			
			for(ULBNameModel ulbNameModel:ulbNameModels){
				
				count++;
				for (int i = 0; i < table.length; i++) {

					int entryCheckResult = 0;
				
					entryCheckResult = dashboardDao.entryCheckWithoutApproval(table[i], startDate, ulbNameModel.getUlb_id());
					
					if(entryCheckResult == 1) { // Entry Occurs
						
						flag = flag + 1;
					} 
					
					int approvalCheckResult = 0;
					
					approvalCheckResult = dashboardDao.entryCheck(table[i], startDate, ulbNameModel.getUlb_id());
					
					if(approvalCheckResult == 1) { // Approval Occurs
						
						flag2 = flag2 + 1;
					}
				
				}
			}
			
			if(count > 1)
				count--;
			
			if(flag == table.length * count) { // Complete Set Of Value Present
				
				ulbModel.setDataEntryStatus("Entry Completed");
				completeDataEntry = completeDataEntry + 1;
				completeDataEntryRecord.put(ulbModel.getPhDistrictModel().getPhDivisionModel().getDiv_name()+"."+ulbModel.getM_ulb_id(), ulbModel);
				
			} else if(flag > 0) { // Partial Set Of Value Present
				
				ulbModel.setDataEntryStatus("Partial Entry");
				partialDataEntry = partialDataEntry + 1;
				partialDataEntryRecord.put(ulbModel.getPhDistrictModel().getPhDivisionModel().getDiv_name()+"."+ulbModel.getM_ulb_id(), ulbModel);
				
			} else { // No Record Found
				
				ulbModel.setDataEntryStatus("Not Complete Entry");
				noDataEntry = noDataEntry + 1;
				noDataEntryRecord.put(ulbModel.getPhDistrictModel().getPhDivisionModel().getDiv_name()+"."+ulbModel.getM_ulb_id(), ulbModel);
			}
			
			if(flag2 == table.length * count) { // Complete Set Of Approval Present
				
				ulbModel.setDataApprovalStatus("Approved");
				approved = approved + 1;
				approvedRecord.put(ulbModel.getPhDistrictModel().getPhDivisionModel().getDiv_name()+"."+ulbModel.getM_ulb_id(), ulbModel);
				
			} else { // No Approval Found
				
				ulbModel.setDataApprovalStatus("Pending");
				notApproved = notApproved + 1;
				noApprovedRecord.put(ulbModel.getPhDistrictModel().getPhDivisionModel().getDiv_name()+"."+ulbModel.getM_ulb_id(), ulbModel);
			} 
		} 
		
		dataEntryStatus.put("C"+completeDataEntry, completeDataEntryRecord);
		dataEntryStatus.put("P"+partialDataEntry, partialDataEntryRecord);
		dataEntryStatus.put("N"+noDataEntry, noDataEntryRecord);
		dataEntryStatus.put("A"+approved, approvedRecord);
		dataEntryStatus.put("NA"+notApproved, noApprovedRecord);
		
		if (logger.isInfoEnabled()) {
			logger.info("dataEntryStatus -- END");
		}

		return dataEntryStatus;

	}
	
	public HashMap<String,HashMap<String,ULBModel>> dataEntryStatusInnerPage(ListingModel listingModel, String amrutTypeId, String startDate)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("dataEntryStatusInnerPage -- START");
		}
		
		HashMap<String,HashMap<String,ULBModel>> dataEntryStatus = new HashMap<String, HashMap<String,ULBModel>>();
		
		HashMap<String,ULBModel> completeDataEntryRecord = new HashMap<String,ULBModel>();
		
		HashMap<String,ULBModel> partialDataEntryRecord = new HashMap<String,ULBModel>();
		
		HashMap<String,ULBModel> noDataEntryRecord = new HashMap<String,ULBModel>();
		
		List<ULBModel> ulbModels = null;
		
		if(listingModel.getUlbId() == null) { // No ULB ID Present
			
			if(listingModel.getDistrictId() == null) { // No Division ID Present
			
				if(listingModel.getDivisionId() == null) { // No District ID Present
				
					if(amrutTypeId == null)
						ulbModels = ulbService.fetchAllULB(); // Fetch All ULB Name
					else
						ulbModels = ulbService.fetchULBByAmrutType(Integer.valueOf(amrutTypeId)); // Fetch All ULB Name By ULB Name
					
				} else { // Division ID Present
					
					if(amrutTypeId == null)
						ulbModels = ulbService.fetchULBByDivisionId(listingModel.getDivisionId());
					else
						ulbModels = ulbService.fetchULBByDivisionIdAndAmrut(listingModel.getDivisionId(), Integer.valueOf(amrutTypeId));
				}
			
			} else { // District ID Present
				
				if(amrutTypeId == null)
					ulbModels = ulbService.fetchULBByDistrictId(listingModel.getDistrictId());
				else
					ulbModels = ulbService.fetchULBByDistrictIdAndAmrut(listingModel.getDistrictId(), Integer.valueOf(amrutTypeId));
			}
			
		} else {//  ULB ID Present
			
			ulbModels = new ArrayList<ULBModel>();
			ULBModel ulbModel = ulbService.fetchULBByULBId(listingModel.getUlbId());
			ulbModels.add(ulbModel);
		}
		
		//List<ULBModel> ulbModels = ulbService.fetchAllULB();
		
		int completeDataEntry = 0;
		int partialDataEntry = 0;
		int noDataEntry = 0;
		
		for(ULBModel ulbModel : ulbModels) {
			
			List<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByULBS(ulbModel.getM_ulb_id());
			
			int flag = 0;
			int count = 1;
			
			String table[] = { "daily_complaints", "daily_connection",
					"daily_water_testing", "daily_tankers", "daily_water_charges_tax",
					"daily_water_supply" };
			
			for(ULBNameModel ulbNameModel:ulbNameModels){
				
				count++;
				for (int i = 0; i < table.length; i++) {

					int entryCheckResult = 0;
				
					entryCheckResult = dashboardDao.entryCheckWithoutApproval(table[i], startDate, ulbNameModel.getUlb_id());
					
					if(entryCheckResult == 1) { // Entry Occurs
						
						flag = flag + 1;
					} 
					
				}
			}
			
			if(count > 1)
				count--;
			
			if(flag == table.length * count) { // Complete Set Of Value Present
				
				ulbModel.setDataEntryStatus("Entry Completed");
				completeDataEntry = completeDataEntry + 1;
				completeDataEntryRecord.put(ulbModel.getPhDistrictModel().getPhDivisionModel().getDiv_name()+"."+ulbModel.getM_ulb_id(), ulbModel);
				
			} else if(flag > 0) { // Partial Set Of Value Present
				
				ulbModel.setDataEntryStatus("Partial Entry");
				partialDataEntry = partialDataEntry + 1;
				partialDataEntryRecord.put(ulbModel.getPhDistrictModel().getPhDivisionModel().getDiv_name()+"."+ulbModel.getM_ulb_id(), ulbModel);
				
			} else { // No Record Found
				
				ulbModel.setDataEntryStatus("Not Complete Entry");
				noDataEntry = noDataEntry + 1;
				noDataEntryRecord.put(ulbModel.getPhDistrictModel().getPhDivisionModel().getDiv_name()+"."+ulbModel.getM_ulb_id(), ulbModel);
			}
			
		} 
		
		dataEntryStatus.put("C"+completeDataEntry, completeDataEntryRecord);
		dataEntryStatus.put("P"+partialDataEntry, partialDataEntryRecord);
		dataEntryStatus.put("N"+noDataEntry, noDataEntryRecord);
		
		if (logger.isInfoEnabled()) {
			logger.info("dataEntryStatusInnerPage -- END");
		}

		return dataEntryStatus;

	}
	
	public HashMap<String,HashMap<String,ULBModel>> dataApprovalStatusInnerPage(ListingModel listingModel, String amrutTypeId, String startDate)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("dataApprovalStatusInnerPage -- START");
		}
		
		HashMap<String,HashMap<String,ULBModel>> dataEntryStatus = new HashMap<String, HashMap<String,ULBModel>>();
		
		HashMap<String,ULBModel> approvedRecord = new HashMap<String,ULBModel>();
		
		HashMap<String,ULBModel> noApprovedRecord = new HashMap<String,ULBModel>();
		
		List<ULBModel> ulbModels = null;
		
		if(listingModel.getUlbId() == null) { // No ULB ID Present
			
			if(listingModel.getDistrictId() == null) { // No Division ID Present
			
				if(listingModel.getDivisionId() == null) { // No District ID Present
				
					if(amrutTypeId == null)
						ulbModels = ulbService.fetchAllULB(); // Fetch All ULB Name
					else
						ulbModels = ulbService.fetchULBByAmrutType(Integer.valueOf(amrutTypeId)); // Fetch All ULB Name By ULB Name
					
				} else { // Division ID Present
					
					if(amrutTypeId == null)
						ulbModels = ulbService.fetchULBByDivisionId(listingModel.getDivisionId());
					else
						ulbModels = ulbService.fetchULBByDivisionIdAndAmrut(listingModel.getDivisionId(), Integer.valueOf(amrutTypeId));
				}
			
			} else { // District ID Present
				
				if(amrutTypeId == null)
					ulbModels = ulbService.fetchULBByDistrictId(listingModel.getDistrictId());
				else
					ulbModels = ulbService.fetchULBByDistrictIdAndAmrut(listingModel.getDistrictId(), Integer.valueOf(amrutTypeId));
			}
			
		} else {//  ULB ID Present
			
			ulbModels = new ArrayList<ULBModel>();
			ULBModel ulbModel = ulbService.fetchULBByULBId(listingModel.getUlbId());
			ulbModels.add(ulbModel);
		}
		
		//List<ULBModel> ulbModels = ulbService.fetchAllULB();
		
		int approved = 0;
		int notApproved = 0;
		
		for(ULBModel ulbModel : ulbModels) {
			
			List<ULBNameModel> ulbNameModels = ulbNameService.fetchULBNameByULBS(ulbModel.getM_ulb_id());
			
			int flag2 = 0;
			int count = 1;
			
			String table[] = { "daily_complaints", "daily_connection",
					"daily_water_testing", "daily_tankers", "daily_water_charges_tax",
					"daily_water_supply" };
			
			for(ULBNameModel ulbNameModel:ulbNameModels){
				
				count++;
				for (int i = 0; i < table.length; i++) {

					int approvalCheckResult = 0;
					
					approvalCheckResult = dashboardDao.entryCheck(table[i], startDate, ulbNameModel.getUlb_id());
					
					if(approvalCheckResult == 1) { // Approval Occurs
						
						flag2 = flag2 + 1;
					}
				
				}
			}
			
			if(count > 1)
				count--;
			
			if(flag2 == table.length * count) { // Complete Set Of Approval Present
				
				ulbModel.setDataApprovalStatus("Approved");
				approved = approved + 1;
				approvedRecord.put(ulbModel.getPhDistrictModel().getPhDivisionModel().getDiv_name()+"."+ulbModel.getM_ulb_id(), ulbModel);
				
			} else { // No Approval Found
				
				ulbModel.setDataApprovalStatus("Pending");
				notApproved = notApproved + 1;
				noApprovedRecord.put(ulbModel.getPhDistrictModel().getPhDivisionModel().getDiv_name()+"."+ulbModel.getM_ulb_id(), ulbModel);
			} 
		} 
		
		dataEntryStatus.put("A"+approved, approvedRecord);
		dataEntryStatus.put("NA"+notApproved, noApprovedRecord);
		
		if (logger.isInfoEnabled()) {
			logger.info("dataApprovalStatusInnerPage -- END");
		}

		return dataEntryStatus;

	}
	
	public LinkedList<TargetModel> fetchTargetWaterChargesTaxInnerPageReport(ListingModel listingModel, String amrutTypeId, String startDate, String endDate) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTaxInnerPageReport -- START");
		}
		
		LinkedList<TargetModel> targetModels = dashboardDao.fetchTargetWaterChargesTaxReport(listingModel, amrutTypeId, startDate, endDate);
	
		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTaxInnerPageReport -- END");
		}
	
		return targetModels;
	
	}
	
}
