package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.PhysicalInfraTubewellDao;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraTubewellsModel;
import com.orissa.validation.PhysicalInfraTubewellValidation;

@Transactional
@Service
public class PhysicalInfraTubewellService {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraTubewellService.class);

	@Autowired
	private PhysicalInfraTubewellDao physicalInfraTubewellDao;

	@Autowired
	private PhysicalInfraTubewellValidation physicalInfraTubewellValidation;

	public int addPhysicalInfraTubewell(
			PhysicalInfraTubewellsModel physicalInfraTubewellsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTubewells -- START");
		}
		physicalInfraTubewellValidation
				.createPhysicalInfraTubewelValidation(physicalInfraTubewellsModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		physicalInfraTubewellsModel.setLoginUsersModel(loginUsersModel);

		physicalInfraTubewellsModel
				.setPro_tw_wc((physicalInfraTubewellsModel.getTot_no_tw_wc() / physicalInfraTubewellsModel
						.getTot_no_tw()) * 100);
		physicalInfraTubewellsModel
				.setTot_no_tw_ur(physicalInfraTubewellsModel.getTot_no_tw()
						- physicalInfraTubewellsModel.getTot_no_tw_wc());
		
		int result = physicalInfraTubewellDao
				.addPhysicalInfraTubewell(physicalInfraTubewellsModel);

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTubewells -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraTubewellsModel> fetchPhysicalInfraTubewells(HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTubewells -- START");
		}
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		LinkedList<PhysicalInfraTubewellsModel> physicalInfraTubewellsModels = physicalInfraTubewellDao
				.fetchPhysicalInfraTubewells(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTubewells -- END");
		}

		return physicalInfraTubewellsModels;

	}

	public PhysicalInfraTubewellsModel fetchPhysicalInfraTubewellsById(
			int phyInfra_tw_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTubewellsById -- START");
		}

		PhysicalInfraTubewellsModel physicalInfraTubewellsModel = physicalInfraTubewellDao
				.fetchPhysicalInfraTubewellsById(phyInfra_tw_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraTubewellsById -- END");
		}

		return physicalInfraTubewellsModel;

	}

	public int deletePhysicalInfraTubewellsById(int phyInfra_tw_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraTubewellsById -- START");
		}

		int result = physicalInfraTubewellDao.commonDelete(
				"physical_infra_tubewells", "phyInfra_tw_id", phyInfra_tw_id);

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraTubewellsById -- END");
		}

		return result;

	}

	public int updatePhysicalInfraTubewells(
			PhysicalInfraTubewellsModel physicalInfraTubewellsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraTubewells -- START");
		}
		
		physicalInfraTubewellValidation
		.createPhysicalInfraTubewelValidation(physicalInfraTubewellsModel);
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));


		physicalInfraTubewellsModel.setLoginUsersModel(loginUsersModel);
		
		physicalInfraTubewellsModel
				.setPro_tw_wc((physicalInfraTubewellsModel.getTot_no_tw_wc() / physicalInfraTubewellsModel
						.getTot_no_tw()) * 100);
		physicalInfraTubewellsModel
				.setTot_no_tw_ur(physicalInfraTubewellsModel.getTot_no_tw()
						- physicalInfraTubewellsModel.getTot_no_tw_wc());
		
		int result = physicalInfraTubewellDao
				.updatePhysicalInfraTubewells(physicalInfraTubewellsModel);
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraTubewells -- END");
		}

		return result;

	}
	public LinkedList<PhysicalInfraTubewellsModel> fetchAJAXPhysicalInfraTubewells(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraTubewells -- START");
		}

		LinkedList<PhysicalInfraTubewellsModel> physicalInfraTubewellsModels = physicalInfraTubewellDao
				.fetchAJAXPhysicalInfraTubewell(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraTubewells -- END");
		}

		return physicalInfraTubewellsModels;

	}

}
