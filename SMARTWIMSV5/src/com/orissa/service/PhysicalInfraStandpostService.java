package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.PhysicalInfraStandpostDao;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraStandpostsModel;
import com.orissa.model.ULBNameModel;
import com.orissa.validation.PhysicalInfraStandpostValidation;

@Transactional
@Service
public class PhysicalInfraStandpostService {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraStandpostService.class);

	@Autowired
	private PhysicalInfraStandpostDao physicalInfraStandpostDao;
	@Autowired
	private PhysicalInfraStandpostValidation physicalInfraStandpostValidation;

	public int addPhysicalInfraStandpost(
			PhysicalInfraStandpostsModel physicalInfraStandpostsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraStandposts -- START");
		}

		physicalInfraStandpostValidation
				.createPhysicalInfraStandpostValidation(physicalInfraStandpostsModel);
		

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		physicalInfraStandpostsModel.setLoginUsersModel(loginUsersModel);
		
		physicalInfraStandpostsModel
				.setProp_standposts_work_cond((physicalInfraStandpostsModel
						.getNo_standposts_work_cond() / physicalInfraStandpostsModel
						.getNo_exst_stanposts()) * 100);

		int result = physicalInfraStandpostDao
				.addPhysicalInfraStandpost(physicalInfraStandpostsModel);

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraStandposts -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraStandpostsModel> fetchPhysicalInfraStandposts(HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStandposts -- START");
		}
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		LinkedList<PhysicalInfraStandpostsModel> physicalInfraStandpostsModels = physicalInfraStandpostDao
				.fetchPhysicalInfraStandposts(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStandpost -- END");
		}

		return physicalInfraStandpostsModels;

	}

	public PhysicalInfraStandpostsModel fetchPhysicalInfraStandpostById(
			int phyInfra_con_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStandpostById -- START");
		}

		PhysicalInfraStandpostsModel physicalInfraStandpostsModel = physicalInfraStandpostDao
				.fetchPhysicalInfraStandpostsById(phyInfra_con_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStandpostById -- END");
		}

		return physicalInfraStandpostsModel;

	}

	public int deletePhysicalInfraStandpostById(int phyInfra_std_post_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraStandpostById -- START");
		}

		int result = physicalInfraStandpostDao.commonDelete(
				"physical_infra_standposts", "phyInfra_std_post_id",
				phyInfra_std_post_id);

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraStandpostById -- END");
		}

		return result;

	}

	public int updatePhysicalInfraStandpost(
			PhysicalInfraStandpostsModel physicalInfraStandpostsModel,
			HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraStandpost -- START");
		}
		
		physicalInfraStandpostValidation
		.createPhysicalInfraStandpostValidation(physicalInfraStandpostsModel);
		
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		physicalInfraStandpostsModel.setLoginUsersModel(loginUsersModel);
		physicalInfraStandpostsModel
				.setProp_standposts_work_cond((physicalInfraStandpostsModel
						.getNo_standposts_work_cond() / physicalInfraStandpostsModel
						.getNo_exst_stanposts()) * 100);

		int result = physicalInfraStandpostDao
				.updatePhysicalInfraStandposts(physicalInfraStandpostsModel);
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraStandpost -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraStandpostsModel> fetchAJAXPhysicalInfraStandpost(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStandpost -- START");
		}
		
		LinkedList<PhysicalInfraStandpostsModel> physicalInfraStandpostsModels = physicalInfraStandpostDao.fetchAJAXPhysicalInfraStandpost(listingModel);
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStandpost -- END");
		}
		return physicalInfraStandpostsModels;
	}

}
