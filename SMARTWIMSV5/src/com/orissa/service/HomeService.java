package com.orissa.service;

import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.HomeDao;
import com.orissa.exception.DBConnectionException;
import com.orissa.model.DailyWaterSupplyModel;

@Transactional
@Service
public class HomeService {

	private static Logger logger = Logger.getLogger(HomeService.class);

	@Autowired
	private HomeDao homeDao;

	public String index() throws DBConnectionException {

		if (logger.isInfoEnabled()) {
			logger.info("index -- START");
		}

		String connect = homeDao.index();

		if (logger.isInfoEnabled()) {
			logger.info("index -- END");
		}

		return connect;
	}

	public LinkedList<DailyWaterSupplyModel> fetchDailyWaterSupplyReport()
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyReport -- START");
		}

		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = homeDao
				.fetchDailyWaterSupplyReport();

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyReport -- END");
		}

		return dailyWaterSupplyModels;

	}
}
