package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.DailyWaterChargesDao;
import com.orissa.model.DailyWaterChargesModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.validation.DailyWaterChargesValidation;

@Transactional
@Service
public class DailyWaterChargesService {

	private static Logger logger = Logger
			.getLogger(DailyWaterChargesService.class);

	@Autowired
	private DailyWaterChargesDao dailyWaterChargesDao;

	@Autowired
	private DailyWaterChargesValidation dailyWaterChargesValidation;

	public int addDailyWaterCharge(
			DailyWaterChargesModel dailyWaterChargesModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterCharge -- START");
		}
		dailyWaterChargesValidation
				.createDailyWaterChargesValidation(dailyWaterChargesModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		dailyWaterChargesModel.setLoginUsersModel(loginUsersModel);

		/*if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4)
			dailyWaterChargesModel.setIsApproved(0); // To be Changed
		else*/
			dailyWaterChargesModel.setIsApproved(0); // To be Changed

		int result = dailyWaterChargesDao
				.addDailyWaterCharges(dailyWaterChargesModel);

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterCharge -- END");
		}

		return result;

	}

	public LinkedList<DailyWaterChargesModel> fetchDailyWaterCharge(
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterCharge -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		LinkedList<DailyWaterChargesModel> dailyWaterChargesModels = dailyWaterChargesDao
				.fetchDailyWaterCharges(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterCharge -- END");
		}

		return dailyWaterChargesModels;

	}

	public DailyWaterChargesModel fetchDailyWaterChargeById(int W_tax_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterChargeById -- START");
		}

		DailyWaterChargesModel dailyWaterChargesModel = dailyWaterChargesDao
				.fetchDailyWaterChargeById(W_tax_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterChargeById -- END");
		}

		return dailyWaterChargesModel;

	}

	public int isApprovedUpdate(int W_tax_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		int result = dailyWaterChargesDao.isApprovedUpdate(
				"daily_water_charges_tax", "W_tax_id", W_tax_id);

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return result;

	}
	
	public int isNotApprovedUpdate(int W_tax_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}

		int result = dailyWaterChargesDao.isNotApprovedUpdate(
				"daily_water_charges_tax", "W_tax_id", W_tax_id);

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return result;

	}

	public int deleteDailyWaterChargeById(int W_tax_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterChargesById -- START");
		}

		int result = dailyWaterChargesDao.commonDelete(
				"daily_water_charges_tax", "W_tax_id", W_tax_id);

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterChargesById -- END");
		}

		return result;

	}

	public int updateDailyWaterCharge(
			DailyWaterChargesModel dailyWaterChargesModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterCharge -- START");
		}

		dailyWaterChargesValidation
				.createDailyWaterChargesValidation(dailyWaterChargesModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		dailyWaterChargesModel.setLoginUsersModel(loginUsersModel);

		int result = dailyWaterChargesDao
				.updateDailyWaterCharges(dailyWaterChargesModel);
		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterCharge -- END");
		}

		return result;

	}

	public LinkedList<DailyWaterChargesModel> fetchAJAXDailyWaterCharges(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterCharges -- START");
		}

		LinkedList<DailyWaterChargesModel> dailyWaterChargesModels = dailyWaterChargesDao
				.fetchAJAXDailyWaterCharges(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterCharges -- END");
		}

		return dailyWaterChargesModels;

	}
}
