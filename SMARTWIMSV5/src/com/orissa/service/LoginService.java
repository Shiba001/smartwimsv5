package com.orissa.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.LoginDao;
import com.orissa.model.LoginUsersModel;
import com.orissa.validation.LoginValidation;

@Transactional
@Service
public class LoginService {

	private static Logger logger = Logger.getLogger(LoginService.class);

	@Autowired
	private LoginDao loginDao;
	
	@Autowired
	private LoginValidation loginValidation;

	public LoginUsersModel loginCheck(LoginUsersModel loginUsersModel, HttpServletRequest request)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("loginCheck -- START");
		}
		
		loginValidation.loginCheck(loginUsersModel, request);

		LoginUsersModel loginUsersModel2 = loginDao.loginCheck(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("loginCheck -- END");
		}

		return loginUsersModel2;

	}
}
