package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.DailyConnectionsDao;
import com.orissa.model.DailyConnetionsModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.validation.DailyConnectionsValidation;

@Transactional
@Service
public class DailyConnectionsService {

	private static Logger logger = Logger
			.getLogger(DailyConnectionsService.class);

	@Autowired
	private DailyConnectionsDao dailyConnetionsDao;

	@Autowired
	private DailyConnectionsValidation dailyConnectionsValidation;

	public int addDailyConnetions(DailyConnetionsModel dailyConnetionsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyConnetions -- START");
		}
		dailyConnectionsValidation
				.createDailyConnectionsValidation(dailyConnetionsModel);
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		dailyConnetionsModel.setTot_no_new_cons(dailyConnetionsModel
				.getNo_new_com_cons_add()
				+ dailyConnetionsModel.getNo_new_ind_cons_add()
				+ dailyConnetionsModel.getNo_new_inst_cons_add()
				+ dailyConnetionsModel.getNo_new_res_cons_add());
		dailyConnetionsModel.setTot_no_cons_disc(dailyConnetionsModel
				.getNo_com_cons_disc()
				+ dailyConnetionsModel.getNo_ind_cons_disc()
				+ dailyConnetionsModel.getNo_inst_cons_disc()
				+ dailyConnetionsModel.getNo_res_cons_disc());

		dailyConnetionsModel.setLoginUsersModel(loginUsersModel);

		/*if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4)
			dailyConnetionsModel.setIsApproved(0); // To be Changed
		else*/
			dailyConnetionsModel.setIsApproved(0); // To be Changed

		int result = dailyConnetionsDao
				.addDailyConnetions(dailyConnetionsModel);

		if (logger.isInfoEnabled()) {
			logger.info("addDailyConnetions -- END");
		}

		return result;

	}

	public LinkedList<DailyConnetionsModel> fetchDailyConnetions(
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetions -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		LinkedList<DailyConnetionsModel> dailyConnetionsModels = dailyConnetionsDao
				.fetchDailyConnetions(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetions -- END");
		}

		return dailyConnetionsModels;

	}

	public DailyConnetionsModel fetchDailyConnetionsById(int con_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetionsById -- START");
		}

		DailyConnetionsModel dailyConnetionsModel = dailyConnetionsDao
				.fetchDailyConnetionsById(con_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyConnetionsById -- END");
		}

		return dailyConnetionsModel;

	}

	public int deleteDailyConnectionById(int con_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyConnectionById -- START");
		}

		int result = dailyConnetionsDao.commonDelete("daily_connection",
				"con_id", con_id);

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyConnectionById -- END");
		}

		return result;

	}

	public int isApprovedUpdate(int con_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		int result = dailyConnetionsDao.isApprovedUpdate("daily_connection",
				"con_id", con_id);

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return result;

	}
	
	public int isNotApprovedUpdate(int con_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}

		int result = dailyConnetionsDao.isNotApprovedUpdate("daily_connection",
				"con_id", con_id);

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return result;

	}

	public int updateDailyConnetions(DailyConnetionsModel dailyConnetionsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyConnetions -- START");
		}
		dailyConnectionsValidation
				.createDailyConnectionsValidation(dailyConnetionsModel);
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		dailyConnetionsModel.setTot_no_new_cons(dailyConnetionsModel
				.getNo_new_com_cons_add()
				+ dailyConnetionsModel.getNo_new_ind_cons_add()
				+ dailyConnetionsModel.getNo_new_inst_cons_add()
				+ dailyConnetionsModel.getNo_new_res_cons_add());
		dailyConnetionsModel.setTot_no_cons_disc(dailyConnetionsModel
				.getNo_com_cons_disc()
				+ dailyConnetionsModel.getNo_ind_cons_disc()
				+ dailyConnetionsModel.getNo_inst_cons_disc()
				+ dailyConnetionsModel.getNo_res_cons_disc());

		dailyConnetionsModel.setLoginUsersModel(loginUsersModel);

		int result = dailyConnetionsDao
				.updateDailyConnetions(dailyConnetionsModel);

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyConnetions -- END");
		}

		return result;

	}

	public LinkedList<DailyConnetionsModel> fetchAJAXDailyConnetions(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyConnetions -- START");
		}

		LinkedList<DailyConnetionsModel> dailyConnetionsModels = dailyConnetionsDao
				.fetchAJAXDailyConnetions(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyConnetions -- END");
		}

		return dailyConnetionsModels;

	}
}
