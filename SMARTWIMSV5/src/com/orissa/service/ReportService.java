package com.orissa.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.common.DateCalculation;
import com.orissa.dao.ReportDao;
import com.orissa.model.ListingModel;
import com.orissa.model.ULBModel;
import com.orissa.model.ULBNameModel;

@Transactional
@Service
public class ReportService {

	private static Logger logger = Logger.getLogger(ReportService.class);

	@Autowired
	private ReportDao reportDao;
	
	@Autowired
	private ULBNameService ulbNameService;
	
	@Autowired
	private ULBService ulbService;
	
	@Autowired
	private DateCalculation dateCalculation;
	
	public LinkedHashMap<String,LinkedList<String>> dataEntryStatus(ListingModel listingModel, String amrutTypeId, String startDate, String tableName)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportPage -- START");
		}
		
		LinkedHashMap<String,LinkedList<String>> dataEntryReport = new LinkedHashMap<String,LinkedList<String>>();
		
		List<ULBNameModel> ulbNameModels = null;
		
		if(listingModel.getUlbId() == null) { // No ULB ID Present
			
			if(listingModel.getDistrictId() == null) { // No Division ID Present
			
				if(listingModel.getDivisionId() == null) { // No District ID Present
				
					ulbNameModels = ulbNameService.fetchAllULBName(); // Fetch All ULB Name
					
				} else { // Division ID Present
					
					ulbNameModels = ulbNameService.fetchULBNameByDivisionId(listingModel.getDivisionId());
					
				}
			
			} else { // District ID Present
				
				ulbNameModels = ulbNameService.fetchULBNameByDistrictId(listingModel.getDistrictId());
			}
			
		} else {//  ULB ID Present
			
			ulbNameModels = new ArrayList<ULBNameModel>();
			
			ulbNameModels = ulbNameService.fetchULBNameByULBId(listingModel.getUlbId());
		}
		
		/*String table[] = { "daily_complaints", "daily_connection",
				"daily_water_testing", "daily_tankers", "daily_water_charges_tax",
				"daily_water_supply" };*/
			
			for(ULBNameModel ulbNameModel:ulbNameModels) {
				
				LinkedList<String> ulbEntry = new LinkedList<String>();
				List<String> datesBetween = dateCalculation.findDatesBetween(startDate);
				
				for(String date:datesBetween) {
					
					int entryCheckResult = 0;
					
					entryCheckResult = reportDao.entryCheckWithoutApproval(tableName, date, ulbNameModel.getUlb_id());
					
					if(entryCheckResult == 1) { // Entry Occurs
						
						ulbEntry.add("Y");
						
					} else {
						
						ulbEntry.add("N");
					
					}	
				
				}	
				
				dataEntryReport.put(ulbNameModel.getUlb_Name(), ulbEntry);
			}
			
		if (logger.isInfoEnabled()) {
			logger.info("exceptionReportPage -- END");
		}

		return dataEntryReport;

	}
}
