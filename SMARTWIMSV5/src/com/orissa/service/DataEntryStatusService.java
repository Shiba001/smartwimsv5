package com.orissa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.DataEntryStatusDao;
import com.orissa.model.ULBNameModel;

@Transactional
@Service
public class DataEntryStatusService {

	@Autowired
	private DataEntryStatusDao dataEntryStatusDao;

	public List getServiceLogic1(List<ULBNameModel> list, String date_of_report, String amrutTypeId) throws Exception {

		String date = date_of_report;
		String amruthvalue = amrutTypeId;

		System.out.println("date value -  " + date);
		System.out.println("amruthvalue - " + amruthvalue);

		/*
		 * System.out.println("id" +list.getFirst().getUlb_id());
		 * System.out.println("name" +list.getFirst().getUlb_Name());
		 */

		System.out.println("in service layer");

		// List<ULBNameModel> l2 = list;
		// int ulbID=1;

		// Boolean b = satyaDao.getResult(ulbID, date);

		/*
		 * for(int i=0; i<l2.size(); i++){
		 * 
		 * ulbID = l2.get(i).getUlb_id();
		 * 
		 * System.out.println("ULB ID -- "+l2.get(i).getUlb_id() +
		 * "ULB NAME -- " +l2.get(i).getUlb_Name());
		 * 
		 * Boolean b = satyaDao.getResult(ulbID, date);
		 * //System.out.println("Boolean Value -> " +b);
		 * 
		 * }
		 */

		List ulbNameModels = dataEntryStatusDao.getResult(list, date);

		return ulbNameModels;

	}
	
	
	//public List getApprovalStatus(List<ULBNameModel> list, String date_of_report, String amrutTypeId) throws Exception {
		
		public List getServiceLogic(List<ULBNameModel> list, String date_of_report, String amrutTypeId) throws Exception {

		String date = date_of_report;
		String amruthvalue = amrutTypeId;

		/*System.out.println("date value -  " + date);
		System.out.println("amruthvalue - " + amruthvalue);
		System.out.println("in service layer");*/
		
		List ulbNameModels = dataEntryStatusDao.getResult(list, date);

		return ulbNameModels;

	}
		
		public List getApprovalStatus(List<ULBNameModel> list, String date_of_report, String amrutTypeId) throws Exception {

		String date = date_of_report;
		String amruthvalue = amrutTypeId;

		/*System.out.println("date value -  " + date);
		System.out.println("amruthvalue - " + amruthvalue);
		System.out.println("in service layer");*/
		
		List ulbNameModels = dataEntryStatusDao.getApprovalStatus(list, date);

		return ulbNameModels;

	}
		
	
}
