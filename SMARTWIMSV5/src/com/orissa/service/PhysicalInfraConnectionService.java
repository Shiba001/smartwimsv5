package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.PhysicalInfraConnectionDao;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraConnectionModel;
import com.orissa.validation.PhysicalInfraConnectionValidation;

@Transactional
@Service
public class PhysicalInfraConnectionService {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraConnectionService.class);

	@Autowired
	private PhysicalInfraConnectionDao physicalInfraConnectionDao;

	@Autowired
	private PhysicalInfraConnectionValidation physicalInfraConnectionValidation;

	public int addPhysicalInfraConnection(
			PhysicalInfraConnectionModel physicalInfraConnectionModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraConnection -- START");
		}

		physicalInfraConnectionValidation
				.createPhysicalInfraConnectionValidation(physicalInfraConnectionModel);
		HttpSession httpSession = httpServletRequest.getSession();
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		physicalInfraConnectionModel.setLoginUsersModel(loginUsersModel);
		physicalInfraConnectionModel
				.setTot_no_cons(physicalInfraConnectionModel.getNo_com_cons()
						+ physicalInfraConnectionModel.getNo_ind_cons()
						+ physicalInfraConnectionModel.getNo_inst_cons()
						+ physicalInfraConnectionModel.getNo_res_cons());

		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4)
			physicalInfraConnectionModel.setIsApproved(0); // To be Changed
		else
			physicalInfraConnectionModel.setIsApproved(1); // To be Changed

		int result = physicalInfraConnectionDao
				.addPhysicalInfraConnection(physicalInfraConnectionModel);

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraConnection -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraConnectionModel> fetchPhysicalInfraConnection(HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraConnection -- START");
		}
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		LinkedList<PhysicalInfraConnectionModel> physicalInfraConnectionModels = physicalInfraConnectionDao
				.fetchPhysicalInfraConnection(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraConnection -- END");
		}

		return physicalInfraConnectionModels;

	}

	public PhysicalInfraConnectionModel fetchPhysicalInfraConnectionById(
			int phyInfra_con_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraConnectionById -- START");
		}

		PhysicalInfraConnectionModel physicalInfraConnectionModel = physicalInfraConnectionDao
				.fetchPhysicalInfraConnectionById(phyInfra_con_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraConnectionById -- END");
		}

		return physicalInfraConnectionModel;

	}

	public int deletePhysicalInfraConnectionById(int phyInfra_con_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraConnectionById -- START");
		}

		int result = physicalInfraConnectionDao
				.commonDelete("physical_infra_connection", "phyInfra_con_id",
						phyInfra_con_id);

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraConnectionById -- END");
		}

		return result;

	}

	public int updatePhysicalInfraConnection(
			PhysicalInfraConnectionModel physicalInfraConnectionModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraConnection -- START");
		}
		physicalInfraConnectionValidation
				.createPhysicalInfraConnectionValidation(physicalInfraConnectionModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		physicalInfraConnectionModel.setLoginUsersModel(loginUsersModel);
		physicalInfraConnectionModel
				.setTot_no_cons(physicalInfraConnectionModel.getNo_com_cons()
						+ physicalInfraConnectionModel.getNo_ind_cons()
						+ physicalInfraConnectionModel.getNo_inst_cons()
						+ physicalInfraConnectionModel.getNo_res_cons());

		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4)
			physicalInfraConnectionModel.setIsApproved(0); // To be Changed
		else
			physicalInfraConnectionModel.setIsApproved(1); // To be Changed

		int result = physicalInfraConnectionDao
				.updatePhysicalInfraConnection(physicalInfraConnectionModel);

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraConnection -- END");
		}

		return result;

	}
	public LinkedList<PhysicalInfraConnectionModel> fetchAJAXPhysicalInfraConnection(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraConnection -- START");
		}

		LinkedList<PhysicalInfraConnectionModel> physicalInfraConnectionModels = physicalInfraConnectionDao
				.fetchAJAXPhysicalInfraConnection(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraConnection -- END");
		}

		return physicalInfraConnectionModels;

	}

}
