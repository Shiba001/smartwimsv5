package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.DailyComplaintsDao;
import com.orissa.model.DailyComplaintsModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.validation.DailyComplaintsValidation;

@Transactional
@Service
public class DailyComplaintsService {

	private static Logger logger = Logger
			.getLogger(DailyComplaintsService.class);

	@Autowired
	private DailyComplaintsDao dailyComplaintsDao;

	@Autowired
	private DailyComplaintsValidation dailyComplaintsValidation;

	public int addDailyComplaints(DailyComplaintsModel dailyComplaintsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyComplaints -- START");
		}

		dailyComplaintsValidation
				.createDailyComplaintsValidation(dailyComplaintsModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		dailyComplaintsModel.setLoginUsersModel(loginUsersModel);
		dailyComplaintsModel.setNo_ws_comp_pending(dailyComplaintsModel
				.getNo_ws_comp_recv()
				- dailyComplaintsModel.getNo_ws_comp_resolved());
		dailyComplaintsModel.setNo_tw_comp_pending(dailyComplaintsModel
				.getNo_tw_comp_recv()
				- dailyComplaintsModel.getNo_tw_comp_resolved());

		/*if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4)
			dailyComplaintsModel.setIsApproved(0); // To be Changed
		else*/
			dailyComplaintsModel.setIsApproved(0); // To be Changed

		int result = dailyComplaintsDao
				.addDailyComplaints(dailyComplaintsModel);

		if (logger.isInfoEnabled()) {
			logger.info("addDailyComplaints -- END");
		}

		return result;

	}

	public LinkedList<DailyComplaintsModel> fetchDailyComplaints(
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaints -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		LinkedList<DailyComplaintsModel> dailyComplaintsModels = dailyComplaintsDao
				.fetchDailyComplaints(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaints -- END");
		}

		return dailyComplaintsModels;

	}

	public int deleteDailyComplaintsById(int tw_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterSupplyById -- START");
		}

		int result = dailyComplaintsDao.commonDelete("daily_complaints",
				"tw_id", tw_id);

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterSupplyById -- END");
		}

		return result;

	}

	public int isApprovedUpdate(int tw_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		int result = dailyComplaintsDao.isApprovedUpdate("daily_complaints",
				"tw_id", tw_id);

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return result;

	}
	
	public int isNotApprovedUpdate(int tw_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}

		int result = dailyComplaintsDao.isNotApprovedUpdate("daily_complaints",
				"tw_id", tw_id);

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return result;

	}

	public DailyComplaintsModel fetchDailyComplaintsById(int tw_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaintsById -- START");
		}

		DailyComplaintsModel dailyComplaintsModel = dailyComplaintsDao
				.fetchDailyComplaintsById(tw_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyComplaintsById -- END");
		}

		return dailyComplaintsModel;

	}

	public int updateDailyComplaints(DailyComplaintsModel dailyComplaintsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyComplaints -- START");
		}

		dailyComplaintsValidation
				.createDailyComplaintsValidation(dailyComplaintsModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		dailyComplaintsModel.setLoginUsersModel(loginUsersModel);
		dailyComplaintsModel.setNo_ws_comp_pending(dailyComplaintsModel
				.getNo_ws_comp_recv()
				- dailyComplaintsModel.getNo_ws_comp_resolved());
		dailyComplaintsModel.setNo_tw_comp_pending(dailyComplaintsModel
				.getNo_tw_comp_recv()
				- dailyComplaintsModel.getNo_tw_comp_resolved());

		int result = dailyComplaintsDao
				.updateDailyComplaints(dailyComplaintsModel);

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyComplaints -- END");
		}

		return result;

	}

	public LinkedList<DailyComplaintsModel> fetchAJAXDailyComplaints(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyComplaints -- START");
		}

		LinkedList<DailyComplaintsModel> dailyComplaintsModels = dailyComplaintsDao
				.fetchAJAXDailyComplaints(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyComplaints -- END");
		}

		return dailyComplaintsModels;

	}
}
