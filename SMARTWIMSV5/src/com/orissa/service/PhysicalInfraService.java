package com.orissa.service;

import java.util.LinkedList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.common.SortingMap;
import com.orissa.dao.PhysicalInfraDao;
import com.orissa.model.PhysicalInfraElectricalInstallationsModel;
import com.orissa.model.PhysicalInfraModel;
import com.orissa.model.PhysicalInfraProductionWellsModel;
import com.orissa.model.PhysicalInfraPumpsAndMotorsModel;

@Transactional
@Service
public class PhysicalInfraService {

	private static Logger logger = Logger.getLogger(PhysicalInfraService.class);

	@Autowired
	private PhysicalInfraDao physicalInfraDao;
	
	@Autowired
	private SortingMap sortingMap;

	public LinkedList<PhysicalInfraModel> fetchPhysicalInfraReport()
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraReport -- START");
		}

		LinkedList<PhysicalInfraModel> physicalInfraModels = physicalInfraDao
				.fetchPhysicalInfraReport();

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraReport -- END");
		}

		return physicalInfraModels;

	}

	public Map<String, LinkedList<PhysicalInfraElectricalInstallationsModel>> fetchElectricInstallationReport()
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchElectricInstallationReport -- START");
		}
		
		LinkedList<PhysicalInfraElectricalInstallationsModel> physInfraElectInstallModels = physicalInfraDao.fetchElectricInstallationReport();

		Map<String, LinkedList<PhysicalInfraElectricalInstallationsModel>> electricInstallationReport = sortingMap.electricalInstallationListToMap(physInfraElectInstallModels);

		if (logger.isInfoEnabled()) {
			logger.info("fetchElectricInstallationReport -- END");
		}

		return electricInstallationReport;

	}

	public Map<String, LinkedList<PhysicalInfraProductionWellsModel>> fetchPhysicalInfraProdWellsReport()
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraProdWellsReport -- START");
		}

		LinkedList<PhysicalInfraProductionWellsModel> physicalInfraProdWellsModels = physicalInfraDao.fetchPhysicalInfraProdWellsReport();
		
		Map<String, LinkedList<PhysicalInfraProductionWellsModel>> prodWellReport = sortingMap.prodWellsListToMap(physicalInfraProdWellsModels);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraProdWellsReport -- END");
		}

		return prodWellReport;

	}

	public Map<String, LinkedList<PhysicalInfraPumpsAndMotorsModel>> fetchPumpAndMotorReport()
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPumpAndMotorReport -- START");
		}
		
		LinkedList<PhysicalInfraPumpsAndMotorsModel> physicalInfraPumpsAndMotorsModelList = physicalInfraDao.fetchPumpAndMotorReport();
		
		Map<String,LinkedList<PhysicalInfraPumpsAndMotorsModel>> pumpAndMotorsReport = sortingMap.pumpAndMotorsListToMap(physicalInfraPumpsAndMotorsModelList);
		
		if (logger.isInfoEnabled()) {
			logger.info("fetchPumpAndMotorReport -- END");
		}

		return pumpAndMotorsReport;

	}
}
