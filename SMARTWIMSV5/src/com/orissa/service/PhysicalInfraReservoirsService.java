package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.PhysicalInfraReservoirsDao;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraStorageReservoirsModel;
import com.orissa.model.ULBNameModel;
import com.orissa.validation.PhysicalInfraReservoirsValidation;

@Transactional
@Service
public class PhysicalInfraReservoirsService {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraReservoirsService.class);
	@Autowired
	PhysicalInfraReservoirsDao physicalInfraReservoirsDao;
	@Autowired
	PhysicalInfraReservoirsValidation physicalInfraReservoirsValidation;

	/**
	 * ::::Adding physicalInfraStorageReservoirsModel value::::
	 * 
	 * @param physicalInfraStorageReservoirsModel
	 * @return
	 * @throws Exception
	 */
	public int addPhysicalInfraReservoirsService(
			PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel, HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraReservoirsService -- START");
		}
		physicalInfraReservoirsValidation
				.createPhysicalInfraReservoirsValidation(physicalInfraStorageReservoirsModel);
		
		HttpSession httpSession= httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class.cast(httpSession.getAttribute("loginUsersModel2"));

		physicalInfraStorageReservoirsModel.setLoginUsersModel(loginUsersModel);

		/**
		 * Sending values to the DB Section
		 */
		int result = physicalInfraReservoirsDao
				.addPhysicalInfraresivors(physicalInfraStorageReservoirsModel);

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraReservoirsService -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraStorageReservoirsModel> fetchPhysicalInfraReservoirs(HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStorageReservoirs -- START");
		}
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		LinkedList<PhysicalInfraStorageReservoirsModel> physicalInfraStorageReservoirsModels = physicalInfraReservoirsDao
				.fetchPhysicalInfraStorageResivors(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStorageReservoirs -- END");
		}

		return physicalInfraStorageReservoirsModels;

	}

	public PhysicalInfraStorageReservoirsModel fetchPhysicalInfraStorageReservoirsById(
			int phyInfra_sr_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStorageReservoirsById -- START");
		}

		PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel = physicalInfraReservoirsDao
				.fetchPhysicalInfraStorageReservoirsById(phyInfra_sr_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraStorageReservoirsById -- END");
		}

		return physicalInfraStorageReservoirsModel;

	}

	public int deletePhysicalInfraStorageReservoirseById(int phyInfra_sr_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraStorageReservoirseById -- START");
		}

		int result = physicalInfraReservoirsDao.commonDelete(
				"physical_infra_storage_reservoirs", "phyInfra_sr_id",
				phyInfra_sr_id);

		if (logger.isInfoEnabled()) {
			logger.info("deletePhysicalInfraStorageReservoirseById -- END");
		}

		return result;

	}

	public int updatePhysicalInfraStorageReservoirs(
			PhysicalInfraStorageReservoirsModel physicalInfraStorageReservoirsModel, HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraStorageReservoirs -- START");
		}
		
		physicalInfraReservoirsValidation
		.createPhysicalInfraReservoirsValidation(physicalInfraStorageReservoirsModel);
		
		HttpSession httpSession= httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class.cast(httpSession.getAttribute("loginUsersModel2"));

		physicalInfraStorageReservoirsModel.setLoginUsersModel(loginUsersModel);

		int result = physicalInfraReservoirsDao
				.updatePhysicalInfraStorageReservoirs(physicalInfraStorageReservoirsModel);
		
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraStorageReservoirs -- END");
		}

		return result;

	}
	public LinkedList<PhysicalInfraStorageReservoirsModel> fetchAJAXPhysicalInfraStorageReservoirs(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStorageReservoirs-- START");
		}

		LinkedList<PhysicalInfraStorageReservoirsModel> physicalInfraStorageReservoirsModels = physicalInfraReservoirsDao
				.fetchAJAXPhysicalInfraStorageReservoirs(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXPhysicalInfraStorageReservoirs -- END");
		}

		return physicalInfraStorageReservoirsModels;

	}

}
