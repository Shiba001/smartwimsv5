package com.orissa.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.common.Util;
import com.orissa.dao.ChangePasswordDao;
import com.orissa.model.ChangePasswordModel;
import com.orissa.validation.ChangePasswordValidation;

@Transactional
@Service
public class ChangePasswordService {

	private static Logger logger = Logger
			.getLogger(ChangePasswordService.class);

	@Autowired
	private ChangePasswordDao changePasswordDao;
	
	@Autowired
	private ChangePasswordValidation changePasswordValidation;

	public int checkOldPassword(String oldPassword, String newPassword,
			int userId, String confirmNewPassword) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("checkOldPassword -- START");
		}
		
		ChangePasswordModel changePasswordModel = new ChangePasswordModel();
		
		changePasswordModel.setOldPassword(Util.decPass(oldPassword));
		changePasswordModel.setNewPassword(Util.decPass(newPassword));
		changePasswordModel.setConfirmNewPassword(Util.decPass(confirmNewPassword));
		
		changePasswordValidation.changePassword(changePasswordModel);
		
		int result = changePasswordDao.checkOldPassword(changePasswordModel.getOldPassword(),
				changePasswordModel.getNewPassword(), userId);

		if (logger.isInfoEnabled()) {
			logger.info("checkOldPassword -- END");
		}

		return result;

	}
}
