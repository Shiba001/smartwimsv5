package com.orissa.service;

import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.ULBDao;
import com.orissa.model.ULBModel;

@Transactional
@Service
public class ULBService {

	private static Logger logger = Logger.getLogger(ULBService.class);

	@Autowired
	private ULBDao ulbDao;

	public LinkedList<ULBModel> fetchULBByAmrutType(int amrutTypeId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByAmrutType -- START");
		}

		LinkedList<ULBModel> ulbModels = ulbDao
				.fetchULBByAmrutType(amrutTypeId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByAmrutType -- END");
		}

		return ulbModels;

	}

	public LinkedList<ULBModel> fetchAllULB() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAllULB -- START");
		}

		LinkedList<ULBModel> ulbModels = ulbDao.fetchAllULB();

		if (logger.isInfoEnabled()) {
			logger.info("fetchAllULB -- END");
		}

		return ulbModels;

	}

	public LinkedList<ULBModel> fetchULBByDistrictId(int districtId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDistrictId -- START");
		}

		LinkedList<ULBModel> ulbModels = ulbDao
				.fetchULBByDistrictId(districtId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDistrictId -- END");
		}

		return ulbModels;

	}
	
	public LinkedList<ULBModel> fetchULBByDistrictIdAndAmrut(int districtId, int amrutTypeId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDistrictIdAndAmrut -- START");
		}

		LinkedList<ULBModel> ulbModels = ulbDao
				.fetchULBByDistrictIdAndAmrut(districtId, amrutTypeId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDistrictIdAndAmrut -- END");
		}

		return ulbModels;

	}

	public LinkedList<ULBModel> fetchULBByDivisionId(int divisionId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDivisionId -- START");
		}

		LinkedList<ULBModel> ulbModels = ulbDao
				.fetchULBByDivisionId(divisionId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDivisionId -- END");
		}

		return ulbModels;

	}
	
	public LinkedList<ULBModel> fetchULBByDivisionIdAndAmrut(int divisionId, int amrutTypeId)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDivisionIdAndAmrut -- START");
		}

		LinkedList<ULBModel> ulbModels = ulbDao
				.fetchULBByDivisionIdAndAmrut(divisionId, amrutTypeId);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByDivisionIdAndAmrut -- END");
		}

		return ulbModels;

	}

	public ULBModel fetchULBByULBId(int ulbid) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByULBId -- START");
		}

		ULBModel ulbModel = ulbDao.fetchULBByULBId(ulbid);

		if (logger.isInfoEnabled()) {
			logger.info("fetchULBByULBId -- END");
		}

		return ulbModel;

	}

}
