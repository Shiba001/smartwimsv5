package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.DailyWaterTestingDao;
import com.orissa.model.DailyWaterTestingModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.validation.DailyWaterTestingValidation;

@Transactional
@Service
public class DailyWaterTestingService {

	private static Logger logger = Logger
			.getLogger(DailyWaterTestingService.class);

	@Autowired
	private DailyWaterTestingDao dailyWaterTestingDao;

	@Autowired
	private DailyWaterTestingValidation dailyWaterTestingValidation;

	public int addDailyWaterTesting(
			DailyWaterTestingModel dailyWaterTestingModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterTesting -- START");
		}
		dailyWaterTestingValidation
				.createDailyWaterTestingValidation(dailyWaterTestingModel);
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		dailyWaterTestingModel.setNo_bact_samp_defet(dailyWaterTestingModel
				.getNo_bact_samp_tested()
				- dailyWaterTestingModel.getNo_bact_samp_found_ok());
		dailyWaterTestingModel.setNo_other_samp_defet(dailyWaterTestingModel
				.getNo_other_samp_tested()
				- dailyWaterTestingModel.getNo_other_samp_found_ok());
		dailyWaterTestingModel.setLoginUsersModel(loginUsersModel);

		/*if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4)
			dailyWaterTestingModel.setIsApproved(0); // To be Changed
		else*/
			dailyWaterTestingModel.setIsApproved(0); // To be Changed

		int result = dailyWaterTestingDao
				.addDailyWaterTesting(dailyWaterTestingModel);

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterTesting -- END");
		}

		return result;

	}

	public LinkedList<DailyWaterTestingModel> fetchDailyWaterTesting(
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTesting -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dailyWaterTestingDao
				.fetchDailyWaterTesting(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTesting -- END");
		}

		return dailyWaterTestingModels;

	}

	public DailyWaterTestingModel fetchDailyWaterTestingById(int wt_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTestingById -- START");
		}

		DailyWaterTestingModel dailyWaterTestingModel = dailyWaterTestingDao
				.fetchDailyWaterTestingById(wt_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterTestingById -- END");
		}

		return dailyWaterTestingModel;

	}

	public int deleteDailyWaterTestingById(int wt_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterTestingById -- START");
		}

		int result = dailyWaterTestingDao.commonDelete("daily_water_testing",
				"wt_id", wt_id);

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterTestingById -- END");
		}

		return result;

	}

	public int isApprovedUpdate(int wt_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		int result = dailyWaterTestingDao.isApprovedUpdate(
				"daily_water_testing", "wt_id", wt_id);

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return result;

	}
	
	public int isNotApprovedUpdate(int wt_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- START");
		}

		int result = dailyWaterTestingDao.isNotApprovedUpdate(
				"daily_water_testing", "wt_id", wt_id);

		if (logger.isInfoEnabled()) {
			logger.info("isNotApprovedUpdate -- END");
		}

		return result;

	}

	public int updateDailyWaterTesting(
			DailyWaterTestingModel dailyWaterTestingModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterTesting -- START");
		}

		dailyWaterTestingValidation
				.createDailyWaterTestingValidation(dailyWaterTestingModel);
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		dailyWaterTestingModel.setNo_bact_samp_defet(dailyWaterTestingModel
				.getNo_bact_samp_tested()
				- dailyWaterTestingModel.getNo_bact_samp_found_ok());
		dailyWaterTestingModel.setNo_other_samp_defet(dailyWaterTestingModel
				.getNo_other_samp_tested()
				- dailyWaterTestingModel.getNo_other_samp_found_ok());
		dailyWaterTestingModel.setLoginUsersModel(loginUsersModel);

		int result = dailyWaterTestingDao
				.updateDailyWaterTesting(dailyWaterTestingModel);

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterTesting -- END");
		}

		return result;

	}

	public LinkedList<DailyWaterTestingModel> fetchAJAXDailyWaterTesting(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterTesting -- START");
		}

		LinkedList<DailyWaterTestingModel> dailyWaterTestingModels = dailyWaterTestingDao
				.fetchAJAXDailyWaterTesting(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterTesting -- END");
		}

		return dailyWaterTestingModels;

	}
}
