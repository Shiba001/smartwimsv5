package com.orissa.service;

import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.DivisionDao;
import com.orissa.model.PhDivisionModel;

@Transactional
@Service
public class DivisionService {

	private static Logger logger = Logger.getLogger(DivisionService.class);

	@Autowired
	private DivisionDao divisionDao;

	public LinkedList<PhDivisionModel> fetchDivision() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDivision -- START");
		}

		LinkedList<PhDivisionModel> phDivisionModels = divisionDao
				.fetchDivision();

		if (logger.isInfoEnabled()) {
			logger.info("fetchDivision -- END");
		}

		return phDivisionModels;

	}
	
	public LinkedList<PhDivisionModel> fetchDivisionByAmrut() throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDivisionByAmrut -- START");
		}

		LinkedList<PhDivisionModel> phDivisionModels = divisionDao
				.fetchDivisionByAmrut();

		if (logger.isInfoEnabled()) {
			logger.info("fetchDivisionByAmrut -- END");
		}

		return phDivisionModels;

	}
}
