package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.PhysicalInfraElectricalInstallationDao;
import com.orissa.dao.ULBNameDao;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraElectricalInstallationsModel;
import com.orissa.model.ULBNameModel;

@Transactional
@Service
public class PhysicalInfraElectricalInstallationService {

	private static Logger logger = Logger
			.getLogger(PhysicalInfraElectricalInstallationService.class);

	@Autowired
	private PhysicalInfraElectricalInstallationDao physicalInfraElectricalInstallationDao;

	@Autowired
	ULBNameDao ulbNameDao;
	
	public int addPhysElectInstallation(
			PhysicalInfraElectricalInstallationsModel physicalInfraElectricalInstallationsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysElectInstallation -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		int tempResult = 0;
		int result = 0;

		if (physicalInfraElectricalInstallationsModel
				.getElectricalInstallationsModels().size() > 0) {

			for (PhysicalInfraElectricalInstallationsModel physicalInfraElectricalInstallationsModel2 : physicalInfraElectricalInstallationsModel
					.getElectricalInstallationsModels()) {

				physicalInfraElectricalInstallationsModel2
						.setLoginUsersModel(loginUsersModel);
				tempResult = tempResult
						+ physicalInfraElectricalInstallationDao
								.addPhysElectInstallation(physicalInfraElectricalInstallationsModel2);
			}

			if (tempResult == physicalInfraElectricalInstallationsModel
					.getElectricalInstallationsModels().size()) {

				System.out.println("All Rows Inserted Successfully");
				result = 1;
			} else {

				System.out.println("Some Rows Are Missing");
				result = 0;
			}
		}

		if (logger.isInfoEnabled()) {
			logger.info("addPhysElectInstallation -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraElectricalInstallationsModel> fetchElectricInstallation(
			int UlbId, String date_of_report) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchElectricInstallation -- START");
		}

		LinkedList<PhysicalInfraElectricalInstallationsModel> physicalInfraElectricalInstallationsModelList = physicalInfraElectricalInstallationDao
				.fetchElectricInstallation(UlbId, date_of_report);

		if (logger.isInfoEnabled()) {
			logger.info("fetchElectricInstallation -- END");
		}

		return physicalInfraElectricalInstallationsModelList;

	}

	public LinkedList<ULBNameModel> fetchUlbS(ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchUlbS -- START");
		}

		LinkedList<ULBNameModel> ulbNameModels = null;
		if(listingModel.getDivisionId() == null){
			ulbNameModels = ulbNameDao.fetchAllULBName();
		}
		else if(listingModel.getDistrictId() == null){
			ulbNameModels = ulbNameDao.fetchULBNameByDivisionId(listingModel.getDivisionId());
		}
		else if(listingModel.getUlbId() == null){
			ulbNameModels = ulbNameDao.fetchULBNameByDistrictId(listingModel.getDistrictId());
		}
		else{
			ulbNameModels = ulbNameDao.fetchULBNameByULBId(listingModel.getUlbId());
		}

		if (logger.isInfoEnabled()) {
			logger.info("fetchUlbS -- END");
		}

		return ulbNameModels;

	}
	
	public int updatePhysElectInstallation(
			PhysicalInfraElectricalInstallationsModel physicalInfraElectricalInstallationsModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysElectInstallation -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		int tempResult = 0;
		int result = 0;

		
		if (physicalInfraElectricalInstallationsModel.getElectricalInstallationsModels().size() > 0) {

			int deleteResult = physicalInfraElectricalInstallationDao
					.deletePhysElectInstallation(physicalInfraElectricalInstallationsModel
							.getElectricalInstallationsModels().get(0));

			if (deleteResult == 1) {
				System.out.println("Delete Successfull");
				for (PhysicalInfraElectricalInstallationsModel physicalInfraElectricalInstallationsModel2 : physicalInfraElectricalInstallationsModel
						.getElectricalInstallationsModels()) {

					physicalInfraElectricalInstallationsModel2
							.setLoginUsersModel(loginUsersModel);
					tempResult = tempResult
							+ physicalInfraElectricalInstallationDao
									.addPhysElectInstallation(physicalInfraElectricalInstallationsModel2);
					
				}
				
			} else {
				System.out.println("Delete UnSuccessfull");
			}

			

			if (tempResult == physicalInfraElectricalInstallationsModel
					.getElectricalInstallationsModels().size()) {

				System.out.println("All Rows Inserted Successfully");
				result = 1;
			} else {

				System.out.println("Some Rows Are Missing");
				result = 0;
			}
		}

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysElectInstallation -- END");
		}

		return result;

	}
}
