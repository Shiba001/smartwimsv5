package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.PhysicalInfraTreatmentPlantDao;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.PhysicalInfraWaterTreatmentPlantModel;
import com.orissa.model.ULBNameModel;
import com.orissa.validation.PhysicalInfraTreatmentPlantValidation;

@Transactional
@Service
public class PhysicalInfraTreatmentPlantService {
	private static Logger logger = Logger
			.getLogger(PhysicalInfraTreatmentPlantService.class);

	@Autowired
	private PhysicalInfraTreatmentPlantDao physicalInfraTreatmentPlantDao;
	
	@Autowired
	PhysicalInfraTreatmentPlantValidation physicalInfraTreatmentPlantValidation;

	public int addPhysicalInfraTreatmentPlant(PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel, HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTreatmentPlant -- START");
		}
		physicalInfraTreatmentPlantValidation.createPhysicalInfraTreatmentPlantValidation(physicalInfraWaterTreatmentPlantModel);
		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		physicalInfraWaterTreatmentPlantModel.setLoginUsersModel(loginUsersModel);
		
		int result = physicalInfraTreatmentPlantDao
				.addPhysicalInfraTreatment(physicalInfraWaterTreatmentPlantModel);

		if (logger.isInfoEnabled()) {
			logger.info("addPhysicalInfraTreatmentPlant -- END");
		}

		return result;

	}

	public LinkedList<PhysicalInfraWaterTreatmentPlantModel> fetchPhysicalInfraWaterTreatmentPlant(HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraWaterTreatmentPlant -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		
		LinkedList<PhysicalInfraWaterTreatmentPlantModel> physicalInfraWaterTreatmentPlantModels = physicalInfraTreatmentPlantDao
				.fetchPhysicalInfraTreatment(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraWaterTreatmentPlant -- END");
		}

		return physicalInfraWaterTreatmentPlantModels;

	}

	public PhysicalInfraWaterTreatmentPlantModel fetchPhysicalInfraWaterTreatmentPlantById(int wtpid)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraWaterTreatmentPlantById -- START");
		}

		PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel = physicalInfraTreatmentPlantDao
				.fetchPhysicalInfraTreatmentById(wtpid);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraWaterTreatmentPlantById -- END");
		}

		return physicalInfraWaterTreatmentPlantModel;

	}
	
	public int deletePhysicalInfraWaterTreatmentPlantById(int wtpid)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraWaterTreatmentPlantById -- START");
		}

		int result = physicalInfraTreatmentPlantDao
				.commonDelete("physical_infra_water_treatment_plant", "wtpid", wtpid);

		if (logger.isInfoEnabled()) {
			logger.info("fetchPhysicalInfraWaterTreatmentPlantById -- END");
		}

		return result;

	}

	public int updatePhysicalInfraWaterTreatmentPlant(
			PhysicalInfraWaterTreatmentPlantModel physicalInfraWaterTreatmentPlantModel, HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraWaterTreatmentPlant -- START");
		}
		physicalInfraTreatmentPlantValidation.createPhysicalInfraTreatmentPlantValidation(physicalInfraWaterTreatmentPlantModel);
		
		HttpSession httpSession = httpServletRequest.getSession();
		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		physicalInfraWaterTreatmentPlantModel.setLoginUsersModel(loginUsersModel);
		
		int result = physicalInfraTreatmentPlantDao
				.updatePhysicalInfraTreatment(physicalInfraWaterTreatmentPlantModel);
		if (logger.isInfoEnabled()) {
			logger.info("updatePhysicalInfraWaterTreatmentPlant -- END");
		}

		return result;

	}

	
	public LinkedList<PhysicalInfraWaterTreatmentPlantModel> fetchAJAXPhysicalInfraTreat(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- START");
		}

		LinkedList<PhysicalInfraWaterTreatmentPlantModel> physicalInfraWaterTreatmentPlantModel = physicalInfraTreatmentPlantDao
				.fetchAJAXPhysicalInfraTreat(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- END");
		}

		return physicalInfraWaterTreatmentPlantModel;

	}

}
