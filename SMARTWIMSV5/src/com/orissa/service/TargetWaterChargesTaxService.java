package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.TargetWaterChargesTaxDao;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.model.TargetModel;
import com.orissa.validation.TargetWaterChargesTaxValidation;

@Transactional
@Service
public class TargetWaterChargesTaxService {

	private static Logger logger = Logger
			.getLogger(TargetWaterChargesTaxService.class);

	@Autowired
	private TargetWaterChargesTaxDao targetWaterChargesTaxDao;

	@Autowired
	private TargetWaterChargesTaxValidation targetWaterChargesTaxValidation;

	public int addTargetWaterChargesTax(TargetModel targetModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addTargetWaterChargesTax -- START");
		}
		
		targetWaterChargesTaxValidation
				.createTargetWaterChargesTaxValidation(targetModel);


		HttpSession httpSession= httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class.cast(httpSession.getAttribute("loginUsersModel2"));

		targetModel.setLoginUsersModel(loginUsersModel);
		
		targetModel.setProjects("WaterSupply");
		targetModel
				.setProj_entity("Quantity of water to be supplied by PWS (in ML)");
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4)
			targetModel.setIsApproved(0); // To be Changed
		else
			targetModel.setIsApproved(1); // To be Changed
		
		if ("Yearly".equals(targetModel.getTrgt_mode())) {
			
			double yearTarget = targetModel.getY_trgt();
			double monthTarget = yearTarget / 12;

			targetModel.setM_trgt_1(monthTarget);
			targetModel.setM_trgt_2(monthTarget);
			targetModel.setM_trgt_3(monthTarget);
			targetModel.setM_trgt_4(monthTarget);
			targetModel.setM_trgt_5(monthTarget);
			targetModel.setM_trgt_6(monthTarget);
			targetModel.setM_trgt_7(monthTarget);
			targetModel.setM_trgt_8(monthTarget);
			targetModel.setM_trgt_9(monthTarget);
			targetModel.setM_trgt_10(monthTarget);
			targetModel.setM_trgt_11(monthTarget);
			targetModel.setM_trgt_12(monthTarget);
		}else{
			targetModel.setY_trgt(targetModel.getM_trgt_1() + targetModel.getM_trgt_2() + targetModel.getM_trgt_3() + targetModel.getM_trgt_4() + targetModel.getM_trgt_5() + targetModel.getM_trgt_6() + targetModel.getM_trgt_7() + targetModel.getM_trgt_8() + targetModel.getM_trgt_9() + targetModel.getM_trgt_10() + targetModel.getM_trgt_11() + targetModel.getM_trgt_12());
		}
		
		int result = targetWaterChargesTaxDao
				.addTargetWaterChargesTax(targetModel);

		if (logger.isInfoEnabled()) {
			logger.info("addTargetWaterChargesTax -- END");
		}

		return result;

	}

	public LinkedList<TargetModel> fetchTargetWaterChargesTax(HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTax -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));
		
		LinkedList<TargetModel> targetModels = targetWaterChargesTaxDao
				.fetchTargetWaterChargesTax(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTax -- END");
		}

		return targetModels;

	}

	public TargetModel fetchTargetWaterChargesTaxById(int trgt_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTaxById -- START");
		}

		TargetModel targetModel = targetWaterChargesTaxDao
				.fetchTargetWaterChargesTaxById(trgt_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchTargetWaterChargesTaxById -- END");
		}

		return targetModel;

	}

	public int deleteTargetWaterChargesTaxById(int trgt_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterSupplyById -- START");
		}

		int result = targetWaterChargesTaxDao.commonDelete("target", "trgt_id",
				trgt_id);

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterSupplyById -- END");
		}

		return result;

	}

	public int updateTargetWaterChargesTax(TargetModel targetModel, HttpServletRequest httpServletRequest)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateTargetWaterChargesTax -- START");
		}
		
		targetWaterChargesTaxValidation
				.createTargetWaterChargesTaxValidation(targetModel);
		
		HttpSession httpSession= httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class.cast(httpSession.getAttribute("loginUsersModel2"));

		targetModel.setLoginUsersModel(loginUsersModel);
		
		targetModel.setProjects("WaterSupply");
		targetModel
				.setProj_entity("Quantity of water to be supplied by PWS (in ML)");
		if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4)
			targetModel.setIsApproved(0); // To be Changed
		else
			targetModel.setIsApproved(1); // To be Changed
		
		if ("Yearly".equals(targetModel.getTrgt_mode())) {
			
			double yearTarget = targetModel.getY_trgt();
			double monthTarget = yearTarget / 12;

			targetModel.setM_trgt_1(monthTarget);
			targetModel.setM_trgt_2(monthTarget);
			targetModel.setM_trgt_3(monthTarget);
			targetModel.setM_trgt_4(monthTarget);
			targetModel.setM_trgt_5(monthTarget);
			targetModel.setM_trgt_6(monthTarget);
			targetModel.setM_trgt_7(monthTarget);
			targetModel.setM_trgt_8(monthTarget);
			targetModel.setM_trgt_9(monthTarget);
			targetModel.setM_trgt_10(monthTarget);
			targetModel.setM_trgt_11(monthTarget);
			targetModel.setM_trgt_12(monthTarget);
		} else {
			targetModel.setY_trgt(targetModel.getM_trgt_1() + targetModel.getM_trgt_2() + targetModel.getM_trgt_3() + targetModel.getM_trgt_4() + targetModel.getM_trgt_5() + targetModel.getM_trgt_6() + targetModel.getM_trgt_7() + targetModel.getM_trgt_8() + targetModel.getM_trgt_9() + targetModel.getM_trgt_10() + targetModel.getM_trgt_11() + targetModel.getM_trgt_12());
		}

		int result = targetWaterChargesTaxDao
				.updateTargetWaterChargesTax(targetModel);

		if (logger.isInfoEnabled()) {
			logger.info("updateTargetWaterChargesTax -- END");
		}

		return result;

	}
	
	public LinkedList<TargetModel> fetchAJAXTargetWaterChargesTax(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXTargetWaterChargesTax -- START");
		}

		LinkedList<TargetModel> targetModels = targetWaterChargesTaxDao
				.fetchAJAXTargetWaterChargesTax(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXTargetWaterChargesTax -- END");
		}

		return targetModels;

	}

}
