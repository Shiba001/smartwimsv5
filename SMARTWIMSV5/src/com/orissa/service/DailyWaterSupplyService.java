package com.orissa.service;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orissa.dao.DailyWaterSupplyDao;
import com.orissa.model.DailyWaterSupplyModel;
import com.orissa.model.ListingModel;
import com.orissa.model.LoginUsersModel;
import com.orissa.validation.DailyWaterSupplyValidation;

@Transactional
@Service
public class DailyWaterSupplyService {

	private static Logger logger = Logger
			.getLogger(DailyWaterSupplyService.class);

	@Autowired
	private DailyWaterSupplyDao dailyWaterSupplyDao;

	@Autowired
	private DailyWaterSupplyValidation dailyWaterSupplyValidation;

	public int addDailyWaterSupply(DailyWaterSupplyModel dailyWaterSupplyModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterSupply -- START");
		}

		dailyWaterSupplyValidation
				.createDailyWaterSupplyValidation(dailyWaterSupplyModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		Double rateOfSupply = (dailyWaterSupplyModel.getQlty_supply() * 1000000)
				/ dailyWaterSupplyModel.getPop_served();

		dailyWaterSupplyModel.setRate_of_supply(rateOfSupply); // To be Changed
		dailyWaterSupplyModel.setLoginUsersModel(loginUsersModel);
		dailyWaterSupplyModel.setSurplus_deficit(dailyWaterSupplyModel
				.getQlty_supply() - dailyWaterSupplyModel.getTot_demand());

		/*if (loginUsersModel.getUserLevelModel().getUser_level_id() == 4)
			dailyWaterSupplyModel.setIsApproved(0); // To be Changed
		else*/
			dailyWaterSupplyModel.setIsApproved(0); // To be Changed

		int result = dailyWaterSupplyDao
				.addDailyWaterSupply(dailyWaterSupplyModel);

		if (logger.isInfoEnabled()) {
			logger.info("addDailyWaterSupply -- END");
		}

		return result;

	}

	public LinkedList<DailyWaterSupplyModel> fetchDailyWaterSupply(
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupply -- START");
		}

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dailyWaterSupplyDao
				.fetchDailyWaterSupply(loginUsersModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupply -- END");
		}

		return dailyWaterSupplyModels;

	}

	public DailyWaterSupplyModel fetchDailyWaterSupplyById(int ws_id)
			throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyById -- START");
		}

		DailyWaterSupplyModel dailyWaterSupplyModel = dailyWaterSupplyDao
				.fetchDailyWaterSupplyById(ws_id);

		if (logger.isInfoEnabled()) {
			logger.info("fetchDailyWaterSupplyById -- END");
		}

		return dailyWaterSupplyModel;

	}

	public int deleteDailyWaterSupplyById(int ws_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterSupplyById -- START");
		}

		int result = dailyWaterSupplyDao.commonDelete("daily_water_supply",
				"ws_id", ws_id);

		if (logger.isInfoEnabled()) {
			logger.info("deleteDailyWaterSupplyById -- END");
		}

		return result;

	}

	public int updateDailyWaterSupply(
			DailyWaterSupplyModel dailyWaterSupplyModel,
			HttpServletRequest httpServletRequest) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterSupply -- START");
		}

		dailyWaterSupplyValidation
				.createDailyWaterSupplyValidation(dailyWaterSupplyModel);

		HttpSession httpSession = httpServletRequest.getSession();

		LoginUsersModel loginUsersModel = LoginUsersModel.class
				.cast(httpSession.getAttribute("loginUsersModel2"));

		Double rateOfSupply = (dailyWaterSupplyModel.getQlty_supply() * 1000000)
				/ dailyWaterSupplyModel.getPop_served();

		dailyWaterSupplyModel.setRate_of_supply(rateOfSupply); // To be Changed
		dailyWaterSupplyModel.setLoginUsersModel(loginUsersModel);
		dailyWaterSupplyModel.setSurplus_deficit(dailyWaterSupplyModel
				.getQlty_supply() - dailyWaterSupplyModel.getTot_demand());

		int result = dailyWaterSupplyDao
				.updateDailyWaterSupply(dailyWaterSupplyModel);

		if (logger.isInfoEnabled()) {
			logger.info("updateDailyWaterSupply -- END");
		}

		return result;

	}

	public int isApprovedUpdate(int ws_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		int result = dailyWaterSupplyDao.isApprovedUpdate("daily_water_supply",
				"ws_id", ws_id);

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return result;

	}
	
	public int isNotApprovedUpdate(int ws_id) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- START");
		}

		int result = dailyWaterSupplyDao.isNotApprovedUpdate("daily_water_supply",
				"ws_id", ws_id);

		if (logger.isInfoEnabled()) {
			logger.info("isApprovedUpdate -- END");
		}

		return result;

	}

	public LinkedList<DailyWaterSupplyModel> fetchAJAXDailyWaterSupply(
			ListingModel listingModel) throws Exception {

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- START");
		}

		LinkedList<DailyWaterSupplyModel> dailyWaterSupplyModels = dailyWaterSupplyDao
				.fetchAJAXDailyWaterSupply(listingModel);

		if (logger.isInfoEnabled()) {
			logger.info("fetchAJAXDailyWaterSupply -- END");
		}

		return dailyWaterSupplyModels;

	}
}
