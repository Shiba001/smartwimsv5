$(document).ready(
		function() {
			$(".resetAll").click(function(){
					
					$.ajax({
						url : "./entryHeaderMenu/free-amrut",
						async: false,
						error : function(XMLHttpRequest, textStatus,
								errorThrown) {
							console.log('status:' + XMLHttpRequest.status
									+ ', status text: '
									+ XMLHttpRequest.statusText);
						},
						success : function(dists) {
							//alert("Success");
						}
					});
					
					location.reload();
					
				});
			
			
			
			$("#divisionId").change(
					function() {
						
						var divisionId = $(this).val();
						if(divisionId == "ALL"){
							$("#disSelect").html(
							"<option>District</option>");
							$("#ulbSelect").html("<option>ULB</option>");
							getDataEntryStatus();
							return true;
						}
						$.ajax({
							url : "./reportHeaderMenu/distList?divisionId="
									+ divisionId,
							async: false,
							error : function(XMLHttpRequest, textStatus,
									errorThrown) {
								console.log('status:' + XMLHttpRequest.status
										+ ', status text: '
										+ XMLHttpRequest.statusText);
								$("#disSelect").html(
										"<option>District</option>");
								$("#ulbSelect").html("<option>ULB</option>");
								getDataEntryStatus();
							},
							success : function(dists) {
								var distoptions = "<option>District</option>";
								$.each(dists, function(index, element) {
									distoptions += "<option value=\""
											+ element.Dist_id + "\">"
											+ element.Dist_Name + "</option>";
								});

								$("#disSelect").html(distoptions);
								$("#ulbSelect").html("<option>ULB</option>");
								
								if((userLevelId == 3 || userLevelId == 4) || isUperlevel == "true"){
									/**
									 * 3 => District
									 * 4 => Urban Local Body
									 */
									$("#disSelect option[value='"+ distId +"']").prop("selected", true);
								}
								if(!($("#disSelect").val() == "District")){
									$("#disSelect").change();
									
								}
								else{
									getDataEntryStatus();
								}
								
							}
						});
					});

			$("#disSelect").change(
					function() {
						
						var districtId = $(this).val();
						$.ajax({
							url : "./reportHeaderMenu/ULBList?districtId="
									+ districtId,
							async: false,
							error : function(XMLHttpRequest, textStatus,
									errorThrown) {
								console.log('status:' + XMLHttpRequest.status
										+ ', status text: '
										+ XMLHttpRequest.statusText);
								$("#ulbSelect").html("<option>ULB</option>");
								getDataEntryStatus();
							},
							success : function(ULBs) {
								
								var distoptions = "<option>ULB</option>";
								$.each(ULBs, function(index, element) {
									distoptions += "<option value=\""
											+ element.m_ulb_id + "\">"
											+ element.ulb_name + "</option>";
									//console.log(element.ulb_name);
								});

								$("#ulbSelect").html(distoptions);
								
								if(userLevelId == 4 || isUperlevel == "true"){
									/**
									 * 4 => Urban Local Body
									 */
									$("#ulbSelect option[value='"+ ulbId +"']").prop("selected", true);
									if(!($("#ulbSelect").val() == "ULB")){
										$("#ulbSelect").change();
										
									}
									else{
										getDataEntryStatus();
									}
									
								}
								else{
									if(($("#ulbSelect").val() == "ULB")){
										getDataEntryStatus();
									}
								}
							}
						});
					});
			
			$("#ulbSelect").change(function(){
				getDataEntryStatus();
				
				var form = $("button:contains('Clear'),button:contains('Back')").closest("form");
				$(".dynamiULB").remove();
				if($(this).val() != "ULB"){
					jQuery('<input/>', {
					    type: 'hidden',
					    name: 'ulbId',
					    class: 'dynamiULB',
					    value: $(this).val()
					}).appendTo(form);
				}
			});
			$("button:contains('Clear'),button:contains('Back')").closest("form").submit(function(event){
				if($("#ulbSelect").val() == "ULB"){
					event.preventDefault()
					jQuery.growl.error({
						message : "Please select proper ULB"
					});
				}
				
			});
			
			
			var distId = $("script[src$='dashboard-header.js']").attr("data-Dist-id");
			var ulbId = $("script[src$='dashboard-header.js']").attr("data-ulb-id");
			var userLevelId = $("script[src$='dashboard-header.js']").attr("data-user-level-id");
			var isUperlevel = $("script[src$='dashboard-header.js']").attr("data-is-uperlevel");
			$("#divisionId").change();
			
			
			var pathName = window.location.pathname;
			var pathNameArr = pathName.split("/");
			var controllerPath = pathNameArr[pathNameArr.length - 1];
			var isDisabled = true;
			if(controllerPath == "dashboard"){
				isDisabled = false;
				$("#divisionId option:nth-child(1)").prop("selected", true);
				$("#disSelect").html("<option>District</option>");
				$("#ulbSelect").html("<option>ULB</option>");
			}
			
			switch (Number(userLevelId)) {
			case 4:
				$("#ulbSelect").prop("disabled", isDisabled);
				//$("#ulbSelect").change();
			case 3:
				$("#disSelect").prop("disabled", isDisabled);
			case 2:
				$("#divisionId").prop("disabled", isDisabled);
				if(isDisabled != false)
					$("#dashboard_date").change();
			}
			
			function changeLocationName(){
				if($("#ulbSelect").val() != "ULB"){
					$("#area").html($("#ulbSelect option:selected").text());
				}
				else if($("#disSelect").val() != "District"){
					$("#area").html($("#disSelect option:selected").text());
				}
				else if($("#divisionId").val() != "ALL"){
					$("#area").html($("#divisionId option:selected").text());
				}
				else{
					$("#area").html("Odisha");
				}
			}
			changeLocationName();
			
			
			$(document).on("click", ".approval", function() {
				var dataUrl = $(this).attr("data-url");
				var that = this;
				$.ajax({
					url : dataUrl,
					async: false,
					error : function(XMLHttpRequest, textStatus,
							errorThrown) {
						console.log('status:' + XMLHttpRequest.status
								+ ', status text: '
								+ XMLHttpRequest.statusText);
					},
					success : function(ULBs) {
						$(that).closest("tr").find("td:nth-last-child(1)").children("a").each(function(){
							$(this).attr("href", "javascript:void(0)")
							$(this).attr("onclick", "");
						});
						$(that).closest("tr").find("td:nth-last-child(2)").html("<input type=\"checkbox\" checked=\"\" disabled=\"\">Approved");
						jQuery.growl.notice({
							message : "Approved"
						});
					}
				});
			});

		});