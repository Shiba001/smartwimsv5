/**
 * Daily Connection front end va;idation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});

	$('.number_only').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		})); 
	});
});

function dailyConnection() {
	var no_new_res_cons_add = $('#no_new_res_cons_add').val().trim();
	var no_res_cons_disc = $('#no_res_cons_disc').val().trim();
	var no_new_ind_cons_add = $('#no_new_ind_cons_add').val().trim();
	var no_ind_cons_disc = $('#no_ind_cons_disc').val().trim();
	
	var no_new_com_cons_add = $('#no_new_com_cons_add').val().trim();
	var no_com_cons_disc = $('#no_com_cons_disc').val().trim();
	var no_new_inst_cons_add = $('#no_new_inst_cons_add').val().trim();
	var no_inst_cons_disc = $('#no_inst_cons_disc').val().trim();

	var flag = true;

	if (no_new_res_cons_add == null || no_new_res_cons_add == '') {
		jQuery.growl.error({
			message : "No. of New Residential Connections Added can not be blank."
		});
		flag = false;
	} else if (!no_new_res_cons_add.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of New Residential Connections Added."
		});
		flag = false;

	} else if (no_res_cons_disc == null || no_res_cons_disc == '') {
		jQuery.growl.error({
			message : "No. of Residential Connections Disconnected can not be blank."
		});
		flag = false;
	} else if (!no_res_cons_disc.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Residential Connections Disconnected."
		});
		flag = false;
	}

	else if (no_new_ind_cons_add == null || no_new_ind_cons_add == '') {
		jQuery.growl.error({
			message : "No. of New Industrial Connections Added can not be blank."
		});
		flag = false;
	} else if (!no_new_ind_cons_add.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of New Industrial Connections Added."
		});
		flag = false;
	}
	else if (no_ind_cons_disc == null || no_ind_cons_disc == '') {
			jQuery.growl.error({
				message : "No. of Industrial Connections Disconnected can not be blank."
			});
			flag = false;
		} else if (!no_ind_cons_disc.match('^[1-9]([0-9]{0,45}$)')) {
			jQuery.growl.error({
				message : "Invalid number format in No. of Industrial Connections Disconnected."
			});
			flag = false;
		}
	
		else if (no_new_com_cons_add == null || no_new_com_cons_add == '') {
			jQuery.growl.error({
				message : "No. of New Commercial Connections Added can not be blank."
			});
			flag = false;
		} else if (!no_new_com_cons_add.match('^[1-9]([0-9]{0,45}$)')) {
			jQuery.growl.error({
				message : "Invalid number format in No. of New Commercial Connections Added."
			});
			flag = false;
		}
	
		else if (no_com_cons_disc == null || no_com_cons_disc == '') {
			jQuery.growl.error({
				message : "No. of Commercial Connections Disconnected can not be blank."
			});
			flag = false;
		} else if (!no_com_cons_disc.match('^[1-9]([0-9]{0,45}$)')) {
			jQuery.growl.error({
				message : "Invalid number format in No. of Commercial Connections Disconnected."
			});
			flag = false;
		}
	
		else if (no_new_inst_cons_add == null || no_new_inst_cons_add == '') {
			jQuery.growl.error({
				message : "No. of New Institutional Connections Added can not be blank."
			});
			flag = false;
		} else if (!no_new_inst_cons_add.match('^[1-9]([0-9]{0,45}$)')) {
			jQuery.growl.error({
				message : "Invalid number format in No. of New Institutional Connections Added."
			});
			flag = false;
		}
	
		else if (no_inst_cons_disc == null || no_inst_cons_disc == '') {
			jQuery.growl.error({
				message : "No. of New Institutional Connections Disconnected can not be blank."
			});
			flag = false;
		} else if (!no_inst_cons_disc.match('^[1-9]([0-9]{0,45}$)')) {
			jQuery.growl.error({
				message : "Invalid number format in No. of New Institutional Connections Disconnected."
			});
			flag = false;
		}
	

	else {
		flag = true;
	}

	return flag;
}