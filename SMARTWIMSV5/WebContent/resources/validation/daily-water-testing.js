/**
 * Daily water testing front end validation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});

	$('.number_only').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		})); 
	});
});

function dailyWaterTesting() {
	var no_bact_samp_tested = $('#no_bact_samp_tested').val().trim();
	var no_bact_samp_found_ok = $('#no_bact_samp_found_ok').val().trim();
	var no_other_samp_tested = $('#no_other_samp_tested').val().trim();
	var no_other_samp_found_ok = $('#no_other_samp_found_ok').val().trim();
	

	var flag = true;

	if (no_bact_samp_tested == null || no_bact_samp_tested == '') {
		jQuery.growl.error({
			message : "No. of Bacteriological Samples Tested can not be blank."
		});
		flag = false;
	} else if (!no_bact_samp_tested.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Bacteriological Samples Tested."
		});
		flag = false;

	} else if (no_bact_samp_found_ok == null || no_bact_samp_found_ok == '') {
		jQuery.growl.error({
			message : "No. of Bacteriological Samples Found OK can not be blank."
		});
		flag = false;
	} else if (!no_bact_samp_found_ok.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in  No. of Bacteriological Samples Found OK."
		});
		flag = false;
	}
	
	else if (no_other_samp_tested == null || no_other_samp_tested == '') {
		jQuery.growl.error({
			message : "No. of FRC Samples Tested can not be blank."
		});
		flag = false;
	} else if (!no_other_samp_tested.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of FRC Samples Tested."
		});
		flag = false;

	} else if (no_other_samp_found_ok == null || no_other_samp_found_ok == '') {
		jQuery.growl.error({
			message : "No. of FRC Samples Found OK can not be blank."
		});
		flag = false;
	} else if (!no_other_samp_found_ok.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of FRC Samples Found OK."
		});
		flag = false;
	}
	
	else {
		flag = true;
	}

	return flag;
}