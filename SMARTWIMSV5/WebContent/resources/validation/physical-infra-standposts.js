/**
 * Physical Infra Standsposts front-end validation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	$('.number_only').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		}));
	});

});
function physicalInfraStandposts() {
	var no_exst_stanposts = $('#no_exst_stanposts').val().trim();
	var no_standposts_work_cond = $('#no_standposts_work_cond').val().trim();
	var dueDate = $('#dueDate').val().trim();
	
	var flag = true;

	if (no_exst_stanposts == null || no_exst_stanposts == '') {
		jQuery.growl.error({
			message : "No. of Existing Standposts can not be blank."
		});
		flag = false;
	} else if (!no_exst_stanposts.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Existing Standposts."
		});
		flag = false;

	} else if (no_standposts_work_cond == null || no_standposts_work_cond == '') {
		jQuery.growl.error({
			message : "No. of Standposts in Working Condition can not be blank."
		});
		flag = false;
	} else if (!no_standposts_work_cond.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Standposts in Working Condition."
		});
		flag = false;
	}else if (dueDate == null || dueDate == '') {
		jQuery.growl.error({
			message : "Due Date can not be blank."
		});
		flag = false;
	}
	else {
		flag = true;
	}

	return flag;
}