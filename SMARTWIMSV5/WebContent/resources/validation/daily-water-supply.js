/**
 * Daily water supply front-end validation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	$('.number_only').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		}));
	});

});

function dailyWaterSupply() {
	var tot_demand = $('#tot_demand').val().trim();
	var qlty_supply = $('#qlty_supply').val().trim();
	var pop_served = $('#pop_served').val().trim();
	var freq_supply = $('#Freq_supply').val().trim();

	var flag = true;

	if (tot_demand == null || tot_demand == '') {
		jQuery.growl.error({
			message : "Total Demand can not be blank."
		});
		flag = false;
	} else if (!tot_demand.match('^[0-9]*\.[0-9]*$')) {
		jQuery.growl.error({
			message : "Invalid decimal format in Total Demand."
		});
		flag = false;

	} else if (qlty_supply == null || qlty_supply == '') {
		jQuery.growl.error({
			message : "Quantity Supplied by PWS can not be blank."
		});
		flag = false;
	} else if (!qlty_supply.match('^[0-9]*\.[0-9]*$')) {
		jQuery.growl.error({
			message : "Invalid decimal format in Quantity Supplied by PWS."
		});
		flag = false;
	}
	
	else if (pop_served == null || pop_served == '') {
		jQuery.growl.error({
			message : "Population Served by PWS can not be blank."
		});
		flag = false;
	}
	else if (!pop_served.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in Population Served by PWS."
		});
		flag = false;
	}
	else if (freq_supply == null || freq_supply == '') {
		jQuery.growl.error({
			message : "Average Hour of Supply can not be blank."
		});
		flag = false;
	} else if (!freq_supply.match('^[0-9]*\.[0-9]*$')) {
		jQuery.growl.error({
			message : "Invalid decimal format in Average Hour of Supply."
		});
		flag = false;
	}

	else {
		flag = true;
	}

	return flag;
}