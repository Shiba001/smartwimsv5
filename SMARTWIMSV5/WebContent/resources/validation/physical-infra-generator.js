/**
 * Physical Infra Generator front-end validation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	$('.number_only').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		}));
	});

});

function physicalInfraGenerator(){
	//alert("satya generator");
	var tot_no_gen = $('#tot_no_gen').val().trim();
	//alert(tot_no_gen);
	var tot_no_gen_wc = $('#tot_no_gen_wc').val().trim();
	var dueDate = $('#dueDate').val().trim();
	
	var flag = true;
	
	if (tot_no_gen == null || tot_no_gen == '') {
		jQuery.growl.error({
			message : "Total No. of Generators can not be blank."
		});
		
		flag = false;
	} else if (!tot_no_gen.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in Total No. of Generators."
		});
	
		flag = false;
	}
	else if (tot_no_gen_wc == null || tot_no_gen_wc == '') {
		jQuery.growl.error({
			message : "Total No. of Generators in Working Condition can not be blank."
		});
		flag = false;
	} else if (!tot_no_gen_wc.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in Total No. of Generators in Working Condition."
		});	
	
		flag = false;
	}else if (dueDate == null || dueDate == '') {
		jQuery.growl.error({
			message : "Due Date can not be blank."
		});
		flag = false;
	}
	else {
		flag = true;
	}

	return flag;
	
}