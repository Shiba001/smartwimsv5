/**
 *Physical Infra Connection front-end validation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	$('.number_only').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		}));
	});

});

function physicalInfraConnection() {
	var no_res_cons = $('#no_res_cons').val().trim();
	var no_ind_cons = $('#no_ind_cons').val().trim();
	var no_com_cons = $('#no_com_cons').val().trim();
	var no_inst_cons = $('#no_inst_cons').val().trim();
	var dueDate = $('#dueDate').val().trim();

	var flag = true;

	if (no_res_cons == null || no_res_cons == '') {
		jQuery.growl.error({
			message : "No. of Residential Connections Provided can not be blank."
		});
		flag = false;
	} else if (!no_res_cons.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Residential Connections Provided."
		});
		flag = false;

	} else if (no_ind_cons == null || no_ind_cons == '') {
		jQuery.growl.error({
			message : "No. of Industrial Connections Provided can not be blank."
		});
		flag = false;
	} else if (!no_ind_cons.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Industrial Connections Provided."
		});
		flag = false;
	}

	else if (no_com_cons == null || no_com_cons == '') {
		jQuery.growl.error({
			message : "No. of Commercial Connections Provided can not be blank."
		});
		flag = false;
	} 
	else if (!no_com_cons.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Commercial Connections Provided."
		});
		flag = false;
	}
	else if (no_inst_cons == null || no_inst_cons == '') {
		jQuery.growl.error({
			message : "No. of Institutional Connections Provided can not be blank."
		});
		flag = false;
	} 
	else if (!no_inst_cons.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Institutional Connections Provided."
		});
		flag = false;
	}else if (dueDate == null || dueDate == '') {
		jQuery.growl.error({
			message : "Due Date can not be blank."
		});
		flag = false;
	}
	else {
		flag = true;
	}

	return flag;
}