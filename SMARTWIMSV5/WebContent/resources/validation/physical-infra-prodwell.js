/**
 * 
 */
$(document).ready(function() {

	/*$('.decimal_number').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});*/
	$(document).on("keyup",'.only_text',function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^a-zA-Z0-9]/g, function(str) {
			return '';
		}));
	});

});

function physicalInfraProdWells() {
	var list_loc_of_prod_well = document.getElementsByName("list_loc_of_prod_well");
	var list_diam_prod_well = document.getElementsByName("list_diam_prod_well");
	var list_discharge = document.getElementsByName("list_discharge");
	var list_depth = document.getElementsByName("list_depth");
	
	var flag = true;
	
	for (var i = 0; i < list_loc_of_prod_well.length; i++) {
		if (list_loc_of_prod_well[i].name == "list_loc_of_prod_well"
				&& list_loc_of_prod_well[i].value == '') {
			jQuery.growl.error({
				message : "Location of the Production Well can not be blank."
			});
			flag = false;
			break;
		} else if (list_diam_prod_well[i].name == "list_diam_prod_well"
				&& list_diam_prod_well[i].value == '') {
			jQuery.growl.error({
				message : "Diameter of the Production Well can not be blank."
			});
			flag = false;
			break;
		} else if (list_discharge[i].name == "list_discharge"
				&& list_discharge[i].value == '') {
			jQuery.growl.error({
				message : "Discharge can not be blank."
			});
			flag = false;
			break;
		} else if (list_depth[i].name == "list_depth"
				&& list_depth[i].value == '') {
			jQuery.growl.error({
				message : "Depth  can not be blank."
			});
			flag = false;
			break;
		} 
		else {
			flag = true;
		}
		
		
	}
	return flag;
}