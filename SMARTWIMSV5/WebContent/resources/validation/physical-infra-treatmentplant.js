/**
 * Physical Infra Treatmentplant front-end validation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	$('.number_only').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		}));
	});

});

function physicalInfraTreatmentplant() {
	
	var no_of_wtp = $('#no_of_wtp').val().trim();
	var cpty_wtp = $('#cpty_wtp').val().trim();
	var dueDate = $('#dueDate').val().trim();
	
	var flag = true;
	if (no_of_wtp == null || no_of_wtp == '') {
		jQuery.growl.error({
			message : "No. of Water Treatment Plants can not be blank."
		});
		
		flag = false;
	} else if (!no_of_wtp.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Water Treatment Plants."
		});
		
		flag = false;
	} else if (cpty_wtp == null || cpty_wtp == '') {
		jQuery.growl.error({
			message : "Capacity of WTP can not be blank."
		});
		
		flag = false;
	} else if (!cpty_wtp.match('^[0-9]*\.[0-9]*$')) {
		jQuery.growl.error({
			message : "Invalid decimal format in Capacity of WTP."
		});
		
		flag = false;

	}else if (dueDate == null || dueDate == '') {
		jQuery.growl.error({
			message : "Due Date can not be blank."
		});
		flag = false;
	}
	else {
		flag = true;
	}
	return flag;

}