/**
 * 
 */
$(document).ready(function() {

	$(document).on("keyup",'.only_text',function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^a-zA-Z0-9]/g, function(str) {
			return '';
		}));
	});

});

function physicalInfraPumpMotor() {
	var list_loc_of_pum_motor = document.getElementsByName("list_loc_of_pum_motor");
	var list_cap_of_motor = document.getElementsByName("list_cap_of_motor");
	var list_head_dia = document.getElementsByName("list_head_dia");
	var list_discharge = document.getElementsByName("list_discharge");
	
	var list_type_pump = document.getElementsByName("list_type_pump");
	var list_year_inst = document.getElementsByName("list_year_inst");

	var flag = true;
	
	for (var i = 0; i < list_loc_of_pum_motor.length; i++) {
		if (list_loc_of_pum_motor[i].name == "list_loc_of_pum_motor"
				&& list_loc_of_pum_motor[i].value == '') {
			jQuery.growl.error({
				message : "Location of the pump/motor can not be blank."
			});
			flag = false;
			break;
		} else if (list_cap_of_motor[i].name == "list_cap_of_motor"
				&& list_cap_of_motor[i].value == '') {
			jQuery.growl.error({
				message : "Power can not be blank."
			});
			flag = false;
			break;
		} else if (list_head_dia[i].name == "list_head_dia"
				&& list_head_dia[i].value == '') {
			jQuery.growl.error({
				message : "Head can not be blank."
			});
			flag = false;
			break;
		} else if (list_discharge[i].name == "list_discharge"
				&& list_discharge[i].value == '') {
			jQuery.growl.error({
				message : "Discharge  can not be blank."
			});
			flag = false;
			break;
		} 
		
		 else if (list_type_pump[i].name == "list_type_pump"
				&& list_type_pump[i].value == '') {
			jQuery.growl.error({
				message : "Type of Pump & Motor can not be blank."
			});
			flag = false;
			break;
		} else if (list_year_inst[i].name == "list_year_inst"
				&& list_year_inst[i].value == '') {
			jQuery.growl.error({
				message : "Year of Installation can not be blank."
			});
			flag = false;
			break;
		}
		else {
			flag = true;
		}
		
		
	}
	return flag;
}