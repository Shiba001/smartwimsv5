/**
 * Physical Infra Reservoirs front-end validation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	$('.number_only').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		}));
	});

});

function physicalInfraReservoirs() {
	
	var no_grnd_strg_res = $('#no_grnd_strg_res').val().trim();
	var cpty_grnd_strg_res = $('#cpty_grnd_strg_res').val().trim();
	var no_ele_strg_res = $('#no_ele_strg_res').val().trim();
	var cpty_ele_strg_res = $('#cpty_ele_strg_res').val().trim();
	var dueDate = $('#dueDate').val().trim();
	
	var flag = true;

	if (no_grnd_strg_res == null || no_grnd_strg_res == '') {
		jQuery.growl.error({
			message : "No. of Ground Storage Reservoirs can not be blank."
		});
		
		flag = false;
	} else if (!no_grnd_strg_res.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Ground Storage Reservoirs."
		});
		
		flag = false;

	} else if (cpty_grnd_strg_res == null || cpty_grnd_strg_res == '') {
		jQuery.growl.error({
			message : "Capacity of Ground Storage Reservoirs can not be blank."
		});
		
		flag = false;
	} else if (!cpty_grnd_strg_res.match('^[0-9]*\.[0-9]*$')) {
		jQuery.growl.error({
			message : "Invalid decimal format in Capacity of Ground Storage Reservoirs."
		});
		
		flag = false;
	} else if (no_ele_strg_res == null || no_ele_strg_res == '') {
		jQuery.growl.error({
			message : "No. of Elevated Storage Reservoirs can not be blank."
		});
		flag = false;
	} else if (!no_ele_strg_res.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Elevated Storage Reservoirs."
		});
		flag = false;
	} else if (cpty_ele_strg_res == null || cpty_ele_strg_res == '') {
		jQuery.growl.error({
			message : "Capacity of Elevated Storage Reservoirs can not be blank."
		});
		flag = false;
	} else if (!cpty_ele_strg_res.match('^[0-9]*\.[0-9]*$')) {
		jQuery.growl.error({
			message : "Invalid decimal format in Capacity of Elevated Storage Reservoirs."
		});
		flag = false;
	}else if (dueDate == null || dueDate == '') {
		jQuery.growl.error({
			message : "Due Date can not be blank."
		});
		flag = false;
	}
	else {
		flag = true;
	}
	return flag;
}