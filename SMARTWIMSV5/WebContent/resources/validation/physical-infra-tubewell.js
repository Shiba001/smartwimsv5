/**
 * Physical Infra Tubewells front-end validation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	$('.number_only').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		}));
	});

});
function physicalInfraTubewells() {
	var tot_no_tw = $('#tot_no_tw').val().trim();
	var tot_no_tw_wc = $('#tot_no_tw_wc').val().trim();
	var dueDate = $('#dueDate').val().trim();

	var flag = true;

	if (tot_no_tw == null || tot_no_tw == '') {
		jQuery.growl.error({
			message : "Total No. of Tubewells can not be blank."
		});
		flag = false;
	} else if (!tot_no_tw.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in Total No. of Tubewells."
		});
		flag = false;

	} else if (tot_no_tw_wc == null || tot_no_tw_wc == '') {
		jQuery.growl.error({
			message : "Total No. of Tubewells in Working Condition can not be blank."
		});
		flag = false;
	} else if (!tot_no_tw_wc.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in Total No. of Tubewells in Working Condition."
		});
		flag = false;
	}else if (dueDate == null || dueDate == '') {
		jQuery.growl.error({
			message : "Due Date can not be blank."
		});
		flag = false;
	}

	else {
		flag = true;
	}

	return flag;
}