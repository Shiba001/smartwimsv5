/**
 * 
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	
	
	
	$("input[type='radio']").on("change", function(){  
        checkTargetMode(this);
    });
	
	var targetMode = $('input:radio[name=trgt_mode]:checked').val();
	if(targetMode == 'Monthly'){
		$('#y_target').attr('disabled', true);
	}

	$(document).on("change", ".month_val", function() {
	    var sum = 0;
	    $(".month_val").each(function(){
	        sum += +$(this).val();
	    });
	    $("#y_trgt").val(sum);
	});
	
	
	$(".yearselection option[value='"+ $("input[name='fin_year']").val() +"']").prop("selected", true);
	
	if($("input[name='trgt_mode']:checked").val() == "Yearly"){
		$("input[name^='m_trgt_']").attr('disabled', true);
		$("#y_trgt").attr('disabled', true);
		$("#y_target").attr('disabled', false);
	}
	else{
		$("input[name^='m_trgt_']").attr('disabled', false);
		$("#y_trgt").attr('disabled', false);
		$("#y_target").attr('disabled', true);
	}
	

});

function targetWaterCharges() {
	var targetMode = $('input:radio[name=trgt_mode]:checked').val();
	
	var m_trgt_1 = $('#m_trgt_1').val().trim();
	var m_trgt_2 = $('#m_trgt_2').val().trim();
	var m_trgt_3 = $('#m_trgt_3').val().trim();
	var m_trgt_4 = $('#m_trgt_4').val().trim();
	var m_trgt_5 = $('#m_trgt_5').val().trim();
	var m_trgt_6 = $('#m_trgt_6').val().trim();
	var m_trgt_7 = $('#m_trgt_7').val().trim();
	var m_trgt_8 = $('#m_trgt_8').val().trim();
	var m_trgt_9 = $('#m_trgt_9').val().trim();
	var m_trgt_10 = $('#m_trgt_10').val().trim();
	var m_trgt_11 = $('#m_trgt_11').val().trim();
	var m_trgt_12 = $('#m_trgt_12').val().trim();
	
	var total = $('#y_trgt').val().trim();
	var y_target = $('#y_target').val().trim();
	
	//var trget_mode = $('#trget_mode').val().trim();
	
	var flag = true;
	
	if(targetMode == 'Monthly'){
		if (m_trgt_1 == null || m_trgt_1 == '') {
			jQuery.growl.error({
				message : "Apr can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_1.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Apr."
			});
			
			flag = false;

		} else if (m_trgt_2 == null || m_trgt_2 == '') {
			jQuery.growl.error({
				message : "May can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_2.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in May."
			});
			
			flag = false;
		}
		
		else if (m_trgt_3 == null || m_trgt_3 == '') {
			jQuery.growl.error({
				message : "Jun can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_3.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Jun."
			});
			
			flag = false;
		}
		
		else if (m_trgt_4 == null || m_trgt_4 == '') {
			jQuery.growl.error({
				message : "Jul can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_4.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Jul."
			});
			
			flag = false;
		}
		else if (m_trgt_5 == null || m_trgt_5 == '') {
			jQuery.growl.error({
				message : "Aug can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_5.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Aug."
			});
			
			flag = false;
		}
		else if (m_trgt_6 == null || m_trgt_6 == '') {
			jQuery.growl.error({
				message : "Sep can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_6.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Sep."
			});
			
			flag = false;
		}
		else if (m_trgt_7 == null || m_trgt_7 == '') {
			jQuery.growl.error({
				message : "Oct can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_7.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Oct."
			});
			
			flag = false;
		}
		else if (m_trgt_8 == null || m_trgt_8 == '') {
			jQuery.growl.error({
				message : "Nov can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_8.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Nov."
			});
			
			flag = false;
		}
		else if (m_trgt_9 == null || m_trgt_9 == '') {
			jQuery.growl.error({
				message : "Dec can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_9.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Dec."
			});
			
			flag = false;
		}
		else if (m_trgt_10 == null || m_trgt_10 == '') {
			jQuery.growl.error({
				message : "Jan can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_10.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Jan."
			});
			
			flag = false;
		}
		else if (m_trgt_11 == null || m_trgt_11 == '') {
			jQuery.growl.error({
				message : "Feb can not be blank."
			});
			
			flag = false;
		} else if (!m_trgt_11.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Feb."
			});
			
			flag = false;
		}
		else if (m_trgt_12 == null || m_trgt_12 == '') {
			jQuery.growl.error({
				message : "Mar can not be blank."
			});
			flag = false;
		} else if (!m_trgt_12.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Mar."
			});
			
			flag = false;
		}
		
	}
	else if(targetMode == 'Yearly'){
		if (y_target == null || y_target == '') {
			jQuery.growl.error({
				message : "Annual value can not be blank."
			});
			
			flag = false;
		} else if (!y_target.match('^[0-9]*\.[0-9]*$')) {
			jQuery.growl.error({
				message : "Invalid decimal format in Annual."
			});
			
			flag = false;

		}
	}
	else{
		return true;
	}
	

	return flag;
}

function checkTargetMode(obj){
	
	if(obj.value == 'Monthly'){
		
		$('#y_target').val('');
		
		$('#y_target').attr('disabled', true);
		
		$('#m_trgt_1').attr('disabled', false);
		 $('#m_trgt_2').attr('disabled', false);
		 $('#m_trgt_3').attr('disabled', false);
		 $('#m_trgt_4').attr('disabled', false);
		 $('#m_trgt_5').attr('disabled', false);
		 $('#m_trgt_6').attr('disabled', false);
		 $('#m_trgt_7').attr('disabled', false);
		 $('#m_trgt_8').attr('disabled', false);
		 $('#m_trgt_9').attr('disabled', false);
		 $('#m_trgt_10').attr('disabled', false);
		 $('#m_trgt_11').attr('disabled', false);
		 $('#m_trgt_12').attr('disabled', false);
		 $('#y_trgt').attr('disabled', false);
	}
	else {
		$('#m_trgt_1').val('');
		$('#m_trgt_2').val('');
		$('#m_trgt_3').val('');
		$('#m_trgt_4').val('');
		$('#m_trgt_5').val('');
		$('#m_trgt_6').val('');
		$('#m_trgt_7').val('');
		$('#m_trgt_8').val('');
		$('#m_trgt_9').val('');
		$('#m_trgt_10').val('');
		$('#m_trgt_11').val('');
		$('#m_trgt_12').val('');
		$('#y_trgt').val('');
		
		 $('#m_trgt_1').attr('disabled', true);
		 $('#m_trgt_2').attr('disabled', true);
		 $('#m_trgt_3').attr('disabled', true);
		 $('#m_trgt_4').attr('disabled', true);
		 $('#m_trgt_5').attr('disabled', true);
		 $('#m_trgt_6').attr('disabled', true);
		 $('#m_trgt_7').attr('disabled', true);
		 $('#m_trgt_8').attr('disabled', true);
		 $('#m_trgt_9').attr('disabled', true);
		 $('#m_trgt_10').attr('disabled', true);
		 $('#m_trgt_11').attr('disabled', true);
		 $('#m_trgt_12').attr('disabled', true);
		 $('#y_trgt').attr('disabled', true);
		 
		 $('#y_target').attr('disabled', false);
		 
	}
}

