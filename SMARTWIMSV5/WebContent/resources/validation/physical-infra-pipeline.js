/**
 * Physical Infra Pipeline front-end validation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	$('.number_only').keyup(function() {

		var $th = $(this);
		// alert($th.val());
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		}));
	});

});

function physicalInfraPipe(){
	//alert("satya");
	var rising_main = $('#rising_main').val().trim();
	var distr_netw = $('#distr_netw').val().trim();
	var dueDate = $('#dueDate').val().trim();
	
	var flag = true;

	if (rising_main == null || rising_main == '') {
		jQuery.growl.error({
			message : "Length of Rising Main can not be blank."
		});
		
		flag = false;
	} else if (!rising_main.match('^[0-9]*\.[0-9]*$')) {
		jQuery.growl.error({
			message : "Invalid decimal format in Length of Rising Main."
		});
		
		flag = false;
	}
	else if (distr_netw == null || distr_netw == '') {
		jQuery.growl.error({
			message : "Length of Distribution Network can not be blank."
		});
		
		flag = false;
	} else if (!distr_netw.match('^[0-9]*\.[0-9]*$')) {
		jQuery.growl.error({
			message : "Invalid decimal format in Length of Distribution Network."
		});
		
		flag = false;
	}else if (dueDate == null || dueDate == '') {
		jQuery.growl.error({
			message : "Due Date can not be blank."
		});
		flag = false;
	}	
	else {
		flag = true;
	}

	return flag;
	
}