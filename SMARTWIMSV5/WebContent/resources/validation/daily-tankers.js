/**
 * 
 */

$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	
	$('.number_only').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9?]/g, function(str) {
			return '';
		}));
	});

});

function dailyTankers() {
	var no_tank_engd_dept = $('#no_tank_engd_dept').val().trim();
	var no_tank_hire = $('#no_tank_hire').val().trim();
	var no_pvc_rcc_tanks_depl = $('#no_pvc_rcc_tanks_depl').val().trim();
	var popu_served_tanks_pvc_rcc = $('#popu_served_tanks_pvc_rcc').val().trim();
	var qty_mld = $('#qty_mld').val().trim();

	var flag = true;

	if (no_tank_engd_dept == null || no_tank_engd_dept == '') {
		jQuery.growl.error({
			message : "No. of Departmental Tankers Engaged can not be blank."
		});
		flag = false;
	} else if (!no_tank_engd_dept.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Departmental Tankers Engaged."
		});
		flag = false;

	} else if (no_tank_hire == null || no_tank_hire == '') {
		jQuery.growl.error({
			message : "No. of Tankers Hired can not be blank."
		});
		flag = false;
	} else if (!no_tank_hire.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Tankers Hired."
		});
		flag = false;
	}
	else if (no_pvc_rcc_tanks_depl == null || no_pvc_rcc_tanks_depl == '') {
		jQuery.growl.error({
			message : "No. of PVC / RCC Tanks Deployed can not be blank."
		});
		flag = false;
	} else if (!no_pvc_rcc_tanks_depl.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of PVC / RCC Tanks Deployed."
		});
		flag = false;
	}
	
	else if (popu_served_tanks_pvc_rcc == null || popu_served_tanks_pvc_rcc == '') {
		jQuery.growl.error({
			message : "Population Served by Tankers and PVC/RCC Tanks can not be blank."
		});
		flag = false;
	} else if (!popu_served_tanks_pvc_rcc.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in Population Served by Tankers and PVC/RCC Tanks. "
		});
		flag = false;
	}
	
	else if (qty_mld == null || qty_mld == '') {
		jQuery.growl.error({
			message : "Quantity supplied (in Ltr) can not be blank. "
		});
		flag = false;
	} else if (!qty_mld.match('^[0-9]*\.[0-9]*$')) {
		jQuery.growl.error({
			message : "Invalid decimal format in Quantity supplied (in Ltr). "
		});
		flag = false;
	}

	else {
		flag = true;
	}

	return flag;
}