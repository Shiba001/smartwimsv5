/**
 * Daily water charges front-end validation
 */
$(document).ready(function() {

	$('.decimal_number').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	$('.number_only').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		}));
	});

});

function dailyWaterCharges() {
	var tot_cur_coll = $('#tot_cur_coll').val().trim();
	

	var flag = true;

	if (tot_cur_coll == null || tot_cur_coll == '') {
		jQuery.growl.error({
			message : "Total Water Charges Collection  can not be blank."
		});
		flag = false;
	} else if (!tot_cur_coll.match('^[0-9]*\.[0-9]*$')) {
		jQuery.growl.error({
			message : "Invalid decimal format in Total Water Charges Collection."
		});
		flag = false;

	} 

	else {
		flag = true;
	}

	return flag;
}