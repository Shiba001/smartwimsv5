/**
 * Daily Complaints front-end validation
 */
$(document).ready(function() {
	
	$('.decimal_number').keyup(function() {
		
		var $th = $(this);
		$th.val($th.val().replace(/[^0-9,.?]/g, function(str) {
			return '';
		}));
	});
	$('.number_only').keyup(function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^0-9]/g, function(str) {
			return '';
		})); 
	});

});

function dailyComplaints() {
	var no_ws_comp_recv = $('#no_ws_comp_recv').val().trim();
	var no_ws_comp_resolved = $('#no_ws_comp_resolved').val().trim();
	var no_tw_comp_recv = $('#no_tw_comp_recv').val().trim();
	var no_tw_comp_resolved = $('#no_tw_comp_resolved').val().trim();
	var flag = true;

	if (no_ws_comp_recv == null || no_ws_comp_recv == '') {
		jQuery.growl.error({
			message : "No. of Water Supply Complaints Received can not be blank."
		});
		flag = false;
	} else if (!no_ws_comp_recv.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Water Supply Complaints Received."
		});
		flag = false;

	} else if (no_ws_comp_resolved == null || no_ws_comp_resolved == '') {
		jQuery.growl.error({
			message : "No. of Water Supply Complaints Resolved can not be blank."
		});
		flag = false;
	} else if (!no_ws_comp_resolved.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Water Supply Complaints Resolved."
		});
		flag = false;
	}

	else if (no_tw_comp_recv == null || no_tw_comp_recv == '') {
		jQuery.growl.error({
			message : "No. of Tubewell Complaints Received can not be blank."
		});
		flag = false;
	} else if (!no_tw_comp_recv.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Tubewell Complaints Received."
		});
		flag = false;
	} else if (no_tw_comp_resolved == null || no_tw_comp_resolved == '') {
		jQuery.growl.error({
			message : "No. of Tublewell Complaints Resolved can not be blank."
		});
		flag = false;
	} else if (!no_tw_comp_resolved.match('^[1-9]([0-9]{0,45}$)')) {
		jQuery.growl.error({
			message : "Invalid number format in No. of Tublewell Complaints Resolved."
		});
		flag = false;
	}

	else {
		flag = true;
	}

	return flag;
}
