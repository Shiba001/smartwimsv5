/**
 * 
 */
$(document).ready(function() {

	$(document).on("keyup",'.only_text',function() {

		var $th = $(this);
		$th.val($th.val().replace(/[^a-zA-Z0-9]/g, function(str) {
			return '';
		}));
	});

});

function physicalInfraEleInst() {
	var list_loc_of_inst = document.getElementsByName("list_loc_of_inst");
	var list_cap_of_trnf = document.getElementsByName("list_cap_of_trnf");
	var list_year_inst = document.getElementsByName("list_year_inst");
	var list_hp_lt = document.getElementsByName("list_hp_lt");

	var flag = true;
	
	for (var i = 0; i < list_loc_of_inst.length; i++) {
		if (list_loc_of_inst[i].name == "list_loc_of_inst"
				&& list_loc_of_inst[i].value == '') {
			jQuery.growl.error({
				message : "Location of the Installation can not be blank."
			});
			flag = false;
			break;
		} else if (list_cap_of_trnf[i].name == "list_cap_of_trnf"
				&& list_cap_of_trnf[i].value == '') {
			jQuery.growl.error({
				message : "Capacity of the Transformer can not be blank."
			});
			flag = false;
			break;
		} else if (list_year_inst[i].name == "list_year_inst"
				&& list_year_inst[i].value == '') {
			jQuery.growl.error({
				message : "Year of Installation can not be blank."
			});
			flag = false;
			break;
		} else if (list_hp_lt[i].name == "list_hp_lt"
				&& list_hp_lt[i].value == '') {
			jQuery.growl.error({
				message : "Type of Transformer can not be blank."
			});
			flag = false;
			break;
		} else {
			flag = true;
		}
		
		
	}
	return flag;
}