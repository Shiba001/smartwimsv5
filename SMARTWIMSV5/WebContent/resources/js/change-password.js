$(document).ready(function(){
	
	var flag = true;
	$("form").submit(function(event){
		if($("#oldPassword").val().trim() == ""){
			jQuery.growl.error({
				message : "Old Password cannot be Blank"
									});
			event.preventDefault();
			flag = false;
		} 
		
		if($("#newPassword").val().trim() == ""){
			jQuery.growl.error({
				message : "New Password cannot be Blank"
									});
			event.preventDefault();
			flag = false;
		} else if(!(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test($("#newPassword").val()))){
			
			jQuery.growl.error({
				message : "New Password length should be within 6-20 including at least one number, one lowercase and uppercase character, one special character"
									});
			event.preventDefault();
			flag = false;
		} 
		
		if($("#confirmNewPassword").val().trim() == ""){
			jQuery.growl.error({
				message : "Confirm Password cannot be Blank"
									});
			event.preventDefault();
			flag = false;
		} else if(!(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test($("#confirmNewPassword").val()))){
			
			jQuery.growl.error({
				message : "Confirm Password length should be within 6-20 including at least one number, one lowercase and uppercase character, one special character"
									});
			event.preventDefault();
			flag = false;
		} 
		
		if($("#newPassword").val() != $("#confirmNewPassword").val()){
			jQuery.growl.error({
				message : "Password Mismatched"
									});
			event.preventDefault();
			flag = false;
		} 
		
		
		if(flag){
			
			// Old Password Hashing
			var salt = getTokenFromServer();
			var saltLength = salt.length;
			var saltLengthHalf = Math.round(saltLength / 2);
			var firstPart = salt.substr(0,saltLengthHalf);
			var secondPart = salt.substr(saltLengthHalf,saltLength);
			var oldPassword = $("#oldPassword").val();
			var md5password = firstPart + md5(oldPassword) + secondPart;
			$("#oldPassword").val(md5password);
			
			// New Password Hashing
			var salt1 = getTokenFromServer();
			var saltLength1 = salt1.length;
			var saltLengthHalf1 = Math.round(saltLength1 / 2);
			var firstPart1 = salt1.substr(0,saltLengthHalf1);
			var secondPart1 = salt1.substr(saltLengthHalf1,saltLength1);
			var newPassword = $("#newPassword").val();
			var md5password1 = firstPart1 + md5(newPassword) + secondPart1;
			$("#newPassword").val(md5password1);
			
			// New Confirm Password Hashing
			var salt2 = getTokenFromServer();
			var saltLength2 = salt2.length;
			var saltLengthHalf2 = Math.round(saltLength2 / 2);
			var firstPart2 = salt2.substr(0,saltLengthHalf2);
			var secondPart2 = salt2.substr(saltLengthHalf2,saltLength2);
			var confirmNewPassword = $("#confirmNewPassword").val();
			var md5password2 = firstPart2 + md5(confirmNewPassword) + secondPart2;
			$("#confirmNewPassword").val(md5password2);
			
		}
	});	
});