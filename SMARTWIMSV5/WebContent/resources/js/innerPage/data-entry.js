/**
 * DashBoard Charts
 */

$(document).ready(function(){
	$('.btn-all-ulb').on('click', function(){
		var winHeight = $(window).height();
		var chartHeight = parseInt(winHeight) - 112;
		$('#all-ulb-chart').css('height', chartHeight);
	});
});

		/**
		 * Map Implementation 
		 */
		
var map = null;

function initialize() {
	var mapOptions = {
		zoom : 10
	};
	map = new google.maps.Map(document.getElementById('map-canvas'),
			mapOptions);
	bounds = new google.maps.LatLngBounds();
	$.each(dataEntryStatus, function(index1, value) {
		if(index1.indexOf("C") != -1 || index1.indexOf("P") != -1 || index1.indexOf("N") != -1){
			$.each(value, function(index2, value1) {
				if(index1.indexOf("N") != -1){
					pinColor = "FF0000";
				} else if(index1.indexOf("P") != -1){
					pinColor = "FFD500";
				} else {
					pinColor = "008000";
				}
				
			setVendorMarker(value1.latitude, value1.longitude, value1.ulb_name, pinColor,value1.dataEntryStatus);
			});
		}
	});
	map.fitBounds(bounds);
}
var bounds = null;
var infowindow = false;
function setVendorMarker(latitude, longitude, ulb_name, pinColor,dataEntryStatus) {
	var location = new google.maps.LatLng(latitude, longitude);
    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
    		new google.maps.Size(21, 34),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 34));
	var marker = new google.maps.Marker({
		icon: pinImage,
		position : location,
		map : map,
		title : ulb_name
	});
	
	bounds.extend(marker.position);
	google.maps.event.addListener(marker, 'click', function(){
		
		var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<div id="bodyContent">'+
        '<table><tr><td>'+ulb_name+'</td><td style="color:#'+pinColor+';font-weight:bold;">'+dataEntryStatus+'</td></tr></table>'+
        '</div>'+
        '</div>';
		
		if( infowindow ) {
        	infowindow.close();
         }
		
	    infowindow = new google.maps.InfoWindow({
	      content: contentString
	    });
		 infowindow.open(map, this);
	});

	// To add the marker to the map, call setMap();
	marker.setMap(map);
	
}

$(document).ready(function(){
	initialize();
});
//google.maps.event.addDomListener(window, 'load', initialize);

		
		var isAmrut = false;
		$(".btn-amrut").click(function(){
			isAmrut = true;
			getDataEntryStatus();
		});
		
		function genarateQueryStr(){
			/* amrutTypeId, Date, month, divisionId, districtId, ulbId  */
			var queryStr = "";
			if(isAmrut){
				queryStr += "amrutTypeId=1";
			}
			
			if($("[name='inlineRadioOptions']:checked").val() == "daily"){
				if(queryStr != "")
					queryStr += "&"
				queryStr += "Date="+dateFormatter($("#dashboard_date").val());
			}
			else{
				if(queryStr != "")
					queryStr += "&"
				queryStr += "month="+$("select[name='month']").val();
				queryStr += "&selectedYear="+$("select[name='selectedYear']").val();
			}
			
			if($("#ulbSelect").val() != "ULB"){
				queryStr += "&"
				queryStr += "ulbId="+$("#ulbSelect").val();
			}
			else if($("#disSelect").val() != "District"){
				queryStr += "&"
				queryStr += "districtId="+$("#disSelect").val();
			}
			else if($("#divisionId").val() != "ALL"){
				queryStr += "&"
				queryStr += "divisionId="+$("#divisionId").val();
			}
			return queryStr;
		}
		function dashboardAjaxDailyWaterSupplyModels(){
			var queryStr = genarateQueryStr();
			
			$.ajax({
				url: "./ajax-data-entry?"+queryStr,
				async: false,
				success: function(datas){
					
					
					var complete = datas.complete;
					var partial = datas.partial;
					var notApproved = datas.notApproved;
					var approved = datas.approved;
					var noDataEntry = datas.noDataEntry;
					
					dataEntryStatus = datas.dataEntryStatus;
					
					
					$(".green").html(complete);
					$(".yellow").html(partial);
					$(".red").html(noDataEntry);
					
					initialize();
				}
			});
		}
		
		$("input[name='inlineRadioOptions']").change(function(){
			if($("input[value='daily']").prop("checked")){
				$(".dateFetch").html($("#dashboard_date").val());
				$("select[name='month']").prop("disabled", true);
				$("select[name='selectedYear']").prop("disabled", true);
				$("#dashboard_date").prop("disabled", false);
			}
			else{
				$(".dateFetch").html($("select[name='month'] option:selected").text());
				$("select[name='month']").prop("disabled", false);
				$("select[name='selectedYear']").prop("disabled", false);
				$("#dashboard_date").prop("disabled", true);
			}
			getDataEntryStatus();
		});
		
		var isFirst = 0;
		var defaultDailyDt;
		$("input#dashboard_date").change(function(){
			
			var that = this;
			if(!defaultDailyDt || ($(that).val() != defaultDailyDt)){
				defaultDailyDt = $(that).val();
				var getDate = $(that).val();
				
				$('.dateFetch').html(getDate);
				var dateArr = $(that).val().split("-");
				$("select[name='month'] option[value='"+ dateArr[1] +"']").prop("selected", true);
				$("select[name='selectedYear'] option[value='"+ dateArr[2] +"']").prop("selected", true);
				getDataEntryStatus();
			}
		});
		
		$("select[name='month']").change(function(){
			var that = this;
			if($("input[value='monthly']").prop("checked")){
				$(".dateFetch").html($(that).children(":selected").text());
				
				getDataEntryStatus();
			}
		});
		
		$("select[name='selectedYear']").change(function(){
   			var that = this;
   			if($("input[value='monthly']").prop("checked")){
   				$(".dateFetch").html($(that).children(":selected").text());
   				
   				getDataEntryStatus();
   			}
   		});
		
		
		$(".data-entry-circle").click(function(event){
			event.preventDefault();
			var strVar="";
			
			var keys = Object.keys(dataEntryStatus);
			
			if($(this).hasClass("green")){
				$('#entry_status_message').html("Complete");
				var listUlb = {};
				$.each(keys, function(index, val){
					if(val.indexOf("C") != -1){
						listUlb = dataEntryStatus[val];
					}
				});
				var sl = 1;
				$.each(listUlb, function(index, value){
					strVar += "<tr>";
					strVar += "<th class=\"text-center\">"+ sl++ +"<\/th>";
					strVar += "<th class=\"text-center\">"+ index.substr(0, index.indexOf(".")) +"<\/th>";
					strVar += "<th class=\"text-left\">"+ value.ulb_name +"<\/th>";
					strVar += "<\/tr>";
				});
				if(strVar == ""){
					strVar += "<tr>";
					strVar += "<th class=\"text-center\" colspan=\"2\">No Data Available<\/th>";
					strVar += "<\/tr>";
				}
			}
			else if($(this).hasClass("yellow")){
				$('#entry_status_message').html("Partial");
				var listUlb = {};
				$.each(keys, function(index, val){
					if(val.indexOf("P") != -1){
						listUlb = dataEntryStatus[val];
					}
				});
				var slna = 1;
				$.each(listUlb, function(index, value){
					strVar += "<tr>";
					strVar += "<th class=\"text-center\">"+ slna++ +"<\/th>";
					strVar += "<th class=\"text-center\">"+ index.substr(0, index.indexOf(".")) +"<\/th>";
					strVar += "<th class=\"text-left\">"+ value.ulb_name +"<\/th>";
					strVar += "<\/tr>";
				});
				if(strVar == ""){
					strVar += "<tr>";
					strVar += "<th class=\"text-center\" colspan=\"2\">No Data Available<\/th>";
					strVar += "<\/tr>";
				}
			}
			else{
				// red
				$('#entry_status_message').html("No");
				var listUlb = {};
				$.each(keys, function(index, val){
					if(val.indexOf("N") != -1 && val.indexOf("NA") == -1){
						listUlb = dataEntryStatus[val];
					}
				});
				
				var sln = 1;
				$.each(listUlb, function(index, value){
					strVar += "<tr>";
					strVar += "<th class=\"text-center\">"+ sln++ +"<\/th>";
					strVar += "<th class=\"text-center\">"+ index.substr(0, index.indexOf(".")) +"<\/th>";
					strVar += "<th class=\"text-left\">"+ value.ulb_name +"<\/th>";
					strVar += "<\/tr>";
				});
				if(strVar == ""){
					strVar += "<tr>";
					strVar += "<th class=\"text-center\" colspan=\"2\">No Data Available<\/th>";
					strVar += "<\/tr>";
				}
			}
			
			$("#data-table-modal table tbody").html(strVar);
			$("#data-table-modal").modal("show");
		});
		
		$(document).ready(function(){
			$("input[value='monthly']").closest(".search-sec").css("display", "none");
		});
		
		function getDataEntryStatus(){
			var userLevelId = $("script[src$='dashboard-header.js']").attr("data-user-level-id");
			var call = 0;
			if(userLevelId == 3){
				call = 3;
			}
			else {
				call = 2;
			}
					if(isFirst<call){
						
						isFirst++;
						return false;
					}
					else{
						//showOnlyAllLpcd();
						dashboardAjaxDailyWaterSupplyModels();
						
						
						initialize();
					}
		}
		