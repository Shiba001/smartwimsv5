function changeUserTypeDiv(usrType){
	 	//  for Bacteriological
		if(usrType == 'admin'){
			$('#bacteriological .progressBr').find('.progressChart').removeAttr('id');
			$('#bacteriological .progressBr').hide();
			if($('#bacteriological .progressBr').find('.progressChart').attr('id')){
				$('#bacteriological .progressBr').find('.progressChart').removeAttr('id');
			}
			
			if(!$('#myModal_bact').find('.LPCD-chart').attr('id')){
				$('#myModal_bact').find('.LPCD-chart').attr('id','allLPCD_bact');
			}
			
			$('#myModal_progressbar_bact').find('.progressChart').attr('id','container_bact');
			$('#bacteriological .barContainer, #bacteriological .progressBtn, #bacteriological .allUlbBtn').show();
			
		}else if(usrType == 'division'){
			
			if(flag == "false" || flag == false){
				$('#bacteriological .progressBr').find('.progressChart').removeAttr('id');
				$('#bacteriological .progressBr').hide();
				if($('#bacteriological .progressBr').find('.progressChart').attr('id')){
					$('#bacteriological .progressBr').find('.progressChart').removeAttr('id');
				}
				
				if(!$('#myModal_bact').find('.LPCD-chart').attr('id')){
					$('#myModal_bact').find('.LPCD-chart').attr('id','allLPCD_bact');
				}
				
				$('#myModal_progressbar_bact').find('.progressChart').attr('id','container_bact');
				$('#bacteriological .barContainer, #bacteriological .progressBtn, #bacteriological .allUlbBtn').show();
			}
			else{
				$('#bacteriological .barContainer, #bacteriological .allUlbBtn').hide();
				if($('#myModal_bact').find('.LPCD-chart').attr('id')){
					$('#myModal_bact').find('.LPCD-chart').removeAttr('id');
				}
				
				if(!$('#myModal_progressbar_bact').find('.progressChart').attr('id')){
					$('#myModal_progressbar_bact').find('.progressChart').attr('id','container_bact');
				}
				
				$('#bacteriological .progressBr').find('.progressChart').attr('id','allLPCD_bact');
				$('#bacteriological .progressBr h5').text('All ULB');
				$('#bacteriological .progressBr, #bacteriological .progressBtn').show();
			}
		}else if(usrType == 'district'){
			$('#bacteriological .barContainer, #bacteriological .progressBtn').hide();
			$('#bacteriological .progressBr').find('.progressChart').removeAttr('id');
			if($('#myModal_progressbar_bact').find('.progressChart').attr('id')){
				$('#myModal_progressbar_bact').find('.progressChart').removeAttr('id');
			}
			
			if(!$('#myModal_bact').find('.LPCD-chart').attr('id')){
				$('#myModal_bact').find('.LPCD-chart').attr('id','allLPCD_bact');
			}
			
			$('#bacteriological .progressBr').find('.progressChart').attr('id','container_bact');
			$('#bacteriological .progressBr h5').text('Progress Report');
			$('#bacteriological .progressBr, #bacteriological .allUlbBtn').show();
		}else if(usrType == 'ulb'){
			$('#bacteriological .barContainer, #bacteriological .progressBtn, #bacteriological .allUlbBtn').hide();
			if($('#myModal_progressbar_bact').find('.progressChart').attr('id')){
				$('#myModal_progressbar_bact').find('.progressChart').removeAttr('id');
			}
			$('#bacteriological .progressBr').find('.progressChart').attr('id','container_bact');
			$('#bacteriological .progressBr h5').text('Progress Report');
			$('#bacteriological .progressBr').show();
		}
		
		
		// for FRC
		if(usrType == 'admin'){
			$('#frc .progressBr').find('.progressChart').removeAttr('id');
			$('#frc .progressBr').hide();
			if($('#frc .progressBr').find('.progressChart').attr('id')){
				$('#frc .progressBr').find('.progressChart').removeAttr('id');
			}
			
			if(!$('#myModal_frc').find('.LPCD-chart').attr('id')){
				$('#myModal_frc').find('.LPCD-chart').attr('id','allLPCD_frc');
			}
			
			$('#myModal_progressbar_frc').find('.progressChart').attr('id','container_frc');
			$('#frc .barContainer, #frc .progressBtn, #frc .allUlbBtn').show();
		}else if(usrType == 'division'){
			
			
			
			if(flag1 == "false" || flag1 == false){
				$('#frc .progressBr').find('.progressChart').removeAttr('id');
				$('#frc .progressBr').hide();
				if($('#frc .progressBr').find('.progressChart').attr('id')){
					$('#frc .progressBr').find('.progressChart').removeAttr('id');
				}
				
				if(!$('#myModal_frc').find('.LPCD-chart').attr('id')){
					$('#myModal_frc').find('.LPCD-chart').attr('id','allLPCD_frc');
				}
				
				$('#myModal_progressbar_frc').find('.progressChart').attr('id','container_frc');
				$('#frc .barContainer, #frc .progressBtn, #frc .allUlbBtn').show();
			}
			else{
				$('#frc .barContainer, #frc .allUlbBtn').hide();
				if($('#myModal_frc').find('.LPCD-chart').attr('id')){
					$('#myModal_frc').find('.LPCD-chart').removeAttr('id');
				}
				
				if(!$('#myModal_progressbar_frc').find('.progressChart').attr('id')){
					$('#myModal_progressbar_frc').find('.progressChart').attr('id','container_frc');
				}
				
				$('#frc .progressBr').find('.progressChart').attr('id','allLPCD_frc');
				$('#frc .progressBr h5').text('All ULB');
				$('#frc .progressBr, #frc .progressBtn').show();
			}
		}else if(usrType == 'district'){
			$('#frc .barContainer, #frc .progressBtn').hide();
			$('#frc .progressBr').find('.progressChart').removeAttr('id');
			if($('#myModal_progressbar_frc').find('.progressChart').attr('id')){
				$('#myModal_progressbar_frc').find('.progressChart').removeAttr('id');
			}
			
			if(!$('#myModal_frc').find('.LPCD-chart').attr('id')){
				$('#myModal_frc').find('.LPCD-chart').attr('id','allLPCD_frc');
			}
			
			$('#frc .progressBr').find('.progressChart').attr('id','container_frc');
			$('#frc .progressBr h5').text('Progress Report');
			$('#frc .progressBr, #frc .allUlbBtn').show();
		}else if(usrType == 'ulb'){
			$('#frc .barContainer, #frc .progressBtn, #frc .allUlbBtn').hide();
			if($('#myModal_progressbar_frc').find('.progressChart').attr('id')){
				$('#myModal_progressbar_frc').find('.progressChart').removeAttr('id');
			}
			$('#frc .progressBr').find('.progressChart').attr('id','container_frc');
			$('#frc .progressBr h5').text('Progress Report');
			$('#frc .progressBr').show();
			
		}
		
		

}


var defaultDailyDt;
$(document).ready(function(){
	var usrType = "";
	var userLevelId = $("script[src$='dashboard-header.js']").attr("data-user-level-id");
	if(userLevelId == 4){
		usrType = "ulb";
	} else if(userLevelId == 3){
		usrType = "district";
	} else if(userLevelId == 2){
		usrType = "division";
	} else {
		usrType = "admin";
	}


	changeUserTypeDiv(usrType);
	
	drawTopBarColors_bact();	
	drawBottomBarColors_bact();
	drawProgressBarColors_bact();
	drawAllBarColors_bact();
	drawTopBarColors_frc();
	drawBottomBarColors_frc();
	drawProgressBarColors_frc();
	drawAllBarColors_frc();
});



/**
		 * Map Implementation 
		 */
/*--------------------------Bacteriological section----------*/
		var map_bact = null;

		function initialize_bact() {
			var mapOptions = {
				zoom : 10
			};
//			document.getElementById('bacteriological').style.display="block";
			var frcClass =  $("#bacteriological").attr("class");
			$("#bacteriological").addClass("active in");
			
			map_bact = new google.maps.Map(document.getElementById('map-canvas_bact'),
					mapOptions);
			bounds_bact = new google.maps.LatLngBounds();
			var pinColor = "";
			$.each(lpcdMap, function(index, value){
				if(value.total_percentage_found < 100 ){
					pinColor = "FF0000";
				} else {
					pinColor = "008000";
				}
				setVendorMarker_bact(value.ulbNameModel.ulbModel.latitude, value.ulbNameModel.ulbModel.longitude, index, pinColor, value.currentMonth, value.previousMonth,value.percentage_found, value.pm_percentage_found);
			});
				
				
				
			
			map_bact.fitBounds(bounds_bact);
			setTimeout(function(){
				$("#bacteriological").removeAttr("class");
				$("#bacteriological").addClass(frcClass);
			}, 500, frcClass);
		}
		var bounds_bact = null;
		var infowindow_bact = false;
		function setVendorMarker_bact(latitude, longitude, ulb_name, pinColor,currentMonth, previousMonth, percentage_found, pm_percentage_found) {
			var location = new google.maps.LatLng(latitude, longitude);
			//map.setCenter(location);
		    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
		    		new google.maps.Size(21, 34),
		            new google.maps.Point(0,0),
		            new google.maps.Point(10, 34));
			var marker = new google.maps.Marker({
				icon: pinImage,
				position : location,
				map : map_bact,
				title : ulb_name
			});
			
			
			bounds_bact.extend(marker.position);
			google.maps.event.addListener(marker, 'click', function(){
				
				
				var contentString = '<div id="content">'+
	            '<div id="siteNotice">'+
	            '</div>'+
	            '<div id="bodyContent">'+
	            '<table border="1" width="100%"><tr><td rowspan="3" class="thead">'+ulb_name+'</td><td colspan="2" align="center" bgcolor="#C4D600" class="text-center">Monthly Average</td></tr><tr><td bgcolor="#e3e3e3">'+currentMonth+'</td><td bgcolor="#e3e3e3">'+previousMonth+'</td></tr><tr><td style="color:#'+pinColor+';font-weight:bold;">'+percentage_found+' %</td><td style="color:#'+pinColor+';font-weight:bold;">'+pm_percentage_found+' %</td></tr></table>'+
	            '</div>'+
	            '</div>';
				
				if( infowindow_bact ) {
		        	infowindow_bact.close();
		         }
				
	          infowindow_bact = new google.maps.InfoWindow({
	          content: contentString
	        });
	        
				 infowindow_bact.open(map_bact, this);
			});

			// To add the marker to the map, call setMap();
			marker.setMap(map_bact);
			
		}

		$(document).ready(function(){
			initialize_bact();
		});
		//google.maps.event.addDomListener(window, 'load', initialize_bact);
		
	/*-----------------------FRC section------------------*/	
		var map_frc = null;
		function initialize_frc() {
			var mapOptions = {
				zoom : 10
			};
			var frcClass =  $("#frc").attr("class");
			$("#frc").addClass("active in");
			map_frc = new google.maps.Map(document.getElementById('map-canvas_frc'),
					mapOptions);
			bounds_frc = new google.maps.LatLngBounds();
			var pinColor = "";
			$.each(lpcdMap, function(index, value){
				if(value.total_percentage_found_frc < 100 ){
					pinColor = "FF0000";
				} else {
					pinColor = "008000";
				}
				setVendorMarker_frc(value.ulbNameModel.ulbModel.latitude, value.ulbNameModel.ulbModel.longitude, index, pinColor,value.currentMonth, value.previousMonth, value.percentage_found_frc, value.pm_percentage_found_frc);
			});
				
				
				
			
			map_frc.fitBounds(bounds_frc);
			setTimeout(function(){
				$("#frc").removeAttr("class");
				$("#frc").addClass(frcClass);
			}, 500, frcClass);
		}
		var bounds_frc = null;
		var infowindow_frc = false;
		function setVendorMarker_frc(latitude, longitude, ulb_name, pinColor, currentMonth, previousMonth,percentage_found_frc, pm_percentage_found_frc) {
			var location = new google.maps.LatLng(latitude, longitude);
			//map.setCenter(location);
		    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
		    		new google.maps.Size(21, 34),
		            new google.maps.Point(0,0),
		            new google.maps.Point(10, 34));
			var marker = new google.maps.Marker({
				icon: pinImage,
				position : location,
				map : map_frc,
				title : ulb_name
			});
			
			
			bounds_frc.extend(marker.position);
			google.maps.event.addListener(marker, 'click', function(){
				
				
				var contentString = '<div id="content">'+
	            '<div id="siteNotice">'+
	            '</div>'+
	            '<div id="bodyContent">'+
	            '<table border="1" width="100%"><tr><td rowspan="3" class="thead">'+ulb_name+'</td><td colspan="2" align="center" bgcolor="#C4D600" class="text-center">Monthly Average</td></tr><tr><td bgcolor="#e3e3e3">'+currentMonth+'</td><td bgcolor="#e3e3e3">'+previousMonth+'</td></tr><tr><td style="color:#'+pinColor+';font-weight:bold;">'+percentage_found_frc+' %</td><td style="color:#'+pinColor+';font-weight:bold;">'+pm_percentage_found_frc+' %</td></tr></table>'+
	            '</div>'+
	            '</div>';
				
				if( infowindow_frc ) {
		        	infowindow_frc.close();
		         }
				
	          infowindow_frc = new google.maps.InfoWindow({
	          content: contentString
	        });
	        
				 infowindow_frc.open(map_frc, this);
			});

			// To add the marker to the map, call setMap();
			marker.setMap(map_frc);
			
		}
		
		$(document).ready(function(){
			initialize_frc();
		});
		//google.maps.event.addDomListener(window, 'load', initialize_frc);
		
		
		
		/**
		* Top 10 LPCD
		*/


			Highcharts.theme = {
			   xAxis: {
			      gridLineColor: '#fff',
			      labels: {
			         style: {
			            color: '#000'
			         }
			      },
			      lineColor: '#707073',
			      minorGridLineColor: '#505053',
			   },
			   yAxis: {
			      gridLineColor: '#FFFFFF',
			      labels: {
			         style: {
			            color: '#fff'
			         }
			      },
			      lineColor: '#fff',
			      minorGridLineColor: '#505053',
			      tickColor: '#707073',
			      tickWidth: 1,
			   },
			   tooltip: {
			      backgroundColor: 'rgba(0, 0, 0, 0.85)',
			      style: {
			         color: '#F0F0F0'
			      }
			   },
			   plotOptions: {
			      series: {
			         dataLabels: {
			            color: '#B0B0B3'
			         },
			         marker: {
			            lineColor: '#333'
			         }
			      },
			      boxplot: {
			         fillColor: '#505053'
			      }
			   },
			   labels: {
			      style: {
			         color: '#707073'
			      }
			   },

			   // special colors for some of the
			   dataLabelsColor: '#B0B0B3',
			   textColor: '#C0C0C0',
			   contrastTextColor: '#F0F0F3',
			   maskColor: 'rgba(255,255,255,0.3)'
			};
			// Apply the theme
			Highcharts.setOptions(Highcharts.theme);
			/*--------------------------bacteriological section-----------------*/
			function drawTopBarColors_bact() {
				Highcharts.setOptions({
					colors: ['#3c8a2e']
				});
			    Highcharts.chart('top10LPCD_bact', {
			        chart: {
			            type: 'bar'
			        },
			        title: {
			            text: ''
			        },
			        subtitle: {
			            text: ''
			        },
			        xAxis: {
			            categories: top10ulb,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            max:130,
			            visible: false
			        },
			        tooltip: {
			            enabled: false
			        },
			        plotOptions: {
			            bar: {
			                dataLabels: {
			                    enabled: true,
			                    format: '{y} %'
			                }
			            }
			        },
			        legend: {
			            enabled: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: [{
			            name: 'Year 1800',
			            data: top10lpcd
			        }]
			    });
			}
			/*------------------------frc section----------------*/
			function drawTopBarColors_frc() {
				Highcharts.setOptions({
					colors: ['#3c8a2e']
				});
			    Highcharts.chart('top10LPCD_frc', {
			        chart: {
			            type: 'bar'
			        },
			        title: {
			            text: ''
			        },
			        subtitle: {
			            text: ''
			        },
			        xAxis: {
			            categories: top10ulb1,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            max:130,
			            visible: false
			        },
			        tooltip: {
			            enabled: false
			        },
			        plotOptions: {
			            bar: {
			                dataLabels: {
			                    enabled: true,
			                    format: '{y} %'
			                }
			            }
			        },
			        legend: {
			            enabled: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: [{
			            name: 'Year 1800',
			            data: top10lpcd1
			        }]
			    });
			}
			
			/**
			* Bottom 10 LPCD
			*/

				Highcharts.theme = {
				   xAxis: {
				      gridLineColor: '#fff',
				      labels: {
				         style: {
				            color: '#000'
				         }
				      },
				      lineColor: '#707073',
				      minorGridLineColor: '#505053',
				      tickColor: '#707073'
				   },
				   yAxis: {
				      gridLineColor: '#FFFFFF',
				      labels: {
				         style: {
				            color: '#fff'
				         }
				      },
				      lineColor: '#fff',
				      minorGridLineColor: '#505053',
				      tickColor: '#707073'
				   },
				   tooltip: {
				      backgroundColor: 'rgba(0, 0, 0, 0.85)',
				      style: {
				         color: '#F0F0F0'
				      }
				   },
				   plotOptions: {
				      series: {
				         dataLabels: {
				            color: '#000'
				         },
				         marker: {
				            lineColor: '#333'
				         }
				      }
				   },
				   labels: {
				      style: {
				         color: '#707073'
				      }
				   },
				   // special colors for some of the
				   dataLabelsColor: '#000',
				   textColor: '#000'
				};

				// Apply the theme
				Highcharts.setOptions(Highcharts.theme);
				
				function drawBottomBarColors_bact() {
					Highcharts.setOptions({
						colors: ['#c3c3c3']
					});
				    Highcharts.chart('bottom10LPCD_bact', {
				        chart: {
				            type: 'bar'
				        },
				        title: {
				            text: ''
				        },
				        subtitle: {
				            text: ''
				        },
				        xAxis: {
				            categories: bottom10ulb,
				            title: {
				                text: null
				            }
				        },
				        yAxis: {
				            min: 0,
				            max:130,
				            visible: false
				        },
				        tooltip: {
				            enabled: false
				        },
				        plotOptions: {
				            bar: {
				                dataLabels: {
				                    enabled: true,
				                    format: '{y} %'
				                }
				            }
				        },
				        legend: {
				            enabled: false
				        },
				        credits: {
				            enabled: false
				        },
				        series: [{
				            name: 'Year 1800',
				            data: bottom10lpcd
				        }]
				    });
				}
				
				function drawBottomBarColors_frc() {
					Highcharts.setOptions({
						colors: ['#c3c3c3']
					});
				    Highcharts.chart('bottom10LPCD_frc', {
				        chart: {
				            type: 'bar'
				        },
				        title: {
				            text: ''
				        },
				        subtitle: {
				            text: ''
				        },
				        xAxis: {
				            categories: bottom10ulb1,
				            title: {
				                text: null
				            }
				        },
				        yAxis: {
				            min: 0,
				            max:130,
				            visible: false,
				        },
				        tooltip: {
				            enabled: false
				        },
				        plotOptions: {
				            bar: {
				                dataLabels: {
				                    enabled: true,
				                    format: '{y} %'
				                }
				            }
				        },
				        legend: {
				            enabled: false
				        },
				        credits: {
				            enabled: false
				        },
				        series: [{
				            name: 'Year 1800',
				            data: bottom10lpcd1
				        }]
				    });
				}
				
			/*	allLPCD*/
				Highcharts.theme = {
						   xAxis: {
						      gridLineColor: '#fff',
						      labels: {
						         style: {
						            color: '#000'
						         }
						      },
						      lineColor: '#707073',
						      minorGridLineColor: '#505053',
						      tickColor: '#707073'
						   },
						   yAxis: {
						      gridLineColor: '#FFFFFF',
						      labels: {
						         style: {
						            color: '#fff'
						         }
						      },
						      lineColor: '#fff'
						   },
						   tooltip: {
						      backgroundColor: 'rgba(0, 0, 0, 0.85)',
						      style: {
						         color: '#F0F0F0'
						      }
						   },
						   plotOptions: {
						      series: {
						         dataLabels: {
						            color: '#000'
						         },
						         marker: {
						            lineColor: '#333'
						         }
						      }
						   },
						   labels: {
						      style: {
						         color: '#707073'
						      }
						   },
						   // special colors for some of the
						   dataLabelsColor: '#000',
						   textColor: '#000'
						};
				/*-------------End of HighChart Theme option-----------*/
				/*-------------------bacteriological section----------*/			
						// Apply the theme
						Highcharts.setOptions(Highcharts.theme);
						function drawAllBarColors_bact() {
							var categories = new Array();
							var datas = new Array();
							var colors = new Array();
							$.each(topulb, function(index, value){
								categories.push(value);
								datas.push(toplpcd[index]);
								colors.push("#3C8A2E");
							});
							$.each(bottomulb, function(index, value){
								categories.push(value);
								datas.push(bottomlpcd[index]);
								colors.push("#000000");
							});
							var length;
							if(datas.length > 15){
								$(document).find("#allLPCD_bact").css({width:"1600px",height:"533px"});
								length = 10;
							}else if(datas.length > 5 && datas.length < 15){
								$(document).find("#allLPCD_bact").css({width:"1070px",height:"533px"});
								length = 35;
							}else{
								$(document).find("#allLPCD_bact").css({width:"600px",height:"533px"});
								length = 50;
							}
						    Highcharts.chart('allLPCD_bact', {
						    	chart: {
						            type: 'column'
						        },
						        plotOptions: {
						            column: {
						            	colorByPoint: true,
						                stacking: 'normal',
						                dataLabels: {
						                    enabled: false,
						                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
						                }
						            },
						            series: { 
					                    pointWidth: length//width of the column bars irrespective of the chart size
					                }
						        },
					            colors: colors,
						        title: {
						            text: ''
						        },
						        
						        xAxis: {
						            categories: categories,
						            tickPositioner: function() {
						            	var r = [];
						            	 for(var i=0; i<this.categories.length; i++){
						            	   r.push(i);
						            	 }
						            	 return r;
						            	}
						        },
						        yAxis: {
						        	labels: {
						                enabled: false
						            },
						            tickWidth: 0,
						             /*plotLines: [{
						                color: '#FF0000',
						                width: 2,
						                value: 135,
						                label: {
						                    text: '135',
						                    verticalAlign: 'bottom',
						                    textAlign: 'left',
						                    y: 3,
						                    x: -25,
						                    zIndex: 999
						                }
						            }],*/
						            title: {
						            	enabled: true,
						            	style: {
						                    color: 'rgba(0, 0, 0, 0)'
						                }
						            }
						        },
						        legend: {
						            enabled:false
						        },
						        tooltip: {
						        	enabled: true,
						        	valueSuffix: ' %'
						        },
						        
						        series: [ {
						            name: 'Water Testing',
						            data: datas
						        }]
						    });
						}
				/*---------------------frc section---------------*/
						function drawAllBarColors_frc() {
							var categories = new Array();
							var datas = new Array();
							var colors = new Array();
							$.each(topulb1, function(index, value){
								categories.push(value);
								datas.push(toplpcd1[index]);
								colors.push("#3C8A2E");
							});
							$.each(bottomulb1, function(index, value){
								categories.push(value);
								datas.push(bottomlpcd1[index]);
								colors.push("#000000");
							});
							var length;
							if(datas.length > 15){
								$(document).find("#allLPCD_frc").css({width:"1600px",height:"533px"});
								length = 10;
							}else if(datas.length > 5 && datas.length < 15){
								$(document).find("#allLPCD_frc").css({width:"1070px",height:"533px"});
								length = 35;
							}else{
								$(document).find("#allLPCD_frc").css({width:"600px",height:"533px"});
								length = 50;
							}
						    Highcharts.chart('allLPCD_frc', {
						    	chart: {
						            type: 'column'
						        },
						        plotOptions: {
						            column: {
						            	colorByPoint: true,
						                stacking: 'normal',
						                dataLabels: {
						                    enabled: false,
						                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
						                }
						            },
						            series: { 
					                    pointWidth: length//width of the column bars irrespective of the chart size
					                }
						        },
					            colors: colors,
						        title: {
						            text: ''
						        },
						        
						        xAxis: {
						            categories: categories,
						            tickPositioner: function() {
						            	var r = [];
						            	 for(var i=0; i<this.categories.length; i++){
						            	   r.push(i);
						            	 }
						            	 return r;
						            	}
						        },
						        yAxis: {
						        	labels: {
						                enabled: false
						            },
						            tickWidth: 0,
						             /*plotLines: [{
						                color: '#FF0000',
						                width: 2,
						                value: 135,
						                label: {
						                    text: '135',
						                    verticalAlign: 'bottom',
						                    textAlign: 'left',
						                    y: 3,
						                    x: -25,
						                    zIndex: 999
						                }
						            }],*/
						            title: {
						            	enabled: true,
						            	style: {
						                    color: 'rgba(0, 0, 0, 0)'
						                }
						            }
						        },
						        legend: {
						            enabled:false
						        },
						        tooltip: {
						        	enabled: true,
						        	valueSuffix: ' %'
						        },
						        
						        series: [ {
						            name: 'Water Testing',
						            data: datas
						        }]
						    });
						}
				
				/**
				* progress Report
				*/
				
				Highcharts.theme = {
						colors: ['#3c8a2e'],
					
						   xAxis: {
						      gridLineColor: '#fff',
						      labels: {
						         style: {
						            color: '#000'
						         }
						      },
						      lineColor: '#707073',
						      minorGridLineColor: '#505053',
						      tickColor: '#707073'
						   },
						   yAxis: {
						      gridLineColor: '#FFFFFF',
						      labels: {
						         style: {
						            color: '#000'
						         }
						      },
						      lineColor: '#fff',
						      minorGridLineColor: '#505053',
						      tickColor: '#707073'
						   },
						   tooltip: {
						      backgroundColor: 'rgba(0, 0, 0, 0.85)',
						      style: {
						         color: '#F0F0F0'
						      }
						   },
						   plotOptions: {
						      series: {
						         dataLabels: {
						            color: '#000'
						         },
						         marker: {
						            lineColor: '#333'
						         }
						      }
						   },
						   labels: {
						      style: {
						         color: '#707073'
						      }
						   },
						   // special colors for some of the
						   dataLabelsColor: '#000',
						   textColor: '#000'
						};

						// Apply the theme
						Highcharts.setOptions(Highcharts.theme);
			      		/*-------------End of HighChart Theme option-----------*/
						/*-------------------bacteriological section----------*/							
						function drawProgressBarColors_bact() {
							if($("#disSelect").val() != "District"){
								$(document).find("#container_bact").css({width:"660px",height:"533px"});
							}
		      		Highcharts.setOptions({
						colors: ['#A0DCFF']
					});
			      		
					    Highcharts.chart('container_bact', {
					        chart: {
					            type: 'area'
					        },
					        title: {
					            text: ''
					        },
					        legend: {
					            enabled: false
					        },
					        xAxis: {
					            allowDecimals: false,
					            categories: datesBetween,
					        	tickPositioner: function() {
									var r = [];
									 for(var i=0; i<this.categories.length; i++){
									   r.push(i);
									 }
									 return r;
								   },
					            labels: {
					                
					            }
					        },
					        yAxis: {
					            labels: {
					                enabled: false
					            },
					            tickWidth: 0,
					             /*plotLines: [{
					                color: '#FF0000',
					                width: 2,
					                value: 135,
					                label: {
					                    text: '135',
					                    verticalAlign: 'bottom',
					                    textAlign: 'left',
					                    y: 3,
					                    x: -25,
					                    zIndex: 999
					                }
					            }],*/
					            title: {
					            	enabled: true,
					            	style: {
					                    color: 'rgba(0, 0, 0, 0)'
					                }
					            }
					        },
					        tooltip: {
					        	pointFormat: '<b>{point.y} %</b><br/>',
					           enabled: true
					        },
					        plotOptions: {
					            area: {
					                marker: {
					                    enabled: false,
					                    symbol: 'circle',
					                    radius: 2,
					                    states: {
					                        hover: {
					                            enabled: true
					                        }
					                    }
					                }
					            }
					        },
					        series: [{
					            name: '',
					            data: currentLPCD
					        }
					       /* ,
					        {
					            name: 'Previous',
					            data: previousLPCD
					        }*/
					        ]
					    });
					}
			      	
			  /*-------------------frc section----------------*/
						function drawProgressBarColors_frc() {
							if($("#disSelect").val() != "District"){
								$(document).find("#container_frc").css({width:"660px",height:"533px"});
							}
			      		Highcharts.setOptions({
			      			colors: ['#A0DCFF']
						});
				      		
						    Highcharts.chart('container_frc', {
						        chart: {
						            type: 'area'
						        },
						        title: {
						            text: ''
						        },
						        legend: {
						            enabled: false
						        },
						        xAxis: {
						            allowDecimals: false,
						            categories: datesBetween,
						        	tickPositioner: function() {
										var r = [];
										 for(var i=0; i<this.categories.length; i++){
										   r.push(i);
										 }
										 return r;
									   },
						            labels: {
						                
						            }
						        },
						        yAxis: {
						            labels: {
						                enabled: false
						            },
						            tickWidth: 0,
						            title: {
						            	enabled: true,
						            	style: {
						                    color: 'rgba(0, 0, 0, 0)'
						                }
						            }
						        },
						        tooltip: {
						        	pointFormat: '<b>{point.y} %</b><br/>',
						           enabled: true
						        },
						        plotOptions: {
						            area: {
						                marker: {
						                    enabled: false,
						                    symbol: 'circle',
						                    radius: 2,
						                    states: {
						                        hover: {
						                            enabled: true
						                        }
						                    }
						                }
						            }
						        },
						        series: [{
						            name: '',
						            data: currentLPCD1
						        }]
						    });
						}
			      	
		      	
/**
 * ajax Calls
 */
			      	
			      	var isAmrut = false;
					$(".btn-amrut").click(function(){
						isAmrut = true;
						getDataEntryStatus();
					});
					
					function genarateQueryStr(){
						/* amrutTypeId, Date, month, divisionId, districtId, ulbId  */
						var queryStr = "";
						if(isAmrut){
							queryStr += "amrutTypeId=1";
						}
						
						if($("[name='inlineRadioOptions']:checked").val() == "daily"){
							if(queryStr != "")
								queryStr += "&"
							queryStr += "Date="+dateFormatter($("#dashboard_date").val());
							
						}
						else{
							if(queryStr != "")
								queryStr += "&"
							queryStr += "month="+$("select[name='month']").val();
							queryStr += "&selectedYear="+$("select[name='selectedYear']").val();
						}
						
						if($("#ulbSelect").val() != "ULB"){
							queryStr += "&"
							queryStr += "ulbId="+$("#ulbSelect").val();
						}
						else if($("#disSelect").val() != "District"){
							queryStr += "&"
							queryStr += "districtId="+$("#disSelect").val();
						}
						else if($("#divisionId").val() != "ALL"){
							queryStr += "&"
							queryStr += "divisionId="+$("#divisionId").val();
						}
						return queryStr;
					}
				
					var parentAvgHtml = "";
					
					function dashboardAjaxDailyWaterSupplyModels(){
						var queryStr = genarateQueryStr();
						
						$.ajax({
							url: "./ajax-water-testing?"+queryStr,
							async: false,
							success: function(datas){
								var bact_average = datas.bacteriologicalBoxValue;
								var topValue = datas.topValue;
								var frc_average = datas.frcBoxValue;

								lpcdMap = datas.lpcdMap;
								top10ulb = datas.top10ulb;
								top10lpcd = datas.top10lpcd;
								bottom10ulb = datas.bottom10ulb;
								bottom10lpcd = datas.bottom10lpcd;
								datesBetween = datas.datesBetween;
								currentLPCD = datas.currentLPCD;
								previousLPCD = datas.previousLPCD;
								
								top10ulb1 = datas.top10ulb1;
								top10lpcd1 = datas.top10lpcd1;
								bottom10ulb1 = datas.bottom10ulb1;
								bottom10lpcd1 = datas.bottom10lpcd1;
								currentLPCD1 = datas.currentLPCD1;
								previousLPCD1 = datas.previousLPCD1;

								topulb = datas.topulb;
								toplpcd = datas.toplpcd;
								bottomulb = datas.bottomulb;
								bottomlpcd = datas.bottomlpcd;
								
								topulb1 = datas.topulb1;
								toplpcd1 = datas.toplpcd1;
								bottomulb1 = datas.bottomulb1;
								bottomlpcd1 = datas.bottomlpcd1;
								flag = datas.flag;
								flag1 = datas.flag1;
								
								$("#bacteriological #average").html(bact_average);
								$("#frc #average").html(frc_average);
								$(".topValue").html(topValue);
								
								
								var usrType = "admin"
								if($("#ulbSelect").val() != "ULB"){
									usrType = "ulb";
									$(".area").html($("#ulbSelect option:selected").text());
								}
								else if($("#disSelect").val() != "District"){
									usrType = "district";
									$(".area").html($("#disSelect option:selected").text());
								}
								else if($("#divisionId").val() != "ALL"){
									usrType = "division";
									$(".area").html($("#divisionId option:selected").text());
								}
								else{
									$(".area").html("Odisha");
								}
								changeUserTypeDiv(usrType);
								
								drawTopBarColors_bact();	
								drawBottomBarColors_bact();
								drawProgressBarColors_bact();
								drawAllBarColors_bact();
								
								drawTopBarColors_frc();
								drawBottomBarColors_frc();
								drawProgressBarColors_frc();
								drawAllBarColors_frc();
							}
						});
					}
					
					function getYearAndMonth(obj){
			   			var dateObjArr = dashboardDateObj.maxDate.split("-");
			   			obj.year = dateObjArr[2];
			   			obj.month = dateObjArr[1];
			   			return obj;
			   		}
			   		
			   		function getMonthList(dateObj, selectedYear){
			   			var monthNames = [
			   			               "January", "February", "March",
			   			               "April", "May", "June",
			   			               "July", "August", "September",
			   			               "October", "November", "December"
			   			               ];
			   			if(selectedYear == dateObj.year){
			   				monthNames = jQuery.map(monthNames, function(value, index){
			   					if(Number(dateObj.month) >= (index+1)){
			   						return value;
			   					}
			   					else{
			   						return null;
			   					}
			   				});
			   			}
			   			
			   			return monthNames;
			   		}
			   		
			   		function printYearAndMonth(monthArr, endYear, selectedMonth, selectedYear){
			   			$("select[name='selectedYear']").html("");
			   			$("select[name='month']").html("");
			   			
			   			for(year = 2016; year<=endYear; year++){
			   				jQuery("<option/>",{
			   					value:year,
			   					text:year
			   				}).appendTo("select[name='selectedYear']");
			   			}
			   			
			   			$("select[name='selectedYear'] option[value='"+ selectedYear +"']").prop("selected", true);
			   			
			   			jQuery.each(monthArr, function(index, value){
			   				jQuery("<option/>",{
			   					value:(index+1),
			   					text:value
			   				}).appendTo("select[name='month']");
			   			});
			   			
			   			$("select[name='month'] option[value='"+ Number(selectedMonth) +"']").prop("selected", true);
			   		}
					
					$("input[name='inlineRadioOptions']").change(function(){
						if($("input[value='daily']").prop("checked")){
							$(".dateFetch").html($("#dashboard_date").val());
							$("select[name='month']").prop("disabled", true);
							$("select[name='selectedYear']").prop("disabled", true);
							$("select[name='selectedYear']").parent().css("display", "none");
							$("#dashboard_date").prop("disabled", false);
						}
						else{
							$(".dateFetch").html($("select[name='month'] option:selected").text());
							$("select[name='month']").prop("disabled", false);
							$("select[name='selectedYear']").prop("disabled", false);
							$("select[name='selectedYear']").parent().css("display", "block");
							$("#dashboard_date").prop("disabled", true);
							
							var dateObj = {};
							getYearAndMonth(dateObj);
							var monthArr = getMonthList(dateObj, dateObj.year);
							printYearAndMonth(monthArr, dateObj.year,dateObj.month, dateObj.year);
						}
						getDataEntryStatus();
					});
					
					var isFirst = 0;
					$("#dashboard_date").change(function(){
						var that = this;
						if($("input[value='daily']").prop("checked")){
							if(!defaultDailyDt || ($(that).val() != defaultDailyDt)){
								defaultDailyDt = $(that).val();
								$(".dateFetch").html($(that).val());
								var dateArr = $(that).val().split("-");
								$("select[name='month'] option[value='"+ dateArr[1] +"']").prop("selected", true);
								$("select[name='selectedYear'] option[value='"+ dateArr[2] +"']").prop("selected", true);
								getDataEntryStatus();
							}
							
						}
					});
					
					$("select[name='month']").change(function(){
						var that = this;
						if($("input[value='monthly']").prop("checked")){
							$(".dateFetch").html($(that).children(":selected").text());
							
							getDataEntryStatus();
						}
					});
					
					$("select[name='selectedYear']").change(function(){
			   			var that = this;
			   			if($("input[value='monthly']").prop("checked")){
			   				$(".dateFetch").html($(that).children(":selected").text());
			   				
			   				var dateObj = {};
							getYearAndMonth(dateObj);
							var monthArr = getMonthList(dateObj, $(this).val());
							printYearAndMonth(monthArr, dateObj.year, $("input[value='monthly']").val(), $(this).val());
			   				
			   				getDataEntryStatus();
			   			}
			   		});
					
					function showOnlyAllLpcd(){
						if($("#disSelect").val() == "District"){
							if($("#myModal #echart_bar_horizontal2").length != 0){
								$("#myModal #echart_bar_horizontal2").attr("id", "echart_bar_horizontal1");
								$("#echart_bar_horizontal1").attr("id", "echart_bar_horizontal2");
								$("#echart_bar_horizontal2").css("height","250px");
								$("#echart_bar_horizontal2").parent().css("height","230px");
								$("#echart_bar_horizontal3").parent().parent().css("display", "block");
								$("#echart_bar_horizontal1").css("height","2000px");
								$("#echart_bar_horizontal1").parent().css("height", "");
							}
							
						}
						else{
							$("#echart_bar_horizontal2").attr("id", "echart_bar_horizontal1");
							$("#myModal #echart_bar_horizontal1").attr("id", "echart_bar_horizontal2");
							$("#echart_bar_horizontal1").css("height","953px");
							$("#echart_bar_horizontal1").parent().css("height","528px");
							$("#echart_bar_horizontal3").parent().parent().css("display", "none");
							
						}
					}
					 
					function getDataEntryStatus(){
								if(isFirst<2){
									
									isFirst++;
									return false;
								}
								else{
									dashboardAjaxDailyWaterSupplyModels();
									initialize_bact();
									initialize_frc();
								}
					}
					
