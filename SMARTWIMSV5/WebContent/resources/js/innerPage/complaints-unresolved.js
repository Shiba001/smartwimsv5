function changeUserTypeDiv(usrType){

	if(usrType == 'admin' ){
			$('.progressBr').find('.progressChart').removeAttr('id');
			$('.progressBr').hide();
			if($('.progressBr').find('.progressChart').attr('id')){
				$('.progressBr').find('.progressChart').removeAttr('id');
			}
			
			if(!$('#myModal').find('.LPCD-chart').attr('id')){
				$('#myModal').find('.LPCD-chart').attr('id','allLPCD');
			}
			
			$('#myModal_progressbar').find('.progressChart').attr('id','container');
			$('.barContainer, .progressBtn, .allUlbBtn').show();
		
		
	
	}else if(usrType == 'division'){
		
		if(flag == "false" || flag == false){
				$('.progressBr').find('.progressChart').removeAttr('id');
				$('.progressBr').hide();
				if($('.progressBr').find('.progressChart').attr('id')){
					$('.progressBr').find('.progressChart').removeAttr('id');
				}
				
				if(!$('#myModal').find('.LPCD-chart').attr('id')){
					$('#myModal').find('.LPCD-chart').attr('id','allLPCD');
				}
				
				$('#myModal_progressbar').find('.progressChart').attr('id','container');
				$('.barContainer, .progressBtn, .allUlbBtn').show();
			}
			else{
				$('.barContainer, .allUlbBtn').hide();
				if($('#myModal').find('.LPCD-chart').attr('id')){
					$('#myModal').find('.LPCD-chart').removeAttr('id');
				}
				
				if(!$('#myModal_progressbar').find('.progressChart').attr('id')){
					$('#myModal_progressbar').find('.progressChart').attr('id','container');
				}
				
				$('.progressBr').find('.progressChart').attr('id','allLPCD');
				$('.progressBr h5').text('All ULB');
				$('.progressBr, .progressBtn').show();
			}
			
		
	}else if(usrType == 'district'){
		
		$('.barContainer, .progressBtn').hide();
		$('.progressBr').find('.progressChart').removeAttr('id');
		if($('#myModal_progressbar').find('.progressChart').attr('id')){
			$('#myModal_progressbar').find('.progressChart').removeAttr('id');
		}
		
		if(!$('#myModal').find('.LPCD-chart').attr('id')){
			$('#myModal').find('.LPCD-chart').attr('id','allLPCD');
		}
		
		$('.progressBr').find('.progressChart').attr('id','container');
		$('.progressBr h5').text('Progress Report');
		$('.progressBr, .allUlbBtn').show();
	}else if(usrType == 'ulb'){
		$('.barContainer, .progressBtn, .allUlbBtn').hide();
		if($('#myModal_progressbar').find('.progressChart').attr('id')){
			$('#myModal_progressbar').find('.progressChart').removeAttr('id');
		}
		$('.progressBr').find('.progressChart').attr('id','container');
		$('.progressBr h5').text('Progress Report');
		$('.progressBr').show();
	}
}
$(document).ready(function(){
	var usrType = "";
	var userLevelId = $("script[src$='dashboard-header.js']").attr("data-user-level-id");
	if(userLevelId == 4){
		usrType = "ulb";
	} else if(userLevelId == 3){
		usrType = "district";
	} else if(userLevelId == 2){
		usrType = "division";
	} else {
		usrType = "admin";
	}
	
	
	
	changeUserTypeDiv(usrType);
	drawTopBarColors();
	drawBottomBarColors();
	drawAllBarColors();
	drawProgressBarColors();
});

/**
		 * Map Implementation 
		 */
		
		var map = null;

		function initialize() {
			var mapOptions = {
				zoom : 10
			};
			map = new google.maps.Map(document.getElementById('map-canvas'),
					mapOptions);
			bounds = new google.maps.LatLngBounds();
			var pinColor = "";
			$.each(lpcdMap, function(index, value){
				if(value.complaintsResolved >= 100 ){
					pinColor = "008000";
				} else {
					pinColor = "FF0000";
				}
				
				setVendorMarker(value.ulbNameModel.ulbModel.latitude, value.ulbNameModel.ulbModel.longitude, index, pinColor, value.currentMonth, value.previousMonth,value.total_comp_resolved, value.pm_total_comp_resolved);
			});
				
				
				
			
			map.fitBounds(bounds);
		}
		var bounds = null;
		var infowindow = false;
		function setVendorMarker(latitude, longitude, ulb_name, pinColor,currentMonth, previousMonth, total_comp_resolved, pm_total_comp_resolved) {
			var location = new google.maps.LatLng(latitude, longitude);
			//map.setCenter(location);
		    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
		    		new google.maps.Size(21, 34),
		            new google.maps.Point(0,0),
		            new google.maps.Point(10, 34));
			var marker = new google.maps.Marker({
				icon: pinImage,
				position : location,
				map : map,
				title : ulb_name
			});
			
			
			bounds.extend(marker.position);
			google.maps.event.addListener(marker, 'click', function(){
				
				var contentString = '<div id="content">'+
	            '<div id="siteNotice">'+
	            '</div>'+
	            '<div id="bodyContent">'+
	            '<table border="1" width="100%"><tr><td rowspan="3" class="thead">'+ulb_name+'</td><td colspan="2" align="center" bgcolor="#C4D600" class="text-center">Monthly Average</td></tr><tr><td bgcolor="#e3e3e3">'+currentMonth+'</td><td bgcolor="#e3e3e3">'+previousMonth+'</td></tr><tr><td style="color:#'+pinColor+';font-weight:bold;">'+total_comp_resolved+' %</td><td style="color:#'+pinColor+';font-weight:bold;">'+pm_total_comp_resolved+' %</td></tr></table>'+
	            '</div>'+
	            '</div>';
				
				if( infowindow ) {
		        	infowindow.close();
		         }
				
	          infowindow = new google.maps.InfoWindow({
	          content: contentString
	        });
	        
				 infowindow.open(map, this);
			});

			// To add the marker to the map, call setMap();
			marker.setMap(map);
			
		}

		$(document).ready(function(){
			initialize();
		});
		//google.maps.event.addDomListener(window, 'load', initialize);
		
		
		
		
		
		
		/**
		* Top 10 LPCD
		*/


			Highcharts.theme = {
			   xAxis: {
			      gridLineColor: '#fff',
			      labels: {
			         style: {
			            color: '#000'
			         }
			      },
			      lineColor: '#707073',
			      minorGridLineColor: '#505053',
			   },
			   yAxis: {
			      gridLineColor: '#FFFFFF',
			      labels: {
			         style: {
			            color: '#fff'
			         }
			      },
			      lineColor: '#fff',
			      minorGridLineColor: '#505053',
			      tickColor: '#707073',
			      tickWidth: 1,
			   },
			   tooltip: {
			      backgroundColor: 'rgba(0, 0, 0, 0.85)',
			      style: {
			         color: '#F0F0F0'
			      }
			   },
			   plotOptions: {
			      series: {
			         dataLabels: {
			            color: '#B0B0B3'
			         },
			         marker: {
			            lineColor: '#333'
			         }
			      },
			      boxplot: {
			         fillColor: '#505053'
			      }
			   },
			   labels: {
			      style: {
			         color: '#707073'
			      }
			   },

			   // special colors for some of the
			   dataLabelsColor: '#B0B0B3',
			   textColor: '#C0C0C0',
			   contrastTextColor: '#F0F0F3',
			   maskColor: 'rgba(255,255,255,0.3)'
			};
			// Apply the theme
			Highcharts.setOptions(Highcharts.theme);
			function drawTopBarColors() {
				Highcharts.setOptions({
					colors: ['#3c8a2e']
				});
			    Highcharts.chart('top10LPCD', {
			        chart: {
			            type: 'bar'
			        },
			        title: {
			            text: ''
			        },
			        subtitle: {
			            text: ''
			        },
			        xAxis: {
			            categories: top10ulb,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            max:130,
			            visible: false
			        },
			        tooltip: {
			            enabled: false
			        },
			        plotOptions: {
			            bar: {
			                dataLabels: {
			                    enabled: true,
			                    format: '{y} %'
			                }
			            }
			        },
			        legend: {
			            enabled: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: [{
			            name: 'Year 1800',
			            data: top10lpcd
			        }]
			    });
			}
			
			
			
			/**
			* Bottom 10 LPCD
			*/

				Highcharts.theme = {
				   xAxis: {
				      gridLineColor: '#fff',
				      labels: {
				         style: {
				            color: '#000'
				         }
				      },
				      lineColor: '#707073',
				      minorGridLineColor: '#505053',
				      tickColor: '#707073'
				   },
				   yAxis: {
				      gridLineColor: '#FFFFFF',
				      labels: {
				         style: {
				            color: '#fff'
				         }
				      },
				      lineColor: '#fff',
				      minorGridLineColor: '#505053',
				      tickColor: '#707073'
				   },
				   tooltip: {
				      backgroundColor: 'rgba(0, 0, 0, 0.85)',
				      style: {
				         color: '#F0F0F0'
				      }
				   },
				   plotOptions: {
				      series: {
				         dataLabels: {
				            color: '#000'
				         },
				         marker: {
				            lineColor: '#333'
				         }
				      }
				   },
				   labels: {
				      style: {
				         color: '#707073'
				      }
				   },
				   // special colors for some of the
				   dataLabelsColor: '#000',
				   textColor: '#000'
				};

				// Apply the theme
				Highcharts.setOptions(Highcharts.theme);
				function drawBottomBarColors() {
					Highcharts.setOptions({
						colors: ['#c3c3c3']
					});
				    Highcharts.chart('bottom10LPCD', {
				        chart: {
				            type: 'bar'
				        },
				        title: {
				            text: ''
				        },
				        subtitle: {
				            text: ''
				        },
				        xAxis: {
				            categories: bottom10ulb,
				            title: {
				                text: null
				            }
				        },
				        yAxis: {
				            min: 0,
				            max:130,
				            visible: false
				        },
				        tooltip: {
				            enabled: false
				        },
				        plotOptions: {
				            bar: {
				                dataLabels: {
				                    enabled: true,
				                    format: '{y} %'
				                }
				            }
				        },
				        legend: {
				            enabled: false
				        },
				        credits: {
				            enabled: false
				        },
				        series: [{
				            name: 'Year 1800',
				            data: bottom10lpcd
				        }]
				    });
				}
				
				
			/*	allLPCD*/
				Highcharts.theme = {
						   xAxis: {
						      gridLineColor: '#fff',
						      labels: {
						         style: {
						            color: '#000'
						         }
						      },
						      lineColor: '#707073',
						      minorGridLineColor: '#505053',
						      tickColor: '#707073'
						   },
						   yAxis: {
						      gridLineColor: '#FFFFFF',
						      labels: {
						         style: {
						            color: '#fff'
						         }
						      },
						      lineColor: '#fff'
						   },
						   tooltip: {
						      backgroundColor: 'rgba(0, 0, 0, 0.85)',
						      style: {
						         color: '#F0F0F0'
						      }
						   },
						   plotOptions: {
						      series: {
						         dataLabels: {
						            color: '#000'
						         },
						         marker: {
						            lineColor: '#333'
						         }
						      }
						   },
						   labels: {
						      style: {
						         color: '#707073'
						      }
						   },
						   // special colors for some of the
						   dataLabelsColor: '#000',
						   textColor: '#000'
						};

						// Apply the theme
						Highcharts.setOptions(Highcharts.theme);
						function drawAllBarColors() {
							var categories = new Array();
							var datas = new Array();
							var colors = new Array();
							$.each(topulb, function(index, value){
								categories.push(value);
								datas.push(toplpcd[index]);
								colors.push("#3C8A2E");
							});
							
							$.each(bottomulb, function(index, value){
								categories.push(value);
								datas.push(bottomlpcd[index]);
								colors.push("#000000");
							});
							
							var length;
							if(datas.length > 15){
								$(document).find("#allLPCD").css({width:"1600px",height:"533px"});
								length = 10;
							}else if(datas.length > 5 && datas.length < 15){
								$(document).find("#allLPCD").css({width:"1070px",height:"515px"});
								length = 35;
							}else{
								$(document).find("#allLPCD").css({width:"600px",height:"533px"});
								length = 50;
							}
							
						    Highcharts.chart('allLPCD', {
						    	chart: {
						            type: 'column'
						        },
						        plotOptions: {
						            column: {
						            	colorByPoint: true,
						                stacking: 'normal',
						                dataLabels: {
						                    enabled: false,
						                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
						                }
						            },
						            series: { 
						            	pointWidth: length//width of the column bars irrespective of the chart size
						            	}
						        },
					            colors: colors,
						        title: {
						            text: ''
						        },
						        
						        xAxis: {
						            categories: categories,
						            tickPositioner: function() {
						            	var r = [];
						            	 for(var i=0; i<this.categories.length; i++){
						            	   r.push(i);
						            	 }
						            	 return r;
						            	}
						        },
						        yAxis: {
						        	labels: {
						                enabled: false
						            },
						            tickWidth: 0,
						             plotLines: [{
						                color: '#FF0000',
						                width: 2,
						                value: 135,
						                label: {
						                    text: '135',
						                    verticalAlign: 'bottom',
						                    textAlign: 'left',
						                    y: 3,
						                    x: -25,
						                    zIndex: 999
						                }
						            }],
						            title: {
						            	enabled: true,
						            	style: {
						                    color: 'rgba(0, 0, 0, 0)'
						                }
						            }
						        },
						        legend: {
						            enabled:false
						        },
						        tooltip: {
						        	enabled: true,
						        	valueSuffix: ' %'
						        },
						        
						        series: [ {
						            name: 'Complaint Resolved',
						            data: datas
						        }]
						    });
						}
				
				
				/**
				* progress Report
				*/
				
				Highcharts.theme = {
						colors: ['#3c8a2e'],
					
						   xAxis: {
						      gridLineColor: '#fff',
						      labels: {
						         style: {
						            color: '#000'
						         }
						      },
						      lineColor: '#707073',
						      minorGridLineColor: '#505053',
						      tickColor: '#707073'
						   },
						   yAxis: {
						      gridLineColor: '#FFFFFF',
						      labels: {
						         style: {
						            color: '#000'
						         }
						      },
						      lineColor: '#fff',
						      minorGridLineColor: '#505053',
						      tickColor: '#707073'
						   },
						   tooltip: {
						      backgroundColor: 'rgba(0, 0, 0, 0.85)',
						      style: {
						         color: '#F0F0F0'
						      }
						   },
						   plotOptions: {
						      series: {
						         dataLabels: {
						            color: '#000'
						         },
						         marker: {
						            lineColor: '#333'
						         }
						      }
						   },
						   labels: {
						      style: {
						         color: '#707073'
						      }
						   },
						   // special colors for some of the
						   dataLabelsColor: '#000',
						   textColor: '#000'
						};

						// Apply the theme
						Highcharts.setOptions(Highcharts.theme);
			      		/*-------------End of HighChart Theme option-----------*/
						
			      	
						function drawProgressBarColors() {
							if($("#disSelect").val() != "District"){
								$(document).find("#container").css({width:"660px",height:"533px"});
							}
		      		Highcharts.setOptions({
						colors: ['#A0DCFF']
					});
			      		
					    Highcharts.chart('container', {
					        chart: {
					            type: 'area'
					        },
					        title: {
					            text: ''
					        },
					        legend: {
					            enabled: false
					        },
					        xAxis: {
					            allowDecimals: false,
					            categories: datesBetween,tickPositioner: function() {
									var r = [];
									 for(var i=0; i<this.categories.length; i++){
									   r.push(i);
									 }
									 return r;
								   },
					            labels: {
					                
					            }
					        },
					        yAxis: {
					            labels: {
					                enabled: false
					            },
					            tickWidth: 0,
					          /*   plotLines: [{
					                color: '#FF0000',
					                width: 2,
					                value: 135,
					                label: {
					                    text: '135',
					                    verticalAlign: 'bottom',
					                    textAlign: 'left',
					                    y: 3,
					                    x: -25,
					                    zIndex: 999
					                }
					            }],*/
					            title: {
					            	enabled: true,
					            	style: {
					                    color: 'rgba(0, 0, 0, 0)'
					                }
					            }
					        },
					        tooltip: {
					        	pointFormat: '<b>{point.y} %</b><br/>',
					           enabled: true
					        },
					        plotOptions: {
					            area: {
					                marker: {
					                    enabled: false,
					                    symbol: 'circle',
					                    radius: 2,
					                    states: {
					                        hover: {
					                            enabled: true
					                        }
					                    }
					                }
					            }
					        },
					        series: [{
					            name: '',
					            data: currentLPCD
					        }
					       /* ,
					        {
					            name: 'Previous',
					            data: previousLPCD
					        }*/
					        ]
					    });
					}

			      	
		      	
/**
 * ajax Calls
 */
			      	
			      	var isAmrut = false;
					$(".btn-amrut").click(function(){
						isAmrut = true;
						getDataEntryStatus();
					});
					
					function genarateQueryStr(){
						/* amrutTypeId, Date, month, divisionId, districtId, ulbId  */
						var queryStr = "";
						if(isAmrut){
							queryStr += "amrutTypeId=1";
						}
						
						if($("[name='inlineRadioOptions']:checked").val() == "daily"){
							if(queryStr != "")
								queryStr += "&"
							queryStr += "Date="+dateFormatter($("#dashboard_date").val());
						}
						else{
							if(queryStr != "")
								queryStr += "&"
							queryStr += "month="+$("select[name='month']").val();
							queryStr += "&selectedYear="+$("select[name='selectedYear']").val();
						}
						
						if($("#ulbSelect").val() != "ULB"){
							queryStr += "&"
							queryStr += "ulbId="+$("#ulbSelect").val();
						}
						else if($("#disSelect").val() != "District"){
							queryStr += "&"
							queryStr += "districtId="+$("#disSelect").val();
						}
						else if($("#divisionId").val() != "ALL"){
							queryStr += "&"
							queryStr += "divisionId="+$("#divisionId").val();
						}
						return queryStr;
					}
			
					var parentAvgHtml = "";
					
					function dashboardAjaxDailyWaterSupplyModels(){
						var queryStr = genarateQueryStr();
						
						$.ajax({
							url: "./ajax-complaints-resolved?"+queryStr,
							async: false,
							success: function(datas){
								var average = datas.complaintResolved;
								var topValue = datas.topValue;

								lpcdMap = datas.lpcdMap;
								top10ulb = datas.top10ulb;
								top10lpcd = datas.top10lpcd;
								bottom10ulb = datas.bottom10ulb;
								bottom10lpcd = datas.bottom10lpcd;
								datesBetween = datas.datesBetween;
								currentLPCD = datas.currentLPCD;
								previousLPCD = datas.previousLPCD;
								flag = datas.flag;
								
								topulb =  datas.topulb;
								toplpcd =  datas.toplpcd;
								bottomulb =  datas.bottomulb;
								bottomlpcd =  datas.bottomlpcd;
								
								$("#average").html(average);
								$(".topValue").html(topValue);
								
								var usrType = "";
								if($("#ulbSelect").val() != "ULB"){
									usrType = "ulb";
									$("#area").html($("#ulbSelect option:selected").text());
								}
								else if($("#disSelect").val() != "District"){
									usrType = "district";
									$("#area").html($("#disSelect option:selected").text());
								}
								else if($("#divisionId").val() != "ALL"){
									usrType = "division";
									$("#area").html($("#divisionId option:selected").text());
								}
								else{
									usrType = "admin";
									$("#area").html("Odisha");
								}
								changeUserTypeDiv(usrType);
								
								initialize();
								drawTopBarColors();
								drawBottomBarColors();
								drawAllBarColors();
								drawProgressBarColors();
							}
						});
					}
					
					function getYearAndMonth(obj){
			   			var dateObjArr = dashboardDateObj.maxDate.split("-");
			   			obj.year = dateObjArr[2];
			   			obj.month = dateObjArr[1];
			   			return obj;
			   		}
			   		
			   		function getMonthList(dateObj, selectedYear){
			   			var monthNames = [
			   			               "January", "February", "March",
			   			               "April", "May", "June",
			   			               "July", "August", "September",
			   			               "October", "November", "December"
			   			               ];
			   			if(selectedYear == dateObj.year){
			   				monthNames = jQuery.map(monthNames, function(value, index){
			   					if(Number(dateObj.month) >= (index+1)){
			   						return value;
			   					}
			   					else{
			   						return null;
			   					}
			   				});
			   			}
			   			
			   			return monthNames;
			   		}
			   		
			   		function printYearAndMonth(monthArr, endYear, selectedMonth, selectedYear){
			   			$("select[name='selectedYear']").html("");
			   			$("select[name='month']").html("");
			   			
			   			for(year = 2016; year<=endYear; year++){
			   				jQuery("<option/>",{
			   					value:year,
			   					text:year
			   				}).appendTo("select[name='selectedYear']");
			   			}
			   			
			   			$("select[name='selectedYear'] option[value='"+ selectedYear +"']").prop("selected", true);
			   			
			   			jQuery.each(monthArr, function(index, value){
			   				jQuery("<option/>",{
			   					value:(index+1),
			   					text:value
			   				}).appendTo("select[name='month']");
			   			});
			   			
			   			$("select[name='month'] option[value='"+ Number(selectedMonth) +"']").prop("selected", true);
			   		}
					
					$("input[name='inlineRadioOptions']").change(function(){
						if($("input[value='daily']").prop("checked")){
							$(".dateFetch").html($("#dashboard_date").val());
							$("select[name='month']").prop("disabled", true);
							$("select[name='selectedYear']").prop("disabled", true);
							$("select[name='selectedYear']").parent().css("display", "none");
							$("#dashboard_date").prop("disabled", false);
						}
						else{
							$(".dateFetch").html($("select[name='month'] option:selected").text());
							$("select[name='month']").prop("disabled", false);
							$("select[name='selectedYear']").prop("disabled", false);
							$("select[name='selectedYear']").parent().css("display", "block");
							$("#dashboard_date").prop("disabled", true);
							
							var dateObj = {};
							getYearAndMonth(dateObj);
							var monthArr = getMonthList(dateObj, dateObj.year);
							printYearAndMonth(monthArr, dateObj.year,dateObj.month, dateObj.year);
						}
						getDataEntryStatus();
					});
					
					var isFirst = 0;
					var defaultDailyDt;
					$("input#dashboard_date").change(function(){
						
						var that = this;
						if(!defaultDailyDt || ($(that).val() != defaultDailyDt)){
							defaultDailyDt = $(that).val();
							$(".dateFetch").html($(that).val());
							var dateArr = $(that).val().split("-");
							$("select[name='month'] option[value='"+ dateArr[1] +"']").prop("selected", true);
							$("select[name='selectedYear'] option[value='"+ dateArr[2] +"']").prop("selected", true);
							getDataEntryStatus();
						}
					});
					
					$("select[name='month']").change(function(){
						var that = this;
						if($("input[value='monthly']").prop("checked")){
							$(".dateFetch").html($(that).children(":selected").text());
							
							getDataEntryStatus();
						}
					});
					
					$("select[name='selectedYear']").change(function(){
			   			var that = this;
			   			if($("input[value='monthly']").prop("checked")){
			   				$(".dateFetch").html($(that).children(":selected").text());
			   				
			   				
			   				var dateObj = {};
							getYearAndMonth(dateObj);
							var monthArr = getMonthList(dateObj, $(this).val());
							printYearAndMonth(monthArr, dateObj.year, $("input[value='monthly']").val(), $(this).val());
			   				
			   				getDataEntryStatus();
			   			}
			   		});
					
					function showOnlyAllLpcd(){
						if($("#disSelect").val() == "District"){
							if($("#myModal #echart_bar_horizontal2").length != 0){
								//$("#myModal #echart_bar_horizontal2").html("");
								$("#myModal #echart_bar_horizontal2").attr("id", "echart_bar_horizontal1");
								$("#echart_bar_horizontal1").attr("id", "echart_bar_horizontal2");
								$("#echart_bar_horizontal2").css("height","250px");
								$("#echart_bar_horizontal2").parent().css("height","230px");
								
								$("#echart_bar_horizontal3").parent().parent().css("display", "block");
								$("#echart_bar_horizontal1").css("height","2000px");
								$("#echart_bar_horizontal1").parent().css("height", "");
							}
							
						}
						else{
//							$("#echart_bar_horizontal2").html("");
							$("#echart_bar_horizontal2").attr("id", "echart_bar_horizontal1");
							$("#myModal #echart_bar_horizontal1").attr("id", "echart_bar_horizontal2");
							$("#echart_bar_horizontal1").css("height","953px");
							$("#echart_bar_horizontal1").parent().css("height","528px");
							
							$("#echart_bar_horizontal3").parent().parent().css("display", "none");
							
						}
					}
					
					function getDataEntryStatus(){
						var userLevelId = $("script[src$='dashboard-header.js']").attr("data-user-level-id");
						var call = 0;
						if(userLevelId == 3){
							call = 3;
						}
						else {
							call = 2;
						}
								if(isFirst<call){
									
									isFirst++;
									return false;
								}
								else{
									//showOnlyAllLpcd();
									dashboardAjaxDailyWaterSupplyModels();
									
									
									initialize();
								}
					}
					
