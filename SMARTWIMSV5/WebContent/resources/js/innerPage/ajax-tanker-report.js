		var isAmrut = false;
		$(".btn-amrut").click(function(){
			isAmrut = true;
			getDataEntryStatus();
		});
		
		function genarateQueryStr(){
			/* amrutTypeId, Date, month, divisionId, districtId, ulbId  */
			var queryStr = "";
			if(isAmrut){
				queryStr += "amrutTypeId=1";
			}
			
			if($("[name='inlineRadioOptions']:checked").val() == "daily"){
   				if(queryStr != "")
   					queryStr += "&"
   				queryStr += "Date="+dateFormatter($("#dashboard_date").val()); 
   			}
   			else{
   				if(queryStr != "")
   					queryStr += "&"
   				queryStr += "month="+$("select[name='month']").val();
   				queryStr += "&selectedYear="+$("select[name='selectedYear']").val();
   			}
			
			if($("#ulbSelect").val() != "ULB"){
				queryStr += "&"
				queryStr += "ulbId="+$("#ulbSelect").val();
			}
			else if($("#disSelect").val() != "District"){
				queryStr += "&"
				queryStr += "districtId="+$("#disSelect").val();
			}
			else if($("#divisionId").val() != "ALL"){
				queryStr += "&"
				queryStr += "divisionId="+$("#divisionId").val();
			}
			return queryStr;
		}
		
		
		function dashboardAjaxDailyWaterSupplyModels(){
			var queryStr = genarateQueryStr();
			
			$.ajax({
				url: "./ajax-tanker-report?"+queryStr,
				async: false,
				success: function(datas){
					$(".data-tab").html(datas);
					//console.log(datas);
					/*$("#datatable-buttons2").DataTable({
						dom : "Bfrtip",
						paging : false,
						buttons : [ {
							extend : "copy",
							className : "btn-sm"
						}, {
							extend : "csv",
							className : "btn-sm"
						}, {
							extend : "excel",
							className : "btn-sm"
						}, {
							extend : "pdf",
							className : "btn-sm"
						}, {
							extend : "print",
							className : "btn-sm"
						}, ],
						responsive : true
					});*/
				}
			});
		}
		
		function getYearAndMonth(obj){
   			var dateObjArr = dashboardDateObj.maxDate.split("-");
   			obj.year = dateObjArr[2];
   			obj.month = dateObjArr[1];
   			return obj;
   		}
   		
   		function getMonthList(dateObj, selectedYear){
   			var monthNames = [
   			               "January", "February", "March",
   			               "April", "May", "June",
   			               "July", "August", "September",
   			               "October", "November", "December"
   			               ];
   			if(selectedYear == dateObj.year){
   				monthNames = jQuery.map(monthNames, function(value, index){
   					if(Number(dateObj.month) >= (index+1)){
   						return value;
   					}
   					else{
   						return null;
   					}
   				});
   			}
   			
   			return monthNames;
   		}
   		
   		function printYearAndMonth(monthArr, endYear, selectedMonth, selectedYear){
   			$("select[name='selectedYear']").html("");
   			$("select[name='month']").html("");
   			
   			for(year = 2016; year<=endYear; year++){
   				jQuery("<option/>",{
   					value:year,
   					text:year
   				}).appendTo("select[name='selectedYear']");
   			}
   			
   			$("select[name='selectedYear'] option[value='"+ selectedYear +"']").prop("selected", true);
   			
   			jQuery.each(monthArr, function(index, value){
   				jQuery("<option/>",{
   					value:(index+1),
   					text:value
   				}).appendTo("select[name='month']");
   			});
   			
   			$("select[name='month'] option[value='"+ Number(selectedMonth) +"']").prop("selected", true);
   		}
		
		$("input[name='inlineRadioOptions']").change(function(){
   			if($("input[value='daily']").prop("checked")){
   				$(".dateFetch").html($("#dashboard_date").val());
   				$("select[name='month']").prop("disabled", true);
				$("#dashboard_date").prop("disabled", false);
				$("select[name='selectedYear']").prop("disabled", true);
   				$("select[name='selectedYear']").parent().css("display", "none");
   			}
   			else{
   				$(".dateFetch").html($("select[name='month'] option:selected").text());
   				$("select[name='month']").prop("disabled", false);
				$("#dashboard_date").prop("disabled", true);
				$("select[name='selectedYear']").prop("disabled", false);
   				$("select[name='selectedYear']").parent().css("display", "block");
   				
   				var dateObj = {};
				getYearAndMonth(dateObj);
				var monthArr = getMonthList(dateObj, dateObj.year);
				printYearAndMonth(monthArr, dateObj.year,dateObj.month, dateObj.year);
   			}
   			getDataEntryStatus();
   		});
		var defaultDailyDt;
		var isFirst = 0;
		$("input#dashboard_date").change(function(){
			
			var that = this;
			if(!defaultDailyDt || ($(that).val() != defaultDailyDt)){
				$(".dateFetch").html($(that).val());
				var dateArr = $(that).val().split("-");
				$("select[name='month'] option[value='"+ dateArr[1] +"']").prop("selected", true);
				
				getDataEntryStatus();
			}
		});
		
		$("select[name='month']").change(function(){
			var that = this;
			if($("input[value='monthly']").prop("checked")){
				$(".dateFetch").html($(that).children(":selected").text());
				
				getDataEntryStatus();
			}
		});
		
		$("select[name='selectedYear']").change(function(){
   			var that = this;
   			if($("input[value='monthly']").prop("checked")){
   				$(".dateFetch").html($(that).children(":selected").text());
   				
   				var dateObj = {};
				getYearAndMonth(dateObj);
				var monthArr = getMonthList(dateObj, $(this).val());
				printYearAndMonth(monthArr, dateObj.year, $("input[value='monthly']").val(), $(this).val());
   				
   				getDataEntryStatus();
   			}
   		});
		
		function getDataEntryStatus(){
					if(isFirst<2){
						
						isFirst++;
						return false;
					}
					else{
						dashboardAjaxDailyWaterSupplyModels();
						
					}
		}