				/**
				 * DashBoard Charts
				 */
				
				$(document).ready(function() {
					$('.btn-all-ulb').on('click', function() {
						var winHeight = $(window).height();
						var chartHeight = parseInt(winHeight) - 112;
						$('#all-ulb-chart').css('height', chartHeight);
					});
				});
				
				var isAmrut = false;
				$(".btn-amrut").click(function() {
					isAmrut = true;
					getDataEntryStatus();
				});
				
				function genarateQueryStr() {
					/* amrutTypeId, Date, month, divisionId, districtId, ulbId */
					var queryStr = "";
					if (isAmrut) {
						queryStr += "amrutTypeId=1";
					}
				
					queryStr += "&startDate=" + dateFormatter($("#report_start_date").val())+"&endDate=" + dateFormatter($("#report_end_date").val());
				
					if ($("#ulbSelect").val() != "ULB") {
						queryStr += "&"
						queryStr += "ulbId=" + $("#ulbSelect").val();
					} else if ($("#disSelect").val() != "District") {
						queryStr += "&"
						queryStr += "districtId=" + $("#disSelect").val();
					} else if ($("#divisionId").val() != "ALL") {
						queryStr += "&"
						queryStr += "divisionId=" + $("#divisionId").val();
					}
					return queryStr;
				}
				
				function dashboardAjaxDailyWaterSupplyModels() {
					var queryStr = genarateQueryStr();
				
					$.ajax({
						url : "./ajax-average-report?" + queryStr,
						async : false,
						success : function(datas) {
							$(".table-div").html(datas);
							$("#datatable-buttons2").DataTable({
								dom : "Bfrtip",
								paging : false,
								bSort : false,
								buttons : [ {
									extend : "copy",
									className : "btn-sm"
								}, {
									extend : "csv",
									className : "btn-sm"
								}, {
									extend : "excel",
									className : "btn-sm"
								}, {
									extend : "pdf",
									className : "btn-sm"
								}, {
									extend : "print",
									className : "btn-sm"
								}, ],
								responsive : true
							});
						}
					});
				}
				
				
				function getDatefromStr(dateStr){
					var dateArr = dateStr.split("-");

		            return new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);
				}
				
				
				var isFirst = 0;
				var defaultStartDate, defaultEndDate;
				$("input#report_start_date, input#report_end_date").change(
						function() {
							if($("#report_end_date").val() == ""){
								return false;
							}
							var startDate =getDatefromStr($("#report_start_date").val())
				            
				            var endDate =getDatefromStr($("#report_end_date").val());
				            if(startDate > endDate){
				            	defaultStartDate = $("#report_start_date").val();
				            	var date = $('input#report_start_date').val();
								$("input#report_end_date").daterangepicker({
									singleDatePicker : true,
									singleClasses : "picker_2",
									locale : {	format : 'DD-MM-YYYY'},
									minDate:date
								});
				            }
				            else{
				            	if($(this).attr("id") == "report_start_date" && (!defaultStartDate || $(this).val() != defaultStartDate)){
				            		if(typeof defaultStartDate != "undefined" && getDatefromStr($(this).val()) < getDatefromStr(defaultStartDate)){
				            			defaultEndDate = undefined;
				            			var date = $('input#report_start_date').val();
										$("input#report_end_date").daterangepicker({
											singleDatePicker : true,
											singleClasses : "picker_2",
											locale : {	format : 'DD-MM-YYYY'},
											minDate:date
										});
										return false;
				            		}
				            		
									defaultStartDate = $(this).val();
									
									getDataEntryStatus();
								}
								else if($(this).attr("id") == "report_end_date" && (!defaultEndDate || $(this).val() != defaultEndDate)){
									defaultEndDate = $(this).val();
									
									getDataEntryStatus();
								}
				           }
						});
				
				
				
				function getDataEntryStatus() {
					if (isFirst < 2) {
				
						isFirst++;
						return false;
					} else {
						dashboardAjaxDailyWaterSupplyModels();
				
					}
				}
