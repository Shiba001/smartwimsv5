				/**
				 * DashBoard Charts
				 */
				
				$(document).ready(function() {
					$('.btn-all-ulb').on('click', function() {
						var winHeight = $(window).height();
						var chartHeight = parseInt(winHeight) - 112;
						$('#all-ulb-chart').css('height', chartHeight);
					});
				});
				
				var isAmrut = false;
				$(".btn-amrut").click(function() {
					isAmrut = true;
					getDataEntryStatus();
				});
				
				function genarateQueryStr() {
					/* amrutTypeId, Date, month, divisionId, districtId, ulbId */
					var queryStr = "";
					if (isAmrut) {
						queryStr += "amrutTypeId=1";
					}
				
					if ($("[name='inlineRadioOptions']:checked").val() == "daily") {
						if (queryStr != "")
							queryStr += "&"
						queryStr += "Date=" + dateFormatter($("#dashboard_date").val());
					} else {
						if (queryStr != "")
							queryStr += "&"
						queryStr += "month=" + $("select[name='month']").val();
					}
				
					if ($("#ulbSelect").val() != "ULB") {
						queryStr += "&"
						queryStr += "ulbId=" + $("#ulbSelect").val();
					} else if ($("#disSelect").val() != "District") {
						queryStr += "&"
						queryStr += "districtId=" + $("#disSelect").val();
					} else if ($("#divisionId").val() != "ALL") {
						queryStr += "&"
						queryStr += "divisionId=" + $("#divisionId").val();
					}
					return queryStr;
				}
				
				function dashboardAjaxDailyWaterSupplyModels() {
					var queryStr = genarateQueryStr();
				
					$.ajax({
						url : "./ajax-total-population-served?" + queryStr,
						async : false,
						success : function(datas) {
							$(".table-div").html(datas);
							$("#datatable-buttons2").DataTable({
								dom : "Bfrtip",
								paging : false,
								buttons : [ {
									extend : "copy",
									className : "btn-sm"
								}, {
									extend : "csv",
									className : "btn-sm"
								}, {
									extend : "excel",
									className : "btn-sm"
								}, {
									extend : "pdf",
									className : "btn-sm"
								}, {
									extend : "print",
									className : "btn-sm"
								}, ],
								responsive : true
							});
						}
					});
				}
				
				$("input[name='inlineRadioOptions']").change(function() {
					if($("input[value='daily']").prop("checked")){
						$(".dateFetch").html($("#dashboard_date").val());
						$("select[name='month']").prop("disabled", true);
						$("#dashboard_date").prop("disabled", false);
					}
					else{
						$(".dateFetch").html($("select[name='month'] option:selected").text());
						$("select[name='month']").prop("disabled", false);
						$("#dashboard_date").prop("disabled", true);
					}
					getDataEntryStatus();
				});
				
				var isFirst = 0;
				var defaultDailyDt;
				$("input#dashboard_date").change(
						function() {
				
							var that = this;
							if(!defaultDailyDt || ($(that).val() != defaultDailyDt)){
								defaultDailyDt = $(that).val();
								$(".dateFetch").html($(that).val());
								var dateArr = $(that).val().split("-");
								$("select[name='month'] option[value='"+ dateArr[1] +"']").prop("selected", true);
								
								getDataEntryStatus();
							}
						});
				
				$("select[name='month']").change(function() {
					var that = this;
					if ($("input[value='monthly']").prop("checked")) {
						$(".dateFetch").html($(that).children(":selected").text());
				
						getDataEntryStatus();
					}
				});
				
				function getDataEntryStatus() {
					if (isFirst < 2) {
				
						isFirst++;
						return false;
					} else {
						dashboardAjaxDailyWaterSupplyModels();
				
					}
				}
