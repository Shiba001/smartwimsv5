	  
/***
 * LPCD
 */
	  google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChartLpcd);
      function drawChartLpcd() {
        var data = google.visualization.arrayToDataTable([
          ['Language', 'Speakers (in millions)'],
          ['',  Number(ulbGreater135)],         
		  ['',  Number(ulbLesser135)]
        
        ]);
        

        var options = {
		  legend: 'none',
		  pieSliceText: 'value',          
          pieHole: 0.55,
          height:200,
          pieSliceTextStyle: {
              color: 'f75700',
          },
		  /*tooltip: { trigger: 'none' },*/
		  slices: {
            0: { color: '#3C8A2E' },
            1: { color: '#C3C3C3' }
			
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('lpcddonutchart'));
        chart.draw(data, options);
      }
      
      /**
      * Water Supply Coverage
      */
      
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(watersupplycoveragedrawChart);
      function watersupplycoveragedrawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Language', 'Speakers (in millions)'],
          ['',  Number(waterSupplyCoverageGreater100)],         
          ['',  Number(waterSupplyCoverageGreater50)],         
		  ['',  Number(waterSupplyCoverageLesser50)]
        
        ]);

        var options = {
		  legend: 'none',
		  pieSliceText: 'value',         
          pieHole: 0.55,
          height:200,
          pieSliceTextStyle: {
        	  color: 'f75700',
            },
            /*tooltip: { trigger: 'none' },*/
		  slices: {
			    0: { color: '#3C8A2E' },
	            1: { color: '#C3C3C3' },
	            2: { color: '#000000' }
			
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('watersupplycoveragedonutchart'));
        chart.draw(data, options);
      }
      
      /**
       * Deficit/Surplus
       */
       
       google.charts.load("current", {packages:["corechart"]});
       google.charts.setOnLoadCallback(deficitSurplusdrawChart);
       function deficitSurplusdrawChart() {
         var data = google.visualization.arrayToDataTable([
           ['Language', 'Speakers (in millions)'],
           ['',  Number(deficit)],         
 		  ['',  Number(surplus)]
         
         ]);

         var options = {
 		  legend: 'none',
 		 pieSliceText: 'value',           
           pieHole: 0.55,
           height:200,
           pieSliceTextStyle: {
        	   color: 'f75700',
             },
             /*tooltip: { trigger: 'none' },*/
 		  slices: {
 			 0: { color: '#3C8A2E' },
             1: { color: '#C3C3C3' }
 			
           }
         };

         var chart = new google.visualization.PieChart(document.getElementById('deficitSurplusdonutchart'));
         chart.draw(data, options);
       }
       
       
       /**
        * Average Hours Of Water Supply
        */
       
       google.charts.load("current", {packages:["corechart"]});
       google.charts.setOnLoadCallback(averageHoursOfWaterSupplydonutchart);
       function averageHoursOfWaterSupplydonutchart() {
         var data = google.visualization.arrayToDataTable([
           ['Language', 'Speakers (in millions)'],
           ['',  Number(avgGreater)],         
  		  ['',  Number(avgLesser)]
         
         ]);

         var options = {
 		  legend: 'none',
 		 pieSliceText: 'value',            
           pieHole: 0.55,
           height:200,
           pieSliceTextStyle: {
        	   color: 'f75700',
             },
             /*tooltip: { trigger: 'none' },*/
 		  slices: {
 			 0: { color: '#3C8A2E' },
             1: { color: '#C3C3C3' }
 			
           }
         };

         var chart = new google.visualization.PieChart(document.getElementById('averageHoursOfWaterSupplydonutchart'));
         chart.draw(data, options);
       }
       
       
       /**
        * Water Charges Collection
        */
        
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(waterChargesCollectiondonutchart);
        function waterChargesCollectiondonutchart() {
          var data = google.visualization.arrayToDataTable([
            ['Language', 'Speakers (in millions)'],
        	  ['',  Number(collectionGreater100)],         
              ['',  Number(collectionGreater50)],         
      		  ['',  Number(collectionLesser50)]
          
          ]);

          var options = {
  		  legend: 'none',
  		 pieSliceText: 'value',            
            pieHole: 0.55,
            height:200,
            pieSliceTextStyle: {
            	color: 'f75700',
              },
              /*tooltip: { trigger: 'none' },*/
  		  slices: {
  			0: { color: '#3C8A2E' },
            1: { color: '#C3C3C3' },
            2: { color: '#000000' }
  			
            }
          };

          var chart = new google.visualization.PieChart(document.getElementById('waterChargesCollectiondonutchart'));
          chart.draw(data, options);
        }
        
        /**
         * Water Testing
         */
        
        function waterTesting() {
        	/* global document */
			Highcharts.createElement('link', {
			   href: 'https://fonts.googleapis.com/css?family=Unica+One',
			   rel: 'stylesheet',
			   type: 'text/css'
			}, null, document.getElementsByTagName('head')[0]);

			Highcharts.theme = {
			   colors: ['#3c8a2e', '#C3C3C3'],			 
			   xAxis: {
			      gridLineColor: '#fff',
			      labels: {
			         style: {
			            color: '#000',
			            fontSize: '16px'
			         }
			      },
			      lineColor: '#fff',
			      minorGridLineColor: '#000',
			      tickColor: ''
			      
			   },
			   yAxis: {
			      gridLineColor: '#fff',
			      labels: {
			         style: {
			            color: '#fff',
			            fontSize: '16px'
			         }
			      },
			      lineColor: '#fff',
			      minorGridLineColor: '#fff',
			      tickColor: '#fff',
			      tickWidth: 1
			   },
			   plotOptions: {
			      series: {
			         dataLabels: {
			            color: '#f75700'
			         },
			         marker: {
			            lineColor: '',
			            fontSize: '16px'
			         }
			      }
			   },
			   labels: {
			      style: {
			         color: '#707073',
			         fontSize: '16px'
			      }
			   },
			   navigation: {
			      buttonOptions: {
			         symbolStroke: '#DDDDDD',
			         theme: {
			            fill: '#505053'
			         }
			      }
			   },

			   background2: '#fff',
			   dataLabelsColor: '#B0B0B3',
			   textColor: '#C0C0C0',
			   contrastTextColor: '#F0F0F3',
			   maskColor: 'rgba(255,255,255,0.3)'
			};
			// Apply the theme
			Highcharts.setOptions(Highcharts.theme);
			$(function () {
			    Highcharts.chart('waterTesting', {
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: ''
			        },
			        xAxis: {
			            categories: ['Bacteriological', 'FRC']
			        },
			        yAxis: {
			            min: 0,
			            max:100,
			            title: {
			                text: ''
			            },
			            stackLabels: {
			                enabled: false,
			                style: {
			                    fontWeight: 'bold'
			                }
			            }
			        },
			        legend: {
			            enabled:false
			        },
			        tooltip: {
			            enabled:true
			        },
			        plotOptions: {
			            column: {
			                stacking: 'normal',
			                dataLabels: {
			                    enabled: true,
			                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
			                }
			            }
			        },
			        series: [{
			            name: 'Sample Found OK',
			            data: [Number(frcFoundOK), Number(bacteriologicalFoundOK)]
			        }, {
			            name: 'Sample Found Not OK',
			            data: [Number(frcFoundNotOK), Number(bacteriologicalFoundNotOK)]
			        }]
			        /*series: [{
			            name: 'Sample Found OK',
			            data: [10, 20]
			        }, {
			            name: 'Sample Found Not OK',
			            data: [11, 22]
			        }]*/
			    });
			});
        }
        waterTesting();
        /**
         * Complaints Resolved
         */
         
         google.charts.load("current", {packages:["corechart"]});
         google.charts.setOnLoadCallback(complaintsResolveddonutchart);
         function complaintsResolveddonutchart() {
           var data = google.visualization.arrayToDataTable([
             ['Language', 'Speakers (in millions)'],
             ['',  Number(complaintResolved)],         
     		  ['',  Number(complaintUnResolved)]
           
           ]);

           var options = {
   		  legend: 'none',
   		 pieSliceText: 'value',             
             pieHole: 0.55,
             height:200,
             pieSliceTextStyle: {
            	 color: 'f75700',
               },
               /*tooltip: { trigger: 'none' },*/
   		  slices: {
   			 0: { color: '#3C8A2E' },
             1: { color: '#C3C3C3' }
   			
             }
           };

           var chart = new google.visualization.PieChart(document.getElementById('complaintsResolveddonutchart'));
           chart.draw(data, options);
         }
         
         /**
          *Data Entry Status
          */
          
          google.charts.load("current", {packages:["corechart"]});
          google.charts.setOnLoadCallback(dataEntryStatusdonutchart);
          function dataEntryStatusdonutchart() {
            var data = google.visualization.arrayToDataTable([
              ['Language', 'Speakers (in millions)'],
              ['',  Number(complete)], 
              ['',  Number(partial)],         
      		  ['',  Number(noDataEntry)]
            
            ]);

            var options = {
    		  legend: 'none',
    		  pieSliceText: 'value',             
              pieHole: 0.55,
              height:200,
              pieSliceTextStyle: {
            	  color: 'f75700',
                },
                /*tooltip: { trigger: 'none' },*/
    		  slices: {
    			  0: { color: '#3C8A2E' },
    	            1: { color: '#C3C3C3' },
    	            2: { color: '#000000' }
    			
              }
            };

            var chart = new google.visualization.PieChart(document.getElementById('dataEntryStatusdonutchart'));
            chart.draw(data, options);
          }
          
          
          /**
           * Data Approval Status
           */
           
           google.charts.load("current", {packages:["corechart"]});
           google.charts.setOnLoadCallback(dataApprovalStatusdonutchart);
           function dataApprovalStatusdonutchart() {
             var data = google.visualization.arrayToDataTable([
               ['Language', 'Speakers (in millions)'],
               ['',  Number(notApproved)],         
       		  ['',  Number(approved)]
             
             ]);

             var options = {
     		  legend: 'none',
     		 pieSliceText: 'value',            
               pieHole: 0.55,
               height:200,
               pieSliceTextStyle: {
            	   color: 'f75700',
                 },
                 /*tooltip: { trigger: 'none' },*/
     		  slices: {
     			 0: { color: '#C3C3C3' },
     			 1: { color: '#3C8A2E' }
                 
     			
               }
             };

             var chart = new google.visualization.PieChart(document.getElementById('dataApprovalStatusdonutchart'));
             chart.draw(data, options);
           }
           
           
           
           function genarateQueryStr(){
   			/* amrutTypeId, Date, month, divisionId, districtId, ulbId  */
   			var queryStr = "";
   			if(isAmrut){
   				queryStr += "amrutTypeId=1";
   			}
   			
   			if($("[name='inlineRadioOptions']:checked").val() == "daily"){
   				if(queryStr != "")
   					queryStr += "&"
   				queryStr += "Date="+dateFormatter($("#dashboard_date").val()); 
   			}
   			else{
   				if(queryStr != "")
   					queryStr += "&"
   				queryStr += "month="+$("select[name='month']").val();
   				queryStr += "&selectedYear="+$("select[name='selectedYear']").val();
   			}
   			
   			if($("#ulbSelect").val() != "ULB"){
   				queryStr += "&"
   				queryStr += "ulbId="+$("#ulbSelect").val();
   			}
   			else if($("#disSelect").val() != "District"){
   				queryStr += "&"
   				queryStr += "districtId="+$("#disSelect").val();
   			}
   			else if($("#divisionId").val() != "ALL"){
   				queryStr += "&"
   				queryStr += "divisionId="+$("#divisionId").val();
   			}
   			
   			return queryStr;
   		}
   		
   		$(".btn-amrut").click(function(){
   			isAmrut = true;
   			getDataEntryStatus();
   		});
   		
   		function dashboardAjaxDailyWaterSupplyModels(){
   			var queryStr = genarateQueryStr();
   			
   			$.ajax({
   				url: "./ajax-dashboard?"+queryStr,
   				async: false,
   				error : function(XMLHttpRequest, textStatus,
						errorThrown) {
					console.log('status:' + XMLHttpRequest.status
							+ ', status text: '
							+ XMLHttpRequest.statusText);
				},
   				success: function(datas){
   					var lpcdBoxValue = datas.lpcdBoxValue;
   					var waterSupplyCoverageBoxValue = datas.waterSupplyCoverageBoxValue;
   					var surplusDeficitBoxValue = datas.surplusDeficitBoxValue;
   					var avgHrOfSupplyBoxValue = datas.avgHrOfSupplyBoxValue;
   					var totalPercentageBoxValue = datas.totalPercentageBoxValue;
   					var complaintBoxValue = datas.complaintBoxValue;
   					var popServedBoxValue = datas.popServedBoxValue;
   					var tankerBoxValue = datas.tankerBoxValue;
   					var pipeWaterSupplyBoxValue = datas.pipeWaterSupplyBoxValue;
   					var testingBoxValue = datas.testingBoxValue;
   					
   					
   					ulbGreater135 = String(datas.ulbGreater135);
   					ulbLesser135 = String(datas.ulbLesser135);
   					waterSupplyCoverageGreater100 = String(datas.waterSupplyCoverageGreater100);
   					waterSupplyCoverageGreater75 = String(datas.waterSupplyCoverageGreater75);
   					waterSupplyCoverageGreater50 = String(datas.waterSupplyCoverageGreater50);
   					waterSupplyCoverageLesser50 = String(datas.waterSupplyCoverageLesser50);
   					deficit = String(datas.deficit);
   					surplus = String(datas.surplus);
   					avgGreater = String(datas.avgGreater);
   					avgLesser = String(datas.avgLesser);
   					collectionGreater100 = String(datas.collectionGreater100);
   					collectionGreater75 = String(datas.collectionGreater75);
   					collectionGreater50 = String(datas.collectionGreater50);
   					collectionLesser50 = String(datas.collectionLesser50);
   					complete = String(datas.complete);
   					partial = String(datas.partial);
   					notApproved = String(datas.notApproved);
   					approved = String(datas.approved);
   					noDataEntry = String(datas.noDataEntry);
   					complaintResolved = String(datas.complaintResolved);
   					complaintUnResolved = String(datas.complaintUnResolved);
   					frcFoundOK = String(datas.frcFoundOK);
   					bacteriologicalFoundOK = String(datas.bacteriologicalFoundOK);
   					frcFoundNotOK = String(datas.frcFoundNotOK);
   					bacteriologicalFoundNotOK = String(datas.bacteriologicalFoundNotOK);
   					
   					var phDivisionModels = datas.phDivisionModels;
					if(typeof phDivisionModels != "undefined"){
						var divisionSelect = "<option>ALL</option>";
						$.each(phDivisionModels, function(index, value){
							divisionSelect += "<option value=\""+ value.div_id +"\">"+ value.div_name +"</option>";
						});
						$("#divisionId").html(divisionSelect);
						$("#disSelect").html(
						"<option>District</option>");
						$("#ulbSelect").html("<option>ULB</option>");
					}
   					
   					
   					$('#lpcdBoxValue').html( lpcdBoxValue);
   					if(lpcdBoxValue >= 135){
   						$('#lpcdBoxValue').attr("color", "green");
   					}
   					else{
   						$('#lpcdBoxValue').attr("color", "red");
   					}
   					
   					$('#waterSupplyCoverageBoxValue').html( waterSupplyCoverageBoxValue);
   					if(waterSupplyCoverageBoxValue >= 80){
   						$('#waterSupplyCoverageBoxValue').attr("color", "#3c8a2e");
   					}
   					else if(waterSupplyCoverageBoxValue < 80 && waterSupplyCoverageBoxValue >=50){
   						$('#waterSupplyCoverageBoxValue').attr("color", "#c3c3c3");
   					}
   					else{
   						$('#waterSupplyCoverageBoxValue').attr("color", "#000");
   					}
   					
   					$('#surplusDeficitBoxValue').html( surplusDeficitBoxValue);
   					if(surplusDeficitBoxValue > 0){
   						$('#surplusDeficitBoxValue').attr("color", "green");
   					}
   					else{
   						$('#surplusDeficitBoxValue').attr("color", "red");
   					}
   					$('#avgHrOfSupplyBoxValue').html( avgHrOfSupplyBoxValue);
   					$('#avgHrOfSupplyValue1').html(avgHrOfSupplyBoxValue);
   					$('#avgHrOfSupplyValue2').html(avgHrOfSupplyBoxValue);
   					$('#totalPercentageBoxValue').html( totalPercentageBoxValue);
   					if(totalPercentageBoxValue >= 80){
   						$('#totalPercentageBoxValue').attr("color", "#3c8a2e");
   					}
   					else if(totalPercentageBoxValue < 80 && totalPercentageBoxValue >=50){
   						$('#totalPercentageBoxValue').attr("color", "#c3c3c3");
   					}
   					else{
   						$('#totalPercentageBoxValue').attr("color", "#000");
   					}
   					
   					$('#complaintBoxValue').html( complaintBoxValue);
   					$('#popServedBoxValue').html( popServedBoxValue);
   					$('#tankerBoxValue').html( tankerBoxValue);
   					$('#pipeWaterSupplyBoxValue').html( pipeWaterSupplyBoxValue);
   					$('#testingBoxValue').html( testingBoxValue.toFixed(2));
   					if(testingBoxValue >= 100){
   						$('#testingBoxValue').attr("color", "#3c8a2e");
   					}
   					else{
   						$('#testingBoxValue').attr("color", "#c3c3c3");
   					}
   					
   					drawChartLpcd();
   					watersupplycoveragedrawChart();
   					deficitSurplusdrawChart();
   					averageHoursOfWaterSupplydonutchart();
   					waterChargesCollectiondonutchart();
   					waterTesting();
   					complaintsResolveddonutchart();
   					dataEntryStatusdonutchart();
   					dataApprovalStatusdonutchart();
   					
   					if($("#ulbSelect").val() != "ULB"){
   						$("#place").html($("#ulbSelect option:selected").text());
   					}else if($("#disSelect").val() != "District"){
   						$("#place").html($("#disSelect option:selected").text());
   					}else if($("#divisionId").val() != "ALL"){
   						$("#place").html($("#divisionId option:selected").text());
   					}else  if($("#divisionId").val() == "ALL"){
   						$("#place").html("Odisha");
   					}
   					
   				}
   			});
   		}
   		
   		function getDataEntryStatus(){
   			
   			if(isFirst<2){
   				
   				
   				
   				isFirst++;
   				return false;
   			}
   			else{
   				dashboardAjaxDailyWaterSupplyModels();
   				
   				
   			}
   			
   		}
   		
   		function getYearAndMonth(obj){
   			var dateObjArr = dashboardDateObj.maxDate.split("-");
   			obj.year = dateObjArr[2];
   			obj.month = dateObjArr[1];
   			return obj;
   		}
   		
   		function getMonthList(dateObj, selectedYear){
   			var monthNames = [
   			               "January", "February", "March",
   			               "April", "May", "June",
   			               "July", "August", "September",
   			               "October", "November", "December"
   			               ];
   			if(selectedYear == dateObj.year){
   				monthNames = jQuery.map(monthNames, function(value, index){
   					if(Number(dateObj.month) >= (index+1)){
   						return value;
   					}
   					else{
   						return null;
   					}
   				});
   			}
   			
   			return monthNames;
   		}
   		
   		function printYearAndMonth(monthArr, endYear, selectedMonth, selectedYear){
   			$("select[name='selectedYear']").html("");
   			$("select[name='month']").html("");
   			
   			for(year = 2016; year<=endYear; year++){
   				jQuery("<option/>",{
   					value:year,
   					text:year
   				}).appendTo("select[name='selectedYear']");
   			}
   			
   			$("select[name='selectedYear'] option[value='"+ selectedYear +"']").prop("selected", true);
   			
   			jQuery.each(monthArr, function(index, value){
   				jQuery("<option/>",{
   					value:(index+1),
   					text:value
   				}).appendTo("select[name='month']");
   			});
   			
   			$("select[name='month'] option[value='"+ Number(selectedMonth) +"']").prop("selected", true);
   		}
   		
   		
   		
   		$("input[name='inlineRadioOptions']").change(function(){
   			if($("input[value='daily']").prop("checked")){
   				$(".dateFetch").html($("#dashboard_date").val());
   				$("select[name='month']").prop("disabled", true);
   				$("select[name='selectedYear']").prop("disabled", true);
   				$("select[name='selectedYear']").parent().css("display", "none");
				$("#dashboard_date").prop("disabled", false);
   			}
   			else{
   				$(".dateFetch").html($("select[name='month'] option:selected").text());
   				$("select[name='month']").prop("disabled", false);
   				$("select[name='selectedYear']").prop("disabled", false);
   				$("select[name='selectedYear']").parent().css("display", "block");
				$("#dashboard_date").prop("disabled", true);
				
				
				var dateObj = {};
				getYearAndMonth(dateObj);
				var monthArr = getMonthList(dateObj, dateObj.year);
				printYearAndMonth(monthArr, dateObj.year,dateObj.month, dateObj.year);
   			}
   			getDataEntryStatus();
   		});
   		var defaultDailyDt;
   		var isFirst = 0;
   		$("input#dashboard_date").change(function(){
   			
   			var that = this;
   			if(!defaultDailyDt || ($(that).val() != defaultDailyDt)){
				defaultDailyDt = $(that).val();
				$(".dateFetch").html($(that).val());
				var dateArr = $(that).val().split("-");
				$("select[name='month'] option[value='"+ dateArr[1] +"']").prop("selected", true);
				$("select[name='selectedYear'] option[value='"+ dateArr[2] +"']").prop("selected", true);
				getDataEntryStatus();
			}
   		});
   		
   		$("select[name='month']").change(function(){
   			var that = this;
   			if($("input[value='monthly']").prop("checked")){
   				$(".dateFetch").html($(that).children(":selected").text());
   				
   				getDataEntryStatus();
   			}
   		});
   		
   		$("select[name='selectedYear']").change(function(){
   			var that = this;
   			if($("input[value='monthly']").prop("checked")){
   				$(".dateFetch").html($(that).children(":selected").text());
   				
   				var dateObj = {};
				getYearAndMonth(dateObj);
				var monthArr = getMonthList(dateObj, $(this).val());
				printYearAndMonth(monthArr, dateObj.year, $("input[value='monthly']").val(), $(this).val());
   				
   				getDataEntryStatus();
   			}
   		});
   		
   		$(document).ready(function(){
   			
   			$(".resetAll").click(function(){
   				
   				$.ajax({
   					url : "./entryHeaderMenu/free-amrut",
   					async: false,
   					error : function(XMLHttpRequest, textStatus,
   							errorThrown) {
   						console.log('status:' + XMLHttpRequest.status
   								+ ', status text: '
   								+ XMLHttpRequest.statusText);
   					},
   					success : function(dists) {
   						//alert("Success");
   					}
   				});
   				
   				location.reload();
   				
   			});
   			
   		});
   		
   		