$(document).ready(
		function() {
			$("#divisionId").change(
					function() {
						var divisionId = $(this).val();
						if(divisionId == "--- Select ---"){
							$("#disSelect").html(
							"<option>District</option>");
							$("#ulbSelect").html("<option>ULB</option>");
							return true;
						}
						$.ajax({
							url : "./entryHeaderMenu/distList?divisionId="
									+ divisionId,
							async: false,
							error : function(XMLHttpRequest, textStatus,
									errorThrown) {
								console.log('status:' + XMLHttpRequest.status
										+ ', status text: '
										+ XMLHttpRequest.statusText);
								$("#disSelect").html(
										"<option>District</option>");
								$("#ulbSelect").html("<option>ULB</option>");
							},
							success : function(dists) {
								var distoptions = "<option>District</option>";
								$.each(dists, function(index, element) {
									distoptions += "<option value=\""
											+ element.Dist_id + "\">"
											+ element.Dist_Name + "</option>";
								});

								$("#disSelect").html(distoptions);
								if((userLevelId == 3 || userLevelId == 4) || isUperlevel == "true"){
									$("#disSelect option[value='"+ distId +"']").prop("selected", true);
								}
								$("#disSelect").change();
							}
						});
					});

			$("#disSelect").change(
					function() {
						var districtId = $(this).val();
						
						if(districtId == "District") {
							return true;
						}
						$.ajax({
							url : "./entryHeaderMenu/ULBList?districtId="
									+ districtId,
							async: false,
							error : function(XMLHttpRequest, textStatus,
									errorThrown) {
								console.log('status:' + XMLHttpRequest.status
										+ ', status text: '
										+ XMLHttpRequest.statusText);
								$("#ulbSelect").html("<option>ULB</option>");
							},
							success : function(ULBs) {
								var distoptions = "<option>ULB</option>";
								$.each(ULBs, function(index, element) {
									distoptions += "<option value=\""
											+ element.ulb_id + "\">"
											+ element.ulb_Name + "</option>";
								});

								$("#ulbSelect").html(distoptions);
								if(userLevelId == 4 || isUperlevel == "true"){
									$("#ulbSelect option[value='"+ ulbId +"']").prop("selected", true);
									$("#ulbSelect").change();
								}
							}
						});
					});
			
			$("#ulbSelect").change(function(){
				var form = $("button:contains('Clear'),button:contains('Back'),a:contains('Back')").closest("form");
				$(".dynamiULB").remove();
				if($(this).val() != "ULB"){
					jQuery('<input/>', {
					    type: 'hidden',
					    name: 'ulbId',
					    class: 'dynamiULB',
					    value: $(this).val()
					}).appendTo(form);
				}
			});
			$("button:contains('Clear'),button:contains('Back'),a:contains('Back')").closest("form").submit(function(event){
				if($("#ulbSelect").val() == "ULB"){
					event.preventDefault()
					jQuery.growl.error({
						message : "Please select proper ULB"
					});
				}
				
			});
			
			
			var distId = $("script[src$='entry-menu-header.js']").attr("data-Dist-id");
			var ulbId = $("script[src$='entry-menu-header.js']").attr("data-ulb-id");
			var userLevelId = $("script[src$='entry-menu-header.js']").attr("data-user-level-id");
			var isUperlevel = $("script[src$='entry-menu-header.js']").attr("data-is-uperlevel");
			$("#divisionId").change();
			
			

			
			switch (Number(userLevelId)) {
			case 4:
				$("#ulbSelect").prop("disabled", true);
				$("#ulbSelect").change();
			case 3:
				$("#disSelect").prop("disabled", true);
			case 2:
				$("#divisionId").prop("disabled", true);
			}

			
			
			$(document).on("click", ".approval", function() {
				var dataUrl = $(this).attr("data-url");
				var that = this;
				$.ajax({
					url : dataUrl,
					async: false,
					error : function(XMLHttpRequest, textStatus,
							errorThrown) {
						console.log('status:' + XMLHttpRequest.status
								+ ', status text: '
								+ XMLHttpRequest.statusText);
					},
					success : function(ULBs) {
						var queryStr = $(that).attr("data-url").substr(dataUrl.indexOf("?"));
						
						if($(that).next().html() == "Pending"){
							$(that).closest("tr").find("td:nth-last-child(1)").children("a").each(function(){
								$(this).attr("href", "javascript:void(0)")
								$(this).attr("onclick", "");
							});
							var url = $(that).attr("data-url").replace("is", "isNot");
							$(that).closest("tr").find("td:nth-last-child(2)").html("<input type=\"checkbox\" checked=\"\" data-url=\""+ url +"\" class=\"approval\"><font color=\"green\">Approved</font>");
							jQuery.growl.notice({
								message : "Approved"
							});
						}
						else{
							var cUrl = $(that).attr("data-url").substring($(that).attr("data-url").indexOf("-"), $(that).attr("data-url").indexOf("?"));

							var hrefArr = ["edit"+cUrl, "delete"+cUrl];
							var onclickArr = ["", "return confirm('Are you sure you want to delete?')"];
							var index = 0;
							$(that).closest("tr").find("td:nth-last-child(1)").children("a").each(function(){
								$(this).attr("href", hrefArr[index]+queryStr)
								$(this).attr("onclick", onclickArr[index++]);
							});
							var url = $(that).attr("data-url").replace("isNot", "is");
							$(that).closest("tr").find("td:nth-last-child(2)").html("<input type=\"checkbox\" data-url=\""+ url +"\" class=\"approval\"><font color=\"red\">Pending</font>");
							
							jQuery.growl.error({
								message : "Pending"
							});
						}
					}
				});
			});
			
			
			
			var isFirstCall = true;
			var defaultDailyDt;
			$("input#add_date, select").change(function(){
				if(isFirstCall == true){
					isFirstCall = false;
					if($(".yearselection").length == 0){
						return true;
					}
				}
				var urlArr = $(location).attr('href').split("/");
				var url = urlArr[urlArr.length - 1];
				if(url.indexOf("list-") == -1){
					return true;
				}
				
				if($(this).prop("tagName") == "SELECT"  || !defaultDailyDt || ($("input#add_date").val() != defaultDailyDt)){
					defaultDailyDt = $("input#add_date").val();
					$(".dateFetch").html($("input#add_date").val());
					/*var dateArr = $("input#add_date").val().split("-");*/
					
					var date = $("input#add_date").val();
					

					var divisionId = $("#divisionId").val();
					var districtId = $("#disSelect").val();
					var ulbId = $("#ulbSelect").val();
					
					var queryStr = "";
					if($(".yearselection").length != 0){
						queryStr = "fin_year="+ $(".yearselection").val();
					}
					else{
						queryStr = "Date="+dateFormatter(date);
					}
					
					if(divisionId != "--- Select ---"){
						queryStr += "&";
						queryStr += "divisionId="+divisionId;
					}
					
					if(districtId != "District"){
						queryStr += "&";
						queryStr += "districtId="+districtId;
					}
					
					if(ulbId != "ULB"){
						queryStr += "&";
						queryStr += "ulbId="+ulbId;
					}
					
					
					url = url.replace("list-", "fetch-ajax-")
					//$("#datatable-buttons").parent().addClass("dataTable-loader");
					$.ajax({
						url: "./"+url+"?"+queryStr,
						success : function(datas) {
							$("#datatable-buttons2").parent().html(datas);
							$("#datatable-buttons2").DataTable({
								dom : "Bfrtip",
								buttons : [ {
									extend : "copy",
									className : "btn-sm"
								}, {
									extend : "csv",
									className : "btn-sm"
								}, {
									extend : "excel",
									className : "btn-sm"
								}, {
									extend : "pdf",
									className : "btn-sm"
								}, {
									extend : "print",
									className : "btn-sm"
								}, ],
								responsive : true
							});					
						}
					});
				}
			});
			
			

			$(".resetAll").click(function(){
				location.reload();
			});
			
			
			
			  $( document ).on( 'focus', ':input', function(){
			        $( this ).attr( 'autocomplete', 'off' );
			    });
			  
			  
			  $(".btn-amrut").remove();
			  
			  //   hide all physical infra date calender
			  if(window.location.pathname.indexOf("list-physical-infra") != -1){
				  var formDateTime = $("#add_date").closest("div.form_datetime");
				  var parentOfformDateTime = formDateTime.parent();
				  formDateTime.remove();
				  parentOfformDateTime.html("<input type='hidden' id='add_date' />");
			  }

		});
