<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
							<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>Date (dd/MM/yyyy)</th>
									<th>No. of Water Supply Complaints Received</th>
									<th>No. of Water Supply Complaints Resolved</th>
									<th>No. of Tubewell Complaints Received</th>
									<th>No. of Tubewell Complaints Resolved</th>
									<th>Remarks</th>
									<th>Approval Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty dailyWaterSupplyModels}">
										<c:forEach items="${dailyWaterSupplyModels}" var="data">
											<c:set var="count" value="${count + 1}" scope="page" />
											<tr>
											<c:set var="id" value="${data.tw_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
												<td><c:out value="${count}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td><fmt:parseDate value="${data.date_of_report}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></td>
												<td>${fn:substringBefore(data.no_ws_comp_recv, '.')}</td>
												<td>${fn:substringBefore(data.no_ws_comp_resolved, '.')}</td>
												<td>${fn:substringBefore(data.no_tw_comp_recv, '.')}</td>
												<td>${fn:substringBefore(data.no_tw_comp_resolved, '.')}</td>
												<td>${data.remarks}</td>
												<c:choose>
													<c:when
														test='${data.isApproved == "0" and loginUsersModel2.getUserLevelModel().getUser_level_id() == "2"}'>
														<td><input type="checkbox" class="approval"
															data-url="./isApproved-daily-complaint?tw_id=<%=id%>"><font
															color="red">Pending</font></td>
														<td><a
															href="edit-daily-complaint?tw_id=<%=id%>"><i
																class="fa fa-pencil"></i></a> 
																<%-- <a
															href="delete-daily-complaint?tw_id=<%=id%>"
															onclick="return confirm('Are you sure you want to delete?')"><i
																class="fa fa-trash-o"></i></a> --%>
																<a onclick="return complaintConfirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
															<i class="fa fa-trash-o delete_pointer"></i>
														</a>
																</td>

													</c:when>
													<c:when
														test='${data.isApproved == "0" and loginUsersModel2.getUserLevelModel().getUser_level_id() != "2"}'>
														<td><input type="checkbox" disabled><font
															color="red">Pending</font></td>
														<td><a
															href="edit-daily-complaint?tw_id=<%=id%>"><i
																class="fa fa-pencil"></i></a>
																 <%-- <a
															href="delete-daily-complaint?tw_id=<%=id%>"
															onclick="return confirm('Are you sure you want to delete?')"><i
																class="fa fa-trash-o"></i></a> --%>
																<a onclick="return complaintConfirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
															<i class="fa fa-trash-o delete_pointer"></i>
														</a>
																</td>

													</c:when>
													<c:when
														test='${data.isApproved == "1" and loginUsersModel2.getUserLevelModel().getUser_level_id() == "2"}'>
														<td><input type="checkbox" class="approval"
															data-url="./isNotApproved-daily-complaint?tw_id=<%=id%>" checked><font
															color="green">Approved</font></td>
														<td><a href="javascript:void(0)"><i
																class="fa fa-pencil"></i></a> <a href="javascript:void(0)"><i
																class="fa fa-trash-o"></i></a></td>

													</c:when>
													<c:otherwise>
														<td><input type="checkbox" checked disabled><font
															color="green">Approved</font></td>
														<td><a href="javascript:void(0)"><i
																class="fa fa-pencil"></i></a> <a href="javascript:void(0)"><i
																class="fa fa-trash-o"></i></a></td>
													</c:otherwise>
												</c:choose>
											</tr>
										</c:forEach>
									</c:when>
								</c:choose>
							</tbody>
						</table>
						