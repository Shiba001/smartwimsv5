<%@ page import="com.orissa.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.Map.*"%>
<%@ page import="com.orissa.model.*"%>
<%@ page import="java.util.Date,java.text.SimpleDateFormat,java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<% List<String> datesBetween = (List<String>) request.getAttribute("datesBetween"); %>
<% SimpleDateFormat dateFormatWithTime = new SimpleDateFormat("MM/dd/yyyy hh:mm");%>
<table  id="datatable-buttons2"	class="table table-striped jambo_table">
							<thead>
								<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<% for(int day = 0;day<datesBetween.size();day++){ %>
									<% 
								     String dateStr = datesBetween.get(day);
								     SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
								     Date result = formater.parse(dateStr);
								     SimpleDateFormat newFormater = new SimpleDateFormat("dd MMM");
								%>
									<th><%= newFormater.format(result) %></th>
									<% } %>
									
								</tr>
							</thead>
								<tbody>
								<%
									Map<String, List<String>> outerMap = (Map<String, List<String>>) request.getAttribute("dataEntryWSReport");
											if (outerMap != null) {
											//System.out.println("outerMap" + outerMap);
											Map<String, List<String>> innerMapValues = null;
											List<String> innerList = null;											
											int i = 1;
											for (Entry<String, List<String>> outerEntry : outerMap.entrySet()) { %>
												<tr>
													<td><%=i %></td>
													<td><%=outerEntry.getKey() %></td>													
													
													<%	innerList = outerEntry.getValue();
														
														if (innerList != null & innerList.size() > 0) {
															
															/* System.out.println("innerList.size() --->>>>"
																	+ innerList.size()); */
															for (int j = 0;j < innerList.size(); j++) { 
															%>
															<td><% if(innerList.get(j).equals("Y")) { %><i class="fa fa-check-circle" aria-hidden="true" style="color:green;"></i><% } else { %><i class="fa fa-times-circle" aria-hidden="true" style="color:red;"></i><% } %></td>
															
															<% } }  %>
												</tr>
										<%	i++; }
											
											
								}%>
								</tbody>
						</table>