<%@ include file="header.jsp"%>
<%@ include file="entry-header.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
<%
      String message = (String) request.getAttribute("No.of.Bacteriological.Samples.Tested");

      if (message != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message1 = (String) request.getAttribute("No.of.Bacteriological.Samples.Found.OK");

      if (message1 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message1%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message2 = (String) request.getAttribute("No.of.FRC.Samples.Tested");

      if (message2 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message2%>"
       });
       });
      </script>
     <%
      }
     %>
<%
      String message3 = (String) request.getAttribute("No.of.FRC.Samples.Found.OK");

      if (message3 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message3%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
		String message4 = (String) request.getAttribute("data.entry.exist");

      if (message4 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message4%>"
       });
       });
      </script>
     <%
      }
     %> 
     
      <%	
		String message5= (String) request.getAttribute("No.of.Bacteriological.Samples.Tested.maxlength");

      if (message5 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message5%>"
       });
       });
      </script>
     <%
      }
     %> 
     
      <%	
		String message6 = (String) request.getAttribute("No.of.Bacteriological.Samples.Found.OK.maxlength");

      if (message6 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message6%>"
       });
       });
      </script>
     <%
      }
     %> 
     
      <%	
		String message7 = (String) request.getAttribute("No.of.FRC.Samples.Tested.maxlength");

      if (message7 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message7%>"
       });
       });
      </script>
     <%
      }
     %> 
     
      <%	
		String message8 = (String) request.getAttribute("No.of.FRC.Samples.Found.OK.maxlength");

      if (message8 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message8%>"
       });
       });
      </script>
     <%
      }
     %> 
     
     <%	
		String message9 = (String) request.getAttribute("remarks.maxlength");

      if (message9 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message9%>"
       });
       });
      </script>
     <%
      }
     %> 
     <%	
			String message12 = (String) request.getAttribute("ULBID.needed");

      if (message12 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message12%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message13 = (String) request.getAttribute("dateofreport.needed");

      if (message13 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message13%>"
       });
       });
      </script>
     <%
      }
     %>
   <div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="list-daily-water-testing" class="edit-link">
					<i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit
		</a>
	</div>
</div>  
        <div class="col-md-12">
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3">
				<div class="x_panel">
					<div class="x_title text-center text-capitalize">
						<h2>
							Water Testing
						</h2>
						<h4>
							Figures for: <label id="show_date"><fmt:parseDate value="${startDate}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></label>
						</h4>
					</div>
					<div class="x_content">
						<br>
						<form action="add-daily-water-testing" method="post" onsubmit="return dailyWaterTesting();" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
							<input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
							<div class="form-field">
								<div class="form-control-wrap">
									<input class="form-control" id="date_of_report" placeholder="" value="" readonly="" name="date_of_report" type="hidden">

								</div>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Bacteriological (H<sub>2</sub>S,
									Vial / Strip) Samples Tested:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_bact_samp_tested" id="no_bact_samp_tested" value="${fn:substringBefore(dailyWaterTestingModel.no_bact_samp_tested, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">

								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Bacteriological (H<sub>2</sub>S,
									Vial / Strip) Samples Found OK:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_bact_samp_found_ok" id="no_bact_samp_found_ok" value="${fn:substringBefore(dailyWaterTestingModel.no_bact_samp_found_ok, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of FRC Samples Tested: <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_other_samp_tested" id="no_other_samp_tested" value="${fn:substringBefore(dailyWaterTestingModel.no_other_samp_tested, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of FRC Samples Found OK: <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" id="no_other_samp_found_ok" name="no_other_samp_found_ok" value="${fn:substringBefore(dailyWaterTestingModel.no_other_samp_found_ok, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Remarks : </label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<textarea class="form-control" rows="5" name="remarks" maxlength="200" >${dailyWaterTestingModel.remarks}</textarea>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Save</button>
								<button type="reset" class="btn btn-primary">Clear</button>
							</div>

						</form>
					</div>
				</div>
			</div>
        </div>
<!-- /page content -->
<%@ include file="footer.jsp"%>
<script src="<%=basePath%>resources/validation/daily-water-testing.js"></script>