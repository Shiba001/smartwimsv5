<%@ include file="header.jsp"%>
<%@ include file="entry-header.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
<%
      String message = (String) request.getAttribute("water.supply.totdemand");

      if (message != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message1 = (String) request.getAttribute("water.supply.Freq_supply");

      if (message1 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message1%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message2 = (String) request.getAttribute("water.supply.Pop_served");

      if (message2 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message2%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message3 = (String) request.getAttribute("water.supply.Freq_supply");

      if (message3 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message3%>"
       });
       });
      </script>
     <%
      }
     %>
			
			
		<%	
			String message4 = (String) request.getAttribute("data.entry.exist");

      if (message4 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message4%>"
       });
       });
      </script>
     <%
      }
     %> 
         <%
      String message5 = (String) request.getAttribute("water.supply.totdemand.maxlength");

      if (message5 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message5%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message6 = (String) request.getAttribute("water.supply.Qlty_supply.maxlength");

      if (message6 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message6%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message7 = (String) request.getAttribute("water.supply.Pop_served.maxlength");

      if (message7 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message7%>"
       });
       });
      </script>
     <%
      }
     %>
			
			
		<%	
			String message8 = (String) request.getAttribute("water.supply.Freq_supply.maxlength");

      if (message8 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message8%>"
       });
       });
      </script>
     <%
      }
     %> 
      <%	
			String message9 = (String) request.getAttribute("remarks.maxlength");

      if (message9 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message9%>"
       });
       });
      </script>
     <%
      }
     %>
     
      <%	
			String message12 = (String) request.getAttribute("ULBID.needed");

      if (message12 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message12%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message13 = (String) request.getAttribute("dateofreport.needed");

      if (message13 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message13%>"
       });
       });
      </script>
     <%
      }
     %>
 <div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="list-daily-water-supply" class="edit-link">
					<i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit
		</a>
	</div>
</div>
<div class="col-md-12">
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3">
				<div class="x_panel">
					<div class="x_title text-center text-capitalize">
						<h2>
							Water Supply
						</h2>
						<h4>
							Figures for: <label id="show_date"><fmt:parseDate value="${startDate}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></label>
						</h4>
					</div>
					<div class="x_content">
						<br>
						<form action="add-daily-water-supply" method="post" onsubmit="return dailyWaterSupply();" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate>
							<input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
							<div class="form-field">
								<div class="form-control-wrap">
									<input class="form-control" id="date_of_report" placeholder="" value="" readonly name="date_of_report" type="hidden">

								</div>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Total Demand (in ML):<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="tot_demand" id="tot_demand" value="${dailyWaterSupplyModel.tot_demand}" class="form-control col-md-7 col-xs-12 decimal_number" type="text">

								</div>
								
								
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">Quantity Supplied by PWS (in ML):<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="qlty_supply" id="qlty_supply" value="${dailyWaterSupplyModel.qlty_supply}" class="form-control col-md-7 col-xs-12 decimal_number" type="text">
								</div>
								
								
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">Population Served by PWS: <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="pop_served" id="pop_served" value="${fn:substringBefore(dailyWaterSupplyModel.pop_served, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
								
								
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">Average Hour of Supply: <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" id="Freq_supply" name="Freq_supply" value="${dailyWaterSupplyModel.freq_supply}" class="form-control col-md-7 col-xs-12 decimal_number" type="text">
								</div>
		
							</div>
							<div class="form-group">
								<label class="control-la
el text-left" for="first-name">Remarks : </label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<textarea class="form-control" rows="5" name="remarks" maxlength="200">${dailyWaterSupplyModel.remarks}</textarea>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Save</button>
								<button type="reset" class="btn btn-primary">Clear</button>
							</div>

						</form>
					</div>
				</div>
			</div>
        </div>
<!-- /page content -->
<%@ include file="footer.jsp"%>
<script src="<%=basePath%>resources/validation/daily-water-supply.js"></script>