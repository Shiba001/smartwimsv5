<%@ include file="header.jsp"%>
<%@ include file="set-target-water-charge-header.jsp"%> 
<div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="list-target-water-charges" class="edit-link">
					<i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit
		</a>
	</div>
</div>

<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
     
<%
      String message = (String) request.getAttribute("target.month.april");

      if (message != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message%>"
       });
       });
      </script>
     <%
      }
     %>
<%
      String message1 = (String) request.getAttribute("target.month.may");

      if (message1 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message1%>"
       });
       });
      </script>
     <%
      }
     %>
<%
      String message2 = (String) request.getAttribute("target.month.june");

      if (message2 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message2%>"
       });
       });
      </script>
     <%
      }
     %>
<%
      String message3 = (String) request.getAttribute("target.month.july");

      if (message3 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message3%>"
       });
       });
      </script>
     <%
      }
     %>
<%
      String message4 = (String) request.getAttribute("target.month.august");

      if (message4 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message4%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message5 = (String) request.getAttribute("target.month.september");

      if (message5 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message5%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message6 = (String) request.getAttribute("target.month.october");

      if (message6 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message6%>"
       });
       });
      </script>
     <%
      }
     %>
  <%
      String message7 = (String) request.getAttribute("target.month.november");

      if (message7 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message7%>"
       });
       });
      </script>
     <%
      }
     %>
  <%
      String message8 = (String) request.getAttribute("target.month.december");

      if (message8 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message8%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message9 = (String) request.getAttribute("target.month.january");

      if (message9 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message9%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message10 = (String) request.getAttribute("target.month.february");

      if (message10 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message10%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message11 = (String) request.getAttribute("target.month.march");

      if (message11 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message11%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message12 = (String) request.getAttribute("target.yearly.annual");

      if (message12 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message12%>"
       });
       });
      </script>
     <%
      }
     %>  
    <%
      String message13 = (String) request.getAttribute("target.month.april.maxlength");

      if (message13 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message13%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message14 = (String) request.getAttribute("target.month.may.maxlength");

      if (message14 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message14%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message15 = (String) request.getAttribute("target.month.june.maxlength");

      if (message15 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message15%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message16 = (String) request.getAttribute("target.month.july.maxlength");

      if (message16 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message16%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message17 = (String) request.getAttribute("target.month.august.maxlength");

      if (message17 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message17%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message18 = (String) request.getAttribute("target.month.september.maxlength");

      if (message18 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message18%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message19 = (String) request.getAttribute("target.month.octember.maxlength");

      if (message19 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message19%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message20 = (String) request.getAttribute("target.month.november.maxlength");

      if (message20 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message20%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message21 = (String) request.getAttribute("target.month.december.maxlength");

      if (message21 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message21%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message22 = (String) request.getAttribute("target.month.january.maxlength");

      if (message22 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message22%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message23 = (String) request.getAttribute("target.month.february.maxlength");

      if (message23 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message23%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message24 = (String) request.getAttribute("target.month.march.maxlength");

      if (message24 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message24%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%
      String message25 = (String) request.getAttribute("target.yearly.annual.maxlength");

      if (message25 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message25%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message26 = (String) request.getAttribute("ULBID.needed");

      if (message26 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message26%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message27 = (String) request.getAttribute("dateofreport.needed");

      if (message27 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message27%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message28 = (String) request.getAttribute("yearselection.needed");

      if (message28 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message28%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message29 = (String) request.getAttribute("yearselection.valid");

      if (message29 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message29%>"
       });
       });
      </script>
     <%
      }
     %>
           
<div class="col-md-12">
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3">
				<div class="x_panel">
					<!-- <div class="x_title">
						<h2>
							Figures for: <label for="" id="show_date">
							</label>
						</h2>
					</div> -->
					<div class="x_title text-center text-capitalize">
						<h2>
							Set Target Water Charges/Tax
						</h2>
					</div>
					<div class="x_content">
						<br>


						<form action="add-target-water-charges-tax" method="POST" onsubmit="return targetWaterCharges();" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
						 <input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
							<div class="form-field">
								<div class="form-control-wrap">
									<input class="form-control" id="date_of_report" placeholder="" value="" readonly="" name="date_of_report" type="hidden">
								</div>
							</div>
							
							
							<div class="form-group">
								
								<label class="control-label text-left" for="first-name">Target for the financial year of:
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="fin_year" value="2016-2017" readonly="" class="form-control col-md-7 col-xs-12" type="text">
								</div>
							</div>
							
							<div class="form-field">
								<div class="form-control-wrap radio ">
									<input id="trgt_mode" name="trgt_mode" value="Monthly" checked="" data-parsley-multiple="trgt_mode" type="radio">
                                    <label for="trgt_mode">Monthly</label>
								</div>
							</div>

							
							<div class="form-group">
								
								<label class="control-label text-left" for="first-name">Apr:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_1" id="m_trgt_1" value="${targetModel.m_trgt_1}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">May: <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_2" id="m_trgt_2" value="${targetModel.m_trgt_2}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Jun:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_3" id="m_trgt_3" value="${targetModel.m_trgt_3}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Jul:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_4" id="m_trgt_4" value="${targetModel.m_trgt_4}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Aug:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_5" id="m_trgt_5" value="${targetModel.m_trgt_5}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Sep:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_6" id="m_trgt_6" value="${targetModel.m_trgt_6}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Oct:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_7" id="m_trgt_7" value="${targetModel.m_trgt_7}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Nov:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_8" id="m_trgt_8" value="${targetModel.m_trgt_8}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Dec:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_9" id="m_trgt_9" value="${targetModel.m_trgt_9}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Jan:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_10" id="m_trgt_10" value="${targetModel.m_trgt_10}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Feb:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_11" id="m_trgt_11" value="${targetModel.m_trgt_11}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Mar:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="m_trgt_12" id="m_trgt_12" value="${targetModel.m_trgt_12}" class="form-control col-md-7 col-xs-12 decimal_number month_val" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Total:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="y_trgt" id="y_trgt" value="${targetModel.y_trgt}" class="form-control col-md-7 col-xs-12 decimal_number" type="text">
								</div>
							</div>
							
							<div class="form-field">
								<div class="form-control-wrap radio">
									<input id="trget_mode1" name="trgt_mode" value="Yearly" data-parsley-multiple="trgt_mode" type="radio">
                                    <label for="trget_mode1">Annual</label>
								</div>
							</div>							
							
							<div class="form-group">
								<label class="control-label text-left">
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="y_trgt" id="y_target" value="${targetModel.y_trgt}" class="form-control col-md-7 col-xs-12 decimal_number" disabled="disabled" type="text">
								</div>
							</div>

							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Save</button>
								<button type="reset" class="btn btn-primary">Clear</button>
							</div>

						</form>

					</div>
				</div>
			</div>
        </div>
<%@ include file="footer.jsp"%>
<script src="<%=basePath%>resources/validation/set-target.js"></script>