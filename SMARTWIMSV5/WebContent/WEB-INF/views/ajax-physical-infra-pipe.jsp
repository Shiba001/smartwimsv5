<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
							<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>Length of Rising Main (in Kms)</th>
									<th>Length of Distribution Network (in Kms)</th>
									<th>Next Maintenance Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty physicalInfraLengthOfPipeModels}">
										<c:forEach items="${physicalInfraLengthOfPipeModels}" var="data">
											<c:set var="count" value="${count + 1}" scope="page" />
											<tr>
											
													<c:set var="id" value="${data.phyInfra_lpid}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
												<td><c:out value="${count}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td>${data.rising_main}</td>
												<td>${data.distr_netw}</td>
												<td>${data.dueDate}</td>

												<td><a
													href="edit-physical-infra-pipe?phyInfra_lpid=<%=id%>"><i
														class="fa fa-pencil"></i></a> 
													<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
													<i class="fa fa-trash-o delete_pointer"></i>
													</a>
												</td>
											</tr>
										</c:forEach>
									</c:when>
								</c:choose>	
							</tbody>
						</table>