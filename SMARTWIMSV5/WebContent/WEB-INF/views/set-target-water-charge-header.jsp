<%
	String path2 = request.getContextPath();
	String basePath2 = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path2 + "/";
	//System.out.println("BasePath : " + basePath);
%>
<script>
var isDashBoard = true;
</script>
<script src="./resources/js/entry-menu-header.js" data-user-level-id="${loginUsersModel2.userLevelModel.user_level_id }"
   data-Dist-id="${upperLevels.get(2)!=null?upperLevels.get(2):loginUsersModel2.phDistrictModel.dist_id }" data-is-uperlevel="${upperLevels.get(0)!=null }" data-ulb-id="${upperLevels.get(0)!=null?upperLevels.get(0):loginUsersModel2.ulbNameModel.ulb_id }"></script>
<div class="content-wrapper clearfix back-img"> 
  		<div class="search-bar search-bar-top">
			<div class="col-md-12">
            	<div class="form-group pull-left">
                	<!-- <div class="radio pull-left">
            			<input name="radio1" id="inlineRadio1" value="option1" type="radio" checked>
            			<input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="daily" checked>
                        <label for="inlineRadio1">Daily</label>
        			</div> -->
                    <div class="pull-left custome-btn">
                                    <!-- <input type='text' id="add_date" class="form-control"  /> -->
                                    <select class="form-control select yearselection">
                                    	<option value="2016-2017">2016-2017</option>
                                    	<option value="2017-2018">2017-2018</option>
                                    	<!-- <option value="2015-2016">2015-2016</option>
                                    	<option value="2014-2015">2014-2015</option> -->
		                          	</select>
                    </div>
                </div>
                <div class="form-group pull-left">
                	<div class="label-txt pull-left">
                        <label>Division</label>
        			</div>
                    <div class="pull-left custome-btn">
                         <select class="form-control select" id="divisionId">
						<option>--- Select ---</option>
						<c:forEach items="${phDivisionModels}" var="division">
							<option value="${division.div_id}" <c:if test="${division.div_id == loginUsersModel2.phDivisionModel.div_id || upperLevels.get(3)==division.div_id}">selected</c:if>>${division.div_name}</option>
						</c:forEach>
                          </select>
                    </div>
                </div>
                <div class="form-group pull-left">
                	<div class="label-txt pull-left">
                        <label>District</label>
        			</div>
                    <div class="pull-left custome-btn short-btn">
                         <select class="form-control select" id="disSelect">
						<option>District</option>
                          </select>
                    </div>
                </div>
                <div class="form-group pull-left">
                	<div class="label-txt pull-left">
                        <label>ULB</label>
        			</div>
                    <div class="pull-left custome-btn short-btn">
                         <select class="form-control select" id="ulbSelect">
						<option>ULB</option>
                          </select>
                    </div>
                </div>
            </div>
		</div>
		<script type="text/javascript">
		var addDate = '${startDate}';
		$(document).ready(function() {			
			$('#add_date').daterangepicker({
				singleDatePicker : true,
				startDate : dateFormatterAdd(addDate),
				singleClasses : "picker_2",
				locale : {

					format : 'DD-MM-YYYY'
				}
			});
			
			$(".yearselection").change(function(){
				$("[name='fin_year']").val($(this).val());
			});
			
			$('#date_of_report').val(addDate);
			
			$('#add_date').change(
					function() {
						$('#show_date').html("");
						$('#show_date').html($('#add_date').val());

						var getDate = $('#add_date').val();
						if (getDate != null) {
							var dateAr = getDate.split('-');
							var newDate = dateAr[0].slice(-4) + '-' + dateAr[1] + '-'
							+ dateAr[2];

							$('#date_of_report').val(dateFormatterAdd(newDate));
						}
					});

        });
		
		function dateFormatterAdd(startDate){
			var dt = startDate.split('-');
			var formattedDt = dt[2]+'-'+dt[1]+'-'+dt[0];

			return formattedDt;

		}
</script>