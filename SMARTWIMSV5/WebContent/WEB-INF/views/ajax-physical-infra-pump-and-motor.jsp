<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
<table id="datatable-buttons2" class="table table-striped jambo_table">
							<thead>
								<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="ulbNameModel" items="${ulbNameModels }" varStatus="step">
									<tr>
									<c:set var="id" value="${ulbNameModel.ulb_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
										<td>${step.index + 1}</td>
										<td>${ulbNameModel.ulb_Name }</td>
										<td><a
											href="add-physical-infra-pump-and-motor?ulb_id=<%=id%>" class="editLink"><i
												class="fa fa-pencil"></i></a></td>
									</tr>
								</c:forEach>

							</tbody>
						</table>