<%@ include file="header.jsp"%>
<div class="content-wrapper clearfix back-img"> 

<div class="col-md-12 card-bottom phy-infra-page">
	<h2 class="phy-hdr">Exception Report</h2>
	<div class="row-height">
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 margbot-20">
			<div class="bottom-card">
				<h3>Water Supply</h3>
				<a href="./report-water-supply" class="btn btn-default">Show
					Details</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 margbot-20">
			<div class="bottom-card">
				<h3>Tankers</h3>
				<a href="./report-daily-tanker" class="btn btn-default">Show
					Details</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 margbot-20">
			<div class="bottom-card">
				<h3>Complaints</h3>
				<a  href="./report-daily-complaint" class="btn btn-default">Show
					Details</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 margbot-20">
			<div class="bottom-card">
				<h3>Connections</h3>
				<a href="./report-daily-connection" class="btn btn-default">Show
					Details</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 margbot-20">
			<div class="bottom-card">
				<h3>Water Testing</h3>
				<a href="./report-daily-watertesting" class="btn btn-default">Show
					Details</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 margbot-20">
			<div class="bottom-card">
				<h3>Water Charges</h3>
				<a href="./report-daily-charges-tax" class="btn btn-default">Show
					Details</a>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<%@ include file="footer.jsp"%>
