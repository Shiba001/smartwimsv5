<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
							<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>No. of Residential Connections Provided</th>
									<th>No. of Industrial Connections Provided</th>
									<th>No. of Commercial Connections Provided</th>
									<th>No. of Institutional Connections Provided</th>
									<th>Next Maintenance Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty physicalInfraConnectionModels}">
										<c:forEach items="${physicalInfraConnectionModels}" var="data">
											<c:set var="count" value="${count + 1}" scope="page" />
											<tr>
												<c:set var="id" value="${data.phyInfra_con_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
											
												<td><c:out value="${count}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td>${fn:substringBefore(data.no_res_cons, '.')}</td>
												<td>${fn:substringBefore(data.no_ind_cons, '.')}</td>
												<td>${fn:substringBefore(data.no_com_cons, '.')}</td>
												<td>${fn:substringBefore(data.no_inst_cons, '.')}</td>
												<td>${data.dueDate}</td>

												<td><a
													href="edit-physical-infra-connection?phyInfra_con_id=<%=id%>"><i
														class="fa fa-pencil"></i></a> <%-- <a
													href="delete-physical-infra-connection?phyInfra_con_id=<%=id%>" onclick="return confirm('Are you sure you want to delete?')"><i
														class="fa fa-trash-o"></i></a></td> --%>
													<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
													<i class="fa fa-trash-o delete_pointer"></i>
													</a>
												</td>
											</tr>
										</c:forEach>
									</c:when>
									<%-- <c:otherwise>
										<td colspan="7" align="center">No Records Found</td>
									</c:otherwise> --%>
								</c:choose>
							</tbody>
						</table>