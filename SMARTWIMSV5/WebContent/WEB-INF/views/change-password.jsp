<%@ include file="header.jsp"%>
<!-- page content -->
<div class="content-wrapper clearfix back-img">
	<div class="col-md-12 card-bottom phy-infra-page">
		<div
			class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3">
			<div class="x_panel">
				<div class="x_title">
					<h2>Change Password</h2>
				</div>
				<%
					String message = (String) request
							.getAttribute("password.not.changed");
					if (message != null) {
				%>
				<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message%>"
							});
						});
					</script>
				<%
					}
				%>

				<%
					String message1 = (String) request.getAttribute("oldPassword.null");
					if (message1 != null) {
				%>
				<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message1%>"
							});
						});
					</script>
				<%
					}
				%>

				<%
					String message2 = (String) request.getAttribute("newPassword.null");
					if (message2 != null) {
				%>
				<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message2%>"
							});
						});
					</script>
				<%
					}
				%>

				<%
					String message3 = (String) request
							.getAttribute("password.mismatch");
					if (message3 != null) {
				%>
				<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message3%>"
						});
					});
				</script>
				<%
					}
				%>
				
				<%
					String message4 = (String) request
							.getAttribute("confirm.newPassword.null");
					if (message4 != null) {
				%>
				<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message4%>"
						});
					});
				</script>
				<%
					}
				%>
				
				<%
					String message5 = (String) request
							.getAttribute("newpassword.format");
					if (message5 != null) {
				%>
				<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message5%>"
						});
					});
				</script>
				<%
					}
				%>
				
				<%
					String message6 = (String) request
							.getAttribute("confirm.password.format");
					if (message6 != null) {
				%>
				<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message6%>"
						});
					});
				</script>
				<%
					}
				%>
				
				<%
					String message7 = (String) request
							.getAttribute("oldpassword.format");
					if (message7 != null) {
				%>
				<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message7%>"
						});
					});
				</script>
				<%
					}
				%>

				<div class="x_content">
					<br />
					<form action="change-password" method="post" id="demo-form2"
						data-parsley-validate class="form-horizontal form-label-left">

						<div class="form-group">
							<label class="control-label text-left" for="first-name">Old
								Password:<span class="required">*</span>
							</label>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<input type="password" id="oldPassword" name="oldPassword"
									value="${oldPassword}" class="form-control col-md-7 col-xs-12 ">

							</div>


						</div>
						<div class="form-group">
							<label class="control-label text-left" for="first-name">New
								Password:<span class="required">*</span>
							</label>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<input type="password" id="newPassword" name="newPassword"
									value="${newPassword}" class="form-control col-md-7 col-xs-12 ">

							</div>


						</div>
						<div class="form-group">
							<label class="control-label text-left" for="first-name">Confirm
								New Password: <span class="required">*</span>
							</label>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<input type="password" id="confirmNewPassword"
									name="confirmNewPassword" value="${confirmNewPassword}"
									class="form-control col-md-7 col-xs-12 ">

							</div>
						</div>
						<div class="ln_solid"></div>
						<div class="form-group text-center">
							<button type="submit" class="btn btn-success">Save</button>
							<button type="reset" class="btn btn-primary">Clear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="<%=basePath%>resources/js/login/md5.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="<%=basePath%>resources/js/change-password.js"></script>
		<script>
		function getTokenFromServer() {

			var getTokenURL = "get-token"

			var resFromServer = '';

			$.ajax({
				async : false,
				type : "POST",
				url : getTokenURL,
				contentType : 'application/text',
				cache : false,

				success : function(response) {

					resFromServer = response

					$("#token").val(resFromServer);
				},
				error : function(err) {
					//alert("error");
				}
			});

			return resFromServer;

		}
		$(document).ready(function() {
			$(document).on('focus', ':input', function() {
				$(this).attr('autocomplete', 'off');
			});
		});
	</script>
	<!-- /page content -->
	<%@ include file="footer.jsp"%>