<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
							<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>Mode of Target</th>
									<th>Jan</th>
									<th>Feb</th>
									<th>Mar</th>
									<th>Apr</th>
									<th>May</th>
									<th>Jun</th>
									<th>Jul</th>
									<th>Aug</th>
									<th>Sep</th>
									<th>Oct</th>
									<th>Nov</th>
									<th>Dec</th>
									<th>Annual</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty targetModels}">
										<c:forEach items="${targetModels}" var="data">
											<c:set var="count" value="${count + 1}" scope="page" />
											<c:set var="id" value="${data.trgt_id}" />
											<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
											<c:choose>
											<c:when test="${data.trgt_mode == 'Monthly'}">
												<tr>
												
												<td><c:out value="${count}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td>${data.trgt_mode}</td>
												<td>${fn:substringBefore(data.m_trgt_10, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_11, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_12, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_1, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_2, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_3, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_4, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_5, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_6, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_7, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_8, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_9, '.')}</td>
												<td>0</td>
												<td><a
													href="edit-target-water-charges?trgt_id=<%=id%>"><i
														class="fa fa-pencil"></i></a> <%-- <a
													href="delete-target-water-charges?trgt_id=<%=id%>" onclick="return confirm('Are you sure you want to delete?')"><i
														class="fa fa-trash-o"></i></a></td> --%>
													<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
													<i class="fa fa-trash-o delete_pointer"></i>
													</a>
												</td>
											</tr>
											</c:when>
											<c:otherwise>
												<tr>
												<td><c:out value="${count}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td>${data.trgt_mode}</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>${data.y_trgt}</td>
												<td><a
													href="edit-target-water-charges?trgt_id=<%=id%>"><i
														class="fa fa-pencil"></i></a> <%-- <a
													href="delete-target-water-charges?trgt_id=<%=id%>" onclick="return confirm('Are you sure you want to delete?')"><i
														class="fa fa-trash-o"></i></a></td> --%>
													<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
													<i class="fa fa-trash-o delete_pointer"></i>
													</a>
												</td>
											</tr>
											</c:otherwise>
											</c:choose>
											
										</c:forEach>
									</c:when>									
									<%-- <c:otherwise>
										<td colspan="17" align="center">No Records Found</td>
									</c:otherwise> --%>
								</c:choose>
							</tbody>
						</table>