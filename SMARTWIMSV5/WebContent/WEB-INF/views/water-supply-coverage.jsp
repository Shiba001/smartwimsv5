<%
	String path3 = request.getContextPath();
	String basePath3 = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path3 + "/";
	//System.out.println("BasePath : " + basePath);
%>
<style>
.highcharts-grid-line, .highcharts-credits {
	display: none;
}
</style>
<%@ include file="header.jsp"%>

<%@ include file="dashboard-header.jsp"%>

<div class="col-md-12 card-sec">
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		<div class="card card-inner">
			<h5><label id="area">Odisha</label> average <label id="average">${lpcdBoxValue }</label>%</h5>
			<div class="card-map">
				<div id="map-canvas" style="height: 529px; border: 1px solid #ccc;"></div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 barContainer"  style="">
		<div class="card card-inner">
			<h5>Water Supply Coverage: Top <label class="topValue">${topValue }</label> ULBs</h5>
			<div class="card-map">
				<div id="top10LPCD" class="LPCD-chart"></div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 barContainer" style="">
		<div class="card card-inner">
			<h5>Water Supply Coverage: Bottom <label class="topValue">${topValue }</label> ULBs</h5>
			<div class="card-map">
				<div id="bottom10LPCD" class="LPCD-chart"></div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 progressBr" style=""> 
		<div class="card-box">
		<h5>Progress Report</h5>
			<div class="card-overflow">
				<div class="progressChart" style="width: 1600px;height: 550px; margin: 0 auto !important;"></div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 card-sec">
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		<div class="card card-map card-map-legend">
			<div class="pull-left">
				<h5>Coverage &gt;= 80% </h5>
				<img class="img-responsive"
					src="<%=basePath%>resources/images/map-green.png" />
			</div>		
			<div class="col-md-offset-4">
				<h5>Coverage >=50% & &lt; 80%</h5>
				<img class="img-responsive"
					src="<%=basePath%>resources/images/map-orange.png" />
			</div>
			<div class="pull-right">
				<h5>Coverage &lt; 50% </h5>
				<img class="img-responsive"
					src="<%=basePath%>resources/images/map-red.png" />
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		<div class="card-map backnone bordernone card-panel">
			<div class="pull-left">
				<button type="button" class="btn btn-default btn-black progressBtn"
					data-toggle="modal" data-target="#myModal_progressbar">Progress Report</button>
			</div>
			<div class="pull-left">
				<button type="button" class="btn btn-default btn-black allUlbBtn"
					data-toggle="modal" data-target="#myModal">All ULBs</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-fullscreen">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">All ULB</h4>
			</div>
			<div class="modal-body modal-overflow">
				<div id="allLPCD" class="LPCD-chart" style="width: 1600px; height: 400px; margin: 0 auto !important;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div id="myModal_progressbar" class="modal fade" role="dialog">
	<div class="modal-dialog modal-fullscreen">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Progress Report Of Water Supply Coverage</h4>
			</div>
			<div class="modal-body modal-overflow">
				<div  class="progressChart" style="min-width: 1600px; height: 400px; margin: 0 auto !important;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
<%@ include file="footer.jsp"%>

<script type="text/javascript">
	var lpcdMap = JSON.parse('${lpcdMap}');
	var top10ulb = JSON.parse('${top10ulb}');
	var top10lpcd = JSON.parse('${top10lpcd}');
	var bottom10ulb = JSON.parse('${bottom10ulb}');
	var bottom10lpcd = JSON.parse('${bottom10lpcd}');
	var datesBetween = JSON.parse('${datesBetween}');
	var currentLPCD = JSON.parse('${currentLPCD}');
	var previousLPCD = JSON.parse('${previousLPCD}');
	
	var middleulb1 = JSON.parse('${middleulb1}');
	var middlelpcd1 = JSON.parse('${middlelpcd1}');
/* 	var middleulb2 = JSON.parse('${middleulb2}');
	var middlelpcd2 = JSON.parse('${middlelpcd2}');
 */	
	var topulb = JSON.parse('${topulb}');
	var toplpcd = JSON.parse('${toplpcd}');
	var bottomulb = JSON.parse('${bottomulb}');
	var bottomlpcd = JSON.parse('${bottomlpcd}');
	var flag = '${flag}';
</script>
<script type="text/javascript"
	src="<%=basePath%>resources/js/innerPage/water-supply-coverage.js"></script>