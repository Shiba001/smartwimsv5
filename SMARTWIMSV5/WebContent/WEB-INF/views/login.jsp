<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	//System.out.println("BasePath : " + basePath);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
html {
	display: none;
}
</style>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<meta name="msapplication-TileImage"
	content="<%=basePath%>resources/images/favicon/ms-icon-144x144.png">
<meta name="theme-color">
<title>Login</title>
<link rel="apple-touch-icon" sizes="57x57"
	href="<%=basePath%>resources/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60"
	href="<%=basePath%>resources/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72"
	href="<%=basePath%>resources/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="<%=basePath%>resources/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114"
	href="<%=basePath%>resources/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="<%=basePath%>resources/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144"
	href="<%=basePath%>resources/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="<%=basePath%>resources/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180"
	href="<%=basePath%>resources/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"
	href="<%=basePath%>resources/images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32"
	href="<%=basePath%>resources/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96"
	href="<%=basePath%>resources/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16"
	href="<%=basePath%>resources/images/favicon/favicon-16x16.png">
<link href="<%=basePath%>resources/css/style.css" type="text/css"
	rel="stylesheet" />
<link href="<%=basePath%>resources/css/custome-responsive.css"
	type="text/css" rel="stylesheet" />
<link rel="stylesheet"
	href="<%=basePath%>resources/css/AdminLTE.min.css">
<link rel="stylesheet"
	href="<%=basePath%>resources/css/_all-skins.min.css">
<link href="<%=basePath%>resources/css/bootstrap.min.css"
	type="text/css" rel="stylesheet" />
<link href="<%=basePath%>resources/css/font-awesome.min.css"
	type="text/css" rel="stylesheet" />
<link
	href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="<%=basePath%>resources/css/blue.css">
<link rel="stylesheet" href="<%=basePath%>resources/css/morris.css">
<link rel="stylesheet"
	href="<%=basePath%>resources/css/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet"
	href="<%=basePath%>resources/css/bootstrap-datetimepicker.css">
<link rel="stylesheet"
	href="<%=basePath%>resources/css/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet" href="<%=basePath%>resources/css/radio-btn.css">
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>resources/vendors/growl/css/jquery.growl.css" />
<script src="<%=basePath%>resources/js/jquery-min.js"
	type="text/javascript"></script>

<script type="text/javascript">
if( self == top ) {
document.documentElement.style.display = 'block' ; 
} 

</script>
</head>

<body class="fullscreen_bg">

	<div class="container">
		<form class="form-signin clearfix" action="login" method="post">
			<div class="form-main clearfix">
				<div class="form-area clearfix">
					<img class="logo img-responsive"
						src="<%=basePath%>resources/images/logo.png" />

					<%
						String message = (String) request.getAttribute("password.changed");
						if (message != null) {
					%>
					<script>
 $(document).ready(function() {
 jQuery.growl.notice({
		message : "<%=message%>"
					});
				});
			</script>
					<%
						}
					%>

					<%
						String message1 = (String) request.getAttribute("Message");
						if (message1 != null) {
					%>
					<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message1%>"
					});
				});
			</script>
					<%
						}
					%>

					<%
						String message2 = (String) request.getAttribute("username.null");
						if (message2 != null) {
					%>
					<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message2%>"
					});
				});
			</script>
					<%
						}
					%>

					<%
						String message3 = (String) request.getAttribute("password.null");
						if (message3 != null) {
					%>
					<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message3%>"
					});
				});
			</script>
					<%
						}
					%>

					<%
						String message4 = (String) request.getAttribute("captcha.null");
						if (message4 != null) {
					%>
					<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message4%>"
					});
				});
			</script>
					<%
						}
					%>

					<%
						String message5 = (String) request.getAttribute("captcha.failed");
						if (message5 != null) {
					%>
					<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message5%>"
							});
						});
					</script>
					<%
						}
					%>
					
					<%
					String message7 = (String) request
							.getAttribute("password.format");
					if (message7 != null) {
				%>
				<script>
 $(document).ready(function() {
 jQuery.growl.error({
		message : "<%=message7%>"
						});
					});
				</script>
				<%
					}
				%>

					<div class="from-content">
						<div class="from-sec user-from-sec clearfix">
							<input type="text" class="form-control" placeholder="User Name"
								value="${loginUsersModel.user_Name}" name="User_Name"
								id="User_Name" required autofocus>
						</div>
						<div class="from-sec pass-from-sec clearfix">
							<input type="password" class="form-control"
								value="${loginUsersModel.user_Password}" placeholder="Password"
								name="User_Password" id="User_Password" required>
						</div>
						<div class="captcha-div clearfix">
							<a href="javascript:void(0);">
								<img src="./ImageCreator" alt="" title="" id="captcha_image">
								<i class="fa fa-refresh refreshcaptcha" aria-hidden="true"
								style="color: #fff; margin: 0 auto; font-size: 24px; padding: 2px 0px 0px 0px"></i>
							</a>
						</div>
						<div class="from-sec white-field clearfix">
							<input type="text" name="code" id="code" placeholder="Enter Captcha" class="form-control singup">
						</div>
						<button class="btn btn-lg btn-primary btn-block" type="submit">log
							In</button>
					</div>
				</div>
			</div>
		</form>
	</div>



	<script src="<%=basePath%>resources/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script src="<%=basePath%>resources/js/app.min.js"
		type="text/javascript"></script>
	<script src="<%=basePath%>resources/js/login/md5.js"
		type="text/javascript"></script>
	<script src="<%=basePath%>resources/js/bootstrap-datepicker.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="<%=basePath%>resources/vendors/growl/js/jquery.growl.js"></script>
	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('#datetimepicker1').datepicker();

							$("form")
									.submit(
											function(event) {
												if ($("input[type='text']")
														.val().trim() == ""
														|| $(
																"input[type='password']")
																.val().trim() == "") {
													event.preventDefault();
													jQuery.growl
															.error({
																message : "Username or Password cannot be Blank"
															});

												} else {

													var salt = getTokenFromServer();

													// 						    alert("salt=== "+salt);
													var saltLength = salt.length;
													var saltLengthHalf = Math
															.round(saltLength / 2);
													var firstPart = salt
															.substr(0,
																	saltLengthHalf);
													var secondPart = salt
															.substr(
																	saltLengthHalf,
																	saltLength);
													var password = $(
															"#User_Password")
															.val();
													// 							alert("password"+password);
													var myPass = md5(password);

													// 							alert("myPass "+myPass)
													var md5password = firstPart
															+ md5(password)
															+ secondPart;

													// 							alert("md5password "+md5password)
													// 							console.log("md5 password is "+md5password);

													$("#User_Password").val(
															md5password);
													loginForm.password.style.background = 'White';
												}

											});
						});
	</script>

	<script>
		function getTokenFromServer() {

			var getTokenURL = "get-token"

			var resFromServer = '';

			$.ajax({
				async : false,
				type : "POST",
				url : getTokenURL,
				contentType : 'application/text',
				cache : false,

				success : function(response) {

					resFromServer = response

					$("#token").val(resFromServer);
				},
				error : function(err) {
					//alert("error");
				}
			});

			return resFromServer;

		}
		$(document).ready(function() {
			$(document).on('focus', ':input', function() {
				$(this).attr('autocomplete', 'off');
			});
		});
	</script>
	
	<script type="text/javascript">
		$(".refreshcaptcha").click(function(){
			$("#captcha_image").attr("src", "./ImageCreator?aa="+Math.random());
		});
	</script>
</body>
</html>
