<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<table id="datatable-buttons2"
											class="table table-striped jambo_table">
											<thead>
												<tr>
<!-- 												<th>Sl.No.</th> -->
													<th>Urban Local Body</th>
													<th>No. of Departmental Tankers Engaged</th>
													<th>No. of Tankers Hired</th>
													<th>No. of PVC/RCC Tanks Deployed</th>
													<th>Population Served </th>
													<th>Quantity supplied (in Ltr)</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
													<c:when test="${not empty dailyTankersModels}">
														<c:forEach items="${dailyTankersModels}" var="data" varStatus="loop">														
													<c:choose>
															  <c:when test="${data.type == 'current' && not loop.last}">
																	<tr>
<%-- 																		<td><c:choose>
															  <c:when test="${data.slNo == null }">
															    Total
															  </c:when>
															  <c:otherwise>
															   <c:out value="${loop.index + 1}" />
															  </c:otherwise>
														  </c:choose></td> --%>
																		<td><c:out
																				value="${data.ulbNameModel.ulbModel.ulb_name}" /></td>
																		<td><c:out value="${data.no_tank_engd_dept}" /></td>
																		<td><c:out value="${data.no_tank_hire}" /></td>
																		<td><c:out value="${data.no_pvc_rcc_tanks_depl}" /></td>
																		<td>${fn:substringBefore(data.popu_served_tanks_pvc_rcc, '.')}</td>
																		<td><c:out value="${data.qty_mld}" /></td>

																	</tr>
													 </c:when>
													 <c:when test="${loop.last}">
												   <tr>
														<td><b>Total :</b></td>
														<td><b><c:out value="${data.total_tankers_engaged}" /></b></td>
														<td><b><c:out value="${data.total_tankers_hired}" /></b></td>
														<td><b><c:out value="${data.total_tanks_deployed}" /></b></td>
														<td><b>${fn:substringBefore(data.total_population_served, '.')}</b></td>
														<td><b><c:out value="${data.total_quantity_supplied}" /></b></td>
													</tr>												
												  </c:when>
													 </c:choose>
														</c:forEach>
													</c:when>
													<c:otherwise>
														<td colspan="10" align="center">No Records Found</td>
													</c:otherwise>
												</c:choose>
											</tbody>
										</table>