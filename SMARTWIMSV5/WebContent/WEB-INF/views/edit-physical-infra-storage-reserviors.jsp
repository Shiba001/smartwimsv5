<%@ include file="header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<fmt:parseDate value="${physicalInfraStorageReservoirsModel.dueDate}" var="parsedDate" 
                              pattern="dd-MM-yyyy" /><fmt:formatDate pattern="yyyy-MM-dd" value="${parsedDate}" var="fmtDueDate"/>
                               <c:set var="date_of_report" value="${physicalInfraStorageReservoirsModel.getDate_of_report()}"/>
<%@ include file="entry-header.jsp"%> 
<%@ page import="com.orissa.common.*"%>
<%	
		String strip_empty = (String) request.getAttribute("strip.empty");

      if (strip_empty != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=strip_empty%>"
       });
       });
      </script>
     <%
      }
     %>
<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>

	<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
     
	<%
      String message = (String) request.getAttribute("number.of.ground.storage.reservoirs");

      if (message != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message1 = (String) request.getAttribute("capacity.of.ground.storage.reservoirs");

      if (message1 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message1%>"
       });
       });
      </script>
     <%
      }
     %>
<%
      String message2 = (String) request.getAttribute("number.of.elevated.storage.reservoirs");

      if (message2 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message2%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message3 = (String) request.getAttribute("number.of.elevated.storage.reservoirs");

      if (message3 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message3%>"
       });
       });
      </script>
     <%
      }
     %> 
     
           <%
      String message4 = (String) request.getAttribute("number.of.ground.storage.reservoirs.maxlength");

      if (message4 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message4%>"
       });
       });
      </script>
     <%
      }
     %>
      <%
      String message5 = (String) request.getAttribute("capacity.of.ground.storage.reservoirs.maxlength");

      if (message5 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message5%>"
       });
       });
      </script>
     <%
      }
     %>
      <%
      String message6 = (String) request.getAttribute("number.of.elevated.storage.reservoirs.maxlength");

      if (message6 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message6%>"
       });
       });
      </script>
     <%
      }
     %>
      <%
      String message7 = (String) request.getAttribute("capacity.of.elevated.storage.reservoirs.maxlength");

      if (message7 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message7%>"
       });
       });
      </script>
     <%
      }
     %>
     
   <%	
			String message11 = (String) request.getAttribute("ULBID.needed");

      if (message11 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message11%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message12 = (String) request.getAttribute("dateofreport.needed");

      if (message12 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message12%>"
       });
       });
      </script>
     <%
      }
     %>   
     
     <%	
			String message13 = (String) request.getAttribute("duedate.needed");

      if (message13 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message13%>"
       });
       });
      </script>
     <%
      }
     %>
     
 <div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="list-physical-infra-storage-reserviors" class="edit-link">
					<i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit
		</a>
	</div>
</div>
<div class="col-md-12">
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3">
				<div class="x_panel">
					<div class="x_title text-center text-capitalize">
						<h2>
							Physical Infra Reservoirs
						</h2>
						<h4>
							Figures for: <label id="show_date"><fmt:parseDate value="${physicalInfraStorageReservoirsModel.date_of_report}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></label>
						</h4>
					</div>
					<div class="x_content">
						<br>


						<form action="update-physical-infra-storage-reserviors" method="POST" onsubmit="return physicalInfraReservoirs();" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
							<input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
							<c:set var="id" value="${physicalInfraStorageReservoirsModel.phyInfra_sr_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
									<input type="hidden" name="phyInfraSrId"
											value="<%=id%>" />
							<div class="form-field">
								<div class="form-control-wrap">
									<input class="form-control" id="date_of_report" placeholder="" value="" readonly="" name="date_of_report" type="hidden">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Ground Storage Reservoirs:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_grnd_strg_res" id="no_grnd_strg_res" value="${fn:substringBefore(physicalInfraStorageReservoirsModel.no_grnd_strg_res, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Capacity of Ground Storage
													Reservoirs (in ML):<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="cpty_grnd_strg_res" id="cpty_grnd_strg_res" value="${physicalInfraStorageReservoirsModel.cpty_grnd_strg_res }" class="form-control col-md-7 col-xs-12 decimal_number" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Elevated Storage Reservoirs:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_ele_strg_res" id="no_ele_strg_res" value="${fn:substringBefore(physicalInfraStorageReservoirsModel.no_ele_strg_res, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Capacity of Elevated Storage
													Reservoirs (in ML):<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="cpty_ele_strg_res" id="cpty_ele_strg_res" value="${physicalInfraStorageReservoirsModel.cpty_ele_strg_res }" class="form-control col-md-7 col-xs-12 decimal_number" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Next Maintenance Date:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12 date" id="dateIp">
									<input  name="dueDate" id="dueDate"  value=""  class="form-control col-md-7 col-xs-12" type="text" readonly>
								</div>
							</div>

							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Update</button>
								<a href="list-physical-infra-storage-reserviors" class="btn btn-primary">Back</a>
							</div>

						</form>

					</div>
				</div>
			</div>
        </div>
  
<%@ include file="footer.jsp"%>
<script
	src="<%=basePath%>resources/validation/physical-infra-reservoirs.js"></script>