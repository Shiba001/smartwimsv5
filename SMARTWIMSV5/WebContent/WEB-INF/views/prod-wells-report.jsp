<%@ page import="java.util.*"%>
<%@ page import="java.util.Map.*"%>
<%@ page import="com.orissa.model.*"%>
<%@ include file="header.jsp"%>
<%-- <%@ include file="dashboard-header.jsp"%> --%>
<div class="content-wrapper clearfix">
<!-- page content -->
   <div class="col-md-12">
   	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="row top_tiles">
			<div class="col-xs-12 col-sm-12">
				<div class="x_panel">				
					<div class="x_title">
						<h2 class="custome-h2">View Production Wells Details</h2>	
					</div>
					<div class="x_content table table-responsive">
						<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
								<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>&nbsp;</th>
								</tr>
							</thead>
								<tbody>
							<%
								Map<String, List<PhysicalInfraProductionWellsModel>> outerMap = (Map<String, List<PhysicalInfraProductionWellsModel>>) request
																			.getAttribute("prodWellReport");
												if (outerMap != null) {
													System.out.println("outerMap" + outerMap);
													List<PhysicalInfraProductionWellsModel> innerList = null;
													int i = 1;
													for (Entry<String, List<PhysicalInfraProductionWellsModel>> outerEntry : outerMap.entrySet()) { %>
														<tr>
															<td><%=i %></td>
															<td><%=outerEntry.getKey() %></td>
															
														
														
														
													<%	innerList = outerEntry.getValue();
														
														if (innerList != null & innerList.size() > 0) {
															
															System.out.println("innerList.size() --->>>>"
																	+ innerList.size());
															%>
															<td>
																<table width="100%">
																	<thead>
																		<tr>
																			<th>Sl.No.</th>
																			<th>Location of The Production Well</th>
																			<th>Diameter of The Production Well (in mm)</th>
																			<th>Discharge (in lmp)</th>
																			<th>Depth (in Meter)</th>
																		</tr>
																	</thead>
																		<tbody>
															<%
															for (int j = 0;j < innerList.size(); j++) { 
																PhysicalInfraProductionWellsModel physicalInfraProductionWellsModel = innerList.get(j);
																int k= j+1;
																%>
																<% if(physicalInfraProductionWellsModel.getLoc_of_prod_well() !=null) { %>
																
																		<tr>
																			<td><%=k %>.</td>
																			<td><%=physicalInfraProductionWellsModel.getLoc_of_prod_well()%></td>
																			<td><%= physicalInfraProductionWellsModel.getDiam_prod_well()%></td>
																			<td><%=physicalInfraProductionWellsModel.getDischarge() %></td>
																			<td><%= physicalInfraProductionWellsModel.getDepth()%></td>
																		</tr>
																<% } else{ %>
																	<tr colspan="5">
																		<td>No Record Found</td>
																	</tr>	
														<%	 } } %>
														</tbody>
														</table>
																</td>
															
														<% }
														i++;%>
													</tr>	
													<%}
												}
										%>
							
						
								

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<%@ include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		var handleDataTableButtons = function() {
			if ($("#datatable-buttons2").length) {
				$("#datatable-buttons2").DataTable({
					dom : "Bfrtip",
					paging : false,
					buttons : [ {
						extend : "copy",
						className : "btn-sm"
					}, {
						extend : "csv",
						className : "btn-sm"
					}, {
						extend : "excel",
						className : "btn-sm"
					}, {
						extend : "pdf",
						className : "btn-sm"
					}, {
						extend : "print",
						className : "btn-sm"
					}, ],
					responsive : true
				});
			}
		};

		TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons();
				}
			};
		}();

		$('#datatable').dataTable();

		
		$('#datatable-responsive').DataTable();



		var $datatable = $('#datatable-checkbox');

		
		$datatable.on('draw.dt', function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_flat-green'
			});
		});

		TableManageButtons.init();
	});
</script>
<%-- <script src="<%=basePath%>resources/dashboard/physical-infra-report.js"></script> --%>