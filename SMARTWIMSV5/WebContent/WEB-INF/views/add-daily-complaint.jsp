<%@ include file="header.jsp"%>

<%@ include file="entry-header.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
      String message5 = (String) request.getAttribute("water.supply.complaints.resolved");

      if (message5 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message5%>"
       });
       });
      </script>
     <%
      }
     %>
			<%
						String message = (String) request.getAttribute("water.supply.complaints.received");
						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>

<%
      String message2 = (String) request.getAttribute("tubewell.complaints.received");

      if (message2 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message2%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message3 = (String) request.getAttribute("tubewell.complaints.resolved");

      if (message3 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message3%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message4 = (String) request.getAttribute("data.entry.exist");

      if (message4 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message4%>"
       });
       });
      </script>
     <%
      }
     %>
     
      <%	
			String message6 = (String) request.getAttribute("water.supply.complaints.received.maxlength");

      if (message6 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message6%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message7 = (String) request.getAttribute("water.supply.complaints.resolved.maxlength");

      if (message7 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message7%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message8 = (String) request.getAttribute("tubewell.complaints.received.maxlength");

      if (message8 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message8%>"
       });
       });
      </script>
     <%
      }
     %>
     
     
     <%	
			String message9 = (String) request.getAttribute("tubewell.complaints.resolved.maxlength");

      if (message9 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message9%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message10 = (String) request.getAttribute("remarks.maxlength");

      if (message10 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message10%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message11 = (String) request.getAttribute("ULBID.needed");

      if (message11 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message11%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message12 = (String) request.getAttribute("dateofreport.needed");

      if (message12 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message12%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
     
<div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="list-daily-complaint" class="edit-link">
					<i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit
		</a>
	</div>
</div>
		 <div class="col-md-12">
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3">
				<div class="x_panel">
					<div class="x_title text-center text-capitalize">
						<h2>
							Complaints
						</h2>
						<h4>
							Figures for: <label id="show_date"><fmt:parseDate value="${startDate}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></label>
						</h4>
					</div>
					<div class="x_content">
						<br>
						<form action="add-daily-complaint" method="post" onsubmit="return dailyComplaints();" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
							<input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
							<div class="form-field">
								<div class="form-control-wrap">
									<input class="form-control" id="date_of_report" placeholder="" value="" readonly="" name="date_of_report" type="hidden">

								</div>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Water Supply Complaints
									Received:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_ws_comp_recv" id="no_ws_comp_recv" value="${fn:substringBefore(dailyComplaintsModel.no_ws_comp_recv, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">

								</div>													
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Water Supply Complaints
									Resolved:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_ws_comp_resolved" id="no_ws_comp_resolved" value="${fn:substringBefore(dailyComplaintsModel.no_ws_comp_resolved, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Tubewell Complaints Received: <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_tw_comp_recv" id="no_tw_comp_recv" value="${fn:substringBefore(dailyComplaintsModel.no_tw_comp_recv, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Tubewell Complaints Resolved: <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" id="no_tw_comp_resolved" name="no_tw_comp_resolved" value="${fn:substringBefore(dailyComplaintsModel.no_tw_comp_resolved, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>								
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Remarks : </label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<textarea class="form-control" rows="5" name="remarks" maxlength="200">${dailyComplaintsModel.remarks}</textarea>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Save</button>
								<button type="reset" class="btn btn-primary">Clear</button>
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
 
<!-- /page content -->
<%@ include file="footer.jsp"%>
<script src="<%=basePath%>resources/validation/daily-complaints.js"></script>