<%@ include file="header.jsp"%>
<script type="text/javascript"
	src="https://www.gstatic.com/charts/loader.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="dashboard-header.jsp"%>
<div class="col-md-12 card-sec clearfix">
	<h2>
		Average for <label id="place">Odisha</label>
	</h2>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="cardouter">
			<div class="card">
				<div class="card-panel">
					<strong> <font id="lpcdBoxValue"
						color="<c:choose><c:when test="${lpcdBoxValue >= 135 }">green</c:when><c:otherwise>red</c:otherwise></c:choose>">${lpcdBoxValue}</font>
						Ltr
					</strong>
					<p>LPCD</p>
					<a href="lpcd" class="btn btn-default">Show Details in Map</a>
				</div>
				<div class="card-panel">
					<strong>LPCD<span>( No of ULBs)</span></strong>
					<div class="pai-chat">
						<div class="pai-chat" id="lpcddonutchart"></div>
					</div>
					<div class="value">
						<div class="value-sec">
							<span class="green"></span>
							<p class="code-p">LPCD >= 135</p>
						</div>
						<div class="value-sec">
							<span class="light"></span>
							<p class="code-p">LPCD < 135</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="cardouter">
			<div class="card">
				<div class="card-panel">
					<strong> <font id="waterSupplyCoverageBoxValue" 
						color="<c:choose><c:when test="${waterSupplyCoverageBoxValue >= 80 }">#3c8a2e</c:when><c:when test="${waterSupplyCoverageBoxValue < 80 && waterSupplyCoverageBoxValue >=50 }">#c3c3c3</c:when><c:otherwise>#000</c:otherwise></c:choose>">${waterSupplyCoverageBoxValue }</font>
						%
					</strong>
					<p>Water Supply Coverage</p>
					<a href="water-supply-coverage" class="btn btn-default">Show
						Details in Map</a>
				</div>
				<div class="card-panel">
					<strong>Coverage <span>( No of ULBs)</span></strong>
					<div class="pai-chat">
						<div class="pai-chat" id="watersupplycoveragedonutchart"></div>
					</div>
					<div class="value">
						<div class="value-sec">
							<span class="green"></span>
							<p class="code-p">>= 80%</p>
						</div>
						<div class="value-sec">
							<span class="light"></span>
							<p class="code-p">=50% - <80%</p>
						</div>
						<div class="value-sec">
							<span class="black"></span>
							<p class="code-p"><50%</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="cardouter">
			<div class="card">
				<div class="card-panel">
					<strong> <font id="surplusDeficitBoxValue"
						color="<c:choose><c:when test="${surplusDeficitBoxValue >= 0 }">green</c:when><c:otherwise>red</c:otherwise></c:choose>">
							${surplusDeficitBoxValue }</font> MLD
					</strong>
					<p>Deficit / Surplus</p>
					<a href="deficit-surplus" class="btn btn-default">Show Details
						in Map</a>
				</div>
				<div class="card-panel">
					<strong>Deficit/Surplus<span>( No of ULBs)</span></strong>
					<div class="pai-chat">
						<div class="pai-chat" id="deficitSurplusdonutchart"></div>
					</div>
					<div class="value">
						<div class="value-sec">
							<span class="green"></span>
							<p class="code-p">Surplus</p>
						</div>
						<div class="value-sec">
							<span class="light"></span>
							<p class="code-p">Deficit</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="cardouter">
			<div class="card">
				<div class="card-panel">
					<strong> <font id="avgHrOfSupplyBoxValue">${avgHrOfSupplyBoxValue }</font>
						Hr
					</strong>
					<p>Average Hours Of Water Supply</p>
					<a href="avg-hr-supply" class="btn btn-default">Show Details in
						Map</a>
				</div>
				<div class="card-panel">
					<strong>Hours <span>( No of ULBs)</span></strong>
					<div class="pai-chat">
						<div class="pai-chat" id="averageHoursOfWaterSupplydonutchart"></div>
					</div>
					<div class="value">
						<div class="value-sec">
							<span class="green"></span>
							<p class="code-p">
								Hours >= <label id="avgHrOfSupplyValue1">${avgHrOfSupplyBoxValue }</label>
							</p>
						</div>
						<div class="value-sec">
							<span class="light"></span>
							<p class="code-p">
								Hours < <label id="avgHrOfSupplyValue2">${avgHrOfSupplyBoxValue }</label>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="cardouter">
			<div class="card">
				<div class="card-panel">
					<strong> <font id="totalPercentageBoxValue" color="<c:choose><c:when test="${totalPercentageBoxValue >= 80 }">#3c8a2e</c:when><c:when test="${totalPercentageBoxValue < 80 && totalPercentageBoxValue >=50 }">#c3c3c3</c:when><c:otherwise>#000</c:otherwise></c:choose>">${totalPercentageBoxValue }</font>
						%
					</strong>
					<p>Water Charges Collection</p>
					<a href="water-charges-collection" class="btn btn-default">Show
						Details in Map</a>
				</div>
				<div class="card-panel">
					<strong>Collection <span>( No of ULBs)</span></strong>
					<div class="pai-chat">
						<div class="pai-chat" id="waterChargesCollectiondonutchart"></div>
					</div>
					<div class="value">
						<div class="value-sec">
							<span class="green"></span>
							<p class="code-p">>= 80%</p>
						</div>
						<div class="value-sec">
							<span class="light"></span>
							<p class="code-p">=50% - <80%</p>
						</div>
						<div class="value-sec">
							<span class="black"></span>
							<p class="code-p"><50%</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="cardouter">
			<div class="card">
				<div class="card-panel">
					<strong><font id="testingBoxValue" color="<c:choose><c:when test="${testingBoxValue >= 100 }">#3c8a2e</c:when><c:otherwise>#c3c3c3</c:otherwise></c:choose>"><fmt:formatNumber type="number" 
            maxFractionDigits="2" value="${testingBoxValue}" /></font>
						%</strong>
					<p>Water Testing Samples Found OK</p>
					<a href="water-testing" class="btn btn-default">Show Details in
						Map</a>
				</div>
				<div class="card-panel">
					<strong>Samples <span>( No of ULBs)</span></strong>
					<div class="pai-chat">
						<div id="waterTesting" style="height: 200px;"></div>
					</div>
					<div class="value">
						<div class="value-sec">
							<span class="green"></span>
							<p class="code-p">Found Ok</p>
						</div>
						<div class="value-sec">
							<span class="light"></span>
							<p class="code-p">Found Not Ok</p>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="cardouter">
			<div class="card">
				<div class="card-panel">
					<strong> <font id="complaintBoxValue" color="#3c8a2e"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${complaintBoxValue}" /></font>
						%
					</strong>
					<p>Complaints Resolved</p>
					<a href="complaints-resolved" class="btn btn-default">Show
						Details in Map</a>
				</div>
				<div class="card-panel">
					<strong>Resolved <span>( % Of Complaints)</span></strong>
					<div class="pai-chat">
						<div class="pai-chat" id="complaintsResolveddonutchart"></div>
					</div>
					<div class="value">
						<div class="value-sec">
							<span class="green"></span>
							<p class="code-p">Resolved</p>
						</div>
						<div class="value-sec">
							<span class="light"></span>
							<p class="code-p">UnResolved</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="cardouter">
			<div class="card">
				<div class="card-panel">
					<p>Data Entry Status</p>
					<a href="data-entry" class="btn btn-default">Show Details in
						Map</a>
				</div>
				<div class="card-panel">
					<strong>Data Entry status<span>( No of ULBs)</span></strong>
					<div class="pai-chat">
						<div class="pai-chat" id="dataEntryStatusdonutchart"></div>
					</div>
					<div class="value">
						<div class="value-sec">
							<span class="green"></span>
							<p class="code-p">Complete</p>
						</div>
						<div class="value-sec">
							<span class="light"></span>
							<p class="code-p">Partial</p>
						</div>
						<div class="value-sec">
							<span class="black"></span>
							<p class="code-p">None</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="cardouter">
			<div class="card">
				<div class="card-panel">
					<p>Data Approval Status</p>
					<a href="data-approval" class="btn btn-default">Show Details in
						Map</a>
				</div>
				<div class="card-panel">
					<strong>Approval <span>( No of ULBs)</span></strong>
					<div class="pai-chat">
						<div class="pai-chat" id="dataApprovalStatusdonutchart"></div>
					</div>
					<div class="value">
						<div class="value-sec">
							<span class="green"></span>
							<p class="code-p">Approved</p>
						</div>
						<div class="value-sec">
							<span class="light"></span>
							<p class="code-p">Pending</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 card-bottom">
	<div class="row-height">
		<div class="col-xs-12 col-sm-6 col-md-3 col-md-3 col-height">
			<div class="bottom-card">
				<h3>
					<font id="popServedBoxValue">${popServedBoxValue }</font>
				</h3>
				<p>Total Population Served</p>
				<a href="total-population-served" class="btn btn-default">Show
					Details</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-md-3 col-height">
			<div class="bottom-card">
				<h3>
					<font id="tankerBoxValue">${tankerBoxValue }</font> MLD
				</h3>
				<p>Tankers Water Supply</p>
				<a href="tanker" class="btn btn-default">Show Details</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-md-3 col-height">
			<div class="bottom-card">
				<h3>&nbsp;</h3>
				<p>Physical Infrastructure</p>
				<a href="physical-infra" class="btn btn-default">Show Details</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-md-3 col-height">
			<div class="bottom-card">
				<h3>
					<font id="pipeWaterSupplyBoxValue">${pipeWaterSupplyBoxValue }</font>
					MLD
				</h3>
				<p>Pipe Water Supply</p>
				<a href="pipe-water-supply" class="btn btn-default">Show Details</a>
			</div>
		</div>
	</div>
</div>

<%@ include file="footer.jsp"%>
<script type="text/javascript"
	src="<%=basePath%>resources/js/dashboard/dashboard.js"></script>
<script>
	//FOR EQUAL HEIGHT
	var cardRowHeight = $('.card-bottom').height();
	$('.bottom-card').css('height', cardRowHeight);
	// FOR EQUAL HEIGHT
</script>
<script>
	var isAmrut = false;

	var ulbGreater135 = '${ulbGreater135}';
	var ulbLesser135 = '${ulbLesser135}';
	var waterSupplyCoverageGreater100 = '${waterSupplyCoverageGreater100}';
	var waterSupplyCoverageGreater75 = '${waterSupplyCoverageGreater75}';
	var waterSupplyCoverageGreater50 = '${waterSupplyCoverageGreater50}';
	var waterSupplyCoverageLesser50 = '${waterSupplyCoverageLesser50}';
	var deficit = '${deficit}';
	var surplus = '${surplus}';
	var avgGreater = '${avgGreater}';
	var avgLesser = '${avgLesser}';
	var collectionGreater100 = '${collectionGreater100}';
	var collectionGreater75 = '${collectionGreater75}';
	var collectionGreater50 = '${collectionGreater50}';
	var collectionLesser50 = '${collectionLesser50}';

	var complete = '${complete}';
	var partial = '${partial}';
	var notApproved = '${notApproved}';
	var approved = '${approved}';
	var noDataEntry = '${noDataEntry}';
	var complaintResolved = '${complaintResolved}';
	var complaintUnResolved = '${complaintUnResolved}';
	var frcFoundOK = '${frcFoundOK}';
	var bacteriologicalFoundOK = '${bacteriologicalFoundOK}';
	var frcFoundNotOK = '${frcFoundNotOK}';
	var bacteriologicalFoundNotOK = '${bacteriologicalFoundNotOK}';
</script>