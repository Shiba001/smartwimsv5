<%@ include file="header.jsp"%>

<%@ include file="set-target-water-charge-header.jsp"%>
<%-- <%@ include file="entry-header.jsp"%> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>

	<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
     
	<%
						String message = (String) request.getAttribute("Success");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.notice({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					<%
						String message2 = (String) request.getAttribute("Failure");

						if (message2 != null) {
					%>
					<script>
				 		 $(document).ready(function() {
				 		 jQuery.growl.error({
				 				message : "<%=message2%>"
							});
						});
					</script>
					<%
						}
					%>
<div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="add-target-water-charges-tax" class="edit-link">
					<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;New
		</a>
	</div>
</div>
   <div class="col-md-12">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="row top_tiles">
			<div class="col-xs-12 col-sm-12">
				<div class="x_panel">
					<div class="x_title">
						<h2 class="custome-h2">List Target Water Charges</h2>
					</div>
					<div class="x_content table table-responsive">
						<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
							<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>Mode of Target</th>
									<th>Jan</th>
									<th>Feb</th>
									<th>Mar</th>
									<th>Apr</th>
									<th>May</th>
									<th>Jun</th>
									<th>Jul</th>
									<th>Aug</th>
									<th>Sep</th>
									<th>Oct</th>
									<th>Nov</th>
									<th>Dec</th>
									<th>Annual</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty targetModels}">
										<c:forEach items="${targetModels}" var="data">
											<c:set var="count" value="${count + 1}" scope="page" />
											<c:set var="id" value="${data.trgt_id}" />
											<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
											<c:choose>
											<c:when test="${data.trgt_mode == 'Monthly'}">
												<tr>
												
												<td><c:out value="${count}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td>${data.trgt_mode}</td>
												<td>${fn:substringBefore(data.m_trgt_10, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_11, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_12, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_1, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_2, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_3, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_4, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_5, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_6, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_7, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_8, '.')}</td>
												<td>${fn:substringBefore(data.m_trgt_9, '.')}</td>
												<td>0</td>
												<td><a
													href="edit-target-water-charges?trgt_id=<%=id%>"><i
														class="fa fa-pencil"></i></a> <%-- <a
													href="delete-target-water-charges?trgt_id=<%=id%>" onclick="return confirm('Are you sure you want to delete?')"><i
														class="fa fa-trash-o"></i></a></td> --%>
													<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
													<i class="fa fa-trash-o delete_pointer"></i>
													</a>
												</td>
											</tr>
											</c:when>
											<c:otherwise>
												<tr>
												<td><c:out value="${count}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td>${data.trgt_mode}</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>0</td>
												<td>${data.y_trgt}</td>
												<td><a
													href="edit-target-water-charges?trgt_id=<%=id%>"><i
														class="fa fa-pencil"></i></a> <%-- <a
													href="delete-target-water-charges?trgt_id=<%=id%>" onclick="return confirm('Are you sure you want to delete?')"><i
														class="fa fa-trash-o"></i></a></td> --%>
													<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
													<i class="fa fa-trash-o delete_pointer"></i>
													</a>
												</td>
											</tr>
											</c:otherwise>
											</c:choose>
											
										</c:forEach>
									</c:when>									
									<%-- <c:otherwise>
										<td colspan="17" align="center">No Records Found</td>
									</c:otherwise> --%>
								</c:choose>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<%@ include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		var handleDataTableButtons = function() {
			if ($("#datatable-buttons2").length) {
				$("#datatable-buttons2").DataTable({
					dom : "Bfrtip",
					paging : true,					
					buttons : [ {
						extend : "copy",
						className : "btn-sm"
					}, {
						extend : "csv",
						className : "btn-sm"
					}, {
						extend : "excel",
						className : "btn-sm"
					}, {
						extend : "pdf",
						className : "btn-sm"
					}, {
						extend : "print",
						className : "btn-sm"
					}, ],
					responsive : true
				});
			}
		};

		TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons();
				}
			};
		}();

		$('#datatable').dataTable();

		
		$('#datatable-responsive').DataTable();



		var $datatable = $('#datatable-checkbox');

		
		$datatable.on('draw.dt', function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_flat-green'
			});
		});

		TableManageButtons.init();
	});
</script>

<!-- Modal Start -->
	<div class="modal fade" id="deleteModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form class="form-horizontal" method="post" action="delete-target-water-charges">
						  <input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Delete Target Water Charge</h4>
							<hr />
						</div>
						<div class="modal-body">
							<input type="hidden" name="trgtId" id="trgtId" value="">
							<div class="text-center">
								<span>Are you sure, you want to <b>
								<font color="green">Delete</font> 
								<strong> this Target Water Charge</strong></span> ?
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
							<input type="submit" class="btn btn-primary" value="Delete
								Target Water Charge">
						</div>
					</form>
				</div>
			</div>
		</div>
<!-- Modal End -->

<!-- Datatables -->
<script src="<%=basePath%>resources/js/datatable/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<!-- Datatables -->
<script>
function confirmation(id)
{
	$("#trgtId").val(id);
}
</script>
