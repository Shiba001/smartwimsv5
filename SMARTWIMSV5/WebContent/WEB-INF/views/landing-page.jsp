<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	//System.out.println("BasePath : " + basePath);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style> html{display : none ; } </style>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<meta name="msapplication-TileColor">
<meta name="msapplication-TileImage" content="<%=basePath%>resources/images/favicon/ms-icon-144x144.png">
<meta name="theme-color">
<title>Landing Page</title>
<link rel="apple-touch-icon" sizes="57x57" href="<%=basePath%>resources/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<%=basePath%>resources/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<%=basePath%>resources/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath%>resources/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<%=basePath%>resources/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<%=basePath%>resources/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<%=basePath%>resources/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<%=basePath%>resources/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<%=basePath%>resources/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<%=basePath%>resources/images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<%=basePath%>resources/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath%>resources/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<%=basePath%>resources/images/favicon/favicon-16x16.png">
<link href="<%=basePath%>resources/css/custome-responsive.css" type="text/css" rel="stylesheet"/>
<link href="<%=basePath%>resources/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
<link href="<%=basePath%>resources/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"> 
<link href="<%=basePath%>resources/css/landing-style.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript">
if( self == top ) {
document.documentElement.style.display = 'block' ; 
} 
</script>
</head>

<body>
	<div class="back-div">
		<div class="landing-content">
       		<div class="container">
       			<a href="javascript:void(0);">
	                <div class="landing-sec amrut">
	                    <img class="img-responsive" src="<%=basePath%>resources/images/icon1.png"/>
	                    <h4>AMRUT</h4>
	                </div>
                </a>
                <a href="swims-login">
	                <div class="landing-sec swims">
	                    <img class="img-responsive" src="<%=basePath%>resources/images/icon2.png"/>
	                    <h4>SWIMS</h4>
	                </div>
                </a>
            <a href="javascript:void(0);">
	                <div class="landing-sec ulb">
	                    <img class="img-responsive" src="<%=basePath%>resources/images/icon4.png"/>
	                    <h4>Other ULBs</h4>
	                    <p>Works/Projects</p>
	                </div>
                </a>
                <a href="javascript:void(0);">
	                <div class="landing-sec city">
	                    <img class="img-responsive" src="<%=basePath%>resources/images/icon3.png"/>
	                    <h4>Smart Cities</h4>
	                </div>
                </a>
                <a href="javascript:void(0);">
	                <div class="landing-sec nulm">
	                    <img class="img-responsive" src="<%=basePath%>resources/images/icon5.png"/>
	                    <h4>NULM</h4>
	                </div>
                </a>
               <a href="javascript:void(0);">
	                <div class="landing-sec rev">
	                    <img class="img-responsive" src="<%=basePath%>resources/images/icon6.png"/>
	                    <h4>Revenue</h4>
	                    <p>Management</p>
	                </div>
                </a>
                <a href="javascript:void(0);">
	                <div class="landing-sec cm-pri">
	                    <img class="img-responsive" src="<%=basePath%>resources/images/icon7.png"/>
	                    <h4>CM's Priority</h4>
	                </div>
                </a>
                <a href="javascript:void(0);">
	                <div class="landing-sec pmay">
	                    <img class="img-responsive" src="<%=basePath%>resources/images/icon8.png"/>
	                    <h4>PMAY</h4>
	                </div>
                </a>
            </div>
       </div>
   	   <div class="landing-bg"></div>
   	</div>
   
</body>
</html>
<script src="<%=basePath%>resources/js/jquery-min.js" type="text/javascript"></script>
<script src="<%=basePath%>resources/js/bootstrap.min.js" type="text/javascript"></script>