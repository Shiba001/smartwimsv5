<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="header.jsp"%>
<%@ include file="report-header.jsp"%>
<%@ page import="java.util.Date,java.text.SimpleDateFormat,java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<!-- page content -->
<div class="col-md-12">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="row top_tiles">
			<div class="col-xs-12 col-sm-12">
				<div class="x_panel">
					<div class="x_title">
						<h2 class="custome-h2">View Deficit/Surplus Report</h2>
					</div>
					<div class="x_content table-div table-responsive">
						<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
								<tr>
									<th>Urban Local Body</th>
									<% 
								     String dateStr = (String)request.getAttribute("startDate");
								     SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
								     Date result = formater.parse(dateStr);
								     SimpleDateFormat newFormater = new SimpleDateFormat("dd MMM yyyy");
								%>
									<th><%= newFormater.format(result) %></th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty dailyWaterSupplyModels}">
										<c:forEach items="${dailyWaterSupplyModels}" var="data"
											varStatus="loop">
											<c:choose>
												<c:when test="${data.type == 'current' && not loop.last}">
													<tr>
														<td><c:out value="${data.ulbNameModel.ulbModel.ulb_name}" /></td>
														<c:choose>
															<c:when test="${data.surplus_deficit >= 0 }">
																<td style="color:#3c8a2e;">${data.surplus_deficit}</td>
															</c:when>
															<c:otherwise>
																<td style="color:#F00000;">${data.surplus_deficit}</td>
															</c:otherwise>
														</c:choose>
													</tr>
												</c:when>
												<c:when test="${loop.last}">
													<tr>
														<td><b>Total:</b></td>
															<c:choose>
																<c:when test="${data.total_surplus_deficit >= 0 }">
																	<td style="color:#3c8a2e;"><b>${data.total_surplus_deficit}</b></td>
																</c:when>
																<c:otherwise>
																	<td style="color:#F00000;"><b>${data.total_surplus_deficit}</b></td>
																</c:otherwise>
														</c:choose>
													</tr>
												</c:when>
											</c:choose>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<td colspan="10" align="center">No Records Found</td>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<%@ include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		var handleDataTableButtons = function() {
			if ($("#datatable-buttons2").length) {
				$("#datatable-buttons2").DataTable({
					dom : "Bfrtip",
					paging : false,
					bSort : false,
					buttons : [ {
						extend : "copy",
						className : "btn-sm"
					}, {
						extend : "csv",
						className : "btn-sm"
					}, {
						extend : "excel",
						className : "btn-sm"
					}, {
						extend : "pdf",
						className : "btn-sm"
					}, {
						extend : "print",
						className : "btn-sm"
					}, ],
					responsive : true
				});
			}
		};

		TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons();
				}
			};
		}();

		$('#datatable').dataTable();

		$('#datatable-responsive').DataTable();

		var $datatable = $('#datatable-checkbox');

		$datatable.on('draw.dt', function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_flat-green'
			});
		});

		TableManageButtons.init();
	});
</script>


<!-- Datatables -->
<script
	src="<%=basePath%>resources/js/datatable/datatables.net/js/jquery.dataTables.min.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.print.min.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script
	src="<%=basePath%>resources/js/datatable/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<!-- Datatables -->

<script
	src="<%=basePath%>resources/js/innerPage/deficit-report.js"></script>