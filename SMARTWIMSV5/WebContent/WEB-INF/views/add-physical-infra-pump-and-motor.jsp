<%@ include file="header.jsp"%>
<c:if test="${physicalInfraPumpsAndMotorsModels.size() > 0 }">
<fmt:parseDate value="${physicalInfraPumpsAndMotorsModels.get(0).getDueDate()}" var="parsedDate" 
                              pattern="dd-MM-yyyy" /><fmt:formatDate pattern="yyyy-MM-dd" value="${parsedDate}" var="fmtDueDate"/>
                              <c:set var="date_of_report" value="${physicalInfraPumpsAndMotorsModels.get(0).getDate_of_report()}"/>
</c:if>

<%@ include file="entry-header.jsp"%> 
<%@ page import="com.orissa.common.*"%>
 <%	
			String message13 = (String) request.getAttribute("duedate.needed");

      if (message13 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message13%>"
       });
       });
      </script>
     <%
      }
     %>
     
    <%	
		String dueDate = (String) request.getAttribute("duedate.greater");

      if (dueDate != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=dueDate%>"
       });
       });
      </script>
     <%
      }
     %>
<%	
		String validYear = (String) request.getAttribute("Year.of.Installation.validYear");

      if (validYear != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=validYear%>"
       });
       });
      </script>
     <%
      }
     %>
<%	
		String specialChar1 = (String) request.getAttribute("Location.of.the.Pump/Motor.specialChar");

      if (specialChar1 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=specialChar1%>"
       });
       });
      </script>
     <%
      }
     %>
     <%	
		String specialChar2 = (String) request.getAttribute("Power.(HP/KW).specialChar");

      if (specialChar2 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=specialChar2%>"
       });
       });
      </script>
     <%
      }
     %>
     <%	
		String specialChar3 = (String) request.getAttribute("Head.in.(meter).specialChar");

      if (specialChar3 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=specialChar3%>"
       });
       });
      </script>
     <%
      }
     %>
     <%	
		String specialChar4 = (String) request.getAttribute("Discharge.in(lps).specialChar");

      if (specialChar4 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=specialChar4%>"
       });
       });
      </script>
     <%
      }
     %>
     <%	
		String specialChar5 = (String) request.getAttribute("Type.of.Pump.&.Motor.specialChar");

      if (specialChar5 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=specialChar5%>"
       });
       });
      </script>
     <%
      }
     %>
     <%	
		String specialChar6 = (String) request.getAttribute("Year.of.Installation.specialChar");

      if (specialChar6 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=specialChar6%>"
       });
       });
      </script>
     <%
      }
     %>
<!-- ------------------------------------- -->

<%	
		String strip_empty = (String) request.getAttribute("strip.empty");

      if (strip_empty != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=strip_empty%>"
       });
       });
      </script>
     <%
      }
     %>
<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>

<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
<%
						String message = (String) request.getAttribute("Location.of.the.Pump/Motor");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
							<%
						message = (String) request.getAttribute("Power.(HP/KW)");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
							<%
						message = (String) request.getAttribute("Head.in.(meter)");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
							<%
						message = (String) request.getAttribute("Discharge.in(lps)");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					<%
						message = (String) request.getAttribute("Type.of.Pump.&.Motor");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					
					<%
						message = (String) request.getAttribute("Year.of.Installation");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					
					<%
						message = (String) request.getAttribute("Location.of.the.Pump/Motor.maxlength");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					
					<%
						message = (String) request.getAttribute("Power.(HP/KW).maxlength");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					
					<%
						message = (String) request.getAttribute("Head.in.(meter).maxlength");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					
					<%
						message = (String) request.getAttribute("Discharge.in(lps).maxlength");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					
					<%
						message = (String) request.getAttribute("Type.of.Pump.&.Motor.maxlength");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					
					<%
						message = (String) request.getAttribute("Year.of.Installation.maxlength");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.error({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					

 <%	
			String message11 = (String) request.getAttribute("ULBID.needed");

      if (message11 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message11%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message12 = (String) request.getAttribute("dateofreport.needed");

      if (message12 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message12%>"
       });
       });
      </script>
     <%
      }
     %>
    
   
<div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="list-physical-infra-pump-and-motor" class="edit-link">
					<i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit
		</a>
	</div>
</div>
<div class="col-md-12">
        	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-md-offset-2 col-lg-offset-2">
				<div class="x_panel">
					<div class="x_title text-center text-capitalize">
						<h2>
							Physical Infra Pump & Motor
						</h2>
						<h4>
							Figures for: <label id="show_date"><fmt:parseDate value="${physicalInfraPumpsAndMotorsModels.get(0).getDate_of_report()}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></label>
						</h4>
					</div>
					<div class="x_content">
						<br>
						<form id="demo-form2" method="POST" action="./add-physical-infra-pump-and-motor" onsubmit="return physicalInfraPumpMotor();">
						<input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
						<c:set var="id" value="${ulb_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
							<c:if test="${ulb_id != null }">
								<input type="hidden" name="enculbId" value="<%=id%>" />
								<input type="hidden" name="ulb_id" value="${ulb_id}" />
							</c:if>
							<div class="table-responsive">
								<table class="table table-striped jambo_table formTable" id="">
									 <thead>
							          <tr>
							            <th class="text-center">Sl. No.</th>
							            <th class="text-center">Location of the pump/motor</th>
							            <th class="text-center">Power (HP/KW)</th>
							            <th class="text-center">Head (in metre)</th>
							            <th class="text-center">Discharge (in Ips)</th>
							            <th class="text-center">Type of Pump &amp; Motor</th>
							            <th class="text-center">Year of Installation</th>
							            <th>&nbsp;</th>
							          </tr>
							        </thead>
									<tbody>
										<c:choose>
									<c:when test="${physicalInfraPumpsAndMotorsModels.size() > 0 }">
									<c:forEach var="physicalInfraPumpsAndMotorsModel" items="${physicalInfraPumpsAndMotorsModels }" varStatus="step">
									   <tr class="tblRow">
											<td align="center">${step.index +1 }</td>
											<td align="center"><input maxlength="100" name="list_loc_of_pum_motor"
												type="text" value="${physicalInfraPumpsAndMotorsModel.loc_of_pum_motor }" class="form-control form-control-sm only_text" /></td>
											<td align="center"><input maxlength="50" name="list_cap_of_motor"
												type="text" value="${physicalInfraPumpsAndMotorsModel.cap_of_motor }" class="form-control form-control-sm only_text" /></td>
											<td align="center"><input maxlength="50" name="list_head_dia"
												type="text" value="${physicalInfraPumpsAndMotorsModel.head_dia }" class="form-control form-control-sm only_text" /></td>
											<td align="center"><input maxlength="50" name="list_discharge" type="text" value="${physicalInfraPumpsAndMotorsModel.discharge }"
												class="form-control form-control-sm only_text" /></td>
												<td align="center"><input maxlength="50" name="list_type_pump"
												type="text" value="${physicalInfraPumpsAndMotorsModel.type_pump }" class="form-control form-control-sm only_text" /></td>
											<td align="center"><input maxlength="50" name="list_year_inst" type="text" value="${physicalInfraPumpsAndMotorsModel.year_inst }"
												class="form-control form-control-sm only_text" /></td>
											<td><input type="button" value="Delete"
												class="delRow btn btn-primary btn-sm" <c:if test="${loginUsersModel2.userLevelModel.user_level_id == 4 && addreq=='true' }">disabled</c:if> /></td>
										</tr>
									</c:forEach>
									</c:when>
									<c:otherwise>
									<tr class="tblRow">
											<td align="center">1</td>
											<td align="center"><input maxlength="100" name="list_loc_of_pum_motor"
												type="text" class="form-control form-control-sm only_text" /></td>
											<td align="center"><input maxlength="50" name="list_cap_of_motor"
												type="text" class="form-control form-control-sm only_text" /></td>
											<td align="center"><input maxlength="50" name="list_head_dia"
												type="text" class="form-control form-control-sm only_text" /></td>
											<td align="center"><input maxlength="50" name="list_discharge" type="text"
												class="form-control form-control-sm only_text" /></td>
												
												<td align="center"><input maxlength="50" name="list_type_pump"
												type="text" class="form-control form-control-sm only_text" /></td>
											<td align="center"><input maxlength="50" name="list_year_inst" type="text"
												class="form-control form-control-sm only_text" /></td>
												
											<td><input type="button" value="Delete"
												class="delRow btn btn-primary btn-sm" <c:if test="${loginUsersModel2.userLevelModel.user_level_id == 4 && addreq=='true' }">disabled</c:if> /></td>
										</tr>
									</c:otherwise>
									</c:choose>
									
										
										<tr>
											<td align="center" colspan="9"><input type="button"
												value="Add Row" class="addRow btn btn-primary btn-sm" <c:if test="${loginUsersModel2.userLevelModel.user_level_id == 4 && addreq=='true' }">disabled</c:if> /></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Next Maintenance Date:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12 date" id="dateIp">
									<input  name="dueDate" id="dueDate" class="form-control col-md-7 col-xs-12" type="text" readonly>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Save</button>
								<a href="list-physical-infra-pump-and-motor" class="btn btn-primary">Back</a>
							</div>
							<div class="form-control-wrap">
								<input class="form-control" id="date_of_report" placeholder="" value="${physicalInfraPumpsAndMotorsModels.size() > 0?physicalInfraPumpsAndMotorsModels.get(0).getDate_of_report():''}" readonly name="date_of_report" type="hidden">
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
<script>
      $(document).ready(function(){
      	
      	var wdth = $(window).width();
		var navwdth = wdth - 70;
		$('.top_nav.fixed_top_nav').css('width', navwdth+'px');
      	
      	$('.addRow').click(function(){
  		$('.formTable tr:last').before('<tr class="tblRow"><td align="center">'+ $('.formTable tbody').children().length +'</td><td align="center"><input type="text" maxlength="100" name="list_loc_of_pum_motor" class="form-control form-control-sm only_text"/></td><td align="center"><input type="text" maxlength="50" name="list_cap_of_motor" class="form-control form-control-sm only_text"/></td><td align="center"><input type="text" maxlength="50" name="list_head_dia" class="form-control form-control-sm only_text"/></td><td align="center"><input type="text" maxlength="50" name="list_discharge" class="form-control form-control-sm only_text"/></td><td align="center"><input type="text" maxlength="50" name="list_type_pump" class="form-control form-control-sm only_text"/></td><td align="center"><input type="text" maxlength="50" name="list_year_inst" class="form-control form-control-sm only_text"/></td><td><input type="button" value="Delete" class="delRow btn btn-primary btn-sm"/></td></tr>');
  		});
	  	$(document).on('click', '.delRow', function(){
	  		$(this).parent().parent().remove();
	  		$(".formTable").find("tbody").children(".tblRow").each(function(index){
				$(this).find("td:first").html(index+1);
			});
	  	});
	  	
	  	
	  	
	  	
	  	//  works for edit page only
	  	<c:if test="${ulb_id != null  && physicalInfraPumpsAndMotorsModels.size() > 0}">
	      //   hide all physical infra date calender
			  var formDateTime = $("#add_date").closest("div.form_datetime");
			  var parentOfformDateTime = formDateTime.parent();
			  formDateTime.remove();
			  parentOfformDateTime.html("<input type='hidden' id='add_date' value='${physicalInfraPumpsAndMotorsModels.get(0).getDate_of_report()}' />");
		  
	    </c:if>
      	
      });
      
</script>  
<%@ include file="footer.jsp"%>
<script
	src="<%=basePath%>resources/validation/physical-infra-pump&motors.js"></script>