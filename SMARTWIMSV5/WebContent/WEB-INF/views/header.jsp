<%-- <%@ page errorPage="page-500.jsp" %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
<style> html{display : none ; } </style>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	//System.out.println("BasePath : " + basePath);
%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="msapplication-TileImage" content="<%=basePath%>resources/images/favicon/ms-icon-144x144.png">
<meta name="theme-color">
<title>Dashboard</title>
<link rel="apple-touch-icon" sizes="57x57" href="<%=basePath%>resources/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<%=basePath%>resources/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<%=basePath%>resources/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath%>resources/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<%=basePath%>resources/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<%=basePath%>resources/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<%=basePath%>resources/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<%=basePath%>resources/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<%=basePath%>resources/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<%=basePath%>resources/images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<%=basePath%>resources/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath%>resources/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<%=basePath%>resources/images/favicon/favicon-16x16.png">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link href="<%=basePath%>resources/dist/loading.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<%=basePath%>resources/css/AdminLTE.min.css">
<link rel="stylesheet" href="<%=basePath%>resources/css/_all-skins.min.css">
<link href="<%=basePath%>resources/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
<link href="<%=basePath%>resources/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="<%=basePath%>resources/css/blue.css">
<link rel="stylesheet" href="<%=basePath%>resources/css/morris.css">
<link rel="stylesheet" href="<%=basePath%>resources/css/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="<%=basePath%>resources/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="<%=basePath%>resources/css/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet" href="<%=basePath%>resources/css/radio-btn.css">
<link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet"/>
<link href="<%=basePath%>resources/css/custome-responsive.css" type="text/css" rel="stylesheet"/>
<link href="<%=basePath%>resources/css/new-file.css" type="text/css" rel="stylesheet"/>
<link href="<%=basePath%>resources/css/daterangepicker.css" rel="stylesheet">
<script src="<%=basePath%>resources/js/chart/highcharts.js"></script>
<script src="<%=basePath%>resources/js/chart/exporting.js"></script>
<script src="<%=basePath%>resources/js/jquery-min.js" type="text/javascript"></script>
<script src="<%=basePath%>resources/dist/jquery.loading.min.js"></script>
<script src="<%=basePath%>resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script> 
<script src="<%=basePath%>resources/js/app.min.js" type="text/javascript"></script>
<script	src="<%=basePath%>resources/js/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>resources/vendors/growl/css/jquery.growl.css" />
<script type="text/javascript"
	src="<%=basePath%>resources/vendors/growl/js/jquery.growl.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
if( self == top ) {
document.documentElement.style.display = 'block' ; 
} 
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAuvUq57kTUj8br6zVRuWNbz20hqZxSeco&sensor=true&v=3.exp" async defer></script>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<script type="text/javascript">
	var startDate = '${startDate}';
	var dashboardDateObj = {
			singleDatePicker : true,
			startDate : dateFormatter(startDate),
			singleClasses : "picker_2",
			maxDate:dateFormatter(startDate),
			locale : {

				format : 'DD-MM-YYYY'
			}
		};
	
	$(document).ready(
			function() {

				//Menu
				$('.treeview-menu>li').click(
						function() {
							if ($(this).hasClass('active')) {
								$(this).removeClass('active');
								$(this).find('ul.treeview-menu').removeClass(
										'menu-open').slideUp('slow');
							}
						});

				
				
				
				$('#dashboard_date').daterangepicker(dashboardDateObj);
				
				if($('#dashboard_date').length != 0){
					var month = $('#dashboard_date').val().split("-")[1];
					var year = $('#dashboard_date').val().split("-")[2];
					$("[name='month'] option:nth-child(" + month + ")").prop(
							"selected", true);
					$("[name='selectedYear'] option[value='"+ year +"']").prop(
							"selected", true);
				}
				
				
			});
		/*Report Start and End Date starts Here*/
		$(document).ready(
			function() {
				$('#report_start_date').daterangepicker({
					singleDatePicker : true,
					startDate : dateFormatter(startDate),
					maxDate:dateFormatter(startDate),
					singleClasses : "picker_2",
					locale : {

						format : 'DD-MM-YYYY'
					},
				});
					
				$('#report_end_date').daterangepicker({
					singleDatePicker : true,
					startDate : dateFormatter(startDate),
					maxDate:dateFormatter(startDate),
					singleClasses : "picker_2",
					locale : {

						format : 'DD-MM-YYYY'
					}
				});
				
				/*Report Start and End Date ends Here*/
			});
	
	function dateFormatter(startDate){
		var dt = startDate.split('-');
		var formattedDt = dt[2]+'-'+dt[1]+'-'+dt[0];

		return formattedDt;

	}
	
	
	jQuery(document).ajaxStart(function () {
 		//show ajax indicator
		$.showLoading({name: 'jump-pulse',allowHide: false});  
	}).ajaxStop(function () {
	//hide ajax indicator
		setTimeout(function(){
			$.hideLoading({name: 'jump-pulse'});
		}, 200);
		
	});
	
	
	$( document ).ajaxSuccess(function( event, request, settings ) {
		 if(request.responseText.trim() == ""){
			 window.location = "./error500?AGSuccess=emtyresult";
		 }
	});
	
	$( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
		if(jqxhr.status == 404){
			 window.location = "./logout?invaliedsession=1";
		}
	});
	
	$(document).ready(function() {
		$(document).on('focus', ':input', function() {
			$(this).attr('autocomplete', 'off');
		});
	});
	
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper clearfix">
  <header class="main-header"> 
    <a href="dashboard" class="logo"><span class="logo-mini"><img class="img-responsive" src="<%=basePath%>resources/images/dash-logo.png"/></span>  </a>
    <nav class="navbar navbar-static-top"> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>
      <div class="title pull-left">
      	<h4>Smart Water Information Management System</h4>
        <h6>Department of Housing & Urban Development, Govt. of Odisha</h6>
      </div>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="tasks-menu">
          		<a href="javascript:void(0);" class="pull-right btn-amrut"><i class="fa fa-star"></i> AMRUT</a>
          </li>
          <li class="dropdown user user-menu"> 
          	<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
            <img src="<%=basePath%>resources/images/profile-icon.png" class="user-image" alt="User Image"> <span class="hidden-xs">${loginUsersModel2.full_Name}</span> </a>
            <ul class="dropdown-menu">
              <li class="user-footer">
                <a href="change-password" class="btn btn-default btn-flat"><span>Change password</span> <i class="fa fa-edit" aria-hidden="true"></i></a> 
               	<a href="logout" class="btn btn-default btn-flat"><span>log out</span> <i class="fa fa-sign-out" aria-hidden="true"></i></a> 
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <aside class="main-sidebar"> 
        <section class="sidebar"> 
          <ul class="sidebar-menu">
            <li class="active treeview"> 
                <a href="dashboard"> 
                    <img class="img-responsive" src="<%=basePath%>resources/images/menu1.png"/>
                    <span>DASHBOARD</span>
                </a>
            </li>
            <li class="treeview"> 
                <a href="javascript:void(0);" class="resetAll"> 
                    <img class="img-responsive" src="<%=basePath%>resources/images/menu2.png"/>
                    <span>All ULBs</span>
                </a>
            </li>
            <li class="treeview"> 
                <a href="javascript:void(0);" class="resetAll"> 
                    <img class="img-responsive" src="<%=basePath%>resources/images/menu3.png"/>
                    <span>Reset All</span>
                </a>
            </li>
            <li class="treeview">
              <a href="javascript:void(0);">
                <img class="img-responsive" src="<%=basePath%>resources/images/menu4.png"/>
                <span>Report</span>
              </a>
              <ul class="treeview-menu menu-open">
                <li class="">
                  <a href="javascript:void(0);"> Exception Report
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="sub_menu"><a href="exception-report">Entry Report</a></li>
								<li class="sub_menu"><a href="lpcd-report">LPCD</a></li>
								<li class="sub_menu"><a href="coverage-report">Water Supply Coverage</a></li>
								<li class="sub_menu"><a href="deficit-report">Deficit/Surplus</a></li>
								<li class="sub_menu"><a href="average-report">Average Hours of Water Supply</a></li>
								<li class="sub_menu"><a href="charges-report">Water Charges Collection</a></li>
								<li class="sub_menu"><a href="testing-report">Water Samples Found OK</a></li>
								<li class="sub_menu"><a href="complaints-report">Complaints Resolved</a></li>
                  </ul>
                </li>
                </ul>
            </li>
            <li class="treeview"> 
                <a href="javascript:void(0);"> 
                    <img class="img-responsive" src="<%=basePath%>resources/images/menu5.png"/>
                    <span>Data Entry</span>
                </a>
                <ul class="treeview-menu menu-open">
                <li class="">
                  <a href="javascript:void(0);"> Daily
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="sub_menu"><a href="add-daily-water-supply">Water
										Supply</a></li>
								<li class="sub_menu"><a href="add-daily-tanker">Tanker</a></li>
								<li class="sub_menu"><a href="add-daily-complaint">Complaint</a></li>
								<li class="sub_menu"><a href="add-daily-connection">Connection</a></li>
								<li class="sub_menu"><a href="add-daily-water-testing">Water
										Testing</a></li>
								<li class="sub_menu"><a href="add-daily-water-charge">Water
										Charge/Tax</a></li>
                  </ul>
                </li>
                 <li class="">
                  <a href="javascript:void(0);"> Physical Infra
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    	<li class="sub_menu"><a	href="add-physical-infra-standpost">Standposts</a></li>
							<li class="sub_menu"><a href="add-physical-infra-tubewell">Tubewells</a></li>
							<li class="sub_menu"><a href="add-physical-infra-connection">Connection</a></li>
							<li class="sub_menu"><a href="add-physical-infra-reservoirs">Reservoirs</a></li>
							<li class="sub_menu"><a href="add-physical-infra-pipe">Pipe Line</a></li>
							<li class="sub_menu"><a href="add-physical-infra-treatment-plant">Treatment Plant</a></li>
							<li class="sub_menu"><a href="add-physical-infra-generator">Generator</a></li>
							<li class="sub_menu"><a href="add-physical-infra-pump-and-motor">Pump & Motor</a></li>
							<li class="sub_menu"><a href="add-physical-infra-prod-wells">Production Well</a></li>
							<li class="sub_menu"><a href="add-physical-infra-electric-installation">Electrical Installation</a></li>
                  </ul>
                </li>
                 <li class="">
                  <a href="javascript:void(0);"> Set Target
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    	<li class="sub_menu"><a	href="add-target-water-charges-tax">Water Charge/Tax</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            
          </ul>
        </section>
        <!-- /.sidebar --> 
      </aside>