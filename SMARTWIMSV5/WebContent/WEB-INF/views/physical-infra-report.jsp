<%@ include file="header.jsp"%>
<%-- <%@ include file="dashboard-header.jsp"%> --%>
<div class="content-wrapper clearfix">
<!-- page content -->
   <div class="col-md-12">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="row top_tiles">
			<div class="col-xs-12 col-sm-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>View Physical Infrastructure Details</h2>
					</div>
					<div class="x_content table table-responsive">
						<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
								<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>No Of StandPosts</th>
									<th>No Of StandPosts in Working Conditions</th>
									<th>No Of Tube Wells</th>
									<th>No Of Tube Wells in Working Conditions</th>
									<th>No Of Ground Reservoirs</th>
									<th>No Of Elevated Reservoirs</th>
									<th>Capacity Of Ground Reservoir(in ML)</th>
									<th>Capacity Of Elevated Reservoir(in ML)</th>
									<th>Length of Rising Main (in Km)</th>
									<th>Length of Distribution Network (in Km)</th>
									<th>No Of Treatment Plants</th>
									<th>Capacity Of Treatment Plants (in MLD)</th>
									<th>No of Generators</th>
									<th>No of Generators in Working Conditions</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty physicalInfraModels}">
										<c:forEach items="${physicalInfraModels}" var="data" varStatus="loop">
										<c:set var="count" value="${count + 1}" scope="page" />
												<tr>
												    
														<td><c:out value="${count}" /></td>
														<td><c:out value="${data.ulbName}" /></td>
														<td><c:out value="${data.noOfStandPosts}" /></td>
														<td><c:out value="${data.noOfStandPostsInWorkingCondition}" /></td>
														<td><c:out value="${data.noOfTubeWells}" /></td>
														<td><c:out value="${data.noOfTubeWellsInWorkingCondition}" /></td>														
														<td><c:out value="${data.noOfGroundReservoirs}" /></td>
														<td><c:out value="${data.noOfElavatedReservoirs}" /></td>
														<td><c:out value="${data.capacityOfGroundReservoirs}" /></td>
														<td><c:out value="${data.capacityOfElavatedReservoirs}" /></td>
														<td><c:out value="${data.lengthOfRisingMain}" /></td>
														<td><c:out value="${data.lengthOfDistributionNetwork}" /></td>
														<td><c:out value="${data.noOfTreatmentPlant}" /></td>
														<td><c:out value="${data.capacityOfTreatmentPlant}" /></td>
														<td><c:out value="${data.noOfGenerators}" /></td>
														<td><c:out value="${data.noOfGeneratorsInWorkingCondition}" /></td>

													</tr>
												
											
										</c:forEach>
									</c:when>
									<c:otherwise>
										<td colspan="10" align="center">No Records Found</td>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<%@ include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		var handleDataTableButtons = function() {
			if ($("#datatable-buttons2").length) {
				$("#datatable-buttons2").DataTable({
					dom : "Bfrtip",
					paging : false,
					buttons : [ {
						extend : "copy",
						className : "btn-sm"
					}, {
						extend : "csv",
						className : "btn-sm"
					}, {
						extend : "excel",
						className : "btn-sm"
					}, {
						extend : "pdf",
						className : "btn-sm"
					}, {
						extend : "print",
						className : "btn-sm"
					}, ],
					responsive : true
				});
			}
		};

		TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons();
				}
			};
		}();

		$('#datatable').dataTable();

		
		$('#datatable-responsive').DataTable();



		var $datatable = $('#datatable-checkbox');

		
		$datatable.on('draw.dt', function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_flat-green'
			});
		});

		TableManageButtons.init();
	});
</script>
<!-- Datatables -->
<script src="<%=basePath%>resources/js/datatable/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<!-- Datatables -->
<script src="<%=basePath%>resources/js/innerPage/physical-infra-report.js"></script>