<%@ include file="header.jsp"%>

<%@ include file="entry-header.jsp"%>

<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>

	<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
     
	<%
						String message = (String) request.getAttribute("Success");

						if (message != null) {
					%>
					<script>
						 $(document).ready(function() {
						 jQuery.growl.notice({
								message : "<%=message%>"
							});
						 });
					 </script>
					<%
						}
					%>
					<%
						String message2 = (String) request.getAttribute("Failure");

						if (message2 != null) {
					%>
					<script>
				 		 $(document).ready(function() {
				 		 jQuery.growl.error({
				 				message : "<%=message2%>"
							});
						});
					</script>
					<%
						}
					%>
<div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="add-physical-infra-electric-installation" class="edit-link">
					<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;New
		</a>
	</div>
</div>
   <div class="col-md-12">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="row top_tiles">
			<div class="col-xs-12 col-sm-12">
				<div class="x_panel">
					<div class="x_title">
						<h2 class="custome-h2">List Physical Infra Electrical Installation</h2>
					</div>
					<div class="x_content table table-responsive">
						<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
							<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="ulbNameModel" items="${ulbNameModels }" varStatus="step">
									<tr>
									<c:set var="id" value="${ulbNameModel.ulb_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
										<td>${step.index + 1}</td>
										<td>${ulbNameModel.ulb_Name }</td>
										<td><a
											href="add-physical-infra-electric-installation?ulb_id=<%=id%>"><i
												class="fa fa-pencil"></i></a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<%@ include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		var handleDataTableButtons = function() {
			if ($("#datatable-buttons2").length) {
				$("#datatable-buttons2").DataTable({
					dom : "Bfrtip",
					paging : true,
					buttons : [ {
						extend : "copy",
						className : "btn-sm"
					}, {
						extend : "csv",
						className : "btn-sm"
					}, {
						extend : "excel",
						className : "btn-sm"
					}, {
						extend : "pdf",
						className : "btn-sm"
					}, {
						extend : "print",
						className : "btn-sm"
					}, ],
					responsive : true
				});
			}
		};

		TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons();
				}
			};
		}();

		$('#datatable').dataTable();

		
		$('#datatable-responsive').DataTable();



		var $datatable = $('#datatable-checkbox');

		
		$datatable.on('draw.dt', function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_flat-green'
			});
		});

		TableManageButtons.init();
	});
</script>


<!-- Datatables -->
<script src="<%=basePath%>resources/js/datatable/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<!-- Datatables -->

