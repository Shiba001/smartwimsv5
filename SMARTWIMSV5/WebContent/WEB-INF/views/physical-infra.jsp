<%@ include file="header.jsp"%>

<%-- <%@ include file="dashboard-header.jsp"%> --%>
<div class="content-wrapper clearfix">
	<!-- page content -->

	<div class="col-md-12 card-bottom phy-infra-page">
		<h2 class="phy-hdr">Physical Infra</h2>
		<div class="row-height">
			<div class="col-xs-12 col-sm-6 col-md-3 col-md-3 col-height">
				<div class="bottom-card">
					<h3>Physical Infrastructure</h3>
					<a href="physical-infra-report" class="btn btn-default">Show
						Details</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-md-3 col-height">
				<div class="bottom-card">
					<h3>Electrical Installation</h3>
					<a href="electrical-installation-report" class="btn btn-default">Show
						Details</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-md-3 col-height">
				<div class="bottom-card">
					<h3>Pumps & Motors</h3>
					<a href="pump-motors-report" class="btn btn-default">Show
						Details</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-md-3 col-height">
				<div class="bottom-card">
					<h3>Production Wells</h3>
					<a href="prod-wells-report" class="btn btn-default">Show
						Details</a>
				</div>
			</div>
		</div>
	</div>

	<!-- /page content -->
	<%@ include file="footer.jsp"%>