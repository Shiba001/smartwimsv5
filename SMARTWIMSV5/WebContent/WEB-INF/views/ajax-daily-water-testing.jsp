<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
							<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>Date (dd/MM/yyyy)</th>
									<th>No. of Bacteriological (H2S, Vial / Strip) Samples
										Tested</th>
									<th>No. of Bacteriological (H2S, Vial / Strip) Samples
										Found OK</th>
									<th>No. of FRC Samples Tested</th>
									<th>No. of FRC Samples Found OK</th>
									<th>Remarks</th>
									<th>Approval Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty dailyWaterTestingModels}">
										<c:forEach items="${dailyWaterTestingModels}" var="data">
											<c:set var="count" value="${count + 1}" scope="page" />
											<tr>
											<c:set var="id" value="${data.wt_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
											
												<td><c:out value="${count}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td><fmt:parseDate value="${data.date_of_report}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></td>
												<td>${fn:substringBefore(data.no_bact_samp_tested, '.')}</td>
												<td>${fn:substringBefore(data.no_bact_samp_found_ok, '.')}</td>
												<td>${fn:substringBefore(data.no_other_samp_tested, '.')}</td>
												<td>${fn:substringBefore(data.no_other_samp_found_ok, '.')}</td>
												<td>${data.remarks}</td>
												<c:choose>
													<c:when
														test='${data.isApproved == "0" and loginUsersModel2.getUserLevelModel().getUser_level_id() == "2"}'>
														<td><input type="checkbox" class="approval"
															data-url="./isApproved-daily-water-testing?wt_id=<%=id%>"><font
															color="red">Pending</font></td>
														<td><a
															href="edit-daily-water-testing?wt_id=<%=id%>"><i
																class="fa fa-pencil"></i></a> 
															<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
															<i class="fa fa-trash-o delete_pointer"></i>
															</a>
														</td>

													</c:when>
													<c:when
														test='${data.isApproved == "0" and loginUsersModel2.getUserLevelModel().getUser_level_id() != "2"}'>
														<td><input type="checkbox" disabled><font
															color="red">Pending</font></td>
														<td><a
															href="edit-daily-water-testing?wt_id=<%=id%>"><i
																class="fa fa-pencil"></i></a>
															<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
															<i class="fa fa-trash-o delete_pointer"></i>
															</a>
														</td>

													</c:when>
													<c:when
														test='${data.isApproved == "1" and loginUsersModel2.getUserLevelModel().getUser_level_id() == "2"}'>
														<td><input type="checkbox" class="approval"
															data-url="./isNotApproved-daily-water-testing?wt_id=<%=id%>" checked><font
															color="green">Approved</font></td>
														<td><a href="javascript:void(0)"><i
																class="fa fa-pencil"></i></a> <a href="javascript:void(0)"><i
																class="fa fa-trash-o"></i></a></td>

													</c:when>
													<c:otherwise>
														<td><input type="checkbox" checked disabled>Approved</td>
														<td><a href="javascript:void(0)"><i
																class="fa fa-pencil"></i></a> <a href="javascript:void(0)"><i
																class="fa fa-trash-o"></i></a></td>
													</c:otherwise>
												</c:choose>


											</tr>
										</c:forEach>
									</c:when>
									<%-- <c:otherwise>
										<td colspan="10" align="center">No Records Found</td>
									</c:otherwise> --%>
								</c:choose>
							</tbody>
						</table>