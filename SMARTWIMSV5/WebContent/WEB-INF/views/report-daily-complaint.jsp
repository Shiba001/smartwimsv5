<%@ include file="header.jsp"%>
<%@ include file="dashboard-header.jsp"%>
<%@ page import="com.orissa.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.Map.*"%>
<%@ page import="com.orissa.model.*"%>
<%@ page import="java.util.Date,java.text.SimpleDateFormat,java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<% List<String> datesBetween = (List<String>) request.getAttribute("datesBetween"); %>
   <div class="col-md-12">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="row top_tiles">
			<div class="col-xs-12 col-sm-12">
				<div class="x_panel">
					<div class="x_title">
						<h2 class="custome-h2">List of ULBs provided / defaulted input data of Complaints</h2>
					</div>
					<div class="x_content table data-tab">
					<table  id="datatable-buttons2"	class="table table-striped jambo_table">
							
							<thead>
								<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<% for(int day = 0;day<datesBetween.size();day++){ %>
									<% 
								     String dateStr = datesBetween.get(day);
								     SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
								     Date result = formater.parse(dateStr);
								     SimpleDateFormat newFormater = new SimpleDateFormat("dd MMM");
								%>
									<th><%= newFormater.format(result) %></th>
									<% } %>
									
								</tr>
							</thead>
								<tbody>
								<%
									Map<String, List<String>> outerComplaintsMap = (Map<String, List<String>>) request.getAttribute("dataEntryComplaintsReport");
											if (outerComplaintsMap != null) {
											//System.out.println("outerComplaintsMap" + outerComplaintsMap);
											Map<String, List<String>> innerMapValues = null;
											List<String> innerList = null;											
											int i = 1;
											for (Entry<String, List<String>> outerEntry : outerComplaintsMap.entrySet()) { %>
												<tr>
													<td><%=i %></td>
													<td><%=outerEntry.getKey() %></td>													
													
													<%	innerList = outerEntry.getValue();
														
														if (innerList != null & innerList.size() > 0) {
															
															/* System.out.println("innerList.size() --->>>>"
																	+ innerList.size()); */
															for (int j = 0;j < innerList.size(); j++) { 
															%>
															<td><% if(innerList.get(j).equals("Y")) { %><i class="fa fa-check-circle" aria-hidden="true" style="color:green;"></i><% } else { %><i class="fa fa-times-circle" aria-hidden="true" style="color:red;"></i><% } %></td>
															
															<% } }  %>
												</tr>
										<%	i++; }
											
											
								}%>
								</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<%@ include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		var handleDataTableButtons = function() {
			if ($("#datatable-buttons2").length) {
		/* 		$("#datatable-buttons2").DataTable({
					dom : "Bfrtip",
					paging : false,
					buttons : [ {
						extend : "copy",
						className : "btn-sm"
					}, {
						extend : "csv",
						className : "btn-sm"
					}, {
						extend : "excel",
						className : "btn-sm"
					}, {
						extend : "pdf",
						className : "btn-sm"
					}, {
						extend : "print",
						className : "btn-sm"
					}, ],
					responsive : true
				}); */
			}
		};

		TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons();
				}
			};
		}();

		$('#datatable').dataTable();

		$('#datatable-responsive').DataTable();

		var $datatable = $('#datatable-checkbox');

		$datatable.on('draw.dt', function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_flat-green'
			});
		});

		TableManageButtons.init();
	});
</script>
<style>.table { overflow-x: scroll; }</style>

<script src="<%=basePath%>resources/js/innerPage/ajax-complaint-report.js"></script>

<!-- Datatables -->
<script src="<%=basePath%>resources/js/datatable/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<!-- Datatables -->

