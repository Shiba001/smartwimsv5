<%@ include file="header.jsp"%>
<%@ include file="entry-header.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%	
		String strip_empty = (String) request.getAttribute("strip.empty");

      if (strip_empty != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=strip_empty%>"
       });
       });
      </script>
     <%
      }
     %>

<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
     
<%
      String message = (String) request.getAttribute("total.no.tubewells");

      if (message != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message%>"
       });
       });
      </script>
     <%
      }
     %>
<%
      String message1 = (String) request.getAttribute("total.no.tubewells.in.working.condition");

      if (message1 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message1%>"
       });
       });
      </script>
     <%
      }
     %> 
 
<%
      String message2 = (String) request.getAttribute("total.no.tubewells.maxlength");

      if (message2 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message2%>"
       });
       });
      </script>
     <%
      }
     %>  
     
     <%
      String message3 = (String) request.getAttribute("total.no.tubewells.in.working.condition.maxlength");

      if (message3 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message3%>"
       });
       });
      </script>
     <%
      }
     %>       
      
       <%	
			String message11 = (String) request.getAttribute("ULBID.needed");

      if (message11 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message11%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message12 = (String) request.getAttribute("dateofreport.needed");

      if (message12 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message12%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message13 = (String) request.getAttribute("duedate.needed");

      if (message13 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message13%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
		String dueDate = (String) request.getAttribute("duedate.greater");

      if (dueDate != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=dueDate%>"
       });
       });
      </script>
     <%
      }
     %>
     
   <div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="list-physical-infra-tubewell" class="edit-link">
					<i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit
		</a>
	</div>
</div>
 <div class="col-md-12">
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3">
				<div class="x_panel">
					<div class="x_title text-center text-capitalize">
						<h2>
							Physical Infra Tubewells
						</h2>
						<h4>
							Figures for: <label id="show_date"><fmt:parseDate value="${startDate}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></label>
						</h4>
					</div>
					<div class="x_content">
						<br>


						<form action="add-physical-infra-tubewell" method="POST" onsubmit="return physicalInfraTubewells();" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
							<input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
							<div class="form-field">
								<div class="form-control-wrap">
									<input class="form-control" id="date_of_report" placeholder="" value="" readonly="" name="date_of_report" type="hidden">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">Total No. of Tubewells:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="tot_no_tw" id="tot_no_tw" value="${fn:substringBefore(physicalInfraTubewellsModel.tot_no_tw, '.')}"  class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Total No. of
													Tubewells in Working Condition:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="tot_no_tw_wc" id="tot_no_tw_wc" value="${fn:substringBefore(physicalInfraTubewellsModel.tot_no_tw_wc, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Next Maintenance Date:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12 date" id="dateIp">
									<input  name="dueDate" id="dueDate" class="form-control col-md-7 col-xs-12" type="text" readonly>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Save</button>
								<button type="reset" class="btn btn-primary">Clear</button>
							</div>

						</form>

					</div>
				</div>
			</div>
        </div>
<%@ include file="footer.jsp"%>
<script src="<%=basePath%>resources/validation/physical-infra-tubewell.js"></script>