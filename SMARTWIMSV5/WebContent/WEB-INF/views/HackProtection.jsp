<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Hackers Are Not Allowed</title>
<style type="text/css">
/* html {
	background-color: gray;
}

body {
	padding: 20px;
}

#arrow-left {
	position: relative;
	padding: 30px;
}

#arrow-left:before {
	content: '';
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
	width: 40%;
	background: blue;
	-webkit-transform: skew(135deg, 0deg);
	-moz-transform: skew(135deg, 0deg);
	-ms-transform: skew(135deg, 0deg);
	-o-transform: skew(135deg, 0deg);
	transform: skew(135deg, 0deg);
}

#arrow-left:after {
	content: '';
	position: absolute;
	top: 100%;
	right: 60%;
	height: 100%;
	width: 40%;
	background: blue;
	-webkit-transform: skew(-135deg, 0deg);
	-moz-transform: skew(-135deg, 0deg);
	-ms-transform: skew(-135deg, 0deg);
	-o-transform: skew(-135deg, 0deg);
	transform: skew(-135deg, 0deg);
}

#arrow-right {
	position: relative;
	padding: 30px;
}

#arrow-right:before {
	content: '';
	position: absolute;
	top: 0;
	left: 0px;
	height: 100%;
	width: 40%;
	background: red;
	-webkit-transform: skew(45deg, 0deg);
	-moz-transform: skew(45deg, 0deg);
	-ms-transform: skew(45deg, 0deg);
	-o-transform: skew(45deg, 0deg);
	transform: skew(45deg, 0deg);
}

#arrow-right:after {
	content: '';
	position: absolute;
	top: 100%;
	right: 60%;
	height: 100%;
	width: 40%;
	background: red;
	-webkit-transform: skew(-45deg, 0deg);
	-moz-transform: skew(-45deg, 0deg);
	-ms-transform: skew(-45deg, 0deg);
	-o-transform: skew(-45deg, 0deg);
	transform: skew(-45deg, 0deg);
} */

/*----------------------------ERROR PAGE----------------------------------*/
.error-page {
	background-color: #DB291C;
}

.error-content {
	height: 100%;
	left: 0;
	position: absolute;
	width: 100%;
}

.error-main {
	color: #fff;
	left: 50%;
	position: absolute;
	top: 50%;
	transform: translateX(-50%) translateY(-50%);
	width: 100%;
}

.error-main i {
	font-size: 700%;
	color: #FBD734;
}

.error-main h1 {
	font-size: 900%;
	margin: 0;
	color: #FBD734;
}

.error-main p {
	font-size: 200%;
	margin: 0;
}

.error-main span {
	display: block;
	font-size: 17px;
}

.error-main a {
	border: 1px solid #FBD734;
	padding: 10px 20px;
	display: inline-block;
	margin-top: 20px;
	font-size: 110%;
	text-transform: capitalize;
	text-decoration: none;
	border-radius: 3px;
}

.error-main a {
	display: inline-block;
	text-shadow: none;
	color: #FFF;
	position: relative;
	transform: perspective(1px) translateZ(0px);
	transition-duration: 0.3s;
	transition-property: color;
	vertical-align: middle;
}

.error-main a::before {
	background: #FBD734 none repeat scroll 0 0;
	bottom: 0;
	content: "";
	left: 0;
	position: absolute;
	right: 0;
	top: 0;
	transform: scale(0);
	transition-duration: 0.3s;
	transition-property: transform;
	transition-timing-function: ease-out;
	z-index: -1;
}

.error-main a:hover, .error-main a:focus, .error-main a:active {
	color: #666;
}

.error-main a:hover::before, .error-main a:focus::before, .error-main a:active::before
	{
	transform: scale(1);
}

.container {
    max-width: 1200px;
    width: 100%;
}
body {
    font-family: 'Roboto', sans-serif !important;
}
.text-center {
    text-align: center;
}
.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}
@media (min-width: 1200px)
.container {
    width: 1170px;
}
@media (min-width: 992px)
.container {
    width: 970px;
}

@media (min-width: 768px)
.container {
    width: 750px;
}
/*----------------------------ERROR PAGE----------------------------------*/
​
</style>
</head>
<body class="error-page">
	<div class="container">
		<div class="error-content text-center">
			<div class="error-main">
				<i class="fa fa-warning" aria-hidden="true"></i>
				<h1>Hacker Attack</h1>
                 <p>You Are Not Authorize To Do This</p>
                <span>Your
				connection is not private Attackers might be trying to steal your
				information from This Website. NET::ERR_CERT_AUTHORITY_INVALID</span>
               <a href="index">Go To Login Page</a>
				
			</div>
		</div>
	</div>

</body>
</html>