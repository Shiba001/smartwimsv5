<%
	String path3 = request.getContextPath();
	String basePath3 = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path3 + "/";
	//System.out.println("BasePath : " + basePath);
%>
<style>
.highcharts-grid-line, .highcharts-credits {
	display: none;
}
</style>
<%@ include file="header.jsp"%>

<%@ include file="dashboard-header.jsp"%>


	<div class="col-md-12">
	  <ul class="nav nav-tabs custome-tab">
	    <li class="active"><a data-toggle="tab" href="#bacteriological">Bacteriological</a></li>
	    <li><a data-toggle="tab" href="#frc">FRC</a></li>
	  </ul>
	</div>
  <div class="tab-content">
    <div id="bacteriological" class="tab-pane fade in active">
		<div class="col-md-12 card-sec">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="card card-inner">
					<h5><label class="area">Odisha</label> Average: <label id="average">${bacteriologicalBoxValue }</label> %</h5>
					<div class="card-map">
						<div id="map-canvas_bact" style="height: 529px; border: 1px solid #ccc;"></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 barContainer"  style="">
				<div class="card card-inner">
					<h5>Water Testing: Top <label class="topValue">${topValue }</label> ULBs</h5>
					<div class="card-map">
						<div id="top10LPCD_bact" class="LPCD-chart"></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 barContainer" style="">
				<div class="card card-inner">
					<h5>Water Testing: Bottom <label class="topValue">${topValue }</label> ULBs</h5>
					<div class="card-map">
						<div id="bottom10LPCD_bact" class="LPCD-chart"></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 progressBr" style=""> 
				<div class="card-box">
				<h5>Progress Report</h5>
					<div class="card-overflow">
						<div class="progressChart" style="width: 800px;height: 515px;margin: 0 auto !important;"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 card-sec">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="card card-map card-map-legend">
					<div class="pull-left">
						<h5>Bacteriological &gt;= 100</h5>
						<img class="img-responsive"
							src="<%=basePath%>resources/images/map-green.png" />
					</div>
					<div class="pull-right">
						<h5>Bacteriological &lt; 100</h5>
						<img class="img-responsive"
							src="<%=basePath%>resources/images/map-red.png" />
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="card-map backnone bordernone card-panel">
					<div class="pull-left">
						<button type="button" class="btn btn-default btn-black progressBtn"
							data-toggle="modal" data-target="#myModal_progressbar_bact">Progress Report</button>
					</div>
					<div class="pull-left">
						<button type="button" class="btn btn-default btn-black allUlbBtn"
							data-toggle="modal" data-target="#myModal_bact">All ULBs</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="frc" class="tab-pane fade in">
		<div class="col-md-12 card-sec">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="card card-inner">
					<h5><label class="area">Odisha</label> Average: <label id="average">${frcBoxValue }</label> %</h5>
					<div class="card-map">
						<div id="map-canvas_frc" style="height: 529px; border: 1px solid #ccc;"></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 barContainer"  style="">
				<div class="card card-inner">
					<h5>Water Testing: Top <label class="topValue">${topValue }</label> ULBs</h5>
					<div class="card-map">
						<div id="top10LPCD_frc" class="LPCD-chart"></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 barContainer" style="">
				<div class="card card-inner">
					<h5>Water Testing: Bottom <label class="topValue">${topValue }</label> ULBs</h5>
					<div class="card-map">
						<div id="bottom10LPCD_frc" class="LPCD-chart"></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 progressBr" style=""> 
				<div class="card-box">
				   <h5>Progress Report</h5>
					<div class="card-overflow">
						<div class="progressChart" style="width: 800px;height: 515px;margin: 0 auto !important;"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 card-sec">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="card card-map card-map-legend">
					<div class="pull-left">
						<h5>FRC &gt;= 100</h5>
						<img class="img-responsive"
							src="<%=basePath%>resources/images/map-green.png" />
					</div>
					<div class="pull-right">
						<h5>FRC &lt; 100</h5>
						<img class="img-responsive"
							src="<%=basePath%>resources/images/map-red.png" />
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="card-map backnone bordernone card-panel">
					<div class="pull-left">
						<button type="button" class="btn btn-default btn-black progressBtn"
							data-toggle="modal" data-target="#myModal_progressbar_frc">Progress Report</button>
					</div>
					<div class="pull-left">
						<button type="button" class="btn btn-default btn-black allUlbBtn"
							data-toggle="modal" data-target="#myModal_frc">All ULBs</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<!-----------bacteriological section-------------->
<div id="myModal_bact" class="modal fade" role="dialog">
	<div class="modal-dialog modal-fullscreen">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">All ULB</h4>
			</div>
			<div class="modal-body modal-overflow">
				<div id="allLPCD_bact" class="LPCD-chart" style="min-width: 100%; height: 400px;margin: 0 auto !important;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div id="myModal_progressbar_bact" class="modal fade" role="dialog">
	<div class="modal-dialog modal-fullscreen">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Progress Report Of Water Testing</h4>
			</div>
			<div class="modal-body modal-overflow">
				<div  class="progressChart" id="container_bact" style="min-width: 100%; height: 400px;margin: 0 auto !important;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
<!-----------frc section-------------->
<div id="myModal_frc" class="modal fade" role="dialog">
	<div class="modal-dialog modal-fullscreen">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">All ULB</h4>
			</div>
			<div class="modal-body modal-overflow">
				<div id="allLPCD_frc" class="LPCD-chart" style="min-width: 100%; height: 400px; margin: 0 auto !important;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div id="myModal_progressbar_frc" class="modal fade" role="dialog">
	<div class="modal-dialog modal-fullscreen">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Progress Report Of Water Testing</h4>
			</div>
			<div class="modal-body modal-overflow">
				<div  class="progressChart" id="container_frc" style="min-width: 100%; height: 400px;margin: 0 auto !important;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<%@ include file="footer.jsp"%>

<script type="text/javascript">
	var lpcdMap = JSON.parse('${lpcdMap}');
	var top10ulb = JSON.parse('${top10ulb}');
	var top10lpcd = JSON.parse('${top10lpcd}');
	var bottom10ulb = JSON.parse('${bottom10ulb}');
	var bottom10lpcd = JSON.parse('${bottom10lpcd}');
	var datesBetween = JSON.parse('${datesBetween}');
	var currentLPCD = JSON.parse('${currentLPCD}');
	var previousLPCD = JSON.parse('${previousLPCD}');
	
	var top10ulb1 = JSON.parse('${top10ulb1}');
	var top10lpcd1 = JSON.parse('${top10lpcd1}');
	var bottom10ulb1 = JSON.parse('${bottom10ulb1}');
	var bottom10lpcd1 = JSON.parse('${bottom10lpcd1}');
	var currentLPCD1 = JSON.parse('${currentLPCD1}');
	var previousLPCD1 = JSON.parse('${previousLPCD1}');

	var topulb = JSON.parse('${topulb}');
	var toplpcd = JSON.parse('${toplpcd}');
	var bottomulb = JSON.parse('${bottomulb}');
	var bottomlpcd = JSON.parse('${bottomlpcd}');
	
	var topulb1 = JSON.parse('${topulb1}');
	var toplpcd1 = JSON.parse('${toplpcd1}');
	var bottomulb1 = JSON.parse('${bottomulb1}');
	var bottomlpcd1 = JSON.parse('${bottomlpcd1}');
	var flag = '${flag}';
	var flag1 = '${flag1}';
</script>
<script type="text/javascript"
	src="<%=basePath%>resources/js/innerPage/water-testing.js"></script>