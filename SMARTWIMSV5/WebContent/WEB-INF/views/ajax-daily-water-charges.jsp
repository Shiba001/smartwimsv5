<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
							<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>Date (dd/MM/yyyy)</th>
									<th>Total Water Charges Collection (in Rs.)</th>
									<th>Remarks</th>
									<th>Approval Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty dailyWaterChargesModels}">
										<c:forEach items="${dailyWaterChargesModels}" var="data"
											varStatus="loop">
											<tr>
											<c:set var="id" value="${data.w_tax_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
												<td><c:out value="${loop.index + 1}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td><fmt:parseDate value="${data.date_of_report}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></td>
												<td>${data.tot_cur_coll}</td>
												<td>${data.remarks}</td>
												<c:choose>
													<c:when
														test='${data.isApproved == "0" and loginUsersModel2.getUserLevelModel().getUser_level_id() == "2"}'>
														<td><input type="checkbox" class="approval"
															data-url="./isApproved-daily-water-charge?W_tax_id=<%=id%>"><font
															color="red">Pending</font></td>
														<td><a
															href="edit-daily-water-charge?W_tax_id=<%=id%>"><i
																class="fa fa-pencil"></i></a>
															<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
															<i class="fa fa-trash-o delete_pointer"></i>
															</a>
														</td>

													</c:when>
													<c:when
														test='${data.isApproved == "0" and loginUsersModel2.getUserLevelModel().getUser_level_id() != "2"}'>
														<td><input type="checkbox" disabled><font
															color="red">Pending</font></td>
														<td><a
															href="edit-daily-water-charge?W_tax_id=<%=id%>"><i
																class="fa fa-pencil"></i></a> 
															<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
															<i class="fa fa-trash-o delete_pointer"></i>
															</a>
														</td>

													</c:when>
													<c:when
														test='${data.isApproved == "1" and loginUsersModel2.getUserLevelModel().getUser_level_id() == "2"}'>
														<td><input type="checkbox" class="approval"
															data-url="./isNotApproved-daily-water-charge?W_tax_id=<%=id%>" checked><font
															color="green">Approved</font></td>
														<td><a href="javascript:void(0)"><i
																class="fa fa-pencil"></i></a> <a href="javascript:void(0)"><i
																class="fa fa-trash-o"></i></a></td>

													</c:when>
													<c:otherwise>
														<td><input type="checkbox" checked disabled>Approved</td>
														<td><a href="javascript:void(0)"><i
																class="fa fa-pencil"></i></a> <a href="javascript:void(0)"><i
																class="fa fa-trash-o"></i></a></td>
													</c:otherwise>
												</c:choose>

											</tr>
										</c:forEach>
									</c:when>
									<%-- <c:otherwise>
										<td colspan="5" align="center">No Records Found</td>
									</c:otherwise> --%>
								</c:choose>
							</tbody>
						</table>