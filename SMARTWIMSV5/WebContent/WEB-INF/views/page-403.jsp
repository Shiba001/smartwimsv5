<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<title>Error</title>
<link href="<%=basePath%>resources/css/custome-responsive.css" type="text/css" rel="stylesheet"/>
<link href="<%=basePath%>resources/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
<link href="<%=basePath%>resources/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"> 
<link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet"/>
</head>
<body class="error-page">
    <div class="container">
        <div class="error-content text-center">
        	<div class="error-main">
                <i class="fa fa-warning" aria-hidden="true"></i>
                <h1>403</h1>
                <p>Access denied</p>
                <span>Full authentication is required to access this resource.</span>
                <a href="index">Go To Login Page</a>
            </div>
        </div>
    </div>
   
</body>
</html>
