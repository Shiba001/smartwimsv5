<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.orissa.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.Map.*"%>
<%@ page import="com.orissa.model.*"%>
<%@ page import="java.util.Date,java.text.SimpleDateFormat,java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<% List<String> datesBetween = (List<String>) request.getAttribute("datesBetween"); %>
<% SimpleDateFormat dateFormatWithTime = new SimpleDateFormat("MM/dd/yyyy hh:mm");%>
						<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
								<tr>
									<th>Urban Local Body</th>
									<% 
									for(int day = 0;day<datesBetween.size();day++){ %>
									<% 
								     String dateStr = datesBetween.get(day);
								     SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
								     Date result = formater.parse(dateStr);
								     SimpleDateFormat newFormater = new SimpleDateFormat("dd MMM yyyy");
								%>
									<th style="text-align: right;"><%= newFormater.format(result) %></th>
									<th></th>
									<% } %>
								</tr>
								
							</thead>
							<tbody>
								<tr>
									<td>&nbsp;</td>
									<% 
									for(int day = 0;day<datesBetween.size();day++){ %>
									<td style="text-align: center;">Bacteriological Water Samples Found OK</td>
									<td style="text-align: center;">FRC Water Samples Found OK</td>
									<% } %>
								</tr>
								<c:choose>
									<c:when test="${not empty ultimateMap}">
										<c:forEach items="${ultimateMap}" var="type">
											<tr>
												<td><c:out value="${type.key}" /></td>
												<c:forEach items="${type.value}" var="data">
													<c:choose>
														 	<c:when test="${data.get(0) >= 100 }">															
																<td style="color:#3c8a2e;text-align: center;">${data.get(0)}</td>
															</c:when>
															<c:otherwise>
																<td  style="color:#F00000;text-align: center;">${data.get(0)}</td>
															</c:otherwise>
													</c:choose>
													<c:choose>
															<c:when test="${data.get(1) >= 100 }">															
																<td style="color:#3c8a2e;text-align: center;">${data.get(1)}</td>
															</c:when>
															<c:otherwise>
																<td  style="color:#F00000;text-align: center;">${data.get(1)}</td>
															</c:otherwise>
													</c:choose>
												</c:forEach>
											</tr>
										</c:forEach>
										
											
									</c:when>
								</c:choose>
							</tbody>
						</table>
					