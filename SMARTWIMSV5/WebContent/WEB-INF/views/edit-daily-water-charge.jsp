<%@ include file="header.jsp"%>
 <c:set var="date_of_report" value="${dailyWaterChargeModel.getDate_of_report()}"/>
<%@ include file="entry-header.jsp"%> 

<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
	<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
     
	<%
      String message = (String) request.getAttribute("Total.Water.Charges.Collection");

      if (message != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message4 = (String) request.getAttribute("data.entry.exist");

      if (message4 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message4%>"
       });
       });
      </script>
     <%
      }
     %>
     
      <%	
		String message5 = (String) request.getAttribute("Total.Water.Charges.Collection.maxlength");

      if (message5 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message5%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
		String message6 = (String) request.getAttribute("remarks.maxlength");

      if (message6 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message6%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message12 = (String) request.getAttribute("ULBID.needed");

      if (message12 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message12%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message13 = (String) request.getAttribute("dateofreport.needed");

      if (message13 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message13%>"
       });
       });
      </script>
     <%
      }
     %>
   <div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="list-daily-water-charge" class="edit-link">
					<i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit
		</a>
	</div>
</div>
     <div class="col-md-12">
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3">
				<div class="x_panel">
					<div class="x_title text-center text-capitalize">
						<h2>
							Water Charge
						</h2>
						<h4>
							Figures for: <label id="show_date"><fmt:parseDate value="${dailyWaterChargeModel.date_of_report}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></label>
						</h4>
					</div>
					<div class="x_content">
						<br>

						<form action="update-daily-water-charge" method="post" onsubmit="return dailyWaterCharges();" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate>
							<input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
							<c:set var="id" value="${dailyWaterChargeModel.w_tax_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
							<input type="hidden" name="w_taxId"
								value="<%=id%>">
							<div class="form-field">
								<div class="form-control-wrap">
									<input class="form-control" id="date_of_report" placeholder="" value="" readonly name="date_of_report" type="hidden">

								</div>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Total Water Charges Collection (In
													Rs.):<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="tot_cur_coll" id="tot_cur_coll" value="${dailyWaterChargeModel.tot_cur_coll}" class="form-control col-md-7 col-xs-12 decimal_number" type="text">

								</div>
							</div>

							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Remarks : </label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<textarea class="form-control" rows="5" name="remarks" maxlength="200">${dailyWaterChargeModel.remarks}</textarea>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Update</button>
								<a href="list-daily-water-charge" class="btn btn-primary">Back</a>
							</div>

						</form>
					</div>
				</div>
			</div>
        </div>
<!-- /page content -->
<%@ include file="footer.jsp"%>
<script src="<%=basePath%>resources/validation/daily-water-charges.js"></script>