
<%
	String path3 = request.getContextPath();
	String basePath3 = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path3 + "/";
	//System.out.println("BasePath : " + basePath);
%>
<style>
.highcharts-grid-line, .highcharts-credits {
	display: none;
}
</style>
<%@ include file="header.jsp"%>

<%@ include file="dashboard-header.jsp"%>
<!-- page content -->
<div class="col-md-12 card-sec">
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		<div class="card card-inner">
			<div class="card-map">
				<div id="map-canvas" style="height: 516px; border: 1px solid #ccc;"></div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		<div class="card card-inner card-h-a">
			<h5 class="data-card-hdr"> Data approval status as on '<label class="dateFetch"></label>' for all ULBs</h5>
			<table class="" width="100%" style="font-size: 14px;">
				<tr>
					<td>Approved:</td>
					<td style="width: 50px; font-size: 36px; color: #28B779;"><i
						class="fa fa-thumbs-up" aria-hidden="true"></i></td>
					<td
						style="font-size: 14px; font-weight: bold; text-align: center;">
						<a href="javascript:void(0);" class="approved">${approved }</a>
					</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td>Pending:</td>
					<td style="width: 50px; font-size: 36px; color: #F74D4D;"><i
						class="fa fa-thumbs-down" aria-hidden="true"></i></td>
					<td
						style="font-size: 14px; font-weight: bold; text-align: center;">
						<a href="javascript:void(0);" class="pending">${notApproved }</a>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="col-md-12 card-sec">
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		<div class="card card-map card-map-legend">
			<div class="pull-left">
				<h5>Approved</h5>
				<img class="img-responsive"
					src="<%=basePath%>resources/images/map-green.png" />
			</div>
			<div class="pull-right">
				<h5>Pending</h5>
				<img class="img-responsive"
					src="<%=basePath%>resources/images/map-red.png" />
			</div>
		</div>
	</div>
</div>



	<div class="modal fade" id="data-table-modal" tabindex="-1"
		role="dialog" aria-labelledby="">
		<div class="modal-dialog modal-lg modal-lg90" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="">ULBs list of <span id="approved_status_message">Approved</span> for as on '<label class="dateFetch"></label>'</h4>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-striped jambo_table">
							<thead>
								<tr>
									<th class="text-center">Sl.No.</th>
									<th class="text-center">Division</th>
									<th class="text-left">Urban Local Body</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<%@ include file="footer.jsp"%>
<script type="text/javascript">
	var dataEntryStatus = JSON.parse('${dataEntryStatus}');// json to javascript object which coming ulbmodels
</script>
<script src="<%=basePath%>resources/js/innerPage/data-approval.js"></script>