<%@ include file="header.jsp"%>
<%@ include file="entry-header.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
     
<%
      String message = (String) request.getAttribute("new.residential.connections.added");

      if (message != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message1 = (String) request.getAttribute("residential.connections.disconnected");

      if (message1 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message1%>"
       });
       });
      </script>
     <%
      }
     %>
 
<%
      String message2 = (String) request.getAttribute("New.Industrial.Connections.Added");

      if (message2 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message2%>"
       });
       });
      </script>
     <%
      }
     %>
 
 <%
      String message3 = (String) request.getAttribute("Industrial.Connections.Disconnected");

      if (message3 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message3%>"
       });
       });
      </script>
     <%
      }
     %>
  <%
      String message4 = (String) request.getAttribute("New.Commercial.Connections.Added");

      if (message4 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message4%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message5 = (String) request.getAttribute("Commercial.Connections.Disconnected");

      if (message5 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message5%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message6 = (String) request.getAttribute("New.Institutional.Connections.Added");

      if (message6 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message6%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message7 = (String) request.getAttribute("New.Institutional.Connections.Disconnected");

      if (message7 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message7%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message8 = (String) request.getAttribute("data.entry.exist");

      if (message8 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message8%>"
       });
       });
      </script>
     <%
      }
     %> 
      <%
      String message9 = (String) request.getAttribute("new.residential.connections.added.maxlength");

      if (message9 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message9%>"
       });
       });
      </script>
     <%
      }
     %>
 
<%
      String message10 = (String) request.getAttribute("residential.connections.disconnected.maxlength");

      if (message10 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message10%>"
       });
       });
      </script>
     <%
      }
     %>
 
 <%
      String message11 = (String) request.getAttribute("New.Industrial.Connections.Added.maxlength");

      if (message11 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message11%>"
       });
       });
      </script>
     <%
      }
     %>
  <%
      String message12 = (String) request.getAttribute("Industrial.Connections.Disconnected.maxlength");

      if (message12 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message12%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message13 = (String) request.getAttribute("New.Commercial.Connections.Added.maxlength");

      if (message13 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message13%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message14 = (String) request.getAttribute("Commercial.Connections.Disconnected.maxlength");

      if (message14 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message14%>"
       });
       });
      </script>
     <%
      }
     %>
 <%
      String message15 = (String) request.getAttribute("New.Institutional.Connections.Added.maxlength");

      if (message15 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message15%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message16 = (String) request.getAttribute("New.Institutional.Connections.Disconnected.maxlength");

      if (message16 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message16%>"
       });
       });
      </script>
     <%
      }
     %> 
     <%	
			String message17 = (String) request.getAttribute("remarks.maxlength");

      if (message17 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message17%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message18 = (String) request.getAttribute("ULBID.needed");

      if (message18 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message18%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message19 = (String) request.getAttribute("dateofreport.needed");

      if (message19 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message19%>"
       });
       });
      </script>
     <%
      }
     %>
     <div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="list-daily-connection" class="edit-link">
					<i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit
		</a>
	</div>
</div>
		<div class="col-md-12">
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3">
				<div class="x_panel">
					<div class="x_title text-center text-capitalize">
						<h2>
							Connections
						</h2>
						<h4>
							Figures for: <label id="show_date"><fmt:parseDate value="${startDate}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></label>
						</h4>
					</div>
					<div class="x_content">
						<br>
						<form action="add-daily-connection" method="post" onsubmit="return dailyConnection();" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
						<input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
							<div class="form-field">
								<div class="form-control-wrap">
									<input class="form-control" id="date_of_report" placeholder="" value="" readonly="" name="date_of_report" type="hidden">

								</div>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of New Residential Connections
													Added:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_new_res_cons_add" id="no_new_res_cons_add" value="${fn:substringBefore(dailyConnectionsModel.no_new_res_cons_add, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">

								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Residential Connections Disconnected:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_res_cons_disc" id="no_res_cons_disc" value="${fn:substringBefore(dailyConnectionsModel.no_res_cons_disc, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of New Industrial Connections
													Added:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="no_new_ind_cons_add" id="no_new_ind_cons_add" value="${fn:substringBefore(dailyConnectionsModel.no_new_ind_cons_add, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Industrial Connections
													Disconnected: <span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" id="no_ind_cons_disc" name="no_ind_cons_disc" value="${fn:substringBefore(dailyConnectionsModel.no_ind_cons_disc, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of New Commercial Connections
													Added:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" id="no_new_com_cons_add" name="no_new_com_cons_add" value="${fn:substringBefore(dailyConnectionsModel.no_new_com_cons_add, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>
									<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Commercial Connections
													Disconnected:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" id="no_com_cons_disc" name="no_com_cons_disc" value="${fn:substringBefore(dailyConnectionsModel.no_com_cons_disc, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>
									<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of New Institutional
													Connections Added:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" id="no_new_inst_cons_add" name="no_new_inst_cons_add" value="${fn:substringBefore(dailyConnectionsModel.no_new_inst_cons_add, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>
									<div class="form-group">
								<label class="control-label text-left" for="first-name">No. of Institutional Connections
													Disconnected:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" id="no_inst_cons_disc" name="no_inst_cons_disc" value="${fn:substringBefore(dailyConnectionsModel.no_inst_cons_disc, '.')}" class="form-control col-md-7 col-xs-12 number_only" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">Remarks : </label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<textarea class="form-control" rows="5" name="remarks" maxlength="200">${dailyConnectionsModel.remarks}</textarea>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Save</button>
								<button type="reset" class="btn btn-primary">Clear</button>
							</div>

						</form>
					</div>
				</div>
			</div>
        </div>

<%@ include file="footer.jsp"%>
<script src="<%=basePath%>resources/validation/daily-connection.js"></script>