<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
							<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>No. of Ground Storage Reservoirs</th>
									<th>Capacity of Ground Storage Reservoirs (in ML)</th>
									<th>No. of Elevated Storage Reservoirs</th>
									<th>Capacity of Elevated Storage Reservoirs (in ML)</th>
									<th>Next Maintenance Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty physicalInfraStorageReservoirsModels}">
										<c:forEach items="${physicalInfraStorageReservoirsModels}"
											var="data" varStatus="loop">
											<tr>
												<c:set var="id" value="${data.phyInfra_sr_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
											
												<td><c:out value="${loop.index + 1}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td>${fn:substringBefore(data.no_grnd_strg_res, '.')}</td>
												<td>${data.cpty_grnd_strg_res}</td>
												<td>${fn:substringBefore(data.no_ele_strg_res, '.')}</td>
												<td>${data.cpty_ele_strg_res}</td>
												<td>${data.dueDate}</td>

												<td><a
													href="edit-physical-infra-storage-reserviors?phyInfra_sr_id=<%=id%>"><i
														class="fa fa-pencil"></i></a> 
													<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
													<i class="fa fa-trash-o delete_pointer"></i>
													</a>
												</td>
											</tr>
										</c:forEach>
									</c:when>
								</c:choose>
							</tbody>
						</table>