
<%
	String path2 = request.getContextPath();
	String basePath2 = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path2 + "/";
	//System.out.println("BasePath : " + basePath);
%>
<script>
	var isDashBoard = true;
</script>
<script src="./resources/dashboard/dashboard-header.js"
	data-user-level-id="${loginUsersModel2.userLevelModel.user_level_id }"
	data-Dist-id="${upperLevels.get(2)!=null?upperLevels.get(2):loginUsersModel2.phDistrictModel.dist_id }"
	data-is-uperlevel="${upperLevels.get(0)!=null }"
	data-ulb-id="${upperLevels.get(0)!=null?upperLevels.get(0):loginUsersModel2.ulbNameModel.ulbModel.m_ulb_id }"></script>
<div class="content-wrapper clearfix">
	<div class="search-bar search-bar-top">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group pull-left search-sec">
					<div class="search-panel">
						<div class="radio pull-left">
							<!-- <input name="radio1" id="inlineRadio1" value="option1" type="radio" checked> -->
							<input type="radio" name="inlineRadioOptions" id="inlineRadio1"
								value="daily" checked> <label for="inlineRadio1">Daily</label>
						</div>
						<div class="input-append date form_datetime pull-left">
							<div class="form-group">
								<div class='input-group date' id="dateIp">
									<input type='text' id="dashboard_date" class="form-control" readonly/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group pull-left search-sec">
					<div class="search-panel">
						<div class="radio pull-left">
							<!-- <input name="radio1" id="inlineRadio2" value="option1" type="radio"> -->
							<input type="radio" name="inlineRadioOptions" id="inlineRadio2"
								value="monthly"> <label id="monthlyLabel" for="inlineRadio2">Monthly</label>
						</div>
						<div class="pull-left custome-btn">
							<select class="form-control select" name="month" disabled>								
							</select>
						</div>
						<div class="pull-left custome-btn" style="display:none;">
							<select class="form-control select" name="selectedYear" disabled>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group pull-left search-sec">
					<div class="search-panel">
						<div class="label-txt pull-left">
							<label>Division </label>
						</div>
						<div class="pull-left custome-btn">
							<select class="form-control select" id="divisionId">
								<option>ALL</option>
								<c:forEach items="${phDivisionModels}" var="division">
									<option value="${division.div_id}"
										<c:if test="${division.div_id == loginUsersModel2.phDivisionModel.div_id || upperLevels.get(3)==division.div_id}">selected</c:if>>${division.div_name}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group pull-left search-sec">
					<div class="search-panel">
						<div class="label-txt pull-left">
							<label>District</label>
						</div>
						<div class="pull-left custome-btn short-btn">
							<select class="form-control select" id="disSelect">
								<option>District</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group pull-left search-sec">
					<div class="search-panel">
						<div class="label-txt pull-left">
							<label>ULB</label>
						</div>
						<div class="pull-left custome-btn short-btn">
							<select class="form-control select" id="ulbSelect">
								<option>ULB</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>