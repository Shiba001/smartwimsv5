<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.orissa.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.Map.*"%>
<%@ page import="com.orissa.model.*"%>
<%@ page import="java.util.Date,java.text.SimpleDateFormat,java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<% List<String> datesBetween = (List<String>) request.getAttribute("datesBetween"); %>
<% SimpleDateFormat dateFormatWithTime = new SimpleDateFormat("MM/dd/yyyy hh:mm");%>
						<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
								<tr>
									<th>Urban Local Body</th>
									<% 
									for(int day = 0;day<datesBetween.size();day++){ %>
									<% 
								     String dateStr = datesBetween.get(day);
								     SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
								     Date result = formater.parse(dateStr);
								     SimpleDateFormat newFormater = new SimpleDateFormat("dd MMM yyyy");
								%>
									<th><%= newFormater.format(result) %></th>
									<% } %>
								</tr>
							</thead>
							<tbody>
							<c:choose>
									<c:when test="${not empty ultimateMap}">
										<c:forEach items="${ultimateMap}" var="type">
											<tr>
												<td><c:out value="${type.key}" /></td>
												<c:forEach items="${type.value}" var="data">
													<c:choose>
															<c:when test="${data >= 80 }">
																<td style="color:#3c8a2e;">${data}</td>
															</c:when>
															<c:when test="${data < 80 && data >=50 }">
																<td style="color:#c3c3c3;">${data}</td>
															</c:when>
															<c:otherwise>
																<td style="color:#000000;">${data}</td>
															</c:otherwise>
														</c:choose>
													
												</c:forEach>
											</tr>
										</c:forEach>
										<tr>
												<td><b>Total</b></td>
												<c:forEach items="${totalValue}" var="totalData">
												<c:choose>
															<c:when test="${totalData >= 80 }">
																<td style="color:#3c8a2e;"><b>${totalData}</b></td>
															</c:when>
															<c:when test="${totalData < 80 && totalData >=50 }">
																<td style="color:#c3c3c3;"><b>${totalData}</b></td>
															</c:when>
															<c:otherwise>
																<td style="color:#000000;"><b>${totalData}</b></td>
															</c:otherwise>
														</c:choose>
												</c:forEach>
											</tr>
									</c:when>
								</c:choose>
							</tbody>
						</table>
					