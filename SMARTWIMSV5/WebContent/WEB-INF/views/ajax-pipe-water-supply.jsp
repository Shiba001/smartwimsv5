<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
						<table id="datatable-buttons1"
							class="table table-striped jambo_table">
							<thead>
								<tr>
									<!-- 									<th>Sl.No.</th> -->
									<th>Urban Local Body</th>
									<th>Quantity Supplied by PWS (in ML)</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty dailyWaterSupplyModels}">
										<c:forEach items="${dailyWaterSupplyModels}" var="data"
											varStatus="loop">
											<c:choose>
												<c:when test="${data.type == 'current' && not loop.last}">
													<tr>
														<%-- <td><c:choose>
															  <c:when test="${data.slNo == null }">
															    Total
															  </c:when>
															  <c:otherwise>
															   <c:out value="${loop.index+1}" />
															  </c:otherwise>
														  </c:choose></td> --%>
														<td><c:out
																value="${data.ulbNameModel.ulbModel.ulb_name}" /></td>
														<td>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${data.qlty_supply}" />
														
														</td>

													</tr>
												</c:when>
												<c:when test="${loop.last}">
												   <tr>
												   <td><b>Total:</b></td>
														<td><b>
														<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${data.total_qlty_supply}" />
														</b></td>
												   </tr>												
												  </c:when>
											</c:choose> 
										</c:forEach>
									</c:when>
									<c:otherwise>
										<td colspan="10" align="center">No Records Found</td>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>