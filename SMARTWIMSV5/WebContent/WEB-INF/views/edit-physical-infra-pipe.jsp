<%@ include file="header.jsp"%>
<fmt:parseDate value="${physicalInfraLengthOfPipeModel.dueDate}" var="parsedDate" 
                              pattern="dd-MM-yyyy" /><fmt:formatDate pattern="yyyy-MM-dd" value="${parsedDate}" var="fmtDueDate"/>
                               <c:set var="date_of_report" value="${physicalInfraLengthOfPipeModel.getDate_of_report()}"/>
<%@ include file="entry-header.jsp"%> 
<%@ page import="com.orissa.common.*"%>
<%	
		String strip_empty = (String) request.getAttribute("strip.empty");

      if (strip_empty != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=strip_empty%>"
       });
       });
      </script>
     <%
      }
     %>
<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>

	<%	
		String error = (String) request.getAttribute("error");
      if (error != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=error%>"
       });
       });
      </script>
     <%
      }
     %>
     
	<%
      String message = (String) request.getAttribute("Length.of.Rising.Main.(in.Kms)");

      if (message != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message%>"
       });
       });
      </script>
     <%
      }
     %>
<%
      String message1 = (String) request.getAttribute("Length.of.Distribution.Network.(in.Kms)");

      if (message1 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message1%>"
       });
       });
      </script>
     <%
      }
     %> 
     
  <%
      String message2 = (String) request.getAttribute("Length.of.Rising.Main.(in.Kms).maxlength");

      if (message2 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message2%>"
       });
       });
      </script>
     <%
      }
     %> 
     
     <%
      String message3 = (String) request.getAttribute("Length.of.Distribution.Network.(in.Kms).maxlength");

      if (message3 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message3%>"
       });
       });
      </script>
     <%
      }
     %>       
    
     <%	
			String message11 = (String) request.getAttribute("ULBID.needed");

      if (message11 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message11%>"
       });
       });
      </script>
     <%
      }
     %>
     
     <%	
			String message12 = (String) request.getAttribute("dateofreport.needed");

      if (message12 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message12%>"
       });
       });
      </script>
     <%
      }
     %> 
     
     <%	
			String message13 = (String) request.getAttribute("duedate.needed");

      if (message13 != null) {
     %>
     <script>
       $(document).ready(function() {
       jQuery.growl.error({
        message : "<%=message13%>"
       });
       });
      </script>
     <%
      }
     %>
     
  <div class="col-md-12">
	<div class="black-btn pull-right">
		<a href="list-physical-infra-pipe" class="edit-link">
					<i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit
		</a>
	</div>
</div>
<div class="col-md-12">
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3">
				<div class="x_panel">
					<div class="x_title text-center text-capitalize">
						<h2>
							Physical Infra Pipe Line
						</h2>
						<h4>
							Figures for: <label id="show_date"><fmt:parseDate value="${physicalInfraLengthOfPipeModel.date_of_report}" var="parsedEmpDate" 
                              pattern="yyyy-MM-dd" /><fmt:formatDate pattern="dd-MM-yyyy" value="${parsedEmpDate}" /></label>
						</h4>
					</div>
					<div class="x_content">
						<br>
						<form action="update-physical-infra-pipe" method="post" onsubmit="return physicalInfraPipe();" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
						<input type="hidden" name="csrfPreventionSalt" value="<%= request.getAttribute("csrfPreventionSaltPage")%>"/>
						<c:set var="id" value="${physicalInfraLengthOfPipeModel.phyInfra_lpid}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
							<input type="hidden" name="phyInfraLpid"
								value="<%=id%>">
							<div class="form-field">
								<div class="form-control-wrap">
									<input class="form-control" id="date_of_report" placeholder="" value="" readonly="" name="date_of_report" type="hidden">

								</div>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Length of Rising Main (in Kms):<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input maxlength="18" name="rising_main" id="rising_main" value="${physicalInfraLengthOfPipeModel.rising_main}" class="form-control col-md-7 col-xs-12 decimal_number" type="text">

								</div>
							</div>

							<div class="form-group">
								<label class="control-label text-left" for="first-name">Length of Distribution Network (in
									Kms):<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input name="distr_netw" id="distr_netw" value="${physicalInfraLengthOfPipeModel.distr_netw}" class="form-control col-md-7 col-xs-12 decimal_number" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label text-left" for="first-name">Next Maintenance Date:<span class="required">*</span>
								</label>
								<div class="col-md-12 col-sm-12 col-xs-12 date" id="dateIp">
									<input  name="dueDate" id="dueDate"  value=""  class="form-control col-md-7 col-xs-12" type="text" readonly>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Update</button>
								<a href="list-physical-infra-pipe" class="btn btn-primary">Back</a>
							</div>

						</form>
					</div>
				</div>
			</div>
        </div>
  
<%@ include file="footer.jsp"%>
<script src="<%=basePath%>resources/validation/physical-infra-pipeline.js"></script>
