<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.orissa.common.*"%>

<%
	MessageUtil messageUtil = new MessageUtil();
	Encryption encryption = new Encryption();
	String key = null;
	try {
		key = messageUtil.getBundle("secret.key");

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
<table id="datatable-buttons2"
							class="table table-striped jambo_table">
							<thead>
							<tr>
									<th>Sl.No.</th>
									<th>Urban Local Body</th>
									<th>No. of Existing Standposts</th>
									<th>No. of Standposts in Working Condition</th>
									<th>Next Maintenance Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty physicalInfraStandpostsModels}">
										<c:forEach items="${physicalInfraStandpostsModels}" var="data">
											<c:set var="count" value="${count + 1}" scope="page" />
											<tr>
												<c:set var="id" value="${data.phyInfra_std_post_id}" />
												<%
													String id = null;
																try {
																	id = String.valueOf(pageContext.getAttribute("id"));
																	id = encryption.encode(key, id);

																} catch (Exception e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
												%>
											
												<td><c:out value="${count}" /></td>
												<td>${data.ulbNameModel.ulb_Name}</td>
												<td>${data.no_exst_stanposts}</td>
												<td>${data.no_standposts_work_cond}</td>
												<td>${data.dueDate}</td>
												<td><a
													href="edit-physical-infra-standpost?phyInfra_std_post_id=<%=id%>"><i
														class="fa fa-pencil"></i></a> 
													<a onclick="return confirmation('<%=id%>');" title="Delete" data-toggle="modal" data-target="#deleteModal">
													<i class="fa fa-trash-o delete_pointer"></i>
													</a>
												</td>
											</tr>
										</c:forEach>
									</c:when>
								</c:choose>
							</tbody>
						</table>