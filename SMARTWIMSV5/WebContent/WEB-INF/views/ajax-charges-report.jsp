<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.orissa.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.Map.*"%>
<%@ page import="com.orissa.model.*"%>

						<table id="datatable-buttons2"
							class="table table-striped jambo_table">
						<thead>
								<tr>
									<th>Urban Local Body</th>
									<th>Water Charges Collection</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty targetModels}">
										<c:forEach items="${targetModels}" var="data"
											varStatus="loop">
											<c:choose>
												<c:when test="${data.type == 'current' && not loop.last}">
													<tr>
													<td><c:out	value="${data.ulbNameModel.ulbModel.ulb_name}" /></td>
														<c:choose>
															  <c:when test="${data.percentage >= 80 }">
																	<td style="color:#3c8a2e">${data.percentage}</td>
															   </c:when>
															   <c:when test="${data.percentage < 80 && data.percentage >=50 }">
																	<td style="color:#c3c3c3">${data.percentage}</td>
															   </c:when>
															   <c:otherwise>
																	<td style="color:#000">${data.percentage}</td>
															   </c:otherwise>
													   </c:choose>

													</tr>
												</c:when>
												<c:when test="${loop.last}">
													<tr>
														<td><b>Total:</b></td>
														<c:choose>
															  <c:when test="${data.total_percentage >= 80 }">
																	<td  style="color:#3c8a2e"><b>${data.total_percentage}</b></td>
															   </c:when>
															   <c:when test="${data.total_percentage < 80 && data.total_percentage >=50 }">
																	<td style="color:#c3c3c3"><b>${data.total_percentage}</b></td>
															   </c:when>
															   <c:otherwise>
																	<td style="color:#000"><b>${data.total_percentage}</b></td>
															   </c:otherwise>
													   </c:choose>
													</tr>
												</c:when>
											</c:choose>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<td colspan="10" align="center">No Records Found</td>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					