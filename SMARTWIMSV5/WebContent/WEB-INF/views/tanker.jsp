<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="header.jsp"%>
<%@ include file="dashboard-header.jsp"%>
<!-- page content -->
   <div class="col-md-12">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="">

					<div>
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="x_panel">
									<div class="x_title">
										<h2 class="custome-h2">View Tankers Water Supply Details </h2>
									</div>
									<div class="x_content table-div table-responsive">
										<table id="datatable-buttons2"
											class="table table-striped jambo_table">
											<thead>
												<tr>
													<th>Urban Local Body</th>
													<th>No. of Departmental Tankers Engaged</th>
													<th>No. of Tankers Hired</th>
													<th>No. of PVC/RCC Tanks Deployed</th>
													<th>Population Served </th>
													<th>Quantity supplied (in Ltr)</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
													<c:when test="${not empty dailyTankersModels}">
														<c:forEach items="${dailyTankersModels}" var="data" varStatus="loop">														
													<c:choose>
															  <c:when test="${data.type == 'current' && not loop.last}">
																	<tr>
																		<td><c:out
																				value="${data.ulbNameModel.ulbModel.ulb_name}" /></td>
																		<td><c:out value="${data.no_tank_engd_dept}" /></td>
																		<td><c:out value="${data.no_tank_hire}" /></td>
																		<td><c:out value="${data.no_pvc_rcc_tanks_depl}" /></td>
																		<td>${fn:substringBefore(data.popu_served_tanks_pvc_rcc, '.')}</td>
																		<td><c:out value="${data.qty_mld}" /></td>

																	</tr>
													 </c:when>
													 <c:when test="${loop.last}">
												   <tr>
														<td><b>Total :</b></td>
														<td><b><c:out value="${data.total_tankers_engaged}" /></b></td>
														<td><b><c:out value="${data.total_tankers_hired}" /></b></td>
														<td><b><c:out value="${data.total_tanks_deployed}" /></b></td>
														<td><b>${fn:substringBefore(data.total_population_served, '.')}</b></td>
														<td><b><c:out value="${data.total_quantity_supplied}" /></b></td>
													</tr>												
												  </c:when>
													 </c:choose>
														</c:forEach>
													</c:when>
													<c:otherwise>
														<td colspan="10" align="center">No Records Found</td>
													</c:otherwise>
												</c:choose>
											</tbody>
										</table>
									</div>
								</div>

							</div>
							
						</div>

						<!-- For Modal -->
						<div class="col-md-4 col-sm-4 col-xs-12"></div>


					</div>
				</div>
			</div>
		</div>

<!-- /page content -->
<%@ include file="footer.jsp"%>
	<script>
	$(document).ready(function() {
		var handleDataTableButtons = function() {
			if ($("#datatable-buttons2").length) {
				$("#datatable-buttons2").DataTable({
					dom : "Bfrtip",
					paging : false,
					bSort : false,
					buttons : [ {
						extend : "copy",
						className : "btn-sm"
					}, {
						extend : "csv",
						className : "btn-sm"
					}, {
						extend : "excel",
						className : "btn-sm"
					}, {
						extend : "pdf",
						className : "btn-sm"
					}, {
						extend : "print",
						className : "btn-sm"
					}, ],
					responsive : true
				});
			}
		};

		TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons();
				}
			};
		}();

		$('#datatable').dataTable();

		$('#datatable-keytable').DataTable({
			keys : true
		});

		$('#datatable-responsive').DataTable();

		$('#datatable-scroller').DataTable({
			ajax : "js/datatables/json/scroller-demo.json",
			deferRender : true,
			scrollY : 380,
			scrollCollapse : true,
			scroller : true
		});

		$('#datatable-fixed-header').DataTable({
			fixedHeader : true
		});

		var $datatable = $('#datatable-checkbox');

		$datatable.dataTable({
			'order' : [ [ 1, 'asc' ] ],
			'columnDefs' : [ {
				orderable : false,
				targets : [ 0 ]
			} ]
		});
		$datatable.on('draw.dt', function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_flat-green'
			});
		});

		TableManageButtons.init();
	});
</script>

<!-- Datatables -->
<script src="<%=basePath%>resources/js/datatable/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<%=basePath%>resources/js/datatable/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<!-- Datatables -->
	<script src="<%=basePath%>resources/js/innerPage/tanker.js"></script>